gdjs.GameCode = {};
gdjs.GameCode.GDmobile_95jumpObjects1_1final = [];

gdjs.GameCode.GDmobile_95slideObjects1_1final = [];

gdjs.GameCode.GDplayerObjects1= [];
gdjs.GameCode.GDplayerObjects2= [];
gdjs.GameCode.GDplayerObjects3= [];
gdjs.GameCode.GDgroundObjects1= [];
gdjs.GameCode.GDgroundObjects2= [];
gdjs.GameCode.GDgroundObjects3= [];
gdjs.GameCode.GDbuttomObjects1= [];
gdjs.GameCode.GDbuttomObjects2= [];
gdjs.GameCode.GDbuttomObjects3= [];
gdjs.GameCode.GDbackgroundObjects1= [];
gdjs.GameCode.GDbackgroundObjects2= [];
gdjs.GameCode.GDbackgroundObjects3= [];
gdjs.GameCode.GDpoeiraObjects1= [];
gdjs.GameCode.GDpoeiraObjects2= [];
gdjs.GameCode.GDpoeiraObjects3= [];
gdjs.GameCode.GDobstaclesObjects1= [];
gdjs.GameCode.GDobstaclesObjects2= [];
gdjs.GameCode.GDobstaclesObjects3= [];
gdjs.GameCode.GDmobile_95slideObjects1= [];
gdjs.GameCode.GDmobile_95slideObjects2= [];
gdjs.GameCode.GDmobile_95slideObjects3= [];
gdjs.GameCode.GDmobile_95jumpObjects1= [];
gdjs.GameCode.GDmobile_95jumpObjects2= [];
gdjs.GameCode.GDmobile_95jumpObjects3= [];
gdjs.GameCode.GDpontosObjects1= [];
gdjs.GameCode.GDpontosObjects2= [];
gdjs.GameCode.GDpontosObjects3= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};
gdjs.GameCode.conditionTrue_2 = {val:false};
gdjs.GameCode.condition0IsTrue_2 = {val:false};
gdjs.GameCode.condition1IsTrue_2 = {val:false};
gdjs.GameCode.condition2IsTrue_2 = {val:false};
gdjs.GameCode.condition3IsTrue_2 = {val:false};


gdjs.GameCode.eventsList0x950d10 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "Mobile");
}}

}


}; //End of gdjs.GameCode.eventsList0x950d10
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595jumpObjects2Objects = Hashtable.newFrom({"mobile_jump": gdjs.GameCode.GDmobile_95jumpObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595slideObjects2Objects = Hashtable.newFrom({"mobile_slide": gdjs.GameCode.GDmobile_95slideObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpoeiraObjects1Objects = Hashtable.newFrom({"poeira": gdjs.GameCode.GDpoeiraObjects1});gdjs.GameCode.eventsList0x956b28 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.sound.isSoundOnChannelStopped(runtimeScene, 7);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "run.ogg", 7, false, 100, 1.2);
}}

}


}; //End of gdjs.GameCode.eventsList0x956b28
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobstaclesObjects1Objects = Hashtable.newFrom({"obstacles": gdjs.GameCode.GDobstaclesObjects1});gdjs.GameCode.eventsList0x952fb8 = function(runtimeScene) {

{

gdjs.GameCode.GDobstaclesObjects2.createFrom(gdjs.GameCode.GDobstaclesObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobstaclesObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDobstaclesObjects2[i].getAnimation() < 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobstaclesObjects2[k] = gdjs.GameCode.GDobstaclesObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDobstaclesObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDgroundObjects2.createFrom(gdjs.GameCode.GDgroundObjects1);

/* Reuse gdjs.GameCode.GDobstaclesObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects2[i].setY((( gdjs.GameCode.GDgroundObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDgroundObjects2[0].getY()) - (gdjs.GameCode.GDobstaclesObjects2[i].getHeight()));
}
}}

}


{

/* Reuse gdjs.GameCode.GDobstaclesObjects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobstaclesObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDobstaclesObjects1[i].getAnimation() >= 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobstaclesObjects1[k] = gdjs.GameCode.GDobstaclesObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDobstaclesObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDgroundObjects1 */
/* Reuse gdjs.GameCode.GDobstaclesObjects1 */
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].setY((( gdjs.GameCode.GDgroundObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDgroundObjects1[0].getY()) - (gdjs.GameCode.GDobstaclesObjects1[i].getHeight()) - (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getHeight())/2 - 20);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x952fb8
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobstaclesObjects1Objects = Hashtable.newFrom({"obstacles": gdjs.GameCode.GDobstaclesObjects1});gdjs.GameCode.eventsList0x9538e8 = function(runtimeScene) {

{

gdjs.GameCode.GDobstaclesObjects2.createFrom(gdjs.GameCode.GDobstaclesObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobstaclesObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDobstaclesObjects2[i].getAnimation() < 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobstaclesObjects2[k] = gdjs.GameCode.GDobstaclesObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDobstaclesObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDgroundObjects2.createFrom(gdjs.GameCode.GDgroundObjects1);

/* Reuse gdjs.GameCode.GDobstaclesObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects2[i].setY((( gdjs.GameCode.GDgroundObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDgroundObjects2[0].getY()) - (gdjs.GameCode.GDobstaclesObjects2[i].getHeight()));
}
}}

}


{

/* Reuse gdjs.GameCode.GDobstaclesObjects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobstaclesObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDobstaclesObjects1[i].getAnimation() >= 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobstaclesObjects1[k] = gdjs.GameCode.GDobstaclesObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDobstaclesObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDgroundObjects1 */
/* Reuse gdjs.GameCode.GDobstaclesObjects1 */
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].setY((( gdjs.GameCode.GDgroundObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDgroundObjects1[0].getY()) - (gdjs.GameCode.GDobstaclesObjects1[i].getHeight()) - (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getHeight())/2 - 20);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x9538e8
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobstaclesObjects1Objects = Hashtable.newFrom({"obstacles": gdjs.GameCode.GDobstaclesObjects1});gdjs.GameCode.eventsList0x954240 = function(runtimeScene) {

{

gdjs.GameCode.GDobstaclesObjects2.createFrom(gdjs.GameCode.GDobstaclesObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobstaclesObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDobstaclesObjects2[i].getAnimation() < 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobstaclesObjects2[k] = gdjs.GameCode.GDobstaclesObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDobstaclesObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDgroundObjects2.createFrom(gdjs.GameCode.GDgroundObjects1);

/* Reuse gdjs.GameCode.GDobstaclesObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects2[i].setY((( gdjs.GameCode.GDgroundObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDgroundObjects2[0].getY()) - (gdjs.GameCode.GDobstaclesObjects2[i].getHeight()));
}
}}

}


{

/* Reuse gdjs.GameCode.GDobstaclesObjects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobstaclesObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDobstaclesObjects1[i].getAnimation() >= 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobstaclesObjects1[k] = gdjs.GameCode.GDobstaclesObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDobstaclesObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDgroundObjects1 */
/* Reuse gdjs.GameCode.GDobstaclesObjects1 */
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].setY((( gdjs.GameCode.GDgroundObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDgroundObjects1[0].getY()) - (gdjs.GameCode.GDobstaclesObjects1[i].getHeight()) - (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getHeight())/2 - 20);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x954240
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.GameCode.GDplayerObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobstaclesObjects1Objects = Hashtable.newFrom({"obstacles": gdjs.GameCode.GDobstaclesObjects1});gdjs.GameCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}
{ //Subevents
gdjs.GameCode.eventsList0x950d10(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbackgroundObjects1.createFrom(runtimeScene.getObjects("background"));
gdjs.GameCode.GDgroundObjects1.createFrom(runtimeScene.getObjects("ground"));
gdjs.GameCode.GDpontosObjects1.createFrom(runtimeScene.getObjects("pontos"));
{for(var i = 0, len = gdjs.GameCode.GDgroundObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDgroundObjects1[i].setXOffset(gdjs.GameCode.GDgroundObjects1[i].getXOffset() + (3));
}
}{for(var i = 0, len = gdjs.GameCode.GDbackgroundObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbackgroundObjects1[i].setXOffset(gdjs.GameCode.GDbackgroundObjects1[i].getXOffset() + (1));
}
}{for(var i = 0, len = gdjs.GameCode.GDpontosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDpontosObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDpontosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDpontosObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.GameCode.GDpontosObjects1[i].getWidth())/2);
}
}}

}


{



}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
gdjs.GameCode.GDmobile_95jumpObjects1.length = 0;


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.GameCode.GDplayerObjects1[i].getBehavior("PlatformerObject").isJumping()) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.GDmobile_95jumpObjects1_1final.length = 0;gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "Up");
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
gdjs.GameCode.GDmobile_95jumpObjects2.createFrom(runtimeScene.getObjects("mobile_jump"));
{gdjs.GameCode.conditionTrue_2 = gdjs.GameCode.condition1IsTrue_1;
gdjs.GameCode.condition0IsTrue_2.val = false;
gdjs.GameCode.condition1IsTrue_2.val = false;
gdjs.GameCode.condition2IsTrue_2.val = false;
{
gdjs.GameCode.condition0IsTrue_2.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Mobile");
}if ( gdjs.GameCode.condition0IsTrue_2.val ) {
{
gdjs.GameCode.condition1IsTrue_2.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595jumpObjects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition1IsTrue_2.val ) {
{
gdjs.GameCode.condition2IsTrue_2.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
gdjs.GameCode.conditionTrue_2.val = true && gdjs.GameCode.condition0IsTrue_2.val && gdjs.GameCode.condition1IsTrue_2.val && gdjs.GameCode.condition2IsTrue_2.val;
}
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDmobile_95jumpObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDmobile_95jumpObjects1_1final.indexOf(gdjs.GameCode.GDmobile_95jumpObjects2[j]) === -1 )
            gdjs.GameCode.GDmobile_95jumpObjects1_1final.push(gdjs.GameCode.GDmobile_95jumpObjects2[j]);
    }
}
}
{
gdjs.GameCode.GDmobile_95jumpObjects1.createFrom(gdjs.GameCode.GDmobile_95jumpObjects1_1final);
}
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "jumppp11.ogg", false, 100, 1);
}}

}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
gdjs.GameCode.GDmobile_95slideObjects1.length = 0;


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.GameCode.GDplayerObjects1[i].isCurrentAnimationName("slide")) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.GDmobile_95slideObjects1_1final.length = 0;gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "Down");
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
gdjs.GameCode.GDmobile_95slideObjects2.createFrom(runtimeScene.getObjects("mobile_slide"));
{gdjs.GameCode.conditionTrue_2 = gdjs.GameCode.condition1IsTrue_1;
gdjs.GameCode.condition0IsTrue_2.val = false;
gdjs.GameCode.condition1IsTrue_2.val = false;
gdjs.GameCode.condition2IsTrue_2.val = false;
{
gdjs.GameCode.condition0IsTrue_2.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Mobile");
}if ( gdjs.GameCode.condition0IsTrue_2.val ) {
{
gdjs.GameCode.condition1IsTrue_2.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595slideObjects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition1IsTrue_2.val ) {
{
gdjs.GameCode.condition2IsTrue_2.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
gdjs.GameCode.conditionTrue_2.val = true && gdjs.GameCode.condition0IsTrue_2.val && gdjs.GameCode.condition1IsTrue_2.val && gdjs.GameCode.condition2IsTrue_2.val;
}
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDmobile_95slideObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDmobile_95slideObjects1_1final.indexOf(gdjs.GameCode.GDmobile_95slideObjects2[j]) === -1 )
            gdjs.GameCode.GDmobile_95slideObjects1_1final.push(gdjs.GameCode.GDmobile_95slideObjects2[j]);
    }
}
}
{
gdjs.GameCode.GDmobile_95slideObjects1.createFrom(gdjs.GameCode.GDmobile_95slideObjects1_1final);
}
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects1 */
gdjs.GameCode.GDpoeiraObjects1.length = 0;

{gdjs.evtTools.sound.playSound(runtimeScene, "slightscream-05.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].setAnimationName("slide");
}
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpoeiraObjects1Objects, (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointX("")) + (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getWidth())/2 - 50, (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointY("")) + (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getHeight()) - 15, "");
}{for(var i = 0, len = gdjs.GameCode.GDpoeiraObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDpoeiraObjects1[i].setAngle(180);
}
}}

}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].setAnimationName("jump");
}
}}

}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].hasAnimationEnded() ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].setAnimationName("run");
}
}
{ //Subevents
gdjs.GameCode.eventsList0x956b28(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, gdjs.randomInRange(2,  3), "obstacle");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) <= 10;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDgroundObjects1.createFrom(runtimeScene.getObjects("ground"));
gdjs.GameCode.GDobstaclesObjects1.length = 0;

{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "obstacle");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobstaclesObjects1Objects, gdjs.evtTools.window.getWindowWidth() + 300, (( gdjs.GameCode.GDgroundObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDgroundObjects1[0].getY()), "Obstacles");
}{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].setScale(0.7);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x952fb8(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, gdjs.randomInRange(1,  4), "obstacle");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) > 10;
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) <= 20;
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
gdjs.GameCode.GDgroundObjects1.createFrom(runtimeScene.getObjects("ground"));
gdjs.GameCode.GDobstaclesObjects1.length = 0;

{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "obstacle");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobstaclesObjects1Objects, gdjs.evtTools.window.getWindowWidth() + 300, (( gdjs.GameCode.GDgroundObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDgroundObjects1[0].getY()), "Obstacles");
}{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].setAnimation(gdjs.random(6));
}
}{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].setScale(0.7);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x9538e8(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, gdjs.randomInRange(1,  3), "obstacle");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) > 20;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDgroundObjects1.createFrom(runtimeScene.getObjects("ground"));
gdjs.GameCode.GDobstaclesObjects1.length = 0;

{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "obstacle");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobstaclesObjects1Objects, gdjs.evtTools.window.getWindowWidth() + 300, (( gdjs.GameCode.GDgroundObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDgroundObjects1[0].getY()), "Obstacles");
}{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].setAnimation(gdjs.random(6));
}
}{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].setScale(0.7);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x954240(runtimeScene);} //End of subevents
}

}


{



}


{


{
gdjs.GameCode.GDobstaclesObjects1.createFrom(runtimeScene.getObjects("obstacles"));
{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].addForce((gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDobstaclesObjects1[i].getVariables().getFromIndex(0))), 0, 0);
}
}}

}


{

gdjs.GameCode.GDobstaclesObjects1.createFrom(runtimeScene.getObjects("obstacles"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobstaclesObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDobstaclesObjects1[i].getAnimation() >= 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobstaclesObjects1[k] = gdjs.GameCode.GDobstaclesObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDobstaclesObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDobstaclesObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].rotate(-90, runtimeScene);
}
}}

}


{

gdjs.GameCode.GDobstaclesObjects1.createFrom(runtimeScene.getObjects("obstacles"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobstaclesObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDobstaclesObjects1[i].getX() < -200 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobstaclesObjects1[k] = gdjs.GameCode.GDobstaclesObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDobstaclesObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDobstaclesObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{

gdjs.GameCode.GDobstaclesObjects1.createFrom(runtimeScene.getObjects("obstacles"));
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobstaclesObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDobstaclesObjects1[i].getX() < (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointX("")) - (gdjs.GameCode.GDobstaclesObjects1[i].getWidth()) - 10 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobstaclesObjects1[k] = gdjs.GameCode.GDobstaclesObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDobstaclesObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobstaclesObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDobstaclesObjects1[i].getVariableNumber(gdjs.GameCode.GDobstaclesObjects1[i].getVariables().getFromIndex(1)) > 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDobstaclesObjects1[k] = gdjs.GameCode.GDobstaclesObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDobstaclesObjects1.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDobstaclesObjects1 */
{runtimeScene.getGame().getVariables().getFromIndex(0).add((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDobstaclesObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDobstaclesObjects1[0].getVariables()).getFromIndex(1))));
}{for(var i = 0, len = gdjs.GameCode.GDobstaclesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobstaclesObjects1[i].returnVariable(gdjs.GameCode.GDobstaclesObjects1[i].getVariables().getFromIndex(1)).setNumber(0);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Pickup_Coin17.ogg", false, 100, 1);
}}

}


{



}


{

gdjs.GameCode.GDobstaclesObjects1.createFrom(runtimeScene.getObjects("obstacles"));
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobstaclesObjects1Objects, false, runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Dead", false);
}}

}


}; //End of gdjs.GameCode.eventsList0xaff48


gdjs.GameCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.GameCode.GDplayerObjects1.length = 0;
gdjs.GameCode.GDplayerObjects2.length = 0;
gdjs.GameCode.GDplayerObjects3.length = 0;
gdjs.GameCode.GDgroundObjects1.length = 0;
gdjs.GameCode.GDgroundObjects2.length = 0;
gdjs.GameCode.GDgroundObjects3.length = 0;
gdjs.GameCode.GDbuttomObjects1.length = 0;
gdjs.GameCode.GDbuttomObjects2.length = 0;
gdjs.GameCode.GDbuttomObjects3.length = 0;
gdjs.GameCode.GDbackgroundObjects1.length = 0;
gdjs.GameCode.GDbackgroundObjects2.length = 0;
gdjs.GameCode.GDbackgroundObjects3.length = 0;
gdjs.GameCode.GDpoeiraObjects1.length = 0;
gdjs.GameCode.GDpoeiraObjects2.length = 0;
gdjs.GameCode.GDpoeiraObjects3.length = 0;
gdjs.GameCode.GDobstaclesObjects1.length = 0;
gdjs.GameCode.GDobstaclesObjects2.length = 0;
gdjs.GameCode.GDobstaclesObjects3.length = 0;
gdjs.GameCode.GDmobile_95slideObjects1.length = 0;
gdjs.GameCode.GDmobile_95slideObjects2.length = 0;
gdjs.GameCode.GDmobile_95slideObjects3.length = 0;
gdjs.GameCode.GDmobile_95jumpObjects1.length = 0;
gdjs.GameCode.GDmobile_95jumpObjects2.length = 0;
gdjs.GameCode.GDmobile_95jumpObjects3.length = 0;
gdjs.GameCode.GDpontosObjects1.length = 0;
gdjs.GameCode.GDpontosObjects2.length = 0;
gdjs.GameCode.GDpontosObjects3.length = 0;

gdjs.GameCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['GameCode'] = gdjs.GameCode;
