gdjs.StartCode = {};
gdjs.StartCode.GDplayerObjects1= [];
gdjs.StartCode.GDplayerObjects2= [];
gdjs.StartCode.GDgroundObjects1= [];
gdjs.StartCode.GDgroundObjects2= [];
gdjs.StartCode.GDbuttomObjects1= [];
gdjs.StartCode.GDbuttomObjects2= [];
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDbackground2Objects1= [];
gdjs.StartCode.GDbackground2Objects2= [];
gdjs.StartCode.GDgroundBordaObjects1= [];
gdjs.StartCode.GDgroundBordaObjects2= [];
gdjs.StartCode.GDobjects_950Objects1= [];
gdjs.StartCode.GDobjects_950Objects2= [];
gdjs.StartCode.GDnuvens1Objects1= [];
gdjs.StartCode.GDnuvens1Objects2= [];
gdjs.StartCode.GDlblTituloObjects1= [];
gdjs.StartCode.GDlblTituloObjects2= [];
gdjs.StartCode.GDlblJogarObjects1= [];
gdjs.StartCode.GDlblJogarObjects2= [];
gdjs.StartCode.GDninjaObjects1= [];
gdjs.StartCode.GDninjaObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbuttomObjects1Objects = Hashtable.newFrom({"buttom": gdjs.StartCode.GDbuttomObjects1});gdjs.StartCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "music.ogg", true, 100, 1);
}}

}


{


{
gdjs.StartCode.GDnuvens1Objects1.createFrom(runtimeScene.getObjects("nuvens1"));
{for(var i = 0, len = gdjs.StartCode.GDnuvens1Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDnuvens1Objects1[i].setXOffset(gdjs.StartCode.GDnuvens1Objects1[i].getXOffset() + (0.5));
}
}}

}


{

gdjs.StartCode.GDbuttomObjects1.createFrom(runtimeScene.getObjects("buttom"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbuttomObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xaff48


gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.StartCode.GDplayerObjects1.length = 0;
gdjs.StartCode.GDplayerObjects2.length = 0;
gdjs.StartCode.GDgroundObjects1.length = 0;
gdjs.StartCode.GDgroundObjects2.length = 0;
gdjs.StartCode.GDbuttomObjects1.length = 0;
gdjs.StartCode.GDbuttomObjects2.length = 0;
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDbackground2Objects1.length = 0;
gdjs.StartCode.GDbackground2Objects2.length = 0;
gdjs.StartCode.GDgroundBordaObjects1.length = 0;
gdjs.StartCode.GDgroundBordaObjects2.length = 0;
gdjs.StartCode.GDobjects_950Objects1.length = 0;
gdjs.StartCode.GDobjects_950Objects2.length = 0;
gdjs.StartCode.GDnuvens1Objects1.length = 0;
gdjs.StartCode.GDnuvens1Objects2.length = 0;
gdjs.StartCode.GDlblTituloObjects1.length = 0;
gdjs.StartCode.GDlblTituloObjects2.length = 0;
gdjs.StartCode.GDlblJogarObjects1.length = 0;
gdjs.StartCode.GDlblJogarObjects2.length = 0;
gdjs.StartCode.GDninjaObjects1.length = 0;
gdjs.StartCode.GDninjaObjects2.length = 0;

gdjs.StartCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['StartCode'] = gdjs.StartCode;
