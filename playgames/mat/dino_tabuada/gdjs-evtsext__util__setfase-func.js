gdjs.evtsExt__Util__setFase = {};

gdjs.evtsExt__Util__setFase.conditionTrue_0 = {val:false};
gdjs.evtsExt__Util__setFase.condition0IsTrue_0 = {val:false};


gdjs.evtsExt__Util__setFase.eventsList0x5b7328 = function(runtimeScene, eventsFunctionContext) {

{


{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Fase" + gdjs.evtTools.common.toString((typeof eventsFunctionContext !== 'undefined' ? Number(eventsFunctionContext.getArgument("fase")) || 0 : 0)), false);
}}

}


}; //End of gdjs.evtsExt__Util__setFase.eventsList0x5b7328


gdjs.evtsExt__Util__setFase.func = function(runtimeScene, fase, parentEventsFunctionContext) {
var eventsFunctionContext = {
  _objectsMap: {
},
  _behaviorNamesMap: {
},
  getObjects: function(objectName) {
        var objectsList = eventsFunctionContext._objectsMap[objectName];
    return objectsList ? gdjs.objectsListsToArray(objectsList) : [];
  },
  getObjectsLists: function(objectName) {
    return eventsFunctionContext._objectsMap[objectName] || null;
  },
  getBehaviorName: function(behaviorName) {
    return eventsFunctionContext._behaviorNamesMap[behaviorName];
  },
  createObject: function(objectName) {
    var objectsList = eventsFunctionContext._objectsMap[objectName];
    if (objectsList) {
      return parentEventsFunctionContext ?
        parentEventsFunctionContext.createObject(objectsList.firstKey()) :
        runtimeScene.createObject(objectsList.firstKey());
    }
    return null;
  },
  getArgument: function(argName) {
if (argName === "fase") return fase;
    return "";
  }
};


gdjs.evtsExt__Util__setFase.eventsList0x5b7328(runtimeScene, eventsFunctionContext);
return;
}

