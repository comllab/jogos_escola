gdjs.StartCode = {};
gdjs.StartCode.forEachCount0_2 = 0;

gdjs.StartCode.forEachCount0_3 = 0;

gdjs.StartCode.forEachCount1_2 = 0;

gdjs.StartCode.forEachCount1_3 = 0;

gdjs.StartCode.forEachCount2_2 = 0;

gdjs.StartCode.forEachCount2_3 = 0;

gdjs.StartCode.forEachCount3_2 = 0;

gdjs.StartCode.forEachCount3_3 = 0;

gdjs.StartCode.forEachCount4_2 = 0;

gdjs.StartCode.forEachCount4_3 = 0;

gdjs.StartCode.forEachCount5_2 = 0;

gdjs.StartCode.forEachCount5_3 = 0;

gdjs.StartCode.forEachIndex2 = 0;

gdjs.StartCode.forEachIndex3 = 0;

gdjs.StartCode.forEachObjects2 = [];

gdjs.StartCode.forEachObjects3 = [];

gdjs.StartCode.forEachTotalCount2 = 0;

gdjs.StartCode.forEachTotalCount3 = 0;

gdjs.StartCode.GDdinoObjects1= [];
gdjs.StartCode.GDdinoObjects2= [];
gdjs.StartCode.GDdinoObjects3= [];
gdjs.StartCode.GDplayerObjects1= [];
gdjs.StartCode.GDplayerObjects2= [];
gdjs.StartCode.GDplayerObjects3= [];
gdjs.StartCode.GDground1Objects1= [];
gdjs.StartCode.GDground1Objects2= [];
gdjs.StartCode.GDground1Objects3= [];
gdjs.StartCode.GDwaterObjects1= [];
gdjs.StartCode.GDwaterObjects2= [];
gdjs.StartCode.GDwaterObjects3= [];
gdjs.StartCode.GDfade_95faseObjects1= [];
gdjs.StartCode.GDfade_95faseObjects2= [];
gdjs.StartCode.GDfade_95faseObjects3= [];
gdjs.StartCode.GDlblFaseObjects1= [];
gdjs.StartCode.GDlblFaseObjects2= [];
gdjs.StartCode.GDlblFaseObjects3= [];
gdjs.StartCode.GDpanelObjects1= [];
gdjs.StartCode.GDpanelObjects2= [];
gdjs.StartCode.GDpanelObjects3= [];
gdjs.StartCode.GDfaseObjects1= [];
gdjs.StartCode.GDfaseObjects2= [];
gdjs.StartCode.GDfaseObjects3= [];
gdjs.StartCode.GDdo_95dinoObjects1= [];
gdjs.StartCode.GDdo_95dinoObjects2= [];
gdjs.StartCode.GDdo_95dinoObjects3= [];
gdjs.StartCode.GDtabuadas_95doObjects1= [];
gdjs.StartCode.GDtabuadas_95doObjects2= [];
gdjs.StartCode.GDtabuadas_95doObjects3= [];
gdjs.StartCode.GDstarsObjects1= [];
gdjs.StartCode.GDstarsObjects2= [];
gdjs.StartCode.GDstarsObjects3= [];
gdjs.StartCode.GDplayObjects1= [];
gdjs.StartCode.GDplayObjects2= [];
gdjs.StartCode.GDplayObjects3= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};


gdjs.StartCode.eventsList0x904c54 = function(runtimeScene) {

}; //End of gdjs.StartCode.eventsList0x904c54
gdjs.StartCode.eventsList0x903ffc = function(runtimeScene) {

{

gdjs.StartCode.GDdo_95dinoObjects2.createFrom(runtimeScene.getObjects("do_dino"));
gdjs.StartCode.GDfaseObjects2.createFrom(runtimeScene.getObjects("fase"));
gdjs.StartCode.GDpanelObjects2.createFrom(runtimeScene.getObjects("panel"));
gdjs.StartCode.GDplayObjects2.createFrom(runtimeScene.getObjects("play"));
gdjs.StartCode.GDstarsObjects2.createFrom(runtimeScene.getObjects("stars"));
gdjs.StartCode.GDtabuadas_95doObjects2.createFrom(runtimeScene.getObjects("tabuadas_do"));

gdjs.StartCode.forEachTotalCount3 = 0;
gdjs.StartCode.forEachObjects3.length = 0;
gdjs.StartCode.forEachCount0_3 = gdjs.StartCode.GDpanelObjects2.length;
gdjs.StartCode.forEachTotalCount3 += gdjs.StartCode.forEachCount0_3;
gdjs.StartCode.forEachObjects3.push.apply(gdjs.StartCode.forEachObjects3,gdjs.StartCode.GDpanelObjects2);
gdjs.StartCode.forEachCount1_3 = gdjs.StartCode.GDfaseObjects2.length;
gdjs.StartCode.forEachTotalCount3 += gdjs.StartCode.forEachCount1_3;
gdjs.StartCode.forEachObjects3.push.apply(gdjs.StartCode.forEachObjects3,gdjs.StartCode.GDfaseObjects2);
gdjs.StartCode.forEachCount2_3 = gdjs.StartCode.GDdo_95dinoObjects2.length;
gdjs.StartCode.forEachTotalCount3 += gdjs.StartCode.forEachCount2_3;
gdjs.StartCode.forEachObjects3.push.apply(gdjs.StartCode.forEachObjects3,gdjs.StartCode.GDdo_95dinoObjects2);
gdjs.StartCode.forEachCount3_3 = gdjs.StartCode.GDtabuadas_95doObjects2.length;
gdjs.StartCode.forEachTotalCount3 += gdjs.StartCode.forEachCount3_3;
gdjs.StartCode.forEachObjects3.push.apply(gdjs.StartCode.forEachObjects3,gdjs.StartCode.GDtabuadas_95doObjects2);
gdjs.StartCode.forEachCount4_3 = gdjs.StartCode.GDstarsObjects2.length;
gdjs.StartCode.forEachTotalCount3 += gdjs.StartCode.forEachCount4_3;
gdjs.StartCode.forEachObjects3.push.apply(gdjs.StartCode.forEachObjects3,gdjs.StartCode.GDstarsObjects2);
gdjs.StartCode.forEachCount5_3 = gdjs.StartCode.GDplayObjects2.length;
gdjs.StartCode.forEachTotalCount3 += gdjs.StartCode.forEachCount5_3;
gdjs.StartCode.forEachObjects3.push.apply(gdjs.StartCode.forEachObjects3,gdjs.StartCode.GDplayObjects2);
for(gdjs.StartCode.forEachIndex3 = 0;gdjs.StartCode.forEachIndex3 < gdjs.StartCode.forEachTotalCount3;++gdjs.StartCode.forEachIndex3) {
gdjs.StartCode.GDdo_95dinoObjects3.length = 0;

gdjs.StartCode.GDfaseObjects3.length = 0;

gdjs.StartCode.GDpanelObjects3.length = 0;

gdjs.StartCode.GDplayObjects3.length = 0;

gdjs.StartCode.GDstarsObjects3.length = 0;

gdjs.StartCode.GDtabuadas_95doObjects3.length = 0;


if (gdjs.StartCode.forEachIndex3 < gdjs.StartCode.forEachCount0_3) {
    gdjs.StartCode.GDpanelObjects3.push(gdjs.StartCode.forEachObjects3[gdjs.StartCode.forEachIndex3]);
}
else if (gdjs.StartCode.forEachIndex3 < gdjs.StartCode.forEachCount0_3+gdjs.StartCode.forEachCount1_3) {
    gdjs.StartCode.GDfaseObjects3.push(gdjs.StartCode.forEachObjects3[gdjs.StartCode.forEachIndex3]);
}
else if (gdjs.StartCode.forEachIndex3 < gdjs.StartCode.forEachCount0_3+gdjs.StartCode.forEachCount1_3+gdjs.StartCode.forEachCount2_3) {
    gdjs.StartCode.GDdo_95dinoObjects3.push(gdjs.StartCode.forEachObjects3[gdjs.StartCode.forEachIndex3]);
}
else if (gdjs.StartCode.forEachIndex3 < gdjs.StartCode.forEachCount0_3+gdjs.StartCode.forEachCount1_3+gdjs.StartCode.forEachCount2_3+gdjs.StartCode.forEachCount3_3) {
    gdjs.StartCode.GDtabuadas_95doObjects3.push(gdjs.StartCode.forEachObjects3[gdjs.StartCode.forEachIndex3]);
}
else if (gdjs.StartCode.forEachIndex3 < gdjs.StartCode.forEachCount0_3+gdjs.StartCode.forEachCount1_3+gdjs.StartCode.forEachCount2_3+gdjs.StartCode.forEachCount3_3+gdjs.StartCode.forEachCount4_3) {
    gdjs.StartCode.GDstarsObjects3.push(gdjs.StartCode.forEachObjects3[gdjs.StartCode.forEachIndex3]);
}
else if (gdjs.StartCode.forEachIndex3 < gdjs.StartCode.forEachCount0_3+gdjs.StartCode.forEachCount1_3+gdjs.StartCode.forEachCount2_3+gdjs.StartCode.forEachCount3_3+gdjs.StartCode.forEachCount4_3+gdjs.StartCode.forEachCount5_3) {
    gdjs.StartCode.GDplayObjects3.push(gdjs.StartCode.forEachObjects3[gdjs.StartCode.forEachIndex3]);
}
if (true) {
{runtimeScene.getVariables().get("px").setNumber((( gdjs.StartCode.GDplayObjects3.length === 0 ) ? (( gdjs.StartCode.GDstarsObjects3.length === 0 ) ? (( gdjs.StartCode.GDtabuadas_95doObjects3.length === 0 ) ? (( gdjs.StartCode.GDdo_95dinoObjects3.length === 0 ) ? (( gdjs.StartCode.GDfaseObjects3.length === 0 ) ? (( gdjs.StartCode.GDpanelObjects3.length === 0 ) ? 0 :gdjs.StartCode.GDpanelObjects3[0].getX()) :gdjs.StartCode.GDfaseObjects3[0].getX()) :gdjs.StartCode.GDdo_95dinoObjects3[0].getX()) :gdjs.StartCode.GDtabuadas_95doObjects3[0].getX()) :gdjs.StartCode.GDstarsObjects3[0].getX()) :gdjs.StartCode.GDplayObjects3[0].getX()));
}{runtimeScene.getVariables().get("py").setNumber((( gdjs.StartCode.GDplayObjects3.length === 0 ) ? (( gdjs.StartCode.GDstarsObjects3.length === 0 ) ? (( gdjs.StartCode.GDtabuadas_95doObjects3.length === 0 ) ? (( gdjs.StartCode.GDdo_95dinoObjects3.length === 0 ) ? (( gdjs.StartCode.GDfaseObjects3.length === 0 ) ? (( gdjs.StartCode.GDpanelObjects3.length === 0 ) ? 0 :gdjs.StartCode.GDpanelObjects3[0].getY()) :gdjs.StartCode.GDfaseObjects3[0].getY()) :gdjs.StartCode.GDdo_95dinoObjects3[0].getY()) :gdjs.StartCode.GDtabuadas_95doObjects3[0].getY()) :gdjs.StartCode.GDstarsObjects3[0].getY()) :gdjs.StartCode.GDplayObjects3[0].getY()));
}{for(var i = 0, len = gdjs.StartCode.GDpanelObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDpanelObjects3[i].setY(-(500));
}
for(var i = 0, len = gdjs.StartCode.GDfaseObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDfaseObjects3[i].setY(-(500));
}
for(var i = 0, len = gdjs.StartCode.GDdo_95dinoObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDdo_95dinoObjects3[i].setY(-(500));
}
for(var i = 0, len = gdjs.StartCode.GDtabuadas_95doObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDtabuadas_95doObjects3[i].setY(-(500));
}
for(var i = 0, len = gdjs.StartCode.GDstarsObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDstarsObjects3[i].setY(-(500));
}
for(var i = 0, len = gdjs.StartCode.GDplayObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDplayObjects3[i].setY(-(500));
}
}{for(var i = 0, len = gdjs.StartCode.GDpanelObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDpanelObjects3[i].getBehavior("Tween").addObjectPositionTween("input", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("py")), "swingFromTo", 1500, false);
}
for(var i = 0, len = gdjs.StartCode.GDfaseObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDfaseObjects3[i].getBehavior("Tween").addObjectPositionTween("input", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("py")), "swingFromTo", 1500, false);
}
for(var i = 0, len = gdjs.StartCode.GDdo_95dinoObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDdo_95dinoObjects3[i].getBehavior("Tween").addObjectPositionTween("input", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("py")), "swingFromTo", 1500, false);
}
for(var i = 0, len = gdjs.StartCode.GDtabuadas_95doObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDtabuadas_95doObjects3[i].getBehavior("Tween").addObjectPositionTween("input", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("py")), "swingFromTo", 1500, false);
}
for(var i = 0, len = gdjs.StartCode.GDstarsObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDstarsObjects3[i].getBehavior("Tween").addObjectPositionTween("input", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("py")), "swingFromTo", 1500, false);
}
for(var i = 0, len = gdjs.StartCode.GDplayObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDplayObjects3[i].getBehavior("Tween").addObjectPositionTween("input", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("py")), "swingFromTo", 1500, false);
}
}}
}

}


{

gdjs.StartCode.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));

gdjs.StartCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDfaseObjects1.length;i<l;++i) {
    if ( gdjs.StartCode.GDfaseObjects1[i].getVariableNumber(gdjs.StartCode.GDfaseObjects1[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDfaseObjects1[k] = gdjs.StartCode.GDfaseObjects1[i];
        ++k;
    }
}
gdjs.StartCode.GDfaseObjects1.length = k;}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDfaseObjects1 */
{for(var i = 0, len = gdjs.StartCode.GDfaseObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDfaseObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.StartCode.GDfaseObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDfaseObjects1[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.StartCode.GDfaseObjects1[i].getVariables().getFromIndex(0))));
}
}}

}


}; //End of gdjs.StartCode.eventsList0x903ffc
gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDfaseObjects1Objects = Hashtable.newFrom({"fase": gdjs.StartCode.GDfaseObjects1});gdjs.StartCode.eventsList0x905dbc = function(runtimeScene) {

}; //End of gdjs.StartCode.eventsList0x905dbc
gdjs.StartCode.eventsList0x905b6c = function(runtimeScene) {

{

gdjs.StartCode.GDdo_95dinoObjects1.createFrom(runtimeScene.getObjects("do_dino"));
gdjs.StartCode.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));
gdjs.StartCode.GDpanelObjects1.createFrom(runtimeScene.getObjects("panel"));
gdjs.StartCode.GDplayObjects1.createFrom(runtimeScene.getObjects("play"));
gdjs.StartCode.GDstarsObjects1.createFrom(runtimeScene.getObjects("stars"));
gdjs.StartCode.GDtabuadas_95doObjects1.createFrom(runtimeScene.getObjects("tabuadas_do"));

gdjs.StartCode.forEachTotalCount2 = 0;
gdjs.StartCode.forEachObjects2.length = 0;
gdjs.StartCode.forEachCount0_2 = gdjs.StartCode.GDpanelObjects1.length;
gdjs.StartCode.forEachTotalCount2 += gdjs.StartCode.forEachCount0_2;
gdjs.StartCode.forEachObjects2.push.apply(gdjs.StartCode.forEachObjects2,gdjs.StartCode.GDpanelObjects1);
gdjs.StartCode.forEachCount1_2 = gdjs.StartCode.GDfaseObjects1.length;
gdjs.StartCode.forEachTotalCount2 += gdjs.StartCode.forEachCount1_2;
gdjs.StartCode.forEachObjects2.push.apply(gdjs.StartCode.forEachObjects2,gdjs.StartCode.GDfaseObjects1);
gdjs.StartCode.forEachCount2_2 = gdjs.StartCode.GDdo_95dinoObjects1.length;
gdjs.StartCode.forEachTotalCount2 += gdjs.StartCode.forEachCount2_2;
gdjs.StartCode.forEachObjects2.push.apply(gdjs.StartCode.forEachObjects2,gdjs.StartCode.GDdo_95dinoObjects1);
gdjs.StartCode.forEachCount3_2 = gdjs.StartCode.GDtabuadas_95doObjects1.length;
gdjs.StartCode.forEachTotalCount2 += gdjs.StartCode.forEachCount3_2;
gdjs.StartCode.forEachObjects2.push.apply(gdjs.StartCode.forEachObjects2,gdjs.StartCode.GDtabuadas_95doObjects1);
gdjs.StartCode.forEachCount4_2 = gdjs.StartCode.GDstarsObjects1.length;
gdjs.StartCode.forEachTotalCount2 += gdjs.StartCode.forEachCount4_2;
gdjs.StartCode.forEachObjects2.push.apply(gdjs.StartCode.forEachObjects2,gdjs.StartCode.GDstarsObjects1);
gdjs.StartCode.forEachCount5_2 = gdjs.StartCode.GDplayObjects1.length;
gdjs.StartCode.forEachTotalCount2 += gdjs.StartCode.forEachCount5_2;
gdjs.StartCode.forEachObjects2.push.apply(gdjs.StartCode.forEachObjects2,gdjs.StartCode.GDplayObjects1);
for(gdjs.StartCode.forEachIndex2 = 0;gdjs.StartCode.forEachIndex2 < gdjs.StartCode.forEachTotalCount2;++gdjs.StartCode.forEachIndex2) {
gdjs.StartCode.GDdo_95dinoObjects2.length = 0;

gdjs.StartCode.GDfaseObjects2.length = 0;

gdjs.StartCode.GDpanelObjects2.length = 0;

gdjs.StartCode.GDplayObjects2.length = 0;

gdjs.StartCode.GDstarsObjects2.length = 0;

gdjs.StartCode.GDtabuadas_95doObjects2.length = 0;


if (gdjs.StartCode.forEachIndex2 < gdjs.StartCode.forEachCount0_2) {
    gdjs.StartCode.GDpanelObjects2.push(gdjs.StartCode.forEachObjects2[gdjs.StartCode.forEachIndex2]);
}
else if (gdjs.StartCode.forEachIndex2 < gdjs.StartCode.forEachCount0_2+gdjs.StartCode.forEachCount1_2) {
    gdjs.StartCode.GDfaseObjects2.push(gdjs.StartCode.forEachObjects2[gdjs.StartCode.forEachIndex2]);
}
else if (gdjs.StartCode.forEachIndex2 < gdjs.StartCode.forEachCount0_2+gdjs.StartCode.forEachCount1_2+gdjs.StartCode.forEachCount2_2) {
    gdjs.StartCode.GDdo_95dinoObjects2.push(gdjs.StartCode.forEachObjects2[gdjs.StartCode.forEachIndex2]);
}
else if (gdjs.StartCode.forEachIndex2 < gdjs.StartCode.forEachCount0_2+gdjs.StartCode.forEachCount1_2+gdjs.StartCode.forEachCount2_2+gdjs.StartCode.forEachCount3_2) {
    gdjs.StartCode.GDtabuadas_95doObjects2.push(gdjs.StartCode.forEachObjects2[gdjs.StartCode.forEachIndex2]);
}
else if (gdjs.StartCode.forEachIndex2 < gdjs.StartCode.forEachCount0_2+gdjs.StartCode.forEachCount1_2+gdjs.StartCode.forEachCount2_2+gdjs.StartCode.forEachCount3_2+gdjs.StartCode.forEachCount4_2) {
    gdjs.StartCode.GDstarsObjects2.push(gdjs.StartCode.forEachObjects2[gdjs.StartCode.forEachIndex2]);
}
else if (gdjs.StartCode.forEachIndex2 < gdjs.StartCode.forEachCount0_2+gdjs.StartCode.forEachCount1_2+gdjs.StartCode.forEachCount2_2+gdjs.StartCode.forEachCount3_2+gdjs.StartCode.forEachCount4_2+gdjs.StartCode.forEachCount5_2) {
    gdjs.StartCode.GDplayObjects2.push(gdjs.StartCode.forEachObjects2[gdjs.StartCode.forEachIndex2]);
}
if (true) {
{runtimeScene.getVariables().get("px").setNumber((( gdjs.StartCode.GDplayObjects2.length === 0 ) ? (( gdjs.StartCode.GDstarsObjects2.length === 0 ) ? (( gdjs.StartCode.GDtabuadas_95doObjects2.length === 0 ) ? (( gdjs.StartCode.GDdo_95dinoObjects2.length === 0 ) ? (( gdjs.StartCode.GDfaseObjects2.length === 0 ) ? (( gdjs.StartCode.GDpanelObjects2.length === 0 ) ? 0 :gdjs.StartCode.GDpanelObjects2[0].getX()) :gdjs.StartCode.GDfaseObjects2[0].getX()) :gdjs.StartCode.GDdo_95dinoObjects2[0].getX()) :gdjs.StartCode.GDtabuadas_95doObjects2[0].getX()) :gdjs.StartCode.GDstarsObjects2[0].getX()) :gdjs.StartCode.GDplayObjects2[0].getX()));
}{for(var i = 0, len = gdjs.StartCode.GDpanelObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDpanelObjects2[i].getBehavior("Tween").addObjectPositionTween("start", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), -(500), "swingFromTo", 1500, false);
}
for(var i = 0, len = gdjs.StartCode.GDfaseObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDfaseObjects2[i].getBehavior("Tween").addObjectPositionTween("start", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), -(500), "swingFromTo", 1500, false);
}
for(var i = 0, len = gdjs.StartCode.GDdo_95dinoObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDdo_95dinoObjects2[i].getBehavior("Tween").addObjectPositionTween("start", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), -(500), "swingFromTo", 1500, false);
}
for(var i = 0, len = gdjs.StartCode.GDtabuadas_95doObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDtabuadas_95doObjects2[i].getBehavior("Tween").addObjectPositionTween("start", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), -(500), "swingFromTo", 1500, false);
}
for(var i = 0, len = gdjs.StartCode.GDstarsObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDstarsObjects2[i].getBehavior("Tween").addObjectPositionTween("start", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), -(500), "swingFromTo", 1500, false);
}
for(var i = 0, len = gdjs.StartCode.GDplayObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayObjects2[i].getBehavior("Tween").addObjectPositionTween("start", gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("px")), -(500), "swingFromTo", 1500, false);
}
}}
}

}


}; //End of gdjs.StartCode.eventsList0x905b6c
gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDplayObjects1Objects = Hashtable.newFrom({"play": gdjs.StartCode.GDplayObjects1});gdjs.StartCode.eventsList0x5b6e18 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "tabuada");
}{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "tabuada");
}
{ //Subevents
gdjs.StartCode.eventsList0x903ffc(runtimeScene);} //End of subevents
}

}


{



}


{


{
gdjs.StartCode.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
{for(var i = 0, len = gdjs.StartCode.GDdinoObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDdinoObjects1[i].addForce(200, 0, 0);
}
}}

}


{

gdjs.StartCode.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.StartCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.StartCode.GDdinoObjects1[i].getX() > gdjs.evtTools.window.getWindowInnerHeight() + 500 ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDdinoObjects1[k] = gdjs.StartCode.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.StartCode.GDdinoObjects1.length = k;}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDdinoObjects1 */
{for(var i = 0, len = gdjs.StartCode.GDdinoObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDdinoObjects1[i].setX(-(300));
}
}}

}


{



}


{

gdjs.StartCode.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDfaseObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDfaseObjects1.length;i<l;++i) {
    if ( gdjs.StartCode.GDfaseObjects1[i].getAnimation() > 0 ) {
        gdjs.StartCode.condition1IsTrue_0.val = true;
        gdjs.StartCode.GDfaseObjects1[k] = gdjs.StartCode.GDfaseObjects1[i];
        ++k;
    }
}
gdjs.StartCode.GDfaseObjects1.length = k;}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
gdjs.StartCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDfaseObjects1 */
{runtimeScene.getVariables().getFromIndex(0).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.StartCode.GDfaseObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.StartCode.GDfaseObjects1[0].getVariables()).getFromIndex(0))));
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(0);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "tabuada");
}
{ //Subevents
gdjs.StartCode.eventsList0x905b6c(runtimeScene);} //End of subevents
}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 1.5, "tabuada");
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Tabuada", false);
}{gdjs.evtTools.runtimeScene.removeTimer(runtimeScene, "tabuada");
}}

}


{



}


{

gdjs.StartCode.GDplayObjects1.createFrom(runtimeScene.getObjects("play"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDplayObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)));
}}

}


}; //End of gdjs.StartCode.eventsList0x5b6e18


gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.StartCode.GDdinoObjects1.length = 0;
gdjs.StartCode.GDdinoObjects2.length = 0;
gdjs.StartCode.GDdinoObjects3.length = 0;
gdjs.StartCode.GDplayerObjects1.length = 0;
gdjs.StartCode.GDplayerObjects2.length = 0;
gdjs.StartCode.GDplayerObjects3.length = 0;
gdjs.StartCode.GDground1Objects1.length = 0;
gdjs.StartCode.GDground1Objects2.length = 0;
gdjs.StartCode.GDground1Objects3.length = 0;
gdjs.StartCode.GDwaterObjects1.length = 0;
gdjs.StartCode.GDwaterObjects2.length = 0;
gdjs.StartCode.GDwaterObjects3.length = 0;
gdjs.StartCode.GDfade_95faseObjects1.length = 0;
gdjs.StartCode.GDfade_95faseObjects2.length = 0;
gdjs.StartCode.GDfade_95faseObjects3.length = 0;
gdjs.StartCode.GDlblFaseObjects1.length = 0;
gdjs.StartCode.GDlblFaseObjects2.length = 0;
gdjs.StartCode.GDlblFaseObjects3.length = 0;
gdjs.StartCode.GDpanelObjects1.length = 0;
gdjs.StartCode.GDpanelObjects2.length = 0;
gdjs.StartCode.GDpanelObjects3.length = 0;
gdjs.StartCode.GDfaseObjects1.length = 0;
gdjs.StartCode.GDfaseObjects2.length = 0;
gdjs.StartCode.GDfaseObjects3.length = 0;
gdjs.StartCode.GDdo_95dinoObjects1.length = 0;
gdjs.StartCode.GDdo_95dinoObjects2.length = 0;
gdjs.StartCode.GDdo_95dinoObjects3.length = 0;
gdjs.StartCode.GDtabuadas_95doObjects1.length = 0;
gdjs.StartCode.GDtabuadas_95doObjects2.length = 0;
gdjs.StartCode.GDtabuadas_95doObjects3.length = 0;
gdjs.StartCode.GDstarsObjects1.length = 0;
gdjs.StartCode.GDstarsObjects2.length = 0;
gdjs.StartCode.GDstarsObjects3.length = 0;
gdjs.StartCode.GDplayObjects1.length = 0;
gdjs.StartCode.GDplayObjects2.length = 0;
gdjs.StartCode.GDplayObjects3.length = 0;

gdjs.StartCode.eventsList0x5b6e18(runtimeScene);
return;

}

gdjs['StartCode'] = gdjs.StartCode;
