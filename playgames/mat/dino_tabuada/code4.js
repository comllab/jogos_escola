gdjs.Fase3Code = {};
gdjs.Fase3Code.GDdinoObjects1= [];
gdjs.Fase3Code.GDdinoObjects2= [];
gdjs.Fase3Code.GDdinoObjects3= [];
gdjs.Fase3Code.GDplayerObjects1= [];
gdjs.Fase3Code.GDplayerObjects2= [];
gdjs.Fase3Code.GDplayerObjects3= [];
gdjs.Fase3Code.GDground1Objects1= [];
gdjs.Fase3Code.GDground1Objects2= [];
gdjs.Fase3Code.GDground1Objects3= [];
gdjs.Fase3Code.GDwaterObjects1= [];
gdjs.Fase3Code.GDwaterObjects2= [];
gdjs.Fase3Code.GDwaterObjects3= [];
gdjs.Fase3Code.GDfade_95faseObjects1= [];
gdjs.Fase3Code.GDfade_95faseObjects2= [];
gdjs.Fase3Code.GDfade_95faseObjects3= [];
gdjs.Fase3Code.GDlblFaseObjects1= [];
gdjs.Fase3Code.GDlblFaseObjects2= [];
gdjs.Fase3Code.GDlblFaseObjects3= [];
gdjs.Fase3Code.GDbg1Objects1= [];
gdjs.Fase3Code.GDbg1Objects2= [];
gdjs.Fase3Code.GDbg1Objects3= [];
gdjs.Fase3Code.GDboxObjects1= [];
gdjs.Fase3Code.GDboxObjects2= [];
gdjs.Fase3Code.GDboxObjects3= [];
gdjs.Fase3Code.GDplacaObjects1= [];
gdjs.Fase3Code.GDplacaObjects2= [];
gdjs.Fase3Code.GDplacaObjects3= [];
gdjs.Fase3Code.GDthreeObjects1= [];
gdjs.Fase3Code.GDthreeObjects2= [];
gdjs.Fase3Code.GDthreeObjects3= [];
gdjs.Fase3Code.GDenemy1Objects1= [];
gdjs.Fase3Code.GDenemy1Objects2= [];
gdjs.Fase3Code.GDenemy1Objects3= [];
gdjs.Fase3Code.GDflipEnemyObjects1= [];
gdjs.Fase3Code.GDflipEnemyObjects2= [];
gdjs.Fase3Code.GDflipEnemyObjects3= [];
gdjs.Fase3Code.GDground1bordasObjects1= [];
gdjs.Fase3Code.GDground1bordasObjects2= [];
gdjs.Fase3Code.GDground1bordasObjects3= [];
gdjs.Fase3Code.GDbigGroundObjects1= [];
gdjs.Fase3Code.GDbigGroundObjects2= [];
gdjs.Fase3Code.GDbigGroundObjects3= [];
gdjs.Fase3Code.GDplataforma1Objects1= [];
gdjs.Fase3Code.GDplataforma1Objects2= [];
gdjs.Fase3Code.GDplataforma1Objects3= [];
gdjs.Fase3Code.GDendObjects1= [];
gdjs.Fase3Code.GDendObjects2= [];
gdjs.Fase3Code.GDendObjects3= [];

gdjs.Fase3Code.conditionTrue_0 = {val:false};
gdjs.Fase3Code.condition0IsTrue_0 = {val:false};
gdjs.Fase3Code.condition1IsTrue_0 = {val:false};
gdjs.Fase3Code.condition2IsTrue_0 = {val:false};
gdjs.Fase3Code.condition3IsTrue_0 = {val:false};
gdjs.Fase3Code.condition4IsTrue_0 = {val:false};
gdjs.Fase3Code.condition5IsTrue_0 = {val:false};
gdjs.Fase3Code.conditionTrue_1 = {val:false};
gdjs.Fase3Code.condition0IsTrue_1 = {val:false};
gdjs.Fase3Code.condition1IsTrue_1 = {val:false};
gdjs.Fase3Code.condition2IsTrue_1 = {val:false};
gdjs.Fase3Code.condition3IsTrue_1 = {val:false};
gdjs.Fase3Code.condition4IsTrue_1 = {val:false};
gdjs.Fase3Code.condition5IsTrue_1 = {val:false};


gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDwaterObjects1Objects = Hashtable.newFrom({"water": gdjs.Fase3Code.GDwaterObjects1});gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase3Code.GDplayerObjects1});gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDendObjects1Objects = Hashtable.newFrom({"end": gdjs.Fase3Code.GDendObjects1});gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase3Code.GDenemy1Objects1});gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase3Code.GDplayerObjects1});gdjs.Fase3Code.eventsList0x879864 = function(runtimeScene) {

{

gdjs.Fase3Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase3Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
gdjs.Fase3Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase3Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDdinoObjects2[k] = gdjs.Fase3Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase3Code.GDdinoObjects2.length = k;}if ( gdjs.Fase3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.Fase3Code.condition1IsTrue_0.val = true;
        gdjs.Fase3Code.GDplayerObjects2[k] = gdjs.Fase3Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase3Code.GDplayerObjects2.length = k;}}
if (gdjs.Fase3Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase3Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase3Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase3Code.GDdinoObjects2[i].setAnimationName("jump");
}
}}

}


{

gdjs.Fase3Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase3Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
gdjs.Fase3Code.condition1IsTrue_0.val = false;
gdjs.Fase3Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase3Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDdinoObjects2[k] = gdjs.Fase3Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase3Code.GDdinoObjects2.length = k;}if ( gdjs.Fase3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving() ) {
        gdjs.Fase3Code.condition1IsTrue_0.val = true;
        gdjs.Fase3Code.GDplayerObjects2[k] = gdjs.Fase3Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase3Code.GDplayerObjects2.length = k;}if ( gdjs.Fase3Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase3Code.condition2IsTrue_0.val = true;
        gdjs.Fase3Code.GDplayerObjects2[k] = gdjs.Fase3Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase3Code.GDplayerObjects2.length = k;}}
}
if (gdjs.Fase3Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Fase3Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase3Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase3Code.GDdinoObjects2[i].setAnimationName("run");
}
}}

}


{

gdjs.Fase3Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase3Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
gdjs.Fase3Code.condition1IsTrue_0.val = false;
gdjs.Fase3Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase3Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDdinoObjects2[k] = gdjs.Fase3Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase3Code.GDdinoObjects2.length = k;}if ( gdjs.Fase3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDplayerObjects2.length;i<l;++i) {
    if ( !(gdjs.Fase3Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Fase3Code.condition1IsTrue_0.val = true;
        gdjs.Fase3Code.GDplayerObjects2[k] = gdjs.Fase3Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase3Code.GDplayerObjects2.length = k;}if ( gdjs.Fase3Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase3Code.condition2IsTrue_0.val = true;
        gdjs.Fase3Code.GDplayerObjects2[k] = gdjs.Fase3Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase3Code.GDplayerObjects2.length = k;}}
}
if (gdjs.Fase3Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Fase3Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase3Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase3Code.GDdinoObjects2[i].setAnimationName("idle");
}
}}

}


{

gdjs.Fase3Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
gdjs.Fase3Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase3Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isJumping()) ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDplayerObjects1[k] = gdjs.Fase3Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDplayerObjects1.length = k;}if ( gdjs.Fase3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase3Code.condition1IsTrue_0.val = true;
        gdjs.Fase3Code.GDplayerObjects1[k] = gdjs.Fase3Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase3Code.condition1IsTrue_0.val) {
gdjs.Fase3Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
{for(var i = 0, len = gdjs.Fase3Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDdinoObjects1[i].setAnimationName("falling");
}
}}

}


}; //End of gdjs.Fase3Code.eventsList0x879864
gdjs.Fase3Code.eventsList0x88c614 = function(runtimeScene) {

{


gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
gdjs.Fase3Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if (gdjs.Fase3Code.condition0IsTrue_0.val) {
gdjs.Fase3Code.GDdinoObjects2.createFrom(gdjs.Fase3Code.GDdinoObjects1);

{for(var i = 0, len = gdjs.Fase3Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase3Code.GDdinoObjects2[i].flipX(true);
}
}}

}


{


gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
gdjs.Fase3Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if (gdjs.Fase3Code.condition0IsTrue_0.val) {
gdjs.Fase3Code.GDdinoObjects2.createFrom(gdjs.Fase3Code.GDdinoObjects1);

{for(var i = 0, len = gdjs.Fase3Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase3Code.GDdinoObjects2[i].flipX(false);
}
}}

}


{

gdjs.Fase3Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
gdjs.Fase3Code.condition1IsTrue_0.val = false;
{
gdjs.Fase3Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.Fase3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase3Code.condition1IsTrue_0.val = true;
        gdjs.Fase3Code.GDplayerObjects1[k] = gdjs.Fase3Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase3Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase3Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase3Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDplayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


}; //End of gdjs.Fase3Code.eventsList0x88c614
gdjs.Fase3Code.eventsList0x7c40a4 = function(runtimeScene) {

{

gdjs.Fase3Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDdinoObjects1[i].getVariableNumber(gdjs.Fase3Code.GDdinoObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDdinoObjects1[k] = gdjs.Fase3Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDdinoObjects1.length = k;}if (gdjs.Fase3Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.Fase3Code.eventsList0x88c614(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.Fase3Code.eventsList0x7c40a4
gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase3Code.GDenemy1Objects1});gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDflipEnemyObjects1Objects = Hashtable.newFrom({"flipEnemy": gdjs.Fase3Code.GDflipEnemyObjects1});gdjs.Fase3Code.eventsList0x92d90c = function(runtimeScene) {

{

gdjs.Fase3Code.GDenemy1Objects2.createFrom(gdjs.Fase3Code.GDenemy1Objects1);


gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDenemy1Objects2.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDenemy1Objects2[i].getVariableNumber(gdjs.Fase3Code.GDenemy1Objects2[i].getVariables().getFromIndex(0)) < 0 ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDenemy1Objects2[k] = gdjs.Fase3Code.GDenemy1Objects2[i];
        ++k;
    }
}
gdjs.Fase3Code.GDenemy1Objects2.length = k;}if (gdjs.Fase3Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase3Code.GDenemy1Objects2 */
{for(var i = 0, len = gdjs.Fase3Code.GDenemy1Objects2.length ;i < len;++i) {
    gdjs.Fase3Code.GDenemy1Objects2[i].flipX(false);
}
}}

}


{

/* Reuse gdjs.Fase3Code.GDenemy1Objects1 */

gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase3Code.GDenemy1Objects1[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDenemy1Objects1[k] = gdjs.Fase3Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDenemy1Objects1.length = k;}if (gdjs.Fase3Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase3Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase3Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDenemy1Objects1[i].flipX(true);
}
}}

}


}; //End of gdjs.Fase3Code.eventsList0x92d90c
gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase3Code.GDplayerObjects1});gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase3Code.GDenemy1Objects1});gdjs.Fase3Code.eventsList0x5b6e18 = function(runtimeScene) {

{



}


{


gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
gdjs.Fase3Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase3Code.condition0IsTrue_0.val) {
gdjs.Fase3Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));
gdjs.Fase3Code.GDlblFaseObjects1.createFrom(runtimeScene.getObjects("lblFase"));
{for(var i = 0, len = gdjs.Fase3Code.GDlblFaseObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDlblFaseObjects1[i].setString("FASE " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{gdjs.evtTools.camera.showLayer(runtimeScene, "UI");
}{for(var i = 0, len = gdjs.Fase3Code.GDfade_95faseObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDfade_95faseObjects1[i].getBehavior("Tween").addObjectOpacityTween("fadeout", 0, "linear", 2000, false);
}
}}

}


{

gdjs.Fase3Code.GDwaterObjects1.createFrom(runtimeScene.getObjects("water"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
gdjs.Fase3Code.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDwaterObjects1Objects) > 0;
}if (gdjs.Fase3Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase3Code.GDwaterObjects1 */
{for(var i = 0, len = gdjs.Fase3Code.GDwaterObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDwaterObjects1[i].setXOffset(gdjs.Fase3Code.GDwaterObjects1[i].getXOffset() + (0.5));
}
}}

}


{

gdjs.Fase3Code.GDendObjects1.createFrom(runtimeScene.getObjects("end"));
gdjs.Fase3Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
gdjs.Fase3Code.condition1IsTrue_0.val = false;
{
gdjs.Fase3Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDplayerObjects1Objects, gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDendObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase3Code.condition0IsTrue_0.val ) {
{
gdjs.Fase3Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 0;
}}
if (gdjs.Fase3Code.condition1IsTrue_0.val) {
gdjs.Fase3Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}{for(var i = 0, len = gdjs.Fase3Code.GDfade_95faseObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDfade_95faseObjects1[i].getBehavior("Tween").addObjectOpacityTween("fadein", 255, "linear", 2000, false);
}
}}

}


{

gdjs.Fase3Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
gdjs.Fase3Code.condition1IsTrue_0.val = false;
{
gdjs.Fase3Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if ( gdjs.Fase3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDfade_95faseObjects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDfade_95faseObjects1[i].getOpacity() >= 255 ) {
        gdjs.Fase3Code.condition1IsTrue_0.val = true;
        gdjs.Fase3Code.GDfade_95faseObjects1[k] = gdjs.Fase3Code.GDfade_95faseObjects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDfade_95faseObjects1.length = k;}}
if (gdjs.Fase3Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(2);
}}

}


{


gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
gdjs.Fase3Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 2;
}if (gdjs.Fase3Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


{



}


{


gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
gdjs.Fase3Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase3Code.condition0IsTrue_0.val) {
gdjs.Fase3Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.Fase3Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDplayerObjects1[i].hide();
}
}}

}


{



}


{


{
gdjs.Fase3Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase3Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.Fase3Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDdinoObjects1[i].setPosition((( gdjs.Fase3Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase3Code.GDplayerObjects1[0].getPointX("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase3Code.GDdinoObjects1[i].getVariables().getFromIndex(0))),(( gdjs.Fase3Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase3Code.GDplayerObjects1[0].getPointY("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase3Code.GDdinoObjects1[i].getVariables().getFromIndex(1))));
}
}{gdjs.evtTools.camera.centerCameraWithinLimits(runtimeScene, (gdjs.Fase3Code.GDdinoObjects1.length !== 0 ? gdjs.Fase3Code.GDdinoObjects1[0] : null), 0, 0, gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)), 768, true, "", 0);
}{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.Fase3Code.GDdinoObjects1.length === 0 ) ? 0 :gdjs.Fase3Code.GDdinoObjects1[0].getPointX("")) * 0.15, "Background", 0);
}}

}


{



}


{



}


{

gdjs.Fase3Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase3Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase3Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
gdjs.Fase3Code.condition1IsTrue_0.val = false;
gdjs.Fase3Code.condition2IsTrue_0.val = false;
gdjs.Fase3Code.condition3IsTrue_0.val = false;
gdjs.Fase3Code.condition4IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase3Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling()) ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDplayerObjects1[k] = gdjs.Fase3Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDplayerObjects1.length = k;}if ( gdjs.Fase3Code.condition0IsTrue_0.val ) {
{
gdjs.Fase3Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDenemy1Objects1Objects, gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDplayerObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase3Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase3Code.GDenemy1Objects1[i].getVariables().get("die")) == 0 ) {
        gdjs.Fase3Code.condition2IsTrue_0.val = true;
        gdjs.Fase3Code.GDenemy1Objects1[k] = gdjs.Fase3Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDenemy1Objects1.length = k;}if ( gdjs.Fase3Code.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDdinoObjects1[i].getVariableNumber(gdjs.Fase3Code.GDdinoObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase3Code.condition3IsTrue_0.val = true;
        gdjs.Fase3Code.GDdinoObjects1[k] = gdjs.Fase3Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDdinoObjects1.length = k;}if ( gdjs.Fase3Code.condition3IsTrue_0.val ) {
{
{gdjs.Fase3Code.conditionTrue_1 = gdjs.Fase3Code.condition4IsTrue_0;
gdjs.Fase3Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8850940);
}
}}
}
}
}
if (gdjs.Fase3Code.condition4IsTrue_0.val) {
/* Reuse gdjs.Fase3Code.GDdinoObjects1 */
/* Reuse gdjs.Fase3Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase3Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDplayerObjects1[i].getBehavior("PlatformerObject").ignoreDefaultControls(true);
}
}{for(var i = 0, len = gdjs.Fase3Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDdinoObjects1[i].returnVariable(gdjs.Fase3Code.GDdinoObjects1[i].getVariables().getFromIndex(2)).setNumber(1);
}
}{for(var i = 0, len = gdjs.Fase3Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDdinoObjects1[i].setAnimationName("die");
}
}}

}


{



}


{

gdjs.Fase3Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
gdjs.Fase3Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDdinoObjects1[i].isCurrentAnimationName("die") ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDdinoObjects1[k] = gdjs.Fase3Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDdinoObjects1.length = k;}if ( gdjs.Fase3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDdinoObjects1[i].hasAnimationEnded() ) {
        gdjs.Fase3Code.condition1IsTrue_0.val = true;
        gdjs.Fase3Code.GDdinoObjects1[k] = gdjs.Fase3Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDdinoObjects1.length = k;}}
if (gdjs.Fase3Code.condition1IsTrue_0.val) {
{gdjs.evtsExt__Util__setFase.func(runtimeScene, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.Fase3Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDdinoObjects1[i].getY() > gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDdinoObjects1[k] = gdjs.Fase3Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDdinoObjects1.length = k;}if (gdjs.Fase3Code.condition0IsTrue_0.val) {
{gdjs.evtsExt__Util__setFase.func(runtimeScene, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Fase3Code.eventsList0x879864(runtimeScene);
}


{


gdjs.Fase3Code.eventsList0x7c40a4(runtimeScene);
}


{



}


{


gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
gdjs.Fase3Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase3Code.condition0IsTrue_0.val) {
gdjs.Fase3Code.GDflipEnemyObjects1.createFrom(runtimeScene.getObjects("flipEnemy"));
{for(var i = 0, len = gdjs.Fase3Code.GDflipEnemyObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDflipEnemyObjects1[i].hide();
}
}}

}


{



}


{


{
gdjs.Fase3Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
{for(var i = 0, len = gdjs.Fase3Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDenemy1Objects1[i].setX(gdjs.Fase3Code.GDenemy1Objects1[i].getX() + ((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase3Code.GDenemy1Objects1[i].getVariables().getFromIndex(0))) * gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{



}


{

gdjs.Fase3Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase3Code.GDflipEnemyObjects1.createFrom(runtimeScene.getObjects("flipEnemy"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
gdjs.Fase3Code.condition1IsTrue_0.val = false;
{
gdjs.Fase3Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDenemy1Objects1Objects, gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDflipEnemyObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase3Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase3Code.conditionTrue_1 = gdjs.Fase3Code.condition1IsTrue_0;
gdjs.Fase3Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8404284);
}
}}
if (gdjs.Fase3Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase3Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase3Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDenemy1Objects1[i].returnVariable(gdjs.Fase3Code.GDenemy1Objects1[i].getVariables().getFromIndex(0)).mul(-(1));
}
}
{ //Subevents
gdjs.Fase3Code.eventsList0x92d90c(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.Fase3Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase3Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
gdjs.Fase3Code.condition1IsTrue_0.val = false;
gdjs.Fase3Code.condition2IsTrue_0.val = false;
gdjs.Fase3Code.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDplayerObjects1[k] = gdjs.Fase3Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDplayerObjects1.length = k;}if ( gdjs.Fase3Code.condition0IsTrue_0.val ) {
{
gdjs.Fase3Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDplayerObjects1Objects, gdjs.Fase3Code.mapOfGDgdjs_46Fase3Code_46GDenemy1Objects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase3Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase3Code.GDenemy1Objects1[i].getVariables().get("die")) == 0 ) {
        gdjs.Fase3Code.condition2IsTrue_0.val = true;
        gdjs.Fase3Code.GDenemy1Objects1[k] = gdjs.Fase3Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDenemy1Objects1.length = k;}if ( gdjs.Fase3Code.condition2IsTrue_0.val ) {
{
{gdjs.Fase3Code.conditionTrue_1 = gdjs.Fase3Code.condition3IsTrue_0;
gdjs.Fase3Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8887292);
}
}}
}
}
if (gdjs.Fase3Code.condition3IsTrue_0.val) {
/* Reuse gdjs.Fase3Code.GDenemy1Objects1 */
/* Reuse gdjs.Fase3Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase3Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDplayerObjects1[i].getBehavior("PlatformerObject").setCanJump();
}
}{for(var i = 0, len = gdjs.Fase3Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDplayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}{for(var i = 0, len = gdjs.Fase3Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDenemy1Objects1[i].returnVariable(gdjs.Fase3Code.GDenemy1Objects1[i].getVariables().get("die")).setNumber(1);
}
}{for(var i = 0, len = gdjs.Fase3Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDenemy1Objects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.Fase3Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDenemy1Objects1[i].getBehavior("Tween").addObjectOpacityTween("enemy1_die", 0, "linear", 1000, false);
}
}}

}


{

gdjs.Fase3Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));

gdjs.Fase3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase3Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase3Code.GDenemy1Objects1[i].getOpacity() <= 0 ) {
        gdjs.Fase3Code.condition0IsTrue_0.val = true;
        gdjs.Fase3Code.GDenemy1Objects1[k] = gdjs.Fase3Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase3Code.GDenemy1Objects1.length = k;}if (gdjs.Fase3Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase3Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase3Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase3Code.GDenemy1Objects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.Fase3Code.eventsList0x5b6e18


gdjs.Fase3Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Fase3Code.GDdinoObjects1.length = 0;
gdjs.Fase3Code.GDdinoObjects2.length = 0;
gdjs.Fase3Code.GDdinoObjects3.length = 0;
gdjs.Fase3Code.GDplayerObjects1.length = 0;
gdjs.Fase3Code.GDplayerObjects2.length = 0;
gdjs.Fase3Code.GDplayerObjects3.length = 0;
gdjs.Fase3Code.GDground1Objects1.length = 0;
gdjs.Fase3Code.GDground1Objects2.length = 0;
gdjs.Fase3Code.GDground1Objects3.length = 0;
gdjs.Fase3Code.GDwaterObjects1.length = 0;
gdjs.Fase3Code.GDwaterObjects2.length = 0;
gdjs.Fase3Code.GDwaterObjects3.length = 0;
gdjs.Fase3Code.GDfade_95faseObjects1.length = 0;
gdjs.Fase3Code.GDfade_95faseObjects2.length = 0;
gdjs.Fase3Code.GDfade_95faseObjects3.length = 0;
gdjs.Fase3Code.GDlblFaseObjects1.length = 0;
gdjs.Fase3Code.GDlblFaseObjects2.length = 0;
gdjs.Fase3Code.GDlblFaseObjects3.length = 0;
gdjs.Fase3Code.GDbg1Objects1.length = 0;
gdjs.Fase3Code.GDbg1Objects2.length = 0;
gdjs.Fase3Code.GDbg1Objects3.length = 0;
gdjs.Fase3Code.GDboxObjects1.length = 0;
gdjs.Fase3Code.GDboxObjects2.length = 0;
gdjs.Fase3Code.GDboxObjects3.length = 0;
gdjs.Fase3Code.GDplacaObjects1.length = 0;
gdjs.Fase3Code.GDplacaObjects2.length = 0;
gdjs.Fase3Code.GDplacaObjects3.length = 0;
gdjs.Fase3Code.GDthreeObjects1.length = 0;
gdjs.Fase3Code.GDthreeObjects2.length = 0;
gdjs.Fase3Code.GDthreeObjects3.length = 0;
gdjs.Fase3Code.GDenemy1Objects1.length = 0;
gdjs.Fase3Code.GDenemy1Objects2.length = 0;
gdjs.Fase3Code.GDenemy1Objects3.length = 0;
gdjs.Fase3Code.GDflipEnemyObjects1.length = 0;
gdjs.Fase3Code.GDflipEnemyObjects2.length = 0;
gdjs.Fase3Code.GDflipEnemyObjects3.length = 0;
gdjs.Fase3Code.GDground1bordasObjects1.length = 0;
gdjs.Fase3Code.GDground1bordasObjects2.length = 0;
gdjs.Fase3Code.GDground1bordasObjects3.length = 0;
gdjs.Fase3Code.GDbigGroundObjects1.length = 0;
gdjs.Fase3Code.GDbigGroundObjects2.length = 0;
gdjs.Fase3Code.GDbigGroundObjects3.length = 0;
gdjs.Fase3Code.GDplataforma1Objects1.length = 0;
gdjs.Fase3Code.GDplataforma1Objects2.length = 0;
gdjs.Fase3Code.GDplataforma1Objects3.length = 0;
gdjs.Fase3Code.GDendObjects1.length = 0;
gdjs.Fase3Code.GDendObjects2.length = 0;
gdjs.Fase3Code.GDendObjects3.length = 0;

gdjs.Fase3Code.eventsList0x5b6e18(runtimeScene);
return;

}

gdjs['Fase3Code'] = gdjs.Fase3Code;
