gdjs.Fase6Code = {};
gdjs.Fase6Code.GDdinoObjects1= [];
gdjs.Fase6Code.GDdinoObjects2= [];
gdjs.Fase6Code.GDdinoObjects3= [];
gdjs.Fase6Code.GDplayerObjects1= [];
gdjs.Fase6Code.GDplayerObjects2= [];
gdjs.Fase6Code.GDplayerObjects3= [];
gdjs.Fase6Code.GDground1Objects1= [];
gdjs.Fase6Code.GDground1Objects2= [];
gdjs.Fase6Code.GDground1Objects3= [];
gdjs.Fase6Code.GDwaterObjects1= [];
gdjs.Fase6Code.GDwaterObjects2= [];
gdjs.Fase6Code.GDwaterObjects3= [];
gdjs.Fase6Code.GDfade_95faseObjects1= [];
gdjs.Fase6Code.GDfade_95faseObjects2= [];
gdjs.Fase6Code.GDfade_95faseObjects3= [];
gdjs.Fase6Code.GDlblFaseObjects1= [];
gdjs.Fase6Code.GDlblFaseObjects2= [];
gdjs.Fase6Code.GDlblFaseObjects3= [];
gdjs.Fase6Code.GDbg1Objects1= [];
gdjs.Fase6Code.GDbg1Objects2= [];
gdjs.Fase6Code.GDbg1Objects3= [];
gdjs.Fase6Code.GDboxObjects1= [];
gdjs.Fase6Code.GDboxObjects2= [];
gdjs.Fase6Code.GDboxObjects3= [];
gdjs.Fase6Code.GDplacaObjects1= [];
gdjs.Fase6Code.GDplacaObjects2= [];
gdjs.Fase6Code.GDplacaObjects3= [];
gdjs.Fase6Code.GDthreeObjects1= [];
gdjs.Fase6Code.GDthreeObjects2= [];
gdjs.Fase6Code.GDthreeObjects3= [];
gdjs.Fase6Code.GDenemy1Objects1= [];
gdjs.Fase6Code.GDenemy1Objects2= [];
gdjs.Fase6Code.GDenemy1Objects3= [];
gdjs.Fase6Code.GDflipEnemyObjects1= [];
gdjs.Fase6Code.GDflipEnemyObjects2= [];
gdjs.Fase6Code.GDflipEnemyObjects3= [];
gdjs.Fase6Code.GDground1bordasObjects1= [];
gdjs.Fase6Code.GDground1bordasObjects2= [];
gdjs.Fase6Code.GDground1bordasObjects3= [];
gdjs.Fase6Code.GDbigGroundObjects1= [];
gdjs.Fase6Code.GDbigGroundObjects2= [];
gdjs.Fase6Code.GDbigGroundObjects3= [];
gdjs.Fase6Code.GDplataforma1Objects1= [];
gdjs.Fase6Code.GDplataforma1Objects2= [];
gdjs.Fase6Code.GDplataforma1Objects3= [];
gdjs.Fase6Code.GDendObjects1= [];
gdjs.Fase6Code.GDendObjects2= [];
gdjs.Fase6Code.GDendObjects3= [];

gdjs.Fase6Code.conditionTrue_0 = {val:false};
gdjs.Fase6Code.condition0IsTrue_0 = {val:false};
gdjs.Fase6Code.condition1IsTrue_0 = {val:false};
gdjs.Fase6Code.condition2IsTrue_0 = {val:false};
gdjs.Fase6Code.condition3IsTrue_0 = {val:false};
gdjs.Fase6Code.condition4IsTrue_0 = {val:false};
gdjs.Fase6Code.condition5IsTrue_0 = {val:false};
gdjs.Fase6Code.conditionTrue_1 = {val:false};
gdjs.Fase6Code.condition0IsTrue_1 = {val:false};
gdjs.Fase6Code.condition1IsTrue_1 = {val:false};
gdjs.Fase6Code.condition2IsTrue_1 = {val:false};
gdjs.Fase6Code.condition3IsTrue_1 = {val:false};
gdjs.Fase6Code.condition4IsTrue_1 = {val:false};
gdjs.Fase6Code.condition5IsTrue_1 = {val:false};


gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDwaterObjects1Objects = Hashtable.newFrom({"water": gdjs.Fase6Code.GDwaterObjects1});gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase6Code.GDplayerObjects1});gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDendObjects1Objects = Hashtable.newFrom({"end": gdjs.Fase6Code.GDendObjects1});gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase6Code.GDenemy1Objects1});gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase6Code.GDplayerObjects1});gdjs.Fase6Code.eventsList0x802a64 = function(runtimeScene) {

{

gdjs.Fase6Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase6Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
gdjs.Fase6Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase6Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDdinoObjects2[k] = gdjs.Fase6Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase6Code.GDdinoObjects2.length = k;}if ( gdjs.Fase6Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.Fase6Code.condition1IsTrue_0.val = true;
        gdjs.Fase6Code.GDplayerObjects2[k] = gdjs.Fase6Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase6Code.GDplayerObjects2.length = k;}}
if (gdjs.Fase6Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase6Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase6Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase6Code.GDdinoObjects2[i].setAnimationName("jump");
}
}}

}


{

gdjs.Fase6Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase6Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
gdjs.Fase6Code.condition1IsTrue_0.val = false;
gdjs.Fase6Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase6Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDdinoObjects2[k] = gdjs.Fase6Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase6Code.GDdinoObjects2.length = k;}if ( gdjs.Fase6Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving() ) {
        gdjs.Fase6Code.condition1IsTrue_0.val = true;
        gdjs.Fase6Code.GDplayerObjects2[k] = gdjs.Fase6Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase6Code.GDplayerObjects2.length = k;}if ( gdjs.Fase6Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase6Code.condition2IsTrue_0.val = true;
        gdjs.Fase6Code.GDplayerObjects2[k] = gdjs.Fase6Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase6Code.GDplayerObjects2.length = k;}}
}
if (gdjs.Fase6Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Fase6Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase6Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase6Code.GDdinoObjects2[i].setAnimationName("run");
}
}}

}


{

gdjs.Fase6Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase6Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
gdjs.Fase6Code.condition1IsTrue_0.val = false;
gdjs.Fase6Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase6Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDdinoObjects2[k] = gdjs.Fase6Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase6Code.GDdinoObjects2.length = k;}if ( gdjs.Fase6Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDplayerObjects2.length;i<l;++i) {
    if ( !(gdjs.Fase6Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Fase6Code.condition1IsTrue_0.val = true;
        gdjs.Fase6Code.GDplayerObjects2[k] = gdjs.Fase6Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase6Code.GDplayerObjects2.length = k;}if ( gdjs.Fase6Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase6Code.condition2IsTrue_0.val = true;
        gdjs.Fase6Code.GDplayerObjects2[k] = gdjs.Fase6Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase6Code.GDplayerObjects2.length = k;}}
}
if (gdjs.Fase6Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Fase6Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase6Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase6Code.GDdinoObjects2[i].setAnimationName("idle");
}
}}

}


{

gdjs.Fase6Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
gdjs.Fase6Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase6Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isJumping()) ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDplayerObjects1[k] = gdjs.Fase6Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDplayerObjects1.length = k;}if ( gdjs.Fase6Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase6Code.condition1IsTrue_0.val = true;
        gdjs.Fase6Code.GDplayerObjects1[k] = gdjs.Fase6Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase6Code.condition1IsTrue_0.val) {
gdjs.Fase6Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
{for(var i = 0, len = gdjs.Fase6Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDdinoObjects1[i].setAnimationName("falling");
}
}}

}


}; //End of gdjs.Fase6Code.eventsList0x802a64
gdjs.Fase6Code.eventsList0x7b45cc = function(runtimeScene) {

{


gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
gdjs.Fase6Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if (gdjs.Fase6Code.condition0IsTrue_0.val) {
gdjs.Fase6Code.GDdinoObjects2.createFrom(gdjs.Fase6Code.GDdinoObjects1);

{for(var i = 0, len = gdjs.Fase6Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase6Code.GDdinoObjects2[i].flipX(true);
}
}}

}


{


gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
gdjs.Fase6Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if (gdjs.Fase6Code.condition0IsTrue_0.val) {
gdjs.Fase6Code.GDdinoObjects2.createFrom(gdjs.Fase6Code.GDdinoObjects1);

{for(var i = 0, len = gdjs.Fase6Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase6Code.GDdinoObjects2[i].flipX(false);
}
}}

}


{

gdjs.Fase6Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
gdjs.Fase6Code.condition1IsTrue_0.val = false;
{
gdjs.Fase6Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.Fase6Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase6Code.condition1IsTrue_0.val = true;
        gdjs.Fase6Code.GDplayerObjects1[k] = gdjs.Fase6Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase6Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase6Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase6Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDplayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


}; //End of gdjs.Fase6Code.eventsList0x7b45cc
gdjs.Fase6Code.eventsList0x6b6684 = function(runtimeScene) {

{

gdjs.Fase6Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDdinoObjects1[i].getVariableNumber(gdjs.Fase6Code.GDdinoObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDdinoObjects1[k] = gdjs.Fase6Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDdinoObjects1.length = k;}if (gdjs.Fase6Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.Fase6Code.eventsList0x7b45cc(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.Fase6Code.eventsList0x6b6684
gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase6Code.GDenemy1Objects1});gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDflipEnemyObjects1Objects = Hashtable.newFrom({"flipEnemy": gdjs.Fase6Code.GDflipEnemyObjects1});gdjs.Fase6Code.eventsList0x95ed14 = function(runtimeScene) {

{

gdjs.Fase6Code.GDenemy1Objects2.createFrom(gdjs.Fase6Code.GDenemy1Objects1);


gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDenemy1Objects2.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDenemy1Objects2[i].getVariableNumber(gdjs.Fase6Code.GDenemy1Objects2[i].getVariables().getFromIndex(0)) < 0 ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDenemy1Objects2[k] = gdjs.Fase6Code.GDenemy1Objects2[i];
        ++k;
    }
}
gdjs.Fase6Code.GDenemy1Objects2.length = k;}if (gdjs.Fase6Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase6Code.GDenemy1Objects2 */
{for(var i = 0, len = gdjs.Fase6Code.GDenemy1Objects2.length ;i < len;++i) {
    gdjs.Fase6Code.GDenemy1Objects2[i].flipX(false);
}
}}

}


{

/* Reuse gdjs.Fase6Code.GDenemy1Objects1 */

gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase6Code.GDenemy1Objects1[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDenemy1Objects1[k] = gdjs.Fase6Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDenemy1Objects1.length = k;}if (gdjs.Fase6Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase6Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase6Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDenemy1Objects1[i].flipX(true);
}
}}

}


}; //End of gdjs.Fase6Code.eventsList0x95ed14
gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase6Code.GDplayerObjects1});gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase6Code.GDenemy1Objects1});gdjs.Fase6Code.eventsList0x5b6e18 = function(runtimeScene) {

{



}


{


gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
gdjs.Fase6Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase6Code.condition0IsTrue_0.val) {
gdjs.Fase6Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));
gdjs.Fase6Code.GDlblFaseObjects1.createFrom(runtimeScene.getObjects("lblFase"));
{for(var i = 0, len = gdjs.Fase6Code.GDlblFaseObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDlblFaseObjects1[i].setString("FASE " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{gdjs.evtTools.camera.showLayer(runtimeScene, "UI");
}{for(var i = 0, len = gdjs.Fase6Code.GDfade_95faseObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDfade_95faseObjects1[i].getBehavior("Tween").addObjectOpacityTween("fadeout", 0, "linear", 2000, false);
}
}}

}


{

gdjs.Fase6Code.GDwaterObjects1.createFrom(runtimeScene.getObjects("water"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
gdjs.Fase6Code.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDwaterObjects1Objects) > 0;
}if (gdjs.Fase6Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase6Code.GDwaterObjects1 */
{for(var i = 0, len = gdjs.Fase6Code.GDwaterObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDwaterObjects1[i].setXOffset(gdjs.Fase6Code.GDwaterObjects1[i].getXOffset() + (0.5));
}
}}

}


{

gdjs.Fase6Code.GDendObjects1.createFrom(runtimeScene.getObjects("end"));
gdjs.Fase6Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
gdjs.Fase6Code.condition1IsTrue_0.val = false;
{
gdjs.Fase6Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDplayerObjects1Objects, gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDendObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase6Code.condition0IsTrue_0.val ) {
{
gdjs.Fase6Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 0;
}}
if (gdjs.Fase6Code.condition1IsTrue_0.val) {
gdjs.Fase6Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}{for(var i = 0, len = gdjs.Fase6Code.GDfade_95faseObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDfade_95faseObjects1[i].getBehavior("Tween").addObjectOpacityTween("fadein", 255, "linear", 2000, false);
}
}}

}


{

gdjs.Fase6Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
gdjs.Fase6Code.condition1IsTrue_0.val = false;
{
gdjs.Fase6Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if ( gdjs.Fase6Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDfade_95faseObjects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDfade_95faseObjects1[i].getOpacity() >= 255 ) {
        gdjs.Fase6Code.condition1IsTrue_0.val = true;
        gdjs.Fase6Code.GDfade_95faseObjects1[k] = gdjs.Fase6Code.GDfade_95faseObjects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDfade_95faseObjects1.length = k;}}
if (gdjs.Fase6Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(2);
}}

}


{


gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
gdjs.Fase6Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 2;
}if (gdjs.Fase6Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


{



}


{


gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
gdjs.Fase6Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase6Code.condition0IsTrue_0.val) {
gdjs.Fase6Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.Fase6Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDplayerObjects1[i].hide();
}
}}

}


{



}


{


{
gdjs.Fase6Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase6Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.Fase6Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDdinoObjects1[i].setPosition((( gdjs.Fase6Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase6Code.GDplayerObjects1[0].getPointX("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase6Code.GDdinoObjects1[i].getVariables().getFromIndex(0))),(( gdjs.Fase6Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase6Code.GDplayerObjects1[0].getPointY("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase6Code.GDdinoObjects1[i].getVariables().getFromIndex(1))));
}
}{gdjs.evtTools.camera.centerCameraWithinLimits(runtimeScene, (gdjs.Fase6Code.GDdinoObjects1.length !== 0 ? gdjs.Fase6Code.GDdinoObjects1[0] : null), 0, 0, gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)), 768, true, "", 0);
}{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.Fase6Code.GDdinoObjects1.length === 0 ) ? 0 :gdjs.Fase6Code.GDdinoObjects1[0].getPointX("")) * 0.15, "Background", 0);
}}

}


{



}


{



}


{

gdjs.Fase6Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase6Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase6Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
gdjs.Fase6Code.condition1IsTrue_0.val = false;
gdjs.Fase6Code.condition2IsTrue_0.val = false;
gdjs.Fase6Code.condition3IsTrue_0.val = false;
gdjs.Fase6Code.condition4IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase6Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling()) ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDplayerObjects1[k] = gdjs.Fase6Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDplayerObjects1.length = k;}if ( gdjs.Fase6Code.condition0IsTrue_0.val ) {
{
gdjs.Fase6Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDenemy1Objects1Objects, gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDplayerObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase6Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase6Code.GDenemy1Objects1[i].getVariables().get("die")) == 0 ) {
        gdjs.Fase6Code.condition2IsTrue_0.val = true;
        gdjs.Fase6Code.GDenemy1Objects1[k] = gdjs.Fase6Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDenemy1Objects1.length = k;}if ( gdjs.Fase6Code.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDdinoObjects1[i].getVariableNumber(gdjs.Fase6Code.GDdinoObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase6Code.condition3IsTrue_0.val = true;
        gdjs.Fase6Code.GDdinoObjects1[k] = gdjs.Fase6Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDdinoObjects1.length = k;}if ( gdjs.Fase6Code.condition3IsTrue_0.val ) {
{
{gdjs.Fase6Code.conditionTrue_1 = gdjs.Fase6Code.condition4IsTrue_0;
gdjs.Fase6Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8850868);
}
}}
}
}
}
if (gdjs.Fase6Code.condition4IsTrue_0.val) {
/* Reuse gdjs.Fase6Code.GDdinoObjects1 */
/* Reuse gdjs.Fase6Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase6Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDplayerObjects1[i].getBehavior("PlatformerObject").ignoreDefaultControls(true);
}
}{for(var i = 0, len = gdjs.Fase6Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDdinoObjects1[i].returnVariable(gdjs.Fase6Code.GDdinoObjects1[i].getVariables().getFromIndex(2)).setNumber(1);
}
}{for(var i = 0, len = gdjs.Fase6Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDdinoObjects1[i].setAnimationName("die");
}
}}

}


{



}


{

gdjs.Fase6Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
gdjs.Fase6Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDdinoObjects1[i].isCurrentAnimationName("die") ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDdinoObjects1[k] = gdjs.Fase6Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDdinoObjects1.length = k;}if ( gdjs.Fase6Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDdinoObjects1[i].hasAnimationEnded() ) {
        gdjs.Fase6Code.condition1IsTrue_0.val = true;
        gdjs.Fase6Code.GDdinoObjects1[k] = gdjs.Fase6Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDdinoObjects1.length = k;}}
if (gdjs.Fase6Code.condition1IsTrue_0.val) {
{gdjs.evtsExt__Util__setFase.func(runtimeScene, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.Fase6Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDdinoObjects1[i].getY() > gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDdinoObjects1[k] = gdjs.Fase6Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDdinoObjects1.length = k;}if (gdjs.Fase6Code.condition0IsTrue_0.val) {
{gdjs.evtsExt__Util__setFase.func(runtimeScene, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Fase6Code.eventsList0x802a64(runtimeScene);
}


{


gdjs.Fase6Code.eventsList0x6b6684(runtimeScene);
}


{



}


{


gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
gdjs.Fase6Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase6Code.condition0IsTrue_0.val) {
gdjs.Fase6Code.GDflipEnemyObjects1.createFrom(runtimeScene.getObjects("flipEnemy"));
{for(var i = 0, len = gdjs.Fase6Code.GDflipEnemyObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDflipEnemyObjects1[i].hide();
}
}}

}


{



}


{


{
gdjs.Fase6Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
{for(var i = 0, len = gdjs.Fase6Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDenemy1Objects1[i].setX(gdjs.Fase6Code.GDenemy1Objects1[i].getX() + ((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase6Code.GDenemy1Objects1[i].getVariables().getFromIndex(0))) * gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{



}


{

gdjs.Fase6Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase6Code.GDflipEnemyObjects1.createFrom(runtimeScene.getObjects("flipEnemy"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
gdjs.Fase6Code.condition1IsTrue_0.val = false;
{
gdjs.Fase6Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDenemy1Objects1Objects, gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDflipEnemyObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase6Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase6Code.conditionTrue_1 = gdjs.Fase6Code.condition1IsTrue_0;
gdjs.Fase6Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(9623756);
}
}}
if (gdjs.Fase6Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase6Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase6Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDenemy1Objects1[i].returnVariable(gdjs.Fase6Code.GDenemy1Objects1[i].getVariables().getFromIndex(0)).mul(-(1));
}
}
{ //Subevents
gdjs.Fase6Code.eventsList0x95ed14(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.Fase6Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase6Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
gdjs.Fase6Code.condition1IsTrue_0.val = false;
gdjs.Fase6Code.condition2IsTrue_0.val = false;
gdjs.Fase6Code.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDplayerObjects1[k] = gdjs.Fase6Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDplayerObjects1.length = k;}if ( gdjs.Fase6Code.condition0IsTrue_0.val ) {
{
gdjs.Fase6Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDplayerObjects1Objects, gdjs.Fase6Code.mapOfGDgdjs_46Fase6Code_46GDenemy1Objects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase6Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase6Code.GDenemy1Objects1[i].getVariables().get("die")) == 0 ) {
        gdjs.Fase6Code.condition2IsTrue_0.val = true;
        gdjs.Fase6Code.GDenemy1Objects1[k] = gdjs.Fase6Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDenemy1Objects1.length = k;}if ( gdjs.Fase6Code.condition2IsTrue_0.val ) {
{
{gdjs.Fase6Code.conditionTrue_1 = gdjs.Fase6Code.condition3IsTrue_0;
gdjs.Fase6Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8454172);
}
}}
}
}
if (gdjs.Fase6Code.condition3IsTrue_0.val) {
/* Reuse gdjs.Fase6Code.GDenemy1Objects1 */
/* Reuse gdjs.Fase6Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase6Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDplayerObjects1[i].getBehavior("PlatformerObject").setCanJump();
}
}{for(var i = 0, len = gdjs.Fase6Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDplayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}{for(var i = 0, len = gdjs.Fase6Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDenemy1Objects1[i].returnVariable(gdjs.Fase6Code.GDenemy1Objects1[i].getVariables().get("die")).setNumber(1);
}
}{for(var i = 0, len = gdjs.Fase6Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDenemy1Objects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.Fase6Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDenemy1Objects1[i].getBehavior("Tween").addObjectOpacityTween("enemy1_die", 0, "linear", 1000, false);
}
}}

}


{

gdjs.Fase6Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));

gdjs.Fase6Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase6Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase6Code.GDenemy1Objects1[i].getOpacity() <= 0 ) {
        gdjs.Fase6Code.condition0IsTrue_0.val = true;
        gdjs.Fase6Code.GDenemy1Objects1[k] = gdjs.Fase6Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase6Code.GDenemy1Objects1.length = k;}if (gdjs.Fase6Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase6Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase6Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase6Code.GDenemy1Objects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.Fase6Code.eventsList0x5b6e18


gdjs.Fase6Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Fase6Code.GDdinoObjects1.length = 0;
gdjs.Fase6Code.GDdinoObjects2.length = 0;
gdjs.Fase6Code.GDdinoObjects3.length = 0;
gdjs.Fase6Code.GDplayerObjects1.length = 0;
gdjs.Fase6Code.GDplayerObjects2.length = 0;
gdjs.Fase6Code.GDplayerObjects3.length = 0;
gdjs.Fase6Code.GDground1Objects1.length = 0;
gdjs.Fase6Code.GDground1Objects2.length = 0;
gdjs.Fase6Code.GDground1Objects3.length = 0;
gdjs.Fase6Code.GDwaterObjects1.length = 0;
gdjs.Fase6Code.GDwaterObjects2.length = 0;
gdjs.Fase6Code.GDwaterObjects3.length = 0;
gdjs.Fase6Code.GDfade_95faseObjects1.length = 0;
gdjs.Fase6Code.GDfade_95faseObjects2.length = 0;
gdjs.Fase6Code.GDfade_95faseObjects3.length = 0;
gdjs.Fase6Code.GDlblFaseObjects1.length = 0;
gdjs.Fase6Code.GDlblFaseObjects2.length = 0;
gdjs.Fase6Code.GDlblFaseObjects3.length = 0;
gdjs.Fase6Code.GDbg1Objects1.length = 0;
gdjs.Fase6Code.GDbg1Objects2.length = 0;
gdjs.Fase6Code.GDbg1Objects3.length = 0;
gdjs.Fase6Code.GDboxObjects1.length = 0;
gdjs.Fase6Code.GDboxObjects2.length = 0;
gdjs.Fase6Code.GDboxObjects3.length = 0;
gdjs.Fase6Code.GDplacaObjects1.length = 0;
gdjs.Fase6Code.GDplacaObjects2.length = 0;
gdjs.Fase6Code.GDplacaObjects3.length = 0;
gdjs.Fase6Code.GDthreeObjects1.length = 0;
gdjs.Fase6Code.GDthreeObjects2.length = 0;
gdjs.Fase6Code.GDthreeObjects3.length = 0;
gdjs.Fase6Code.GDenemy1Objects1.length = 0;
gdjs.Fase6Code.GDenemy1Objects2.length = 0;
gdjs.Fase6Code.GDenemy1Objects3.length = 0;
gdjs.Fase6Code.GDflipEnemyObjects1.length = 0;
gdjs.Fase6Code.GDflipEnemyObjects2.length = 0;
gdjs.Fase6Code.GDflipEnemyObjects3.length = 0;
gdjs.Fase6Code.GDground1bordasObjects1.length = 0;
gdjs.Fase6Code.GDground1bordasObjects2.length = 0;
gdjs.Fase6Code.GDground1bordasObjects3.length = 0;
gdjs.Fase6Code.GDbigGroundObjects1.length = 0;
gdjs.Fase6Code.GDbigGroundObjects2.length = 0;
gdjs.Fase6Code.GDbigGroundObjects3.length = 0;
gdjs.Fase6Code.GDplataforma1Objects1.length = 0;
gdjs.Fase6Code.GDplataforma1Objects2.length = 0;
gdjs.Fase6Code.GDplataforma1Objects3.length = 0;
gdjs.Fase6Code.GDendObjects1.length = 0;
gdjs.Fase6Code.GDendObjects2.length = 0;
gdjs.Fase6Code.GDendObjects3.length = 0;

gdjs.Fase6Code.eventsList0x5b6e18(runtimeScene);
return;

}

gdjs['Fase6Code'] = gdjs.Fase6Code;
