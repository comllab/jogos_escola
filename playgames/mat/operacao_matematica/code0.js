gdjs.StartCode = {};
gdjs.StartCode.GDbuttomObjects1= [];
gdjs.StartCode.GDbuttomObjects2= [];
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDsiteObjects1= [];
gdjs.StartCode.GDsiteObjects2= [];
gdjs.StartCode.GDlblJogarObjects1= [];
gdjs.StartCode.GDlblJogarObjects2= [];
gdjs.StartCode.GDtituloObjects1= [];
gdjs.StartCode.GDtituloObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};
gdjs.StartCode.condition3IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbuttomObjects1Objects = Hashtable.newFrom({"buttom": gdjs.StartCode.GDbuttomObjects1});gdjs.StartCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "House_1.wav", true, 100, 1);
}}

}


{

gdjs.StartCode.GDbuttomObjects1.createFrom(runtimeScene.getObjects("buttom"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbuttomObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7019828);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xaff48


gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.StartCode.GDbuttomObjects1.length = 0;
gdjs.StartCode.GDbuttomObjects2.length = 0;
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDsiteObjects1.length = 0;
gdjs.StartCode.GDsiteObjects2.length = 0;
gdjs.StartCode.GDlblJogarObjects1.length = 0;
gdjs.StartCode.GDlblJogarObjects2.length = 0;
gdjs.StartCode.GDtituloObjects1.length = 0;
gdjs.StartCode.GDtituloObjects2.length = 0;

gdjs.StartCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['StartCode'] = gdjs.StartCode;
