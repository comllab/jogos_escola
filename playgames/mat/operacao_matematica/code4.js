gdjs.GameOverCode = {};
gdjs.GameOverCode.GDbuttomObjects1= [];
gdjs.GameOverCode.GDbuttomObjects2= [];
gdjs.GameOverCode.GDbackgroundObjects1= [];
gdjs.GameOverCode.GDbackgroundObjects2= [];
gdjs.GameOverCode.GDsiteObjects1= [];
gdjs.GameOverCode.GDsiteObjects2= [];
gdjs.GameOverCode.GDlblJogarObjects1= [];
gdjs.GameOverCode.GDlblJogarObjects2= [];
gdjs.GameOverCode.GDtituloObjects1= [];
gdjs.GameOverCode.GDtituloObjects2= [];

gdjs.GameOverCode.conditionTrue_0 = {val:false};
gdjs.GameOverCode.condition0IsTrue_0 = {val:false};
gdjs.GameOverCode.condition1IsTrue_0 = {val:false};
gdjs.GameOverCode.condition2IsTrue_0 = {val:false};
gdjs.GameOverCode.condition3IsTrue_0 = {val:false};
gdjs.GameOverCode.conditionTrue_1 = {val:false};
gdjs.GameOverCode.condition0IsTrue_1 = {val:false};
gdjs.GameOverCode.condition1IsTrue_1 = {val:false};
gdjs.GameOverCode.condition2IsTrue_1 = {val:false};
gdjs.GameOverCode.condition3IsTrue_1 = {val:false};


gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbuttomObjects1Objects = Hashtable.newFrom({"buttom": gdjs.GameOverCode.GDbuttomObjects1});gdjs.GameOverCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameOverCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "preview.ogg", false, 100, 1);
}}

}


{

gdjs.GameOverCode.GDbuttomObjects1.createFrom(runtimeScene.getObjects("buttom"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
gdjs.GameOverCode.condition2IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbuttomObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameOverCode.condition1IsTrue_0.val ) {
{
{gdjs.GameOverCode.conditionTrue_1 = gdjs.GameOverCode.condition2IsTrue_0;
gdjs.GameOverCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(6946596);
}
}}
}
if (gdjs.GameOverCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.GameOverCode.eventsList0xaff48


gdjs.GameOverCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.GameOverCode.GDbuttomObjects1.length = 0;
gdjs.GameOverCode.GDbuttomObjects2.length = 0;
gdjs.GameOverCode.GDbackgroundObjects1.length = 0;
gdjs.GameOverCode.GDbackgroundObjects2.length = 0;
gdjs.GameOverCode.GDsiteObjects1.length = 0;
gdjs.GameOverCode.GDsiteObjects2.length = 0;
gdjs.GameOverCode.GDlblJogarObjects1.length = 0;
gdjs.GameOverCode.GDlblJogarObjects2.length = 0;
gdjs.GameOverCode.GDtituloObjects1.length = 0;
gdjs.GameOverCode.GDtituloObjects2.length = 0;

gdjs.GameOverCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['GameOverCode'] = gdjs.GameOverCode;
