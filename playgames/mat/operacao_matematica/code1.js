gdjs.GameCode = {};
gdjs.GameCode.GDbulletObjects1_1final = [];

gdjs.GameCode.GDenemy1Objects1_1final = [];

gdjs.GameCode.GDenemy2Objects1_1final = [];

gdjs.GameCode.GDenemy_95fireObjects1_1final = [];

gdjs.GameCode.GDplayerObjects1_1final = [];

gdjs.GameCode.GDbuttomObjects1= [];
gdjs.GameCode.GDbuttomObjects2= [];
gdjs.GameCode.GDbuttomObjects3= [];
gdjs.GameCode.GDbackground8Objects1= [];
gdjs.GameCode.GDbackground8Objects2= [];
gdjs.GameCode.GDbackground8Objects3= [];
gdjs.GameCode.GDbackground7Objects1= [];
gdjs.GameCode.GDbackground7Objects2= [];
gdjs.GameCode.GDbackground7Objects3= [];
gdjs.GameCode.GDbackground6Objects1= [];
gdjs.GameCode.GDbackground6Objects2= [];
gdjs.GameCode.GDbackground6Objects3= [];
gdjs.GameCode.GDbackground5Objects1= [];
gdjs.GameCode.GDbackground5Objects2= [];
gdjs.GameCode.GDbackground5Objects3= [];
gdjs.GameCode.GDbackground4Objects1= [];
gdjs.GameCode.GDbackground4Objects2= [];
gdjs.GameCode.GDbackground4Objects3= [];
gdjs.GameCode.GDbackground3Objects1= [];
gdjs.GameCode.GDbackground3Objects2= [];
gdjs.GameCode.GDbackground3Objects3= [];
gdjs.GameCode.GDbackground2Objects1= [];
gdjs.GameCode.GDbackground2Objects2= [];
gdjs.GameCode.GDbackground2Objects3= [];
gdjs.GameCode.GDbackground1Objects1= [];
gdjs.GameCode.GDbackground1Objects2= [];
gdjs.GameCode.GDbackground1Objects3= [];
gdjs.GameCode.GDplayerObjects1= [];
gdjs.GameCode.GDplayerObjects2= [];
gdjs.GameCode.GDplayerObjects3= [];
gdjs.GameCode.GDbulletObjects1= [];
gdjs.GameCode.GDbulletObjects2= [];
gdjs.GameCode.GDbulletObjects3= [];
gdjs.GameCode.GDenemy2Objects1= [];
gdjs.GameCode.GDenemy2Objects2= [];
gdjs.GameCode.GDenemy2Objects3= [];
gdjs.GameCode.GDenemy1Objects1= [];
gdjs.GameCode.GDenemy1Objects2= [];
gdjs.GameCode.GDenemy1Objects3= [];
gdjs.GameCode.GDmobile_95dirObjects1= [];
gdjs.GameCode.GDmobile_95dirObjects2= [];
gdjs.GameCode.GDmobile_95dirObjects3= [];
gdjs.GameCode.GDmobile_95planeObjects1= [];
gdjs.GameCode.GDmobile_95planeObjects2= [];
gdjs.GameCode.GDmobile_95planeObjects3= [];
gdjs.GameCode.GDmobile_95shotObjects1= [];
gdjs.GameCode.GDmobile_95shotObjects2= [];
gdjs.GameCode.GDmobile_95shotObjects3= [];
gdjs.GameCode.GDbullet_95spawnObjects1= [];
gdjs.GameCode.GDbullet_95spawnObjects2= [];
gdjs.GameCode.GDbullet_95spawnObjects3= [];
gdjs.GameCode.GDenemy_95fireObjects1= [];
gdjs.GameCode.GDenemy_95fireObjects2= [];
gdjs.GameCode.GDenemy_95fireObjects3= [];
gdjs.GameCode.GDexploseObjects1= [];
gdjs.GameCode.GDexploseObjects2= [];
gdjs.GameCode.GDexploseObjects3= [];
gdjs.GameCode.GDplayer_95lifeObjects1= [];
gdjs.GameCode.GDplayer_95lifeObjects2= [];
gdjs.GameCode.GDplayer_95lifeObjects3= [];
gdjs.GameCode.GDdisplay_95lifeObjects1= [];
gdjs.GameCode.GDdisplay_95lifeObjects2= [];
gdjs.GameCode.GDdisplay_95lifeObjects3= [];
gdjs.GameCode.GDlifeObjects1= [];
gdjs.GameCode.GDlifeObjects2= [];
gdjs.GameCode.GDlifeObjects3= [];
gdjs.GameCode.GDalvosObjects1= [];
gdjs.GameCode.GDalvosObjects2= [];
gdjs.GameCode.GDalvosObjects3= [];
gdjs.GameCode.GDlblMissaoObjects1= [];
gdjs.GameCode.GDlblMissaoObjects2= [];
gdjs.GameCode.GDlblMissaoObjects3= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.condition4IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};
gdjs.GameCode.condition4IsTrue_1 = {val:false};


gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground1Objects2Objects = Hashtable.newFrom({"background1": gdjs.GameCode.GDbackground1Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground2Objects2Objects = Hashtable.newFrom({"background2": gdjs.GameCode.GDbackground2Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground3Objects2Objects = Hashtable.newFrom({"background3": gdjs.GameCode.GDbackground3Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground4Objects2Objects = Hashtable.newFrom({"background4": gdjs.GameCode.GDbackground4Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground5Objects2Objects = Hashtable.newFrom({"background5": gdjs.GameCode.GDbackground5Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground6Objects2Objects = Hashtable.newFrom({"background6": gdjs.GameCode.GDbackground6Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground7Objects2Objects = Hashtable.newFrom({"background7": gdjs.GameCode.GDbackground7Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground8Objects2Objects = Hashtable.newFrom({"background8": gdjs.GameCode.GDbackground8Objects2});gdjs.GameCode.eventsList0x6b0ca8 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("background")) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbackground1Objects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground1Objects2Objects, 0, 0, "Background");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("background")) == 2;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbackground2Objects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground2Objects2Objects, 0, 0, "Background");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("background")) == 3;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbackground3Objects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground3Objects2Objects, 0, 0, "Background");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("background")) == 4;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbackground4Objects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground4Objects2Objects, 0, 0, "Background");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("background")) == 5;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbackground5Objects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground5Objects2Objects, 0, 0, "Background");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("background")) == 6;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbackground6Objects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground6Objects2Objects, 0, 0, "Background");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("background")) == 7;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbackground7Objects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground7Objects2Objects, 0, 0, "Background");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("background")) == 8;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbackground8Objects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbackground8Objects2Objects, 0, 0, "Background");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().get("missão")) < 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDlblMissaoObjects2.createFrom(runtimeScene.getObjects("lblMissao"));
{for(var i = 0, len = gdjs.GameCode.GDlblMissaoObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlblMissaoObjects2[i].setString("MISSÃO 0" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5))));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().get("missão")) >= 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDlblMissaoObjects2.createFrom(runtimeScene.getObjects("lblMissao"));
{for(var i = 0, len = gdjs.GameCode.GDlblMissaoObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlblMissaoObjects2[i].setString("MISSÃO " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5))));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().get("missão")) > 99;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDlblMissaoObjects1.createFrom(runtimeScene.getObjects("lblMissao"));
{for(var i = 0, len = gdjs.GameCode.GDlblMissaoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDlblMissaoObjects1[i].setString("O MELHOR!");
}
}}

}


}; //End of gdjs.GameCode.eventsList0x6b0ca8
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbulletObjects2Objects = Hashtable.newFrom({"bullet": gdjs.GameCode.GDbulletObjects2});gdjs.GameCode.eventsList0x9a18f8 = function(runtimeScene) {

{

/* Reuse gdjs.GameCode.GDplayerObjects2 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects2[i].hasAnimationEnded() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects2[k] = gdjs.GameCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].setAnimation(0);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x9a18f8
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595planeObjects2Objects = Hashtable.newFrom({"mobile_plane": gdjs.GameCode.GDmobile_95planeObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595planeObjects2Objects = Hashtable.newFrom({"mobile_plane": gdjs.GameCode.GDmobile_95planeObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595planeObjects2Objects = Hashtable.newFrom({"mobile_plane": gdjs.GameCode.GDmobile_95planeObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595planeObjects2Objects = Hashtable.newFrom({"mobile_plane": gdjs.GameCode.GDmobile_95planeObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595shotObjects1Objects = Hashtable.newFrom({"mobile_shot": gdjs.GameCode.GDmobile_95shotObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbulletObjects1Objects = Hashtable.newFrom({"bullet": gdjs.GameCode.GDbulletObjects1});gdjs.GameCode.eventsList0x6d6bb0 = function(runtimeScene) {

{

gdjs.GameCode.GDmobile_95planeObjects2.createFrom(runtimeScene.getObjects("mobile_plane"));
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595planeObjects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDmobile_95planeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDmobile_95planeObjects2[i].isCurrentAnimationName("up") ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDmobile_95planeObjects2[k] = gdjs.GameCode.GDmobile_95planeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDmobile_95planeObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects2[i].getY() > 95 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects2[k] = gdjs.GameCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects2.length = k;}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
gdjs.GameCode.condition3IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Mobile");
}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].setAngle(-10);
}
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].addForce(0, -700, 0);
}
}}

}


{

gdjs.GameCode.GDmobile_95planeObjects2.createFrom(runtimeScene.getObjects("mobile_plane"));
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595planeObjects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDmobile_95planeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDmobile_95planeObjects2[i].isCurrentAnimationName("down") ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDmobile_95planeObjects2[k] = gdjs.GameCode.GDmobile_95planeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDmobile_95planeObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects2[i].getY() < gdjs.evtTools.window.getWindowHeight() ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects2[k] = gdjs.GameCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].setAngle(10);
}
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].addForce(0, 700, 0);
}
}}

}


{

gdjs.GameCode.GDmobile_95planeObjects2.createFrom(runtimeScene.getObjects("mobile_plane"));
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595planeObjects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDmobile_95planeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDmobile_95planeObjects2[i].isCurrentAnimationName("left") ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDmobile_95planeObjects2[k] = gdjs.GameCode.GDmobile_95planeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDmobile_95planeObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects2[i].getX() > 5 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects2[k] = gdjs.GameCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].setAngle(-10);
}
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].addForce(-700, 0, 0);
}
}}

}


{

gdjs.GameCode.GDmobile_95planeObjects2.createFrom(runtimeScene.getObjects("mobile_plane"));
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595planeObjects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDmobile_95planeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDmobile_95planeObjects2[i].isCurrentAnimationName("right") ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDmobile_95planeObjects2[k] = gdjs.GameCode.GDmobile_95planeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDmobile_95planeObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects2[i].getX() < gdjs.evtTools.window.getWindowWidth() - (gdjs.GameCode.GDplayerObjects2[i].getWidth()) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects2[k] = gdjs.GameCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].addForce(700, 0, 0);
}
}}

}


{

gdjs.GameCode.GDmobile_95shotObjects1.createFrom(runtimeScene.getObjects("mobile_shot"));
/* Reuse gdjs.GameCode.GDplayerObjects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getVariableNumber(gdjs.GameCode.GDplayerObjects1[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmobile_9595shotObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDbullet_95spawnObjects1.createFrom(runtimeScene.getObjects("bullet_spawn"));
gdjs.GameCode.GDbulletObjects1.length = 0;

{gdjs.evtTools.object.createObjectFromGroupOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbulletObjects1Objects, "shot", (( gdjs.GameCode.GDbullet_95spawnObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDbullet_95spawnObjects1[0].getPointX("")), (( gdjs.GameCode.GDbullet_95spawnObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDbullet_95spawnObjects1[0].getPointY("")), "");
}{for(var i = 0, len = gdjs.GameCode.GDbulletObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbulletObjects1[i].setZOrder(99);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x6d6bb0
gdjs.GameCode.eventsList0x95ace0 = function(runtimeScene) {

{

gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects2[i].getY() > 100 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects2[k] = gdjs.GameCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].addForce(0, -700, 0);
}
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].setAngle(-10);
}
}}

}


{

gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects2[i].getY() < gdjs.evtTools.window.getWindowHeight() - (gdjs.GameCode.GDplayerObjects2[i].getHeight())/2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects2[k] = gdjs.GameCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].addForce(0, 700, 0);
}
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].setAngle(10);
}
}}

}


{

gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects2[i].getX() > 5 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects2[k] = gdjs.GameCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].addForce(-700, 0, 0);
}
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].setAngle(-10);
}
}}

}


{

gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects2[i].getX() < gdjs.evtTools.window.getWindowWidth() - (gdjs.GameCode.GDplayerObjects2[i].getWidth()) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects2[k] = gdjs.GameCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].addForce(700, 0, 0);
}
}}

}


{

gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects2[i].getVariableNumber(gdjs.GameCode.GDplayerObjects2[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects2[k] = gdjs.GameCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "Space");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDbullet_95spawnObjects2.createFrom(runtimeScene.getObjects("bullet_spawn"));
/* Reuse gdjs.GameCode.GDplayerObjects2 */
gdjs.GameCode.GDbulletObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbulletObjects2Objects, (( gdjs.GameCode.GDbullet_95spawnObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbullet_95spawnObjects2[0].getPointX("")), (( gdjs.GameCode.GDbullet_95spawnObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbullet_95spawnObjects2[0].getPointY("")), "Tiro");
}{gdjs.evtTools.sound.playSound(runtimeScene, "Laser_01.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].setAnimation(1);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x9a18f8(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Mobile");
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x6d6bb0(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x95ace0
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbulletObjects2Objects = Hashtable.newFrom({"bullet": gdjs.GameCode.GDbulletObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy1Objects2Objects = Hashtable.newFrom({"enemy1": gdjs.GameCode.GDenemy1Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbulletObjects2Objects = Hashtable.newFrom({"bullet": gdjs.GameCode.GDbulletObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy2Objects2Objects = Hashtable.newFrom({"enemy2": gdjs.GameCode.GDenemy2Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbulletObjects1Objects = Hashtable.newFrom({"bullet": gdjs.GameCode.GDbulletObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy_9595fireObjects1Objects = Hashtable.newFrom({"enemy_fire": gdjs.GameCode.GDenemy_95fireObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects = Hashtable.newFrom({"explose": gdjs.GameCode.GDexploseObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy_9595fireObjects2Objects = Hashtable.newFrom({"enemy_fire": gdjs.GameCode.GDenemy_95fireObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects = Hashtable.newFrom({"player": gdjs.GameCode.GDplayerObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy1Objects2Objects = Hashtable.newFrom({"enemy1": gdjs.GameCode.GDenemy1Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects = Hashtable.newFrom({"player": gdjs.GameCode.GDplayerObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy2Objects2Objects = Hashtable.newFrom({"enemy2": gdjs.GameCode.GDenemy2Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects = Hashtable.newFrom({"player": gdjs.GameCode.GDplayerObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects = Hashtable.newFrom({"explose": gdjs.GameCode.GDexploseObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.GameCode.GDplayerObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects = Hashtable.newFrom({"explose": gdjs.GameCode.GDexploseObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy_9595fireObjects2Objects = Hashtable.newFrom({"enemy_fire": gdjs.GameCode.GDenemy_95fireObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects = Hashtable.newFrom({"player": gdjs.GameCode.GDplayerObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy1Objects2Objects = Hashtable.newFrom({"enemy1": gdjs.GameCode.GDenemy1Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects = Hashtable.newFrom({"player": gdjs.GameCode.GDplayerObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy2Objects2Objects = Hashtable.newFrom({"enemy2": gdjs.GameCode.GDenemy2Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects = Hashtable.newFrom({"player": gdjs.GameCode.GDplayerObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects = Hashtable.newFrom({"explose": gdjs.GameCode.GDexploseObjects1});gdjs.GameCode.eventsList0xaf9a10 = function(runtimeScene) {

{

gdjs.GameCode.GDenemy1Objects1.length = 0;

gdjs.GameCode.GDenemy2Objects1.length = 0;

gdjs.GameCode.GDenemy_95fireObjects1.length = 0;

/* Reuse gdjs.GameCode.GDplayerObjects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.GDenemy1Objects1_1final.length = 0;gdjs.GameCode.GDenemy2Objects1_1final.length = 0;gdjs.GameCode.GDenemy_95fireObjects1_1final.length = 0;gdjs.GameCode.GDplayerObjects1_1final.length = 0;gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
gdjs.GameCode.condition2IsTrue_1.val = false;
gdjs.GameCode.condition3IsTrue_1.val = false;
{
gdjs.GameCode.GDenemy_95fireObjects2.createFrom(runtimeScene.getObjects("enemy_fire"));
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy_9595fireObjects2Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects, false, runtimeScene);
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDenemy_95fireObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDenemy_95fireObjects1_1final.indexOf(gdjs.GameCode.GDenemy_95fireObjects2[j]) === -1 )
            gdjs.GameCode.GDenemy_95fireObjects1_1final.push(gdjs.GameCode.GDenemy_95fireObjects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDplayerObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDplayerObjects1_1final.indexOf(gdjs.GameCode.GDplayerObjects2[j]) === -1 )
            gdjs.GameCode.GDplayerObjects1_1final.push(gdjs.GameCode.GDplayerObjects2[j]);
    }
}
}
{
gdjs.GameCode.GDenemy1Objects2.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy1Objects2Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects, false, runtimeScene);
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDenemy1Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDenemy1Objects1_1final.indexOf(gdjs.GameCode.GDenemy1Objects2[j]) === -1 )
            gdjs.GameCode.GDenemy1Objects1_1final.push(gdjs.GameCode.GDenemy1Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDplayerObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDplayerObjects1_1final.indexOf(gdjs.GameCode.GDplayerObjects2[j]) === -1 )
            gdjs.GameCode.GDplayerObjects1_1final.push(gdjs.GameCode.GDplayerObjects2[j]);
    }
}
}
{
gdjs.GameCode.GDenemy2Objects2.createFrom(runtimeScene.getObjects("enemy2"));
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

gdjs.GameCode.condition2IsTrue_1.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy2Objects2Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects, false, runtimeScene);
if( gdjs.GameCode.condition2IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDenemy2Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDenemy2Objects1_1final.indexOf(gdjs.GameCode.GDenemy2Objects2[j]) === -1 )
            gdjs.GameCode.GDenemy2Objects1_1final.push(gdjs.GameCode.GDenemy2Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDplayerObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDplayerObjects1_1final.indexOf(gdjs.GameCode.GDplayerObjects2[j]) === -1 )
            gdjs.GameCode.GDplayerObjects1_1final.push(gdjs.GameCode.GDplayerObjects2[j]);
    }
}
}
{
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects2[i].getY() > gdjs.evtTools.window.getWindowHeight() - (gdjs.GameCode.GDplayerObjects2[i].getHeight())/2 ) {
        gdjs.GameCode.condition3IsTrue_1.val = true;
        gdjs.GameCode.GDplayerObjects2[k] = gdjs.GameCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects2.length = k;if( gdjs.GameCode.condition3IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDplayerObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDplayerObjects1_1final.indexOf(gdjs.GameCode.GDplayerObjects2[j]) === -1 )
            gdjs.GameCode.GDplayerObjects1_1final.push(gdjs.GameCode.GDplayerObjects2[j]);
    }
}
}
{
gdjs.GameCode.GDenemy1Objects1.createFrom(gdjs.GameCode.GDenemy1Objects1_1final);
gdjs.GameCode.GDenemy2Objects1.createFrom(gdjs.GameCode.GDenemy2Objects1_1final);
gdjs.GameCode.GDenemy_95fireObjects1.createFrom(gdjs.GameCode.GDenemy_95fireObjects1_1final);
gdjs.GameCode.GDplayerObjects1.createFrom(gdjs.GameCode.GDplayerObjects1_1final);
}
}
}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy1Objects1 */
/* Reuse gdjs.GameCode.GDenemy2Objects1 */
/* Reuse gdjs.GameCode.GDenemy_95fireObjects1 */
/* Reuse gdjs.GameCode.GDplayerObjects1 */
gdjs.GameCode.GDexploseObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects, (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointX("")), (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointY("")), "Enemys");
}{gdjs.evtTools.sound.playSound(runtimeScene, "shipexplode.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDenemy_95fireObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy_95fireObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy2Objects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].deleteFromScene(runtimeScene);
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GameOver", false);
}}

}


}; //End of gdjs.GameCode.eventsList0xaf9a10
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.GameCode.GDplayerObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlifeObjects1Objects = Hashtable.newFrom({"life": gdjs.GameCode.GDlifeObjects1});gdjs.GameCode.eventsList0xaeade8 = function(runtimeScene) {

{

/* Reuse gdjs.GameCode.GDplayerObjects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getVariableNumber(gdjs.GameCode.GDplayerObjects1[i].getVariables().getFromIndex(0)) < 10 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].returnVariable(gdjs.GameCode.GDplayerObjects1[i].getVariables().getFromIndex(0)).add(1);
}
}}

}


}; //End of gdjs.GameCode.eventsList0xaeade8
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.GameCode.GDenemy1Objects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.GameCode.GDenemy1Objects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy2Objects1Objects = Hashtable.newFrom({"enemy2": gdjs.GameCode.GDenemy2Objects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy2Objects1Objects = Hashtable.newFrom({"enemy2": gdjs.GameCode.GDenemy2Objects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy_9595fireObjects1Objects = Hashtable.newFrom({"enemy_fire": gdjs.GameCode.GDenemy_95fireObjects1});gdjs.GameCode.eventsList0x6ae960 = function(runtimeScene) {

{

/* Reuse gdjs.GameCode.GDenemy_95fireObjects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy_9595fireObjects1Objects) > 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.sound.isSoundOnChannelStopped(runtimeScene, 9);
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "bomb.ogg", 9, false, 100, 1);
}}

}


}; //End of gdjs.GameCode.eventsList0x6ae960
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy_9595fireObjects1Objects = Hashtable.newFrom({"enemy_fire": gdjs.GameCode.GDenemy_95fireObjects1});gdjs.GameCode.eventsList0x685260 = function(runtimeScene) {

{

/* Reuse gdjs.GameCode.GDenemy2Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy2Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy2Objects1[i].getVariableNumber(gdjs.GameCode.GDenemy2Objects1[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy2Objects1[k] = gdjs.GameCode.GDenemy2Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy2Objects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, gdjs.randomInRange(3,  5), "enemy2_fire");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy2Objects1 */
gdjs.GameCode.GDenemy_95fireObjects1.length = 0;

{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "enemy2_fire");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy_9595fireObjects1Objects, (( gdjs.GameCode.GDenemy2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects1[0].getPointX("")), (( gdjs.GameCode.GDenemy2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects1[0].getPointY("")), "Enemys");
}{for(var i = 0, len = gdjs.GameCode.GDenemy_95fireObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy_95fireObjects1[i].setAngle(-90);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x685260
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects = Hashtable.newFrom({"explose": gdjs.GameCode.GDexploseObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects = Hashtable.newFrom({"explose": gdjs.GameCode.GDexploseObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlifeObjects2Objects = Hashtable.newFrom({"life": gdjs.GameCode.GDlifeObjects2});gdjs.GameCode.eventsList0xaeb6b0 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = (gdjs.random(4) == 3);
}
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDenemy1Objects2.createFrom(gdjs.GameCode.GDenemy1Objects1);

gdjs.GameCode.GDenemy2Objects2.createFrom(runtimeScene.getObjects("enemy2"));
gdjs.GameCode.GDenemy_95fireObjects2.createFrom(runtimeScene.getObjects("enemy_fire"));
gdjs.GameCode.GDlifeObjects2.length = 0;

{gdjs.evtTools.object.createObjectFromGroupOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlifeObjects2Objects, "life", (( gdjs.GameCode.GDenemy_95fireObjects2.length === 0 ) ? (( gdjs.GameCode.GDenemy1Objects2.length === 0 ) ? (( gdjs.GameCode.GDenemy2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects2[0].getPointX("")) :gdjs.GameCode.GDenemy1Objects2[0].getPointX("")) :gdjs.GameCode.GDenemy_95fireObjects2[0].getPointX("")), (( gdjs.GameCode.GDenemy_95fireObjects2.length === 0 ) ? (( gdjs.GameCode.GDenemy1Objects2.length === 0 ) ? (( gdjs.GameCode.GDenemy2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects2[0].getPointY("")) :gdjs.GameCode.GDenemy1Objects2[0].getPointY("")) :gdjs.GameCode.GDenemy_95fireObjects2[0].getPointY("")), "Enemys");
}}

}


{

gdjs.GameCode.GDenemy1Objects2.createFrom(gdjs.GameCode.GDenemy1Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy1Objects2[i].getAnimation() == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy1Objects2[k] = gdjs.GameCode.GDenemy1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy1Objects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDenemy1Objects2.createFrom(gdjs.GameCode.GDenemy1Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy1Objects2[i].getAnimation() == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy1Objects2[k] = gdjs.GameCode.GDenemy1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy1Objects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}}

}


{

gdjs.GameCode.GDenemy1Objects2.createFrom(gdjs.GameCode.GDenemy1Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy1Objects2[i].getAnimation() == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy1Objects2[k] = gdjs.GameCode.GDenemy1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy1Objects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).add(1);
}}

}


{


{
/* Reuse gdjs.GameCode.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.GameCode.eventsList0xaeb6b0
gdjs.GameCode.eventsList0xaeb420 = function(runtimeScene) {

{

/* Reuse gdjs.GameCode.GDenemy1Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy1Objects1[i].timerElapsedTime("enemy1_death", gdjs.random(3)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy1Objects1[k] = gdjs.GameCode.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy1Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy1Objects1 */
gdjs.GameCode.GDexploseObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects, (( gdjs.GameCode.GDenemy1Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy1Objects1[0].getPointX("")), (( gdjs.GameCode.GDenemy1Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy1Objects1[0].getPointY("")), "Enemys");
}{gdjs.evtTools.sound.playSound(runtimeScene, "shipexplode.ogg", false, 100, 1);
}
{ //Subevents
gdjs.GameCode.eventsList0xaeb6b0(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0xaeb420
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects = Hashtable.newFrom({"explose": gdjs.GameCode.GDexploseObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlifeObjects1Objects = Hashtable.newFrom({"life": gdjs.GameCode.GDlifeObjects1});gdjs.GameCode.eventsList0xaaa828 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = (gdjs.random(4) == 3);
}
}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy1Objects1 */
/* Reuse gdjs.GameCode.GDenemy2Objects1 */
gdjs.GameCode.GDenemy_95fireObjects1.createFrom(runtimeScene.getObjects("enemy_fire"));
gdjs.GameCode.GDlifeObjects1.length = 0;

{gdjs.evtTools.object.createObjectFromGroupOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlifeObjects1Objects, "life", (( gdjs.GameCode.GDenemy_95fireObjects1.length === 0 ) ? (( gdjs.GameCode.GDenemy1Objects1.length === 0 ) ? (( gdjs.GameCode.GDenemy2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects1[0].getPointX("")) :gdjs.GameCode.GDenemy1Objects1[0].getPointX("")) :gdjs.GameCode.GDenemy_95fireObjects1[0].getPointX("")), (( gdjs.GameCode.GDenemy_95fireObjects1.length === 0 ) ? (( gdjs.GameCode.GDenemy1Objects1.length === 0 ) ? (( gdjs.GameCode.GDenemy2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects1[0].getPointY("")) :gdjs.GameCode.GDenemy1Objects1[0].getPointY("")) :gdjs.GameCode.GDenemy_95fireObjects1[0].getPointY("")), "Enemys");
}}

}


}; //End of gdjs.GameCode.eventsList0xaaa828
gdjs.GameCode.eventsList0xaaa4f8 = function(runtimeScene) {

{

gdjs.GameCode.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
/* Reuse gdjs.GameCode.GDenemy2Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy2Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy2Objects1[i].getY() >= gdjs.evtTools.window.getWindowHeight() - (( gdjs.GameCode.GDenemy1Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy1Objects1[0].getHeight()) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy2Objects1[k] = gdjs.GameCode.GDenemy2Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy2Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy2Objects1 */
gdjs.GameCode.GDexploseObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects, (( gdjs.GameCode.GDenemy2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects1[0].getPointX("")), (( gdjs.GameCode.GDenemy2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects1[0].getPointY("")), "Enemys");
}{gdjs.evtTools.sound.playSound(runtimeScene, "shipexplode.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(4).add(1);
}{for(var i = 0, len = gdjs.GameCode.GDenemy2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy2Objects1[i].deleteFromScene(runtimeScene);
}
}
{ //Subevents
gdjs.GameCode.eventsList0xaaa828(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0xaaa4f8
gdjs.GameCode.eventsList0x9a0758 = function(runtimeScene) {

{

gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

gdjs.GameCode.GDplayer_95lifeObjects2.createFrom(runtimeScene.getObjects("player_life"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayer_95lifeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayer_95lifeObjects2[i].getVariableNumber(gdjs.GameCode.GDplayer_95lifeObjects2[i].getVariables().getFromIndex(0)) <= (gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDplayerObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDplayerObjects2[0].getVariables()).getFromIndex(0))) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayer_95lifeObjects2[k] = gdjs.GameCode.GDplayer_95lifeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayer_95lifeObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayer_95lifeObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDplayer_95lifeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayer_95lifeObjects2[i].setAnimation(0);
}
}}

}


{

gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

gdjs.GameCode.GDplayer_95lifeObjects2.createFrom(runtimeScene.getObjects("player_life"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayer_95lifeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayer_95lifeObjects2[i].getVariableNumber(gdjs.GameCode.GDplayer_95lifeObjects2[i].getVariables().getFromIndex(0)) > (gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDplayerObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDplayerObjects2[0].getVariables()).getFromIndex(0))) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayer_95lifeObjects2[k] = gdjs.GameCode.GDplayer_95lifeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDplayer_95lifeObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayer_95lifeObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDplayer_95lifeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayer_95lifeObjects2[i].setAnimation(1);
}
}}

}


{



}


{

gdjs.GameCode.GDalvosObjects2.createFrom(runtimeScene.getObjects("alvos"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvosObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvosObjects2[i].getAnimation() == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvosObjects2[k] = gdjs.GameCode.GDalvosObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvosObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvosObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvosObjects2[i].getVariableNumber(gdjs.GameCode.GDalvosObjects2[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvosObjects2[k] = gdjs.GameCode.GDalvosObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvosObjects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvosObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvosObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvosObjects2[i].setAnimation(0);
}
}}

}


{

gdjs.GameCode.GDalvosObjects2.createFrom(runtimeScene.getObjects("alvos"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvosObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvosObjects2[i].getAnimation() == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvosObjects2[k] = gdjs.GameCode.GDalvosObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvosObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvosObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvosObjects2[i].getVariableNumber(gdjs.GameCode.GDalvosObjects2[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvosObjects2[k] = gdjs.GameCode.GDalvosObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvosObjects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvosObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvosObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvosObjects2[i].setAnimation(0);
}
}}

}


{

gdjs.GameCode.GDalvosObjects2.createFrom(runtimeScene.getObjects("alvos"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvosObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvosObjects2[i].getAnimation() == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvosObjects2[k] = gdjs.GameCode.GDalvosObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvosObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvosObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvosObjects2[i].getVariableNumber(gdjs.GameCode.GDalvosObjects2[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvosObjects2[k] = gdjs.GameCode.GDalvosObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvosObjects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvosObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvosObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvosObjects2[i].setAnimation(0);
}
}}

}


{

gdjs.GameCode.GDalvosObjects2.createFrom(runtimeScene.getObjects("alvos"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvosObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvosObjects2[i].getAnimation() == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvosObjects2[k] = gdjs.GameCode.GDalvosObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvosObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvosObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvosObjects2[i].getVariableNumber(gdjs.GameCode.GDalvosObjects2[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvosObjects2[k] = gdjs.GameCode.GDalvosObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvosObjects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvosObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvosObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvosObjects2[i].setAnimation(0);
}
}}

}


{


{
{runtimeScene.getVariables().get("fim").setNumber(0);
}}

}


{

gdjs.GameCode.GDalvosObjects2.createFrom(runtimeScene.getObjects("alvos"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvosObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvosObjects2[i].getAnimation() != 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvosObjects2[k] = gdjs.GameCode.GDalvosObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvosObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("fim").add(1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("fim")) == 0;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "End", false);
}}

}


}; //End of gdjs.GameCode.eventsList0x9a0758
gdjs.GameCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbullet_95spawnObjects1.createFrom(runtimeScene.getObjects("bullet_spawn"));
gdjs.GameCode.GDmobile_95planeObjects1.createFrom(runtimeScene.getObjects("mobile_plane"));
{for(var i = 0, len = gdjs.GameCode.GDbullet_95spawnObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbullet_95spawnObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDmobile_95planeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmobile_95planeObjects1[i].hide();
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "soud_plane.ogg", true, 100, 1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "plane.ogg", true, 100, 1);
}{runtimeScene.getVariables().get("background").setNumber(gdjs.randomInRange(1,  8));
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(0);
}
{ //Subevents
gdjs.GameCode.eventsList0x6b0ca8(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getAngle() > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].setAngle(gdjs.GameCode.GDplayerObjects1[i].getAngle() - (1));
}
}}

}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getAngle() < 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].setAngle(gdjs.GameCode.GDplayerObjects1[i].getAngle() + (1));
}
}}

}


{



}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getVariableNumber(gdjs.GameCode.GDplayerObjects1[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x95ace0(runtimeScene);} //End of subevents
}

}


{



}


{


{
gdjs.GameCode.GDbulletObjects1.createFrom(runtimeScene.getObjects("bullet"));
{for(var i = 0, len = gdjs.GameCode.GDbulletObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbulletObjects1[i].addForce(90, 0, 1);
}
}}

}


{

gdjs.GameCode.GDbulletObjects1.length = 0;

gdjs.GameCode.GDenemy1Objects1.length = 0;

gdjs.GameCode.GDenemy2Objects1.length = 0;


gdjs.GameCode.condition0IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.GDbulletObjects1_1final.length = 0;gdjs.GameCode.GDenemy1Objects1_1final.length = 0;gdjs.GameCode.GDenemy2Objects1_1final.length = 0;gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.GDbulletObjects2.createFrom(runtimeScene.getObjects("bullet"));
gdjs.GameCode.GDenemy1Objects2.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbulletObjects2Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy1Objects2Objects, false, runtimeScene);
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDbulletObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDbulletObjects1_1final.indexOf(gdjs.GameCode.GDbulletObjects2[j]) === -1 )
            gdjs.GameCode.GDbulletObjects1_1final.push(gdjs.GameCode.GDbulletObjects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDenemy1Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDenemy1Objects1_1final.indexOf(gdjs.GameCode.GDenemy1Objects2[j]) === -1 )
            gdjs.GameCode.GDenemy1Objects1_1final.push(gdjs.GameCode.GDenemy1Objects2[j]);
    }
}
}
{
gdjs.GameCode.GDbulletObjects2.createFrom(runtimeScene.getObjects("bullet"));
gdjs.GameCode.GDenemy2Objects2.createFrom(runtimeScene.getObjects("enemy2"));
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbulletObjects2Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy2Objects2Objects, false, runtimeScene);
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDbulletObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDbulletObjects1_1final.indexOf(gdjs.GameCode.GDbulletObjects2[j]) === -1 )
            gdjs.GameCode.GDbulletObjects1_1final.push(gdjs.GameCode.GDbulletObjects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDenemy2Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDenemy2Objects1_1final.indexOf(gdjs.GameCode.GDenemy2Objects2[j]) === -1 )
            gdjs.GameCode.GDenemy2Objects1_1final.push(gdjs.GameCode.GDenemy2Objects2[j]);
    }
}
}
{
gdjs.GameCode.GDbulletObjects1.createFrom(gdjs.GameCode.GDbulletObjects1_1final);
gdjs.GameCode.GDenemy1Objects1.createFrom(gdjs.GameCode.GDenemy1Objects1_1final);
gdjs.GameCode.GDenemy2Objects1.createFrom(gdjs.GameCode.GDenemy2Objects1_1final);
}
}
}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbulletObjects1 */
/* Reuse gdjs.GameCode.GDenemy1Objects1 */
/* Reuse gdjs.GameCode.GDenemy2Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDbulletObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbulletObjects1[i].deleteFromScene(runtimeScene);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Laser_collision.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].returnVariable(gdjs.GameCode.GDenemy1Objects1[i].getVariables().getFromIndex(0)).sub(1);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy2Objects1[i].returnVariable(gdjs.GameCode.GDenemy2Objects1[i].getVariables().getFromIndex(0)).sub(1);
}
}}

}


{

gdjs.GameCode.GDbulletObjects1.createFrom(runtimeScene.getObjects("bullet"));
gdjs.GameCode.GDenemy_95fireObjects1.createFrom(runtimeScene.getObjects("enemy_fire"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbulletObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy_9595fireObjects1Objects, false, runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy_95fireObjects1 */
gdjs.GameCode.GDexploseObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects, (( gdjs.GameCode.GDenemy_95fireObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy_95fireObjects1[0].getPointX("")), (( gdjs.GameCode.GDenemy_95fireObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy_95fireObjects1[0].getPointY("")), "Enemys");
}{gdjs.evtTools.sound.playSound(runtimeScene, "shipexplode.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDenemy_95fireObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy_95fireObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.GameCode.GDenemy1Objects1.length = 0;

gdjs.GameCode.GDenemy2Objects1.length = 0;

gdjs.GameCode.GDenemy_95fireObjects1.length = 0;

gdjs.GameCode.GDplayerObjects1.length = 0;


gdjs.GameCode.condition0IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.GDenemy1Objects1_1final.length = 0;gdjs.GameCode.GDenemy2Objects1_1final.length = 0;gdjs.GameCode.GDenemy_95fireObjects1_1final.length = 0;gdjs.GameCode.GDplayerObjects1_1final.length = 0;gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
gdjs.GameCode.condition2IsTrue_1.val = false;
{
gdjs.GameCode.GDenemy_95fireObjects2.createFrom(runtimeScene.getObjects("enemy_fire"));
gdjs.GameCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy_9595fireObjects2Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects, false, runtimeScene);
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDenemy_95fireObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDenemy_95fireObjects1_1final.indexOf(gdjs.GameCode.GDenemy_95fireObjects2[j]) === -1 )
            gdjs.GameCode.GDenemy_95fireObjects1_1final.push(gdjs.GameCode.GDenemy_95fireObjects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDplayerObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDplayerObjects1_1final.indexOf(gdjs.GameCode.GDplayerObjects2[j]) === -1 )
            gdjs.GameCode.GDplayerObjects1_1final.push(gdjs.GameCode.GDplayerObjects2[j]);
    }
}
}
{
gdjs.GameCode.GDenemy1Objects2.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.GameCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy1Objects2Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects, false, runtimeScene);
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDenemy1Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDenemy1Objects1_1final.indexOf(gdjs.GameCode.GDenemy1Objects2[j]) === -1 )
            gdjs.GameCode.GDenemy1Objects1_1final.push(gdjs.GameCode.GDenemy1Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDplayerObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDplayerObjects1_1final.indexOf(gdjs.GameCode.GDplayerObjects2[j]) === -1 )
            gdjs.GameCode.GDplayerObjects1_1final.push(gdjs.GameCode.GDplayerObjects2[j]);
    }
}
}
{
gdjs.GameCode.GDenemy2Objects2.createFrom(runtimeScene.getObjects("enemy2"));
gdjs.GameCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));
gdjs.GameCode.condition2IsTrue_1.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy2Objects2Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects2Objects, false, runtimeScene);
if( gdjs.GameCode.condition2IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDenemy2Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDenemy2Objects1_1final.indexOf(gdjs.GameCode.GDenemy2Objects2[j]) === -1 )
            gdjs.GameCode.GDenemy2Objects1_1final.push(gdjs.GameCode.GDenemy2Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDplayerObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDplayerObjects1_1final.indexOf(gdjs.GameCode.GDplayerObjects2[j]) === -1 )
            gdjs.GameCode.GDplayerObjects1_1final.push(gdjs.GameCode.GDplayerObjects2[j]);
    }
}
}
{
gdjs.GameCode.GDenemy1Objects1.createFrom(gdjs.GameCode.GDenemy1Objects1_1final);
gdjs.GameCode.GDenemy2Objects1.createFrom(gdjs.GameCode.GDenemy2Objects1_1final);
gdjs.GameCode.GDenemy_95fireObjects1.createFrom(gdjs.GameCode.GDenemy_95fireObjects1_1final);
gdjs.GameCode.GDplayerObjects1.createFrom(gdjs.GameCode.GDplayerObjects1_1final);
}
}
}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy1Objects1 */
/* Reuse gdjs.GameCode.GDenemy2Objects1 */
/* Reuse gdjs.GameCode.GDenemy_95fireObjects1 */
/* Reuse gdjs.GameCode.GDplayerObjects1 */
gdjs.GameCode.GDexploseObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects, (( gdjs.GameCode.GDenemy_95fireObjects1.length === 0 ) ? (( gdjs.GameCode.GDenemy1Objects1.length === 0 ) ? (( gdjs.GameCode.GDenemy2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects1[0].getPointX("")) :gdjs.GameCode.GDenemy1Objects1[0].getPointX("")) :gdjs.GameCode.GDenemy_95fireObjects1[0].getPointX("")), (( gdjs.GameCode.GDenemy_95fireObjects1.length === 0 ) ? (( gdjs.GameCode.GDenemy1Objects1.length === 0 ) ? (( gdjs.GameCode.GDenemy2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects1[0].getPointY("")) :gdjs.GameCode.GDenemy1Objects1[0].getPointY("")) :gdjs.GameCode.GDenemy_95fireObjects1[0].getPointY("")), "Enemys");
}{gdjs.evtTools.sound.playSound(runtimeScene, "shipexplode.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].returnVariable(gdjs.GameCode.GDplayerObjects1[i].getVariables().getFromIndex(0)).sub(1);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy_95fireObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy_95fireObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy2Objects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.GameCode.GDexploseObjects1.createFrom(runtimeScene.getObjects("explose"));
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects, false, runtimeScene);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(11218700);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].returnVariable(gdjs.GameCode.GDplayerObjects1[i].getVariables().getFromIndex(0)).sub(1);
}
}}

}


{



}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getVariableNumber(gdjs.GameCode.GDplayerObjects1[i].getVariables().getFromIndex(0)) <= 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].setAnimation(2);
}
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].addForce(0, 300, 0);
}
}
{ //Subevents
gdjs.GameCode.eventsList0xaf9a10(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDlifeObjects1.createFrom(runtimeScene.getObjects("life"));
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDplayerObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlifeObjects1Objects, false, runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDlifeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDlifeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDlifeObjects1[i].deleteFromScene(runtimeScene);
}
}
{ //Subevents
gdjs.GameCode.eventsList0xaeade8(runtimeScene);} //End of subevents
}

}


{



}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, gdjs.randomInRange(3,  8), "enemy1");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDenemy1Objects1.length = 0;

{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "enemy1");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy1Objects1Objects, gdjs.evtTools.window.getWindowWidth() + gdjs.randomInRange(10,  200), gdjs.randomInRange(100,  gdjs.evtTools.window.getWindowHeight() - (( gdjs.GameCode.GDenemy1Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy1Objects1[0].getHeight()) - 100), "Enemys");
}{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].flipX(true);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].setAnimation(gdjs.random(2));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 30, "enemy1b");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDenemy1Objects1.length = 0;

{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "enemy1b");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy1Objects1Objects, gdjs.evtTools.window.getWindowWidth() + gdjs.randomInRange(10,  200), gdjs.randomInRange(100,  gdjs.evtTools.window.getWindowHeight() - (( gdjs.GameCode.GDenemy1Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy1Objects1[0].getHeight()) - 100), "Enemys");
}{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].flipX(true);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].setAnimation(gdjs.random(2));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, gdjs.randomInRange(7,  30), "enemy2");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDenemy2Objects1.length = 0;

{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "enemy2");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy2Objects1Objects, gdjs.evtTools.window.getWindowWidth() + gdjs.randomInRange(10,  200), gdjs.randomInRange(100,  gdjs.evtTools.window.getWindowHeight() - (( gdjs.GameCode.GDenemy2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects1[0].getHeight())), "Enemys");
}{for(var i = 0, len = gdjs.GameCode.GDenemy2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy2Objects1[i].flipX(true);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 30, "enemy2b");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDenemy2Objects1.length = 0;

{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "enemy2b");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemy2Objects1Objects, gdjs.evtTools.window.getWindowWidth() + gdjs.randomInRange(10,  200), gdjs.randomInRange(100,  gdjs.evtTools.window.getWindowHeight() - (( gdjs.GameCode.GDenemy2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy2Objects1[0].getHeight())), "Enemys");
}{for(var i = 0, len = gdjs.GameCode.GDenemy2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy2Objects1[i].flipX(true);
}
}}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.GameCode.GDenemy2Objects1.createFrom(runtimeScene.getObjects("enemy2"));
gdjs.GameCode.GDenemy_95fireObjects1.createFrom(runtimeScene.getObjects("enemy_fire"));
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].addForce(gdjs.randomInRange(200,  400)*-1, 0, 0);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy2Objects1[i].addForce(gdjs.randomInRange(100,  400)*-1, gdjs.randomInRange(-200,  200), 0);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy_95fireObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy_95fireObjects1[i].addForceTowardObject((gdjs.GameCode.GDplayerObjects1.length !== 0 ? gdjs.GameCode.GDplayerObjects1[0] : null), gdjs.randomInRange(90, 200), 0);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy_95fireObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy_95fireObjects1[i].rotateTowardPosition((( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointX("")), (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointY("")), 30, runtimeScene);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x6ae960(runtimeScene);} //End of subevents
}

}


{

gdjs.GameCode.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy1Objects1[i].getX() < -300 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy1Objects1[k] = gdjs.GameCode.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy1Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.GameCode.GDenemy2Objects1.createFrom(runtimeScene.getObjects("enemy2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy2Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy2Objects1[i].getX() < -300 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy2Objects1[k] = gdjs.GameCode.GDenemy2Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy2Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy2Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDenemy2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy2Objects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{

gdjs.GameCode.GDenemy_95fireObjects1.createFrom(runtimeScene.getObjects("enemy_fire"));
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy_95fireObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy_95fireObjects1[i].getX() > (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointX("")) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy_95fireObjects1[k] = gdjs.GameCode.GDenemy_95fireObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy_95fireObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy_95fireObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDenemy_95fireObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy_95fireObjects1[i].setAngle(-90);
}
}}

}


{

gdjs.GameCode.GDenemy_95fireObjects1.createFrom(runtimeScene.getObjects("enemy_fire"));
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy_95fireObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy_95fireObjects1[i].getX() < (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointX("")) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy_95fireObjects1[k] = gdjs.GameCode.GDenemy_95fireObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy_95fireObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy_95fireObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDenemy_95fireObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy_95fireObjects1[i].setAngle(90);
}
}}

}


{

gdjs.GameCode.GDenemy2Objects1.createFrom(runtimeScene.getObjects("enemy2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy2Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy2Objects1[i].getX() < gdjs.evtTools.window.getWindowWidth() - 200 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy2Objects1[k] = gdjs.GameCode.GDenemy2Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy2Objects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy2Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy2Objects1[i].getX() > 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDenemy2Objects1[k] = gdjs.GameCode.GDenemy2Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy2Objects1.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x685260(runtimeScene);} //End of subevents
}

}


{

gdjs.GameCode.GDenemy_95fireObjects1.createFrom(runtimeScene.getObjects("enemy_fire"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy_95fireObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy_95fireObjects1[i].timerElapsedTime("enemy2_fire_explode", gdjs.randomInRange(8,  10)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy_95fireObjects1[k] = gdjs.GameCode.GDenemy_95fireObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy_95fireObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy_95fireObjects1 */
gdjs.GameCode.GDexploseObjects1.length = 0;

{for(var i = 0, len = gdjs.GameCode.GDenemy_95fireObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy_95fireObjects1[i].resetTimer("enemy2_fire_explode");
}
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDexploseObjects1Objects, (( gdjs.GameCode.GDenemy_95fireObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy_95fireObjects1[0].getPointX("")), (( gdjs.GameCode.GDenemy_95fireObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDenemy_95fireObjects1[0].getPointY("")), "Enemys");
}{for(var i = 0, len = gdjs.GameCode.GDenemy_95fireObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy_95fireObjects1[i].deleteFromScene(runtimeScene);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "shipexplode.ogg", false, 100, 1);
}}

}


{

gdjs.GameCode.GDexploseObjects1.createFrom(runtimeScene.getObjects("explose"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDexploseObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDexploseObjects1[i].hasAnimationEnded() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDexploseObjects1[k] = gdjs.GameCode.GDexploseObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDexploseObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDexploseObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDexploseObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDexploseObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{

gdjs.GameCode.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy1Objects1[i].getVariableNumber(gdjs.GameCode.GDenemy1Objects1[i].getVariables().getFromIndex(0)) <= 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy1Objects1[k] = gdjs.GameCode.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy1Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].addForce(30, 300, 0);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy1Objects1[i].setAngle(-15);
}
}
{ //Subevents
gdjs.GameCode.eventsList0xaeb420(runtimeScene);} //End of subevents
}

}


{

gdjs.GameCode.GDenemy2Objects1.createFrom(runtimeScene.getObjects("enemy2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemy2Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemy2Objects1[i].getVariableNumber(gdjs.GameCode.GDenemy2Objects1[i].getVariables().getFromIndex(0)) <= 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemy2Objects1[k] = gdjs.GameCode.GDenemy2Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemy2Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemy2Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDenemy2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy2Objects1[i].addForce(30, 300, 0);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy2Objects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemy2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemy2Objects1[i].setAngle(-15);
}
}
{ //Subevents
gdjs.GameCode.eventsList0xaaa4f8(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbackground1Objects1.createFrom(runtimeScene.getObjects("background1"));
gdjs.GameCode.GDbackground2Objects1.createFrom(runtimeScene.getObjects("background2"));
gdjs.GameCode.GDbackground3Objects1.createFrom(runtimeScene.getObjects("background3"));
gdjs.GameCode.GDbackground4Objects1.createFrom(runtimeScene.getObjects("background4"));
gdjs.GameCode.GDbackground5Objects1.createFrom(runtimeScene.getObjects("background5"));
gdjs.GameCode.GDbackground6Objects1.createFrom(runtimeScene.getObjects("background6"));
gdjs.GameCode.GDbackground7Objects1.createFrom(runtimeScene.getObjects("background7"));
gdjs.GameCode.GDbackground8Objects1.createFrom(runtimeScene.getObjects("background8"));
gdjs.GameCode.GDbullet_95spawnObjects1.createFrom(runtimeScene.getObjects("bullet_spawn"));
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.GameCode.GDbackground1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDbackground1Objects1[i].setXOffset(gdjs.GameCode.GDbackground1Objects1[i].getXOffset() + (1));
}
}{for(var i = 0, len = gdjs.GameCode.GDbackground2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDbackground2Objects1[i].setXOffset(gdjs.GameCode.GDbackground2Objects1[i].getXOffset() + (1));
}
}{for(var i = 0, len = gdjs.GameCode.GDbackground3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDbackground3Objects1[i].setXOffset(gdjs.GameCode.GDbackground3Objects1[i].getXOffset() + (1));
}
}{for(var i = 0, len = gdjs.GameCode.GDbackground4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDbackground4Objects1[i].setXOffset(gdjs.GameCode.GDbackground4Objects1[i].getXOffset() + (1));
}
}{for(var i = 0, len = gdjs.GameCode.GDbackground5Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDbackground5Objects1[i].setXOffset(gdjs.GameCode.GDbackground5Objects1[i].getXOffset() + (1));
}
}{for(var i = 0, len = gdjs.GameCode.GDbackground6Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDbackground6Objects1[i].setXOffset(gdjs.GameCode.GDbackground6Objects1[i].getXOffset() + (1));
}
}{for(var i = 0, len = gdjs.GameCode.GDbackground7Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDbackground7Objects1[i].setXOffset(gdjs.GameCode.GDbackground7Objects1[i].getXOffset() + (1));
}
}{for(var i = 0, len = gdjs.GameCode.GDbackground8Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDbackground8Objects1[i].setXOffset(gdjs.GameCode.GDbackground8Objects1[i].getXOffset() + (1));
}
}{for(var i = 0, len = gdjs.GameCode.GDbullet_95spawnObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbullet_95spawnObjects1[i].setX((( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointX("")) + ((( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getWidth())*0.7));
}
}{for(var i = 0, len = gdjs.GameCode.GDbullet_95spawnObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbullet_95spawnObjects1[i].setY((( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointY("")) + ((( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getHeight()) * 0.55));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x9a0758(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0xaff48


gdjs.GameCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.GameCode.GDbuttomObjects1.length = 0;
gdjs.GameCode.GDbuttomObjects2.length = 0;
gdjs.GameCode.GDbuttomObjects3.length = 0;
gdjs.GameCode.GDbackground8Objects1.length = 0;
gdjs.GameCode.GDbackground8Objects2.length = 0;
gdjs.GameCode.GDbackground8Objects3.length = 0;
gdjs.GameCode.GDbackground7Objects1.length = 0;
gdjs.GameCode.GDbackground7Objects2.length = 0;
gdjs.GameCode.GDbackground7Objects3.length = 0;
gdjs.GameCode.GDbackground6Objects1.length = 0;
gdjs.GameCode.GDbackground6Objects2.length = 0;
gdjs.GameCode.GDbackground6Objects3.length = 0;
gdjs.GameCode.GDbackground5Objects1.length = 0;
gdjs.GameCode.GDbackground5Objects2.length = 0;
gdjs.GameCode.GDbackground5Objects3.length = 0;
gdjs.GameCode.GDbackground4Objects1.length = 0;
gdjs.GameCode.GDbackground4Objects2.length = 0;
gdjs.GameCode.GDbackground4Objects3.length = 0;
gdjs.GameCode.GDbackground3Objects1.length = 0;
gdjs.GameCode.GDbackground3Objects2.length = 0;
gdjs.GameCode.GDbackground3Objects3.length = 0;
gdjs.GameCode.GDbackground2Objects1.length = 0;
gdjs.GameCode.GDbackground2Objects2.length = 0;
gdjs.GameCode.GDbackground2Objects3.length = 0;
gdjs.GameCode.GDbackground1Objects1.length = 0;
gdjs.GameCode.GDbackground1Objects2.length = 0;
gdjs.GameCode.GDbackground1Objects3.length = 0;
gdjs.GameCode.GDplayerObjects1.length = 0;
gdjs.GameCode.GDplayerObjects2.length = 0;
gdjs.GameCode.GDplayerObjects3.length = 0;
gdjs.GameCode.GDbulletObjects1.length = 0;
gdjs.GameCode.GDbulletObjects2.length = 0;
gdjs.GameCode.GDbulletObjects3.length = 0;
gdjs.GameCode.GDenemy2Objects1.length = 0;
gdjs.GameCode.GDenemy2Objects2.length = 0;
gdjs.GameCode.GDenemy2Objects3.length = 0;
gdjs.GameCode.GDenemy1Objects1.length = 0;
gdjs.GameCode.GDenemy1Objects2.length = 0;
gdjs.GameCode.GDenemy1Objects3.length = 0;
gdjs.GameCode.GDmobile_95dirObjects1.length = 0;
gdjs.GameCode.GDmobile_95dirObjects2.length = 0;
gdjs.GameCode.GDmobile_95dirObjects3.length = 0;
gdjs.GameCode.GDmobile_95planeObjects1.length = 0;
gdjs.GameCode.GDmobile_95planeObjects2.length = 0;
gdjs.GameCode.GDmobile_95planeObjects3.length = 0;
gdjs.GameCode.GDmobile_95shotObjects1.length = 0;
gdjs.GameCode.GDmobile_95shotObjects2.length = 0;
gdjs.GameCode.GDmobile_95shotObjects3.length = 0;
gdjs.GameCode.GDbullet_95spawnObjects1.length = 0;
gdjs.GameCode.GDbullet_95spawnObjects2.length = 0;
gdjs.GameCode.GDbullet_95spawnObjects3.length = 0;
gdjs.GameCode.GDenemy_95fireObjects1.length = 0;
gdjs.GameCode.GDenemy_95fireObjects2.length = 0;
gdjs.GameCode.GDenemy_95fireObjects3.length = 0;
gdjs.GameCode.GDexploseObjects1.length = 0;
gdjs.GameCode.GDexploseObjects2.length = 0;
gdjs.GameCode.GDexploseObjects3.length = 0;
gdjs.GameCode.GDplayer_95lifeObjects1.length = 0;
gdjs.GameCode.GDplayer_95lifeObjects2.length = 0;
gdjs.GameCode.GDplayer_95lifeObjects3.length = 0;
gdjs.GameCode.GDdisplay_95lifeObjects1.length = 0;
gdjs.GameCode.GDdisplay_95lifeObjects2.length = 0;
gdjs.GameCode.GDdisplay_95lifeObjects3.length = 0;
gdjs.GameCode.GDlifeObjects1.length = 0;
gdjs.GameCode.GDlifeObjects2.length = 0;
gdjs.GameCode.GDlifeObjects3.length = 0;
gdjs.GameCode.GDalvosObjects1.length = 0;
gdjs.GameCode.GDalvosObjects2.length = 0;
gdjs.GameCode.GDalvosObjects3.length = 0;
gdjs.GameCode.GDlblMissaoObjects1.length = 0;
gdjs.GameCode.GDlblMissaoObjects2.length = 0;
gdjs.GameCode.GDlblMissaoObjects3.length = 0;

gdjs.GameCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['GameCode'] = gdjs.GameCode;
