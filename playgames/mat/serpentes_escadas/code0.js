gdjs.StartCode = {};
gdjs.StartCode.GDinputTextObjects1= [];
gdjs.StartCode.GDinputTextObjects2= [];
gdjs.StartCode.GDtextoObjects1= [];
gdjs.StartCode.GDtextoObjects2= [];
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDBoardObjects1= [];
gdjs.StartCode.GDBoardObjects2= [];
gdjs.StartCode.GDtxtJogarObjects1= [];
gdjs.StartCode.GDtxtJogarObjects2= [];
gdjs.StartCode.GDtxtRegrasObjects1= [];
gdjs.StartCode.GDtxtRegrasObjects2= [];
gdjs.StartCode.GDbtRegrasObjects1= [];
gdjs.StartCode.GDbtRegrasObjects2= [];
gdjs.StartCode.GDbtJogarObjects1= [];
gdjs.StartCode.GDbtJogarObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};
gdjs.StartCode.condition3IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtRegrasObjects1Objects = Hashtable.newFrom({"btRegras": gdjs.StartCode.GDbtRegrasObjects1});gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtJogarObjects1Objects = Hashtable.newFrom({"btJogar": gdjs.StartCode.GDbtJogarObjects1});gdjs.StartCode.eventsList0xaf580 = function(runtimeScene, context) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "Dungeon Theme.ogg", true, 100, 1);
}}

}


{

gdjs.StartCode.GDtxtJogarObjects1.createFrom(runtimeScene.getObjects("txtJogar"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.5, "piscar");
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDtxtJogarObjects1.length;i<l;++i) {
    if ( gdjs.StartCode.GDtxtJogarObjects1[i].isVisible() ) {
        gdjs.StartCode.condition1IsTrue_0.val = true;
        gdjs.StartCode.GDtxtJogarObjects1[k] = gdjs.StartCode.GDtxtJogarObjects1[i];
        ++k;
    }
}
gdjs.StartCode.GDtxtJogarObjects1.length = k;}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDtxtJogarObjects1 */
{for(var i = 0, len = gdjs.StartCode.GDtxtJogarObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDtxtJogarObjects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "piscar");
}}

}


{

gdjs.StartCode.GDtxtJogarObjects1.createFrom(runtimeScene.getObjects("txtJogar"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.5, "piscar");
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDtxtJogarObjects1.length;i<l;++i) {
    if ( !(gdjs.StartCode.GDtxtJogarObjects1[i].isVisible()) ) {
        gdjs.StartCode.condition1IsTrue_0.val = true;
        gdjs.StartCode.GDtxtJogarObjects1[k] = gdjs.StartCode.GDtxtJogarObjects1[i];
        ++k;
    }
}
gdjs.StartCode.GDtxtJogarObjects1.length = k;}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDtxtJogarObjects1 */
{for(var i = 0, len = gdjs.StartCode.GDtxtJogarObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDtxtJogarObjects1[i].hide(false);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "piscar");
}}

}


{

gdjs.StartCode.GDbtRegrasObjects1.createFrom(runtimeScene.getObjects("btRegras"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtRegrasObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Regras", false);
}}

}


{

gdjs.StartCode.GDbtJogarObjects1.createFrom(runtimeScene.getObjects("btJogar"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtJogarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = context.triggerOnce(6616708);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Players", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xaf580


gdjs.StartCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.StartCode.GDinputTextObjects1.length = 0;
gdjs.StartCode.GDinputTextObjects2.length = 0;
gdjs.StartCode.GDtextoObjects1.length = 0;
gdjs.StartCode.GDtextoObjects2.length = 0;
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDBoardObjects1.length = 0;
gdjs.StartCode.GDBoardObjects2.length = 0;
gdjs.StartCode.GDtxtJogarObjects1.length = 0;
gdjs.StartCode.GDtxtJogarObjects2.length = 0;
gdjs.StartCode.GDtxtRegrasObjects1.length = 0;
gdjs.StartCode.GDtxtRegrasObjects2.length = 0;
gdjs.StartCode.GDbtRegrasObjects1.length = 0;
gdjs.StartCode.GDbtRegrasObjects2.length = 0;
gdjs.StartCode.GDbtJogarObjects1.length = 0;
gdjs.StartCode.GDbtJogarObjects2.length = 0;

gdjs.StartCode.eventsList0xaf580(runtimeScene, context);return;
}
gdjs['StartCode']= gdjs.StartCode;
