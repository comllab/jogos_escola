gdjs.PlayersCode = {};
gdjs.PlayersCode.GDplayer1Objects1= [];
gdjs.PlayersCode.GDplayer1Objects2= [];
gdjs.PlayersCode.GDplayer1Objects3= [];
gdjs.PlayersCode.GDtxtJogarObjects1= [];
gdjs.PlayersCode.GDtxtJogarObjects2= [];
gdjs.PlayersCode.GDtxtJogarObjects3= [];
gdjs.PlayersCode.GDbtJogarObjects1= [];
gdjs.PlayersCode.GDbtJogarObjects2= [];
gdjs.PlayersCode.GDbtJogarObjects3= [];
gdjs.PlayersCode.GDplayer2Objects1= [];
gdjs.PlayersCode.GDplayer2Objects2= [];
gdjs.PlayersCode.GDplayer2Objects3= [];
gdjs.PlayersCode.GDNewObjectObjects1= [];
gdjs.PlayersCode.GDNewObjectObjects2= [];
gdjs.PlayersCode.GDNewObjectObjects3= [];
gdjs.PlayersCode.GDtxtJogador2Objects1= [];
gdjs.PlayersCode.GDtxtJogador2Objects2= [];
gdjs.PlayersCode.GDtxtJogador2Objects3= [];
gdjs.PlayersCode.GDtxtJogador1Objects1= [];
gdjs.PlayersCode.GDtxtJogador1Objects2= [];
gdjs.PlayersCode.GDtxtJogador1Objects3= [];
gdjs.PlayersCode.GDNewObject2Objects1= [];
gdjs.PlayersCode.GDNewObject2Objects2= [];
gdjs.PlayersCode.GDNewObject2Objects3= [];
gdjs.PlayersCode.GDNewObject3Objects1= [];
gdjs.PlayersCode.GDNewObject3Objects2= [];
gdjs.PlayersCode.GDNewObject3Objects3= [];
gdjs.PlayersCode.GDinputObjects1= [];
gdjs.PlayersCode.GDinputObjects2= [];
gdjs.PlayersCode.GDinputObjects3= [];
gdjs.PlayersCode.GDnomePlayer2Objects1= [];
gdjs.PlayersCode.GDnomePlayer2Objects2= [];
gdjs.PlayersCode.GDnomePlayer2Objects3= [];
gdjs.PlayersCode.GDnomePlayer1Objects1= [];
gdjs.PlayersCode.GDnomePlayer1Objects2= [];
gdjs.PlayersCode.GDnomePlayer1Objects3= [];

gdjs.PlayersCode.conditionTrue_0 = {val:false};
gdjs.PlayersCode.condition0IsTrue_0 = {val:false};
gdjs.PlayersCode.condition1IsTrue_0 = {val:false};
gdjs.PlayersCode.condition2IsTrue_0 = {val:false};
gdjs.PlayersCode.condition3IsTrue_0 = {val:false};
gdjs.PlayersCode.conditionTrue_1 = {val:false};
gdjs.PlayersCode.condition0IsTrue_1 = {val:false};
gdjs.PlayersCode.condition1IsTrue_1 = {val:false};
gdjs.PlayersCode.condition2IsTrue_1 = {val:false};
gdjs.PlayersCode.condition3IsTrue_1 = {val:false};


gdjs.PlayersCode.mapOfGDgdjs_46PlayersCode_46GDplayer1Objects1Objects = Hashtable.newFrom({"player1": gdjs.PlayersCode.GDplayer1Objects1});gdjs.PlayersCode.mapOfGDgdjs_46PlayersCode_46GDplayer2Objects1Objects = Hashtable.newFrom({"player2": gdjs.PlayersCode.GDplayer2Objects1});gdjs.PlayersCode.eventsList0x6b42e0 = function(runtimeScene, context) {

{

gdjs.PlayersCode.GDtxtJogador1Objects2.createFrom(runtimeScene.getObjects("txtJogador1"));

gdjs.PlayersCode.condition0IsTrue_0.val = false;
gdjs.PlayersCode.condition1IsTrue_0.val = false;
{
gdjs.PlayersCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
}if ( gdjs.PlayersCode.condition0IsTrue_0.val ) {
{
{gdjs.PlayersCode.conditionTrue_1 = gdjs.PlayersCode.condition1IsTrue_0;
gdjs.PlayersCode.conditionTrue_1.val = (gdjs.evtTools.string.strLen((( gdjs.PlayersCode.GDtxtJogador1Objects2.length === 0 ) ? "" :gdjs.PlayersCode.GDtxtJogador1Objects2[0].getString())) <= 10);
}
}}
if (gdjs.PlayersCode.condition1IsTrue_0.val) {
gdjs.PlayersCode.GDinputObjects2.createFrom(gdjs.PlayersCode.GDinputObjects1);

/* Reuse gdjs.PlayersCode.GDtxtJogador1Objects2 */
{for(var i = 0, len = gdjs.PlayersCode.GDtxtJogador1Objects2.length ;i < len;++i) {
    gdjs.PlayersCode.GDtxtJogador1Objects2[i].setString((( gdjs.PlayersCode.GDinputObjects2.length === 0 ) ? "" :gdjs.PlayersCode.GDinputObjects2[0].getString()));
}
}}

}


{

gdjs.PlayersCode.GDtxtJogador2Objects1.createFrom(runtimeScene.getObjects("txtJogador2"));

gdjs.PlayersCode.condition0IsTrue_0.val = false;
gdjs.PlayersCode.condition1IsTrue_0.val = false;
{
gdjs.PlayersCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 2;
}if ( gdjs.PlayersCode.condition0IsTrue_0.val ) {
{
{gdjs.PlayersCode.conditionTrue_1 = gdjs.PlayersCode.condition1IsTrue_0;
gdjs.PlayersCode.conditionTrue_1.val = (gdjs.evtTools.string.strLen((( gdjs.PlayersCode.GDtxtJogador2Objects1.length === 0 ) ? "" :gdjs.PlayersCode.GDtxtJogador2Objects1[0].getString())) <= 10);
}
}}
if (gdjs.PlayersCode.condition1IsTrue_0.val) {
/* Reuse gdjs.PlayersCode.GDinputObjects1 */
/* Reuse gdjs.PlayersCode.GDtxtJogador2Objects1 */
{for(var i = 0, len = gdjs.PlayersCode.GDtxtJogador2Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDtxtJogador2Objects1[i].setString((( gdjs.PlayersCode.GDinputObjects1.length === 0 ) ? "" :gdjs.PlayersCode.GDinputObjects1[0].getString()));
}
}}

}


}; //End of gdjs.PlayersCode.eventsList0x6b42e0
gdjs.PlayersCode.mapOfGDgdjs_46PlayersCode_46GDbtJogarObjects1Objects = Hashtable.newFrom({"btJogar": gdjs.PlayersCode.GDbtJogarObjects1});gdjs.PlayersCode.eventsList0x6b49c0 = function(runtimeScene, context) {

{

gdjs.PlayersCode.GDtxtJogador1Objects2.createFrom(runtimeScene.getObjects("txtJogador1"));

gdjs.PlayersCode.condition0IsTrue_0.val = false;
{
{gdjs.PlayersCode.conditionTrue_1 = gdjs.PlayersCode.condition0IsTrue_0;
gdjs.PlayersCode.conditionTrue_1.val = (gdjs.evtTools.string.strLen((( gdjs.PlayersCode.GDtxtJogador1Objects2.length === 0 ) ? "" :gdjs.PlayersCode.GDtxtJogador1Objects2[0].getString())) >= 3);
}
}if (gdjs.PlayersCode.condition0IsTrue_0.val) {
/* Reuse gdjs.PlayersCode.GDtxtJogador1Objects2 */
{runtimeScene.getGame().getVariables().getFromIndex(0).setString((( gdjs.PlayersCode.GDtxtJogador1Objects2.length === 0 ) ? "" :gdjs.PlayersCode.GDtxtJogador1Objects2[0].getString()));
}}

}


{

gdjs.PlayersCode.GDtxtJogador2Objects2.createFrom(runtimeScene.getObjects("txtJogador2"));

gdjs.PlayersCode.condition0IsTrue_0.val = false;
{
{gdjs.PlayersCode.conditionTrue_1 = gdjs.PlayersCode.condition0IsTrue_0;
gdjs.PlayersCode.conditionTrue_1.val = (gdjs.evtTools.string.strLen((( gdjs.PlayersCode.GDtxtJogador2Objects2.length === 0 ) ? "" :gdjs.PlayersCode.GDtxtJogador2Objects2[0].getString())) >= 3);
}
}if (gdjs.PlayersCode.condition0IsTrue_0.val) {
/* Reuse gdjs.PlayersCode.GDtxtJogador2Objects2 */
{runtimeScene.getGame().getVariables().getFromIndex(1).setString((( gdjs.PlayersCode.GDtxtJogador2Objects2.length === 0 ) ? "" :gdjs.PlayersCode.GDtxtJogador2Objects2[0].getString()));
}}

}


{


{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Board_" + gdjs.evtTools.common.toString(gdjs.random(4) + 1), false);
}}

}


}; //End of gdjs.PlayersCode.eventsList0x6b49c0
gdjs.PlayersCode.eventsList0xaf580 = function(runtimeScene, context) {

{


gdjs.PlayersCode.condition0IsTrue_0.val = false;
{
gdjs.PlayersCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.PlayersCode.condition0IsTrue_0.val) {
gdjs.PlayersCode.GDnomePlayer1Objects1.createFrom(runtimeScene.getObjects("nomePlayer1"));
gdjs.PlayersCode.GDnomePlayer2Objects1.createFrom(runtimeScene.getObjects("nomePlayer2"));
gdjs.PlayersCode.GDtxtJogador1Objects1.createFrom(runtimeScene.getObjects("txtJogador1"));
gdjs.PlayersCode.GDtxtJogador2Objects1.createFrom(runtimeScene.getObjects("txtJogador2"));
{for(var i = 0, len = gdjs.PlayersCode.GDtxtJogador1Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDtxtJogador1Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.PlayersCode.GDtxtJogador2Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDtxtJogador2Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}{for(var i = 0, len = gdjs.PlayersCode.GDnomePlayer1Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDnomePlayer1Objects1[i].hide();
}
}{for(var i = 0, len = gdjs.PlayersCode.GDnomePlayer2Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDnomePlayer2Objects1[i].hide();
}
}}

}


{


gdjs.PlayersCode.condition0IsTrue_0.val = false;
{
gdjs.PlayersCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
}if (gdjs.PlayersCode.condition0IsTrue_0.val) {
gdjs.PlayersCode.GDtxtJogador1Objects1.createFrom(runtimeScene.getObjects("txtJogador1"));
gdjs.PlayersCode.GDtxtJogador2Objects1.createFrom(runtimeScene.getObjects("txtJogador2"));
{for(var i = 0, len = gdjs.PlayersCode.GDtxtJogador1Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDtxtJogador1Objects1[i].setColor("255;0;0");
}
}{for(var i = 0, len = gdjs.PlayersCode.GDtxtJogador2Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDtxtJogador2Objects1[i].setColor("255;255;255");
}
}}

}


{


gdjs.PlayersCode.condition0IsTrue_0.val = false;
{
gdjs.PlayersCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 2;
}if (gdjs.PlayersCode.condition0IsTrue_0.val) {
gdjs.PlayersCode.GDtxtJogador1Objects1.createFrom(runtimeScene.getObjects("txtJogador1"));
gdjs.PlayersCode.GDtxtJogador2Objects1.createFrom(runtimeScene.getObjects("txtJogador2"));
{for(var i = 0, len = gdjs.PlayersCode.GDtxtJogador2Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDtxtJogador2Objects1[i].setColor("255;0;0");
}
}{for(var i = 0, len = gdjs.PlayersCode.GDtxtJogador1Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDtxtJogador1Objects1[i].setColor("255;255;255");
}
}}

}


{

gdjs.PlayersCode.GDplayer1Objects1.createFrom(runtimeScene.getObjects("player1"));

gdjs.PlayersCode.condition0IsTrue_0.val = false;
gdjs.PlayersCode.condition1IsTrue_0.val = false;
{
gdjs.PlayersCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.PlayersCode.mapOfGDgdjs_46PlayersCode_46GDplayer1Objects1Objects, runtimeScene, true, false);
}if ( gdjs.PlayersCode.condition0IsTrue_0.val ) {
{
gdjs.PlayersCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.PlayersCode.condition1IsTrue_0.val) {
gdjs.PlayersCode.GDinputObjects1.createFrom(runtimeScene.getObjects("input"));
gdjs.PlayersCode.GDnomePlayer1Objects1.createFrom(runtimeScene.getObjects("nomePlayer1"));
gdjs.PlayersCode.GDnomePlayer2Objects1.createFrom(runtimeScene.getObjects("nomePlayer2"));
gdjs.PlayersCode.GDtxtJogador1Objects1.createFrom(runtimeScene.getObjects("txtJogador1"));
{for(var i = 0, len = gdjs.PlayersCode.GDinputObjects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDinputObjects1[i].setString("");
}
}{runtimeScene.getVariables().getFromIndex(0).setNumber(1);
}{for(var i = 0, len = gdjs.PlayersCode.GDnomePlayer1Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDnomePlayer1Objects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.PlayersCode.GDnomePlayer2Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDnomePlayer2Objects1[i].hide();
}
}{for(var i = 0, len = gdjs.PlayersCode.GDtxtJogador1Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDtxtJogador1Objects1[i].setString((( gdjs.PlayersCode.GDinputObjects1.length === 0 ) ? "" :gdjs.PlayersCode.GDinputObjects1[0].getString()));
}
}}

}


{

gdjs.PlayersCode.GDplayer2Objects1.createFrom(runtimeScene.getObjects("player2"));

gdjs.PlayersCode.condition0IsTrue_0.val = false;
gdjs.PlayersCode.condition1IsTrue_0.val = false;
{
gdjs.PlayersCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.PlayersCode.mapOfGDgdjs_46PlayersCode_46GDplayer2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.PlayersCode.condition0IsTrue_0.val ) {
{
gdjs.PlayersCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.PlayersCode.condition1IsTrue_0.val) {
gdjs.PlayersCode.GDinputObjects1.createFrom(runtimeScene.getObjects("input"));
gdjs.PlayersCode.GDnomePlayer1Objects1.createFrom(runtimeScene.getObjects("nomePlayer1"));
gdjs.PlayersCode.GDnomePlayer2Objects1.createFrom(runtimeScene.getObjects("nomePlayer2"));
gdjs.PlayersCode.GDtxtJogador2Objects1.createFrom(runtimeScene.getObjects("txtJogador2"));
{for(var i = 0, len = gdjs.PlayersCode.GDinputObjects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDinputObjects1[i].setString("");
}
}{runtimeScene.getVariables().getFromIndex(0).setNumber(2);
}{for(var i = 0, len = gdjs.PlayersCode.GDnomePlayer1Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDnomePlayer1Objects1[i].hide();
}
}{for(var i = 0, len = gdjs.PlayersCode.GDnomePlayer2Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDnomePlayer2Objects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.PlayersCode.GDtxtJogador2Objects1.length ;i < len;++i) {
    gdjs.PlayersCode.GDtxtJogador2Objects1[i].setString((( gdjs.PlayersCode.GDinputObjects1.length === 0 ) ? "" :gdjs.PlayersCode.GDinputObjects1[0].getString()));
}
}}

}


{

gdjs.PlayersCode.GDinputObjects1.createFrom(runtimeScene.getObjects("input"));

gdjs.PlayersCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.PlayersCode.GDinputObjects1.length;i<l;++i) {
    if ( gdjs.PlayersCode.GDinputObjects1[i].isActivated() ) {
        gdjs.PlayersCode.condition0IsTrue_0.val = true;
        gdjs.PlayersCode.GDinputObjects1[k] = gdjs.PlayersCode.GDinputObjects1[i];
        ++k;
    }
}
gdjs.PlayersCode.GDinputObjects1.length = k;}if (gdjs.PlayersCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.PlayersCode.eventsList0x6b42e0(runtimeScene, context);} //End of subevents
}

}


{

gdjs.PlayersCode.GDbtJogarObjects1.createFrom(runtimeScene.getObjects("btJogar"));

gdjs.PlayersCode.condition0IsTrue_0.val = false;
gdjs.PlayersCode.condition1IsTrue_0.val = false;
gdjs.PlayersCode.condition2IsTrue_0.val = false;
{
gdjs.PlayersCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.PlayersCode.mapOfGDgdjs_46PlayersCode_46GDbtJogarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.PlayersCode.condition0IsTrue_0.val ) {
{
gdjs.PlayersCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.PlayersCode.condition1IsTrue_0.val ) {
{
{gdjs.PlayersCode.conditionTrue_1 = gdjs.PlayersCode.condition2IsTrue_0;
gdjs.PlayersCode.conditionTrue_1.val = context.triggerOnce(7031668);
}
}}
}
if (gdjs.PlayersCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.PlayersCode.eventsList0x6b49c0(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.PlayersCode.eventsList0xaf580


gdjs.PlayersCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.PlayersCode.GDplayer1Objects1.length = 0;
gdjs.PlayersCode.GDplayer1Objects2.length = 0;
gdjs.PlayersCode.GDplayer1Objects3.length = 0;
gdjs.PlayersCode.GDtxtJogarObjects1.length = 0;
gdjs.PlayersCode.GDtxtJogarObjects2.length = 0;
gdjs.PlayersCode.GDtxtJogarObjects3.length = 0;
gdjs.PlayersCode.GDbtJogarObjects1.length = 0;
gdjs.PlayersCode.GDbtJogarObjects2.length = 0;
gdjs.PlayersCode.GDbtJogarObjects3.length = 0;
gdjs.PlayersCode.GDplayer2Objects1.length = 0;
gdjs.PlayersCode.GDplayer2Objects2.length = 0;
gdjs.PlayersCode.GDplayer2Objects3.length = 0;
gdjs.PlayersCode.GDNewObjectObjects1.length = 0;
gdjs.PlayersCode.GDNewObjectObjects2.length = 0;
gdjs.PlayersCode.GDNewObjectObjects3.length = 0;
gdjs.PlayersCode.GDtxtJogador2Objects1.length = 0;
gdjs.PlayersCode.GDtxtJogador2Objects2.length = 0;
gdjs.PlayersCode.GDtxtJogador2Objects3.length = 0;
gdjs.PlayersCode.GDtxtJogador1Objects1.length = 0;
gdjs.PlayersCode.GDtxtJogador1Objects2.length = 0;
gdjs.PlayersCode.GDtxtJogador1Objects3.length = 0;
gdjs.PlayersCode.GDNewObject2Objects1.length = 0;
gdjs.PlayersCode.GDNewObject2Objects2.length = 0;
gdjs.PlayersCode.GDNewObject2Objects3.length = 0;
gdjs.PlayersCode.GDNewObject3Objects1.length = 0;
gdjs.PlayersCode.GDNewObject3Objects2.length = 0;
gdjs.PlayersCode.GDNewObject3Objects3.length = 0;
gdjs.PlayersCode.GDinputObjects1.length = 0;
gdjs.PlayersCode.GDinputObjects2.length = 0;
gdjs.PlayersCode.GDinputObjects3.length = 0;
gdjs.PlayersCode.GDnomePlayer2Objects1.length = 0;
gdjs.PlayersCode.GDnomePlayer2Objects2.length = 0;
gdjs.PlayersCode.GDnomePlayer2Objects3.length = 0;
gdjs.PlayersCode.GDnomePlayer1Objects1.length = 0;
gdjs.PlayersCode.GDnomePlayer1Objects2.length = 0;
gdjs.PlayersCode.GDnomePlayer1Objects3.length = 0;

gdjs.PlayersCode.eventsList0xaf580(runtimeScene, context);return;
}
gdjs['PlayersCode']= gdjs.PlayersCode;
