gdjs.RegrasCode = {};
gdjs.RegrasCode.GDbtVoltarObjects1= [];
gdjs.RegrasCode.GDbtVoltarObjects2= [];
gdjs.RegrasCode.GDNewObjectObjects1= [];
gdjs.RegrasCode.GDNewObjectObjects2= [];
gdjs.RegrasCode.GDNewObject2Objects1= [];
gdjs.RegrasCode.GDNewObject2Objects2= [];

gdjs.RegrasCode.conditionTrue_0 = {val:false};
gdjs.RegrasCode.condition0IsTrue_0 = {val:false};
gdjs.RegrasCode.condition1IsTrue_0 = {val:false};
gdjs.RegrasCode.condition2IsTrue_0 = {val:false};


gdjs.RegrasCode.mapOfGDgdjs_46RegrasCode_46GDbtVoltarObjects1Objects = Hashtable.newFrom({"btVoltar": gdjs.RegrasCode.GDbtVoltarObjects1});gdjs.RegrasCode.eventsList0xaf580 = function(runtimeScene, context) {

{

gdjs.RegrasCode.GDbtVoltarObjects1.createFrom(runtimeScene.getObjects("btVoltar"));

gdjs.RegrasCode.condition0IsTrue_0.val = false;
gdjs.RegrasCode.condition1IsTrue_0.val = false;
{
gdjs.RegrasCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.RegrasCode.mapOfGDgdjs_46RegrasCode_46GDbtVoltarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.RegrasCode.condition0IsTrue_0.val ) {
{
gdjs.RegrasCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.RegrasCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


}; //End of gdjs.RegrasCode.eventsList0xaf580


gdjs.RegrasCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.RegrasCode.GDbtVoltarObjects1.length = 0;
gdjs.RegrasCode.GDbtVoltarObjects2.length = 0;
gdjs.RegrasCode.GDNewObjectObjects1.length = 0;
gdjs.RegrasCode.GDNewObjectObjects2.length = 0;
gdjs.RegrasCode.GDNewObject2Objects1.length = 0;
gdjs.RegrasCode.GDNewObject2Objects2.length = 0;

gdjs.RegrasCode.eventsList0xaf580(runtimeScene, context);return;
}
gdjs['RegrasCode']= gdjs.RegrasCode;
