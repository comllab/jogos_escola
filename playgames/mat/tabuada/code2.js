gdjs.EndCode = {};
gdjs.EndCode.GDtopoObjects1= [];
gdjs.EndCode.GDtopoObjects2= [];
gdjs.EndCode.GDcortina_95LObjects1= [];
gdjs.EndCode.GDcortina_95LObjects2= [];
gdjs.EndCode.GDcortina_95DObjects1= [];
gdjs.EndCode.GDcortina_95DObjects2= [];
gdjs.EndCode.GDfundo_95madeiraObjects1= [];
gdjs.EndCode.GDfundo_95madeiraObjects2= [];
gdjs.EndCode.GDcontinaObjects1= [];
gdjs.EndCode.GDcontinaObjects2= [];
gdjs.EndCode.GDagua_951Objects1= [];
gdjs.EndCode.GDagua_951Objects2= [];
gdjs.EndCode.GDagua_952Objects1= [];
gdjs.EndCode.GDagua_952Objects2= [];
gdjs.EndCode.GDbase_95madeiraObjects1= [];
gdjs.EndCode.GDbase_95madeiraObjects2= [];
gdjs.EndCode.GDrifleObjects1= [];
gdjs.EndCode.GDrifleObjects2= [];
gdjs.EndCode.GDmiraObjects1= [];
gdjs.EndCode.GDmiraObjects2= [];
gdjs.EndCode.GDalvoReiniciarObjects1= [];
gdjs.EndCode.GDalvoReiniciarObjects2= [];
gdjs.EndCode.GDreiniciarObjects1= [];
gdjs.EndCode.GDreiniciarObjects2= [];
gdjs.EndCode.GDparabensObjects1= [];
gdjs.EndCode.GDparabensObjects2= [];
gdjs.EndCode.GDsiteObjects1= [];
gdjs.EndCode.GDsiteObjects2= [];
gdjs.EndCode.GDautorObjects1= [];
gdjs.EndCode.GDautorObjects2= [];
gdjs.EndCode.GDtabuada1Objects1= [];
gdjs.EndCode.GDtabuada1Objects2= [];
gdjs.EndCode.GDtabuada2Objects1= [];
gdjs.EndCode.GDtabuada2Objects2= [];
gdjs.EndCode.GDtabuada3Objects1= [];
gdjs.EndCode.GDtabuada3Objects2= [];
gdjs.EndCode.GDtabuada4Objects1= [];
gdjs.EndCode.GDtabuada4Objects2= [];
gdjs.EndCode.GDtabuada5Objects1= [];
gdjs.EndCode.GDtabuada5Objects2= [];
gdjs.EndCode.GDtabuada6Objects1= [];
gdjs.EndCode.GDtabuada6Objects2= [];
gdjs.EndCode.GDtabuada7Objects1= [];
gdjs.EndCode.GDtabuada7Objects2= [];
gdjs.EndCode.GDtabuada8Objects1= [];
gdjs.EndCode.GDtabuada8Objects2= [];
gdjs.EndCode.GDtabuada9Objects1= [];
gdjs.EndCode.GDtabuada9Objects2= [];
gdjs.EndCode.GDtabuada10Objects1= [];
gdjs.EndCode.GDtabuada10Objects2= [];
gdjs.EndCode.GDdominouObjects1= [];
gdjs.EndCode.GDdominouObjects2= [];
gdjs.EndCode.GDtiroObjects1= [];
gdjs.EndCode.GDtiroObjects2= [];

gdjs.EndCode.conditionTrue_0 = {val:false};
gdjs.EndCode.condition0IsTrue_0 = {val:false};
gdjs.EndCode.condition1IsTrue_0 = {val:false};
gdjs.EndCode.condition2IsTrue_0 = {val:false};


gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDalvoReiniciarObjects1Objects = Hashtable.newFrom({"alvoReiniciar": gdjs.EndCode.GDalvoReiniciarObjects1});gdjs.EndCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDdominouObjects1.createFrom(runtimeScene.getObjects("dominou"));
gdjs.EndCode.GDtabuada1Objects1.createFrom(runtimeScene.getObjects("tabuada1"));
gdjs.EndCode.GDtabuada10Objects1.createFrom(runtimeScene.getObjects("tabuada10"));
gdjs.EndCode.GDtabuada2Objects1.createFrom(runtimeScene.getObjects("tabuada2"));
gdjs.EndCode.GDtabuada3Objects1.createFrom(runtimeScene.getObjects("tabuada3"));
gdjs.EndCode.GDtabuada4Objects1.createFrom(runtimeScene.getObjects("tabuada4"));
gdjs.EndCode.GDtabuada5Objects1.createFrom(runtimeScene.getObjects("tabuada5"));
gdjs.EndCode.GDtabuada6Objects1.createFrom(runtimeScene.getObjects("tabuada6"));
gdjs.EndCode.GDtabuada7Objects1.createFrom(runtimeScene.getObjects("tabuada7"));
gdjs.EndCode.GDtabuada8Objects1.createFrom(runtimeScene.getObjects("tabuada8"));
gdjs.EndCode.GDtabuada9Objects1.createFrom(runtimeScene.getObjects("tabuada9"));
{gdjs.evtTools.sound.playMusic(runtimeScene, "CircusDilemmaInit.ogg", true, 70, 1);
}{for(var i = 0, len = gdjs.EndCode.GDdominouObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDdominouObjects1[i].setString("VOCÊ DOMINOU A TABUADA DO " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.EndCode.GDtabuada1Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDtabuada1Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0))+" X 1 = " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))*1));
}
}{for(var i = 0, len = gdjs.EndCode.GDtabuada2Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDtabuada2Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0))+" X 2 = " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))*2));
}
}{for(var i = 0, len = gdjs.EndCode.GDtabuada3Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDtabuada3Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0))+" X 3 = " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))*3));
}
}{for(var i = 0, len = gdjs.EndCode.GDtabuada4Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDtabuada4Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0))+" X 4 = " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))*4));
}
}{for(var i = 0, len = gdjs.EndCode.GDtabuada5Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDtabuada5Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0))+" X 5 = " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))*5));
}
}{for(var i = 0, len = gdjs.EndCode.GDtabuada6Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDtabuada6Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0))+" X 6 = " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))*6));
}
}{for(var i = 0, len = gdjs.EndCode.GDtabuada7Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDtabuada7Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0))+" X 7 = " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))*7));
}
}{for(var i = 0, len = gdjs.EndCode.GDtabuada8Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDtabuada8Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0))+" X 8 = " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))*8));
}
}{for(var i = 0, len = gdjs.EndCode.GDtabuada9Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDtabuada9Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0))+" X 9 = " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))*9));
}
}{for(var i = 0, len = gdjs.EndCode.GDtabuada10Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDtabuada10Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0))+" X 10 = " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))*10));
}
}}

}


{

gdjs.EndCode.GDalvoReiniciarObjects1.createFrom(runtimeScene.getObjects("alvoReiniciar"));

gdjs.EndCode.condition0IsTrue_0.val = false;
gdjs.EndCode.condition1IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDalvoReiniciarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.EndCode.condition0IsTrue_0.val ) {
{
gdjs.EndCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.EndCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Init", false);
}}

}


{



}


{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDagua_951Objects1.createFrom(runtimeScene.getObjects("agua_1"));
gdjs.EndCode.GDagua_952Objects1.createFrom(runtimeScene.getObjects("agua_2"));
gdjs.EndCode.GDmiraObjects1.createFrom(runtimeScene.getObjects("mira"));
{for(var i = 0, len = gdjs.EndCode.GDagua_951Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDagua_951Objects1[i].setXOffset(gdjs.EndCode.GDagua_951Objects1[i].getXOffset() - (0.7));
}
}{for(var i = 0, len = gdjs.EndCode.GDagua_952Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDagua_952Objects1[i].setXOffset(gdjs.EndCode.GDagua_952Objects1[i].getXOffset() + (0.5));
}
}{for(var i = 0, len = gdjs.EndCode.GDmiraObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDmiraObjects1[i].setPosition(gdjs.evtTools.input.getMouseX(runtimeScene, "", 0) - 27,gdjs.evtTools.input.getMouseY(runtimeScene, "", 0) - 27);
}
}}

}


{

gdjs.EndCode.GDrifleObjects1.createFrom(runtimeScene.getObjects("rifle"));

gdjs.EndCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.EndCode.GDrifleObjects1.length;i<l;++i) {
    if ( gdjs.EndCode.GDrifleObjects1[i].getX() <= 70 ) {
        gdjs.EndCode.condition0IsTrue_0.val = true;
        gdjs.EndCode.GDrifleObjects1[k] = gdjs.EndCode.GDrifleObjects1[i];
        ++k;
    }
}
gdjs.EndCode.GDrifleObjects1.length = k;}if (gdjs.EndCode.condition0IsTrue_0.val) {
/* Reuse gdjs.EndCode.GDrifleObjects1 */
{for(var i = 0, len = gdjs.EndCode.GDrifleObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDrifleObjects1[i].setX(70);
}
}}

}


{

gdjs.EndCode.GDrifleObjects1.createFrom(runtimeScene.getObjects("rifle"));

gdjs.EndCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.EndCode.GDrifleObjects1.length;i<l;++i) {
    if ( gdjs.EndCode.GDrifleObjects1[i].getX() >= 660 ) {
        gdjs.EndCode.condition0IsTrue_0.val = true;
        gdjs.EndCode.GDrifleObjects1[k] = gdjs.EndCode.GDrifleObjects1[i];
        ++k;
    }
}
gdjs.EndCode.GDrifleObjects1.length = k;}if (gdjs.EndCode.condition0IsTrue_0.val) {
/* Reuse gdjs.EndCode.GDrifleObjects1 */
{for(var i = 0, len = gdjs.EndCode.GDrifleObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDrifleObjects1[i].setX(660);
}
}}

}


}; //End of gdjs.EndCode.eventsList0xaf630


gdjs.EndCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.EndCode.GDtopoObjects1.length = 0;
gdjs.EndCode.GDtopoObjects2.length = 0;
gdjs.EndCode.GDcortina_95LObjects1.length = 0;
gdjs.EndCode.GDcortina_95LObjects2.length = 0;
gdjs.EndCode.GDcortina_95DObjects1.length = 0;
gdjs.EndCode.GDcortina_95DObjects2.length = 0;
gdjs.EndCode.GDfundo_95madeiraObjects1.length = 0;
gdjs.EndCode.GDfundo_95madeiraObjects2.length = 0;
gdjs.EndCode.GDcontinaObjects1.length = 0;
gdjs.EndCode.GDcontinaObjects2.length = 0;
gdjs.EndCode.GDagua_951Objects1.length = 0;
gdjs.EndCode.GDagua_951Objects2.length = 0;
gdjs.EndCode.GDagua_952Objects1.length = 0;
gdjs.EndCode.GDagua_952Objects2.length = 0;
gdjs.EndCode.GDbase_95madeiraObjects1.length = 0;
gdjs.EndCode.GDbase_95madeiraObjects2.length = 0;
gdjs.EndCode.GDrifleObjects1.length = 0;
gdjs.EndCode.GDrifleObjects2.length = 0;
gdjs.EndCode.GDmiraObjects1.length = 0;
gdjs.EndCode.GDmiraObjects2.length = 0;
gdjs.EndCode.GDalvoReiniciarObjects1.length = 0;
gdjs.EndCode.GDalvoReiniciarObjects2.length = 0;
gdjs.EndCode.GDreiniciarObjects1.length = 0;
gdjs.EndCode.GDreiniciarObjects2.length = 0;
gdjs.EndCode.GDparabensObjects1.length = 0;
gdjs.EndCode.GDparabensObjects2.length = 0;
gdjs.EndCode.GDsiteObjects1.length = 0;
gdjs.EndCode.GDsiteObjects2.length = 0;
gdjs.EndCode.GDautorObjects1.length = 0;
gdjs.EndCode.GDautorObjects2.length = 0;
gdjs.EndCode.GDtabuada1Objects1.length = 0;
gdjs.EndCode.GDtabuada1Objects2.length = 0;
gdjs.EndCode.GDtabuada2Objects1.length = 0;
gdjs.EndCode.GDtabuada2Objects2.length = 0;
gdjs.EndCode.GDtabuada3Objects1.length = 0;
gdjs.EndCode.GDtabuada3Objects2.length = 0;
gdjs.EndCode.GDtabuada4Objects1.length = 0;
gdjs.EndCode.GDtabuada4Objects2.length = 0;
gdjs.EndCode.GDtabuada5Objects1.length = 0;
gdjs.EndCode.GDtabuada5Objects2.length = 0;
gdjs.EndCode.GDtabuada6Objects1.length = 0;
gdjs.EndCode.GDtabuada6Objects2.length = 0;
gdjs.EndCode.GDtabuada7Objects1.length = 0;
gdjs.EndCode.GDtabuada7Objects2.length = 0;
gdjs.EndCode.GDtabuada8Objects1.length = 0;
gdjs.EndCode.GDtabuada8Objects2.length = 0;
gdjs.EndCode.GDtabuada9Objects1.length = 0;
gdjs.EndCode.GDtabuada9Objects2.length = 0;
gdjs.EndCode.GDtabuada10Objects1.length = 0;
gdjs.EndCode.GDtabuada10Objects2.length = 0;
gdjs.EndCode.GDdominouObjects1.length = 0;
gdjs.EndCode.GDdominouObjects2.length = 0;
gdjs.EndCode.GDtiroObjects1.length = 0;
gdjs.EndCode.GDtiroObjects2.length = 0;

gdjs.EndCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['EndCode']= gdjs.EndCode;
