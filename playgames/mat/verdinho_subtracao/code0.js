gdjs.StartCode = {};
gdjs.StartCode.GDfadeObjects1= [];
gdjs.StartCode.GDfadeObjects2= [];
gdjs.StartCode.GDtituloObjects1= [];
gdjs.StartCode.GDtituloObjects2= [];
gdjs.StartCode.GDsubtracaoObjects1= [];
gdjs.StartCode.GDsubtracaoObjects2= [];
gdjs.StartCode.GDverdinhoObjects1= [];
gdjs.StartCode.GDverdinhoObjects2= [];
gdjs.StartCode.GDbtJogarObjects1= [];
gdjs.StartCode.GDbtJogarObjects2= [];
gdjs.StartCode.GDtxtJogarObjects1= [];
gdjs.StartCode.GDtxtJogarObjects2= [];
gdjs.StartCode.GDckNivel_951Objects1= [];
gdjs.StartCode.GDckNivel_951Objects2= [];
gdjs.StartCode.GDckNivel_952Objects1= [];
gdjs.StartCode.GDckNivel_952Objects2= [];
gdjs.StartCode.GDckNivel_953Objects1= [];
gdjs.StartCode.GDckNivel_953Objects2= [];
gdjs.StartCode.GDckNivel_954Objects1= [];
gdjs.StartCode.GDckNivel_954Objects2= [];
gdjs.StartCode.GDckNivel_955Objects1= [];
gdjs.StartCode.GDckNivel_955Objects2= [];
gdjs.StartCode.GDckNivel_956Objects1= [];
gdjs.StartCode.GDckNivel_956Objects2= [];
gdjs.StartCode.GDckNivel_957Objects1= [];
gdjs.StartCode.GDckNivel_957Objects2= [];
gdjs.StartCode.GDckNivel_958Objects1= [];
gdjs.StartCode.GDckNivel_958Objects2= [];
gdjs.StartCode.GDtxtNivel_951Objects1= [];
gdjs.StartCode.GDtxtNivel_951Objects2= [];
gdjs.StartCode.GDtxtNivel_952Objects1= [];
gdjs.StartCode.GDtxtNivel_952Objects2= [];
gdjs.StartCode.GDtxtNivel_953Objects1= [];
gdjs.StartCode.GDtxtNivel_953Objects2= [];
gdjs.StartCode.GDtxtNivel_954Objects1= [];
gdjs.StartCode.GDtxtNivel_954Objects2= [];
gdjs.StartCode.GDtxtNivel_955Objects1= [];
gdjs.StartCode.GDtxtNivel_955Objects2= [];
gdjs.StartCode.GDtxtNivel_956Objects1= [];
gdjs.StartCode.GDtxtNivel_956Objects2= [];
gdjs.StartCode.GDtxtNivel_957Objects1= [];
gdjs.StartCode.GDtxtNivel_957Objects2= [];
gdjs.StartCode.GDtxtNivel_958Objects1= [];
gdjs.StartCode.GDtxtNivel_958Objects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};
gdjs.StartCode.condition3IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtJogarObjects1Objects = Hashtable.newFrom({"btJogar": gdjs.StartCode.GDbtJogarObjects1});gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDckNivel_95951Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95952Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95953Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95954Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95955Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95956Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95957Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95958Objects1Objects = Hashtable.newFrom({"ckNivel_1": gdjs.StartCode.GDckNivel_951Objects1, "ckNivel_2": gdjs.StartCode.GDckNivel_952Objects1, "ckNivel_3": gdjs.StartCode.GDckNivel_953Objects1, "ckNivel_4": gdjs.StartCode.GDckNivel_954Objects1, "ckNivel_5": gdjs.StartCode.GDckNivel_955Objects1, "ckNivel_6": gdjs.StartCode.GDckNivel_956Objects1, "ckNivel_7": gdjs.StartCode.GDckNivel_957Objects1, "ckNivel_8": gdjs.StartCode.GDckNivel_958Objects1});gdjs.StartCode.eventsList0xa86e0 = function(runtimeScene, context) {

{



}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "Mushroom Theme.ogg", true, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


{
gdjs.StartCode.GDckNivel_951Objects1.createFrom(runtimeScene.getObjects("ckNivel_1"));
gdjs.StartCode.GDckNivel_952Objects1.createFrom(runtimeScene.getObjects("ckNivel_2"));
gdjs.StartCode.GDckNivel_953Objects1.createFrom(runtimeScene.getObjects("ckNivel_3"));
gdjs.StartCode.GDckNivel_954Objects1.createFrom(runtimeScene.getObjects("ckNivel_4"));
gdjs.StartCode.GDckNivel_955Objects1.createFrom(runtimeScene.getObjects("ckNivel_5"));
gdjs.StartCode.GDckNivel_956Objects1.createFrom(runtimeScene.getObjects("ckNivel_6"));
gdjs.StartCode.GDckNivel_957Objects1.createFrom(runtimeScene.getObjects("ckNivel_7"));
gdjs.StartCode.GDckNivel_958Objects1.createFrom(runtimeScene.getObjects("ckNivel_8"));
{for(var i = 0, len = gdjs.StartCode.GDckNivel_951Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_951Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_952Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_952Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_953Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_953Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_954Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_954Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_955Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_955Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_956Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_956Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_957Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_957Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_958Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_958Objects1[i].setAnimation(0);
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 1;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDckNivel_951Objects1.createFrom(runtimeScene.getObjects("ckNivel_1"));
{for(var i = 0, len = gdjs.StartCode.GDckNivel_951Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_951Objects1[i].setAnimation(1);
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 2;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDckNivel_952Objects1.createFrom(runtimeScene.getObjects("ckNivel_2"));
{for(var i = 0, len = gdjs.StartCode.GDckNivel_952Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_952Objects1[i].setAnimation(1);
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 3;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDckNivel_953Objects1.createFrom(runtimeScene.getObjects("ckNivel_3"));
{for(var i = 0, len = gdjs.StartCode.GDckNivel_953Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_953Objects1[i].setAnimation(1);
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 4;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDckNivel_954Objects1.createFrom(runtimeScene.getObjects("ckNivel_4"));
{for(var i = 0, len = gdjs.StartCode.GDckNivel_954Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_954Objects1[i].setAnimation(1);
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 5;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDckNivel_955Objects1.createFrom(runtimeScene.getObjects("ckNivel_5"));
{for(var i = 0, len = gdjs.StartCode.GDckNivel_955Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_955Objects1[i].setAnimation(1);
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 6;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDckNivel_956Objects1.createFrom(runtimeScene.getObjects("ckNivel_6"));
{for(var i = 0, len = gdjs.StartCode.GDckNivel_956Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_956Objects1[i].setAnimation(1);
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 7;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDckNivel_957Objects1.createFrom(runtimeScene.getObjects("ckNivel_7"));
{for(var i = 0, len = gdjs.StartCode.GDckNivel_957Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_957Objects1[i].setAnimation(1);
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 8;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDckNivel_958Objects1.createFrom(runtimeScene.getObjects("ckNivel_8"));
{for(var i = 0, len = gdjs.StartCode.GDckNivel_958Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_958Objects1[i].setAnimation(1);
}
}}

}


{

gdjs.StartCode.GDbtJogarObjects1.createFrom(runtimeScene.getObjects("btJogar"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtJogarObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level", false);
}}

}


{

gdjs.StartCode.GDckNivel_951Objects1.createFrom(runtimeScene.getObjects("ckNivel_1"));
gdjs.StartCode.GDckNivel_952Objects1.createFrom(runtimeScene.getObjects("ckNivel_2"));
gdjs.StartCode.GDckNivel_953Objects1.createFrom(runtimeScene.getObjects("ckNivel_3"));
gdjs.StartCode.GDckNivel_954Objects1.createFrom(runtimeScene.getObjects("ckNivel_4"));
gdjs.StartCode.GDckNivel_955Objects1.createFrom(runtimeScene.getObjects("ckNivel_5"));
gdjs.StartCode.GDckNivel_956Objects1.createFrom(runtimeScene.getObjects("ckNivel_6"));
gdjs.StartCode.GDckNivel_957Objects1.createFrom(runtimeScene.getObjects("ckNivel_7"));
gdjs.StartCode.GDckNivel_958Objects1.createFrom(runtimeScene.getObjects("ckNivel_8"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDckNivel_95951Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95952Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95953Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95954Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95955Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95956Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95957Objects1ObjectsGDgdjs_46StartCode_46GDckNivel_95958Objects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = context.triggerOnce(6917980);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDckNivel_951Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_952Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_953Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_954Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_955Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_956Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_957Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_958Objects1 */
{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.StartCode.GDckNivel_958Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_957Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_956Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_955Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_954Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_953Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_952Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_951Objects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.StartCode.GDckNivel_951Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_952Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_953Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_954Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_955Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_956Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_957Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_958Objects1[0].getVariables()).get("nivel"))));
}}

}


}; //End of gdjs.StartCode.eventsList0xa86e0


gdjs.StartCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.StartCode.GDfadeObjects1.length = 0;
gdjs.StartCode.GDfadeObjects2.length = 0;
gdjs.StartCode.GDtituloObjects1.length = 0;
gdjs.StartCode.GDtituloObjects2.length = 0;
gdjs.StartCode.GDsubtracaoObjects1.length = 0;
gdjs.StartCode.GDsubtracaoObjects2.length = 0;
gdjs.StartCode.GDverdinhoObjects1.length = 0;
gdjs.StartCode.GDverdinhoObjects2.length = 0;
gdjs.StartCode.GDbtJogarObjects1.length = 0;
gdjs.StartCode.GDbtJogarObjects2.length = 0;
gdjs.StartCode.GDtxtJogarObjects1.length = 0;
gdjs.StartCode.GDtxtJogarObjects2.length = 0;
gdjs.StartCode.GDckNivel_951Objects1.length = 0;
gdjs.StartCode.GDckNivel_951Objects2.length = 0;
gdjs.StartCode.GDckNivel_952Objects1.length = 0;
gdjs.StartCode.GDckNivel_952Objects2.length = 0;
gdjs.StartCode.GDckNivel_953Objects1.length = 0;
gdjs.StartCode.GDckNivel_953Objects2.length = 0;
gdjs.StartCode.GDckNivel_954Objects1.length = 0;
gdjs.StartCode.GDckNivel_954Objects2.length = 0;
gdjs.StartCode.GDckNivel_955Objects1.length = 0;
gdjs.StartCode.GDckNivel_955Objects2.length = 0;
gdjs.StartCode.GDckNivel_956Objects1.length = 0;
gdjs.StartCode.GDckNivel_956Objects2.length = 0;
gdjs.StartCode.GDckNivel_957Objects1.length = 0;
gdjs.StartCode.GDckNivel_957Objects2.length = 0;
gdjs.StartCode.GDckNivel_958Objects1.length = 0;
gdjs.StartCode.GDckNivel_958Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_951Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_951Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_952Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_952Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_953Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_953Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_954Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_954Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_955Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_955Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_956Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_956Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_957Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_957Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_958Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_958Objects2.length = 0;

gdjs.StartCode.eventsList0xa86e0(runtimeScene, context);return;
}
gdjs['StartCode']= gdjs.StartCode;
