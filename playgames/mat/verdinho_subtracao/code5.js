gdjs.Fase5Code = {};
gdjs.Fase5Code.GDPlayerObjects1_1final = [];

gdjs.Fase5Code.forEachCount0_5 = 0;

gdjs.Fase5Code.forEachCount1_5 = 0;

gdjs.Fase5Code.forEachIndex5 = 0;

gdjs.Fase5Code.forEachObjects5 = [];

gdjs.Fase5Code.forEachTotalCount5 = 0;



gdjs.Fase5Code.GDPlayerObjects1= [];
gdjs.Fase5Code.GDPlayerObjects2= [];
gdjs.Fase5Code.GDPlayerObjects3= [];
gdjs.Fase5Code.GDPlayerObjects4= [];
gdjs.Fase5Code.GDPlayerObjects5= [];
gdjs.Fase5Code.GDPlatformObjects1= [];
gdjs.Fase5Code.GDPlatformObjects2= [];
gdjs.Fase5Code.GDPlatformObjects3= [];
gdjs.Fase5Code.GDPlatformObjects4= [];
gdjs.Fase5Code.GDPlatformObjects5= [];
gdjs.Fase5Code.GDJumpthruObjects1= [];
gdjs.Fase5Code.GDJumpthruObjects2= [];
gdjs.Fase5Code.GDJumpthruObjects3= [];
gdjs.Fase5Code.GDJumpthruObjects4= [];
gdjs.Fase5Code.GDJumpthruObjects5= [];
gdjs.Fase5Code.GDTiledGrassPlatformObjects1= [];
gdjs.Fase5Code.GDTiledGrassPlatformObjects2= [];
gdjs.Fase5Code.GDTiledGrassPlatformObjects3= [];
gdjs.Fase5Code.GDTiledGrassPlatformObjects4= [];
gdjs.Fase5Code.GDTiledGrassPlatformObjects5= [];
gdjs.Fase5Code.GDTiledCastlePlatformObjects1= [];
gdjs.Fase5Code.GDTiledCastlePlatformObjects2= [];
gdjs.Fase5Code.GDTiledCastlePlatformObjects3= [];
gdjs.Fase5Code.GDTiledCastlePlatformObjects4= [];
gdjs.Fase5Code.GDTiledCastlePlatformObjects5= [];
gdjs.Fase5Code.GDMovingPlatformObjects1= [];
gdjs.Fase5Code.GDMovingPlatformObjects2= [];
gdjs.Fase5Code.GDMovingPlatformObjects3= [];
gdjs.Fase5Code.GDMovingPlatformObjects4= [];
gdjs.Fase5Code.GDMovingPlatformObjects5= [];
gdjs.Fase5Code.GDGoLeftObjects1= [];
gdjs.Fase5Code.GDGoLeftObjects2= [];
gdjs.Fase5Code.GDGoLeftObjects3= [];
gdjs.Fase5Code.GDGoLeftObjects4= [];
gdjs.Fase5Code.GDGoLeftObjects5= [];
gdjs.Fase5Code.GDGoRightObjects1= [];
gdjs.Fase5Code.GDGoRightObjects2= [];
gdjs.Fase5Code.GDGoRightObjects3= [];
gdjs.Fase5Code.GDGoRightObjects4= [];
gdjs.Fase5Code.GDGoRightObjects5= [];
gdjs.Fase5Code.GDLadderObjects1= [];
gdjs.Fase5Code.GDLadderObjects2= [];
gdjs.Fase5Code.GDLadderObjects3= [];
gdjs.Fase5Code.GDLadderObjects4= [];
gdjs.Fase5Code.GDLadderObjects5= [];
gdjs.Fase5Code.GDPlayerHitBoxObjects1= [];
gdjs.Fase5Code.GDPlayerHitBoxObjects2= [];
gdjs.Fase5Code.GDPlayerHitBoxObjects3= [];
gdjs.Fase5Code.GDPlayerHitBoxObjects4= [];
gdjs.Fase5Code.GDPlayerHitBoxObjects5= [];
gdjs.Fase5Code.GDSlimeWalkObjects1= [];
gdjs.Fase5Code.GDSlimeWalkObjects2= [];
gdjs.Fase5Code.GDSlimeWalkObjects3= [];
gdjs.Fase5Code.GDSlimeWalkObjects4= [];
gdjs.Fase5Code.GDSlimeWalkObjects5= [];
gdjs.Fase5Code.GDFlyObjects1= [];
gdjs.Fase5Code.GDFlyObjects2= [];
gdjs.Fase5Code.GDFlyObjects3= [];
gdjs.Fase5Code.GDFlyObjects4= [];
gdjs.Fase5Code.GDFlyObjects5= [];
gdjs.Fase5Code.GDCloudObjects1= [];
gdjs.Fase5Code.GDCloudObjects2= [];
gdjs.Fase5Code.GDCloudObjects3= [];
gdjs.Fase5Code.GDCloudObjects4= [];
gdjs.Fase5Code.GDCloudObjects5= [];
gdjs.Fase5Code.GDBackgroundObjectsObjects1= [];
gdjs.Fase5Code.GDBackgroundObjectsObjects2= [];
gdjs.Fase5Code.GDBackgroundObjectsObjects3= [];
gdjs.Fase5Code.GDBackgroundObjectsObjects4= [];
gdjs.Fase5Code.GDBackgroundObjectsObjects5= [];
gdjs.Fase5Code.GDScoreObjects1= [];
gdjs.Fase5Code.GDScoreObjects2= [];
gdjs.Fase5Code.GDScoreObjects3= [];
gdjs.Fase5Code.GDScoreObjects4= [];
gdjs.Fase5Code.GDScoreObjects5= [];
gdjs.Fase5Code.GDCoinObjects1= [];
gdjs.Fase5Code.GDCoinObjects2= [];
gdjs.Fase5Code.GDCoinObjects3= [];
gdjs.Fase5Code.GDCoinObjects4= [];
gdjs.Fase5Code.GDCoinObjects5= [];
gdjs.Fase5Code.GDCoinIconObjects1= [];
gdjs.Fase5Code.GDCoinIconObjects2= [];
gdjs.Fase5Code.GDCoinIconObjects3= [];
gdjs.Fase5Code.GDCoinIconObjects4= [];
gdjs.Fase5Code.GDCoinIconObjects5= [];
gdjs.Fase5Code.GDLeftButtonObjects1= [];
gdjs.Fase5Code.GDLeftButtonObjects2= [];
gdjs.Fase5Code.GDLeftButtonObjects3= [];
gdjs.Fase5Code.GDLeftButtonObjects4= [];
gdjs.Fase5Code.GDLeftButtonObjects5= [];
gdjs.Fase5Code.GDRightButtonObjects1= [];
gdjs.Fase5Code.GDRightButtonObjects2= [];
gdjs.Fase5Code.GDRightButtonObjects3= [];
gdjs.Fase5Code.GDRightButtonObjects4= [];
gdjs.Fase5Code.GDRightButtonObjects5= [];
gdjs.Fase5Code.GDJumpButtonObjects1= [];
gdjs.Fase5Code.GDJumpButtonObjects2= [];
gdjs.Fase5Code.GDJumpButtonObjects3= [];
gdjs.Fase5Code.GDJumpButtonObjects4= [];
gdjs.Fase5Code.GDJumpButtonObjects5= [];
gdjs.Fase5Code.GDArrowButtonsBgObjects1= [];
gdjs.Fase5Code.GDArrowButtonsBgObjects2= [];
gdjs.Fase5Code.GDArrowButtonsBgObjects3= [];
gdjs.Fase5Code.GDArrowButtonsBgObjects4= [];
gdjs.Fase5Code.GDArrowButtonsBgObjects5= [];
gdjs.Fase5Code.GDlockObjects1= [];
gdjs.Fase5Code.GDlockObjects2= [];
gdjs.Fase5Code.GDlockObjects3= [];
gdjs.Fase5Code.GDlockObjects4= [];
gdjs.Fase5Code.GDlockObjects5= [];
gdjs.Fase5Code.GDfaseObjects1= [];
gdjs.Fase5Code.GDfaseObjects2= [];
gdjs.Fase5Code.GDfaseObjects3= [];
gdjs.Fase5Code.GDfaseObjects4= [];
gdjs.Fase5Code.GDfaseObjects5= [];

gdjs.Fase5Code.conditionTrue_0 = {val:false};
gdjs.Fase5Code.condition0IsTrue_0 = {val:false};
gdjs.Fase5Code.condition1IsTrue_0 = {val:false};
gdjs.Fase5Code.condition2IsTrue_0 = {val:false};
gdjs.Fase5Code.condition3IsTrue_0 = {val:false};
gdjs.Fase5Code.conditionTrue_1 = {val:false};
gdjs.Fase5Code.condition0IsTrue_1 = {val:false};
gdjs.Fase5Code.condition1IsTrue_1 = {val:false};
gdjs.Fase5Code.condition2IsTrue_1 = {val:false};
gdjs.Fase5Code.condition3IsTrue_1 = {val:false};

gdjs.Fase5Code.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.Fase5Code.GDPlayerObjects1.length = 0;
gdjs.Fase5Code.GDPlayerObjects2.length = 0;
gdjs.Fase5Code.GDPlayerObjects3.length = 0;
gdjs.Fase5Code.GDPlayerObjects4.length = 0;
gdjs.Fase5Code.GDPlayerObjects5.length = 0;
gdjs.Fase5Code.GDPlatformObjects1.length = 0;
gdjs.Fase5Code.GDPlatformObjects2.length = 0;
gdjs.Fase5Code.GDPlatformObjects3.length = 0;
gdjs.Fase5Code.GDPlatformObjects4.length = 0;
gdjs.Fase5Code.GDPlatformObjects5.length = 0;
gdjs.Fase5Code.GDJumpthruObjects1.length = 0;
gdjs.Fase5Code.GDJumpthruObjects2.length = 0;
gdjs.Fase5Code.GDJumpthruObjects3.length = 0;
gdjs.Fase5Code.GDJumpthruObjects4.length = 0;
gdjs.Fase5Code.GDJumpthruObjects5.length = 0;
gdjs.Fase5Code.GDTiledGrassPlatformObjects1.length = 0;
gdjs.Fase5Code.GDTiledGrassPlatformObjects2.length = 0;
gdjs.Fase5Code.GDTiledGrassPlatformObjects3.length = 0;
gdjs.Fase5Code.GDTiledGrassPlatformObjects4.length = 0;
gdjs.Fase5Code.GDTiledGrassPlatformObjects5.length = 0;
gdjs.Fase5Code.GDTiledCastlePlatformObjects1.length = 0;
gdjs.Fase5Code.GDTiledCastlePlatformObjects2.length = 0;
gdjs.Fase5Code.GDTiledCastlePlatformObjects3.length = 0;
gdjs.Fase5Code.GDTiledCastlePlatformObjects4.length = 0;
gdjs.Fase5Code.GDTiledCastlePlatformObjects5.length = 0;
gdjs.Fase5Code.GDMovingPlatformObjects1.length = 0;
gdjs.Fase5Code.GDMovingPlatformObjects2.length = 0;
gdjs.Fase5Code.GDMovingPlatformObjects3.length = 0;
gdjs.Fase5Code.GDMovingPlatformObjects4.length = 0;
gdjs.Fase5Code.GDMovingPlatformObjects5.length = 0;
gdjs.Fase5Code.GDGoLeftObjects1.length = 0;
gdjs.Fase5Code.GDGoLeftObjects2.length = 0;
gdjs.Fase5Code.GDGoLeftObjects3.length = 0;
gdjs.Fase5Code.GDGoLeftObjects4.length = 0;
gdjs.Fase5Code.GDGoLeftObjects5.length = 0;
gdjs.Fase5Code.GDGoRightObjects1.length = 0;
gdjs.Fase5Code.GDGoRightObjects2.length = 0;
gdjs.Fase5Code.GDGoRightObjects3.length = 0;
gdjs.Fase5Code.GDGoRightObjects4.length = 0;
gdjs.Fase5Code.GDGoRightObjects5.length = 0;
gdjs.Fase5Code.GDLadderObjects1.length = 0;
gdjs.Fase5Code.GDLadderObjects2.length = 0;
gdjs.Fase5Code.GDLadderObjects3.length = 0;
gdjs.Fase5Code.GDLadderObjects4.length = 0;
gdjs.Fase5Code.GDLadderObjects5.length = 0;
gdjs.Fase5Code.GDPlayerHitBoxObjects1.length = 0;
gdjs.Fase5Code.GDPlayerHitBoxObjects2.length = 0;
gdjs.Fase5Code.GDPlayerHitBoxObjects3.length = 0;
gdjs.Fase5Code.GDPlayerHitBoxObjects4.length = 0;
gdjs.Fase5Code.GDPlayerHitBoxObjects5.length = 0;
gdjs.Fase5Code.GDSlimeWalkObjects1.length = 0;
gdjs.Fase5Code.GDSlimeWalkObjects2.length = 0;
gdjs.Fase5Code.GDSlimeWalkObjects3.length = 0;
gdjs.Fase5Code.GDSlimeWalkObjects4.length = 0;
gdjs.Fase5Code.GDSlimeWalkObjects5.length = 0;
gdjs.Fase5Code.GDFlyObjects1.length = 0;
gdjs.Fase5Code.GDFlyObjects2.length = 0;
gdjs.Fase5Code.GDFlyObjects3.length = 0;
gdjs.Fase5Code.GDFlyObjects4.length = 0;
gdjs.Fase5Code.GDFlyObjects5.length = 0;
gdjs.Fase5Code.GDCloudObjects1.length = 0;
gdjs.Fase5Code.GDCloudObjects2.length = 0;
gdjs.Fase5Code.GDCloudObjects3.length = 0;
gdjs.Fase5Code.GDCloudObjects4.length = 0;
gdjs.Fase5Code.GDCloudObjects5.length = 0;
gdjs.Fase5Code.GDBackgroundObjectsObjects1.length = 0;
gdjs.Fase5Code.GDBackgroundObjectsObjects2.length = 0;
gdjs.Fase5Code.GDBackgroundObjectsObjects3.length = 0;
gdjs.Fase5Code.GDBackgroundObjectsObjects4.length = 0;
gdjs.Fase5Code.GDBackgroundObjectsObjects5.length = 0;
gdjs.Fase5Code.GDScoreObjects1.length = 0;
gdjs.Fase5Code.GDScoreObjects2.length = 0;
gdjs.Fase5Code.GDScoreObjects3.length = 0;
gdjs.Fase5Code.GDScoreObjects4.length = 0;
gdjs.Fase5Code.GDScoreObjects5.length = 0;
gdjs.Fase5Code.GDCoinObjects1.length = 0;
gdjs.Fase5Code.GDCoinObjects2.length = 0;
gdjs.Fase5Code.GDCoinObjects3.length = 0;
gdjs.Fase5Code.GDCoinObjects4.length = 0;
gdjs.Fase5Code.GDCoinObjects5.length = 0;
gdjs.Fase5Code.GDCoinIconObjects1.length = 0;
gdjs.Fase5Code.GDCoinIconObjects2.length = 0;
gdjs.Fase5Code.GDCoinIconObjects3.length = 0;
gdjs.Fase5Code.GDCoinIconObjects4.length = 0;
gdjs.Fase5Code.GDCoinIconObjects5.length = 0;
gdjs.Fase5Code.GDLeftButtonObjects1.length = 0;
gdjs.Fase5Code.GDLeftButtonObjects2.length = 0;
gdjs.Fase5Code.GDLeftButtonObjects3.length = 0;
gdjs.Fase5Code.GDLeftButtonObjects4.length = 0;
gdjs.Fase5Code.GDLeftButtonObjects5.length = 0;
gdjs.Fase5Code.GDRightButtonObjects1.length = 0;
gdjs.Fase5Code.GDRightButtonObjects2.length = 0;
gdjs.Fase5Code.GDRightButtonObjects3.length = 0;
gdjs.Fase5Code.GDRightButtonObjects4.length = 0;
gdjs.Fase5Code.GDRightButtonObjects5.length = 0;
gdjs.Fase5Code.GDJumpButtonObjects1.length = 0;
gdjs.Fase5Code.GDJumpButtonObjects2.length = 0;
gdjs.Fase5Code.GDJumpButtonObjects3.length = 0;
gdjs.Fase5Code.GDJumpButtonObjects4.length = 0;
gdjs.Fase5Code.GDJumpButtonObjects5.length = 0;
gdjs.Fase5Code.GDArrowButtonsBgObjects1.length = 0;
gdjs.Fase5Code.GDArrowButtonsBgObjects2.length = 0;
gdjs.Fase5Code.GDArrowButtonsBgObjects3.length = 0;
gdjs.Fase5Code.GDArrowButtonsBgObjects4.length = 0;
gdjs.Fase5Code.GDArrowButtonsBgObjects5.length = 0;
gdjs.Fase5Code.GDlockObjects1.length = 0;
gdjs.Fase5Code.GDlockObjects2.length = 0;
gdjs.Fase5Code.GDlockObjects3.length = 0;
gdjs.Fase5Code.GDlockObjects4.length = 0;
gdjs.Fase5Code.GDlockObjects5.length = 0;
gdjs.Fase5Code.GDfaseObjects1.length = 0;
gdjs.Fase5Code.GDfaseObjects2.length = 0;
gdjs.Fase5Code.GDfaseObjects3.length = 0;
gdjs.Fase5Code.GDfaseObjects4.length = 0;
gdjs.Fase5Code.GDfaseObjects5.length = 0;


{

gdjs.Fase5Code.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));
gdjs.Fase5Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerHitBoxObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.Fase5Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDCoinObjects1[i].setVariableNumber(gdjs.Fase5Code.GDCoinObjects1[i].getVariables().getFromIndex(0), gdjs.random(8) + 1);
}
}{for(var i = 0, len = gdjs.Fase5Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDCoinObjects1[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase5Code.GDCoinObjects1[i].getVariables().getFromIndex(0))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(5);
}{gdjs.evtTools.sound.playMusic(runtimeScene, "Intro Theme.ogg", true, 100, 1);
}}

}


{

gdjs.Fase5Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase5Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

{for(var i = 0, len = gdjs.Fase5Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerObjects1[i].setPosition((( gdjs.Fase5Code.GDPlayerHitBoxObjects1.length === 0 ) ? 0 :gdjs.Fase5Code.GDPlayerHitBoxObjects1[0].getPointX(""))-12,(( gdjs.Fase5Code.GDPlayerHitBoxObjects1.length === 0 ) ? 0 :gdjs.Fase5Code.GDPlayerHitBoxObjects1[0].getPointY("")));
}
}
}


{



}


{

gdjs.Fase5Code.GDPlayerObjects1.length = 0;

gdjs.Fase5Code.condition0IsTrue_0.val = false;
gdjs.Fase5Code.condition1IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "RShift");
}if ( gdjs.Fase5Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase5Code.conditionTrue_1 = gdjs.Fase5Code.condition1IsTrue_0;
gdjs.Fase5Code.GDPlayerObjects1_1final.length = 0;gdjs.Fase5Code.condition0IsTrue_1.val = false;
gdjs.Fase5Code.condition1IsTrue_1.val = false;
{
gdjs.Fase5Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDPlayerObjects2[i].getAnimation() == 0 ) {
        gdjs.Fase5Code.condition0IsTrue_1.val = true;
        gdjs.Fase5Code.GDPlayerObjects2[k] = gdjs.Fase5Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Fase5Code.GDPlayerObjects2.length = k;if( gdjs.Fase5Code.condition0IsTrue_1.val ) {
    gdjs.Fase5Code.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.Fase5Code.GDPlayerObjects2.length;j<jLen;++j) {
        if ( gdjs.Fase5Code.GDPlayerObjects1_1final.indexOf(gdjs.Fase5Code.GDPlayerObjects2[j]) === -1 )
            gdjs.Fase5Code.GDPlayerObjects1_1final.push(gdjs.Fase5Code.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.Fase5Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDPlayerObjects2[i].getAnimation() == 2 ) {
        gdjs.Fase5Code.condition1IsTrue_1.val = true;
        gdjs.Fase5Code.GDPlayerObjects2[k] = gdjs.Fase5Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Fase5Code.GDPlayerObjects2.length = k;if( gdjs.Fase5Code.condition1IsTrue_1.val ) {
    gdjs.Fase5Code.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.Fase5Code.GDPlayerObjects2.length;j<jLen;++j) {
        if ( gdjs.Fase5Code.GDPlayerObjects1_1final.indexOf(gdjs.Fase5Code.GDPlayerObjects2[j]) === -1 )
            gdjs.Fase5Code.GDPlayerObjects1_1final.push(gdjs.Fase5Code.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.Fase5Code.GDPlayerObjects1.createFrom(gdjs.Fase5Code.GDPlayerObjects1_1final);
}
}
}}
if (gdjs.Fase5Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}}

}


{

gdjs.Fase5Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
gdjs.Fase5Code.condition1IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.Fase5Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase5Code.conditionTrue_1 = gdjs.Fase5Code.condition1IsTrue_0;
gdjs.Fase5Code.conditionTrue_1.val = context.triggerOnce(239510236);
}
}}
if (gdjs.Fase5Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}{for(var i = 0, len = gdjs.Fase5Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{

gdjs.Fase5Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase5Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDPlayerHitBoxObjects1[k] = gdjs.Fase5Code.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDPlayerHitBoxObjects1.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.Fase5Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase5Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDPlayerHitBoxObjects1[k] = gdjs.Fase5Code.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDPlayerHitBoxObjects1.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.Fase5Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDPlayerHitBoxObjects1[k] = gdjs.Fase5Code.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDPlayerHitBoxObjects1.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {

{ //Subevents

{

gdjs.Fase5Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase5Code.GDPlayerHitBoxObjects2.createFrom(gdjs.Fase5Code.GDPlayerHitBoxObjects1);

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDPlayerHitBoxObjects2.length;i<l;++i) {
    if ( !(gdjs.Fase5Code.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDPlayerHitBoxObjects2[k] = gdjs.Fase5Code.GDPlayerHitBoxObjects2[i];
        ++k;
    }
}
gdjs.Fase5Code.GDPlayerHitBoxObjects2.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerObjects2[i].setAnimation(0);
}
}}

}


{

gdjs.Fase5Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase5Code.GDPlayerHitBoxObjects2.createFrom(gdjs.Fase5Code.GDPlayerHitBoxObjects1);

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDPlayerHitBoxObjects2.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").isMoving() ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDPlayerHitBoxObjects2[k] = gdjs.Fase5Code.GDPlayerHitBoxObjects2[i];
        ++k;
    }
}
gdjs.Fase5Code.GDPlayerHitBoxObjects2.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerObjects2[i].setAnimation(2);
}
}}

}

} //End of subevents
}

}


{

gdjs.Fase5Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerObjects1[i].flipX(true);
}
}}

}


{

gdjs.Fase5Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerObjects1[i].flipX(false);
}
}}

}


{



}


{

gdjs.Fase5Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));

{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.Fase5Code.GDPlayerObjects1.length === 0 ) ? 0 :gdjs.Fase5Code.GDPlayerObjects1[0].getPointX("")), "", 0);
}
}


{



}


{

gdjs.Fase5Code.GDGoLeftObjects1.createFrom(runtimeScene.getObjects("GoLeft"));
gdjs.Fase5Code.GDGoRightObjects1.createFrom(runtimeScene.getObjects("GoRight"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDGoLeftObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDGoLeftObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.Fase5Code.GDGoRightObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDGoRightObjects1[i].hide();
}
}}

}


{

gdjs.Fase5Code.GDGoLeftObjects1.createFrom(runtimeScene.getObjects("GoLeft"));
gdjs.Fase5Code.GDMovingPlatformObjects1.createFrom(runtimeScene.getObjects("MovingPlatform"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("GoLeft", gdjs.Fase5Code.GDGoLeftObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("MovingPlatform", gdjs.Fase5Code.GDMovingPlatformObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDMovingPlatformObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.Fase5Code.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDMovingPlatformObjects1[i].addForce(-150, 0, 1);
}
}}

}


{

gdjs.Fase5Code.GDGoRightObjects1.createFrom(runtimeScene.getObjects("GoRight"));
gdjs.Fase5Code.GDMovingPlatformObjects1.createFrom(runtimeScene.getObjects("MovingPlatform"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("GoRight", gdjs.Fase5Code.GDGoRightObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("MovingPlatform", gdjs.Fase5Code.GDMovingPlatformObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDMovingPlatformObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.Fase5Code.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDMovingPlatformObjects1[i].addForce(150, 0, 1);
}
}}

}


{



}


{



}


{

gdjs.Fase5Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects1[i].activateBehavior("PlatformerObject", false);
}
}}

}


{



}


{

gdjs.Fase5Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.Fase5Code.GDGoLeftObjects1.createFrom(runtimeScene.getObjects("GoLeft"));
gdjs.Fase5Code.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("GoLeft", gdjs.Fase5Code.GDGoLeftObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("SlimeWalk", gdjs.Fase5Code.GDSlimeWalkObjects1).addObjectsToEventsMap("Fly", gdjs.Fase5Code.GDFlyObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects1[i].setVariableNumber(gdjs.Fase5Code.GDSlimeWalkObjects1[i].getVariables().get("GoingLeft"), 1);
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects1[i].setVariableNumber(gdjs.Fase5Code.GDFlyObjects1[i].getVariables().get("GoingLeft"), 1);
}
}}

}


{

gdjs.Fase5Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.Fase5Code.GDGoRightObjects1.createFrom(runtimeScene.getObjects("GoRight"));
gdjs.Fase5Code.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("GoRight", gdjs.Fase5Code.GDGoRightObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("SlimeWalk", gdjs.Fase5Code.GDSlimeWalkObjects1).addObjectsToEventsMap("Fly", gdjs.Fase5Code.GDFlyObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects1[i].setVariableNumber(gdjs.Fase5Code.GDSlimeWalkObjects1[i].getVariables().get("GoingLeft"), 0);
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects1[i].setVariableNumber(gdjs.Fase5Code.GDFlyObjects1[i].getVariables().get("GoingLeft"), 0);
}
}}

}


{

gdjs.Fase5Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.Fase5Code.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDSlimeWalkObjects1[i].getAnimation() == 0 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDSlimeWalkObjects1[k] = gdjs.Fase5Code.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase5Code.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDFlyObjects1[i].getAnimation() == 0 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDFlyObjects1[k] = gdjs.Fase5Code.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDFlyObjects1.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {

{ //Subevents

{

gdjs.Fase5Code.GDFlyObjects2.createFrom(gdjs.Fase5Code.GDFlyObjects1);
gdjs.Fase5Code.GDSlimeWalkObjects2.createFrom(gdjs.Fase5Code.GDSlimeWalkObjects1);

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDSlimeWalkObjects2.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDSlimeWalkObjects2[i].getVariableNumber(gdjs.Fase5Code.GDSlimeWalkObjects2[i].getVariables().get("GoingLeft")) == 1 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDSlimeWalkObjects2[k] = gdjs.Fase5Code.GDSlimeWalkObjects2[i];
        ++k;
    }
}
gdjs.Fase5Code.GDSlimeWalkObjects2.length = k;for(var i = 0, k = 0, l = gdjs.Fase5Code.GDFlyObjects2.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDFlyObjects2[i].getVariableNumber(gdjs.Fase5Code.GDFlyObjects2[i].getVariables().get("GoingLeft")) == 1 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDFlyObjects2[k] = gdjs.Fase5Code.GDFlyObjects2[i];
        ++k;
    }
}
gdjs.Fase5Code.GDFlyObjects2.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects2[i].addForce(-300, 0, 0);
}
}{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects2[i].getBehavior("PlatformerObject").simulateLeftKey();
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects2[i].getBehavior("PlatformerObject").simulateLeftKey();
}
}{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects2[i].flipX(false);
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects2[i].flipX(false);
}
}}

}


{

gdjs.Fase5Code.GDFlyObjects2.createFrom(gdjs.Fase5Code.GDFlyObjects1);
gdjs.Fase5Code.GDSlimeWalkObjects2.createFrom(gdjs.Fase5Code.GDSlimeWalkObjects1);

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDSlimeWalkObjects2.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDSlimeWalkObjects2[i].getVariableNumber(gdjs.Fase5Code.GDSlimeWalkObjects2[i].getVariables().get("GoingLeft")) == 0 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDSlimeWalkObjects2[k] = gdjs.Fase5Code.GDSlimeWalkObjects2[i];
        ++k;
    }
}
gdjs.Fase5Code.GDSlimeWalkObjects2.length = k;for(var i = 0, k = 0, l = gdjs.Fase5Code.GDFlyObjects2.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDFlyObjects2[i].getVariableNumber(gdjs.Fase5Code.GDFlyObjects2[i].getVariables().get("GoingLeft")) == 0 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDFlyObjects2[k] = gdjs.Fase5Code.GDFlyObjects2[i];
        ++k;
    }
}
gdjs.Fase5Code.GDFlyObjects2.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects2[i].addForce(300, 0, 0);
}
}{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects2[i].getBehavior("PlatformerObject").simulateRightKey();
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects2[i].getBehavior("PlatformerObject").simulateRightKey();
}
}{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects2[i].flipX(true);
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects2[i].flipX(true);
}
}}

}


{



}


{

gdjs.Fase5Code.GDFlyObjects2.createFrom(gdjs.Fase5Code.GDFlyObjects1);
gdjs.Fase5Code.GDPlayerHitBoxObjects2.createFrom(runtimeScene.getObjects("PlayerHitBox"));
gdjs.Fase5Code.GDSlimeWalkObjects2.createFrom(gdjs.Fase5Code.GDSlimeWalkObjects1);

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("PlayerHitBox", gdjs.Fase5Code.GDPlayerHitBoxObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("SlimeWalk", gdjs.Fase5Code.GDSlimeWalkObjects2).addObjectsToEventsMap("Fly", gdjs.Fase5Code.GDFlyObjects2).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {

{ //Subevents

{

gdjs.Fase5Code.GDFlyObjects3.createFrom(gdjs.Fase5Code.GDFlyObjects2);
gdjs.Fase5Code.GDPlayerHitBoxObjects3.createFrom(gdjs.Fase5Code.GDPlayerHitBoxObjects2);
gdjs.Fase5Code.GDSlimeWalkObjects3.createFrom(gdjs.Fase5Code.GDSlimeWalkObjects2);

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDPlayerHitBoxObjects3.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDPlayerHitBoxObjects3[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDPlayerHitBoxObjects3[k] = gdjs.Fase5Code.GDPlayerHitBoxObjects3[i];
        ++k;
    }
}
gdjs.Fase5Code.GDPlayerHitBoxObjects3.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects3.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects3[i].setAnimation(1);
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects3.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects3[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects3.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects3[i].activateBehavior("PlatformerObject", true);
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects3.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects3[i].activateBehavior("PlatformerObject", true);
}
}{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects3.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects3[i].getBehavior("PlatformerObject").setGravity(1500);
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects3.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects3[i].getBehavior("PlatformerObject").setGravity(1500);
}
}{for(var i = 0, len = gdjs.Fase5Code.GDPlayerHitBoxObjects3.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerHitBoxObjects3[i].getBehavior("PlatformerObject").setCanJump();
}
}{for(var i = 0, len = gdjs.Fase5Code.GDPlayerHitBoxObjects3.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerHitBoxObjects3[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}
{ //Subevents

{

gdjs.Fase5Code.GDFlyObjects4.createFrom(gdjs.Fase5Code.GDFlyObjects3);
gdjs.Fase5Code.GDSlimeWalkObjects4.createFrom(gdjs.Fase5Code.GDSlimeWalkObjects3);

gdjs.Fase5Code.forEachTotalCount5 = 0;
gdjs.Fase5Code.forEachObjects5.length = 0;
gdjs.Fase5Code.forEachCount0_5 = gdjs.Fase5Code.GDSlimeWalkObjects4.length;
gdjs.Fase5Code.forEachTotalCount5 += gdjs.Fase5Code.forEachCount0_5;
gdjs.Fase5Code.forEachObjects5.push.apply(gdjs.Fase5Code.forEachObjects5,gdjs.Fase5Code.GDSlimeWalkObjects4);
gdjs.Fase5Code.forEachCount1_5 = gdjs.Fase5Code.GDFlyObjects4.length;
gdjs.Fase5Code.forEachTotalCount5 += gdjs.Fase5Code.forEachCount1_5;
gdjs.Fase5Code.forEachObjects5.push.apply(gdjs.Fase5Code.forEachObjects5,gdjs.Fase5Code.GDFlyObjects4);
for(gdjs.Fase5Code.forEachIndex5 = 0;gdjs.Fase5Code.forEachIndex5 < gdjs.Fase5Code.forEachTotalCount5;++gdjs.Fase5Code.forEachIndex5) {
gdjs.Fase5Code.GDFlyObjects5.createFrom(gdjs.Fase5Code.GDFlyObjects4);
gdjs.Fase5Code.GDSlimeWalkObjects5.createFrom(gdjs.Fase5Code.GDSlimeWalkObjects4);

gdjs.Fase5Code.GDSlimeWalkObjects5.length = 0;
gdjs.Fase5Code.GDFlyObjects5.length = 0;
if (gdjs.Fase5Code.forEachIndex5 < gdjs.Fase5Code.forEachCount0_5) {
    gdjs.Fase5Code.GDSlimeWalkObjects5.push(gdjs.Fase5Code.forEachObjects5[gdjs.Fase5Code.forEachIndex5]);
}
else if (gdjs.Fase5Code.forEachIndex5 < gdjs.Fase5Code.forEachCount0_5+gdjs.Fase5Code.forEachCount1_5) {
    gdjs.Fase5Code.GDFlyObjects5.push(gdjs.Fase5Code.forEachObjects5[gdjs.Fase5Code.forEachIndex5]);
}
if (true) {
{runtimeScene.getVariables().get("Score").add(0);
}}
}

}

} //End of subevents
}

}


{

gdjs.Fase5Code.GDPlayerHitBoxObjects3.createFrom(gdjs.Fase5Code.GDPlayerHitBoxObjects2);

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDPlayerHitBoxObjects3.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDPlayerHitBoxObjects3[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDPlayerHitBoxObjects3[k] = gdjs.Fase5Code.GDPlayerHitBoxObjects3[i];
        ++k;
    }
}
gdjs.Fase5Code.GDPlayerHitBoxObjects3.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).sub(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Fase" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)), false);
}}

}


{

gdjs.Fase5Code.GDFlyObjects3.createFrom(gdjs.Fase5Code.GDFlyObjects2);
gdjs.Fase5Code.GDPlayerHitBoxObjects3.createFrom(gdjs.Fase5Code.GDPlayerHitBoxObjects2);
gdjs.Fase5Code.GDSlimeWalkObjects3.createFrom(gdjs.Fase5Code.GDSlimeWalkObjects2);

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDPlayerHitBoxObjects3.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDPlayerHitBoxObjects3[i].getY() >= (( gdjs.Fase5Code.GDFlyObjects3.length === 0 ) ? (( gdjs.Fase5Code.GDSlimeWalkObjects3.length === 0 ) ? 0 :gdjs.Fase5Code.GDSlimeWalkObjects3[0].getPointY("")) :gdjs.Fase5Code.GDFlyObjects3[0].getPointY(""))-(gdjs.Fase5Code.GDPlayerHitBoxObjects3[i].getHeight())+(( gdjs.Fase5Code.GDFlyObjects3.length === 0 ) ? (( gdjs.Fase5Code.GDSlimeWalkObjects3.length === 0 ) ? 0 :gdjs.Fase5Code.GDSlimeWalkObjects3[0].getHeight()) :gdjs.Fase5Code.GDFlyObjects3[0].getHeight())/2 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDPlayerHitBoxObjects3[k] = gdjs.Fase5Code.GDPlayerHitBoxObjects3[i];
        ++k;
    }
}
gdjs.Fase5Code.GDPlayerHitBoxObjects3.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
}

}

} //End of subevents
}

}

} //End of subevents
}

}


{



}


{

gdjs.Fase5Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.Fase5Code.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
gdjs.Fase5Code.condition1IsTrue_0.val = false;
gdjs.Fase5Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDSlimeWalkObjects1[i].getAnimation() == 1 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDSlimeWalkObjects1[k] = gdjs.Fase5Code.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase5Code.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDFlyObjects1[i].getAnimation() == 1 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDFlyObjects1[k] = gdjs.Fase5Code.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDFlyObjects1.length = k;}if ( gdjs.Fase5Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDSlimeWalkObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase5Code.condition1IsTrue_0.val = true;
        gdjs.Fase5Code.GDSlimeWalkObjects1[k] = gdjs.Fase5Code.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase5Code.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDFlyObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase5Code.condition1IsTrue_0.val = true;
        gdjs.Fase5Code.GDFlyObjects1[k] = gdjs.Fase5Code.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDFlyObjects1.length = k;}if ( gdjs.Fase5Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase5Code.GDSlimeWalkObjects1[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Fase5Code.condition2IsTrue_0.val = true;
        gdjs.Fase5Code.GDSlimeWalkObjects1[k] = gdjs.Fase5Code.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase5Code.GDFlyObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase5Code.GDFlyObjects1[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Fase5Code.condition2IsTrue_0.val = true;
        gdjs.Fase5Code.GDFlyObjects1[k] = gdjs.Fase5Code.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDFlyObjects1.length = k;}}
}
if (gdjs.Fase5Code.condition2IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects1[i].activateBehavior("PlatformerObject", false);
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects1[i].activateBehavior("PlatformerObject", false);
}
}{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects1[i].setOpacity(gdjs.Fase5Code.GDSlimeWalkObjects1[i].getOpacity() - (50*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects1[i].setOpacity(gdjs.Fase5Code.GDFlyObjects1[i].getOpacity() - (50*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}
{ //Subevents

{

gdjs.Fase5Code.GDFlyObjects2.createFrom(gdjs.Fase5Code.GDFlyObjects1);
gdjs.Fase5Code.GDSlimeWalkObjects2.createFrom(gdjs.Fase5Code.GDSlimeWalkObjects1);

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDSlimeWalkObjects2.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDSlimeWalkObjects2[i].getOpacity() == 0 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDSlimeWalkObjects2[k] = gdjs.Fase5Code.GDSlimeWalkObjects2[i];
        ++k;
    }
}
gdjs.Fase5Code.GDSlimeWalkObjects2.length = k;for(var i = 0, k = 0, l = gdjs.Fase5Code.GDFlyObjects2.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDFlyObjects2[i].getOpacity() == 0 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDFlyObjects2[k] = gdjs.Fase5Code.GDFlyObjects2[i];
        ++k;
    }
}
gdjs.Fase5Code.GDFlyObjects2.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDSlimeWalkObjects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects2[i].deleteFromScene(runtimeScene);
}
}}

}

} //End of subevents
}

}


{

gdjs.Fase5Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.Fase5Code.GDTiledCastlePlatformObjects1.createFrom(runtimeScene.getObjects("TiledCastlePlatform"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("Fly", gdjs.Fase5Code.GDFlyObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("TiledCastlePlatform", gdjs.Fase5Code.GDTiledCastlePlatformObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDFlyObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.Fase5Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDPlayerObjects1[i].getY() > gdjs.evtTools.window.getCanvasHeight(runtimeScene) ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDPlayerObjects1[k] = gdjs.Fase5Code.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDPlayerObjects1.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).sub(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Fase" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)), false);
}}

}


{



}


{



}


{

gdjs.Fase5Code.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));
gdjs.Fase5Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
gdjs.Fase5Code.condition1IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("PlayerHitBox", gdjs.Fase5Code.GDPlayerHitBoxObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("Coin", gdjs.Fase5Code.GDCoinObjects1).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.Fase5Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDCoinObjects1[i].getOpacity() == 255 ) {
        gdjs.Fase5Code.condition1IsTrue_0.val = true;
        gdjs.Fase5Code.GDCoinObjects1[k] = gdjs.Fase5Code.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDCoinObjects1.length = k;}}
if (gdjs.Fase5Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDCoinObjects1[i].setOpacity(254);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "coin.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).add((gdjs.RuntimeObject.getVariableNumber(((gdjs.Fase5Code.GDCoinObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase5Code.GDCoinObjects1[0].getVariables()).getFromIndex(0))));
}{runtimeScene.getGame().getVariables().getFromIndex(2).add((gdjs.RuntimeObject.getVariableNumber(((gdjs.Fase5Code.GDCoinObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase5Code.GDCoinObjects1[0].getVariables()).getFromIndex(0))));
}}

}


{

gdjs.Fase5Code.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDCoinObjects1[i].getOpacity() < 255 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDCoinObjects1[k] = gdjs.Fase5Code.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDCoinObjects1.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDCoinObjects1[i].setOpacity(gdjs.Fase5Code.GDCoinObjects1[i].getOpacity() - (255*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}{for(var i = 0, len = gdjs.Fase5Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDCoinObjects1[i].addForce(0, -30, 0);
}
}}

}


{

gdjs.Fase5Code.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDCoinObjects1[i].getOpacity() == 0 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDCoinObjects1[k] = gdjs.Fase5Code.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDCoinObjects1.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDCoinObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{



}


{

gdjs.Fase5Code.GDScoreObjects1.createFrom(runtimeScene.getObjects("Score"));

{for(var i = 0, len = gdjs.Fase5Code.GDScoreObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDScoreObjects1[i].setString("$ "+gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}
}


{



}


{

gdjs.Fase5Code.GDArrowButtonsBgObjects1.createFrom(runtimeScene.getObjects("ArrowButtonsBg"));
gdjs.Fase5Code.GDJumpButtonObjects1.createFrom(runtimeScene.getObjects("JumpButton"));
gdjs.Fase5Code.GDLeftButtonObjects1.createFrom(runtimeScene.getObjects("LeftButton"));
gdjs.Fase5Code.GDRightButtonObjects1.createFrom(runtimeScene.getObjects("RightButton"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.input.anyKeyPressed(runtimeScene);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDLeftButtonObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDLeftButtonObjects1[i].hide();
}
for(var i = 0, len = gdjs.Fase5Code.GDRightButtonObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDRightButtonObjects1[i].hide();
}
for(var i = 0, len = gdjs.Fase5Code.GDJumpButtonObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDJumpButtonObjects1[i].hide();
}
for(var i = 0, len = gdjs.Fase5Code.GDArrowButtonsBgObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDArrowButtonsBgObjects1[i].hide();
}
}}

}


{


gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{gdjs.evtTools.input.touchSimulateMouse(runtimeScene, false);
}}

}


{

gdjs.Fase5Code.GDLeftButtonObjects1.createFrom(runtimeScene.getObjects("LeftButton"));
gdjs.Fase5Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("LeftButton", gdjs.Fase5Code.GDLeftButtonObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateLeftKey();
}
}}

}


{

gdjs.Fase5Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
gdjs.Fase5Code.GDRightButtonObjects1.createFrom(runtimeScene.getObjects("RightButton"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("RightButton", gdjs.Fase5Code.GDRightButtonObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateRightKey();
}
}}

}


{

gdjs.Fase5Code.GDJumpButtonObjects1.createFrom(runtimeScene.getObjects("JumpButton"));
gdjs.Fase5Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("JumpButton", gdjs.Fase5Code.GDJumpButtonObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{



}


{

gdjs.Fase5Code.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));

{for(var i = 0, len = gdjs.Fase5Code.GDfaseObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDfaseObjects1[i].setCharacterSize(gdjs.Fase5Code.GDfaseObjects1[i].getCharacterSize() + (0.1));
}
}{for(var i = 0, len = gdjs.Fase5Code.GDfaseObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDfaseObjects1[i].addForce(0, -220, 0);
}
}
}


{

gdjs.Fase5Code.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase5Code.GDfaseObjects1.length;i<l;++i) {
    if ( gdjs.Fase5Code.GDfaseObjects1[i].getOpacity() <= 0 ) {
        gdjs.Fase5Code.condition0IsTrue_0.val = true;
        gdjs.Fase5Code.GDfaseObjects1[k] = gdjs.Fase5Code.GDfaseObjects1[i];
        ++k;
    }
}
gdjs.Fase5Code.GDfaseObjects1.length = k;}if (gdjs.Fase5Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase5Code.GDfaseObjects1.length ;i < len;++i) {
    gdjs.Fase5Code.GDfaseObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{

gdjs.Fase5Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase5Code.GDlockObjects1.createFrom(runtimeScene.getObjects("lock"));

gdjs.Fase5Code.condition0IsTrue_0.val = false;
gdjs.Fase5Code.condition1IsTrue_0.val = false;
{
gdjs.Fase5Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("Player", gdjs.Fase5Code.GDPlayerObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("lock", gdjs.Fase5Code.GDlockObjects1).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.Fase5Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase5Code.conditionTrue_1 = gdjs.Fase5Code.condition1IsTrue_0;
gdjs.Fase5Code.condition0IsTrue_1.val = false;
{
gdjs.Fase5Code.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 30;
}gdjs.Fase5Code.conditionTrue_1.val = true && gdjs.Fase5Code.condition0IsTrue_1.val;
}
}}
if (gdjs.Fase5Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Chave5", false);
}}

}

return;
}
gdjs['Fase5Code']= gdjs.Fase5Code;
