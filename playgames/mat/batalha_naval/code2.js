gdjs.GameWinCode = {};
gdjs.GameWinCode.GDinimigosObjects1= [];
gdjs.GameWinCode.GDinimigosObjects2= [];
gdjs.GameWinCode.GDinimigosObjects3= [];
gdjs.GameWinCode.GDajudaObjects1= [];
gdjs.GameWinCode.GDajudaObjects2= [];
gdjs.GameWinCode.GDajudaObjects3= [];
gdjs.GameWinCode.GDbackgroundObjects1= [];
gdjs.GameWinCode.GDbackgroundObjects2= [];
gdjs.GameWinCode.GDbackgroundObjects3= [];
gdjs.GameWinCode.GDbtContinuarObjects1= [];
gdjs.GameWinCode.GDbtContinuarObjects2= [];
gdjs.GameWinCode.GDbtContinuarObjects3= [];
gdjs.GameWinCode.GDlblJogarObjects1= [];
gdjs.GameWinCode.GDlblJogarObjects2= [];
gdjs.GameWinCode.GDlblJogarObjects3= [];
gdjs.GameWinCode.GDtituloObjects1= [];
gdjs.GameWinCode.GDtituloObjects2= [];
gdjs.GameWinCode.GDtituloObjects3= [];
gdjs.GameWinCode.GDgamewinObjects1= [];
gdjs.GameWinCode.GDgamewinObjects2= [];
gdjs.GameWinCode.GDgamewinObjects3= [];
gdjs.GameWinCode.GDmapObjects1= [];
gdjs.GameWinCode.GDmapObjects2= [];
gdjs.GameWinCode.GDmapObjects3= [];
gdjs.GameWinCode.GDxObjects1= [];
gdjs.GameWinCode.GDxObjects2= [];
gdjs.GameWinCode.GDxObjects3= [];

gdjs.GameWinCode.conditionTrue_0 = {val:false};
gdjs.GameWinCode.condition0IsTrue_0 = {val:false};
gdjs.GameWinCode.condition1IsTrue_0 = {val:false};
gdjs.GameWinCode.condition2IsTrue_0 = {val:false};
gdjs.GameWinCode.condition3IsTrue_0 = {val:false};
gdjs.GameWinCode.conditionTrue_1 = {val:false};
gdjs.GameWinCode.condition0IsTrue_1 = {val:false};
gdjs.GameWinCode.condition1IsTrue_1 = {val:false};
gdjs.GameWinCode.condition2IsTrue_1 = {val:false};
gdjs.GameWinCode.condition3IsTrue_1 = {val:false};


gdjs.GameWinCode.mapOfGDgdjs_46GameWinCode_46GDbtContinuarObjects1Objects = Hashtable.newFrom({"btContinuar": gdjs.GameWinCode.GDbtContinuarObjects1});gdjs.GameWinCode.eventsList0x8a2238 = function(runtimeScene, context) {

{


gdjs.GameWinCode.condition0IsTrue_0.val = false;
{
gdjs.GameWinCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5));
}if (gdjs.GameWinCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.GameWinCode.condition0IsTrue_0.val = false;
{
gdjs.GameWinCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) > gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5));
}if (gdjs.GameWinCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


{


{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.GameWinCode.eventsList0x8a2238
gdjs.GameWinCode.eventsList0xafd70 = function(runtimeScene, context) {

{


gdjs.GameWinCode.condition0IsTrue_0.val = false;
{
gdjs.GameWinCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameWinCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "snare.ogg", 11, true, 100, 1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "orchestra.ogg", false, 100, 1);
}}

}


{


{
gdjs.GameWinCode.GDbackgroundObjects1.createFrom(runtimeScene.getObjects("background"));
{for(var i = 0, len = gdjs.GameWinCode.GDbackgroundObjects1.length ;i < len;++i) {
    gdjs.GameWinCode.GDbackgroundObjects1[i].setXOffset(gdjs.GameWinCode.GDbackgroundObjects1[i].getXOffset() - (0.25));
}
}}

}


{

gdjs.GameWinCode.GDbtContinuarObjects1.createFrom(runtimeScene.getObjects("btContinuar"));

gdjs.GameWinCode.condition0IsTrue_0.val = false;
gdjs.GameWinCode.condition1IsTrue_0.val = false;
gdjs.GameWinCode.condition2IsTrue_0.val = false;
{
gdjs.GameWinCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameWinCode.mapOfGDgdjs_46GameWinCode_46GDbtContinuarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameWinCode.condition0IsTrue_0.val ) {
{
gdjs.GameWinCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameWinCode.condition1IsTrue_0.val ) {
{
{gdjs.GameWinCode.conditionTrue_1 = gdjs.GameWinCode.condition2IsTrue_0;
gdjs.GameWinCode.conditionTrue_1.val = context.triggerOnce(9053148);
}
}}
}
if (gdjs.GameWinCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 11);
}
{ //Subevents
gdjs.GameWinCode.eventsList0x8a2238(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.GameWinCode.eventsList0xafd70


gdjs.GameWinCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GameWinCode.GDinimigosObjects1.length = 0;
gdjs.GameWinCode.GDinimigosObjects2.length = 0;
gdjs.GameWinCode.GDinimigosObjects3.length = 0;
gdjs.GameWinCode.GDajudaObjects1.length = 0;
gdjs.GameWinCode.GDajudaObjects2.length = 0;
gdjs.GameWinCode.GDajudaObjects3.length = 0;
gdjs.GameWinCode.GDbackgroundObjects1.length = 0;
gdjs.GameWinCode.GDbackgroundObjects2.length = 0;
gdjs.GameWinCode.GDbackgroundObjects3.length = 0;
gdjs.GameWinCode.GDbtContinuarObjects1.length = 0;
gdjs.GameWinCode.GDbtContinuarObjects2.length = 0;
gdjs.GameWinCode.GDbtContinuarObjects3.length = 0;
gdjs.GameWinCode.GDlblJogarObjects1.length = 0;
gdjs.GameWinCode.GDlblJogarObjects2.length = 0;
gdjs.GameWinCode.GDlblJogarObjects3.length = 0;
gdjs.GameWinCode.GDtituloObjects1.length = 0;
gdjs.GameWinCode.GDtituloObjects2.length = 0;
gdjs.GameWinCode.GDtituloObjects3.length = 0;
gdjs.GameWinCode.GDgamewinObjects1.length = 0;
gdjs.GameWinCode.GDgamewinObjects2.length = 0;
gdjs.GameWinCode.GDgamewinObjects3.length = 0;
gdjs.GameWinCode.GDmapObjects1.length = 0;
gdjs.GameWinCode.GDmapObjects2.length = 0;
gdjs.GameWinCode.GDmapObjects3.length = 0;
gdjs.GameWinCode.GDxObjects1.length = 0;
gdjs.GameWinCode.GDxObjects2.length = 0;
gdjs.GameWinCode.GDxObjects3.length = 0;

gdjs.GameWinCode.eventsList0xafd70(runtimeScene, context);return;
}
gdjs['GameWinCode']= gdjs.GameWinCode;
