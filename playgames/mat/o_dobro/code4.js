gdjs.Fase_954Code = {};


gdjs.Fase_954Code.GDenemyObjects1= [];
gdjs.Fase_954Code.GDenemyObjects2= [];
gdjs.Fase_954Code.GDdomoObjects1= [];
gdjs.Fase_954Code.GDdomoObjects2= [];
gdjs.Fase_954Code.GDtxtDobrarObjects1= [];
gdjs.Fase_954Code.GDtxtDobrarObjects2= [];
gdjs.Fase_954Code.GDnave1_95dobroObjects1= [];
gdjs.Fase_954Code.GDnave1_95dobroObjects2= [];
gdjs.Fase_954Code.GDnave2_95dobroObjects1= [];
gdjs.Fase_954Code.GDnave2_95dobroObjects2= [];
gdjs.Fase_954Code.GDnave3_95dobroObjects1= [];
gdjs.Fase_954Code.GDnave3_95dobroObjects2= [];
gdjs.Fase_954Code.GDnave4_95dobroObjects1= [];
gdjs.Fase_954Code.GDnave4_95dobroObjects2= [];
gdjs.Fase_954Code.GDdirecaoObjects1= [];
gdjs.Fase_954Code.GDdirecaoObjects2= [];
gdjs.Fase_954Code.GDplayerObjects1= [];
gdjs.Fase_954Code.GDplayerObjects2= [];
gdjs.Fase_954Code.GDcaptura_95dobroObjects1= [];
gdjs.Fase_954Code.GDcaptura_95dobroObjects2= [];
gdjs.Fase_954Code.GDfundoObjects1= [];
gdjs.Fase_954Code.GDfundoObjects2= [];
gdjs.Fase_954Code.GDnum_95dobro1Objects1= [];
gdjs.Fase_954Code.GDnum_95dobro1Objects2= [];
gdjs.Fase_954Code.GDnum_95dobro2Objects1= [];
gdjs.Fase_954Code.GDnum_95dobro2Objects2= [];
gdjs.Fase_954Code.GDnum_95dobro3Objects1= [];
gdjs.Fase_954Code.GDnum_95dobro3Objects2= [];
gdjs.Fase_954Code.GDnum_95dobro4Objects1= [];
gdjs.Fase_954Code.GDnum_95dobro4Objects2= [];
gdjs.Fase_954Code.GDacertou_95dobroObjects1= [];
gdjs.Fase_954Code.GDacertou_95dobroObjects2= [];
gdjs.Fase_954Code.GDtiroObjects1= [];
gdjs.Fase_954Code.GDtiroObjects2= [];
gdjs.Fase_954Code.GDtiro_95enemyObjects1= [];
gdjs.Fase_954Code.GDtiro_95enemyObjects2= [];
gdjs.Fase_954Code.GDenergia_95enemyObjects1= [];
gdjs.Fase_954Code.GDenergia_95enemyObjects2= [];
gdjs.Fase_954Code.GDenergia_95playerObjects1= [];
gdjs.Fase_954Code.GDenergia_95playerObjects2= [];
gdjs.Fase_954Code.GDplayer_95icon_95lifeObjects1= [];
gdjs.Fase_954Code.GDplayer_95icon_95lifeObjects2= [];
gdjs.Fase_954Code.GDfaseObjects1= [];
gdjs.Fase_954Code.GDfaseObjects2= [];

gdjs.Fase_954Code.conditionTrue_0 = {val:false};
gdjs.Fase_954Code.condition0IsTrue_0 = {val:false};
gdjs.Fase_954Code.condition1IsTrue_0 = {val:false};
gdjs.Fase_954Code.condition2IsTrue_0 = {val:false};
gdjs.Fase_954Code.condition3IsTrue_0 = {val:false};
gdjs.Fase_954Code.conditionTrue_1 = {val:false};
gdjs.Fase_954Code.condition0IsTrue_1 = {val:false};
gdjs.Fase_954Code.condition1IsTrue_1 = {val:false};
gdjs.Fase_954Code.condition2IsTrue_1 = {val:false};
gdjs.Fase_954Code.condition3IsTrue_1 = {val:false};

gdjs.Fase_954Code.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.Fase_954Code.GDenemyObjects1.length = 0;
gdjs.Fase_954Code.GDenemyObjects2.length = 0;
gdjs.Fase_954Code.GDdomoObjects1.length = 0;
gdjs.Fase_954Code.GDdomoObjects2.length = 0;
gdjs.Fase_954Code.GDtxtDobrarObjects1.length = 0;
gdjs.Fase_954Code.GDtxtDobrarObjects2.length = 0;
gdjs.Fase_954Code.GDnave1_95dobroObjects1.length = 0;
gdjs.Fase_954Code.GDnave1_95dobroObjects2.length = 0;
gdjs.Fase_954Code.GDnave2_95dobroObjects1.length = 0;
gdjs.Fase_954Code.GDnave2_95dobroObjects2.length = 0;
gdjs.Fase_954Code.GDnave3_95dobroObjects1.length = 0;
gdjs.Fase_954Code.GDnave3_95dobroObjects2.length = 0;
gdjs.Fase_954Code.GDnave4_95dobroObjects1.length = 0;
gdjs.Fase_954Code.GDnave4_95dobroObjects2.length = 0;
gdjs.Fase_954Code.GDdirecaoObjects1.length = 0;
gdjs.Fase_954Code.GDdirecaoObjects2.length = 0;
gdjs.Fase_954Code.GDplayerObjects1.length = 0;
gdjs.Fase_954Code.GDplayerObjects2.length = 0;
gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length = 0;
gdjs.Fase_954Code.GDcaptura_95dobroObjects2.length = 0;
gdjs.Fase_954Code.GDfundoObjects1.length = 0;
gdjs.Fase_954Code.GDfundoObjects2.length = 0;
gdjs.Fase_954Code.GDnum_95dobro1Objects1.length = 0;
gdjs.Fase_954Code.GDnum_95dobro1Objects2.length = 0;
gdjs.Fase_954Code.GDnum_95dobro2Objects1.length = 0;
gdjs.Fase_954Code.GDnum_95dobro2Objects2.length = 0;
gdjs.Fase_954Code.GDnum_95dobro3Objects1.length = 0;
gdjs.Fase_954Code.GDnum_95dobro3Objects2.length = 0;
gdjs.Fase_954Code.GDnum_95dobro4Objects1.length = 0;
gdjs.Fase_954Code.GDnum_95dobro4Objects2.length = 0;
gdjs.Fase_954Code.GDacertou_95dobroObjects1.length = 0;
gdjs.Fase_954Code.GDacertou_95dobroObjects2.length = 0;
gdjs.Fase_954Code.GDtiroObjects1.length = 0;
gdjs.Fase_954Code.GDtiroObjects2.length = 0;
gdjs.Fase_954Code.GDtiro_95enemyObjects1.length = 0;
gdjs.Fase_954Code.GDtiro_95enemyObjects2.length = 0;
gdjs.Fase_954Code.GDenergia_95enemyObjects1.length = 0;
gdjs.Fase_954Code.GDenergia_95enemyObjects2.length = 0;
gdjs.Fase_954Code.GDenergia_95playerObjects1.length = 0;
gdjs.Fase_954Code.GDenergia_95playerObjects2.length = 0;
gdjs.Fase_954Code.GDplayer_95icon_95lifeObjects1.length = 0;
gdjs.Fase_954Code.GDplayer_95icon_95lifeObjects2.length = 0;
gdjs.Fase_954Code.GDfaseObjects1.length = 0;
gdjs.Fase_954Code.GDfaseObjects2.length = 0;


{


gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "Finalizar");
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {

{ //Subevents

{

gdjs.Fase_954Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects2[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects2[i].getVariables().getFromIndex(3)) > 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects2[k] = gdjs.Fase_954Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects2.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Final", false);
}}

}


{

gdjs.Fase_954Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects2[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects2[i].getVariables().getFromIndex(3)) <= 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects2[k] = gdjs.Fase_954Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects2.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "ReiniciarFase", false);
}}

}

} //End of subevents
}

}


{

gdjs.Fase_954Code.GDacertou_95dobroObjects1.createFrom(runtimeScene.getObjects("acertou_dobro"));
gdjs.Fase_954Code.GDdirecaoObjects1.createFrom(runtimeScene.getObjects("direcao"));
gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));
gdjs.Fase_954Code.GDenergia_95enemyObjects1.createFrom(runtimeScene.getObjects("energia_enemy"));
gdjs.Fase_954Code.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDacertou_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDacertou_95dobroObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(2), 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDenergia_95enemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenergia_95enemyObjects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "Finalizar");
}{for(var i = 0, len = gdjs.Fase_954Code.GDdirecaoObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDdirecaoObjects1[i].hide();
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "01 game-game_0.ogg", 32, true, 100, 1);
}{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(2), -400);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDfaseObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDfaseObjects1[i].setString("FASE " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(3)));
}
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(-70);
}}

}


{



}


{

gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDenemyObjects1[k] = gdjs.Fase_954Code.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDenemyObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].addForce((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(2))), 0, 0);
}
}}

}


{

gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDenemyObjects1[i].getX() <= 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDenemyObjects1[k] = gdjs.Fase_954Code.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDenemyObjects1.length = k;}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase_954Code.conditionTrue_1 = gdjs.Fase_954Code.condition1IsTrue_0;
gdjs.Fase_954Code.conditionTrue_1.val = context.triggerOnce(352606652);
}
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(2), gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(2)) * (-1));
}
}}

}


{

gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDenemyObjects1[i].getX() >= gdjs.evtTools.window.getCanvasWidth(runtimeScene) - (gdjs.Fase_954Code.GDenemyObjects1[i].getWidth()) ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDenemyObjects1[k] = gdjs.Fase_954Code.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDenemyObjects1.length = k;}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase_954Code.conditionTrue_1 = gdjs.Fase_954Code.condition1IsTrue_0;
gdjs.Fase_954Code.conditionTrue_1.val = context.triggerOnce(352606220);
}
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(2), gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(2)) * (-1));
}
}}

}


{

gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4))) + 1, "atirar");
}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.Fase_954Code.condition1IsTrue_0.val = true;
        gdjs.Fase_954Code.GDenemyObjects1[k] = gdjs.Fase_954Code.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDenemyObjects1.length = k;}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(1), 1);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "atirar");
}}

}


{

gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
gdjs.Fase_954Code.GDtiro_95enemyObjects1.length = 0;

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDenemyObjects1[k] = gdjs.Fase_954Code.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDenemyObjects1.length = k;}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(4)) == 0 ) {
        gdjs.Fase_954Code.condition1IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects1[k] = gdjs.Fase_954Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(1), 0);
}
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, context.clearEventsObjectsMap().addObjectsToEventsMap("tiro_enemy", gdjs.Fase_954Code.GDtiro_95enemyObjects1).getEventsObjectsMap(), (( gdjs.Fase_954Code.GDenemyObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDenemyObjects1[0].getPointX("")) + (( gdjs.Fase_954Code.GDenemyObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDenemyObjects1[0].getWidth())/2 - (( gdjs.Fase_954Code.GDtiro_95enemyObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDtiro_95enemyObjects1[0].getWidth())/2, (( gdjs.Fase_954Code.GDenemyObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDenemyObjects1[0].getPointY("")), "player");
}{for(var i = 0, len = gdjs.Fase_954Code.GDtiro_95enemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDtiro_95enemyObjects1[i].setScale(0.5);
}
}{gdjs.evtTools.sound.playMusic(runtimeScene, "Laser_04.ogg", false, 100, 1);
}}

}


{

gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));
gdjs.Fase_954Code.GDenergia_95enemyObjects1.createFrom(runtimeScene.getObjects("energia_enemy"));
gdjs.Fase_954Code.GDtiro_95enemyObjects1.createFrom(runtimeScene.getObjects("tiro_enemy"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDenergia_95enemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenergia_95enemyObjects1[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(((gdjs.Fase_954Code.GDenemyObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDenemyObjects1[0].getVariables()).getFromIndex(3))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDtiro_95enemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDtiro_95enemyObjects1[i].addForce(0, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)), 1);
}
}}

}


{

gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
gdjs.Fase_954Code.GDtiro_95enemyObjects1.createFrom(runtimeScene.getObjects("tiro_enemy"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("tiro_enemy", gdjs.Fase_954Code.GDtiro_95enemyObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("player", gdjs.Fase_954Code.GDplayerObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDtiro_95enemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDtiro_95enemyObjects1[i].deleteFromScene(runtimeScene);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Laser_collision.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(3), gdjs.Fase_954Code.GDplayerObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(3)) - (1));
}
}}

}


{

gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(4)) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDenemyObjects1[k] = gdjs.Fase_954Code.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDenemyObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].addForce(0, 20, 1);
}
}}

}


{



}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().getFromIndex(0), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().getFromIndex(0), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().getFromIndex(0), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().getFromIndex(0), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().getFromIndex(0), gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().getFromIndex(0)) * (-1));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().getFromIndex(0), gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().getFromIndex(0)) * (-1));
}
}}

}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnum_95dobro1Objects1.createFrom(runtimeScene.getObjects("num_dobro1"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().getFromIndex(1)) == 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave1_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave1_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave1_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].addForce((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().getFromIndex(0))), 0, 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro1Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro1Objects1[i].setPosition((( gdjs.Fase_954Code.GDnave1_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave1_95dobroObjects1[0].getPointX("")) + (( gdjs.Fase_954Code.GDnave1_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave1_95dobroObjects1[0].getWidth())/2 - (gdjs.Fase_954Code.GDnum_95dobro1Objects1[i].getWidth())/2,(( gdjs.Fase_954Code.GDnave1_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave1_95dobroObjects1[0].getPointY("")) - 15);
}
}}

}


{

gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnum_95dobro2Objects1.createFrom(runtimeScene.getObjects("num_dobro2"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().getFromIndex(1)) == 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave2_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave2_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave2_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].addForce((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().getFromIndex(0))), 0, 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro2Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro2Objects1[i].setPosition((( gdjs.Fase_954Code.GDnave2_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave2_95dobroObjects1[0].getPointX("")) + (( gdjs.Fase_954Code.GDnave2_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave2_95dobroObjects1[0].getWidth())/2 - (gdjs.Fase_954Code.GDnum_95dobro2Objects1[i].getWidth())/2,(( gdjs.Fase_954Code.GDnave2_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave2_95dobroObjects1[0].getPointY("")) - 15);
}
}}

}


{

gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnum_95dobro3Objects1.createFrom(runtimeScene.getObjects("num_dobro3"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().getFromIndex(1)) == 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave3_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave3_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave3_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].addForce((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().getFromIndex(0))), 0, 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro3Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro3Objects1[i].setPosition((( gdjs.Fase_954Code.GDnave3_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave3_95dobroObjects1[0].getPointX("")) + (( gdjs.Fase_954Code.GDnave3_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave3_95dobroObjects1[0].getWidth())/2 - (gdjs.Fase_954Code.GDnum_95dobro3Objects1[i].getWidth())/2,(( gdjs.Fase_954Code.GDnave3_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave3_95dobroObjects1[0].getPointY("")) - 15);
}
}}

}


{

gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));
gdjs.Fase_954Code.GDnum_95dobro4Objects1.createFrom(runtimeScene.getObjects("num_dobro4"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().getFromIndex(1)) == 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave4_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave4_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave4_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].addForce((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().getFromIndex(0))), 0, 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro4Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro4Objects1[i].setPosition((( gdjs.Fase_954Code.GDnave4_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave4_95dobroObjects1[0].getPointX("")) + (( gdjs.Fase_954Code.GDnave4_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave4_95dobroObjects1[0].getWidth())/2 - (gdjs.Fase_954Code.GDnum_95dobro4Objects1[i].getWidth())/2,(( gdjs.Fase_954Code.GDnave4_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDnave4_95dobroObjects1[0].getPointY("")) - 15);
}
}}

}


{

gdjs.Fase_954Code.GDdirecaoObjects1.createFrom(runtimeScene.getObjects("direcao"));
gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("nave1_dobro", gdjs.Fase_954Code.GDnave1_95dobroObjects1).addObjectsToEventsMap("nave2_dobro", gdjs.Fase_954Code.GDnave2_95dobroObjects1).addObjectsToEventsMap("nave3_dobro", gdjs.Fase_954Code.GDnave3_95dobroObjects1).addObjectsToEventsMap("nave4_dobro", gdjs.Fase_954Code.GDnave4_95dobroObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("direcao", gdjs.Fase_954Code.GDdirecaoObjects1).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase_954Code.conditionTrue_1 = gdjs.Fase_954Code.condition1IsTrue_0;
gdjs.Fase_954Code.conditionTrue_1.val = context.triggerOnce(352609532);
}
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
}}

}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("nave1_dobro", gdjs.Fase_954Code.GDnave1_95dobroObjects1).addObjectsToEventsMap("nave2_dobro", gdjs.Fase_954Code.GDnave2_95dobroObjects1).addObjectsToEventsMap("nave3_dobro", gdjs.Fase_954Code.GDnave3_95dobroObjects1).addObjectsToEventsMap("nave4_dobro", gdjs.Fase_954Code.GDnave4_95dobroObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("nave1_dobro", gdjs.Fase_954Code.GDnave1_95dobroObjects1).addObjectsToEventsMap("nave2_dobro", gdjs.Fase_954Code.GDnave2_95dobroObjects1).addObjectsToEventsMap("nave3_dobro", gdjs.Fase_954Code.GDnave3_95dobroObjects1).addObjectsToEventsMap("nave4_dobro", gdjs.Fase_954Code.GDnave4_95dobroObjects1).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase_954Code.conditionTrue_1 = gdjs.Fase_954Code.condition1IsTrue_0;
gdjs.Fase_954Code.conditionTrue_1.val = context.triggerOnce(352610108);
}
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
}}

}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave1_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave1_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave1_95dobroObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave2_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave2_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave2_95dobroObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave3_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave3_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave3_95dobroObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave4_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave4_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave4_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].addForce(0, 30, 1);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].addForce(0, 30, 1);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].addForce(0, 30, 1);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].addForce(0, 30, 1);
}
}}

}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getX() > gdjs.evtTools.window.getCanvasWidth(runtimeScene) - (gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getWidth()) ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave1_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave1_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave1_95dobroObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getX() > gdjs.evtTools.window.getCanvasWidth(runtimeScene) - (gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getWidth()) ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave2_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave2_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave2_95dobroObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getX() > gdjs.evtTools.window.getCanvasWidth(runtimeScene) - (gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getWidth()) ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave3_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave3_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave3_95dobroObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getX() > gdjs.evtTools.window.getCanvasWidth(runtimeScene) - (gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getWidth()) ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave4_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave4_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave4_95dobroObjects1.length = k;}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase_954Code.conditionTrue_1 = gdjs.Fase_954Code.condition1IsTrue_0;
gdjs.Fase_954Code.conditionTrue_1.val = context.triggerOnce(352609964);
}
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
}}

}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getX() < -280 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave1_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave1_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave1_95dobroObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getX() < -280 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave2_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave2_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave2_95dobroObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getX() < -280 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave3_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave3_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave3_95dobroObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getX() < -280 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave4_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave4_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave4_95dobroObjects1.length = k;}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase_954Code.conditionTrue_1 = gdjs.Fase_954Code.condition1IsTrue_0;
gdjs.Fase_954Code.conditionTrue_1.val = context.triggerOnce(352609028);
}
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setX(-275);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].setX(-275);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setX(-275);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].setX(-275);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().get("velocidade"), gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().get("velocidade")) * (-1));
}
}}

}


{



}


{

gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setX(400 - (gdjs.Fase_954Code.GDplayerObjects1[i].getWidth())/2);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "SpaceShip_Engine_Small_Loop_00.ogg", true, 70, 1);
}}

}


{

gdjs.Fase_954Code.GDenergia_95playerObjects1.createFrom(runtimeScene.getObjects("energia_player"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

{for(var i = 0, len = gdjs.Fase_954Code.GDenergia_95playerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenergia_95playerObjects1[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(((gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDplayerObjects1[0].getVariables()).getFromIndex(3))));
}
}
}


{



}


{

gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects1[k] = gdjs.Fase_954Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setX(gdjs.evtTools.input.getMouseX(runtimeScene, "player", 0) - (gdjs.Fase_954Code.GDplayerObjects1[i].getWidth())/2);
}
}}

}


{

gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects1[i].getX() < 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects1[k] = gdjs.Fase_954Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setX(0);
}
}}

}


{

gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects1[i].getX() > gdjs.evtTools.window.getCanvasWidth(runtimeScene) - (gdjs.Fase_954Code.GDplayerObjects1[i].getWidth()) ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects1[k] = gdjs.Fase_954Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - (gdjs.Fase_954Code.GDplayerObjects1[i].getWidth()));
}
}}

}


{



}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnum_95dobro1Objects1.createFrom(runtimeScene.getObjects("num_dobro1"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length = 0;

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("num_dobro1", gdjs.Fase_954Code.GDnum_95dobro1Objects1).getEventsObjectsMap(), runtimeScene, true, false);
}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
gdjs.Fase_954Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "WarpDrive_02.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(0), 1);
}
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, context.clearEventsObjectsMap().addObjectsToEventsMap("captura_dobro", gdjs.Fase_954Code.GDcaptura_95dobroObjects1).getEventsObjectsMap(), (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getPointX("")) + (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getWidth())/2 - (( gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDcaptura_95dobroObjects1[0].getWidth())/2, (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getPointY("")) + (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getHeight()) - 10, "player");
}{for(var i = 0, len = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].setOpacity(254);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].setScaleY(5);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().getFromIndex(1), 1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro1Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro1Objects1[i].setColor("0;128;0");
}
}}

}


{

gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnum_95dobro2Objects1.createFrom(runtimeScene.getObjects("num_dobro2"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length = 0;

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("num_dobro2", gdjs.Fase_954Code.GDnum_95dobro2Objects1).getEventsObjectsMap(), runtimeScene, true, false);
}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
gdjs.Fase_954Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "WarpDrive_02.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(0), 1);
}
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, context.clearEventsObjectsMap().addObjectsToEventsMap("captura_dobro", gdjs.Fase_954Code.GDcaptura_95dobroObjects1).getEventsObjectsMap(), (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getPointX("")) + (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getWidth())/2 - (( gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDcaptura_95dobroObjects1[0].getWidth())/2, (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getPointY("")) + (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getHeight()) - 10, "player");
}{for(var i = 0, len = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].setOpacity(254);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].setScaleY(5);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().getFromIndex(1), 1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro2Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro2Objects1[i].setColor("0;128;0");
}
}}

}


{

gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnum_95dobro3Objects1.createFrom(runtimeScene.getObjects("num_dobro3"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length = 0;

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("num_dobro3", gdjs.Fase_954Code.GDnum_95dobro3Objects1).getEventsObjectsMap(), runtimeScene, true, false);
}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
gdjs.Fase_954Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "WarpDrive_02.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(0), 1);
}
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, context.clearEventsObjectsMap().addObjectsToEventsMap("captura_dobro", gdjs.Fase_954Code.GDcaptura_95dobroObjects1).getEventsObjectsMap(), (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getPointX("")) + (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getWidth())/2 - (( gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDcaptura_95dobroObjects1[0].getWidth())/2, (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getPointY("")) + (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getHeight()) - 10, "player");
}{for(var i = 0, len = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].setOpacity(254);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].setScaleY(5);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().getFromIndex(1), 1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro3Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro3Objects1[i].setColor("0;128;0");
}
}}

}


{

gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));
gdjs.Fase_954Code.GDnum_95dobro4Objects1.createFrom(runtimeScene.getObjects("num_dobro4"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length = 0;

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("num_dobro4", gdjs.Fase_954Code.GDnum_95dobro4Objects1).getEventsObjectsMap(), runtimeScene, true, false);
}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
gdjs.Fase_954Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "WarpDrive_02.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(0), 1);
}
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, context.clearEventsObjectsMap().addObjectsToEventsMap("captura_dobro", gdjs.Fase_954Code.GDcaptura_95dobroObjects1).getEventsObjectsMap(), (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getPointX("")) + (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getWidth())/2 - (( gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDcaptura_95dobroObjects1[0].getWidth())/2, (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getPointY("")) + (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getHeight()) - 10, "player");
}{for(var i = 0, len = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].setOpacity(254);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].setScaleY(5);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().getFromIndex(1), 1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro4Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro4Objects1[i].setColor("0;128;0");
}
}}

}


{

gdjs.Fase_954Code.GDcaptura_95dobroObjects1.createFrom(runtimeScene.getObjects("captura_dobro"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].getOpacity() < 255 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDcaptura_95dobroObjects1[k] = gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].setOpacity(gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].getOpacity() - (255*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{

gdjs.Fase_954Code.GDcaptura_95dobroObjects1.createFrom(runtimeScene.getObjects("captura_dobro"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].getOpacity() == 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDcaptura_95dobroObjects1[k] = gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDcaptura_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDcaptura_95dobroObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().get("capturar_dobro"), 0);
}
}}

}


{



}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnum_95dobro1Objects1.createFrom(runtimeScene.getObjects("num_dobro1"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave1_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave1_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave1_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro1Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro1Objects1[i].addForce(0, -70, 1);
}
}}

}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnum_95dobro1Objects1.createFrom(runtimeScene.getObjects("num_dobro1"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("num_dobro1", gdjs.Fase_954Code.GDnum_95dobro1Objects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("player", gdjs.Fase_954Code.GDplayerObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro1Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro1Objects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().getFromIndex(1), 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(0), 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(1), (gdjs.RuntimeObject.getVariableNumber(((gdjs.Fase_954Code.GDnave1_95dobroObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave1_95dobroObjects1[0].getVariables()).getFromIndex(2))));
}
}}

}


{

gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnum_95dobro2Objects1.createFrom(runtimeScene.getObjects("num_dobro2"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave2_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave2_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave2_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro2Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro2Objects1[i].addForce(0, -70, 1);
}
}}

}


{

gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnum_95dobro2Objects1.createFrom(runtimeScene.getObjects("num_dobro2"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("num_dobro2", gdjs.Fase_954Code.GDnum_95dobro2Objects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("player", gdjs.Fase_954Code.GDplayerObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro2Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro2Objects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().getFromIndex(1), 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(0), 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(1), (gdjs.RuntimeObject.getVariableNumber(((gdjs.Fase_954Code.GDnave2_95dobroObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave2_95dobroObjects1[0].getVariables()).getFromIndex(2))));
}
}}

}


{

gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnum_95dobro3Objects1.createFrom(runtimeScene.getObjects("num_dobro3"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave3_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave3_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave3_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro3Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro3Objects1[i].addForce(0, -70, 1);
}
}}

}


{

gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnum_95dobro3Objects1.createFrom(runtimeScene.getObjects("num_dobro3"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("num_dobro3", gdjs.Fase_954Code.GDnum_95dobro3Objects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("player", gdjs.Fase_954Code.GDplayerObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro3Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro3Objects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().getFromIndex(1), 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(0), 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(1), (gdjs.RuntimeObject.getVariableNumber(((gdjs.Fase_954Code.GDnave3_95dobroObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave3_95dobroObjects1[0].getVariables()).getFromIndex(2))));
}
}}

}


{

gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));
gdjs.Fase_954Code.GDnum_95dobro4Objects1.createFrom(runtimeScene.getObjects("num_dobro4"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDnave4_95dobroObjects1[k] = gdjs.Fase_954Code.GDnave4_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDnave4_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro4Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro4Objects1[i].addForce(0, -70, 1);
}
}}

}


{

gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));
gdjs.Fase_954Code.GDnum_95dobro4Objects1.createFrom(runtimeScene.getObjects("num_dobro4"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("num_dobro4", gdjs.Fase_954Code.GDnum_95dobro4Objects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("player", gdjs.Fase_954Code.GDplayerObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro4Objects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro4Objects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().getFromIndex(1), 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(0), 0);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(1), (gdjs.RuntimeObject.getVariableNumber(((gdjs.Fase_954Code.GDnave4_95dobroObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave4_95dobroObjects1[0].getVariables()).getFromIndex(2))));
}
}}

}


{



}


{

gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
gdjs.Fase_954Code.GDtiroObjects1.length = 0;

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects1[k] = gdjs.Fase_954Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects1.length = k;}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
gdjs.Fase_954Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{gdjs.evtTools.object.createObjectOnScene(runtimeScene, context.clearEventsObjectsMap().addObjectsToEventsMap("tiro", gdjs.Fase_954Code.GDtiroObjects1).getEventsObjectsMap(), (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getPointX("")) + 75/2 - (( gdjs.Fase_954Code.GDtiroObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDtiroObjects1[0].getWidth())/2, (( gdjs.Fase_954Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase_954Code.GDplayerObjects1[0].getPointY("")) + 75, "player");
}{gdjs.evtTools.sound.playSound(runtimeScene, "Laser_09.ogg", false, 100, 1);
}}

}


{

gdjs.Fase_954Code.GDtiroObjects1.createFrom(runtimeScene.getObjects("tiro"));

{for(var i = 0, len = gdjs.Fase_954Code.GDtiroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDtiroObjects1[i].addForce(0, 70, 1);
}
}
}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));
gdjs.Fase_954Code.GDtiroObjects1.createFrom(runtimeScene.getObjects("tiro"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("tiro", gdjs.Fase_954Code.GDtiroObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("nave1_dobro", gdjs.Fase_954Code.GDnave1_95dobroObjects1).addObjectsToEventsMap("nave2_dobro", gdjs.Fase_954Code.GDnave2_95dobroObjects1).addObjectsToEventsMap("nave3_dobro", gdjs.Fase_954Code.GDnave3_95dobroObjects1).addObjectsToEventsMap("nave4_dobro", gdjs.Fase_954Code.GDnave4_95dobroObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setAnimation(1);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].setAnimation(1);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setAnimation(1);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().get("cair"), 1);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().get("cair"), 1);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().get("cair"), 1);
}
for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().get("cair"), 1);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Laser_collision.ogg", false, 100, 1);
}}

}


{

gdjs.Fase_954Code.GDdomoObjects1.createFrom(runtimeScene.getObjects("domo"));
gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));
gdjs.Fase_954Code.GDenergia_95enemyObjects1.createFrom(runtimeScene.getObjects("energia_enemy"));
gdjs.Fase_954Code.GDnave1_95dobroObjects1.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects1.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects1.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects1.createFrom(runtimeScene.getObjects("nave4_dobro"));
gdjs.Fase_954Code.GDtiroObjects1.createFrom(runtimeScene.getObjects("tiro"));
gdjs.Fase_954Code.GDtxtDobrarObjects1.createFrom(runtimeScene.getObjects("txtDobrar"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("tiro", gdjs.Fase_954Code.GDtiroObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("domo", gdjs.Fase_954Code.GDdomoObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.stopMusicOnChannel(runtimeScene, 32);
}{gdjs.evtTools.sound.playMusic(runtimeScene, "Orbital Colossus.ogg", true, 100, 1);
}{for(var i = 0, len = gdjs.Fase_954Code.GDtiroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDtiroObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDdomoObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDdomoObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects1[i].getVariables().getFromIndex(3), 1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects1[i].getVariables().getFromIndex(3), 1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects1[i].getVariables().getFromIndex(3), 1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects1[i].getVariables().getFromIndex(3), 1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(0), 1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(1), 1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDtxtDobrarObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDtxtDobrarObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDenergia_95enemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenergia_95enemyObjects1[i].hide(false);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Laser_collision.ogg", false, 100, 1);
}}

}


{

gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));
gdjs.Fase_954Code.GDtiroObjects1.createFrom(runtimeScene.getObjects("tiro"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("tiro", gdjs.Fase_954Code.GDtiroObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("enemy", gdjs.Fase_954Code.GDenemyObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDtiroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDtiroObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(3), gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(3)) - (1));
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Laser_collision.ogg", false, 100, 1);
}}

}


{

gdjs.Fase_954Code.GDfundoObjects1.createFrom(runtimeScene.getObjects("fundo"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDfundoObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDfundoObjects1[i].setYOffset(gdjs.Fase_954Code.GDfundoObjects1[i].getYOffset() - (0.1));
}
}}

}


{



}


{

gdjs.Fase_954Code.GDtxtDobrarObjects1.createFrom(runtimeScene.getObjects("txtDobrar"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(9) + 25);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) * 2);
}{for(var i = 0, len = gdjs.Fase_954Code.GDtxtDobrarObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDtxtDobrarObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(gdjs.random(3) + 1);
}
{ //Subevents

{

gdjs.Fase_954Code.GDnave1_95dobroObjects2.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects2.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects2.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects2.createFrom(runtimeScene.getObjects("nave4_dobro"));
gdjs.Fase_954Code.GDnum_95dobro1Objects2.createFrom(runtimeScene.getObjects("num_dobro1"));
gdjs.Fase_954Code.GDnum_95dobro2Objects2.createFrom(runtimeScene.getObjects("num_dobro2"));
gdjs.Fase_954Code.GDnum_95dobro3Objects2.createFrom(runtimeScene.getObjects("num_dobro3"));
gdjs.Fase_954Code.GDnum_95dobro4Objects2.createFrom(runtimeScene.getObjects("num_dobro4"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro1Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro1Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave1_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave1_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro2Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro2Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave2_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave2_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro3Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro3Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave3_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave3_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro4Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro4Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave4_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave4_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}}

}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects2.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects2.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects2.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects2.createFrom(runtimeScene.getObjects("nave4_dobro"));
gdjs.Fase_954Code.GDnum_95dobro1Objects2.createFrom(runtimeScene.getObjects("num_dobro1"));
gdjs.Fase_954Code.GDnum_95dobro2Objects2.createFrom(runtimeScene.getObjects("num_dobro2"));
gdjs.Fase_954Code.GDnum_95dobro3Objects2.createFrom(runtimeScene.getObjects("num_dobro3"));
gdjs.Fase_954Code.GDnum_95dobro4Objects2.createFrom(runtimeScene.getObjects("num_dobro4"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 2;
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro1Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro1Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave1_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave1_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro2Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro2Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave2_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave2_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro3Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro3Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave3_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave3_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro4Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro4Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave4_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave4_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}}

}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects2.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects2.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects2.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects2.createFrom(runtimeScene.getObjects("nave4_dobro"));
gdjs.Fase_954Code.GDnum_95dobro1Objects2.createFrom(runtimeScene.getObjects("num_dobro1"));
gdjs.Fase_954Code.GDnum_95dobro2Objects2.createFrom(runtimeScene.getObjects("num_dobro2"));
gdjs.Fase_954Code.GDnum_95dobro3Objects2.createFrom(runtimeScene.getObjects("num_dobro3"));
gdjs.Fase_954Code.GDnum_95dobro4Objects2.createFrom(runtimeScene.getObjects("num_dobro4"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 3;
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro1Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro1Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave1_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave1_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro2Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro2Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave2_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave2_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro3Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro3Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave3_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave3_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro4Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro4Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave4_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave4_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}}

}


{

gdjs.Fase_954Code.GDnave1_95dobroObjects2.createFrom(runtimeScene.getObjects("nave1_dobro"));
gdjs.Fase_954Code.GDnave2_95dobroObjects2.createFrom(runtimeScene.getObjects("nave2_dobro"));
gdjs.Fase_954Code.GDnave3_95dobroObjects2.createFrom(runtimeScene.getObjects("nave3_dobro"));
gdjs.Fase_954Code.GDnave4_95dobroObjects2.createFrom(runtimeScene.getObjects("nave4_dobro"));
gdjs.Fase_954Code.GDnum_95dobro1Objects2.createFrom(runtimeScene.getObjects("num_dobro1"));
gdjs.Fase_954Code.GDnum_95dobro2Objects2.createFrom(runtimeScene.getObjects("num_dobro2"));
gdjs.Fase_954Code.GDnum_95dobro3Objects2.createFrom(runtimeScene.getObjects("num_dobro3"));
gdjs.Fase_954Code.GDnum_95dobro4Objects2.createFrom(runtimeScene.getObjects("num_dobro4"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 4;
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDnave4_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave4_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave4_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave1_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave1_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave1_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave2_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave2_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave2_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnave3_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnave3_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDnave3_95dobroObjects2[i].getVariables().getFromIndex(2), gdjs.random(50) + 25);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro1Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro1Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave1_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave1_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro2Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro2Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave2_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave2_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro3Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro3Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave3_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave3_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDnum_95dobro4Objects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDnum_95dobro4Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.Fase_954Code.GDnave4_95dobroObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase_954Code.GDnave4_95dobroObjects2[0].getVariables()).getFromIndex(2))));
}
}}

}

} //End of subevents
}

}


{



}


{

gdjs.Fase_954Code.GDacertou_95dobroObjects1.createFrom(runtimeScene.getObjects("acertou_dobro"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects1[k] = gdjs.Fase_954Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects1.length = k;}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDacertou_95dobroObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase_954Code.GDacertou_95dobroObjects1[i].isVisible()) ) {
        gdjs.Fase_954Code.condition1IsTrue_0.val = true;
        gdjs.Fase_954Code.GDacertou_95dobroObjects1[k] = gdjs.Fase_954Code.GDacertou_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDacertou_95dobroObjects1.length = k;}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDacertou_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDacertou_95dobroObjects1[i].hide(false);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Jingle_Win_00.ogg", false, 100, 1);
}}

}


{

gdjs.Fase_954Code.GDacertou_95dobroObjects1.createFrom(runtimeScene.getObjects("acertou_dobro"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDacertou_95dobroObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDacertou_95dobroObjects1[i].isVisible() ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDacertou_95dobroObjects1[k] = gdjs.Fase_954Code.GDacertou_95dobroObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDacertou_95dobroObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDacertou_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDacertou_95dobroObjects1[i].rotate(15, runtimeScene);
}
}
{ //Subevents

{

gdjs.Fase_954Code.GDacertou_95dobroObjects2.createFrom(gdjs.Fase_954Code.GDacertou_95dobroObjects1);
gdjs.Fase_954Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("player", gdjs.Fase_954Code.GDplayerObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("acertou_dobro", gdjs.Fase_954Code.GDacertou_95dobroObjects2).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDacertou_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDacertou_95dobroObjects2[i].setVariableNumber(gdjs.Fase_954Code.GDacertou_95dobroObjects2[i].getVariables().getFromIndex(0), 1);
}
}}

}


{

gdjs.Fase_954Code.GDacertou_95dobroObjects2.createFrom(gdjs.Fase_954Code.GDacertou_95dobroObjects1);

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDacertou_95dobroObjects2.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDacertou_95dobroObjects2[i].getVariableNumber(gdjs.Fase_954Code.GDacertou_95dobroObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDacertou_95dobroObjects2[k] = gdjs.Fase_954Code.GDacertou_95dobroObjects2[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDacertou_95dobroObjects2.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDacertou_95dobroObjects2.length ;i < len;++i) {
    gdjs.Fase_954Code.GDacertou_95dobroObjects2[i].addForce(0, 30, 1);
}
}}

}

} //End of subevents
}

}


{

gdjs.Fase_954Code.GDacertou_95dobroObjects1.createFrom(runtimeScene.getObjects("acertou_dobro"));
gdjs.Fase_954Code.GDdomoObjects1.createFrom(runtimeScene.getObjects("domo"));
gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("acertou_dobro", gdjs.Fase_954Code.GDacertou_95dobroObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("domo", gdjs.Fase_954Code.GDdomoObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDacertou_95dobroObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDacertou_95dobroObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDdomoObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDdomoObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(2), 1);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Laser_collision.ogg", false, 100, 1);
}}

}


{



}


{

gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(1)) > 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects1[k] = gdjs.Fase_954Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects1.length = k;}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(1)) != gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) ) {
        gdjs.Fase_954Code.condition1IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects1[k] = gdjs.Fase_954Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(3), 0);
}
}}

}


{



}


{

gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(3)) <= 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDenemyObjects1[k] = gdjs.Fase_954Code.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDenemyObjects1.length = k;}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(4)) == 0 ) {
        gdjs.Fase_954Code.condition1IsTrue_0.val = true;
        gdjs.Fase_954Code.GDenemyObjects1[k] = gdjs.Fase_954Code.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDenemyObjects1.length = k;}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "WarpDrive_00.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(4), 1);
}
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "Finalizar");
}}

}


{

gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(3)) <= 0 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects1[k] = gdjs.Fase_954Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects1.length = k;}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(4)) == 0 ) {
        gdjs.Fase_954Code.condition1IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects1[k] = gdjs.Fase_954Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "WarpDrive_00.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(4), 1);
}
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "Finalizar");
}}

}


{

gdjs.Fase_954Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDplayerObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDplayerObjects1[i].getVariables().getFromIndex(4)) == 1 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDplayerObjects1[k] = gdjs.Fase_954Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDplayerObjects1.length = k;}if (gdjs.Fase_954Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDplayerObjects1[i].addForce(0, 30, 1);
}
}}

}


{

gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
gdjs.Fase_954Code.condition2IsTrue_0.val = false;
{
gdjs.Fase_954Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "reviver_chefe");
}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(3)) < 11 ) {
        gdjs.Fase_954Code.condition1IsTrue_0.val = true;
        gdjs.Fase_954Code.GDenemyObjects1[k] = gdjs.Fase_954Code.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDenemyObjects1.length = k;}if ( gdjs.Fase_954Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(3)) > 0 ) {
        gdjs.Fase_954Code.condition2IsTrue_0.val = true;
        gdjs.Fase_954Code.GDenemyObjects1[k] = gdjs.Fase_954Code.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDenemyObjects1.length = k;}}
}
if (gdjs.Fase_954Code.condition2IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(3), gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(3)) + (1));
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "reviver_chefe");
}}

}


{

gdjs.Fase_954Code.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));

gdjs.Fase_954Code.condition0IsTrue_0.val = false;
gdjs.Fase_954Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase_954Code.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.Fase_954Code.GDenemyObjects1[i].getVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(3)) == 5 ) {
        gdjs.Fase_954Code.condition0IsTrue_0.val = true;
        gdjs.Fase_954Code.GDenemyObjects1[k] = gdjs.Fase_954Code.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.Fase_954Code.GDenemyObjects1.length = k;}if ( gdjs.Fase_954Code.condition0IsTrue_0.val ) {
{
gdjs.Fase_954Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == -70;
}}
if (gdjs.Fase_954Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase_954Code.GDenemyObjects1.length ;i < len;++i) {
    gdjs.Fase_954Code.GDenemyObjects1[i].setVariableNumber(gdjs.Fase_954Code.GDenemyObjects1[i].getVariables().getFromIndex(2), 600);
}
}{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(-80);
}}

}

return;
}
gdjs['Fase_954Code']= gdjs.Fase_954Code;
