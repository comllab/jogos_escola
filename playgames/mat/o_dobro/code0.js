gdjs.StartCode = {};
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDlbl_95regrasObjects1= [];
gdjs.StartCode.GDlbl_95regrasObjects2= [];
gdjs.StartCode.GDgreen_95buttonObjects1= [];
gdjs.StartCode.GDgreen_95buttonObjects2= [];
gdjs.StartCode.GDlbl_95jogarObjects1= [];
gdjs.StartCode.GDlbl_95jogarObjects2= [];
gdjs.StartCode.GDlbl_95siteObjects1= [];
gdjs.StartCode.GDlbl_95siteObjects2= [];
gdjs.StartCode.GDlbl_95nivel_951Objects1= [];
gdjs.StartCode.GDlbl_95nivel_951Objects2= [];
gdjs.StartCode.GDlbl_95nivel_952Objects1= [];
gdjs.StartCode.GDlbl_95nivel_952Objects2= [];
gdjs.StartCode.GDlbl_95nivel_953Objects1= [];
gdjs.StartCode.GDlbl_95nivel_953Objects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDlbl_9595nivel_95951Objects1Objects = Hashtable.newFrom({"lbl_nivel_1": gdjs.StartCode.GDlbl_95nivel_951Objects1});gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDlbl_9595nivel_95952Objects1Objects = Hashtable.newFrom({"lbl_nivel_2": gdjs.StartCode.GDlbl_95nivel_952Objects1});gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDlbl_9595nivel_95953Objects1Objects = Hashtable.newFrom({"lbl_nivel_3": gdjs.StartCode.GDlbl_95nivel_953Objects1});gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDlbl_9595jogarObjects1Objects = Hashtable.newFrom({"lbl_jogar": gdjs.StartCode.GDlbl_95jogarObjects1});gdjs.StartCode.eventsList0x7b7350 = function(runtimeScene, context) {

{


{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.StartCode.eventsList0x7b7350
gdjs.StartCode.eventsList0xaf580 = function(runtimeScene, context) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "00 intro_0.ogg", true, 100, 1);
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 1;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDlbl_95nivel_951Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_1"));
gdjs.StartCode.GDlbl_95nivel_952Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_2"));
gdjs.StartCode.GDlbl_95nivel_953Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_3"));
{for(var i = 0, len = gdjs.StartCode.GDlbl_95nivel_951Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDlbl_95nivel_951Objects1[i].setOpacity(255);
}
}{for(var i = 0, len = gdjs.StartCode.GDlbl_95nivel_952Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDlbl_95nivel_952Objects1[i].setOpacity(50);
}
}{for(var i = 0, len = gdjs.StartCode.GDlbl_95nivel_953Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDlbl_95nivel_953Objects1[i].setOpacity(50);
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 2;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDlbl_95nivel_951Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_1"));
gdjs.StartCode.GDlbl_95nivel_952Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_2"));
gdjs.StartCode.GDlbl_95nivel_953Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_3"));
{for(var i = 0, len = gdjs.StartCode.GDlbl_95nivel_951Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDlbl_95nivel_951Objects1[i].setOpacity(50);
}
}{for(var i = 0, len = gdjs.StartCode.GDlbl_95nivel_952Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDlbl_95nivel_952Objects1[i].setOpacity(255);
}
}{for(var i = 0, len = gdjs.StartCode.GDlbl_95nivel_953Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDlbl_95nivel_953Objects1[i].setOpacity(50);
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 3;
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDlbl_95nivel_951Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_1"));
gdjs.StartCode.GDlbl_95nivel_952Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_2"));
gdjs.StartCode.GDlbl_95nivel_953Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_3"));
{for(var i = 0, len = gdjs.StartCode.GDlbl_95nivel_951Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDlbl_95nivel_951Objects1[i].setOpacity(50);
}
}{for(var i = 0, len = gdjs.StartCode.GDlbl_95nivel_952Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDlbl_95nivel_952Objects1[i].setOpacity(50);
}
}{for(var i = 0, len = gdjs.StartCode.GDlbl_95nivel_953Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDlbl_95nivel_953Objects1[i].setOpacity(255);
}
}}

}


{

gdjs.StartCode.GDlbl_95nivel_951Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_1"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDlbl_9595nivel_95951Objects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Laser_04.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(1);
}}

}


{

gdjs.StartCode.GDlbl_95nivel_952Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_2"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDlbl_9595nivel_95952Objects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Laser_04.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(2);
}}

}


{

gdjs.StartCode.GDlbl_95nivel_953Objects1.createFrom(runtimeScene.getObjects("lbl_nivel_3"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDlbl_9595nivel_95953Objects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Laser_04.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(3);
}}

}


{

gdjs.StartCode.GDlbl_95jogarObjects1.createFrom(runtimeScene.getObjects("lbl_jogar"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDlbl_9595jogarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.StartCode.eventsList0x7b7350(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.StartCode.eventsList0xaf580


gdjs.StartCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDlbl_95regrasObjects1.length = 0;
gdjs.StartCode.GDlbl_95regrasObjects2.length = 0;
gdjs.StartCode.GDgreen_95buttonObjects1.length = 0;
gdjs.StartCode.GDgreen_95buttonObjects2.length = 0;
gdjs.StartCode.GDlbl_95jogarObjects1.length = 0;
gdjs.StartCode.GDlbl_95jogarObjects2.length = 0;
gdjs.StartCode.GDlbl_95siteObjects1.length = 0;
gdjs.StartCode.GDlbl_95siteObjects2.length = 0;
gdjs.StartCode.GDlbl_95nivel_951Objects1.length = 0;
gdjs.StartCode.GDlbl_95nivel_951Objects2.length = 0;
gdjs.StartCode.GDlbl_95nivel_952Objects1.length = 0;
gdjs.StartCode.GDlbl_95nivel_952Objects2.length = 0;
gdjs.StartCode.GDlbl_95nivel_953Objects1.length = 0;
gdjs.StartCode.GDlbl_95nivel_953Objects2.length = 0;

gdjs.StartCode.eventsList0xaf580(runtimeScene, context);return;
}
gdjs['StartCode']= gdjs.StartCode;
