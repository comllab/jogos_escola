gdjs.ProximaFaseCode = {};


gdjs.ProximaFaseCode.GDfundoObjects1= [];
gdjs.ProximaFaseCode.GDfaseObjects1= [];
gdjs.ProximaFaseCode.GDconcluidaObjects1= [];
gdjs.ProximaFaseCode.GDbtnProximaObjects1= [];
gdjs.ProximaFaseCode.GDtxtProximaObjects1= [];

gdjs.ProximaFaseCode.conditionTrue_0 = {val:false};
gdjs.ProximaFaseCode.condition0IsTrue_0 = {val:false};
gdjs.ProximaFaseCode.condition1IsTrue_0 = {val:false};
gdjs.ProximaFaseCode.condition2IsTrue_0 = {val:false};

gdjs.ProximaFaseCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.ProximaFaseCode.GDfundoObjects1.length = 0;
gdjs.ProximaFaseCode.GDfaseObjects1.length = 0;
gdjs.ProximaFaseCode.GDconcluidaObjects1.length = 0;
gdjs.ProximaFaseCode.GDbtnProximaObjects1.length = 0;
gdjs.ProximaFaseCode.GDtxtProximaObjects1.length = 0;


{

gdjs.ProximaFaseCode.GDfundoObjects1.createFrom(runtimeScene.getObjects("fundo"));

{for(var i = 0, len = gdjs.ProximaFaseCode.GDfundoObjects1.length ;i < len;++i) {
    gdjs.ProximaFaseCode.GDfundoObjects1[i].setYOffset(gdjs.ProximaFaseCode.GDfundoObjects1[i].getYOffset() + (5));
}
}
}


{

gdjs.ProximaFaseCode.GDbtnProximaObjects1.createFrom(runtimeScene.getObjects("btnProxima"));
gdjs.ProximaFaseCode.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));
gdjs.ProximaFaseCode.GDtxtProximaObjects1.createFrom(runtimeScene.getObjects("txtProxima"));

gdjs.ProximaFaseCode.condition0IsTrue_0.val = false;
{
gdjs.ProximaFaseCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.ProximaFaseCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.ProximaFaseCode.GDfaseObjects1.length ;i < len;++i) {
    gdjs.ProximaFaseCode.GDfaseObjects1[i].setString("FASE " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(3)));
}
}{for(var i = 0, len = gdjs.ProximaFaseCode.GDtxtProximaObjects1.length ;i < len;++i) {
    gdjs.ProximaFaseCode.GDtxtProximaObjects1[i].setString("FASE " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) + 1));
}
}{for(var i = 0, len = gdjs.ProximaFaseCode.GDtxtProximaObjects1.length ;i < len;++i) {
    gdjs.ProximaFaseCode.GDtxtProximaObjects1[i].setPosition((( gdjs.ProximaFaseCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.ProximaFaseCode.GDbtnProximaObjects1[0].getPointX("")) + (( gdjs.ProximaFaseCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.ProximaFaseCode.GDbtnProximaObjects1[0].getWidth())/2 - (gdjs.ProximaFaseCode.GDtxtProximaObjects1[i].getWidth())/2,(( gdjs.ProximaFaseCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.ProximaFaseCode.GDbtnProximaObjects1[0].getPointY("")) + (( gdjs.ProximaFaseCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.ProximaFaseCode.GDbtnProximaObjects1[0].getHeight())/2 - (gdjs.ProximaFaseCode.GDtxtProximaObjects1[i].getHeight())/2);
}
}{gdjs.evtTools.sound.playMusic(runtimeScene, "1x level win.ogg", true, 100, 1);
}}

}


{

gdjs.ProximaFaseCode.GDbtnProximaObjects1.createFrom(runtimeScene.getObjects("btnProxima"));

gdjs.ProximaFaseCode.condition0IsTrue_0.val = false;
gdjs.ProximaFaseCode.condition1IsTrue_0.val = false;
{
gdjs.ProximaFaseCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("btnProxima", gdjs.ProximaFaseCode.GDbtnProximaObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if ( gdjs.ProximaFaseCode.condition0IsTrue_0.val ) {
{
gdjs.ProximaFaseCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.ProximaFaseCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).add(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Fase_" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(3)), false);
}}

}

return;
}
gdjs['ProximaFaseCode']= gdjs.ProximaFaseCode;
