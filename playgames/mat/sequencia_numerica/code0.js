gdjs.StartCode = {};
gdjs.StartCode.GDCar3Objects1= [];
gdjs.StartCode.GDCar3Objects2= [];
gdjs.StartCode.GDCar2Objects1= [];
gdjs.StartCode.GDCar2Objects2= [];
gdjs.StartCode.GDCar1Objects1= [];
gdjs.StartCode.GDCar1Objects2= [];
gdjs.StartCode.GDTire3Objects1= [];
gdjs.StartCode.GDTire3Objects2= [];
gdjs.StartCode.GDTire2Objects1= [];
gdjs.StartCode.GDTire2Objects2= [];
gdjs.StartCode.GDTire1Objects1= [];
gdjs.StartCode.GDTire1Objects2= [];
gdjs.StartCode.GDGroundObjects1= [];
gdjs.StartCode.GDGroundObjects2= [];
gdjs.StartCode.GDDebugObjects1= [];
gdjs.StartCode.GDDebugObjects2= [];
gdjs.StartCode.GDBtnLeftObjects1= [];
gdjs.StartCode.GDBtnLeftObjects2= [];
gdjs.StartCode.GDBtnRightObjects1= [];
gdjs.StartCode.GDBtnRightObjects2= [];
gdjs.StartCode.GDgroundBObjects1= [];
gdjs.StartCode.GDgroundBObjects2= [];
gdjs.StartCode.GDgroundObjects1= [];
gdjs.StartCode.GDgroundObjects2= [];
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDblSelecione2Objects1= [];
gdjs.StartCode.GDblSelecione2Objects2= [];
gdjs.StartCode.GDlblSelecioneObjects1= [];
gdjs.StartCode.GDlblSelecioneObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};
gdjs.StartCode.condition3IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDCar1Objects1Objects = Hashtable.newFrom({"Car1": gdjs.StartCode.GDCar1Objects1});gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDCar2Objects1Objects = Hashtable.newFrom({"Car2": gdjs.StartCode.GDCar2Objects1});gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDCar3Objects1Objects = Hashtable.newFrom({"Car3": gdjs.StartCode.GDCar3Objects1});gdjs.StartCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDCar1Objects1.createFrom(runtimeScene.getObjects("Car1"));
gdjs.StartCode.GDCar2Objects1.createFrom(runtimeScene.getObjects("Car2"));
gdjs.StartCode.GDCar3Objects1.createFrom(runtimeScene.getObjects("Car3"));
gdjs.StartCode.GDTire1Objects1.createFrom(runtimeScene.getObjects("Tire1"));
gdjs.StartCode.GDTire2Objects1.createFrom(runtimeScene.getObjects("Tire2"));
gdjs.StartCode.GDTire3Objects1.createFrom(runtimeScene.getObjects("Tire3"));
{for(var i = 0, len = gdjs.StartCode.GDTire1Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDTire1Objects1[i].getBehavior("Physics").addRevoluteJointBetweenObjects((gdjs.StartCode.GDCar1Objects1.length !== 0 ? gdjs.StartCode.GDCar1Objects1[0] : null), runtimeScene, 0, 0);
}
}{for(var i = 0, len = gdjs.StartCode.GDTire2Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDTire2Objects1[i].getBehavior("Physics").addRevoluteJointBetweenObjects((gdjs.StartCode.GDCar2Objects1.length !== 0 ? gdjs.StartCode.GDCar2Objects1[0] : null), runtimeScene, 0, 0);
}
}{for(var i = 0, len = gdjs.StartCode.GDTire3Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDTire3Objects1[i].getBehavior("Physics").addRevoluteJointBetweenObjects((gdjs.StartCode.GDCar3Objects1.length !== 0 ? gdjs.StartCode.GDCar3Objects1[0] : null), runtimeScene, 0, 0);
}
}{gdjs.evtTools.sound.playMusic(runtimeScene, "10 running wild.ogg", true, 100, 1);
}}

}


{

gdjs.StartCode.GDCar1Objects1.createFrom(runtimeScene.getObjects("Car1"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDCar1Objects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7106228);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{

gdjs.StartCode.GDCar2Objects1.createFrom(runtimeScene.getObjects("Car2"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDCar2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7107236);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(2);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{

gdjs.StartCode.GDCar3Objects1.createFrom(runtimeScene.getObjects("Car3"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDCar3Objects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7108236);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(3);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xaff48


gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.StartCode.GDCar3Objects1.length = 0;
gdjs.StartCode.GDCar3Objects2.length = 0;
gdjs.StartCode.GDCar2Objects1.length = 0;
gdjs.StartCode.GDCar2Objects2.length = 0;
gdjs.StartCode.GDCar1Objects1.length = 0;
gdjs.StartCode.GDCar1Objects2.length = 0;
gdjs.StartCode.GDTire3Objects1.length = 0;
gdjs.StartCode.GDTire3Objects2.length = 0;
gdjs.StartCode.GDTire2Objects1.length = 0;
gdjs.StartCode.GDTire2Objects2.length = 0;
gdjs.StartCode.GDTire1Objects1.length = 0;
gdjs.StartCode.GDTire1Objects2.length = 0;
gdjs.StartCode.GDGroundObjects1.length = 0;
gdjs.StartCode.GDGroundObjects2.length = 0;
gdjs.StartCode.GDDebugObjects1.length = 0;
gdjs.StartCode.GDDebugObjects2.length = 0;
gdjs.StartCode.GDBtnLeftObjects1.length = 0;
gdjs.StartCode.GDBtnLeftObjects2.length = 0;
gdjs.StartCode.GDBtnRightObjects1.length = 0;
gdjs.StartCode.GDBtnRightObjects2.length = 0;
gdjs.StartCode.GDgroundBObjects1.length = 0;
gdjs.StartCode.GDgroundBObjects2.length = 0;
gdjs.StartCode.GDgroundObjects1.length = 0;
gdjs.StartCode.GDgroundObjects2.length = 0;
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDblSelecione2Objects1.length = 0;
gdjs.StartCode.GDblSelecione2Objects2.length = 0;
gdjs.StartCode.GDlblSelecioneObjects1.length = 0;
gdjs.StartCode.GDlblSelecioneObjects2.length = 0;

gdjs.StartCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['StartCode'] = gdjs.StartCode;
