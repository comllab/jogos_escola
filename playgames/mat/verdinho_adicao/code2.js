gdjs.CompletouNivelCode = {};
gdjs.CompletouNivelCode.GDplataforma1Objects1= [];
gdjs.CompletouNivelCode.GDplataforma1Objects2= [];
gdjs.CompletouNivelCode.GDplataforma1Objects3= [];
gdjs.CompletouNivelCode.GDverdinhoObjects1= [];
gdjs.CompletouNivelCode.GDverdinhoObjects2= [];
gdjs.CompletouNivelCode.GDverdinhoObjects3= [];
gdjs.CompletouNivelCode.GDpanelObjects1= [];
gdjs.CompletouNivelCode.GDpanelObjects2= [];
gdjs.CompletouNivelCode.GDpanelObjects3= [];
gdjs.CompletouNivelCode.GDparabensObjects1= [];
gdjs.CompletouNivelCode.GDparabensObjects2= [];
gdjs.CompletouNivelCode.GDparabensObjects3= [];
gdjs.CompletouNivelCode.GDcompletouObjects1= [];
gdjs.CompletouNivelCode.GDcompletouObjects2= [];
gdjs.CompletouNivelCode.GDcompletouObjects3= [];
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects1= [];
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects2= [];
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects3= [];
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects1= [];
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects2= [];
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects3= [];
gdjs.CompletouNivelCode.GDbtVoltarObjects1= [];
gdjs.CompletouNivelCode.GDbtVoltarObjects2= [];
gdjs.CompletouNivelCode.GDbtVoltarObjects3= [];
gdjs.CompletouNivelCode.GDparedeObjects1= [];
gdjs.CompletouNivelCode.GDparedeObjects2= [];
gdjs.CompletouNivelCode.GDparedeObjects3= [];
gdjs.CompletouNivelCode.GDfadeObjects1= [];
gdjs.CompletouNivelCode.GDfadeObjects2= [];
gdjs.CompletouNivelCode.GDfadeObjects3= [];


gdjs.CompletouNivelCode.mapOfGDgdjs_9546CompletouNivelCode_9546GDtxtProximoNivelObjects1Objects = Hashtable.newFrom({"txtProximoNivel": gdjs.CompletouNivelCode.GDtxtProximoNivelObjects1});
gdjs.CompletouNivelCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) < 8;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(5).add(1);
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level", false);
}}

}


};gdjs.CompletouNivelCode.mapOfGDgdjs_9546CompletouNivelCode_9546GDtxtOutroNivelObjects1Objects = Hashtable.newFrom({"txtOutroNivel": gdjs.CompletouNivelCode.GDtxtOutroNivelObjects1});
gdjs.CompletouNivelCode.mapOfGDgdjs_9546CompletouNivelCode_9546GDfadeObjects1Objects = Hashtable.newFrom({"fade": gdjs.CompletouNivelCode.GDfadeObjects1});
gdjs.CompletouNivelCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Intro Theme.ogg", 0, true, 50, 1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "applause.ogg", true, 150, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("verdinho"), gdjs.CompletouNivelCode.GDverdinhoObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDverdinhoObjects1.length;i<l;++i) {
    if ( gdjs.CompletouNivelCode.GDverdinhoObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        isConditionTrue_0 = true;
        gdjs.CompletouNivelCode.GDverdinhoObjects1[k] = gdjs.CompletouNivelCode.GDverdinhoObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDverdinhoObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.CompletouNivelCode.GDverdinhoObjects1 */
{for(var i = 0, len = gdjs.CompletouNivelCode.GDverdinhoObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDverdinhoObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("verdinho"), gdjs.CompletouNivelCode.GDverdinhoObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDverdinhoObjects1.length;i<l;++i) {
    if ( !(gdjs.CompletouNivelCode.GDverdinhoObjects1[i].getBehavior("PlatformerObject").isFalling()) ) {
        isConditionTrue_0 = true;
        gdjs.CompletouNivelCode.GDverdinhoObjects1[k] = gdjs.CompletouNivelCode.GDverdinhoObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDverdinhoObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.CompletouNivelCode.GDverdinhoObjects1 */
{for(var i = 0, len = gdjs.CompletouNivelCode.GDverdinhoObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDverdinhoObjects1[i].setAnimation(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("txtProximoNivel"), gdjs.CompletouNivelCode.GDtxtProximoNivelObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.CompletouNivelCode.mapOfGDgdjs_9546CompletouNivelCode_9546GDtxtProximoNivelObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9091044);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.CompletouNivelCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("txtOutroNivel"), gdjs.CompletouNivelCode.GDtxtOutroNivelObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.CompletouNivelCode.mapOfGDgdjs_9546CompletouNivelCode_9546GDtxtOutroNivelObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9092988);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("verdinho"), gdjs.CompletouNivelCode.GDverdinhoObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDverdinhoObjects1.length;i<l;++i) {
    if ( gdjs.CompletouNivelCode.GDverdinhoObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        isConditionTrue_0 = true;
        gdjs.CompletouNivelCode.GDverdinhoObjects1[k] = gdjs.CompletouNivelCode.GDverdinhoObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDverdinhoObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.CompletouNivelCode.GDverdinhoObjects1 */
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "jump.wav", 0, false, 60, 1);
}{for(var i = 0, len = gdjs.CompletouNivelCode.GDverdinhoObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDverdinhoObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(7)) == 1;
if (isConditionTrue_0) {
gdjs.CompletouNivelCode.GDfadeObjects1.length = 0;

{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(0);
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.CompletouNivelCode.mapOfGDgdjs_9546CompletouNivelCode_9546GDfadeObjects1Objects, 0, 0, "GUI");
}{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].setScaleX(gdjs.evtTools.window.getWindowInnerWidth());
}
}{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].setScaleY(gdjs.evtTools.window.getWindowInnerHeight());
}
}{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].setOpacity(255);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("fade"), gdjs.CompletouNivelCode.GDfadeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.CompletouNivelCode.GDfadeObjects1[i].getOpacity() == 255 ) {
        isConditionTrue_0 = true;
        gdjs.CompletouNivelCode.GDfadeObjects1[k] = gdjs.CompletouNivelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDfadeObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.CompletouNivelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].setOpacity(254);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("fade"), gdjs.CompletouNivelCode.GDfadeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.CompletouNivelCode.GDfadeObjects1[i].getOpacity() < 255 ) {
        isConditionTrue_0 = true;
        gdjs.CompletouNivelCode.GDfadeObjects1[k] = gdjs.CompletouNivelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDfadeObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.CompletouNivelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].setOpacity(gdjs.CompletouNivelCode.GDfadeObjects1[i].getOpacity() - (255 * gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("fade"), gdjs.CompletouNivelCode.GDfadeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.CompletouNivelCode.GDfadeObjects1[i].getOpacity() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.CompletouNivelCode.GDfadeObjects1[k] = gdjs.CompletouNivelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDfadeObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.CompletouNivelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


};

gdjs.CompletouNivelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.CompletouNivelCode.GDplataforma1Objects1.length = 0;
gdjs.CompletouNivelCode.GDplataforma1Objects2.length = 0;
gdjs.CompletouNivelCode.GDplataforma1Objects3.length = 0;
gdjs.CompletouNivelCode.GDverdinhoObjects1.length = 0;
gdjs.CompletouNivelCode.GDverdinhoObjects2.length = 0;
gdjs.CompletouNivelCode.GDverdinhoObjects3.length = 0;
gdjs.CompletouNivelCode.GDpanelObjects1.length = 0;
gdjs.CompletouNivelCode.GDpanelObjects2.length = 0;
gdjs.CompletouNivelCode.GDpanelObjects3.length = 0;
gdjs.CompletouNivelCode.GDparabensObjects1.length = 0;
gdjs.CompletouNivelCode.GDparabensObjects2.length = 0;
gdjs.CompletouNivelCode.GDparabensObjects3.length = 0;
gdjs.CompletouNivelCode.GDcompletouObjects1.length = 0;
gdjs.CompletouNivelCode.GDcompletouObjects2.length = 0;
gdjs.CompletouNivelCode.GDcompletouObjects3.length = 0;
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects1.length = 0;
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects2.length = 0;
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects3.length = 0;
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects1.length = 0;
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects2.length = 0;
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects3.length = 0;
gdjs.CompletouNivelCode.GDbtVoltarObjects1.length = 0;
gdjs.CompletouNivelCode.GDbtVoltarObjects2.length = 0;
gdjs.CompletouNivelCode.GDbtVoltarObjects3.length = 0;
gdjs.CompletouNivelCode.GDparedeObjects1.length = 0;
gdjs.CompletouNivelCode.GDparedeObjects2.length = 0;
gdjs.CompletouNivelCode.GDparedeObjects3.length = 0;
gdjs.CompletouNivelCode.GDfadeObjects1.length = 0;
gdjs.CompletouNivelCode.GDfadeObjects2.length = 0;
gdjs.CompletouNivelCode.GDfadeObjects3.length = 0;

gdjs.CompletouNivelCode.eventsList1(runtimeScene);

return;

}

gdjs['CompletouNivelCode'] = gdjs.CompletouNivelCode;
