gdjs.GameCode = {};
gdjs.GameCode.forEachIndex2 = 0;

gdjs.GameCode.forEachObjects2 = [];

gdjs.GameCode.forEachTemporary2 = null;

gdjs.GameCode.forEachTotalCount2 = 0;

gdjs.GameCode.GDnumberObjects1= [];
gdjs.GameCode.GDnumberObjects2= [];
gdjs.GameCode.GDnumberObjects3= [];
gdjs.GameCode.GDlblPontosObjects1= [];
gdjs.GameCode.GDlblPontosObjects2= [];
gdjs.GameCode.GDlblPontosObjects3= [];
gdjs.GameCode.GDgameTitleObjects1= [];
gdjs.GameCode.GDgameTitleObjects2= [];
gdjs.GameCode.GDgameTitleObjects3= [];
gdjs.GameCode.GDlblResultObjects1= [];
gdjs.GameCode.GDlblResultObjects2= [];
gdjs.GameCode.GDlblResultObjects3= [];
gdjs.GameCode.GDbarraTempoObjects1= [];
gdjs.GameCode.GDbarraTempoObjects2= [];
gdjs.GameCode.GDbarraTempoObjects3= [];
gdjs.GameCode.GDpanelResultObjects1= [];
gdjs.GameCode.GDpanelResultObjects2= [];
gdjs.GameCode.GDpanelResultObjects3= [];
gdjs.GameCode.GDplacaObjects1= [];
gdjs.GameCode.GDplacaObjects2= [];
gdjs.GameCode.GDplacaObjects3= [];
gdjs.GameCode.GDescolaPlayObjects1= [];
gdjs.GameCode.GDescolaPlayObjects2= [];
gdjs.GameCode.GDescolaPlayObjects3= [];
gdjs.GameCode.GDsiteObjects1= [];
gdjs.GameCode.GDsiteObjects2= [];
gdjs.GameCode.GDsiteObjects3= [];
gdjs.GameCode.GDbackgroundObjects1= [];
gdjs.GameCode.GDbackgroundObjects2= [];
gdjs.GameCode.GDbackgroundObjects3= [];
gdjs.GameCode.GDNewObjectObjects1= [];
gdjs.GameCode.GDNewObjectObjects2= [];
gdjs.GameCode.GDNewObjectObjects3= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.condition4IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};
gdjs.GameCode.condition4IsTrue_1 = {val:false};


gdjs.GameCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.GameCode.GDnumberObjects1, gdjs.GameCode.GDnumberObjects2);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects2[i].isCurrentAnimationName("1") ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects2[k] = gdjs.GameCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDnumberObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].returnVariable(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)).setNumber(1);
}
}}

}


{

gdjs.copyArray(gdjs.GameCode.GDnumberObjects1, gdjs.GameCode.GDnumberObjects2);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects2[i].isCurrentAnimationName("2") ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects2[k] = gdjs.GameCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDnumberObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].returnVariable(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)).setNumber(2);
}
}}

}


{

gdjs.copyArray(gdjs.GameCode.GDnumberObjects1, gdjs.GameCode.GDnumberObjects2);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects2[i].isCurrentAnimationName("4") ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects2[k] = gdjs.GameCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDnumberObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].returnVariable(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)).setNumber(4);
}
}}

}


{

gdjs.copyArray(gdjs.GameCode.GDnumberObjects1, gdjs.GameCode.GDnumberObjects2);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects2[i].isCurrentAnimationName("5") ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects2[k] = gdjs.GameCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDnumberObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].returnVariable(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)).setNumber(5);
}
}}

}


{

gdjs.copyArray(gdjs.GameCode.GDnumberObjects1, gdjs.GameCode.GDnumberObjects2);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects2[i].isCurrentAnimationName("3") ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects2[k] = gdjs.GameCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDnumberObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].returnVariable(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)).setNumber(3);
}
}}

}


{

gdjs.copyArray(gdjs.GameCode.GDnumberObjects1, gdjs.GameCode.GDnumberObjects2);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects2[i].isCurrentAnimationName("6") ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects2[k] = gdjs.GameCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDnumberObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].returnVariable(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)).setNumber(6);
}
}}

}


{

gdjs.copyArray(gdjs.GameCode.GDnumberObjects1, gdjs.GameCode.GDnumberObjects2);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects2[i].isCurrentAnimationName("7") ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects2[k] = gdjs.GameCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDnumberObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].returnVariable(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)).setNumber(7);
}
}}

}


{

gdjs.copyArray(gdjs.GameCode.GDnumberObjects1, gdjs.GameCode.GDnumberObjects2);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects2[i].isCurrentAnimationName("8") ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects2[k] = gdjs.GameCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDnumberObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].returnVariable(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)).setNumber(8);
}
}}

}


{

/* Reuse gdjs.GameCode.GDnumberObjects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects1[i].isCurrentAnimationName("9") ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects1[k] = gdjs.GameCode.GDnumberObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDnumberObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects1[i].returnVariable(gdjs.GameCode.GDnumberObjects1[i].getVariables().getFromIndex(0)).setNumber(9);
}
}}

}


};gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDnumberObjects1Objects = Hashtable.newFrom({"number": gdjs.GameCode.GDnumberObjects1});gdjs.GameCode.eventsList1 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("number"), gdjs.GameCode.GDnumberObjects1);

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDnumberObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects1[i].isVisible() ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects1[k] = gdjs.GameCode.GDnumberObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects1.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) < 10;
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition3IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8357180);
}
}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDnumberObjects1 */
{runtimeScene.getGame().getVariables().getFromIndex(0).add((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDnumberObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDnumberObjects1[0].getVariables()).getFromIndex(0))));
}{for(var i = 0, len = gdjs.GameCode.GDnumberObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects1[i].returnVariable(gdjs.GameCode.GDnumberObjects1[i].getVariables().getFromIndex(0)).setNumber(0);
}
}{for(var i = 0, len = gdjs.GameCode.GDnumberObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects1[i].hide();
}
}}

}


};gdjs.GameCode.eventsList2 = function(runtimeScene) {

};gdjs.GameCode.eventsList3 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("number"), gdjs.GameCode.GDnumberObjects1);

for(gdjs.GameCode.forEachIndex2 = 0;gdjs.GameCode.forEachIndex2 < gdjs.GameCode.GDnumberObjects1.length;++gdjs.GameCode.forEachIndex2) {
gdjs.GameCode.GDnumberObjects2.length = 0;


gdjs.GameCode.forEachTemporary2 = gdjs.GameCode.GDnumberObjects1[gdjs.GameCode.forEachIndex2];
gdjs.GameCode.GDnumberObjects2.push(gdjs.GameCode.forEachTemporary2);
gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects2[i].getVariableNumber(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects2[k] = gdjs.GameCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].setAnimation(gdjs.random(8));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].returnVariable(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)).setNumber((gdjs.GameCode.GDnumberObjects2[i].getAnimation()) + 1);
}
}{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].hide(false);
}
}}
}

}


};gdjs.GameCode.eventsList4 = function(runtimeScene) {

};gdjs.GameCode.eventsList5 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("number"), gdjs.GameCode.GDnumberObjects1);

for(gdjs.GameCode.forEachIndex2 = 0;gdjs.GameCode.forEachIndex2 < gdjs.GameCode.GDnumberObjects1.length;++gdjs.GameCode.forEachIndex2) {
gdjs.GameCode.GDnumberObjects2.length = 0;


gdjs.GameCode.forEachTemporary2 = gdjs.GameCode.GDnumberObjects1[gdjs.GameCode.forEachIndex2];
gdjs.GameCode.GDnumberObjects2.push(gdjs.GameCode.forEachTemporary2);
gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDnumberObjects2[i].getVariableNumber(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnumberObjects2[k] = gdjs.GameCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDnumberObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects2[i].returnVariable(gdjs.GameCode.GDnumberObjects2[i].getVariables().getFromIndex(0)).setNumber(-(1));
}
}}
}

}


};gdjs.GameCode.eventsList6 = function(runtimeScene) {

{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("number"), gdjs.GameCode.GDnumberObjects1);
{for(var i = 0, len = gdjs.GameCode.GDnumberObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumberObjects1[i].setAnimation(gdjs.random(8));
}
}{gdjs.evtTools.sound.playMusic(runtimeScene, "SzymonMatuszewski-Art.ogg", true, 30, 1);
}
{ //Subevents
gdjs.GameCode.eventsList0(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) > 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lblPontos"), gdjs.GameCode.GDlblPontosObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(2).sub(10);
}{for(var i = 0, len = gdjs.GameCode.GDlblPontosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDlblPontosObjects1[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)));
}
}
{ //Subevents
gdjs.GameCode.eventsList3(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("barraTempo"), gdjs.GameCode.GDbarraTempoObjects1);
gdjs.copyArray(runtimeScene.getObjects("lblPontos"), gdjs.GameCode.GDlblPontosObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(2).add(10);
}{for(var i = 0, len = gdjs.GameCode.GDlblPontosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDlblPontosObjects1[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)));
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDbarraTempoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbarraTempoObjects1[i].setAnimationFrame(0);
}
}
{ //Subevents
gdjs.GameCode.eventsList5(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) < 10;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8362732);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lblPontos"), gdjs.GameCode.GDlblPontosObjects1);
{for(var i = 0, len = gdjs.GameCode.GDlblPontosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDlblPontosObjects1[i].setString("00" + gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) < 100;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= 10;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lblPontos"), gdjs.GameCode.GDlblPontosObjects1);
{for(var i = 0, len = gdjs.GameCode.GDlblPontosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDlblPontosObjects1[i].setString("0" + gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("lblResult"), gdjs.GameCode.GDlblResultObjects1);
{for(var i = 0, len = gdjs.GameCode.GDlblResultObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDlblResultObjects1[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("barraTempo"), gdjs.GameCode.GDbarraTempoObjects1);

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbarraTempoObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbarraTempoObjects1[i].hasAnimationEnded() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbarraTempoObjects1[k] = gdjs.GameCode.GDbarraTempoObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbarraTempoObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "EndGame", false);
}}

}


};

gdjs.GameCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.GameCode.GDnumberObjects1.length = 0;
gdjs.GameCode.GDnumberObjects2.length = 0;
gdjs.GameCode.GDnumberObjects3.length = 0;
gdjs.GameCode.GDlblPontosObjects1.length = 0;
gdjs.GameCode.GDlblPontosObjects2.length = 0;
gdjs.GameCode.GDlblPontosObjects3.length = 0;
gdjs.GameCode.GDgameTitleObjects1.length = 0;
gdjs.GameCode.GDgameTitleObjects2.length = 0;
gdjs.GameCode.GDgameTitleObjects3.length = 0;
gdjs.GameCode.GDlblResultObjects1.length = 0;
gdjs.GameCode.GDlblResultObjects2.length = 0;
gdjs.GameCode.GDlblResultObjects3.length = 0;
gdjs.GameCode.GDbarraTempoObjects1.length = 0;
gdjs.GameCode.GDbarraTempoObjects2.length = 0;
gdjs.GameCode.GDbarraTempoObjects3.length = 0;
gdjs.GameCode.GDpanelResultObjects1.length = 0;
gdjs.GameCode.GDpanelResultObjects2.length = 0;
gdjs.GameCode.GDpanelResultObjects3.length = 0;
gdjs.GameCode.GDplacaObjects1.length = 0;
gdjs.GameCode.GDplacaObjects2.length = 0;
gdjs.GameCode.GDplacaObjects3.length = 0;
gdjs.GameCode.GDescolaPlayObjects1.length = 0;
gdjs.GameCode.GDescolaPlayObjects2.length = 0;
gdjs.GameCode.GDescolaPlayObjects3.length = 0;
gdjs.GameCode.GDsiteObjects1.length = 0;
gdjs.GameCode.GDsiteObjects2.length = 0;
gdjs.GameCode.GDsiteObjects3.length = 0;
gdjs.GameCode.GDbackgroundObjects1.length = 0;
gdjs.GameCode.GDbackgroundObjects2.length = 0;
gdjs.GameCode.GDbackgroundObjects3.length = 0;
gdjs.GameCode.GDNewObjectObjects1.length = 0;
gdjs.GameCode.GDNewObjectObjects2.length = 0;
gdjs.GameCode.GDNewObjectObjects3.length = 0;

gdjs.GameCode.eventsList6(runtimeScene);
return;

}

gdjs['GameCode'] = gdjs.GameCode;
