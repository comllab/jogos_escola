gdjs.EndGameCode = {};
gdjs.EndGameCode.GDbackugroundObjects1= [];
gdjs.EndGameCode.GDbackugroundObjects2= [];
gdjs.EndGameCode.GDpanelObjects1= [];
gdjs.EndGameCode.GDpanelObjects2= [];
gdjs.EndGameCode.GDstarsObjects1= [];
gdjs.EndGameCode.GDstarsObjects2= [];
gdjs.EndGameCode.GDlblPontosObjects1= [];
gdjs.EndGameCode.GDlblPontosObjects2= [];
gdjs.EndGameCode.GDPONTOSObjects1= [];
gdjs.EndGameCode.GDPONTOSObjects2= [];
gdjs.EndGameCode.GDmsgObjects1= [];
gdjs.EndGameCode.GDmsgObjects2= [];
gdjs.EndGameCode.GDbntVoltarObjects1= [];
gdjs.EndGameCode.GDbntVoltarObjects2= [];
gdjs.EndGameCode.GDlblVoltarObjects1= [];
gdjs.EndGameCode.GDlblVoltarObjects2= [];

gdjs.EndGameCode.conditionTrue_0 = {val:false};
gdjs.EndGameCode.condition0IsTrue_0 = {val:false};
gdjs.EndGameCode.condition1IsTrue_0 = {val:false};
gdjs.EndGameCode.condition2IsTrue_0 = {val:false};
gdjs.EndGameCode.conditionTrue_1 = {val:false};
gdjs.EndGameCode.condition0IsTrue_1 = {val:false};
gdjs.EndGameCode.condition1IsTrue_1 = {val:false};
gdjs.EndGameCode.condition2IsTrue_1 = {val:false};


gdjs.EndGameCode.mapOfGDgdjs_46EndGameCode_46GDbntVoltarObjects1Objects = Hashtable.newFrom({"bntVoltar": gdjs.EndGameCode.GDbntVoltarObjects1});gdjs.EndGameCode.eventsList0 = function(runtimeScene) {

{


gdjs.EndGameCode.condition0IsTrue_0.val = false;
{
gdjs.EndGameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.EndGameCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("PONTOS"), gdjs.EndGameCode.GDPONTOSObjects1);
gdjs.copyArray(runtimeScene.getObjects("msg"), gdjs.EndGameCode.GDmsgObjects1);
{gdjs.evtTools.sound.playMusic(runtimeScene, "SzymonMatuszewski-Art.ogg", true, 50, 1);
}{for(var i = 0, len = gdjs.EndGameCode.GDPONTOSObjects1.length ;i < len;++i) {
    gdjs.EndGameCode.GDPONTOSObjects1[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)) + " PONTOS");
}
}{/* Unknown object - skipped. */}{for(var i = 0, len = gdjs.EndGameCode.GDmsgObjects1.length ;i < len;++i) {
    gdjs.EndGameCode.GDmsgObjects1[i].setString("NADA BOM!");
}
}}

}


{


gdjs.EndGameCode.condition0IsTrue_0.val = false;
{
gdjs.EndGameCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= 200;
}if (gdjs.EndGameCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("msg"), gdjs.EndGameCode.GDmsgObjects1);
gdjs.copyArray(runtimeScene.getObjects("stars"), gdjs.EndGameCode.GDstarsObjects1);
{for(var i = 0, len = gdjs.EndGameCode.GDstarsObjects1.length ;i < len;++i) {
    gdjs.EndGameCode.GDstarsObjects1[i].setAnimationName("3 stars");
}
}{for(var i = 0, len = gdjs.EndGameCode.GDmsgObjects1.length ;i < len;++i) {
    gdjs.EndGameCode.GDmsgObjects1[i].setString("EXCELENTE!");
}
}}

}


{


gdjs.EndGameCode.condition0IsTrue_0.val = false;
gdjs.EndGameCode.condition1IsTrue_0.val = false;
{
gdjs.EndGameCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) < 200;
}if ( gdjs.EndGameCode.condition0IsTrue_0.val ) {
{
{gdjs.EndGameCode.conditionTrue_1 = gdjs.EndGameCode.condition1IsTrue_0;
gdjs.EndGameCode.condition0IsTrue_1.val = false;
{
gdjs.EndGameCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= 100;
}gdjs.EndGameCode.conditionTrue_1.val = true && gdjs.EndGameCode.condition0IsTrue_1.val;
}
}}
if (gdjs.EndGameCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("msg"), gdjs.EndGameCode.GDmsgObjects1);
gdjs.copyArray(runtimeScene.getObjects("stars"), gdjs.EndGameCode.GDstarsObjects1);
{for(var i = 0, len = gdjs.EndGameCode.GDstarsObjects1.length ;i < len;++i) {
    gdjs.EndGameCode.GDstarsObjects1[i].setAnimationName("2 stars");
}
}{for(var i = 0, len = gdjs.EndGameCode.GDmsgObjects1.length ;i < len;++i) {
    gdjs.EndGameCode.GDmsgObjects1[i].setString("MUITO BOM!");
}
}}

}


{


gdjs.EndGameCode.condition0IsTrue_0.val = false;
gdjs.EndGameCode.condition1IsTrue_0.val = false;
{
gdjs.EndGameCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) < 100;
}if ( gdjs.EndGameCode.condition0IsTrue_0.val ) {
{
{gdjs.EndGameCode.conditionTrue_1 = gdjs.EndGameCode.condition1IsTrue_0;
gdjs.EndGameCode.condition0IsTrue_1.val = false;
{
gdjs.EndGameCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= 50;
}gdjs.EndGameCode.conditionTrue_1.val = true && gdjs.EndGameCode.condition0IsTrue_1.val;
}
}}
if (gdjs.EndGameCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("msg"), gdjs.EndGameCode.GDmsgObjects1);
gdjs.copyArray(runtimeScene.getObjects("stars"), gdjs.EndGameCode.GDstarsObjects1);
{for(var i = 0, len = gdjs.EndGameCode.GDstarsObjects1.length ;i < len;++i) {
    gdjs.EndGameCode.GDstarsObjects1[i].setAnimationName("1 star");
}
}{for(var i = 0, len = gdjs.EndGameCode.GDmsgObjects1.length ;i < len;++i) {
    gdjs.EndGameCode.GDmsgObjects1[i].setString(" BOM! ");
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bntVoltar"), gdjs.EndGameCode.GDbntVoltarObjects1);

gdjs.EndGameCode.condition0IsTrue_0.val = false;
gdjs.EndGameCode.condition1IsTrue_0.val = false;
{
gdjs.EndGameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.EndGameCode.condition0IsTrue_0.val ) {
{
gdjs.EndGameCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.EndGameCode.mapOfGDgdjs_46EndGameCode_46GDbntVoltarObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.EndGameCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


};

gdjs.EndGameCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.EndGameCode.GDbackugroundObjects1.length = 0;
gdjs.EndGameCode.GDbackugroundObjects2.length = 0;
gdjs.EndGameCode.GDpanelObjects1.length = 0;
gdjs.EndGameCode.GDpanelObjects2.length = 0;
gdjs.EndGameCode.GDstarsObjects1.length = 0;
gdjs.EndGameCode.GDstarsObjects2.length = 0;
gdjs.EndGameCode.GDlblPontosObjects1.length = 0;
gdjs.EndGameCode.GDlblPontosObjects2.length = 0;
gdjs.EndGameCode.GDPONTOSObjects1.length = 0;
gdjs.EndGameCode.GDPONTOSObjects2.length = 0;
gdjs.EndGameCode.GDmsgObjects1.length = 0;
gdjs.EndGameCode.GDmsgObjects2.length = 0;
gdjs.EndGameCode.GDbntVoltarObjects1.length = 0;
gdjs.EndGameCode.GDbntVoltarObjects2.length = 0;
gdjs.EndGameCode.GDlblVoltarObjects1.length = 0;
gdjs.EndGameCode.GDlblVoltarObjects2.length = 0;

gdjs.EndGameCode.eventsList0(runtimeScene);
return;

}

gdjs['EndGameCode'] = gdjs.EndGameCode;
