gdjs.GameCode = {};
gdjs.GameCode.repeatCount3 = 0;

gdjs.GameCode.repeatIndex3 = 0;

gdjs.GameCode.stopDoWhile2 = false;

gdjs.GameCode.GDbackgroundObjects1= [];
gdjs.GameCode.GDbackgroundObjects2= [];
gdjs.GameCode.GDbackgroundObjects3= [];
gdjs.GameCode.GDbackgroundObjects4= [];
gdjs.GameCode.GDbackgroundObjects5= [];
gdjs.GameCode.GDbtJogarObjects1= [];
gdjs.GameCode.GDbtJogarObjects2= [];
gdjs.GameCode.GDbtJogarObjects3= [];
gdjs.GameCode.GDbtJogarObjects4= [];
gdjs.GameCode.GDbtJogarObjects5= [];
gdjs.GameCode.GDcartasObjects1= [];
gdjs.GameCode.GDcartasObjects2= [];
gdjs.GameCode.GDcartasObjects3= [];
gdjs.GameCode.GDcartasObjects4= [];
gdjs.GameCode.GDcartasObjects5= [];
gdjs.GameCode.GDdialogObjects1= [];
gdjs.GameCode.GDdialogObjects2= [];
gdjs.GameCode.GDdialogObjects3= [];
gdjs.GameCode.GDdialogObjects4= [];
gdjs.GameCode.GDdialogObjects5= [];
gdjs.GameCode.GDtempoObjects1= [];
gdjs.GameCode.GDtempoObjects2= [];
gdjs.GameCode.GDtempoObjects3= [];
gdjs.GameCode.GDtempoObjects4= [];
gdjs.GameCode.GDtempoObjects5= [];
gdjs.GameCode.GDlblJogarNovamenteObjects1= [];
gdjs.GameCode.GDlblJogarNovamenteObjects2= [];
gdjs.GameCode.GDlblJogarNovamenteObjects3= [];
gdjs.GameCode.GDlblJogarNovamenteObjects4= [];
gdjs.GameCode.GDlblJogarNovamenteObjects5= [];
gdjs.GameCode.GDlblSeuTempoObjects1= [];
gdjs.GameCode.GDlblSeuTempoObjects2= [];
gdjs.GameCode.GDlblSeuTempoObjects3= [];
gdjs.GameCode.GDlblSeuTempoObjects4= [];
gdjs.GameCode.GDlblSeuTempoObjects5= [];
gdjs.GameCode.GDlblVenceuObjects1= [];
gdjs.GameCode.GDlblVenceuObjects2= [];
gdjs.GameCode.GDlblVenceuObjects3= [];
gdjs.GameCode.GDlblVenceuObjects4= [];
gdjs.GameCode.GDlblVenceuObjects5= [];
gdjs.GameCode.GDlblParabensObjects1= [];
gdjs.GameCode.GDlblParabensObjects2= [];
gdjs.GameCode.GDlblParabensObjects3= [];
gdjs.GameCode.GDlblParabensObjects4= [];
gdjs.GameCode.GDlblParabensObjects5= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.condition4IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};
gdjs.GameCode.condition4IsTrue_1 = {val:false};


gdjs.GameCode.eventsList0x66b048 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 59;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).add(1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}}

}


}; //End of gdjs.GameCode.eventsList0x66b048
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcartasObjects1Objects = Hashtable.newFrom({"cartas": gdjs.GameCode.GDcartasObjects1});gdjs.GameCode.eventsList0x66c5b8 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) > 0;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDcartasObjects2.createFrom(gdjs.GameCode.GDcartasObjects1);

{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDcartasObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDcartasObjects2[0].getVariables()).getFromIndex(0))));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects1 */
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDcartasObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDcartasObjects1[0].getVariables()).getFromIndex(0))));
}}

}


}; //End of gdjs.GameCode.eventsList0x66c5b8
gdjs.GameCode.eventsList0x66d450 = function(runtimeScene, context) {

{

gdjs.GameCode.GDcartasObjects2.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects2[i].getVariableNumber(gdjs.GameCode.GDcartasObjects2[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects2[k] = gdjs.GameCode.GDcartasObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects2[i].setAnimation(0);
}
}}

}


{

gdjs.GameCode.GDcartasObjects1.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects1[i].getVariableNumber(gdjs.GameCode.GDcartasObjects1[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects1[k] = gdjs.GameCode.GDcartasObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects1[i].setAnimation(0);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.runtimeScene.removeTimer(runtimeScene, "virar_carta");
}}

}


}; //End of gdjs.GameCode.eventsList0x66d450
gdjs.GameCode.eventsList0x66cdf8 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1));
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}{gdjs.evtTools.runtimeScene.removeTimer(runtimeScene, "virar_carta");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = !(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)));
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x66d450(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x66cdf8
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbtJogarObjects1Objects = Hashtable.newFrom({"btJogar": gdjs.GameCode.GDbtJogarObjects1});gdjs.GameCode.eventsList0x6f2de8 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("carta")) > 28;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("carta").setNumber(1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("c")) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("carta")));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("c")) == 2;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("carta")));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("c")) == 3;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(2).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("carta")));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("c")) == 4;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(3).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("carta")));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("c")) == 5;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(4).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("carta")));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("c")) == 6;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(5).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("carta")));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("c")) == 7;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(6).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("carta")));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("c")) == 8;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(7).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("carta")));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("c")) == 9;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(8).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("carta")));
}}

}


}; //End of gdjs.GameCode.eventsList0x6f2de8
gdjs.GameCode.eventsList0x66fa30 = function(runtimeScene, context) {

{

gdjs.GameCode.GDcartasObjects3.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 1;
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 2;
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("p")) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects3 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects3[i].returnVariable(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{runtimeScene.getVariables().get("posicoes").add(1);
}}

}


{

gdjs.GameCode.GDcartasObjects3.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 3;
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 4;
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("p")) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects3 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects3[i].returnVariable(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)));
}
}{runtimeScene.getVariables().get("posicoes").add(1);
}}

}


{

gdjs.GameCode.GDcartasObjects3.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 5;
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 6;
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("p")) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects3 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects3[i].returnVariable(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)));
}
}{runtimeScene.getVariables().get("posicoes").add(1);
}}

}


{

gdjs.GameCode.GDcartasObjects3.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 7;
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 8;
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("p")) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects3 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects3[i].returnVariable(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().get("posicoes").add(1);
}}

}


{

gdjs.GameCode.GDcartasObjects3.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 9;
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 10;
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("p")) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects3 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects3[i].returnVariable(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(4)));
}
}{runtimeScene.getVariables().get("posicoes").add(1);
}}

}


{

gdjs.GameCode.GDcartasObjects3.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 11;
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 12;
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("p")) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects3 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects3[i].returnVariable(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(5)));
}
}{runtimeScene.getVariables().get("posicoes").add(1);
}}

}


{

gdjs.GameCode.GDcartasObjects3.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 13;
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 14;
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("p")) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects3 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects3[i].returnVariable(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(6)));
}
}{runtimeScene.getVariables().get("posicoes").add(1);
}}

}


{

gdjs.GameCode.GDcartasObjects3.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 15;
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 16;
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("p")) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects3 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects3[i].returnVariable(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(7)));
}
}{runtimeScene.getVariables().get("posicoes").add(1);
}}

}


{

gdjs.GameCode.GDcartasObjects3.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 17;
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) == 18;
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcartasObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDcartasObjects3[i].getVariableNumber(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("p")) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDcartasObjects3[k] = gdjs.GameCode.GDcartasObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDcartasObjects3.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects3 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects3[i].returnVariable(gdjs.GameCode.GDcartasObjects3[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(8)));
}
}{runtimeScene.getVariables().get("posicoes").add(1);
}}

}


}; //End of gdjs.GameCode.eventsList0x66fa30
gdjs.GameCode.eventsList0x66e0f0 = function(runtimeScene, context) {

{


gdjs.GameCode.repeatCount3 = 9;
for(gdjs.GameCode.repeatIndex3 = 0;gdjs.GameCode.repeatIndex3 < gdjs.GameCode.repeatCount3;++gdjs.GameCode.repeatIndex3) {

if (true)
{
{runtimeScene.getVariables().get("carta").add(1);
}{runtimeScene.getVariables().get("c").add(1);
}
{ //Subevents: 
gdjs.GameCode.eventsList0x6f2de8(runtimeScene, context);} //Subevents end.
}
}

}


{



}


{


gdjs.GameCode.stopDoWhile2 = false;
do {gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("posicoes")) <= 18;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
if (true) {
{runtimeScene.getVariables().get("p").setNumber(gdjs.random(17) + 1);
}
{ //Subevents: 
gdjs.GameCode.eventsList0x66fa30(runtimeScene, context);} //Subevents end.
}
} else gdjs.GameCode.stopDoWhile2 = true; 
} while ( !gdjs.GameCode.stopDoWhile2 );

}


}; //End of gdjs.GameCode.eventsList0x66e0f0
gdjs.GameCode.eventsList0xaf9c0 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.camera.hideLayer(runtimeScene, "Dialog");
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(0);
}{runtimeScene.getVariables().get("str_segundos").setString("00");
}{runtimeScene.getVariables().get("str_minutos").setString("00");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = !(gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Dialog"));
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 1, "tempo");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(6730156);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).add(1);
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "tempo");
}
{ //Subevents
gdjs.GameCode.eventsList0x66b048(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDcartasObjects1.createFrom(runtimeScene.getObjects("cartas"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcartasObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0;
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition3IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(6735852);
}
}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcartasObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDcartasObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcartasObjects1[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDcartasObjects1[i].getVariables().getFromIndex(0))));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x66c5b8(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) > 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.5, "virar_carta");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x66cdf8(runtimeScene, context);} //End of subevents
}

}


{

gdjs.GameCode.GDbtJogarObjects1.createFrom(runtimeScene.getObjects("btJogar"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Dialog");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbtJogarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{



}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("carta").setNumber(gdjs.random(27));
}{runtimeScene.getVariables().get("posicoes").setNumber(1);
}
{ //Subevents
gdjs.GameCode.eventsList0x66e0f0(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 9;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.removeTimer(runtimeScene, "tempo");
}{gdjs.evtTools.camera.showLayer(runtimeScene, "Dialog");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) < 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("str_segundos").setString("0" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3))));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) < 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("str_minutos").setString("0" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4))));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) >= 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("str_segundos").setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3))));
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) >= 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("str_minutos").setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4))));
}}

}


{


{
gdjs.GameCode.GDtempoObjects1.createFrom(runtimeScene.getObjects("tempo"));
{for(var i = 0, len = gdjs.GameCode.GDtempoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtempoObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("str_minutos")) + ":" + gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("str_segundos")));
}
}}

}


}; //End of gdjs.GameCode.eventsList0xaf9c0


gdjs.GameCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GameCode.GDbackgroundObjects1.length = 0;
gdjs.GameCode.GDbackgroundObjects2.length = 0;
gdjs.GameCode.GDbackgroundObjects3.length = 0;
gdjs.GameCode.GDbackgroundObjects4.length = 0;
gdjs.GameCode.GDbackgroundObjects5.length = 0;
gdjs.GameCode.GDbtJogarObjects1.length = 0;
gdjs.GameCode.GDbtJogarObjects2.length = 0;
gdjs.GameCode.GDbtJogarObjects3.length = 0;
gdjs.GameCode.GDbtJogarObjects4.length = 0;
gdjs.GameCode.GDbtJogarObjects5.length = 0;
gdjs.GameCode.GDcartasObjects1.length = 0;
gdjs.GameCode.GDcartasObjects2.length = 0;
gdjs.GameCode.GDcartasObjects3.length = 0;
gdjs.GameCode.GDcartasObjects4.length = 0;
gdjs.GameCode.GDcartasObjects5.length = 0;
gdjs.GameCode.GDdialogObjects1.length = 0;
gdjs.GameCode.GDdialogObjects2.length = 0;
gdjs.GameCode.GDdialogObjects3.length = 0;
gdjs.GameCode.GDdialogObjects4.length = 0;
gdjs.GameCode.GDdialogObjects5.length = 0;
gdjs.GameCode.GDtempoObjects1.length = 0;
gdjs.GameCode.GDtempoObjects2.length = 0;
gdjs.GameCode.GDtempoObjects3.length = 0;
gdjs.GameCode.GDtempoObjects4.length = 0;
gdjs.GameCode.GDtempoObjects5.length = 0;
gdjs.GameCode.GDlblJogarNovamenteObjects1.length = 0;
gdjs.GameCode.GDlblJogarNovamenteObjects2.length = 0;
gdjs.GameCode.GDlblJogarNovamenteObjects3.length = 0;
gdjs.GameCode.GDlblJogarNovamenteObjects4.length = 0;
gdjs.GameCode.GDlblJogarNovamenteObjects5.length = 0;
gdjs.GameCode.GDlblSeuTempoObjects1.length = 0;
gdjs.GameCode.GDlblSeuTempoObjects2.length = 0;
gdjs.GameCode.GDlblSeuTempoObjects3.length = 0;
gdjs.GameCode.GDlblSeuTempoObjects4.length = 0;
gdjs.GameCode.GDlblSeuTempoObjects5.length = 0;
gdjs.GameCode.GDlblVenceuObjects1.length = 0;
gdjs.GameCode.GDlblVenceuObjects2.length = 0;
gdjs.GameCode.GDlblVenceuObjects3.length = 0;
gdjs.GameCode.GDlblVenceuObjects4.length = 0;
gdjs.GameCode.GDlblVenceuObjects5.length = 0;
gdjs.GameCode.GDlblParabensObjects1.length = 0;
gdjs.GameCode.GDlblParabensObjects2.length = 0;
gdjs.GameCode.GDlblParabensObjects3.length = 0;
gdjs.GameCode.GDlblParabensObjects4.length = 0;
gdjs.GameCode.GDlblParabensObjects5.length = 0;

gdjs.GameCode.eventsList0xaf9c0(runtimeScene, context);return;
}
gdjs['GameCode']= gdjs.GameCode;
