gdjs.AjudaCode = {};
gdjs.AjudaCode.GDajudaObjects1= [];
gdjs.AjudaCode.GDajudaObjects2= [];
gdjs.AjudaCode.GDbtJogarObjects1= [];
gdjs.AjudaCode.GDbtJogarObjects2= [];
gdjs.AjudaCode.GDlblJogarObjects1= [];
gdjs.AjudaCode.GDlblJogarObjects2= [];
gdjs.AjudaCode.GDbackgroundObjects1= [];
gdjs.AjudaCode.GDbackgroundObjects2= [];
gdjs.AjudaCode.GDlblVoltarObjects1= [];
gdjs.AjudaCode.GDlblVoltarObjects2= [];
gdjs.AjudaCode.GDlblAjudaObjects1= [];
gdjs.AjudaCode.GDlblAjudaObjects2= [];

gdjs.AjudaCode.conditionTrue_0 = {val:false};
gdjs.AjudaCode.condition0IsTrue_0 = {val:false};
gdjs.AjudaCode.condition1IsTrue_0 = {val:false};
gdjs.AjudaCode.condition2IsTrue_0 = {val:false};
gdjs.AjudaCode.condition3IsTrue_0 = {val:false};
gdjs.AjudaCode.conditionTrue_1 = {val:false};
gdjs.AjudaCode.condition0IsTrue_1 = {val:false};
gdjs.AjudaCode.condition1IsTrue_1 = {val:false};
gdjs.AjudaCode.condition2IsTrue_1 = {val:false};
gdjs.AjudaCode.condition3IsTrue_1 = {val:false};


gdjs.AjudaCode.mapOfGDgdjs_46AjudaCode_46GDbtJogarObjects1Objects = Hashtable.newFrom({"btJogar": gdjs.AjudaCode.GDbtJogarObjects1});gdjs.AjudaCode.eventsList0xafd70 = function(runtimeScene, context) {

{

gdjs.AjudaCode.GDbtJogarObjects1.createFrom(runtimeScene.getObjects("btJogar"));

gdjs.AjudaCode.condition0IsTrue_0.val = false;
gdjs.AjudaCode.condition1IsTrue_0.val = false;
gdjs.AjudaCode.condition2IsTrue_0.val = false;
{
gdjs.AjudaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.AjudaCode.mapOfGDgdjs_46AjudaCode_46GDbtJogarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.AjudaCode.condition0IsTrue_0.val ) {
{
gdjs.AjudaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.AjudaCode.condition1IsTrue_0.val ) {
{
{gdjs.AjudaCode.conditionTrue_1 = gdjs.AjudaCode.condition2IsTrue_0;
gdjs.AjudaCode.conditionTrue_1.val = context.triggerOnce(7099308);
}
}}
}
if (gdjs.AjudaCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.popScene(runtimeScene);
}}

}


}; //End of gdjs.AjudaCode.eventsList0xafd70


gdjs.AjudaCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.AjudaCode.GDajudaObjects1.length = 0;
gdjs.AjudaCode.GDajudaObjects2.length = 0;
gdjs.AjudaCode.GDbtJogarObjects1.length = 0;
gdjs.AjudaCode.GDbtJogarObjects2.length = 0;
gdjs.AjudaCode.GDlblJogarObjects1.length = 0;
gdjs.AjudaCode.GDlblJogarObjects2.length = 0;
gdjs.AjudaCode.GDbackgroundObjects1.length = 0;
gdjs.AjudaCode.GDbackgroundObjects2.length = 0;
gdjs.AjudaCode.GDlblVoltarObjects1.length = 0;
gdjs.AjudaCode.GDlblVoltarObjects2.length = 0;
gdjs.AjudaCode.GDlblAjudaObjects1.length = 0;
gdjs.AjudaCode.GDlblAjudaObjects2.length = 0;

gdjs.AjudaCode.eventsList0xafd70(runtimeScene, context);return;
}
gdjs['AjudaCode']= gdjs.AjudaCode;
