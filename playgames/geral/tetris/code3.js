gdjs.AboutCode = {};
gdjs.AboutCode.GDbackgroundObjects1= [];
gdjs.AboutCode.GDbackgroundObjects2= [];
gdjs.AboutCode.GDbackgroundObjects3= [];
gdjs.AboutCode.GDnext_95pieceObjects1= [];
gdjs.AboutCode.GDnext_95pieceObjects2= [];
gdjs.AboutCode.GDnext_95pieceObjects3= [];
gdjs.AboutCode.GDhomeObjects1= [];
gdjs.AboutCode.GDhomeObjects2= [];
gdjs.AboutCode.GDhomeObjects3= [];
gdjs.AboutCode.GDbuyObjects1= [];
gdjs.AboutCode.GDbuyObjects2= [];
gdjs.AboutCode.GDbuyObjects3= [];
gdjs.AboutCode.GDemailObjects1= [];
gdjs.AboutCode.GDemailObjects2= [];
gdjs.AboutCode.GDemailObjects3= [];
gdjs.AboutCode.GDshareObjects1= [];
gdjs.AboutCode.GDshareObjects2= [];
gdjs.AboutCode.GDshareObjects3= [];
gdjs.AboutCode.GDpointsObjects1= [];
gdjs.AboutCode.GDpointsObjects2= [];
gdjs.AboutCode.GDpointsObjects3= [];
gdjs.AboutCode.GDstartObjects1= [];
gdjs.AboutCode.GDstartObjects2= [];
gdjs.AboutCode.GDstartObjects3= [];
gdjs.AboutCode.GDauthorObjects1= [];
gdjs.AboutCode.GDauthorObjects2= [];
gdjs.AboutCode.GDauthorObjects3= [];
gdjs.AboutCode.GDsiteObjects1= [];
gdjs.AboutCode.GDsiteObjects2= [];
gdjs.AboutCode.GDsiteObjects3= [];
gdjs.AboutCode.GDmy_95emailObjects1= [];
gdjs.AboutCode.GDmy_95emailObjects2= [];
gdjs.AboutCode.GDmy_95emailObjects3= [];

gdjs.AboutCode.conditionTrue_0 = {val:false};
gdjs.AboutCode.condition0IsTrue_0 = {val:false};
gdjs.AboutCode.condition1IsTrue_0 = {val:false};
gdjs.AboutCode.condition2IsTrue_0 = {val:false};


gdjs.AboutCode.eventsList0x89e7a0 = function(runtimeScene) {

{


gdjs.AboutCode.condition0IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 10;
}if (gdjs.AboutCode.condition0IsTrue_0.val) {
gdjs.AboutCode.GDpointsObjects2.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.AboutCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.AboutCode.GDpointsObjects2[i].setString("000000" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.AboutCode.condition0IsTrue_0.val = false;
gdjs.AboutCode.condition1IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 10;
}if ( gdjs.AboutCode.condition0IsTrue_0.val ) {
{
gdjs.AboutCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 100;
}}
if (gdjs.AboutCode.condition1IsTrue_0.val) {
gdjs.AboutCode.GDpointsObjects2.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.AboutCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.AboutCode.GDpointsObjects2[i].setString("00000" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.AboutCode.condition0IsTrue_0.val = false;
gdjs.AboutCode.condition1IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 100;
}if ( gdjs.AboutCode.condition0IsTrue_0.val ) {
{
gdjs.AboutCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 1000;
}}
if (gdjs.AboutCode.condition1IsTrue_0.val) {
gdjs.AboutCode.GDpointsObjects2.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.AboutCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.AboutCode.GDpointsObjects2[i].setString("0000" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.AboutCode.condition0IsTrue_0.val = false;
gdjs.AboutCode.condition1IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 1000;
}if ( gdjs.AboutCode.condition0IsTrue_0.val ) {
{
gdjs.AboutCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 10000;
}}
if (gdjs.AboutCode.condition1IsTrue_0.val) {
gdjs.AboutCode.GDpointsObjects2.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.AboutCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.AboutCode.GDpointsObjects2[i].setString("000" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.AboutCode.condition0IsTrue_0.val = false;
gdjs.AboutCode.condition1IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 10000;
}if ( gdjs.AboutCode.condition0IsTrue_0.val ) {
{
gdjs.AboutCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 100000;
}}
if (gdjs.AboutCode.condition1IsTrue_0.val) {
gdjs.AboutCode.GDpointsObjects2.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.AboutCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.AboutCode.GDpointsObjects2[i].setString("00" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.AboutCode.condition0IsTrue_0.val = false;
gdjs.AboutCode.condition1IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 100000;
}if ( gdjs.AboutCode.condition0IsTrue_0.val ) {
{
gdjs.AboutCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 1000000;
}}
if (gdjs.AboutCode.condition1IsTrue_0.val) {
gdjs.AboutCode.GDpointsObjects2.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.AboutCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.AboutCode.GDpointsObjects2[i].setString("0" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.AboutCode.condition0IsTrue_0.val = false;
gdjs.AboutCode.condition1IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 1000000;
}if ( gdjs.AboutCode.condition0IsTrue_0.val ) {
{
gdjs.AboutCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 10000000;
}}
if (gdjs.AboutCode.condition1IsTrue_0.val) {
gdjs.AboutCode.GDpointsObjects2.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.AboutCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.AboutCode.GDpointsObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


{
gdjs.AboutCode.GDpointsObjects1.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.AboutCode.GDpointsObjects1.length ;i < len;++i) {
    gdjs.AboutCode.GDpointsObjects1[i].setX(576 + (13 * 32 / 2) - (gdjs.AboutCode.GDpointsObjects1[i].getWidth()) / 2);
}
}}

}


}; //End of gdjs.AboutCode.eventsList0x89e7a0
gdjs.AboutCode.mapOfGDgdjs_46AboutCode_46GDsiteObjects1Objects = Hashtable.newFrom({"site": gdjs.AboutCode.GDsiteObjects1});gdjs.AboutCode.mapOfGDgdjs_46AboutCode_46GDshareObjects1Objects = Hashtable.newFrom({"share": gdjs.AboutCode.GDshareObjects1});gdjs.AboutCode.mapOfGDgdjs_46AboutCode_46GDbuyObjects1Objects = Hashtable.newFrom({"buy": gdjs.AboutCode.GDbuyObjects1});gdjs.AboutCode.mapOfGDgdjs_46AboutCode_46GDhomeObjects1Objects = Hashtable.newFrom({"home": gdjs.AboutCode.GDhomeObjects1});gdjs.AboutCode.eventsList0xb2358 = function(runtimeScene) {

{


gdjs.AboutCode.condition0IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.AboutCode.condition0IsTrue_0.val) {
gdjs.AboutCode.GDauthorObjects1.createFrom(runtimeScene.getObjects("author"));
gdjs.AboutCode.GDmy_95emailObjects1.createFrom(runtimeScene.getObjects("my_email"));
gdjs.AboutCode.GDsiteObjects1.createFrom(runtimeScene.getObjects("site"));
{for(var i = 0, len = gdjs.AboutCode.GDauthorObjects1.length ;i < len;++i) {
    gdjs.AboutCode.GDauthorObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene) / 2 - (gdjs.AboutCode.GDauthorObjects1[i].getWidth()) / 2);
}
}{for(var i = 0, len = gdjs.AboutCode.GDsiteObjects1.length ;i < len;++i) {
    gdjs.AboutCode.GDsiteObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene) / 2 - (gdjs.AboutCode.GDsiteObjects1[i].getWidth()) / 2);
}
}{for(var i = 0, len = gdjs.AboutCode.GDmy_95emailObjects1.length ;i < len;++i) {
    gdjs.AboutCode.GDmy_95emailObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene) / 2 - (gdjs.AboutCode.GDmy_95emailObjects1[i].getWidth()) / 2);
}
}
{ //Subevents
gdjs.AboutCode.eventsList0x89e7a0(runtimeScene);} //End of subevents
}

}


{

gdjs.AboutCode.GDsiteObjects1.createFrom(runtimeScene.getObjects("site"));

gdjs.AboutCode.condition0IsTrue_0.val = false;
gdjs.AboutCode.condition1IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.AboutCode.mapOfGDgdjs_46AboutCode_46GDsiteObjects1Objects, runtimeScene, true, false);
}if ( gdjs.AboutCode.condition0IsTrue_0.val ) {
{
gdjs.AboutCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.AboutCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("http://www.escolaplay.com.br/jogos_.html", runtimeScene);
}}

}


{

gdjs.AboutCode.GDshareObjects1.createFrom(runtimeScene.getObjects("share"));

gdjs.AboutCode.condition0IsTrue_0.val = false;
gdjs.AboutCode.condition1IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.AboutCode.mapOfGDgdjs_46AboutCode_46GDshareObjects1Objects, runtimeScene, true, false);
}if ( gdjs.AboutCode.condition0IsTrue_0.val ) {
{
gdjs.AboutCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.AboutCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("https://www.facebook.com/marcelomdea", runtimeScene);
}}

}


{

gdjs.AboutCode.GDbuyObjects1.createFrom(runtimeScene.getObjects("buy"));

gdjs.AboutCode.condition0IsTrue_0.val = false;
gdjs.AboutCode.condition1IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.AboutCode.mapOfGDgdjs_46AboutCode_46GDbuyObjects1Objects, runtimeScene, true, false);
}if ( gdjs.AboutCode.condition0IsTrue_0.val ) {
{
gdjs.AboutCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.AboutCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("http://www.escolaplay.com.br", runtimeScene);
}}

}


{

gdjs.AboutCode.GDhomeObjects1.createFrom(runtimeScene.getObjects("home"));

gdjs.AboutCode.condition0IsTrue_0.val = false;
gdjs.AboutCode.condition1IsTrue_0.val = false;
{
gdjs.AboutCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.AboutCode.mapOfGDgdjs_46AboutCode_46GDhomeObjects1Objects, runtimeScene, true, false);
}if ( gdjs.AboutCode.condition0IsTrue_0.val ) {
{
gdjs.AboutCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.AboutCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


}; //End of gdjs.AboutCode.eventsList0xb2358


gdjs.AboutCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.AboutCode.GDbackgroundObjects1.length = 0;
gdjs.AboutCode.GDbackgroundObjects2.length = 0;
gdjs.AboutCode.GDbackgroundObjects3.length = 0;
gdjs.AboutCode.GDnext_95pieceObjects1.length = 0;
gdjs.AboutCode.GDnext_95pieceObjects2.length = 0;
gdjs.AboutCode.GDnext_95pieceObjects3.length = 0;
gdjs.AboutCode.GDhomeObjects1.length = 0;
gdjs.AboutCode.GDhomeObjects2.length = 0;
gdjs.AboutCode.GDhomeObjects3.length = 0;
gdjs.AboutCode.GDbuyObjects1.length = 0;
gdjs.AboutCode.GDbuyObjects2.length = 0;
gdjs.AboutCode.GDbuyObjects3.length = 0;
gdjs.AboutCode.GDemailObjects1.length = 0;
gdjs.AboutCode.GDemailObjects2.length = 0;
gdjs.AboutCode.GDemailObjects3.length = 0;
gdjs.AboutCode.GDshareObjects1.length = 0;
gdjs.AboutCode.GDshareObjects2.length = 0;
gdjs.AboutCode.GDshareObjects3.length = 0;
gdjs.AboutCode.GDpointsObjects1.length = 0;
gdjs.AboutCode.GDpointsObjects2.length = 0;
gdjs.AboutCode.GDpointsObjects3.length = 0;
gdjs.AboutCode.GDstartObjects1.length = 0;
gdjs.AboutCode.GDstartObjects2.length = 0;
gdjs.AboutCode.GDstartObjects3.length = 0;
gdjs.AboutCode.GDauthorObjects1.length = 0;
gdjs.AboutCode.GDauthorObjects2.length = 0;
gdjs.AboutCode.GDauthorObjects3.length = 0;
gdjs.AboutCode.GDsiteObjects1.length = 0;
gdjs.AboutCode.GDsiteObjects2.length = 0;
gdjs.AboutCode.GDsiteObjects3.length = 0;
gdjs.AboutCode.GDmy_95emailObjects1.length = 0;
gdjs.AboutCode.GDmy_95emailObjects2.length = 0;
gdjs.AboutCode.GDmy_95emailObjects3.length = 0;

gdjs.AboutCode.eventsList0xb2358(runtimeScene);
return;
}
gdjs['AboutCode'] = gdjs.AboutCode;
