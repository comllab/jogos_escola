gdjs.StartCode = {};
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDnext_95pieceObjects1= [];
gdjs.StartCode.GDnext_95pieceObjects2= [];
gdjs.StartCode.GDhomeObjects1= [];
gdjs.StartCode.GDhomeObjects2= [];
gdjs.StartCode.GDbuyObjects1= [];
gdjs.StartCode.GDbuyObjects2= [];
gdjs.StartCode.GDemailObjects1= [];
gdjs.StartCode.GDemailObjects2= [];
gdjs.StartCode.GDshareObjects1= [];
gdjs.StartCode.GDshareObjects2= [];
gdjs.StartCode.GDpointsObjects1= [];
gdjs.StartCode.GDpointsObjects2= [];
gdjs.StartCode.GDstartObjects1= [];
gdjs.StartCode.GDstartObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDstartObjects1Objects = Hashtable.newFrom({"start": gdjs.StartCode.GDstartObjects1});gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDshareObjects1Objects = Hashtable.newFrom({"share": gdjs.StartCode.GDshareObjects1});gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbuyObjects1Objects = Hashtable.newFrom({"buy": gdjs.StartCode.GDbuyObjects1});gdjs.StartCode.eventsList0xb2358 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDstartObjects1.createFrom(runtimeScene.getObjects("start"));
{for(var i = 0, len = gdjs.StartCode.GDstartObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDstartObjects1[i].setPosition(gdjs.evtTools.window.getCanvasWidth(runtimeScene) / 2 - (gdjs.StartCode.GDstartObjects1[i].getWidth()) / 2,gdjs.evtTools.window.getCanvasHeight(runtimeScene) / 2 - (gdjs.StartCode.GDstartObjects1[i].getHeight()) / 2);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Dungeon Theme.wav", true, 100, 1);
}}

}


{

gdjs.StartCode.GDstartObjects1.createFrom(runtimeScene.getObjects("start"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDstartObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{

gdjs.StartCode.GDshareObjects1.createFrom(runtimeScene.getObjects("share"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDshareObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("https://www.facebook.com/marcelomdea", runtimeScene);
}}

}


{

gdjs.StartCode.GDbuyObjects1.createFrom(runtimeScene.getObjects("buy"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbuyObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("http://www.escolaplay.com.br", runtimeScene);
}}

}


}; //End of gdjs.StartCode.eventsList0xb2358


gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDnext_95pieceObjects1.length = 0;
gdjs.StartCode.GDnext_95pieceObjects2.length = 0;
gdjs.StartCode.GDhomeObjects1.length = 0;
gdjs.StartCode.GDhomeObjects2.length = 0;
gdjs.StartCode.GDbuyObjects1.length = 0;
gdjs.StartCode.GDbuyObjects2.length = 0;
gdjs.StartCode.GDemailObjects1.length = 0;
gdjs.StartCode.GDemailObjects2.length = 0;
gdjs.StartCode.GDshareObjects1.length = 0;
gdjs.StartCode.GDshareObjects2.length = 0;
gdjs.StartCode.GDpointsObjects1.length = 0;
gdjs.StartCode.GDpointsObjects2.length = 0;
gdjs.StartCode.GDstartObjects1.length = 0;
gdjs.StartCode.GDstartObjects2.length = 0;

gdjs.StartCode.eventsList0xb2358(runtimeScene);
return;
}
gdjs['StartCode'] = gdjs.StartCode;
