gdjs.GameOverCode = {};
gdjs.GameOverCode.GDbackgroundObjects1= [];
gdjs.GameOverCode.GDbackgroundObjects2= [];
gdjs.GameOverCode.GDbackgroundObjects3= [];
gdjs.GameOverCode.GDnext_95pieceObjects1= [];
gdjs.GameOverCode.GDnext_95pieceObjects2= [];
gdjs.GameOverCode.GDnext_95pieceObjects3= [];
gdjs.GameOverCode.GDhomeObjects1= [];
gdjs.GameOverCode.GDhomeObjects2= [];
gdjs.GameOverCode.GDhomeObjects3= [];
gdjs.GameOverCode.GDbuyObjects1= [];
gdjs.GameOverCode.GDbuyObjects2= [];
gdjs.GameOverCode.GDbuyObjects3= [];
gdjs.GameOverCode.GDemailObjects1= [];
gdjs.GameOverCode.GDemailObjects2= [];
gdjs.GameOverCode.GDemailObjects3= [];
gdjs.GameOverCode.GDshareObjects1= [];
gdjs.GameOverCode.GDshareObjects2= [];
gdjs.GameOverCode.GDshareObjects3= [];
gdjs.GameOverCode.GDpointsObjects1= [];
gdjs.GameOverCode.GDpointsObjects2= [];
gdjs.GameOverCode.GDpointsObjects3= [];
gdjs.GameOverCode.GDstartObjects1= [];
gdjs.GameOverCode.GDstartObjects2= [];
gdjs.GameOverCode.GDstartObjects3= [];
gdjs.GameOverCode.GDNewObjectObjects1= [];
gdjs.GameOverCode.GDNewObjectObjects2= [];
gdjs.GameOverCode.GDNewObjectObjects3= [];
gdjs.GameOverCode.GDgameOverObjects1= [];
gdjs.GameOverCode.GDgameOverObjects2= [];
gdjs.GameOverCode.GDgameOverObjects3= [];

gdjs.GameOverCode.conditionTrue_0 = {val:false};
gdjs.GameOverCode.condition0IsTrue_0 = {val:false};
gdjs.GameOverCode.condition1IsTrue_0 = {val:false};
gdjs.GameOverCode.condition2IsTrue_0 = {val:false};


gdjs.GameOverCode.eventsList0x89e7a0 = function(runtimeScene) {

{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 10;
}if (gdjs.GameOverCode.condition0IsTrue_0.val) {
gdjs.GameOverCode.GDpointsObjects2.createFrom(gdjs.GameOverCode.GDpointsObjects1);

{for(var i = 0, len = gdjs.GameOverCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.GameOverCode.GDpointsObjects2[i].setString("000000" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 10;
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 100;
}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
gdjs.GameOverCode.GDpointsObjects2.createFrom(gdjs.GameOverCode.GDpointsObjects1);

{for(var i = 0, len = gdjs.GameOverCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.GameOverCode.GDpointsObjects2[i].setString("00000" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 100;
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 1000;
}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
gdjs.GameOverCode.GDpointsObjects2.createFrom(gdjs.GameOverCode.GDpointsObjects1);

{for(var i = 0, len = gdjs.GameOverCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.GameOverCode.GDpointsObjects2[i].setString("0000" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 1000;
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 10000;
}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
gdjs.GameOverCode.GDpointsObjects2.createFrom(gdjs.GameOverCode.GDpointsObjects1);

{for(var i = 0, len = gdjs.GameOverCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.GameOverCode.GDpointsObjects2[i].setString("000" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 10000;
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 100000;
}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
gdjs.GameOverCode.GDpointsObjects2.createFrom(gdjs.GameOverCode.GDpointsObjects1);

{for(var i = 0, len = gdjs.GameOverCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.GameOverCode.GDpointsObjects2[i].setString("00" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 100000;
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 1000000;
}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
gdjs.GameOverCode.GDpointsObjects2.createFrom(gdjs.GameOverCode.GDpointsObjects1);

{for(var i = 0, len = gdjs.GameOverCode.GDpointsObjects2.length ;i < len;++i) {
    gdjs.GameOverCode.GDpointsObjects2[i].setString("0" + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 1000000;
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 10000000;
}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameOverCode.GDpointsObjects1 */
{for(var i = 0, len = gdjs.GameOverCode.GDpointsObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDpointsObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


}; //End of gdjs.GameOverCode.eventsList0x89e7a0
gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDhomeObjects1Objects = Hashtable.newFrom({"home": gdjs.GameOverCode.GDhomeObjects1});gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbuyObjects1Objects = Hashtable.newFrom({"buy": gdjs.GameOverCode.GDbuyObjects1});gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDshareObjects1Objects = Hashtable.newFrom({"share": gdjs.GameOverCode.GDshareObjects1});gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDemailObjects1Objects = Hashtable.newFrom({"email": gdjs.GameOverCode.GDemailObjects1});gdjs.GameOverCode.eventsList0xb2358 = function(runtimeScene) {

{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameOverCode.condition0IsTrue_0.val) {
gdjs.GameOverCode.GDgameOverObjects1.createFrom(runtimeScene.getObjects("gameOver"));
gdjs.GameOverCode.GDpointsObjects1.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.GameOverCode.GDgameOverObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDgameOverObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene) / 2 - (gdjs.GameOverCode.GDgameOverObjects1[i].getWidth()) / 2);
}
}{for(var i = 0, len = gdjs.GameOverCode.GDpointsObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDpointsObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene) / 2 - (gdjs.GameOverCode.GDpointsObjects1[i].getWidth()) / 2);
}
}
{ //Subevents
gdjs.GameOverCode.eventsList0x89e7a0(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.GameOverCode.GDhomeObjects1.createFrom(runtimeScene.getObjects("home"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDhomeObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


{

gdjs.GameOverCode.GDbuyObjects1.createFrom(runtimeScene.getObjects("buy"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbuyObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("http://www.escolaplay.com.br", runtimeScene);
}}

}


{

gdjs.GameOverCode.GDshareObjects1.createFrom(runtimeScene.getObjects("share"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDshareObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("https://www.facebook.com/marcelomdea", runtimeScene);
}}

}


{

gdjs.GameOverCode.GDemailObjects1.createFrom(runtimeScene.getObjects("email"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDemailObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("http://www.escolaplay.com.br/contact.html", runtimeScene);
}}

}


}; //End of gdjs.GameOverCode.eventsList0xb2358


gdjs.GameOverCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.GameOverCode.GDbackgroundObjects1.length = 0;
gdjs.GameOverCode.GDbackgroundObjects2.length = 0;
gdjs.GameOverCode.GDbackgroundObjects3.length = 0;
gdjs.GameOverCode.GDnext_95pieceObjects1.length = 0;
gdjs.GameOverCode.GDnext_95pieceObjects2.length = 0;
gdjs.GameOverCode.GDnext_95pieceObjects3.length = 0;
gdjs.GameOverCode.GDhomeObjects1.length = 0;
gdjs.GameOverCode.GDhomeObjects2.length = 0;
gdjs.GameOverCode.GDhomeObjects3.length = 0;
gdjs.GameOverCode.GDbuyObjects1.length = 0;
gdjs.GameOverCode.GDbuyObjects2.length = 0;
gdjs.GameOverCode.GDbuyObjects3.length = 0;
gdjs.GameOverCode.GDemailObjects1.length = 0;
gdjs.GameOverCode.GDemailObjects2.length = 0;
gdjs.GameOverCode.GDemailObjects3.length = 0;
gdjs.GameOverCode.GDshareObjects1.length = 0;
gdjs.GameOverCode.GDshareObjects2.length = 0;
gdjs.GameOverCode.GDshareObjects3.length = 0;
gdjs.GameOverCode.GDpointsObjects1.length = 0;
gdjs.GameOverCode.GDpointsObjects2.length = 0;
gdjs.GameOverCode.GDpointsObjects3.length = 0;
gdjs.GameOverCode.GDstartObjects1.length = 0;
gdjs.GameOverCode.GDstartObjects2.length = 0;
gdjs.GameOverCode.GDstartObjects3.length = 0;
gdjs.GameOverCode.GDNewObjectObjects1.length = 0;
gdjs.GameOverCode.GDNewObjectObjects2.length = 0;
gdjs.GameOverCode.GDNewObjectObjects3.length = 0;
gdjs.GameOverCode.GDgameOverObjects1.length = 0;
gdjs.GameOverCode.GDgameOverObjects2.length = 0;
gdjs.GameOverCode.GDgameOverObjects3.length = 0;

gdjs.GameOverCode.eventsList0xb2358(runtimeScene);
return;
}
gdjs['GameOverCode'] = gdjs.GameOverCode;
