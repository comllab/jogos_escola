gdjs.EndCode = {};
gdjs.EndCode.GDletrasObjects1= [];
gdjs.EndCode.GDletrasObjects2= [];
gdjs.EndCode.GDletrasObjects3= [];
gdjs.EndCode.GDforcaObjects1= [];
gdjs.EndCode.GDforcaObjects2= [];
gdjs.EndCode.GDforcaObjects3= [];
gdjs.EndCode.GDbackgroundObjects1= [];
gdjs.EndCode.GDbackgroundObjects2= [];
gdjs.EndCode.GDbackgroundObjects3= [];
gdjs.EndCode.GDdicasObjects1= [];
gdjs.EndCode.GDdicasObjects2= [];
gdjs.EndCode.GDdicasObjects3= [];
gdjs.EndCode.GDtecladoObjects1= [];
gdjs.EndCode.GDtecladoObjects2= [];
gdjs.EndCode.GDtecladoObjects3= [];
gdjs.EndCode.GDbtAjudaObjects1= [];
gdjs.EndCode.GDbtAjudaObjects2= [];
gdjs.EndCode.GDbtAjudaObjects3= [];
gdjs.EndCode.GDbtPlayObjects1= [];
gdjs.EndCode.GDbtPlayObjects2= [];
gdjs.EndCode.GDbtPlayObjects3= [];
gdjs.EndCode.GDbtRecarregarObjects1= [];
gdjs.EndCode.GDbtRecarregarObjects2= [];
gdjs.EndCode.GDbtRecarregarObjects3= [];
gdjs.EndCode.GDbuttonDicasObjects1= [];
gdjs.EndCode.GDbuttonDicasObjects2= [];
gdjs.EndCode.GDbuttonDicasObjects3= [];
gdjs.EndCode.GDlblDicasObjects1= [];
gdjs.EndCode.GDlblDicasObjects2= [];
gdjs.EndCode.GDlblDicasObjects3= [];
gdjs.EndCode.GDlabel3Objects1= [];
gdjs.EndCode.GDlabel3Objects2= [];
gdjs.EndCode.GDlabel3Objects3= [];
gdjs.EndCode.GDlabel1Objects1= [];
gdjs.EndCode.GDlabel1Objects2= [];
gdjs.EndCode.GDlabel1Objects3= [];
gdjs.EndCode.GDlabel4Objects1= [];
gdjs.EndCode.GDlabel4Objects2= [];
gdjs.EndCode.GDlabel4Objects3= [];
gdjs.EndCode.GDlabel2Objects1= [];
gdjs.EndCode.GDlabel2Objects2= [];
gdjs.EndCode.GDlabel2Objects3= [];
gdjs.EndCode.GDlabel3Objects1= [];
gdjs.EndCode.GDlabel3Objects2= [];
gdjs.EndCode.GDlabel3Objects3= [];
gdjs.EndCode.GDlabelObjects1= [];
gdjs.EndCode.GDlabelObjects2= [];
gdjs.EndCode.GDlabelObjects3= [];
gdjs.EndCode.GDbtContinuarObjects1= [];
gdjs.EndCode.GDbtContinuarObjects2= [];
gdjs.EndCode.GDbtContinuarObjects3= [];
gdjs.EndCode.GDpalavra3Objects1= [];
gdjs.EndCode.GDpalavra3Objects2= [];
gdjs.EndCode.GDpalavra3Objects3= [];
gdjs.EndCode.GDpalavra2Objects1= [];
gdjs.EndCode.GDpalavra2Objects2= [];
gdjs.EndCode.GDpalavra2Objects3= [];
gdjs.EndCode.GDpalavra1Objects1= [];
gdjs.EndCode.GDpalavra1Objects2= [];
gdjs.EndCode.GDpalavra1Objects3= [];

gdjs.EndCode.conditionTrue_0 = {val:false};
gdjs.EndCode.condition0IsTrue_0 = {val:false};
gdjs.EndCode.condition1IsTrue_0 = {val:false};
gdjs.EndCode.condition2IsTrue_0 = {val:false};
gdjs.EndCode.condition3IsTrue_0 = {val:false};
gdjs.EndCode.conditionTrue_1 = {val:false};
gdjs.EndCode.condition0IsTrue_1 = {val:false};
gdjs.EndCode.condition1IsTrue_1 = {val:false};
gdjs.EndCode.condition2IsTrue_1 = {val:false};
gdjs.EndCode.condition3IsTrue_1 = {val:false};


gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDbtContinuarObjects1Objects = Hashtable.newFrom({"btContinuar": gdjs.EndCode.GDbtContinuarObjects1});gdjs.EndCode.eventsList0x706ac0 = function(runtimeScene) {

{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3));
}if (gdjs.EndCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) > gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3));
}if (gdjs.EndCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


{


{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.EndCode.eventsList0x706ac0
gdjs.EndCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.EndCode.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.EndCode.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{for(var i = 0, len = gdjs.EndCode.GDpalavra1Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDpalavra1Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}
}{for(var i = 0, len = gdjs.EndCode.GDpalavra2Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDpalavra2Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(5)));
}
}{for(var i = 0, len = gdjs.EndCode.GDpalavra3Objects1.length ;i < len;++i) {
    gdjs.EndCode.GDpalavra3Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(6)));
}
}}

}


{

gdjs.EndCode.GDbtContinuarObjects1.createFrom(runtimeScene.getObjects("btContinuar"));

gdjs.EndCode.condition0IsTrue_0.val = false;
gdjs.EndCode.condition1IsTrue_0.val = false;
gdjs.EndCode.condition2IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDbtContinuarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.EndCode.condition0IsTrue_0.val ) {
{
gdjs.EndCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.EndCode.condition1IsTrue_0.val ) {
{
{gdjs.EndCode.conditionTrue_1 = gdjs.EndCode.condition2IsTrue_0;
gdjs.EndCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7367820);
}
}}
}
if (gdjs.EndCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.EndCode.eventsList0x706ac0(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.EndCode.eventsList0xaff48


gdjs.EndCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.EndCode.GDletrasObjects1.length = 0;
gdjs.EndCode.GDletrasObjects2.length = 0;
gdjs.EndCode.GDletrasObjects3.length = 0;
gdjs.EndCode.GDforcaObjects1.length = 0;
gdjs.EndCode.GDforcaObjects2.length = 0;
gdjs.EndCode.GDforcaObjects3.length = 0;
gdjs.EndCode.GDbackgroundObjects1.length = 0;
gdjs.EndCode.GDbackgroundObjects2.length = 0;
gdjs.EndCode.GDbackgroundObjects3.length = 0;
gdjs.EndCode.GDdicasObjects1.length = 0;
gdjs.EndCode.GDdicasObjects2.length = 0;
gdjs.EndCode.GDdicasObjects3.length = 0;
gdjs.EndCode.GDtecladoObjects1.length = 0;
gdjs.EndCode.GDtecladoObjects2.length = 0;
gdjs.EndCode.GDtecladoObjects3.length = 0;
gdjs.EndCode.GDbtAjudaObjects1.length = 0;
gdjs.EndCode.GDbtAjudaObjects2.length = 0;
gdjs.EndCode.GDbtAjudaObjects3.length = 0;
gdjs.EndCode.GDbtPlayObjects1.length = 0;
gdjs.EndCode.GDbtPlayObjects2.length = 0;
gdjs.EndCode.GDbtPlayObjects3.length = 0;
gdjs.EndCode.GDbtRecarregarObjects1.length = 0;
gdjs.EndCode.GDbtRecarregarObjects2.length = 0;
gdjs.EndCode.GDbtRecarregarObjects3.length = 0;
gdjs.EndCode.GDbuttonDicasObjects1.length = 0;
gdjs.EndCode.GDbuttonDicasObjects2.length = 0;
gdjs.EndCode.GDbuttonDicasObjects3.length = 0;
gdjs.EndCode.GDlblDicasObjects1.length = 0;
gdjs.EndCode.GDlblDicasObjects2.length = 0;
gdjs.EndCode.GDlblDicasObjects3.length = 0;
gdjs.EndCode.GDlabel3Objects1.length = 0;
gdjs.EndCode.GDlabel3Objects2.length = 0;
gdjs.EndCode.GDlabel3Objects3.length = 0;
gdjs.EndCode.GDlabel1Objects1.length = 0;
gdjs.EndCode.GDlabel1Objects2.length = 0;
gdjs.EndCode.GDlabel1Objects3.length = 0;
gdjs.EndCode.GDlabel4Objects1.length = 0;
gdjs.EndCode.GDlabel4Objects2.length = 0;
gdjs.EndCode.GDlabel4Objects3.length = 0;
gdjs.EndCode.GDlabel2Objects1.length = 0;
gdjs.EndCode.GDlabel2Objects2.length = 0;
gdjs.EndCode.GDlabel2Objects3.length = 0;
gdjs.EndCode.GDlabel3Objects1.length = 0;
gdjs.EndCode.GDlabel3Objects2.length = 0;
gdjs.EndCode.GDlabel3Objects3.length = 0;
gdjs.EndCode.GDlabelObjects1.length = 0;
gdjs.EndCode.GDlabelObjects2.length = 0;
gdjs.EndCode.GDlabelObjects3.length = 0;
gdjs.EndCode.GDbtContinuarObjects1.length = 0;
gdjs.EndCode.GDbtContinuarObjects2.length = 0;
gdjs.EndCode.GDbtContinuarObjects3.length = 0;
gdjs.EndCode.GDpalavra3Objects1.length = 0;
gdjs.EndCode.GDpalavra3Objects2.length = 0;
gdjs.EndCode.GDpalavra3Objects3.length = 0;
gdjs.EndCode.GDpalavra2Objects1.length = 0;
gdjs.EndCode.GDpalavra2Objects2.length = 0;
gdjs.EndCode.GDpalavra2Objects3.length = 0;
gdjs.EndCode.GDpalavra1Objects1.length = 0;
gdjs.EndCode.GDpalavra1Objects2.length = 0;
gdjs.EndCode.GDpalavra1Objects3.length = 0;

gdjs.EndCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['EndCode'] = gdjs.EndCode;
