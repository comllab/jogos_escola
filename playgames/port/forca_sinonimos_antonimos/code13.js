gdjs.Palavras9Code = {};
gdjs.Palavras9Code.forEachIndex2 = 0;

gdjs.Palavras9Code.forEachIndex3 = 0;

gdjs.Palavras9Code.forEachObjects2 = [];

gdjs.Palavras9Code.forEachObjects3 = [];

gdjs.Palavras9Code.forEachTemporary2 = null;

gdjs.Palavras9Code.forEachTemporary3 = null;

gdjs.Palavras9Code.forEachTotalCount2 = 0;

gdjs.Palavras9Code.forEachTotalCount3 = 0;

gdjs.Palavras9Code.GDletrasObjects1= [];
gdjs.Palavras9Code.GDletrasObjects2= [];
gdjs.Palavras9Code.GDletrasObjects3= [];
gdjs.Palavras9Code.GDforcaObjects1= [];
gdjs.Palavras9Code.GDforcaObjects2= [];
gdjs.Palavras9Code.GDforcaObjects3= [];
gdjs.Palavras9Code.GDbackgroundObjects1= [];
gdjs.Palavras9Code.GDbackgroundObjects2= [];
gdjs.Palavras9Code.GDbackgroundObjects3= [];
gdjs.Palavras9Code.GDdicasObjects1= [];
gdjs.Palavras9Code.GDdicasObjects2= [];
gdjs.Palavras9Code.GDdicasObjects3= [];
gdjs.Palavras9Code.GDtecladoObjects1= [];
gdjs.Palavras9Code.GDtecladoObjects2= [];
gdjs.Palavras9Code.GDtecladoObjects3= [];
gdjs.Palavras9Code.GDbtAjudaObjects1= [];
gdjs.Palavras9Code.GDbtAjudaObjects2= [];
gdjs.Palavras9Code.GDbtAjudaObjects3= [];
gdjs.Palavras9Code.GDbtPlayObjects1= [];
gdjs.Palavras9Code.GDbtPlayObjects2= [];
gdjs.Palavras9Code.GDbtPlayObjects3= [];
gdjs.Palavras9Code.GDbtRecarregarObjects1= [];
gdjs.Palavras9Code.GDbtRecarregarObjects2= [];
gdjs.Palavras9Code.GDbtRecarregarObjects3= [];
gdjs.Palavras9Code.GDbuttonDicasObjects1= [];
gdjs.Palavras9Code.GDbuttonDicasObjects2= [];
gdjs.Palavras9Code.GDbuttonDicasObjects3= [];
gdjs.Palavras9Code.GDlblDicasObjects1= [];
gdjs.Palavras9Code.GDlblDicasObjects2= [];
gdjs.Palavras9Code.GDlblDicasObjects3= [];
gdjs.Palavras9Code.GDlabel3Objects1= [];
gdjs.Palavras9Code.GDlabel3Objects2= [];
gdjs.Palavras9Code.GDlabel3Objects3= [];
gdjs.Palavras9Code.GDlabel1Objects1= [];
gdjs.Palavras9Code.GDlabel1Objects2= [];
gdjs.Palavras9Code.GDlabel1Objects3= [];
gdjs.Palavras9Code.GDpalavra1Objects1= [];
gdjs.Palavras9Code.GDpalavra1Objects2= [];
gdjs.Palavras9Code.GDpalavra1Objects3= [];
gdjs.Palavras9Code.GDpalavra2Objects1= [];
gdjs.Palavras9Code.GDpalavra2Objects2= [];
gdjs.Palavras9Code.GDpalavra2Objects3= [];
gdjs.Palavras9Code.GDpalavra3Objects1= [];
gdjs.Palavras9Code.GDpalavra3Objects2= [];
gdjs.Palavras9Code.GDpalavra3Objects3= [];
gdjs.Palavras9Code.GDdica1Objects1= [];
gdjs.Palavras9Code.GDdica1Objects2= [];
gdjs.Palavras9Code.GDdica1Objects3= [];
gdjs.Palavras9Code.GDdica2Objects1= [];
gdjs.Palavras9Code.GDdica2Objects2= [];
gdjs.Palavras9Code.GDdica2Objects3= [];
gdjs.Palavras9Code.GDdica3Objects1= [];
gdjs.Palavras9Code.GDdica3Objects2= [];
gdjs.Palavras9Code.GDdica3Objects3= [];

gdjs.Palavras9Code.conditionTrue_0 = {val:false};
gdjs.Palavras9Code.condition0IsTrue_0 = {val:false};
gdjs.Palavras9Code.condition1IsTrue_0 = {val:false};
gdjs.Palavras9Code.condition2IsTrue_0 = {val:false};
gdjs.Palavras9Code.condition3IsTrue_0 = {val:false};
gdjs.Palavras9Code.condition4IsTrue_0 = {val:false};
gdjs.Palavras9Code.conditionTrue_1 = {val:false};
gdjs.Palavras9Code.condition0IsTrue_1 = {val:false};
gdjs.Palavras9Code.condition1IsTrue_1 = {val:false};
gdjs.Palavras9Code.condition2IsTrue_1 = {val:false};
gdjs.Palavras9Code.condition3IsTrue_1 = {val:false};
gdjs.Palavras9Code.condition4IsTrue_1 = {val:false};


gdjs.Palavras9Code.eventsList0x701528 = function(runtimeScene) {

{

gdjs.Palavras9Code.GDletrasObjects1.createFrom(runtimeScene.getObjects("letras"));

gdjs.Palavras9Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras9Code.GDletrasObjects1.length;i<l;++i) {
    if ( gdjs.Palavras9Code.GDletrasObjects1[i].getVariableString(gdjs.Palavras9Code.GDletrasObjects1[i].getVariables().getFromIndex(0)) == "#" ) {
        gdjs.Palavras9Code.condition0IsTrue_0.val = true;
        gdjs.Palavras9Code.GDletrasObjects1[k] = gdjs.Palavras9Code.GDletrasObjects1[i];
        ++k;
    }
}
gdjs.Palavras9Code.GDletrasObjects1.length = k;}if (gdjs.Palavras9Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Palavras9Code.GDletrasObjects1 */
{for(var i = 0, len = gdjs.Palavras9Code.GDletrasObjects1.length ;i < len;++i) {
    gdjs.Palavras9Code.GDletrasObjects1[i].hide();
}
}}

}


}; //End of gdjs.Palavras9Code.eventsList0x701528
gdjs.Palavras9Code.mapOfGDgdjs_46Palavras9Code_46GDtecladoObjects1Objects = Hashtable.newFrom({"teclado": gdjs.Palavras9Code.GDtecladoObjects1});gdjs.Palavras9Code.eventsList0x730f00 = function(runtimeScene) {

}; //End of gdjs.Palavras9Code.eventsList0x730f00
gdjs.Palavras9Code.eventsList0x730ac0 = function(runtimeScene) {

{

gdjs.Palavras9Code.GDletrasObjects2.createFrom(runtimeScene.getObjects("letras"));

for(gdjs.Palavras9Code.forEachIndex3 = 0;gdjs.Palavras9Code.forEachIndex3 < gdjs.Palavras9Code.GDletrasObjects2.length;++gdjs.Palavras9Code.forEachIndex3) {
gdjs.Palavras9Code.GDletrasObjects3.createFrom(gdjs.Palavras9Code.GDletrasObjects2);

gdjs.Palavras9Code.GDtecladoObjects3.createFrom(gdjs.Palavras9Code.GDtecladoObjects1);


gdjs.Palavras9Code.forEachTemporary3 = gdjs.Palavras9Code.GDletrasObjects2[gdjs.Palavras9Code.forEachIndex3];
gdjs.Palavras9Code.GDletrasObjects3.length = 0;
gdjs.Palavras9Code.GDletrasObjects3.push(gdjs.Palavras9Code.forEachTemporary3);
gdjs.Palavras9Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras9Code.GDletrasObjects3.length;i<l;++i) {
    if ( gdjs.Palavras9Code.GDletrasObjects3[i].getVariableString(gdjs.Palavras9Code.GDletrasObjects3[i].getVariables().getFromIndex(0)) == (( gdjs.Palavras9Code.GDtecladoObjects3.length === 0 ) ? "" :gdjs.Palavras9Code.GDtecladoObjects3[0].getAnimationName()) ) {
        gdjs.Palavras9Code.condition0IsTrue_0.val = true;
        gdjs.Palavras9Code.GDletrasObjects3[k] = gdjs.Palavras9Code.GDletrasObjects3[i];
        ++k;
    }
}
gdjs.Palavras9Code.GDletrasObjects3.length = k;}if (gdjs.Palavras9Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Palavras9Code.GDletrasObjects3.length ;i < len;++i) {
    gdjs.Palavras9Code.GDletrasObjects3[i].setAnimationName((gdjs.RuntimeObject.getVariableString(gdjs.Palavras9Code.GDletrasObjects3[i].getVariables().getFromIndex(0))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}
}

}


{


gdjs.Palavras9Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras9Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0;
}if (gdjs.Palavras9Code.condition0IsTrue_0.val) {
gdjs.Palavras9Code.GDforcaObjects2.createFrom(runtimeScene.getObjects("forca"));
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras9Code.GDforcaObjects2.length ;i < len;++i) {
    gdjs.Palavras9Code.GDforcaObjects2[i].setAnimation((gdjs.Palavras9Code.GDforcaObjects2[i].getAnimation()) + 1);
}
}}

}


{


gdjs.Palavras9Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras9Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) > 0;
}if (gdjs.Palavras9Code.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise04.ogg", false, 100, 1);
}}

}


}; //End of gdjs.Palavras9Code.eventsList0x730ac0
gdjs.Palavras9Code.mapOfGDgdjs_46Palavras9Code_46GDbuttonDicasObjects1Objects = Hashtable.newFrom({"buttonDicas": gdjs.Palavras9Code.GDbuttonDicasObjects1});gdjs.Palavras9Code.mapOfGDgdjs_46Palavras9Code_46GDdicasObjects1Objects = Hashtable.newFrom({"dicas": gdjs.Palavras9Code.GDdicasObjects1});gdjs.Palavras9Code.eventsList0x72f9b0 = function(runtimeScene) {

}; //End of gdjs.Palavras9Code.eventsList0x72f9b0
gdjs.Palavras9Code.mapOfGDgdjs_46Palavras9Code_46GDbtAjudaObjects1Objects = Hashtable.newFrom({"btAjuda": gdjs.Palavras9Code.GDbtAjudaObjects1});gdjs.Palavras9Code.mapOfGDgdjs_46Palavras9Code_46GDbtRecarregarObjects1Objects = Hashtable.newFrom({"btRecarregar": gdjs.Palavras9Code.GDbtRecarregarObjects1});gdjs.Palavras9Code.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.Palavras9Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras9Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Palavras9Code.condition0IsTrue_0.val) {
gdjs.Palavras9Code.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.Palavras9Code.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.Palavras9Code.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{gdjs.evtTools.camera.hideLayer(runtimeScene, "dicas");
}{for(var i = 0, len = gdjs.Palavras9Code.GDpalavra1Objects1.length ;i < len;++i) {
    gdjs.Palavras9Code.GDpalavra1Objects1[i].hide();
}
for(var i = 0, len = gdjs.Palavras9Code.GDpalavra2Objects1.length ;i < len;++i) {
    gdjs.Palavras9Code.GDpalavra2Objects1[i].hide();
}
for(var i = 0, len = gdjs.Palavras9Code.GDpalavra3Objects1.length ;i < len;++i) {
    gdjs.Palavras9Code.GDpalavra3Objects1[i].hide();
}
}
{ //Subevents
gdjs.Palavras9Code.eventsList0x701528(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.Palavras9Code.GDtecladoObjects1.createFrom(runtimeScene.getObjects("teclado"));

gdjs.Palavras9Code.condition0IsTrue_0.val = false;
gdjs.Palavras9Code.condition1IsTrue_0.val = false;
gdjs.Palavras9Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras9Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras9Code.mapOfGDgdjs_46Palavras9Code_46GDtecladoObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras9Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras9Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras9Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras9Code.conditionTrue_1 = gdjs.Palavras9Code.condition2IsTrue_0;
gdjs.Palavras9Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7539828);
}
}}
}
if (gdjs.Palavras9Code.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.sound.playSound(runtimeScene, "artista desconhecido - pepdown.ogg", false, 100, 1);
}
{ //Subevents
gdjs.Palavras9Code.eventsList0x730ac0(runtimeScene);} //End of subevents
}

}


{

gdjs.Palavras9Code.GDbuttonDicasObjects1.createFrom(runtimeScene.getObjects("buttonDicas"));

gdjs.Palavras9Code.condition0IsTrue_0.val = false;
gdjs.Palavras9Code.condition1IsTrue_0.val = false;
gdjs.Palavras9Code.condition2IsTrue_0.val = false;
gdjs.Palavras9Code.condition3IsTrue_0.val = false;
{
gdjs.Palavras9Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras9Code.mapOfGDgdjs_46Palavras9Code_46GDbuttonDicasObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras9Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras9Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras9Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Palavras9Code.GDbuttonDicasObjects1.length;i<l;++i) {
    if ( gdjs.Palavras9Code.GDbuttonDicasObjects1[i].isVisible() ) {
        gdjs.Palavras9Code.condition2IsTrue_0.val = true;
        gdjs.Palavras9Code.GDbuttonDicasObjects1[k] = gdjs.Palavras9Code.GDbuttonDicasObjects1[i];
        ++k;
    }
}
gdjs.Palavras9Code.GDbuttonDicasObjects1.length = k;}if ( gdjs.Palavras9Code.condition2IsTrue_0.val ) {
{
{gdjs.Palavras9Code.conditionTrue_1 = gdjs.Palavras9Code.condition3IsTrue_0;
gdjs.Palavras9Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7543276);
}
}}
}
}
if (gdjs.Palavras9Code.condition3IsTrue_0.val) {
/* Reuse gdjs.Palavras9Code.GDbuttonDicasObjects1 */
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise04.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras9Code.GDbuttonDicasObjects1.length ;i < len;++i) {
    gdjs.Palavras9Code.GDbuttonDicasObjects1[i].hide();
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "dicas");
}}

}


{

gdjs.Palavras9Code.GDdicasObjects1.createFrom(runtimeScene.getObjects("dicas"));

gdjs.Palavras9Code.condition0IsTrue_0.val = false;
gdjs.Palavras9Code.condition1IsTrue_0.val = false;
{
gdjs.Palavras9Code.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "dicas");
}if ( gdjs.Palavras9Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras9Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras9Code.mapOfGDgdjs_46Palavras9Code_46GDdicasObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.Palavras9Code.condition1IsTrue_0.val) {
gdjs.Palavras9Code.GDbuttonDicasObjects1.createFrom(runtimeScene.getObjects("buttonDicas"));
{gdjs.evtTools.sound.playSound(runtimeScene, "artista desconhecido - pepdown.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras9Code.GDbuttonDicasObjects1.length ;i < len;++i) {
    gdjs.Palavras9Code.GDbuttonDicasObjects1[i].hide(false);
}
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "dicas");
}}

}


{



}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.Palavras9Code.GDletrasObjects1.createFrom(runtimeScene.getObjects("letras"));

for(gdjs.Palavras9Code.forEachIndex2 = 0;gdjs.Palavras9Code.forEachIndex2 < gdjs.Palavras9Code.GDletrasObjects1.length;++gdjs.Palavras9Code.forEachIndex2) {
gdjs.Palavras9Code.GDletrasObjects2.createFrom(gdjs.Palavras9Code.GDletrasObjects1);


gdjs.Palavras9Code.forEachTemporary2 = gdjs.Palavras9Code.GDletrasObjects1[gdjs.Palavras9Code.forEachIndex2];
gdjs.Palavras9Code.GDletrasObjects2.length = 0;
gdjs.Palavras9Code.GDletrasObjects2.push(gdjs.Palavras9Code.forEachTemporary2);
gdjs.Palavras9Code.condition0IsTrue_0.val = false;
gdjs.Palavras9Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras9Code.GDletrasObjects2.length;i<l;++i) {
    if ( gdjs.Palavras9Code.GDletrasObjects2[i].getVariableString(gdjs.Palavras9Code.GDletrasObjects2[i].getVariables().getFromIndex(0)) != "#" ) {
        gdjs.Palavras9Code.condition0IsTrue_0.val = true;
        gdjs.Palavras9Code.GDletrasObjects2[k] = gdjs.Palavras9Code.GDletrasObjects2[i];
        ++k;
    }
}
gdjs.Palavras9Code.GDletrasObjects2.length = k;}if ( gdjs.Palavras9Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Palavras9Code.GDletrasObjects2.length;i<l;++i) {
    if ( gdjs.Palavras9Code.GDletrasObjects2[i].getAnimation() == 0 ) {
        gdjs.Palavras9Code.condition1IsTrue_0.val = true;
        gdjs.Palavras9Code.GDletrasObjects2[k] = gdjs.Palavras9Code.GDletrasObjects2[i];
        ++k;
    }
}
gdjs.Palavras9Code.GDletrasObjects2.length = k;}}
if (gdjs.Palavras9Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}}
}

}


{


gdjs.Palavras9Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras9Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.Palavras9Code.condition0IsTrue_0.val) {
gdjs.Palavras9Code.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.Palavras9Code.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.Palavras9Code.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{runtimeScene.getGame().getVariables().getFromIndex(4).setString((( gdjs.Palavras9Code.GDpalavra1Objects1.length === 0 ) ? "" :gdjs.Palavras9Code.GDpalavra1Objects1[0].getString()));
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString((( gdjs.Palavras9Code.GDpalavra2Objects1.length === 0 ) ? "" :gdjs.Palavras9Code.GDpalavra2Objects1[0].getString()));
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString((( gdjs.Palavras9Code.GDpalavra3Objects1.length === 0 ) ? "" :gdjs.Palavras9Code.GDpalavra3Objects1[0].getString()));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "End", false);
}}

}


{

gdjs.Palavras9Code.GDforcaObjects1.createFrom(runtimeScene.getObjects("forca"));

gdjs.Palavras9Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras9Code.GDforcaObjects1.length;i<l;++i) {
    if ( gdjs.Palavras9Code.GDforcaObjects1[i].getAnimation() == 6 ) {
        gdjs.Palavras9Code.condition0IsTrue_0.val = true;
        gdjs.Palavras9Code.GDforcaObjects1[k] = gdjs.Palavras9Code.GDforcaObjects1[i];
        ++k;
    }
}
gdjs.Palavras9Code.GDforcaObjects1.length = k;}if (gdjs.Palavras9Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GameOver", false);
}}

}


{



}


{

gdjs.Palavras9Code.GDbtAjudaObjects1.createFrom(runtimeScene.getObjects("btAjuda"));

gdjs.Palavras9Code.condition0IsTrue_0.val = false;
gdjs.Palavras9Code.condition1IsTrue_0.val = false;
gdjs.Palavras9Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras9Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras9Code.mapOfGDgdjs_46Palavras9Code_46GDbtAjudaObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras9Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras9Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras9Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras9Code.conditionTrue_1 = gdjs.Palavras9Code.condition2IsTrue_0;
gdjs.Palavras9Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7538036);
}
}}
}
if (gdjs.Palavras9Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "Ajuda");
}}

}


{

gdjs.Palavras9Code.GDbtRecarregarObjects1.createFrom(runtimeScene.getObjects("btRecarregar"));

gdjs.Palavras9Code.condition0IsTrue_0.val = false;
gdjs.Palavras9Code.condition1IsTrue_0.val = false;
gdjs.Palavras9Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras9Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras9Code.mapOfGDgdjs_46Palavras9Code_46GDbtRecarregarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras9Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras9Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras9Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras9Code.conditionTrue_1 = gdjs.Palavras9Code.condition2IsTrue_0;
gdjs.Palavras9Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7538908);
}
}}
}
if (gdjs.Palavras9Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.Palavras9Code.eventsList0xaff48


gdjs.Palavras9Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.Palavras9Code.GDletrasObjects1.length = 0;
gdjs.Palavras9Code.GDletrasObjects2.length = 0;
gdjs.Palavras9Code.GDletrasObjects3.length = 0;
gdjs.Palavras9Code.GDforcaObjects1.length = 0;
gdjs.Palavras9Code.GDforcaObjects2.length = 0;
gdjs.Palavras9Code.GDforcaObjects3.length = 0;
gdjs.Palavras9Code.GDbackgroundObjects1.length = 0;
gdjs.Palavras9Code.GDbackgroundObjects2.length = 0;
gdjs.Palavras9Code.GDbackgroundObjects3.length = 0;
gdjs.Palavras9Code.GDdicasObjects1.length = 0;
gdjs.Palavras9Code.GDdicasObjects2.length = 0;
gdjs.Palavras9Code.GDdicasObjects3.length = 0;
gdjs.Palavras9Code.GDtecladoObjects1.length = 0;
gdjs.Palavras9Code.GDtecladoObjects2.length = 0;
gdjs.Palavras9Code.GDtecladoObjects3.length = 0;
gdjs.Palavras9Code.GDbtAjudaObjects1.length = 0;
gdjs.Palavras9Code.GDbtAjudaObjects2.length = 0;
gdjs.Palavras9Code.GDbtAjudaObjects3.length = 0;
gdjs.Palavras9Code.GDbtPlayObjects1.length = 0;
gdjs.Palavras9Code.GDbtPlayObjects2.length = 0;
gdjs.Palavras9Code.GDbtPlayObjects3.length = 0;
gdjs.Palavras9Code.GDbtRecarregarObjects1.length = 0;
gdjs.Palavras9Code.GDbtRecarregarObjects2.length = 0;
gdjs.Palavras9Code.GDbtRecarregarObjects3.length = 0;
gdjs.Palavras9Code.GDbuttonDicasObjects1.length = 0;
gdjs.Palavras9Code.GDbuttonDicasObjects2.length = 0;
gdjs.Palavras9Code.GDbuttonDicasObjects3.length = 0;
gdjs.Palavras9Code.GDlblDicasObjects1.length = 0;
gdjs.Palavras9Code.GDlblDicasObjects2.length = 0;
gdjs.Palavras9Code.GDlblDicasObjects3.length = 0;
gdjs.Palavras9Code.GDlabel3Objects1.length = 0;
gdjs.Palavras9Code.GDlabel3Objects2.length = 0;
gdjs.Palavras9Code.GDlabel3Objects3.length = 0;
gdjs.Palavras9Code.GDlabel1Objects1.length = 0;
gdjs.Palavras9Code.GDlabel1Objects2.length = 0;
gdjs.Palavras9Code.GDlabel1Objects3.length = 0;
gdjs.Palavras9Code.GDpalavra1Objects1.length = 0;
gdjs.Palavras9Code.GDpalavra1Objects2.length = 0;
gdjs.Palavras9Code.GDpalavra1Objects3.length = 0;
gdjs.Palavras9Code.GDpalavra2Objects1.length = 0;
gdjs.Palavras9Code.GDpalavra2Objects2.length = 0;
gdjs.Palavras9Code.GDpalavra2Objects3.length = 0;
gdjs.Palavras9Code.GDpalavra3Objects1.length = 0;
gdjs.Palavras9Code.GDpalavra3Objects2.length = 0;
gdjs.Palavras9Code.GDpalavra3Objects3.length = 0;
gdjs.Palavras9Code.GDdica1Objects1.length = 0;
gdjs.Palavras9Code.GDdica1Objects2.length = 0;
gdjs.Palavras9Code.GDdica1Objects3.length = 0;
gdjs.Palavras9Code.GDdica2Objects1.length = 0;
gdjs.Palavras9Code.GDdica2Objects2.length = 0;
gdjs.Palavras9Code.GDdica2Objects3.length = 0;
gdjs.Palavras9Code.GDdica3Objects1.length = 0;
gdjs.Palavras9Code.GDdica3Objects2.length = 0;
gdjs.Palavras9Code.GDdica3Objects3.length = 0;

gdjs.Palavras9Code.eventsList0xaff48(runtimeScene);
return;
}
gdjs['Palavras9Code'] = gdjs.Palavras9Code;
