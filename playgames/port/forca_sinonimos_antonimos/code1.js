gdjs.GameCode = {};
gdjs.GameCode.GDletrasObjects1= [];
gdjs.GameCode.GDletrasObjects2= [];
gdjs.GameCode.GDforcaObjects1= [];
gdjs.GameCode.GDforcaObjects2= [];
gdjs.GameCode.GDbackgroundObjects1= [];
gdjs.GameCode.GDbackgroundObjects2= [];
gdjs.GameCode.GDdicasObjects1= [];
gdjs.GameCode.GDdicasObjects2= [];
gdjs.GameCode.GDtecladoObjects1= [];
gdjs.GameCode.GDtecladoObjects2= [];
gdjs.GameCode.GDbtAjudaObjects1= [];
gdjs.GameCode.GDbtAjudaObjects2= [];
gdjs.GameCode.GDbtPlayObjects1= [];
gdjs.GameCode.GDbtPlayObjects2= [];
gdjs.GameCode.GDbtRecarregarObjects1= [];
gdjs.GameCode.GDbtRecarregarObjects2= [];
gdjs.GameCode.GDbuttonDicasObjects1= [];
gdjs.GameCode.GDbuttonDicasObjects2= [];
gdjs.GameCode.GDlblDicasObjects1= [];
gdjs.GameCode.GDlblDicasObjects2= [];
gdjs.GameCode.GDlabel3Objects1= [];
gdjs.GameCode.GDlabel3Objects2= [];
gdjs.GameCode.GDlabel1Objects1= [];
gdjs.GameCode.GDlabel1Objects2= [];
gdjs.GameCode.GDpalavra1Objects1= [];
gdjs.GameCode.GDpalavra1Objects2= [];
gdjs.GameCode.GDpalavra2Objects1= [];
gdjs.GameCode.GDpalavra2Objects2= [];
gdjs.GameCode.GDpalavra3Objects1= [];
gdjs.GameCode.GDpalavra3Objects2= [];
gdjs.GameCode.GDdica1Objects1= [];
gdjs.GameCode.GDdica1Objects2= [];
gdjs.GameCode.GDdica2Objects1= [];
gdjs.GameCode.GDdica2Objects2= [];
gdjs.GameCode.GDdica3Objects1= [];
gdjs.GameCode.GDdica3Objects2= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};


gdjs.GameCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras1", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 2;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras2", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 3;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras3", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 4;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras4", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 5;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras5", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 6;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras6", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 7;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras7", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 8;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras8", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 9;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras9", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras10", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 11;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras11", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 12;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras12", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 13;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras13", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 14;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Palavras14", false);
}}

}


}; //End of gdjs.GameCode.eventsList0xaff48


gdjs.GameCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.GameCode.GDletrasObjects1.length = 0;
gdjs.GameCode.GDletrasObjects2.length = 0;
gdjs.GameCode.GDforcaObjects1.length = 0;
gdjs.GameCode.GDforcaObjects2.length = 0;
gdjs.GameCode.GDbackgroundObjects1.length = 0;
gdjs.GameCode.GDbackgroundObjects2.length = 0;
gdjs.GameCode.GDdicasObjects1.length = 0;
gdjs.GameCode.GDdicasObjects2.length = 0;
gdjs.GameCode.GDtecladoObjects1.length = 0;
gdjs.GameCode.GDtecladoObjects2.length = 0;
gdjs.GameCode.GDbtAjudaObjects1.length = 0;
gdjs.GameCode.GDbtAjudaObjects2.length = 0;
gdjs.GameCode.GDbtPlayObjects1.length = 0;
gdjs.GameCode.GDbtPlayObjects2.length = 0;
gdjs.GameCode.GDbtRecarregarObjects1.length = 0;
gdjs.GameCode.GDbtRecarregarObjects2.length = 0;
gdjs.GameCode.GDbuttonDicasObjects1.length = 0;
gdjs.GameCode.GDbuttonDicasObjects2.length = 0;
gdjs.GameCode.GDlblDicasObjects1.length = 0;
gdjs.GameCode.GDlblDicasObjects2.length = 0;
gdjs.GameCode.GDlabel3Objects1.length = 0;
gdjs.GameCode.GDlabel3Objects2.length = 0;
gdjs.GameCode.GDlabel1Objects1.length = 0;
gdjs.GameCode.GDlabel1Objects2.length = 0;
gdjs.GameCode.GDpalavra1Objects1.length = 0;
gdjs.GameCode.GDpalavra1Objects2.length = 0;
gdjs.GameCode.GDpalavra2Objects1.length = 0;
gdjs.GameCode.GDpalavra2Objects2.length = 0;
gdjs.GameCode.GDpalavra3Objects1.length = 0;
gdjs.GameCode.GDpalavra3Objects2.length = 0;
gdjs.GameCode.GDdica1Objects1.length = 0;
gdjs.GameCode.GDdica1Objects2.length = 0;
gdjs.GameCode.GDdica2Objects1.length = 0;
gdjs.GameCode.GDdica2Objects2.length = 0;
gdjs.GameCode.GDdica3Objects1.length = 0;
gdjs.GameCode.GDdica3Objects2.length = 0;

gdjs.GameCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['GameCode'] = gdjs.GameCode;
