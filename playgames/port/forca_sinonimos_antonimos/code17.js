gdjs.Palavras13Code = {};
gdjs.Palavras13Code.forEachIndex2 = 0;

gdjs.Palavras13Code.forEachIndex3 = 0;

gdjs.Palavras13Code.forEachObjects2 = [];

gdjs.Palavras13Code.forEachObjects3 = [];

gdjs.Palavras13Code.forEachTemporary2 = null;

gdjs.Palavras13Code.forEachTemporary3 = null;

gdjs.Palavras13Code.forEachTotalCount2 = 0;

gdjs.Palavras13Code.forEachTotalCount3 = 0;

gdjs.Palavras13Code.GDletrasObjects1= [];
gdjs.Palavras13Code.GDletrasObjects2= [];
gdjs.Palavras13Code.GDletrasObjects3= [];
gdjs.Palavras13Code.GDforcaObjects1= [];
gdjs.Palavras13Code.GDforcaObjects2= [];
gdjs.Palavras13Code.GDforcaObjects3= [];
gdjs.Palavras13Code.GDbackgroundObjects1= [];
gdjs.Palavras13Code.GDbackgroundObjects2= [];
gdjs.Palavras13Code.GDbackgroundObjects3= [];
gdjs.Palavras13Code.GDdicasObjects1= [];
gdjs.Palavras13Code.GDdicasObjects2= [];
gdjs.Palavras13Code.GDdicasObjects3= [];
gdjs.Palavras13Code.GDtecladoObjects1= [];
gdjs.Palavras13Code.GDtecladoObjects2= [];
gdjs.Palavras13Code.GDtecladoObjects3= [];
gdjs.Palavras13Code.GDbtAjudaObjects1= [];
gdjs.Palavras13Code.GDbtAjudaObjects2= [];
gdjs.Palavras13Code.GDbtAjudaObjects3= [];
gdjs.Palavras13Code.GDbtPlayObjects1= [];
gdjs.Palavras13Code.GDbtPlayObjects2= [];
gdjs.Palavras13Code.GDbtPlayObjects3= [];
gdjs.Palavras13Code.GDbtRecarregarObjects1= [];
gdjs.Palavras13Code.GDbtRecarregarObjects2= [];
gdjs.Palavras13Code.GDbtRecarregarObjects3= [];
gdjs.Palavras13Code.GDbuttonDicasObjects1= [];
gdjs.Palavras13Code.GDbuttonDicasObjects2= [];
gdjs.Palavras13Code.GDbuttonDicasObjects3= [];
gdjs.Palavras13Code.GDlblDicasObjects1= [];
gdjs.Palavras13Code.GDlblDicasObjects2= [];
gdjs.Palavras13Code.GDlblDicasObjects3= [];
gdjs.Palavras13Code.GDlabel3Objects1= [];
gdjs.Palavras13Code.GDlabel3Objects2= [];
gdjs.Palavras13Code.GDlabel3Objects3= [];
gdjs.Palavras13Code.GDlabel1Objects1= [];
gdjs.Palavras13Code.GDlabel1Objects2= [];
gdjs.Palavras13Code.GDlabel1Objects3= [];
gdjs.Palavras13Code.GDpalavra1Objects1= [];
gdjs.Palavras13Code.GDpalavra1Objects2= [];
gdjs.Palavras13Code.GDpalavra1Objects3= [];
gdjs.Palavras13Code.GDpalavra2Objects1= [];
gdjs.Palavras13Code.GDpalavra2Objects2= [];
gdjs.Palavras13Code.GDpalavra2Objects3= [];
gdjs.Palavras13Code.GDpalavra3Objects1= [];
gdjs.Palavras13Code.GDpalavra3Objects2= [];
gdjs.Palavras13Code.GDpalavra3Objects3= [];
gdjs.Palavras13Code.GDdica1Objects1= [];
gdjs.Palavras13Code.GDdica1Objects2= [];
gdjs.Palavras13Code.GDdica1Objects3= [];
gdjs.Palavras13Code.GDdica2Objects1= [];
gdjs.Palavras13Code.GDdica2Objects2= [];
gdjs.Palavras13Code.GDdica2Objects3= [];
gdjs.Palavras13Code.GDdica3Objects1= [];
gdjs.Palavras13Code.GDdica3Objects2= [];
gdjs.Palavras13Code.GDdica3Objects3= [];

gdjs.Palavras13Code.conditionTrue_0 = {val:false};
gdjs.Palavras13Code.condition0IsTrue_0 = {val:false};
gdjs.Palavras13Code.condition1IsTrue_0 = {val:false};
gdjs.Palavras13Code.condition2IsTrue_0 = {val:false};
gdjs.Palavras13Code.condition3IsTrue_0 = {val:false};
gdjs.Palavras13Code.condition4IsTrue_0 = {val:false};
gdjs.Palavras13Code.conditionTrue_1 = {val:false};
gdjs.Palavras13Code.condition0IsTrue_1 = {val:false};
gdjs.Palavras13Code.condition1IsTrue_1 = {val:false};
gdjs.Palavras13Code.condition2IsTrue_1 = {val:false};
gdjs.Palavras13Code.condition3IsTrue_1 = {val:false};
gdjs.Palavras13Code.condition4IsTrue_1 = {val:false};


gdjs.Palavras13Code.eventsList0x701528 = function(runtimeScene) {

{

gdjs.Palavras13Code.GDletrasObjects1.createFrom(runtimeScene.getObjects("letras"));

gdjs.Palavras13Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras13Code.GDletrasObjects1.length;i<l;++i) {
    if ( gdjs.Palavras13Code.GDletrasObjects1[i].getVariableString(gdjs.Palavras13Code.GDletrasObjects1[i].getVariables().getFromIndex(0)) == "#" ) {
        gdjs.Palavras13Code.condition0IsTrue_0.val = true;
        gdjs.Palavras13Code.GDletrasObjects1[k] = gdjs.Palavras13Code.GDletrasObjects1[i];
        ++k;
    }
}
gdjs.Palavras13Code.GDletrasObjects1.length = k;}if (gdjs.Palavras13Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Palavras13Code.GDletrasObjects1 */
{for(var i = 0, len = gdjs.Palavras13Code.GDletrasObjects1.length ;i < len;++i) {
    gdjs.Palavras13Code.GDletrasObjects1[i].hide();
}
}}

}


}; //End of gdjs.Palavras13Code.eventsList0x701528
gdjs.Palavras13Code.mapOfGDgdjs_46Palavras13Code_46GDtecladoObjects1Objects = Hashtable.newFrom({"teclado": gdjs.Palavras13Code.GDtecladoObjects1});gdjs.Palavras13Code.eventsList0x748150 = function(runtimeScene) {

}; //End of gdjs.Palavras13Code.eventsList0x748150
gdjs.Palavras13Code.eventsList0x747d10 = function(runtimeScene) {

{

gdjs.Palavras13Code.GDletrasObjects2.createFrom(runtimeScene.getObjects("letras"));

for(gdjs.Palavras13Code.forEachIndex3 = 0;gdjs.Palavras13Code.forEachIndex3 < gdjs.Palavras13Code.GDletrasObjects2.length;++gdjs.Palavras13Code.forEachIndex3) {
gdjs.Palavras13Code.GDletrasObjects3.createFrom(gdjs.Palavras13Code.GDletrasObjects2);

gdjs.Palavras13Code.GDtecladoObjects3.createFrom(gdjs.Palavras13Code.GDtecladoObjects1);


gdjs.Palavras13Code.forEachTemporary3 = gdjs.Palavras13Code.GDletrasObjects2[gdjs.Palavras13Code.forEachIndex3];
gdjs.Palavras13Code.GDletrasObjects3.length = 0;
gdjs.Palavras13Code.GDletrasObjects3.push(gdjs.Palavras13Code.forEachTemporary3);
gdjs.Palavras13Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras13Code.GDletrasObjects3.length;i<l;++i) {
    if ( gdjs.Palavras13Code.GDletrasObjects3[i].getVariableString(gdjs.Palavras13Code.GDletrasObjects3[i].getVariables().getFromIndex(0)) == (( gdjs.Palavras13Code.GDtecladoObjects3.length === 0 ) ? "" :gdjs.Palavras13Code.GDtecladoObjects3[0].getAnimationName()) ) {
        gdjs.Palavras13Code.condition0IsTrue_0.val = true;
        gdjs.Palavras13Code.GDletrasObjects3[k] = gdjs.Palavras13Code.GDletrasObjects3[i];
        ++k;
    }
}
gdjs.Palavras13Code.GDletrasObjects3.length = k;}if (gdjs.Palavras13Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Palavras13Code.GDletrasObjects3.length ;i < len;++i) {
    gdjs.Palavras13Code.GDletrasObjects3[i].setAnimationName((gdjs.RuntimeObject.getVariableString(gdjs.Palavras13Code.GDletrasObjects3[i].getVariables().getFromIndex(0))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}
}

}


{


gdjs.Palavras13Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras13Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0;
}if (gdjs.Palavras13Code.condition0IsTrue_0.val) {
gdjs.Palavras13Code.GDforcaObjects2.createFrom(runtimeScene.getObjects("forca"));
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras13Code.GDforcaObjects2.length ;i < len;++i) {
    gdjs.Palavras13Code.GDforcaObjects2[i].setAnimation((gdjs.Palavras13Code.GDforcaObjects2[i].getAnimation()) + 1);
}
}}

}


{


gdjs.Palavras13Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras13Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) > 0;
}if (gdjs.Palavras13Code.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise04.ogg", false, 100, 1);
}}

}


}; //End of gdjs.Palavras13Code.eventsList0x747d10
gdjs.Palavras13Code.mapOfGDgdjs_46Palavras13Code_46GDbuttonDicasObjects1Objects = Hashtable.newFrom({"buttonDicas": gdjs.Palavras13Code.GDbuttonDicasObjects1});gdjs.Palavras13Code.mapOfGDgdjs_46Palavras13Code_46GDdicasObjects1Objects = Hashtable.newFrom({"dicas": gdjs.Palavras13Code.GDdicasObjects1});gdjs.Palavras13Code.eventsList0x746c00 = function(runtimeScene) {

}; //End of gdjs.Palavras13Code.eventsList0x746c00
gdjs.Palavras13Code.mapOfGDgdjs_46Palavras13Code_46GDbtAjudaObjects1Objects = Hashtable.newFrom({"btAjuda": gdjs.Palavras13Code.GDbtAjudaObjects1});gdjs.Palavras13Code.mapOfGDgdjs_46Palavras13Code_46GDbtRecarregarObjects1Objects = Hashtable.newFrom({"btRecarregar": gdjs.Palavras13Code.GDbtRecarregarObjects1});gdjs.Palavras13Code.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.Palavras13Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras13Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Palavras13Code.condition0IsTrue_0.val) {
gdjs.Palavras13Code.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.Palavras13Code.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.Palavras13Code.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{gdjs.evtTools.camera.hideLayer(runtimeScene, "dicas");
}{for(var i = 0, len = gdjs.Palavras13Code.GDpalavra1Objects1.length ;i < len;++i) {
    gdjs.Palavras13Code.GDpalavra1Objects1[i].hide();
}
for(var i = 0, len = gdjs.Palavras13Code.GDpalavra2Objects1.length ;i < len;++i) {
    gdjs.Palavras13Code.GDpalavra2Objects1[i].hide();
}
for(var i = 0, len = gdjs.Palavras13Code.GDpalavra3Objects1.length ;i < len;++i) {
    gdjs.Palavras13Code.GDpalavra3Objects1[i].hide();
}
}
{ //Subevents
gdjs.Palavras13Code.eventsList0x701528(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.Palavras13Code.GDtecladoObjects1.createFrom(runtimeScene.getObjects("teclado"));

gdjs.Palavras13Code.condition0IsTrue_0.val = false;
gdjs.Palavras13Code.condition1IsTrue_0.val = false;
gdjs.Palavras13Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras13Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras13Code.mapOfGDgdjs_46Palavras13Code_46GDtecladoObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras13Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras13Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras13Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras13Code.conditionTrue_1 = gdjs.Palavras13Code.condition2IsTrue_0;
gdjs.Palavras13Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7634628);
}
}}
}
if (gdjs.Palavras13Code.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.sound.playSound(runtimeScene, "artista desconhecido - pepdown.ogg", false, 100, 1);
}
{ //Subevents
gdjs.Palavras13Code.eventsList0x747d10(runtimeScene);} //End of subevents
}

}


{

gdjs.Palavras13Code.GDbuttonDicasObjects1.createFrom(runtimeScene.getObjects("buttonDicas"));

gdjs.Palavras13Code.condition0IsTrue_0.val = false;
gdjs.Palavras13Code.condition1IsTrue_0.val = false;
gdjs.Palavras13Code.condition2IsTrue_0.val = false;
gdjs.Palavras13Code.condition3IsTrue_0.val = false;
{
gdjs.Palavras13Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras13Code.mapOfGDgdjs_46Palavras13Code_46GDbuttonDicasObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras13Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras13Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras13Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Palavras13Code.GDbuttonDicasObjects1.length;i<l;++i) {
    if ( gdjs.Palavras13Code.GDbuttonDicasObjects1[i].isVisible() ) {
        gdjs.Palavras13Code.condition2IsTrue_0.val = true;
        gdjs.Palavras13Code.GDbuttonDicasObjects1[k] = gdjs.Palavras13Code.GDbuttonDicasObjects1[i];
        ++k;
    }
}
gdjs.Palavras13Code.GDbuttonDicasObjects1.length = k;}if ( gdjs.Palavras13Code.condition2IsTrue_0.val ) {
{
{gdjs.Palavras13Code.conditionTrue_1 = gdjs.Palavras13Code.condition3IsTrue_0;
gdjs.Palavras13Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7638076);
}
}}
}
}
if (gdjs.Palavras13Code.condition3IsTrue_0.val) {
/* Reuse gdjs.Palavras13Code.GDbuttonDicasObjects1 */
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise04.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras13Code.GDbuttonDicasObjects1.length ;i < len;++i) {
    gdjs.Palavras13Code.GDbuttonDicasObjects1[i].hide();
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "dicas");
}}

}


{

gdjs.Palavras13Code.GDdicasObjects1.createFrom(runtimeScene.getObjects("dicas"));

gdjs.Palavras13Code.condition0IsTrue_0.val = false;
gdjs.Palavras13Code.condition1IsTrue_0.val = false;
{
gdjs.Palavras13Code.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "dicas");
}if ( gdjs.Palavras13Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras13Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras13Code.mapOfGDgdjs_46Palavras13Code_46GDdicasObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.Palavras13Code.condition1IsTrue_0.val) {
gdjs.Palavras13Code.GDbuttonDicasObjects1.createFrom(runtimeScene.getObjects("buttonDicas"));
{gdjs.evtTools.sound.playSound(runtimeScene, "artista desconhecido - pepdown.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras13Code.GDbuttonDicasObjects1.length ;i < len;++i) {
    gdjs.Palavras13Code.GDbuttonDicasObjects1[i].hide(false);
}
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "dicas");
}}

}


{



}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.Palavras13Code.GDletrasObjects1.createFrom(runtimeScene.getObjects("letras"));

for(gdjs.Palavras13Code.forEachIndex2 = 0;gdjs.Palavras13Code.forEachIndex2 < gdjs.Palavras13Code.GDletrasObjects1.length;++gdjs.Palavras13Code.forEachIndex2) {
gdjs.Palavras13Code.GDletrasObjects2.createFrom(gdjs.Palavras13Code.GDletrasObjects1);


gdjs.Palavras13Code.forEachTemporary2 = gdjs.Palavras13Code.GDletrasObjects1[gdjs.Palavras13Code.forEachIndex2];
gdjs.Palavras13Code.GDletrasObjects2.length = 0;
gdjs.Palavras13Code.GDletrasObjects2.push(gdjs.Palavras13Code.forEachTemporary2);
gdjs.Palavras13Code.condition0IsTrue_0.val = false;
gdjs.Palavras13Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras13Code.GDletrasObjects2.length;i<l;++i) {
    if ( gdjs.Palavras13Code.GDletrasObjects2[i].getVariableString(gdjs.Palavras13Code.GDletrasObjects2[i].getVariables().getFromIndex(0)) != "#" ) {
        gdjs.Palavras13Code.condition0IsTrue_0.val = true;
        gdjs.Palavras13Code.GDletrasObjects2[k] = gdjs.Palavras13Code.GDletrasObjects2[i];
        ++k;
    }
}
gdjs.Palavras13Code.GDletrasObjects2.length = k;}if ( gdjs.Palavras13Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Palavras13Code.GDletrasObjects2.length;i<l;++i) {
    if ( gdjs.Palavras13Code.GDletrasObjects2[i].getAnimation() == 0 ) {
        gdjs.Palavras13Code.condition1IsTrue_0.val = true;
        gdjs.Palavras13Code.GDletrasObjects2[k] = gdjs.Palavras13Code.GDletrasObjects2[i];
        ++k;
    }
}
gdjs.Palavras13Code.GDletrasObjects2.length = k;}}
if (gdjs.Palavras13Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}}
}

}


{


gdjs.Palavras13Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras13Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.Palavras13Code.condition0IsTrue_0.val) {
gdjs.Palavras13Code.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.Palavras13Code.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.Palavras13Code.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{runtimeScene.getGame().getVariables().getFromIndex(4).setString((( gdjs.Palavras13Code.GDpalavra1Objects1.length === 0 ) ? "" :gdjs.Palavras13Code.GDpalavra1Objects1[0].getString()));
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString((( gdjs.Palavras13Code.GDpalavra2Objects1.length === 0 ) ? "" :gdjs.Palavras13Code.GDpalavra2Objects1[0].getString()));
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString((( gdjs.Palavras13Code.GDpalavra3Objects1.length === 0 ) ? "" :gdjs.Palavras13Code.GDpalavra3Objects1[0].getString()));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "End", false);
}}

}


{

gdjs.Palavras13Code.GDforcaObjects1.createFrom(runtimeScene.getObjects("forca"));

gdjs.Palavras13Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras13Code.GDforcaObjects1.length;i<l;++i) {
    if ( gdjs.Palavras13Code.GDforcaObjects1[i].getAnimation() == 6 ) {
        gdjs.Palavras13Code.condition0IsTrue_0.val = true;
        gdjs.Palavras13Code.GDforcaObjects1[k] = gdjs.Palavras13Code.GDforcaObjects1[i];
        ++k;
    }
}
gdjs.Palavras13Code.GDforcaObjects1.length = k;}if (gdjs.Palavras13Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GameOver", false);
}}

}


{



}


{

gdjs.Palavras13Code.GDbtAjudaObjects1.createFrom(runtimeScene.getObjects("btAjuda"));

gdjs.Palavras13Code.condition0IsTrue_0.val = false;
gdjs.Palavras13Code.condition1IsTrue_0.val = false;
gdjs.Palavras13Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras13Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras13Code.mapOfGDgdjs_46Palavras13Code_46GDbtAjudaObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras13Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras13Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras13Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras13Code.conditionTrue_1 = gdjs.Palavras13Code.condition2IsTrue_0;
gdjs.Palavras13Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7632836);
}
}}
}
if (gdjs.Palavras13Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "Ajuda");
}}

}


{

gdjs.Palavras13Code.GDbtRecarregarObjects1.createFrom(runtimeScene.getObjects("btRecarregar"));

gdjs.Palavras13Code.condition0IsTrue_0.val = false;
gdjs.Palavras13Code.condition1IsTrue_0.val = false;
gdjs.Palavras13Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras13Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras13Code.mapOfGDgdjs_46Palavras13Code_46GDbtRecarregarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras13Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras13Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras13Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras13Code.conditionTrue_1 = gdjs.Palavras13Code.condition2IsTrue_0;
gdjs.Palavras13Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7633708);
}
}}
}
if (gdjs.Palavras13Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.Palavras13Code.eventsList0xaff48


gdjs.Palavras13Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.Palavras13Code.GDletrasObjects1.length = 0;
gdjs.Palavras13Code.GDletrasObjects2.length = 0;
gdjs.Palavras13Code.GDletrasObjects3.length = 0;
gdjs.Palavras13Code.GDforcaObjects1.length = 0;
gdjs.Palavras13Code.GDforcaObjects2.length = 0;
gdjs.Palavras13Code.GDforcaObjects3.length = 0;
gdjs.Palavras13Code.GDbackgroundObjects1.length = 0;
gdjs.Palavras13Code.GDbackgroundObjects2.length = 0;
gdjs.Palavras13Code.GDbackgroundObjects3.length = 0;
gdjs.Palavras13Code.GDdicasObjects1.length = 0;
gdjs.Palavras13Code.GDdicasObjects2.length = 0;
gdjs.Palavras13Code.GDdicasObjects3.length = 0;
gdjs.Palavras13Code.GDtecladoObjects1.length = 0;
gdjs.Palavras13Code.GDtecladoObjects2.length = 0;
gdjs.Palavras13Code.GDtecladoObjects3.length = 0;
gdjs.Palavras13Code.GDbtAjudaObjects1.length = 0;
gdjs.Palavras13Code.GDbtAjudaObjects2.length = 0;
gdjs.Palavras13Code.GDbtAjudaObjects3.length = 0;
gdjs.Palavras13Code.GDbtPlayObjects1.length = 0;
gdjs.Palavras13Code.GDbtPlayObjects2.length = 0;
gdjs.Palavras13Code.GDbtPlayObjects3.length = 0;
gdjs.Palavras13Code.GDbtRecarregarObjects1.length = 0;
gdjs.Palavras13Code.GDbtRecarregarObjects2.length = 0;
gdjs.Palavras13Code.GDbtRecarregarObjects3.length = 0;
gdjs.Palavras13Code.GDbuttonDicasObjects1.length = 0;
gdjs.Palavras13Code.GDbuttonDicasObjects2.length = 0;
gdjs.Palavras13Code.GDbuttonDicasObjects3.length = 0;
gdjs.Palavras13Code.GDlblDicasObjects1.length = 0;
gdjs.Palavras13Code.GDlblDicasObjects2.length = 0;
gdjs.Palavras13Code.GDlblDicasObjects3.length = 0;
gdjs.Palavras13Code.GDlabel3Objects1.length = 0;
gdjs.Palavras13Code.GDlabel3Objects2.length = 0;
gdjs.Palavras13Code.GDlabel3Objects3.length = 0;
gdjs.Palavras13Code.GDlabel1Objects1.length = 0;
gdjs.Palavras13Code.GDlabel1Objects2.length = 0;
gdjs.Palavras13Code.GDlabel1Objects3.length = 0;
gdjs.Palavras13Code.GDpalavra1Objects1.length = 0;
gdjs.Palavras13Code.GDpalavra1Objects2.length = 0;
gdjs.Palavras13Code.GDpalavra1Objects3.length = 0;
gdjs.Palavras13Code.GDpalavra2Objects1.length = 0;
gdjs.Palavras13Code.GDpalavra2Objects2.length = 0;
gdjs.Palavras13Code.GDpalavra2Objects3.length = 0;
gdjs.Palavras13Code.GDpalavra3Objects1.length = 0;
gdjs.Palavras13Code.GDpalavra3Objects2.length = 0;
gdjs.Palavras13Code.GDpalavra3Objects3.length = 0;
gdjs.Palavras13Code.GDdica1Objects1.length = 0;
gdjs.Palavras13Code.GDdica1Objects2.length = 0;
gdjs.Palavras13Code.GDdica1Objects3.length = 0;
gdjs.Palavras13Code.GDdica2Objects1.length = 0;
gdjs.Palavras13Code.GDdica2Objects2.length = 0;
gdjs.Palavras13Code.GDdica2Objects3.length = 0;
gdjs.Palavras13Code.GDdica3Objects1.length = 0;
gdjs.Palavras13Code.GDdica3Objects2.length = 0;
gdjs.Palavras13Code.GDdica3Objects3.length = 0;

gdjs.Palavras13Code.eventsList0xaff48(runtimeScene);
return;
}
gdjs['Palavras13Code'] = gdjs.Palavras13Code;
