gdjs.StartCode = {};
gdjs.StartCode.GDobjetosObjects1= [];
gdjs.StartCode.GDobjetosObjects2= [];
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDMiObjects1= [];
gdjs.StartCode.GDMiObjects2= [];
gdjs.StartCode.GDNewObjectObjects1= [];
gdjs.StartCode.GDNewObjectObjects2= [];
gdjs.StartCode.GDNewObject2Objects1= [];
gdjs.StartCode.GDNewObject2Objects2= [];
gdjs.StartCode.GDbuttomObjects1= [];
gdjs.StartCode.GDbuttomObjects2= [];
gdjs.StartCode.GDjogarObjects1= [];
gdjs.StartCode.GDjogarObjects2= [];
gdjs.StartCode.GDinterrogacaoObjects1= [];
gdjs.StartCode.GDinterrogacaoObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};
gdjs.StartCode.condition3IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbuttomObjects1Objects = Hashtable.newFrom({"buttom": gdjs.StartCode.GDbuttomObjects1});gdjs.StartCode.eventsList0xb1208 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDinterrogacaoObjects1.createFrom(runtimeScene.getObjects("interrogacao"));
{for(var i = 0, len = gdjs.StartCode.GDinterrogacaoObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDinterrogacaoObjects1[i].setAnimationSpeedScale(gdjs.randomFloatInRange(1,  2));
}
}{gdjs.evtTools.sound.playMusic(runtimeScene, "Iceland Theme.ogg", true, 100, 1);
}}

}


{

gdjs.StartCode.GDbuttomObjects1.createFrom(runtimeScene.getObjects("buttom"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbuttomObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7110692);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xb1208


gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.StartCode.GDobjetosObjects1.length = 0;
gdjs.StartCode.GDobjetosObjects2.length = 0;
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDMiObjects1.length = 0;
gdjs.StartCode.GDMiObjects2.length = 0;
gdjs.StartCode.GDNewObjectObjects1.length = 0;
gdjs.StartCode.GDNewObjectObjects2.length = 0;
gdjs.StartCode.GDNewObject2Objects1.length = 0;
gdjs.StartCode.GDNewObject2Objects2.length = 0;
gdjs.StartCode.GDbuttomObjects1.length = 0;
gdjs.StartCode.GDbuttomObjects2.length = 0;
gdjs.StartCode.GDjogarObjects1.length = 0;
gdjs.StartCode.GDjogarObjects2.length = 0;
gdjs.StartCode.GDinterrogacaoObjects1.length = 0;
gdjs.StartCode.GDinterrogacaoObjects2.length = 0;

gdjs.StartCode.eventsList0xb1208(runtimeScene);
return;
}
gdjs['StartCode'] = gdjs.StartCode;
