gdjs.GameCode = {};
gdjs.GameCode.repeatCount3 = 0;

gdjs.GameCode.repeatIndex3 = 0;

gdjs.GameCode.GDobjetosObjects1= [];
gdjs.GameCode.GDobjetosObjects2= [];
gdjs.GameCode.GDobjetosObjects3= [];
gdjs.GameCode.GDobjetosObjects4= [];
gdjs.GameCode.GDobjetosObjects5= [];
gdjs.GameCode.GDMindaObjects1= [];
gdjs.GameCode.GDMindaObjects2= [];
gdjs.GameCode.GDMindaObjects3= [];
gdjs.GameCode.GDMindaObjects4= [];
gdjs.GameCode.GDMindaObjects5= [];
gdjs.GameCode.GDTilebgObjects1= [];
gdjs.GameCode.GDTilebgObjects2= [];
gdjs.GameCode.GDTilebgObjects3= [];
gdjs.GameCode.GDTilebgObjects4= [];
gdjs.GameCode.GDTilebgObjects5= [];
gdjs.GameCode.GDAvatarObjects1= [];
gdjs.GameCode.GDAvatarObjects2= [];
gdjs.GameCode.GDAvatarObjects3= [];
gdjs.GameCode.GDAvatarObjects4= [];
gdjs.GameCode.GDAvatarObjects5= [];
gdjs.GameCode.GDthinkObjects1= [];
gdjs.GameCode.GDthinkObjects2= [];
gdjs.GameCode.GDthinkObjects3= [];
gdjs.GameCode.GDthinkObjects4= [];
gdjs.GameCode.GDthinkObjects5= [];
gdjs.GameCode.GDadivinhaObjects1= [];
gdjs.GameCode.GDadivinhaObjects2= [];
gdjs.GameCode.GDadivinhaObjects3= [];
gdjs.GameCode.GDadivinhaObjects4= [];
gdjs.GameCode.GDadivinhaObjects5= [];
gdjs.GameCode.GDspawn_95enemyObjects1= [];
gdjs.GameCode.GDspawn_95enemyObjects2= [];
gdjs.GameCode.GDspawn_95enemyObjects3= [];
gdjs.GameCode.GDspawn_95enemyObjects4= [];
gdjs.GameCode.GDspawn_95enemyObjects5= [];
gdjs.GameCode.GDspawn_95objectsObjects1= [];
gdjs.GameCode.GDspawn_95objectsObjects2= [];
gdjs.GameCode.GDspawn_95objectsObjects3= [];
gdjs.GameCode.GDspawn_95objectsObjects4= [];
gdjs.GameCode.GDspawn_95objectsObjects5= [];
gdjs.GameCode.GDtxtAdivinhaObjects1= [];
gdjs.GameCode.GDtxtAdivinhaObjects2= [];
gdjs.GameCode.GDtxtAdivinhaObjects3= [];
gdjs.GameCode.GDtxtAdivinhaObjects4= [];
gdjs.GameCode.GDtxtAdivinhaObjects5= [];
gdjs.GameCode.GDtxtOqueObjects1= [];
gdjs.GameCode.GDtxtOqueObjects2= [];
gdjs.GameCode.GDtxtOqueObjects3= [];
gdjs.GameCode.GDtxtOqueObjects4= [];
gdjs.GameCode.GDtxtOqueObjects5= [];
gdjs.GameCode.GDenemyObjects1= [];
gdjs.GameCode.GDenemyObjects2= [];
gdjs.GameCode.GDenemyObjects3= [];
gdjs.GameCode.GDenemyObjects4= [];
gdjs.GameCode.GDenemyObjects5= [];
gdjs.GameCode.GDacertosObjects1= [];
gdjs.GameCode.GDacertosObjects2= [];
gdjs.GameCode.GDacertosObjects3= [];
gdjs.GameCode.GDacertosObjects4= [];
gdjs.GameCode.GDacertosObjects5= [];
gdjs.GameCode.GDlblAcertosObjects1= [];
gdjs.GameCode.GDlblAcertosObjects2= [];
gdjs.GameCode.GDlblAcertosObjects3= [];
gdjs.GameCode.GDlblAcertosObjects4= [];
gdjs.GameCode.GDlblAcertosObjects5= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};


gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDMindaObjects1Objects = Hashtable.newFrom({"Minda": gdjs.GameCode.GDMindaObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDadivinhaObjects1Objects = Hashtable.newFrom({"adivinha": gdjs.GameCode.GDadivinhaObjects1});gdjs.GameCode.eventsList0x6c5f80 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("n1")) > 7;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("n1").setNumber(1);
}}

}


{

gdjs.GameCode.GDobjetosObjects4.createFrom(runtimeScene.getObjects("objetos"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobjetosObjects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDobjetosObjects4[i].getVariableNumber(gdjs.GameCode.GDobjetosObjects4[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("n1")) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobjetosObjects4[k] = gdjs.GameCode.GDobjetosObjects4[i];
        ++k;
    }
}
gdjs.GameCode.GDobjetosObjects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("n2")) == 30;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDobjetosObjects4 */
{for(var i = 0, len = gdjs.GameCode.GDobjetosObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDobjetosObjects4[i].setAnimation(24);
}
}{runtimeScene.getVariables().get("n2").add(1);
}}

}


{

gdjs.GameCode.GDobjetosObjects4.createFrom(runtimeScene.getObjects("objetos"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobjetosObjects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDobjetosObjects4[i].getVariableNumber(gdjs.GameCode.GDobjetosObjects4[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("n1")) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobjetosObjects4[k] = gdjs.GameCode.GDobjetosObjects4[i];
        ++k;
    }
}
gdjs.GameCode.GDobjetosObjects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("n2")) < 30;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDobjetosObjects4 */
{for(var i = 0, len = gdjs.GameCode.GDobjetosObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDobjetosObjects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("n2")));
}
}{runtimeScene.getVariables().get("n2").add(1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("n2")) > 30;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("n2").setNumber(0);
}}

}


}; //End of gdjs.GameCode.eventsList0x6c5f80
gdjs.GameCode.eventsList0x6c9ea8 = function(runtimeScene) {

{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("É surdo e mudo, mas conta tudo.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Voa sem ter asas e chora sem ter olhos.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 2;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Corre a casa inteira e depois vai dormir num canto.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(2);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 3;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("É grande antes de ser pequena.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(3);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 4;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Tem um palmo de pescoço, tem barriga e não tem osso.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(4);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 5;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Não se come, mas é bom para se comer.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(5);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 6;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Tem mais de dez cabeças e não sabe pensar.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(6);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 7;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Tem cauda, mas não é cão; não tem asas, mas sabe voar. Se a largam, não sobe, e sai ao vento a brincar.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(7);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 8;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Uma casinha sem porta e sem janela.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(8);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 9;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("O gafanhoto traz na frente e a pulga traz atrás.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(9);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Tem 5 dedos, mas não tem unha.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(10);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 11;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Fica cheio durante o dia vazio durante a noite.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(11);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 12;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Há no meio do coração.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(12);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 13;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Está no final do fim, no início do meio e no meio do começo.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(13);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 14;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Todo mês tem, menos abril.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(14);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 15;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Fica no início da rua, no fim do mar e no meio da cara.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(15);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 16;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Está sempre no meio da rua e de pernas para o ar.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(16);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 17;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Tem no meio do ovo.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(17);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 18;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Tem dentes e não come.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(18);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 19;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Tem coroa, mas não é rei; tem espinho, mas não é peixe.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(19);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 20;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("É verde e não é planta, fala e não é gente.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(20);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 21;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Dá um pulo e se veste de noiva.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(21);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 22;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("O bicho que anda com as patas.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(22);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 23;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Enche uma casa, mas não enche uma mão.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(23);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 24;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Dá muitas voltas e não sai do lugar.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(24);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 25;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("De dia tem quatro pés e de noite tem seis.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(25);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 26;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Fica cheio de boca para baixo e vazio de boca para cima.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(26);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 27;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Quanto mais rugas têm mais novo é.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(27);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 28;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Sobe quando a chuva desce.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(28);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 29;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Tem pernas, mas não anda; tem braços, mas não abraça.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(29);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 30;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setString("Tem uma perna longa, uma curta e anda sem parar.");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(24);
}}

}


{


{
gdjs.GameCode.GDtxtAdivinhaObjects2.createFrom(runtimeScene.getObjects("txtAdivinha"));
{for(var i = 0, len = gdjs.GameCode.GDtxtAdivinhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtxtAdivinhaObjects2[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}}

}


{


gdjs.GameCode.repeatCount3 = 7;
for(gdjs.GameCode.repeatIndex3 = 0;gdjs.GameCode.repeatIndex3 < gdjs.GameCode.repeatCount3;++gdjs.GameCode.repeatIndex3) {

if (true)
{
{runtimeScene.getVariables().get("n1").add(1);
}
{ //Subevents: 
gdjs.GameCode.eventsList0x6c5f80(runtimeScene);} //Subevents end.
}
}

}


{


{
gdjs.GameCode.GDobjetosObjects1.createFrom(runtimeScene.getObjects("objetos"));
{for(var i = 0, len = gdjs.GameCode.GDobjetosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobjetosObjects1[i].hide(false);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x6c9ea8
gdjs.GameCode.eventsList0x6cb308 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = !(gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up"));
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = !(gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down"));
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
gdjs.GameCode.GDMindaObjects3.createFrom(gdjs.GameCode.GDMindaObjects2);

{for(var i = 0, len = gdjs.GameCode.GDMindaObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects3[i].setAnimation(1);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = !(gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up"));
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = !(gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down"));
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
gdjs.GameCode.GDMindaObjects3.createFrom(gdjs.GameCode.GDMindaObjects2);

{for(var i = 0, len = gdjs.GameCode.GDMindaObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects3[i].setAnimation(2);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = !(gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left"));
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = !(gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right"));
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
gdjs.GameCode.GDMindaObjects3.createFrom(gdjs.GameCode.GDMindaObjects2);

{for(var i = 0, len = gdjs.GameCode.GDMindaObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects3[i].setAnimation(3);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = !(gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left"));
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = !(gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right"));
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
gdjs.GameCode.GDMindaObjects3.createFrom(gdjs.GameCode.GDMindaObjects2);

{for(var i = 0, len = gdjs.GameCode.GDMindaObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects3[i].setAnimation(4);
}
}}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDMindaObjects3.createFrom(gdjs.GameCode.GDMindaObjects2);

{for(var i = 0, len = gdjs.GameCode.GDMindaObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects3[i].setAnimation(5);
}
}{for(var i = 0, len = gdjs.GameCode.GDMindaObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects3[i].addForce(-20, 45, 0);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDMindaObjects3.createFrom(gdjs.GameCode.GDMindaObjects2);

{for(var i = 0, len = gdjs.GameCode.GDMindaObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects3[i].setAnimation(6);
}
}{for(var i = 0, len = gdjs.GameCode.GDMindaObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects3[i].addForce(-45, -45, 0);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDMindaObjects3.createFrom(gdjs.GameCode.GDMindaObjects2);

{for(var i = 0, len = gdjs.GameCode.GDMindaObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects3[i].setAnimation(7);
}
}{for(var i = 0, len = gdjs.GameCode.GDMindaObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects3[i].addForce(45, 45, 0);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDMindaObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDMindaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects2[i].setAnimation(8);
}
}{for(var i = 0, len = gdjs.GameCode.GDMindaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects2[i].addForce(20, -45, 0);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x6cb308
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDadivinhaObjects1Objects = Hashtable.newFrom({"adivinha": gdjs.GameCode.GDadivinhaObjects1});gdjs.GameCode.eventsList0x6cce68 = function(runtimeScene) {

{

gdjs.GameCode.GDadivinhaObjects1.createFrom(runtimeScene.getObjects("adivinha"));
gdjs.GameCode.GDthinkObjects1.createFrom(runtimeScene.getObjects("think"));
gdjs.GameCode.GDtxtAdivinhaObjects1.createFrom(runtimeScene.getObjects("txtAdivinha"));
gdjs.GameCode.GDtxtOqueObjects1.createFrom(runtimeScene.getObjects("txtOque"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDthinkObjects1.length;i<l;++i) {
    if ( !(gdjs.GameCode.GDthinkObjects1[i].isVisible()) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDthinkObjects1[k] = gdjs.GameCode.GDthinkObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDthinkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDtxtAdivinhaObjects1.length;i<l;++i) {
    if ( !(gdjs.GameCode.GDtxtAdivinhaObjects1[i].isVisible()) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDtxtAdivinhaObjects1[k] = gdjs.GameCode.GDtxtAdivinhaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDtxtAdivinhaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDtxtOqueObjects1.length;i<l;++i) {
    if ( !(gdjs.GameCode.GDtxtOqueObjects1[i].isVisible()) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDtxtOqueObjects1[k] = gdjs.GameCode.GDtxtOqueObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDtxtOqueObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDadivinhaObjects1Objects) == 0;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDthinkObjects1 */
/* Reuse gdjs.GameCode.GDtxtAdivinhaObjects1 */
/* Reuse gdjs.GameCode.GDtxtOqueObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDthinkObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDthinkObjects1[i].hide(false);
}
for(var i = 0, len = gdjs.GameCode.GDtxtAdivinhaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtxtAdivinhaObjects1[i].hide(false);
}
for(var i = 0, len = gdjs.GameCode.GDtxtOqueObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtxtOqueObjects1[i].hide(false);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x6cce68
gdjs.GameCode.eventsList0x66c200 = function(runtimeScene) {

{

gdjs.GameCode.GDMindaObjects2.createFrom(runtimeScene.getObjects("Minda"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDMindaObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDMindaObjects2[i].getBehavior("TopDownMovement").isMoving() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDMindaObjects2[k] = gdjs.GameCode.GDMindaObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDMindaObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDthinkObjects2.createFrom(runtimeScene.getObjects("think"));
gdjs.GameCode.GDtxtAdivinhaObjects2.createFrom(runtimeScene.getObjects("txtAdivinha"));
gdjs.GameCode.GDtxtOqueObjects2.createFrom(runtimeScene.getObjects("txtOque"));
{for(var i = 0, len = gdjs.GameCode.GDthinkObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDthinkObjects2[i].hide();
}
for(var i = 0, len = gdjs.GameCode.GDtxtAdivinhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtxtAdivinhaObjects2[i].hide();
}
for(var i = 0, len = gdjs.GameCode.GDtxtOqueObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtxtOqueObjects2[i].hide();
}
}
{ //Subevents
gdjs.GameCode.eventsList0x6cb308(runtimeScene);} //End of subevents
}

}


{

gdjs.GameCode.GDMindaObjects1.createFrom(runtimeScene.getObjects("Minda"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDMindaObjects1.length;i<l;++i) {
    if ( !(gdjs.GameCode.GDMindaObjects1[i].getBehavior("TopDownMovement").isMoving()) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDMindaObjects1[k] = gdjs.GameCode.GDMindaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDMindaObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDMindaObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDMindaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects1[i].setAnimation(0);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x6cce68(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x66c200
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDTilebgObjects1Objects = Hashtable.newFrom({"Tilebg": gdjs.GameCode.GDTilebgObjects1});gdjs.GameCode.eventsList0x6cd2e8 = function(runtimeScene) {

{



}


{


{
gdjs.GameCode.GDMindaObjects1.createFrom(runtimeScene.getObjects("Minda"));
gdjs.GameCode.GDTilebgObjects1.createFrom(runtimeScene.getObjects("Tilebg"));
{for(var i = 0, len = gdjs.GameCode.GDMindaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDMindaObjects1[i].separateFromObjectsList(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDTilebgObjects1Objects);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x6cd2e8
gdjs.GameCode.eventsList0x6cd670 = function(runtimeScene) {

{


{
gdjs.GameCode.GDMindaObjects1.createFrom(runtimeScene.getObjects("Minda"));
{gdjs.evtTools.camera.centerCameraWithinLimits(runtimeScene, (gdjs.GameCode.GDMindaObjects1.length !== 0 ? gdjs.GameCode.GDMindaObjects1[0] : null), 0, 0, 1920, 1080, true, "", 0);
}{gdjs.evtTools.camera.centerCameraWithinLimits(runtimeScene, (gdjs.GameCode.GDMindaObjects1.length !== 0 ? gdjs.GameCode.GDMindaObjects1[0] : null), 0, 0, 1920, 1080, true, "Player", 0);
}}

}


}; //End of gdjs.GameCode.eventsList0x6cd670
gdjs.GameCode.eventsList0x6cde80 = function(runtimeScene) {

{

gdjs.GameCode.GDMindaObjects2.createFrom(gdjs.GameCode.GDMindaObjects1);

gdjs.GameCode.GDenemyObjects2.createFrom(gdjs.GameCode.GDenemyObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDMindaObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDMindaObjects2[i].getX() > (( gdjs.GameCode.GDenemyObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDenemyObjects2[0].getPointX("")) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDMindaObjects2[k] = gdjs.GameCode.GDMindaObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDMindaObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemyObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDenemyObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDenemyObjects2[i].flipX(true);
}
}}

}


{

gdjs.GameCode.GDMindaObjects2.createFrom(gdjs.GameCode.GDMindaObjects1);

gdjs.GameCode.GDenemyObjects2.createFrom(gdjs.GameCode.GDenemyObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDMindaObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDMindaObjects2[i].getX() < (( gdjs.GameCode.GDenemyObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDenemyObjects2[0].getPointX("")) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDMindaObjects2[k] = gdjs.GameCode.GDMindaObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDMindaObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDenemyObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDenemyObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDenemyObjects2[i].flipX(false);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.sound.isSoundOnChannelStopped(runtimeScene, 3);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "bird_flap.ogg", 3, false, 100, 1);
}}

}


}; //End of gdjs.GameCode.eventsList0x6cde80
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDMindaObjects1Objects = Hashtable.newFrom({"Minda": gdjs.GameCode.GDMindaObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemyObjects1Objects = Hashtable.newFrom({"enemy": gdjs.GameCode.GDenemyObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDMindaObjects1Objects = Hashtable.newFrom({"Minda": gdjs.GameCode.GDMindaObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobjetosObjects1Objects = Hashtable.newFrom({"objetos": gdjs.GameCode.GDobjetosObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDMindaObjects1Objects = Hashtable.newFrom({"Minda": gdjs.GameCode.GDMindaObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobjetosObjects1Objects = Hashtable.newFrom({"objetos": gdjs.GameCode.GDobjetosObjects1});gdjs.GameCode.eventsList0xb1208 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDobjetosObjects1.createFrom(runtimeScene.getObjects("objetos"));
gdjs.GameCode.GDthinkObjects1.createFrom(runtimeScene.getObjects("think"));
gdjs.GameCode.GDtxtAdivinhaObjects1.createFrom(runtimeScene.getObjects("txtAdivinha"));
gdjs.GameCode.GDtxtOqueObjects1.createFrom(runtimeScene.getObjects("txtOque"));
{for(var i = 0, len = gdjs.GameCode.GDthinkObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDthinkObjects1[i].hide();
}
for(var i = 0, len = gdjs.GameCode.GDtxtAdivinhaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtxtAdivinhaObjects1[i].hide();
}
for(var i = 0, len = gdjs.GameCode.GDtxtOqueObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtxtOqueObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDobjetosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobjetosObjects1[i].hide();
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDacertosObjects1.createFrom(runtimeScene.getObjects("acertos"));
{for(var i = 0, len = gdjs.GameCode.GDacertosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDacertosObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)));
}
}}

}


{

gdjs.GameCode.GDMindaObjects1.createFrom(runtimeScene.getObjects("Minda"));
gdjs.GameCode.GDadivinhaObjects1.createFrom(runtimeScene.getObjects("adivinha"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDMindaObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDadivinhaObjects1Objects, false, runtimeScene);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7118772);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDadivinhaObjects1 */
gdjs.GameCode.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(30));
}{runtimeScene.getVariables().get("n1").setNumber(gdjs.randomInRange(1,  7));
}{runtimeScene.getVariables().get("n2").setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)));
}{for(var i = 0, len = gdjs.GameCode.GDadivinhaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDadivinhaObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.GameCode.GDenemyObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemyObjects1[i].returnVariable(gdjs.GameCode.GDenemyObjects1[i].getVariables().getFromIndex(0)).setNumber(1);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise06.ogg", false, 100, 1);
}
{ //Subevents
gdjs.GameCode.eventsList0x6c9ea8(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.eventsList0x66c200(runtimeScene);
}


{


gdjs.GameCode.eventsList0x6cd2e8(runtimeScene);
}


{


gdjs.GameCode.eventsList0x6cd670(runtimeScene);
}


{



}


{


{
gdjs.GameCode.GDMindaObjects1.createFrom(runtimeScene.getObjects("Minda"));
gdjs.GameCode.GDthinkObjects1.createFrom(runtimeScene.getObjects("think"));
gdjs.GameCode.GDtxtAdivinhaObjects1.createFrom(runtimeScene.getObjects("txtAdivinha"));
gdjs.GameCode.GDtxtOqueObjects1.createFrom(runtimeScene.getObjects("txtOque"));
{for(var i = 0, len = gdjs.GameCode.GDthinkObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDthinkObjects1[i].setPosition((( gdjs.GameCode.GDMindaObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDMindaObjects1[0].getPointX("")) - (gdjs.GameCode.GDthinkObjects1[i].getWidth()) + 50,(( gdjs.GameCode.GDMindaObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDMindaObjects1[0].getPointY("")) - (gdjs.GameCode.GDthinkObjects1[i].getHeight()) + 20);
}
}{for(var i = 0, len = gdjs.GameCode.GDtxtOqueObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtxtOqueObjects1[i].setPosition((( gdjs.GameCode.GDthinkObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDthinkObjects1[0].getPointX("")) + (( gdjs.GameCode.GDthinkObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDthinkObjects1[0].getWidth())/2 - (gdjs.GameCode.GDtxtOqueObjects1[i].getWidth())/2,(( gdjs.GameCode.GDthinkObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDthinkObjects1[0].getPointY("")) + 25);
}
}{for(var i = 0, len = gdjs.GameCode.GDtxtAdivinhaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtxtAdivinhaObjects1[i].setPosition((( gdjs.GameCode.GDthinkObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDthinkObjects1[0].getPointX("")) + (( gdjs.GameCode.GDthinkObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDthinkObjects1[0].getWidth())/2 + 5 - (gdjs.GameCode.GDtxtAdivinhaObjects1[i].getWidth())/2,(( gdjs.GameCode.GDthinkObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDthinkObjects1[0].getPointY("")) + 55);
}
}}

}


{

gdjs.GameCode.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemyObjects1[i].getVariableNumber(gdjs.GameCode.GDenemyObjects1[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemyObjects1[k] = gdjs.GameCode.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemyObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDMindaObjects1.createFrom(runtimeScene.getObjects("Minda"));
/* Reuse gdjs.GameCode.GDenemyObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDenemyObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemyObjects1[i].getBehavior("Pathfinding").moveTo(runtimeScene, (( gdjs.GameCode.GDMindaObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDMindaObjects1[0].getPointX("")), (( gdjs.GameCode.GDMindaObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDMindaObjects1[0].getPointY("")));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x6cde80(runtimeScene);} //End of subevents
}

}


{

gdjs.GameCode.GDMindaObjects1.createFrom(runtimeScene.getObjects("Minda"));
gdjs.GameCode.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDenemyObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDenemyObjects1[i].getVariableNumber(gdjs.GameCode.GDenemyObjects1[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDenemyObjects1[k] = gdjs.GameCode.GDenemyObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDenemyObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDMindaObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDenemyObjects1Objects, false, runtimeScene);
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GameOver", false);
}}

}


{

gdjs.GameCode.GDMindaObjects1.createFrom(runtimeScene.getObjects("Minda"));
gdjs.GameCode.GDobjetosObjects1.createFrom(runtimeScene.getObjects("objetos"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobjetosObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDobjetosObjects1[i].isVisible() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobjetosObjects1[k] = gdjs.GameCode.GDobjetosObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDobjetosObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDMindaObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobjetosObjects1Objects, false, runtimeScene);
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobjetosObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDobjetosObjects1[i].getAnimation() == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDobjetosObjects1[k] = gdjs.GameCode.GDobjetosObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDobjetosObjects1.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Acertou", false);
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise06.ogg", false, 100, 1);
}}

}


{

gdjs.GameCode.GDMindaObjects1.createFrom(runtimeScene.getObjects("Minda"));
gdjs.GameCode.GDobjetosObjects1.createFrom(runtimeScene.getObjects("objetos"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobjetosObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDobjetosObjects1[i].isVisible() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDobjetosObjects1[k] = gdjs.GameCode.GDobjetosObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDobjetosObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDMindaObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDobjetosObjects1Objects, false, runtimeScene);
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDobjetosObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDobjetosObjects1[i].getAnimation() != gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDobjetosObjects1[k] = gdjs.GameCode.GDobjetosObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDobjetosObjects1.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
gdjs.GameCode.GDenemyObjects1.createFrom(runtimeScene.getObjects("enemy"));
/* Reuse gdjs.GameCode.GDobjetosObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDobjetosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDobjetosObjects1[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getGame().getVariables().getFromIndex(3).add(5);
}{for(var i = 0, len = gdjs.GameCode.GDenemyObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDenemyObjects1[i].getBehavior("Pathfinding").setMaxSpeed(gdjs.GameCode.GDenemyObjects1[i].getBehavior("Pathfinding").getMaxSpeed() + (gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3))));
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Downer01.ogg", false, 100, 1);
}}

}


}; //End of gdjs.GameCode.eventsList0xb1208


gdjs.GameCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.GameCode.GDobjetosObjects1.length = 0;
gdjs.GameCode.GDobjetosObjects2.length = 0;
gdjs.GameCode.GDobjetosObjects3.length = 0;
gdjs.GameCode.GDobjetosObjects4.length = 0;
gdjs.GameCode.GDobjetosObjects5.length = 0;
gdjs.GameCode.GDMindaObjects1.length = 0;
gdjs.GameCode.GDMindaObjects2.length = 0;
gdjs.GameCode.GDMindaObjects3.length = 0;
gdjs.GameCode.GDMindaObjects4.length = 0;
gdjs.GameCode.GDMindaObjects5.length = 0;
gdjs.GameCode.GDTilebgObjects1.length = 0;
gdjs.GameCode.GDTilebgObjects2.length = 0;
gdjs.GameCode.GDTilebgObjects3.length = 0;
gdjs.GameCode.GDTilebgObjects4.length = 0;
gdjs.GameCode.GDTilebgObjects5.length = 0;
gdjs.GameCode.GDAvatarObjects1.length = 0;
gdjs.GameCode.GDAvatarObjects2.length = 0;
gdjs.GameCode.GDAvatarObjects3.length = 0;
gdjs.GameCode.GDAvatarObjects4.length = 0;
gdjs.GameCode.GDAvatarObjects5.length = 0;
gdjs.GameCode.GDthinkObjects1.length = 0;
gdjs.GameCode.GDthinkObjects2.length = 0;
gdjs.GameCode.GDthinkObjects3.length = 0;
gdjs.GameCode.GDthinkObjects4.length = 0;
gdjs.GameCode.GDthinkObjects5.length = 0;
gdjs.GameCode.GDadivinhaObjects1.length = 0;
gdjs.GameCode.GDadivinhaObjects2.length = 0;
gdjs.GameCode.GDadivinhaObjects3.length = 0;
gdjs.GameCode.GDadivinhaObjects4.length = 0;
gdjs.GameCode.GDadivinhaObjects5.length = 0;
gdjs.GameCode.GDspawn_95enemyObjects1.length = 0;
gdjs.GameCode.GDspawn_95enemyObjects2.length = 0;
gdjs.GameCode.GDspawn_95enemyObjects3.length = 0;
gdjs.GameCode.GDspawn_95enemyObjects4.length = 0;
gdjs.GameCode.GDspawn_95enemyObjects5.length = 0;
gdjs.GameCode.GDspawn_95objectsObjects1.length = 0;
gdjs.GameCode.GDspawn_95objectsObjects2.length = 0;
gdjs.GameCode.GDspawn_95objectsObjects3.length = 0;
gdjs.GameCode.GDspawn_95objectsObjects4.length = 0;
gdjs.GameCode.GDspawn_95objectsObjects5.length = 0;
gdjs.GameCode.GDtxtAdivinhaObjects1.length = 0;
gdjs.GameCode.GDtxtAdivinhaObjects2.length = 0;
gdjs.GameCode.GDtxtAdivinhaObjects3.length = 0;
gdjs.GameCode.GDtxtAdivinhaObjects4.length = 0;
gdjs.GameCode.GDtxtAdivinhaObjects5.length = 0;
gdjs.GameCode.GDtxtOqueObjects1.length = 0;
gdjs.GameCode.GDtxtOqueObjects2.length = 0;
gdjs.GameCode.GDtxtOqueObjects3.length = 0;
gdjs.GameCode.GDtxtOqueObjects4.length = 0;
gdjs.GameCode.GDtxtOqueObjects5.length = 0;
gdjs.GameCode.GDenemyObjects1.length = 0;
gdjs.GameCode.GDenemyObjects2.length = 0;
gdjs.GameCode.GDenemyObjects3.length = 0;
gdjs.GameCode.GDenemyObjects4.length = 0;
gdjs.GameCode.GDenemyObjects5.length = 0;
gdjs.GameCode.GDacertosObjects1.length = 0;
gdjs.GameCode.GDacertosObjects2.length = 0;
gdjs.GameCode.GDacertosObjects3.length = 0;
gdjs.GameCode.GDacertosObjects4.length = 0;
gdjs.GameCode.GDacertosObjects5.length = 0;
gdjs.GameCode.GDlblAcertosObjects1.length = 0;
gdjs.GameCode.GDlblAcertosObjects2.length = 0;
gdjs.GameCode.GDlblAcertosObjects3.length = 0;
gdjs.GameCode.GDlblAcertosObjects4.length = 0;
gdjs.GameCode.GDlblAcertosObjects5.length = 0;

gdjs.GameCode.eventsList0xb1208(runtimeScene);
return;
}
gdjs['GameCode'] = gdjs.GameCode;
