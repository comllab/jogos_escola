gdjs.AcertouCode = {};
gdjs.AcertouCode.GDobjetosObjects1= [];
gdjs.AcertouCode.GDobjetosObjects2= [];
gdjs.AcertouCode.GDobjetosObjects3= [];
gdjs.AcertouCode.GDcontinuarObjects1= [];
gdjs.AcertouCode.GDcontinuarObjects2= [];
gdjs.AcertouCode.GDcontinuarObjects3= [];
gdjs.AcertouCode.GDCopyOfmuitoBemObjects1= [];
gdjs.AcertouCode.GDCopyOfmuitoBemObjects2= [];
gdjs.AcertouCode.GDCopyOfmuitoBemObjects3= [];
gdjs.AcertouCode.GDmuitoBemObjects1= [];
gdjs.AcertouCode.GDmuitoBemObjects2= [];
gdjs.AcertouCode.GDmuitoBemObjects3= [];
gdjs.AcertouCode.GDbackgroundObjects1= [];
gdjs.AcertouCode.GDbackgroundObjects2= [];
gdjs.AcertouCode.GDbackgroundObjects3= [];
gdjs.AcertouCode.GDbuttomObjects1= [];
gdjs.AcertouCode.GDbuttomObjects2= [];
gdjs.AcertouCode.GDbuttomObjects3= [];
gdjs.AcertouCode.GDrespostaObjects1= [];
gdjs.AcertouCode.GDrespostaObjects2= [];
gdjs.AcertouCode.GDrespostaObjects3= [];
gdjs.AcertouCode.GDadivinhaObjects1= [];
gdjs.AcertouCode.GDadivinhaObjects2= [];
gdjs.AcertouCode.GDadivinhaObjects3= [];

gdjs.AcertouCode.conditionTrue_0 = {val:false};
gdjs.AcertouCode.condition0IsTrue_0 = {val:false};
gdjs.AcertouCode.condition1IsTrue_0 = {val:false};
gdjs.AcertouCode.condition2IsTrue_0 = {val:false};
gdjs.AcertouCode.condition3IsTrue_0 = {val:false};
gdjs.AcertouCode.conditionTrue_1 = {val:false};
gdjs.AcertouCode.condition0IsTrue_1 = {val:false};
gdjs.AcertouCode.condition1IsTrue_1 = {val:false};
gdjs.AcertouCode.condition2IsTrue_1 = {val:false};
gdjs.AcertouCode.condition3IsTrue_1 = {val:false};


gdjs.AcertouCode.eventsList0x6c8b08 = function(runtimeScene) {

{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "O Livro.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 1;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A nuvem.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 2;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A vassoura.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 3;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A vela.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 4;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A garrafa.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 5;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A colher.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 6;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "Uma caixa de fósforos.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 7;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A pipa.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 8;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "O ovo.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 9;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A sílaba GA.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 10;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A luva.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 11;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "O sapato.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 12;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A letra A.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 13;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A letra M.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 14;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A letra O.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 15;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A letra R.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 16;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A letra U.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 17;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A letra V.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 18;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "O pente.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 19;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "O abacaxi.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 20;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "Papagaio.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 21;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "Pipoca.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 22;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "O pato.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 23;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "Um botão.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 24;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "O relógio.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 25;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A cama.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 26;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "O chapéu.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 27;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "O pneu.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 28;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "O guarda-chuva.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 29;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects2.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects2.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects2[i].setString("R: " + "A cadeira.");
}
}}

}


{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 30;
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDrespostaObjects1.createFrom(runtimeScene.getObjects("resposta"));
{for(var i = 0, len = gdjs.AcertouCode.GDrespostaObjects1.length ;i < len;++i) {
    gdjs.AcertouCode.GDrespostaObjects1[i].setString("R: " + "O relógio.");
}
}}

}


}; //End of gdjs.AcertouCode.eventsList0x6c8b08
gdjs.AcertouCode.mapOfGDgdjs_46AcertouCode_46GDbuttomObjects1Objects = Hashtable.newFrom({"buttom": gdjs.AcertouCode.GDbuttomObjects1});gdjs.AcertouCode.eventsList0xb1208 = function(runtimeScene) {

{


gdjs.AcertouCode.condition0IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.AcertouCode.condition0IsTrue_0.val) {
gdjs.AcertouCode.GDadivinhaObjects1.createFrom(runtimeScene.getObjects("adivinha"));
gdjs.AcertouCode.GDobjetosObjects1.createFrom(runtimeScene.getObjects("objetos"));
{for(var i = 0, len = gdjs.AcertouCode.GDadivinhaObjects1.length ;i < len;++i) {
    gdjs.AcertouCode.GDadivinhaObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.AcertouCode.GDobjetosObjects1.length ;i < len;++i) {
    gdjs.AcertouCode.GDobjetosObjects1[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}
{ //Subevents
gdjs.AcertouCode.eventsList0x6c8b08(runtimeScene);} //End of subevents
}

}


{

gdjs.AcertouCode.GDbuttomObjects1.createFrom(runtimeScene.getObjects("buttom"));

gdjs.AcertouCode.condition0IsTrue_0.val = false;
gdjs.AcertouCode.condition1IsTrue_0.val = false;
gdjs.AcertouCode.condition2IsTrue_0.val = false;
{
gdjs.AcertouCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.AcertouCode.mapOfGDgdjs_46AcertouCode_46GDbuttomObjects1Objects, runtimeScene, true, false);
}if ( gdjs.AcertouCode.condition0IsTrue_0.val ) {
{
gdjs.AcertouCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.AcertouCode.condition1IsTrue_0.val ) {
{
{gdjs.AcertouCode.conditionTrue_1 = gdjs.AcertouCode.condition2IsTrue_0;
gdjs.AcertouCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7090564);
}
}}
}
if (gdjs.AcertouCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.AcertouCode.eventsList0xb1208


gdjs.AcertouCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.AcertouCode.GDobjetosObjects1.length = 0;
gdjs.AcertouCode.GDobjetosObjects2.length = 0;
gdjs.AcertouCode.GDobjetosObjects3.length = 0;
gdjs.AcertouCode.GDcontinuarObjects1.length = 0;
gdjs.AcertouCode.GDcontinuarObjects2.length = 0;
gdjs.AcertouCode.GDcontinuarObjects3.length = 0;
gdjs.AcertouCode.GDCopyOfmuitoBemObjects1.length = 0;
gdjs.AcertouCode.GDCopyOfmuitoBemObjects2.length = 0;
gdjs.AcertouCode.GDCopyOfmuitoBemObjects3.length = 0;
gdjs.AcertouCode.GDmuitoBemObjects1.length = 0;
gdjs.AcertouCode.GDmuitoBemObjects2.length = 0;
gdjs.AcertouCode.GDmuitoBemObjects3.length = 0;
gdjs.AcertouCode.GDbackgroundObjects1.length = 0;
gdjs.AcertouCode.GDbackgroundObjects2.length = 0;
gdjs.AcertouCode.GDbackgroundObjects3.length = 0;
gdjs.AcertouCode.GDbuttomObjects1.length = 0;
gdjs.AcertouCode.GDbuttomObjects2.length = 0;
gdjs.AcertouCode.GDbuttomObjects3.length = 0;
gdjs.AcertouCode.GDrespostaObjects1.length = 0;
gdjs.AcertouCode.GDrespostaObjects2.length = 0;
gdjs.AcertouCode.GDrespostaObjects3.length = 0;
gdjs.AcertouCode.GDadivinhaObjects1.length = 0;
gdjs.AcertouCode.GDadivinhaObjects2.length = 0;
gdjs.AcertouCode.GDadivinhaObjects3.length = 0;

gdjs.AcertouCode.eventsList0xb1208(runtimeScene);
return;
}
gdjs['AcertouCode'] = gdjs.AcertouCode;
