gdjs.GameOverCode = {};
gdjs.GameOverCode.GDobjetosObjects1= [];
gdjs.GameOverCode.GDobjetosObjects2= [];
gdjs.GameOverCode.GDDenovoObjects1= [];
gdjs.GameOverCode.GDDenovoObjects2= [];
gdjs.GameOverCode.GDgameOverObjects1= [];
gdjs.GameOverCode.GDgameOverObjects2= [];
gdjs.GameOverCode.GDbackgroundObjects1= [];
gdjs.GameOverCode.GDbackgroundObjects2= [];
gdjs.GameOverCode.GDbuttomObjects1= [];
gdjs.GameOverCode.GDbuttomObjects2= [];

gdjs.GameOverCode.conditionTrue_0 = {val:false};
gdjs.GameOverCode.condition0IsTrue_0 = {val:false};
gdjs.GameOverCode.condition1IsTrue_0 = {val:false};
gdjs.GameOverCode.condition2IsTrue_0 = {val:false};
gdjs.GameOverCode.condition3IsTrue_0 = {val:false};
gdjs.GameOverCode.conditionTrue_1 = {val:false};
gdjs.GameOverCode.condition0IsTrue_1 = {val:false};
gdjs.GameOverCode.condition1IsTrue_1 = {val:false};
gdjs.GameOverCode.condition2IsTrue_1 = {val:false};
gdjs.GameOverCode.condition3IsTrue_1 = {val:false};


gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbuttomObjects1Objects = Hashtable.newFrom({"buttom": gdjs.GameOverCode.GDbuttomObjects1});gdjs.GameOverCode.eventsList0xb1208 = function(runtimeScene) {

{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameOverCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}}

}


{

gdjs.GameOverCode.GDbuttomObjects1.createFrom(runtimeScene.getObjects("buttom"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
gdjs.GameOverCode.condition2IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbuttomObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameOverCode.condition1IsTrue_0.val ) {
{
{gdjs.GameOverCode.conditionTrue_1 = gdjs.GameOverCode.condition2IsTrue_0;
gdjs.GameOverCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(6793908);
}
}}
}
if (gdjs.GameOverCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.GameOverCode.eventsList0xb1208


gdjs.GameOverCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.GameOverCode.GDobjetosObjects1.length = 0;
gdjs.GameOverCode.GDobjetosObjects2.length = 0;
gdjs.GameOverCode.GDDenovoObjects1.length = 0;
gdjs.GameOverCode.GDDenovoObjects2.length = 0;
gdjs.GameOverCode.GDgameOverObjects1.length = 0;
gdjs.GameOverCode.GDgameOverObjects2.length = 0;
gdjs.GameOverCode.GDbackgroundObjects1.length = 0;
gdjs.GameOverCode.GDbackgroundObjects2.length = 0;
gdjs.GameOverCode.GDbuttomObjects1.length = 0;
gdjs.GameOverCode.GDbuttomObjects2.length = 0;

gdjs.GameOverCode.eventsList0xb1208(runtimeScene);
return;
}
gdjs['GameOverCode'] = gdjs.GameOverCode;
