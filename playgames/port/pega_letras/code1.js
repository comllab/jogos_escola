gdjs.GamePlaneCode = {};
gdjs.GamePlaneCode.GDplaneObjects1= [];
gdjs.GamePlaneCode.GDplaneObjects2= [];
gdjs.GamePlaneCode.GDpointsObjects1= [];
gdjs.GamePlaneCode.GDpointsObjects2= [];
gdjs.GamePlaneCode.GDbackground1Objects1= [];
gdjs.GamePlaneCode.GDbackground1Objects2= [];
gdjs.GamePlaneCode.GDrockObjects1= [];
gdjs.GamePlaneCode.GDrockObjects2= [];
gdjs.GamePlaneCode.GDrockSnowUPObjects1= [];
gdjs.GamePlaneCode.GDrockSnowUPObjects2= [];
gdjs.GamePlaneCode.GDrockSnowDOWNObjects1= [];
gdjs.GamePlaneCode.GDrockSnowDOWNObjects2= [];
gdjs.GamePlaneCode.GDalfabetoObjects1= [];
gdjs.GamePlaneCode.GDalfabetoObjects2= [];
gdjs.GamePlaneCode.GDlethersObjects1= [];
gdjs.GamePlaneCode.GDlethersObjects2= [];
gdjs.GamePlaneCode.GDpanelObjects1= [];
gdjs.GamePlaneCode.GDpanelObjects2= [];
gdjs.GamePlaneCode.GDgame_95overObjects1= [];
gdjs.GamePlaneCode.GDgame_95overObjects2= [];
gdjs.GamePlaneCode.GDbtnStartObjects1= [];
gdjs.GamePlaneCode.GDbtnStartObjects2= [];
gdjs.GamePlaneCode.GDtxtStartObjects1= [];
gdjs.GamePlaneCode.GDtxtStartObjects2= [];

gdjs.GamePlaneCode.conditionTrue_0 = {val:false};
gdjs.GamePlaneCode.condition0IsTrue_0 = {val:false};
gdjs.GamePlaneCode.condition1IsTrue_0 = {val:false};
gdjs.GamePlaneCode.condition2IsTrue_0 = {val:false};
gdjs.GamePlaneCode.condition3IsTrue_0 = {val:false};
gdjs.GamePlaneCode.conditionTrue_1 = {val:false};
gdjs.GamePlaneCode.condition0IsTrue_1 = {val:false};
gdjs.GamePlaneCode.condition1IsTrue_1 = {val:false};
gdjs.GamePlaneCode.condition2IsTrue_1 = {val:false};
gdjs.GamePlaneCode.condition3IsTrue_1 = {val:false};


gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDrockSnowDOWNObjects1Objects = Hashtable.newFrom({"rockSnowDOWN": gdjs.GamePlaneCode.GDrockSnowDOWNObjects1});gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDrockSnowUPObjects1Objects = Hashtable.newFrom({"rockSnowUP": gdjs.GamePlaneCode.GDrockSnowUPObjects1});gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDrockSnowDOWNObjects1Objects = Hashtable.newFrom({"rockSnowDOWN": gdjs.GamePlaneCode.GDrockSnowDOWNObjects1});gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDrockSnowUPObjects1Objects = Hashtable.newFrom({"rockSnowUP": gdjs.GamePlaneCode.GDrockSnowUPObjects1});gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDlethersObjects1Objects = Hashtable.newFrom({"lethers": gdjs.GamePlaneCode.GDlethersObjects1});gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDplaneObjects1Objects = Hashtable.newFrom({"plane": gdjs.GamePlaneCode.GDplaneObjects1});gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDrockSnowUPObjects1ObjectsGDgdjs_46GamePlaneCode_46GDrockSnowDOWNObjects1ObjectsGDgdjs_46GamePlaneCode_46GDrockObjects1Objects = Hashtable.newFrom({"rockSnowUP": gdjs.GamePlaneCode.GDrockSnowUPObjects1, "rockSnowDOWN": gdjs.GamePlaneCode.GDrockSnowDOWNObjects1, "rock": gdjs.GamePlaneCode.GDrockObjects1});gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDplaneObjects1Objects = Hashtable.newFrom({"plane": gdjs.GamePlaneCode.GDplaneObjects1});gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDlethersObjects1Objects = Hashtable.newFrom({"lethers": gdjs.GamePlaneCode.GDlethersObjects1});gdjs.GamePlaneCode.eventsList0x5d2748 = function(runtimeScene, context) {

{


{
}

}


}; //End of gdjs.GamePlaneCode.eventsList0x5d2748
gdjs.GamePlaneCode.eventsList0x5d3098 = function(runtimeScene, context) {

{

gdjs.GamePlaneCode.GDalfabetoObjects1.createFrom(runtimeScene.getObjects("alfabeto"));
/* Reuse gdjs.GamePlaneCode.GDlethersObjects1 */

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDalfabetoObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDalfabetoObjects1[i].getAnimation() == (( gdjs.GamePlaneCode.GDlethersObjects1.length === 0 ) ? 0 :gdjs.GamePlaneCode.GDlethersObjects1[0].getAnimation()) ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDalfabetoObjects1[k] = gdjs.GamePlaneCode.GDalfabetoObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDalfabetoObjects1.length = k;}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GamePlaneCode.GDalfabetoObjects1 */
/* Reuse gdjs.GamePlaneCode.GDlethersObjects1 */
{for(var i = 0, len = gdjs.GamePlaneCode.GDalfabetoObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDalfabetoObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDlethersObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDlethersObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.GamePlaneCode.eventsList0x5d3098
gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDbtnStartObjects1Objects = Hashtable.newFrom({"btnStart": gdjs.GamePlaneCode.GDbtnStartObjects1});gdjs.GamePlaneCode.eventsList0xac5d0 = function(runtimeScene, context) {

{


gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
gdjs.GamePlaneCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
gdjs.GamePlaneCode.GDalfabetoObjects1.createFrom(runtimeScene.getObjects("alfabeto"));
gdjs.GamePlaneCode.GDbtnStartObjects1.createFrom(runtimeScene.getObjects("btnStart"));
gdjs.GamePlaneCode.GDgame_95overObjects1.createFrom(runtimeScene.getObjects("game_over"));
gdjs.GamePlaneCode.GDtxtStartObjects1.createFrom(runtimeScene.getObjects("txtStart"));
gdjs.GamePlaneCode.GDrockSnowDOWNObjects1.length = 0;

gdjs.GamePlaneCode.GDrockSnowUPObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDrockSnowDOWNObjects1Objects, gdjs.evtTools.window.getCanvasWidth(runtimeScene), gdjs.random(100) - 100, "");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDrockSnowUPObjects1Objects, gdjs.evtTools.window.getCanvasWidth(runtimeScene), (( gdjs.GamePlaneCode.GDrockSnowDOWNObjects1.length === 0 ) ? 0 :gdjs.GamePlaneCode.GDrockSnowDOWNObjects1[0].getPointY("")) + (( gdjs.GamePlaneCode.GDrockSnowDOWNObjects1.length === 0 ) ? 0 :gdjs.GamePlaneCode.GDrockSnowDOWNObjects1[0].getHeight()) + gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)), "");
}{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "plane.ogg", 1, true, 15, 1);
}{runtimeScene.getVariables().getFromIndex(3).setNumber(gdjs.random(9) + 1);
}{for(var i = 0, len = gdjs.GamePlaneCode.GDbtnStartObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDbtnStartObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDtxtStartObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDtxtStartObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDgame_95overObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDgame_95overObjects1[i].hide();
}
}{gdjs.evtTools.sound.playMusic(runtimeScene, "Intro Theme.ogg", true, 70, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{for(var i = 0, len = gdjs.GamePlaneCode.GDalfabetoObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDalfabetoObjects1[i].hide();
}
}}

}


{


gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
gdjs.GamePlaneCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
gdjs.GamePlaneCode.GDlethersObjects1.createFrom(runtimeScene.getObjects("lethers"));
gdjs.GamePlaneCode.GDrockSnowDOWNObjects1.createFrom(runtimeScene.getObjects("rockSnowDOWN"));
gdjs.GamePlaneCode.GDrockSnowUPObjects1.createFrom(runtimeScene.getObjects("rockSnowUP"));
{for(var i = 0, len = gdjs.GamePlaneCode.GDrockSnowUPObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDrockSnowUPObjects1[i].addForce(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)), 0, 0);
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDrockSnowDOWNObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDrockSnowDOWNObjects1[i].addForce(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)), 0, 0);
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDlethersObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDlethersObjects1[i].addForce(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)), 0, 0);
}
}}

}


{

gdjs.GamePlaneCode.GDplaneObjects1.createFrom(runtimeScene.getObjects("plane"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
gdjs.GamePlaneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDplaneObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDplaneObjects1[i].getVariableNumber(gdjs.GamePlaneCode.GDplaneObjects1[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDplaneObjects1[k] = gdjs.GamePlaneCode.GDplaneObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDplaneObjects1.length = k;}if ( gdjs.GamePlaneCode.condition0IsTrue_0.val ) {
{
{gdjs.GamePlaneCode.conditionTrue_1 = gdjs.GamePlaneCode.condition1IsTrue_0;
gdjs.GamePlaneCode.conditionTrue_1.val = context.triggerOnce(6126820);
}
}}
if (gdjs.GamePlaneCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 1);
}}

}


{

gdjs.GamePlaneCode.GDplaneObjects1.createFrom(runtimeScene.getObjects("plane"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
gdjs.GamePlaneCode.condition1IsTrue_0.val = false;
gdjs.GamePlaneCode.condition2IsTrue_0.val = false;
{
gdjs.GamePlaneCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
}if ( gdjs.GamePlaneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDplaneObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDplaneObjects1[i].getVariableNumber(gdjs.GamePlaneCode.GDplaneObjects1[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GamePlaneCode.condition1IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDplaneObjects1[k] = gdjs.GamePlaneCode.GDplaneObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDplaneObjects1.length = k;}if ( gdjs.GamePlaneCode.condition1IsTrue_0.val ) {
{
{gdjs.GamePlaneCode.conditionTrue_1 = gdjs.GamePlaneCode.condition2IsTrue_0;
gdjs.GamePlaneCode.conditionTrue_1.val = context.triggerOnce(6127604);
}
}}
}
if (gdjs.GamePlaneCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GamePlaneCode.GDplaneObjects1 */
{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].getBehavior("Physics").setLinearVelocity(0, 0, runtimeScene);
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].getBehavior("Physics").applyImpulse(0, -1.5, runtimeScene);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "flap_plane.ogg", false, 100, 1);
}}

}


{

gdjs.GamePlaneCode.GDplaneObjects1.createFrom(runtimeScene.getObjects("plane"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
gdjs.GamePlaneCode.condition1IsTrue_0.val = false;
gdjs.GamePlaneCode.condition2IsTrue_0.val = false;
{
gdjs.GamePlaneCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GamePlaneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDplaneObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDplaneObjects1[i].getVariableNumber(gdjs.GamePlaneCode.GDplaneObjects1[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GamePlaneCode.condition1IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDplaneObjects1[k] = gdjs.GamePlaneCode.GDplaneObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDplaneObjects1.length = k;}if ( gdjs.GamePlaneCode.condition1IsTrue_0.val ) {
{
{gdjs.GamePlaneCode.conditionTrue_1 = gdjs.GamePlaneCode.condition2IsTrue_0;
gdjs.GamePlaneCode.conditionTrue_1.val = context.triggerOnce(6128908);
}
}}
}
if (gdjs.GamePlaneCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GamePlaneCode.GDplaneObjects1 */
{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].getBehavior("Physics").setLinearVelocity(0, 0, runtimeScene);
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].getBehavior("Physics").applyImpulse(0, -1.5, runtimeScene);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "flap_plane.ogg", false, 100, 1);
}}

}


{

gdjs.GamePlaneCode.GDplaneObjects1.createFrom(runtimeScene.getObjects("plane"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
gdjs.GamePlaneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDplaneObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDplaneObjects1[i].getVariableNumber(gdjs.GamePlaneCode.GDplaneObjects1[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDplaneObjects1[k] = gdjs.GamePlaneCode.GDplaneObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDplaneObjects1.length = k;}if ( gdjs.GamePlaneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDplaneObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDplaneObjects1[i].getAngle() <= -15 ) {
        gdjs.GamePlaneCode.condition1IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDplaneObjects1[k] = gdjs.GamePlaneCode.GDplaneObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDplaneObjects1.length = k;}}
if (gdjs.GamePlaneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GamePlaneCode.GDplaneObjects1 */
{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].setAngle(-15);
}
}}

}


{

gdjs.GamePlaneCode.GDplaneObjects1.createFrom(runtimeScene.getObjects("plane"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
gdjs.GamePlaneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDplaneObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDplaneObjects1[i].getVariableNumber(gdjs.GamePlaneCode.GDplaneObjects1[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDplaneObjects1[k] = gdjs.GamePlaneCode.GDplaneObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDplaneObjects1.length = k;}if ( gdjs.GamePlaneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDplaneObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDplaneObjects1[i].getAngle() >= 15 ) {
        gdjs.GamePlaneCode.condition1IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDplaneObjects1[k] = gdjs.GamePlaneCode.GDplaneObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDplaneObjects1.length = k;}}
if (gdjs.GamePlaneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GamePlaneCode.GDplaneObjects1 */
{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].setAngle(15);
}
}}

}


{

gdjs.GamePlaneCode.GDplaneObjects1.createFrom(runtimeScene.getObjects("plane"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDplaneObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDplaneObjects1[i].getBehavior("Physics").getLinearVelocityY(runtimeScene) < 0 ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDplaneObjects1[k] = gdjs.GamePlaneCode.GDplaneObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDplaneObjects1.length = k;}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GamePlaneCode.GDplaneObjects1 */
{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].getBehavior("Physics").setAngularVelocity(0.8, runtimeScene);
}
}}

}


{

gdjs.GamePlaneCode.GDplaneObjects1.createFrom(runtimeScene.getObjects("plane"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDplaneObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDplaneObjects1[i].getBehavior("Physics").getLinearVelocityY(runtimeScene) > 0 ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDplaneObjects1[k] = gdjs.GamePlaneCode.GDplaneObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDplaneObjects1.length = k;}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GamePlaneCode.GDplaneObjects1 */
{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].getBehavior("Physics").setAngularVelocity(-0.5, runtimeScene);
}
}}

}


{



}


{


gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
gdjs.GamePlaneCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), "rocks");
}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
gdjs.GamePlaneCode.GDrockSnowDOWNObjects1.length = 0;

gdjs.GamePlaneCode.GDrockSnowUPObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDrockSnowDOWNObjects1Objects, gdjs.evtTools.window.getCanvasWidth(runtimeScene), gdjs.random(100) - 100, "");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDrockSnowUPObjects1Objects, gdjs.evtTools.window.getCanvasWidth(runtimeScene), (( gdjs.GamePlaneCode.GDrockSnowDOWNObjects1.length === 0 ) ? 0 :gdjs.GamePlaneCode.GDrockSnowDOWNObjects1[0].getPointY("")) + (( gdjs.GamePlaneCode.GDrockSnowDOWNObjects1.length === 0 ) ? 0 :gdjs.GamePlaneCode.GDrockSnowDOWNObjects1[0].getHeight()) + gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)), "");
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "rocks");
}}

}


{



}


{


gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
gdjs.GamePlaneCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "letters");
}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
gdjs.GamePlaneCode.GDlethersObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDlethersObjects1Objects, gdjs.evtTools.window.getWindowWidth() - (( gdjs.GamePlaneCode.GDlethersObjects1.length === 0 ) ? 0 :gdjs.GamePlaneCode.GDlethersObjects1[0].getWidth())/2, gdjs.random(350) + 100, "");
}{for(var i = 0, len = gdjs.GamePlaneCode.GDlethersObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDlethersObjects1[i].setAnimation(gdjs.random(25));
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDlethersObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDlethersObjects1[i].returnVariable(gdjs.GamePlaneCode.GDlethersObjects1[i].getVariables().getFromIndex(0)).setNumber((gdjs.GamePlaneCode.GDlethersObjects1[i].getAnimation()));
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "letters");
}}

}


{



}


{

gdjs.GamePlaneCode.GDplaneObjects1.createFrom(runtimeScene.getObjects("plane"));
gdjs.GamePlaneCode.GDrockObjects1.createFrom(runtimeScene.getObjects("rock"));
gdjs.GamePlaneCode.GDrockSnowDOWNObjects1.createFrom(runtimeScene.getObjects("rockSnowDOWN"));
gdjs.GamePlaneCode.GDrockSnowUPObjects1.createFrom(runtimeScene.getObjects("rockSnowUP"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
gdjs.GamePlaneCode.condition1IsTrue_0.val = false;
{
gdjs.GamePlaneCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDplaneObjects1Objects, gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDrockSnowUPObjects1ObjectsGDgdjs_46GamePlaneCode_46GDrockSnowDOWNObjects1ObjectsGDgdjs_46GamePlaneCode_46GDrockObjects1Objects, false, runtimeScene);
}if ( gdjs.GamePlaneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDplaneObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDplaneObjects1[i].getVariableNumber(gdjs.GamePlaneCode.GDplaneObjects1[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GamePlaneCode.condition1IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDplaneObjects1[k] = gdjs.GamePlaneCode.GDplaneObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDplaneObjects1.length = k;}}
if (gdjs.GamePlaneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GamePlaneCode.GDplaneObjects1 */
{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].returnVariable(gdjs.GamePlaneCode.GDplaneObjects1[i].getVariables().getFromIndex(0)).setNumber(1);
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].setAnimation(3);
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].getBehavior("Physics").applyTorque(135, runtimeScene);
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDplaneObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDplaneObjects1[i].getBehavior("Physics").applyImpulse(-0.7, 0.2, runtimeScene);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "qubodupImpactMeat02.ogg", false, 100, 1);
}}

}


{



}


{

gdjs.GamePlaneCode.GDlethersObjects1.createFrom(runtimeScene.getObjects("lethers"));
gdjs.GamePlaneCode.GDplaneObjects1.createFrom(runtimeScene.getObjects("plane"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
gdjs.GamePlaneCode.condition1IsTrue_0.val = false;
gdjs.GamePlaneCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDlethersObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDlethersObjects1[i].getOpacity() == 255 ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDlethersObjects1[k] = gdjs.GamePlaneCode.GDlethersObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDlethersObjects1.length = k;}if ( gdjs.GamePlaneCode.condition0IsTrue_0.val ) {
{
gdjs.GamePlaneCode.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDplaneObjects1Objects, gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDlethersObjects1Objects, false, runtimeScene);
}if ( gdjs.GamePlaneCode.condition1IsTrue_0.val ) {
{
{gdjs.GamePlaneCode.conditionTrue_1 = gdjs.GamePlaneCode.condition2IsTrue_0;
gdjs.GamePlaneCode.conditionTrue_1.val = context.triggerOnce(6105332);
}
}}
}
if (gdjs.GamePlaneCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GamePlaneCode.GDlethersObjects1 */
{for(var i = 0, len = gdjs.GamePlaneCode.GDlethersObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDlethersObjects1[i].setOpacity(254);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise03.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).add(1);
}{runtimeScene.getVariables().getFromIndex(2).add(1);
}
{ //Subevents
gdjs.GamePlaneCode.eventsList0x5d2748(runtimeScene, context);} //End of subevents
}

}


{

gdjs.GamePlaneCode.GDlethersObjects1.createFrom(runtimeScene.getObjects("lethers"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDlethersObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDlethersObjects1[i].getOpacity() < 255 ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDlethersObjects1[k] = gdjs.GamePlaneCode.GDlethersObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDlethersObjects1.length = k;}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GamePlaneCode.GDlethersObjects1 */
{for(var i = 0, len = gdjs.GamePlaneCode.GDlethersObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDlethersObjects1[i].setOpacity(gdjs.GamePlaneCode.GDlethersObjects1[i].getOpacity() - (255*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDlethersObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDlethersObjects1[i].addForce(0, -30, 0);
}
}}

}


{

gdjs.GamePlaneCode.GDlethersObjects1.createFrom(runtimeScene.getObjects("lethers"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDlethersObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDlethersObjects1[i].getOpacity() == 0 ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDlethersObjects1[k] = gdjs.GamePlaneCode.GDlethersObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDlethersObjects1.length = k;}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GamePlaneCode.eventsList0x5d3098(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
gdjs.GamePlaneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) < 10;
}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
gdjs.GamePlaneCode.GDpointsObjects1.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.GamePlaneCode.GDpointsObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDpointsObjects1[i].setString("0" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}}

}


{


gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
gdjs.GamePlaneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) >= 10;
}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
gdjs.GamePlaneCode.GDpointsObjects1.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.GamePlaneCode.GDpointsObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDpointsObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}}

}


{


gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
gdjs.GamePlaneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) > 99;
}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
gdjs.GamePlaneCode.GDpointsObjects1.createFrom(runtimeScene.getObjects("points"));
{for(var i = 0, len = gdjs.GamePlaneCode.GDpointsObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDpointsObjects1[i].setString("99");
}
}}

}


{


gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
gdjs.GamePlaneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 10;
}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).add(-35);
}{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).sub(0.5);
}}

}


{

gdjs.GamePlaneCode.GDgame_95overObjects1.createFrom(runtimeScene.getObjects("game_over"));
gdjs.GamePlaneCode.GDplaneObjects1.createFrom(runtimeScene.getObjects("plane"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
gdjs.GamePlaneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDplaneObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDplaneObjects1[i].getVariableNumber(gdjs.GamePlaneCode.GDplaneObjects1[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDplaneObjects1[k] = gdjs.GamePlaneCode.GDplaneObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDplaneObjects1.length = k;}if ( gdjs.GamePlaneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDgame_95overObjects1.length;i<l;++i) {
    if ( !(gdjs.GamePlaneCode.GDgame_95overObjects1[i].isVisible()) ) {
        gdjs.GamePlaneCode.condition1IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDgame_95overObjects1[k] = gdjs.GamePlaneCode.GDgame_95overObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDgame_95overObjects1.length = k;}}
if (gdjs.GamePlaneCode.condition1IsTrue_0.val) {
gdjs.GamePlaneCode.GDpanelObjects1.createFrom(runtimeScene.getObjects("panel"));
{for(var i = 0, len = gdjs.GamePlaneCode.GDpanelObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDpanelObjects1[i].addForce(0, -60, 15);
}
}}

}


{

gdjs.GamePlaneCode.GDpanelObjects1.createFrom(runtimeScene.getObjects("panel"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
gdjs.GamePlaneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDpanelObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDpanelObjects1[i].getY() <= 200 ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDpanelObjects1[k] = gdjs.GamePlaneCode.GDpanelObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDpanelObjects1.length = k;}if ( gdjs.GamePlaneCode.condition0IsTrue_0.val ) {
{
{gdjs.GamePlaneCode.conditionTrue_1 = gdjs.GamePlaneCode.condition1IsTrue_0;
gdjs.GamePlaneCode.conditionTrue_1.val = context.triggerOnce(6111524);
}
}}
if (gdjs.GamePlaneCode.condition1IsTrue_0.val) {
gdjs.GamePlaneCode.GDbtnStartObjects1.createFrom(runtimeScene.getObjects("btnStart"));
gdjs.GamePlaneCode.GDgame_95overObjects1.createFrom(runtimeScene.getObjects("game_over"));
/* Reuse gdjs.GamePlaneCode.GDpanelObjects1 */
gdjs.GamePlaneCode.GDtxtStartObjects1.createFrom(runtimeScene.getObjects("txtStart"));
{for(var i = 0, len = gdjs.GamePlaneCode.GDpanelObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDpanelObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDpanelObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDpanelObjects1[i].setY(202);
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDgame_95overObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDgame_95overObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDbtnStartObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDbtnStartObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.GamePlaneCode.GDtxtStartObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDtxtStartObjects1[i].hide(false);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise01.ogg", false, 100, 1);
}}

}


{

gdjs.GamePlaneCode.GDbtnStartObjects1.createFrom(runtimeScene.getObjects("btnStart"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
gdjs.GamePlaneCode.condition1IsTrue_0.val = false;
gdjs.GamePlaneCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDbtnStartObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDbtnStartObjects1[i].isVisible() ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDbtnStartObjects1[k] = gdjs.GamePlaneCode.GDbtnStartObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDbtnStartObjects1.length = k;}if ( gdjs.GamePlaneCode.condition0IsTrue_0.val ) {
{
gdjs.GamePlaneCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GamePlaneCode.mapOfGDgdjs_46GamePlaneCode_46GDbtnStartObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GamePlaneCode.condition1IsTrue_0.val ) {
{
gdjs.GamePlaneCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
if (gdjs.GamePlaneCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GamePlane", false);
}}

}


{

gdjs.GamePlaneCode.GDlethersObjects1.createFrom(runtimeScene.getObjects("lethers"));

gdjs.GamePlaneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GamePlaneCode.GDlethersObjects1.length;i<l;++i) {
    if ( gdjs.GamePlaneCode.GDlethersObjects1[i].getX() < -1*(gdjs.GamePlaneCode.GDlethersObjects1[i].getWidth()) ) {
        gdjs.GamePlaneCode.condition0IsTrue_0.val = true;
        gdjs.GamePlaneCode.GDlethersObjects1[k] = gdjs.GamePlaneCode.GDlethersObjects1[i];
        ++k;
    }
}
gdjs.GamePlaneCode.GDlethersObjects1.length = k;}if (gdjs.GamePlaneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GamePlaneCode.GDlethersObjects1 */
{for(var i = 0, len = gdjs.GamePlaneCode.GDlethersObjects1.length ;i < len;++i) {
    gdjs.GamePlaneCode.GDlethersObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.GamePlaneCode.eventsList0xac5d0


gdjs.GamePlaneCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GamePlaneCode.GDplaneObjects1.length = 0;
gdjs.GamePlaneCode.GDplaneObjects2.length = 0;
gdjs.GamePlaneCode.GDpointsObjects1.length = 0;
gdjs.GamePlaneCode.GDpointsObjects2.length = 0;
gdjs.GamePlaneCode.GDbackground1Objects1.length = 0;
gdjs.GamePlaneCode.GDbackground1Objects2.length = 0;
gdjs.GamePlaneCode.GDrockObjects1.length = 0;
gdjs.GamePlaneCode.GDrockObjects2.length = 0;
gdjs.GamePlaneCode.GDrockSnowUPObjects1.length = 0;
gdjs.GamePlaneCode.GDrockSnowUPObjects2.length = 0;
gdjs.GamePlaneCode.GDrockSnowDOWNObjects1.length = 0;
gdjs.GamePlaneCode.GDrockSnowDOWNObjects2.length = 0;
gdjs.GamePlaneCode.GDalfabetoObjects1.length = 0;
gdjs.GamePlaneCode.GDalfabetoObjects2.length = 0;
gdjs.GamePlaneCode.GDlethersObjects1.length = 0;
gdjs.GamePlaneCode.GDlethersObjects2.length = 0;
gdjs.GamePlaneCode.GDpanelObjects1.length = 0;
gdjs.GamePlaneCode.GDpanelObjects2.length = 0;
gdjs.GamePlaneCode.GDgame_95overObjects1.length = 0;
gdjs.GamePlaneCode.GDgame_95overObjects2.length = 0;
gdjs.GamePlaneCode.GDbtnStartObjects1.length = 0;
gdjs.GamePlaneCode.GDbtnStartObjects2.length = 0;
gdjs.GamePlaneCode.GDtxtStartObjects1.length = 0;
gdjs.GamePlaneCode.GDtxtStartObjects2.length = 0;

gdjs.GamePlaneCode.eventsList0xac5d0(runtimeScene, context);return;
}
gdjs['GamePlaneCode']= gdjs.GamePlaneCode;
