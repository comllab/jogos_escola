gdjs.StartCode = {};
gdjs.StartCode.GDplayerObjects2_1final = [];

gdjs.StartCode.GDplayerObjects1= [];
gdjs.StartCode.GDplayerObjects2= [];
gdjs.StartCode.GDplayerObjects3= [];
gdjs.StartCode.GDplayerObjects4= [];
gdjs.StartCode.GDground1Objects1= [];
gdjs.StartCode.GDground1Objects2= [];
gdjs.StartCode.GDground1Objects3= [];
gdjs.StartCode.GDground1Objects4= [];
gdjs.StartCode.GDwall_951Objects1= [];
gdjs.StartCode.GDwall_951Objects2= [];
gdjs.StartCode.GDwall_951Objects3= [];
gdjs.StartCode.GDwall_951Objects4= [];
gdjs.StartCode.GDbtStartObjects1= [];
gdjs.StartCode.GDbtStartObjects2= [];
gdjs.StartCode.GDbtStartObjects3= [];
gdjs.StartCode.GDbtStartObjects4= [];
gdjs.StartCode.GDlblJogarObjects1= [];
gdjs.StartCode.GDlblJogarObjects2= [];
gdjs.StartCode.GDlblJogarObjects3= [];
gdjs.StartCode.GDlblJogarObjects4= [];
gdjs.StartCode.GDtituloObjects1= [];
gdjs.StartCode.GDtituloObjects2= [];
gdjs.StartCode.GDtituloObjects3= [];
gdjs.StartCode.GDtituloObjects4= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};
gdjs.StartCode.condition3IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtStartObjects1Objects = Hashtable.newFrom({"btStart": gdjs.StartCode.GDbtStartObjects1});gdjs.StartCode.eventsList0x756ce0 = function(runtimeScene) {

{

/* Reuse gdjs.StartCode.GDplayerObjects2 */

gdjs.StartCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].setAnimationName("walk");
}
}}

}


}; //End of gdjs.StartCode.eventsList0x756ce0
gdjs.StartCode.eventsList0x757288 = function(runtimeScene) {

{

/* Reuse gdjs.StartCode.GDplayerObjects2 */

gdjs.StartCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].setAnimationName("walk");
}
}}

}


}; //End of gdjs.StartCode.eventsList0x757288
gdjs.StartCode.eventsList0x757cf8 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition0IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7700348);
}
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}}

}


}; //End of gdjs.StartCode.eventsList0x757cf8
gdjs.StartCode.eventsList0x7585a8 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
gdjs.StartCode.GDplayerObjects3.createFrom(gdjs.StartCode.GDplayerObjects2);

{for(var i = 0, len = gdjs.StartCode.GDplayerObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects3[i].getBehavior("PlatformerObject").simulateLadderKey();
}
}{for(var i = 0, len = gdjs.StartCode.GDplayerObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects3[i].getBehavior("PlatformerObject").simulateUpKey();
}
}{for(var i = 0, len = gdjs.StartCode.GDplayerObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects3[i].setAnimationName("climb");
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
gdjs.StartCode.GDplayerObjects3.createFrom(gdjs.StartCode.GDplayerObjects2);

{for(var i = 0, len = gdjs.StartCode.GDplayerObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects3[i].getBehavior("PlatformerObject").simulateLadderKey();
}
}{for(var i = 0, len = gdjs.StartCode.GDplayerObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects3[i].getBehavior("PlatformerObject").simulateDownKey();
}
}{for(var i = 0, len = gdjs.StartCode.GDplayerObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects3[i].setAnimationName("climb");
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = !(gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left"));
}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].setAnimationName("climb_stop");
}
}}

}


}; //End of gdjs.StartCode.eventsList0x7585a8
gdjs.StartCode.eventsList0x756758 = function(runtimeScene) {

{



}


{



}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
gdjs.StartCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].flipX(false);
}
}{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").simulateRightKey();
}
}
{ //Subevents
gdjs.StartCode.eventsList0x756ce0(runtimeScene);} //End of subevents
}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
gdjs.StartCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].flipX(true);
}
}{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").simulateLeftKey();
}
}
{ //Subevents
gdjs.StartCode.eventsList0x757288(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
if ( gdjs.StartCode.condition0IsTrue_0.val ) {
if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
gdjs.StartCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
gdjs.StartCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{

gdjs.StartCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.StartCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].setAnimationName("jump");
}
}
{ //Subevents
gdjs.StartCode.eventsList0x757cf8(runtimeScene);} //End of subevents
}

}


{

gdjs.StartCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( !(gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.StartCode.condition1IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].setAnimationName("idle");
}
}}

}


{



}


{

gdjs.StartCode.GDplayerObjects2.length = 0;


gdjs.StartCode.condition0IsTrue_0.val = false;
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition0IsTrue_0;
gdjs.StartCode.GDplayerObjects2_1final.length = 0;gdjs.StartCode.condition0IsTrue_1.val = false;
gdjs.StartCode.condition1IsTrue_1.val = false;
{
gdjs.StartCode.GDplayerObjects3.createFrom(runtimeScene.getObjects("player"));
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects3.length;i<l;++i) {
    if ( gdjs.StartCode.GDplayerObjects3[i].getBehavior("PlatformerObject").isOnLadder() ) {
        gdjs.StartCode.condition0IsTrue_1.val = true;
        gdjs.StartCode.GDplayerObjects3[k] = gdjs.StartCode.GDplayerObjects3[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects3.length = k;if( gdjs.StartCode.condition0IsTrue_1.val ) {
    gdjs.StartCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.StartCode.GDplayerObjects3.length;j<jLen;++j) {
        if ( gdjs.StartCode.GDplayerObjects2_1final.indexOf(gdjs.StartCode.GDplayerObjects3[j]) === -1 )
            gdjs.StartCode.GDplayerObjects2_1final.push(gdjs.StartCode.GDplayerObjects3[j]);
    }
}
}
{
if( gdjs.StartCode.condition1IsTrue_1.val ) {
    gdjs.StartCode.conditionTrue_1.val = true;
}
}
{
gdjs.StartCode.GDplayerObjects2.createFrom(gdjs.StartCode.GDplayerObjects2_1final);
}
}
}if (gdjs.StartCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.StartCode.eventsList0x7585a8(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.StartCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.StartCode.GDplayerObjects1[0].getPointX("")), "", 0);
}}

}


}; //End of gdjs.StartCode.eventsList0x756758
gdjs.StartCode.eventsList0x759790 = function(runtimeScene) {

{

/* Reuse gdjs.StartCode.GDplayerObjects2 */

gdjs.StartCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].setAnimationName("walk");
}
}}

}


}; //End of gdjs.StartCode.eventsList0x759790
gdjs.StartCode.eventsList0x759b88 = function(runtimeScene) {

{

/* Reuse gdjs.StartCode.GDplayerObjects2 */

gdjs.StartCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].setAnimationName("walk");
}
}}

}


}; //End of gdjs.StartCode.eventsList0x759b88
gdjs.StartCode.eventsList0x724c48 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition0IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7491276);
}
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}}

}


}; //End of gdjs.StartCode.eventsList0x724c48
gdjs.StartCode.eventsList0x7254f8 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition0IsTrue_0;
gdjs.StartCode.condition0IsTrue_1.val = false;
gdjs.StartCode.condition1IsTrue_1.val = false;
{
gdjs.StartCode.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if( gdjs.StartCode.condition0IsTrue_1.val ) {
    gdjs.StartCode.conditionTrue_1.val = true;
}
}
{
gdjs.StartCode.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
if( gdjs.StartCode.condition1IsTrue_1.val ) {
    gdjs.StartCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDplayerObjects3.createFrom(gdjs.StartCode.GDplayerObjects2);

{for(var i = 0, len = gdjs.StartCode.GDplayerObjects3.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects3[i].setAnimationName("climb");
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = !(gdjs.evtTools.input.anyKeyPressed(runtimeScene));
}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].setAnimationName("climb_stop");
}
}}

}


}; //End of gdjs.StartCode.eventsList0x7254f8
gdjs.StartCode.eventsList0x756978 = function(runtimeScene) {

{



}


{



}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].flipX(false);
}
}
{ //Subevents
gdjs.StartCode.eventsList0x759790(runtimeScene);} //End of subevents
}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].flipX(true);
}
}
{ //Subevents
gdjs.StartCode.eventsList0x759b88(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.StartCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( !(gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.StartCode.condition1IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].setAnimationName("idle");
}
}}

}


{



}


{

gdjs.StartCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.StartCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].setAnimationName("jump");
}
}
{ //Subevents
gdjs.StartCode.eventsList0x724c48(runtimeScene);} //End of subevents
}

}


{

gdjs.StartCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( !(gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnLadder()) ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( !(gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor()) ) {
        gdjs.StartCode.condition1IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDplayerObjects2 */
{for(var i = 0, len = gdjs.StartCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.StartCode.GDplayerObjects2[i].setAnimationName("jump");
}
}}

}


{



}


{

gdjs.StartCode.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.StartCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.StartCode.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnLadder() ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDplayerObjects2[k] = gdjs.StartCode.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.StartCode.GDplayerObjects2.length = k;}if (gdjs.StartCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.StartCode.eventsList0x7254f8(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.StartCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.StartCode.GDplayerObjects1[0].getPointX("")), "", 0);
}}

}


}; //End of gdjs.StartCode.eventsList0x756978
gdjs.StartCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "music.ogg", true, 100, 1);
}}

}


{

gdjs.StartCode.GDbtStartObjects1.createFrom(runtimeScene.getObjects("btStart"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtStartObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7693780);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if (gdjs.StartCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.StartCode.eventsList0x756758(runtimeScene);} //End of subevents
}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = !(gdjs.evtTools.systemInfo.isMobile());
}if (gdjs.StartCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.StartCode.eventsList0x756978(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.StartCode.eventsList0xaff48


gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.StartCode.GDplayerObjects1.length = 0;
gdjs.StartCode.GDplayerObjects2.length = 0;
gdjs.StartCode.GDplayerObjects3.length = 0;
gdjs.StartCode.GDplayerObjects4.length = 0;
gdjs.StartCode.GDground1Objects1.length = 0;
gdjs.StartCode.GDground1Objects2.length = 0;
gdjs.StartCode.GDground1Objects3.length = 0;
gdjs.StartCode.GDground1Objects4.length = 0;
gdjs.StartCode.GDwall_951Objects1.length = 0;
gdjs.StartCode.GDwall_951Objects2.length = 0;
gdjs.StartCode.GDwall_951Objects3.length = 0;
gdjs.StartCode.GDwall_951Objects4.length = 0;
gdjs.StartCode.GDbtStartObjects1.length = 0;
gdjs.StartCode.GDbtStartObjects2.length = 0;
gdjs.StartCode.GDbtStartObjects3.length = 0;
gdjs.StartCode.GDbtStartObjects4.length = 0;
gdjs.StartCode.GDlblJogarObjects1.length = 0;
gdjs.StartCode.GDlblJogarObjects2.length = 0;
gdjs.StartCode.GDlblJogarObjects3.length = 0;
gdjs.StartCode.GDlblJogarObjects4.length = 0;
gdjs.StartCode.GDtituloObjects1.length = 0;
gdjs.StartCode.GDtituloObjects2.length = 0;
gdjs.StartCode.GDtituloObjects3.length = 0;
gdjs.StartCode.GDtituloObjects4.length = 0;

gdjs.StartCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['StartCode'] = gdjs.StartCode;
