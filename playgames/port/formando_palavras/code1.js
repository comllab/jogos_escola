gdjs.GameCode = {};


gdjs.GameCode.GDplayerObjects1= [];
gdjs.GameCode.GDplayerObjects2= [];
gdjs.GameCode.GDplayerObjects3= [];
gdjs.GameCode.GDplayerObjects4= [];
gdjs.GameCode.GDgramaObjects1= [];
gdjs.GameCode.GDgramaObjects2= [];
gdjs.GameCode.GDgramaObjects3= [];
gdjs.GameCode.GDgramaObjects4= [];
gdjs.GameCode.GDletraObjects1= [];
gdjs.GameCode.GDletraObjects2= [];
gdjs.GameCode.GDletraObjects3= [];
gdjs.GameCode.GDletraObjects4= [];
gdjs.GameCode.GDparedesObjects1= [];
gdjs.GameCode.GDparedesObjects2= [];
gdjs.GameCode.GDparedesObjects3= [];
gdjs.GameCode.GDparedesObjects4= [];
gdjs.GameCode.GDcarrinho_95topoObjects1= [];
gdjs.GameCode.GDcarrinho_95topoObjects2= [];
gdjs.GameCode.GDcarrinho_95topoObjects3= [];
gdjs.GameCode.GDcarrinho_95topoObjects4= [];
gdjs.GameCode.GDcarrinho_95direitaObjects1= [];
gdjs.GameCode.GDcarrinho_95direitaObjects2= [];
gdjs.GameCode.GDcarrinho_95direitaObjects3= [];
gdjs.GameCode.GDcarrinho_95direitaObjects4= [];
gdjs.GameCode.GDcarrinho_95esquerdaObjects1= [];
gdjs.GameCode.GDcarrinho_95esquerdaObjects2= [];
gdjs.GameCode.GDcarrinho_95esquerdaObjects3= [];
gdjs.GameCode.GDcarrinho_95esquerdaObjects4= [];
gdjs.GameCode.GDimagem_95palavraObjects1= [];
gdjs.GameCode.GDimagem_95palavraObjects2= [];
gdjs.GameCode.GDimagem_95palavraObjects3= [];
gdjs.GameCode.GDimagem_95palavraObjects4= [];
gdjs.GameCode.GDletra1_95palavraObjects1= [];
gdjs.GameCode.GDletra1_95palavraObjects2= [];
gdjs.GameCode.GDletra1_95palavraObjects3= [];
gdjs.GameCode.GDletra1_95palavraObjects4= [];
gdjs.GameCode.GDletra2_95palavraObjects1= [];
gdjs.GameCode.GDletra2_95palavraObjects2= [];
gdjs.GameCode.GDletra2_95palavraObjects3= [];
gdjs.GameCode.GDletra2_95palavraObjects4= [];
gdjs.GameCode.GDletra3_95palavraObjects1= [];
gdjs.GameCode.GDletra3_95palavraObjects2= [];
gdjs.GameCode.GDletra3_95palavraObjects3= [];
gdjs.GameCode.GDletra3_95palavraObjects4= [];
gdjs.GameCode.GDletra4_95palavraObjects1= [];
gdjs.GameCode.GDletra4_95palavraObjects2= [];
gdjs.GameCode.GDletra4_95palavraObjects3= [];
gdjs.GameCode.GDletra4_95palavraObjects4= [];
gdjs.GameCode.GDletra5_95palavraObjects1= [];
gdjs.GameCode.GDletra5_95palavraObjects2= [];
gdjs.GameCode.GDletra5_95palavraObjects3= [];
gdjs.GameCode.GDletra5_95palavraObjects4= [];
gdjs.GameCode.GDletra6_95palavraObjects1= [];
gdjs.GameCode.GDletra6_95palavraObjects2= [];
gdjs.GameCode.GDletra6_95palavraObjects3= [];
gdjs.GameCode.GDletra6_95palavraObjects4= [];
gdjs.GameCode.GDletra7_95palavraObjects1= [];
gdjs.GameCode.GDletra7_95palavraObjects2= [];
gdjs.GameCode.GDletra7_95palavraObjects3= [];
gdjs.GameCode.GDletra7_95palavraObjects4= [];
gdjs.GameCode.GDletra8_95palavraObjects1= [];
gdjs.GameCode.GDletra8_95palavraObjects2= [];
gdjs.GameCode.GDletra8_95palavraObjects3= [];
gdjs.GameCode.GDletra8_95palavraObjects4= [];
gdjs.GameCode.GDletra9_95palavraObjects1= [];
gdjs.GameCode.GDletra9_95palavraObjects2= [];
gdjs.GameCode.GDletra9_95palavraObjects3= [];
gdjs.GameCode.GDletra9_95palavraObjects4= [];
gdjs.GameCode.GDnuvemObjects1= [];
gdjs.GameCode.GDnuvemObjects2= [];
gdjs.GameCode.GDnuvemObjects3= [];
gdjs.GameCode.GDnuvemObjects4= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.condition4IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};
gdjs.GameCode.condition4IsTrue_1 = {val:false};

gdjs.GameCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GameCode.GDplayerObjects1.length = 0;
gdjs.GameCode.GDplayerObjects2.length = 0;
gdjs.GameCode.GDplayerObjects3.length = 0;
gdjs.GameCode.GDplayerObjects4.length = 0;
gdjs.GameCode.GDgramaObjects1.length = 0;
gdjs.GameCode.GDgramaObjects2.length = 0;
gdjs.GameCode.GDgramaObjects3.length = 0;
gdjs.GameCode.GDgramaObjects4.length = 0;
gdjs.GameCode.GDletraObjects1.length = 0;
gdjs.GameCode.GDletraObjects2.length = 0;
gdjs.GameCode.GDletraObjects3.length = 0;
gdjs.GameCode.GDletraObjects4.length = 0;
gdjs.GameCode.GDparedesObjects1.length = 0;
gdjs.GameCode.GDparedesObjects2.length = 0;
gdjs.GameCode.GDparedesObjects3.length = 0;
gdjs.GameCode.GDparedesObjects4.length = 0;
gdjs.GameCode.GDcarrinho_95topoObjects1.length = 0;
gdjs.GameCode.GDcarrinho_95topoObjects2.length = 0;
gdjs.GameCode.GDcarrinho_95topoObjects3.length = 0;
gdjs.GameCode.GDcarrinho_95topoObjects4.length = 0;
gdjs.GameCode.GDcarrinho_95direitaObjects1.length = 0;
gdjs.GameCode.GDcarrinho_95direitaObjects2.length = 0;
gdjs.GameCode.GDcarrinho_95direitaObjects3.length = 0;
gdjs.GameCode.GDcarrinho_95direitaObjects4.length = 0;
gdjs.GameCode.GDcarrinho_95esquerdaObjects1.length = 0;
gdjs.GameCode.GDcarrinho_95esquerdaObjects2.length = 0;
gdjs.GameCode.GDcarrinho_95esquerdaObjects3.length = 0;
gdjs.GameCode.GDcarrinho_95esquerdaObjects4.length = 0;
gdjs.GameCode.GDimagem_95palavraObjects1.length = 0;
gdjs.GameCode.GDimagem_95palavraObjects2.length = 0;
gdjs.GameCode.GDimagem_95palavraObjects3.length = 0;
gdjs.GameCode.GDimagem_95palavraObjects4.length = 0;
gdjs.GameCode.GDletra1_95palavraObjects1.length = 0;
gdjs.GameCode.GDletra1_95palavraObjects2.length = 0;
gdjs.GameCode.GDletra1_95palavraObjects3.length = 0;
gdjs.GameCode.GDletra1_95palavraObjects4.length = 0;
gdjs.GameCode.GDletra2_95palavraObjects1.length = 0;
gdjs.GameCode.GDletra2_95palavraObjects2.length = 0;
gdjs.GameCode.GDletra2_95palavraObjects3.length = 0;
gdjs.GameCode.GDletra2_95palavraObjects4.length = 0;
gdjs.GameCode.GDletra3_95palavraObjects1.length = 0;
gdjs.GameCode.GDletra3_95palavraObjects2.length = 0;
gdjs.GameCode.GDletra3_95palavraObjects3.length = 0;
gdjs.GameCode.GDletra3_95palavraObjects4.length = 0;
gdjs.GameCode.GDletra4_95palavraObjects1.length = 0;
gdjs.GameCode.GDletra4_95palavraObjects2.length = 0;
gdjs.GameCode.GDletra4_95palavraObjects3.length = 0;
gdjs.GameCode.GDletra4_95palavraObjects4.length = 0;
gdjs.GameCode.GDletra5_95palavraObjects1.length = 0;
gdjs.GameCode.GDletra5_95palavraObjects2.length = 0;
gdjs.GameCode.GDletra5_95palavraObjects3.length = 0;
gdjs.GameCode.GDletra5_95palavraObjects4.length = 0;
gdjs.GameCode.GDletra6_95palavraObjects1.length = 0;
gdjs.GameCode.GDletra6_95palavraObjects2.length = 0;
gdjs.GameCode.GDletra6_95palavraObjects3.length = 0;
gdjs.GameCode.GDletra6_95palavraObjects4.length = 0;
gdjs.GameCode.GDletra7_95palavraObjects1.length = 0;
gdjs.GameCode.GDletra7_95palavraObjects2.length = 0;
gdjs.GameCode.GDletra7_95palavraObjects3.length = 0;
gdjs.GameCode.GDletra7_95palavraObjects4.length = 0;
gdjs.GameCode.GDletra8_95palavraObjects1.length = 0;
gdjs.GameCode.GDletra8_95palavraObjects2.length = 0;
gdjs.GameCode.GDletra8_95palavraObjects3.length = 0;
gdjs.GameCode.GDletra8_95palavraObjects4.length = 0;
gdjs.GameCode.GDletra9_95palavraObjects1.length = 0;
gdjs.GameCode.GDletra9_95palavraObjects2.length = 0;
gdjs.GameCode.GDletra9_95palavraObjects3.length = 0;
gdjs.GameCode.GDletra9_95palavraObjects4.length = 0;
gdjs.GameCode.GDnuvemObjects1.length = 0;
gdjs.GameCode.GDnuvemObjects2.length = 0;
gdjs.GameCode.GDnuvemObjects3.length = 0;
gdjs.GameCode.GDnuvemObjects4.length = 0;


{



}


{

gdjs.GameCode.GDcarrinho_95direitaObjects1.createFrom(runtimeScene.getObjects("carrinho_direita"));
gdjs.GameCode.GDcarrinho_95esquerdaObjects1.createFrom(runtimeScene.getObjects("carrinho_esquerda"));
gdjs.GameCode.GDcarrinho_95topoObjects1.createFrom(runtimeScene.getObjects("carrinho_topo"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDcarrinho_95topoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcarrinho_95topoObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDcarrinho_95direitaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcarrinho_95direitaObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDcarrinho_95esquerdaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcarrinho_95esquerdaObjects1[i].hide();
}
}}

}


{

gdjs.GameCode.GDcarrinho_95direitaObjects1.createFrom(runtimeScene.getObjects("carrinho_direita"));
gdjs.GameCode.GDcarrinho_95esquerdaObjects1.createFrom(runtimeScene.getObjects("carrinho_esquerda"));
gdjs.GameCode.GDcarrinho_95topoObjects1.createFrom(runtimeScene.getObjects("carrinho_topo"));
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDcarrinho_95topoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcarrinho_95topoObjects1[i].setPosition((( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointX("")) + (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getWidth())/2 - (gdjs.GameCode.GDcarrinho_95topoObjects1[i].getWidth())/2,(( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.GameCode.GDcarrinho_95esquerdaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcarrinho_95esquerdaObjects1[i].setPosition((( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointX("")) - (gdjs.GameCode.GDcarrinho_95esquerdaObjects1[i].getWidth())/2,(( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.GameCode.GDcarrinho_95direitaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcarrinho_95direitaObjects1[i].setPosition((( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointX("")) + (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getWidth()) - (gdjs.GameCode.GDcarrinho_95direitaObjects1[i].getWidth())/2,(( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointY("")));
}
}}

}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].setAnimation(2);
}
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].flipX(false);
}
}}

}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].setAnimation(2);
}
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].flipX(true);
}
}}

}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.GameCode.GDplayerObjects1[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].setAnimation(0);
}
}}

}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].setAnimation(1);
}
}}

}


{



}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.GameCode.GDplayerObjects1[i].getBehavior("PlatformerObject").isJumping()) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(261247268);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "motor_car.ogg", 32, false, 100, 1);
}}

}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.GameCode.GDplayerObjects1[i].getBehavior("PlatformerObject").isJumping()) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(261247124);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "motor_car.ogg", 32, false, 100, 1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "Right");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 32);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "Left");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 32);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(261248636);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 32);
}{gdjs.evtTools.sound.playSound(runtimeScene, "jump_car.ogg", false, 100, 1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "LShift");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(261248852);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 32);
}{gdjs.evtTools.sound.playSound(runtimeScene, "jump_car.ogg", false, 100, 1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "RShift");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(261249068);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 32);
}{gdjs.evtTools.sound.playSound(runtimeScene, "jump_car.ogg", false, 100, 1);
}}

}


{



}


{



{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(11));
}
{ //Subevents

{



{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "B");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra4_95palavraObjects4[i].getVariables().getFromIndex(0), "C");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra5_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra6_95palavraObjects4[i].getVariables().getFromIndex(0), "X");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra7_95palavraObjects4[i].getVariables().getFromIndex(0), "I");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(7);
}}

}


{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "B");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "N");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra4_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra5_95palavraObjects4[i].getVariables().getFromIndex(0), "N");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra6_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(6);
}}

}


{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 2;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(2);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "C");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "E");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "R");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra4_95palavraObjects4[i].getVariables().getFromIndex(0), "E");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra5_95palavraObjects4[i].getVariables().getFromIndex(0), "J");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra6_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(6);
}}

}


{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 3;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(3);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "L");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "R");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra4_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra5_95palavraObjects4[i].getVariables().getFromIndex(0), "N");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra6_95palavraObjects4[i].getVariables().getFromIndex(0), "J");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra7_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(7);
}}

}


{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 4;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(4);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "M");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "O");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "R");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra4_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra5_95palavraObjects4[i].getVariables().getFromIndex(0), "N");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra6_95palavraObjects4[i].getVariables().getFromIndex(0), "G");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra7_95palavraObjects4[i].getVariables().getFromIndex(0), "O");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(7);
}}

}


{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 5;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(5);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "U");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "V");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(3);
}}

}


{



}


}


{



{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 6;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(6);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "C");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "C");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra4_95palavraObjects4[i].getVariables().getFromIndex(0), "H");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra5_95palavraObjects4[i].getVariables().getFromIndex(0), "O");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra6_95palavraObjects4[i].getVariables().getFromIndex(0), "R");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra7_95palavraObjects4[i].getVariables().getFromIndex(0), "R");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra8_95palavraObjects4[i].getVariables().getFromIndex(0), "O");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(8);
}}

}


{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 7;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(7);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "E");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "L");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "E");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra4_95palavraObjects4[i].getVariables().getFromIndex(0), "F");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra5_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra6_95palavraObjects4[i].getVariables().getFromIndex(0), "N");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra7_95palavraObjects4[i].getVariables().getFromIndex(0), "T");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra8_95palavraObjects4[i].getVariables().getFromIndex(0), "E");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(8);
}}

}


{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 8;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(8);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "G");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "I");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "R");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra4_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra5_95palavraObjects4[i].getVariables().getFromIndex(0), "F");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra6_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(6);
}}

}


{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 9;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(9);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "C");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "V");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra4_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra5_95palavraObjects4[i].getVariables().getFromIndex(0), "L");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra6_95palavraObjects4[i].getVariables().getFromIndex(0), "O");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(6);
}}

}


{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(10);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "G");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "T");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra4_95palavraObjects4[i].getVariables().getFromIndex(0), "O");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(4);
}}

}


{



}


{

gdjs.GameCode.GDimagem_95palavraObjects4.createFrom(runtimeScene.getObjects("imagem_palavra"));
gdjs.GameCode.GDletra1_95palavraObjects4.createFrom(runtimeScene.getObjects("letra1_palavra"));
gdjs.GameCode.GDletra2_95palavraObjects4.createFrom(runtimeScene.getObjects("letra2_palavra"));
gdjs.GameCode.GDletra3_95palavraObjects4.createFrom(runtimeScene.getObjects("letra3_palavra"));
gdjs.GameCode.GDletra4_95palavraObjects4.createFrom(runtimeScene.getObjects("letra4_palavra"));
gdjs.GameCode.GDletra5_95palavraObjects4.createFrom(runtimeScene.getObjects("letra5_palavra"));
gdjs.GameCode.GDletra6_95palavraObjects4.createFrom(runtimeScene.getObjects("letra6_palavra"));
gdjs.GameCode.GDletra7_95palavraObjects4.createFrom(runtimeScene.getObjects("letra7_palavra"));
gdjs.GameCode.GDletra8_95palavraObjects4.createFrom(runtimeScene.getObjects("letra8_palavra"));
gdjs.GameCode.GDletra9_95palavraObjects4.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 11;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDimagem_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDimagem_95palavraObjects4[i].setAnimation(11);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra1_95palavraObjects4[i].getVariables().getFromIndex(0), "M");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra2_95palavraObjects4[i].getVariables().getFromIndex(0), "I");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra3_95palavraObjects4[i].getVariables().getFromIndex(0), "N");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra4_95palavraObjects4[i].getVariables().getFromIndex(0), "H");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra5_95palavraObjects4[i].getVariables().getFromIndex(0), "O");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra6_95palavraObjects4[i].getVariables().getFromIndex(0), "C");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects4[i].setVariableString(gdjs.GameCode.GDletra7_95palavraObjects4[i].getVariables().getFromIndex(0), "A");
}
}{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects4.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects4[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(7);
}}

}


}

} //End of subevents
}

}


}


{



{

gdjs.GameCode.GDletraObjects2.length = 0;

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "gera_letras");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.object.createObjectOnScene(runtimeScene, context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), gdjs.random(550) + 30, -50, "letras");
}{runtimeScene.getVariables().getFromIndex(1).add(1);
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "gera_letras");
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].setScale(0.9);
}
}
{ //Subevents

{

gdjs.GameCode.GDletraObjects3.createFrom(gdjs.GameCode.GDletraObjects2);

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletraObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects3[i].setAnimation(gdjs.random(9));
}
}}

}


{

gdjs.GameCode.GDletraObjects3.createFrom(gdjs.GameCode.GDletraObjects2);

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 2;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletraObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects3[i].setAnimation(gdjs.random(9) + 9);
}
}}

}


{

gdjs.GameCode.GDletraObjects3.createFrom(gdjs.GameCode.GDletraObjects2);

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 3;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletraObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects3[i].setAnimation(gdjs.random(9) + 16);
}
}{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}

} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].addForce(0, gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)), 0);
}
}}

}


{



}


{

gdjs.GameCode.GDcarrinho_95topoObjects2.createFrom(runtimeScene.getObjects("carrinho_topo"));
gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("carrinho_topo", gdjs.GameCode.GDcarrinho_95topoObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(261651900);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "entrega2.ogg", 30, false, 50, 1);
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].setVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(2), 1);
}
}}

}


{

gdjs.GameCode.GDcarrinho_95topoObjects2.createFrom(runtimeScene.getObjects("carrinho_topo"));
gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("carrinho_topo", gdjs.GameCode.GDcarrinho_95topoObjects2).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].clearForces();
}
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].setPosition((( gdjs.GameCode.GDcarrinho_95topoObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDcarrinho_95topoObjects2[0].getPointX("")) + (( gdjs.GameCode.GDcarrinho_95topoObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDcarrinho_95topoObjects2[0].getWidth())/2 - (gdjs.GameCode.GDletraObjects2[i].getWidth())/2,(( gdjs.GameCode.GDcarrinho_95topoObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDcarrinho_95topoObjects2[0].getPointY("")) - (gdjs.GameCode.GDletraObjects2[i].getHeight()));
}
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].setVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0), 1);
}
}}

}


{



}


{

gdjs.GameCode.GDcarrinho_95topoObjects2.createFrom(runtimeScene.getObjects("carrinho_topo"));
gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("carrinho_topo", gdjs.GameCode.GDcarrinho_95topoObjects2).getEventsObjectsMap(), true, runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].setVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0), 0);
}
}}

}


{



}


{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(1)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].setVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(1), 1);
}
}
{ //Subevents

{

gdjs.GameCode.GDletraObjects3.createFrom(gdjs.GameCode.GDletraObjects2);

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects3.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects3[i].getVariableNumber(gdjs.GameCode.GDletraObjects3[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects3[k] = gdjs.GameCode.GDletraObjects3[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects3.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletraObjects3.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects3[i].addPolarForce(gdjs.random(90) + 45, -120, 1);
}
}}

}

} //End of subevents
}

}


{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(261654492);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "letra_car.ogg", false, 40, 1);
}}

}


{



}


{

gdjs.GameCode.GDcarrinho_95direitaObjects2.createFrom(runtimeScene.getObjects("carrinho_direita"));
gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("carrinho_direita", gdjs.GameCode.GDcarrinho_95direitaObjects2).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].addForce(120, -45, 1);
}
}}

}


{

gdjs.GameCode.GDcarrinho_95direitaObjects2.createFrom(runtimeScene.getObjects("carrinho_direita"));
gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("carrinho_direita", gdjs.GameCode.GDcarrinho_95direitaObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(261654132);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "letra_car.ogg", false, 100, 1);
}}

}


{

gdjs.GameCode.GDcarrinho_95esquerdaObjects2.createFrom(runtimeScene.getObjects("carrinho_esquerda"));
gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("carrinho_esquerda", gdjs.GameCode.GDcarrinho_95esquerdaObjects2).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].addForce(-120, -45, 1);
}
}}

}


{

gdjs.GameCode.GDcarrinho_95esquerdaObjects2.createFrom(runtimeScene.getObjects("carrinho_esquerda"));
gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("carrinho_esquerda", gdjs.GameCode.GDcarrinho_95esquerdaObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(261655500);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "letra_car.ogg", false, 100, 1);
}}

}


}


{



{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));
gdjs.GameCode.GDletra1_95palavraObjects2.createFrom(runtimeScene.getObjects("letra1_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletra1_95palavraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletra1_95palavraObjects2[i].isVisible() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDletra1_95palavraObjects2[k] = gdjs.GameCode.GDletra1_95palavraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletra1_95palavraObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("letra1_palavra", gdjs.GameCode.GDletra1_95palavraObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].isCurrentAnimationName((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDletra1_95palavraObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDletra1_95palavraObjects2[0].getVariables()).getFromIndex(0)))) ) {
        gdjs.GameCode.condition3IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletra1_95palavraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletra1_95palavraObjects2[i].setAnimationName((( gdjs.GameCode.GDletraObjects2.length === 0 ) ? "" :gdjs.GameCode.GDletraObjects2[0].getAnimationName()));
}
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).sub(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "entrega2.ogg", false, 100, 1);
}}

}


{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));
gdjs.GameCode.GDletra2_95palavraObjects2.createFrom(runtimeScene.getObjects("letra2_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletra2_95palavraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletra2_95palavraObjects2[i].isVisible() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDletra2_95palavraObjects2[k] = gdjs.GameCode.GDletra2_95palavraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletra2_95palavraObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("letra2_palavra", gdjs.GameCode.GDletra2_95palavraObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].isCurrentAnimationName((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDletra2_95palavraObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDletra2_95palavraObjects2[0].getVariables()).getFromIndex(0)))) ) {
        gdjs.GameCode.condition3IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletra2_95palavraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletra2_95palavraObjects2[i].setAnimationName((( gdjs.GameCode.GDletraObjects2.length === 0 ) ? "" :gdjs.GameCode.GDletraObjects2[0].getAnimationName()));
}
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).sub(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "entrega2.ogg", false, 100, 1);
}}

}


{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));
gdjs.GameCode.GDletra3_95palavraObjects2.createFrom(runtimeScene.getObjects("letra3_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletra3_95palavraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletra3_95palavraObjects2[i].isVisible() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDletra3_95palavraObjects2[k] = gdjs.GameCode.GDletra3_95palavraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletra3_95palavraObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("letra3_palavra", gdjs.GameCode.GDletra3_95palavraObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].isCurrentAnimationName((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDletra3_95palavraObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDletra3_95palavraObjects2[0].getVariables()).getFromIndex(0)))) ) {
        gdjs.GameCode.condition3IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletra3_95palavraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletra3_95palavraObjects2[i].setAnimationName((( gdjs.GameCode.GDletraObjects2.length === 0 ) ? "" :gdjs.GameCode.GDletraObjects2[0].getAnimationName()));
}
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].deleteFromScene(runtimeScene);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "entrega2.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).sub(1);
}}

}


{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));
gdjs.GameCode.GDletra4_95palavraObjects2.createFrom(runtimeScene.getObjects("letra4_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletra4_95palavraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletra4_95palavraObjects2[i].isVisible() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDletra4_95palavraObjects2[k] = gdjs.GameCode.GDletra4_95palavraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletra4_95palavraObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("letra4_palavra", gdjs.GameCode.GDletra4_95palavraObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].isCurrentAnimationName((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDletra4_95palavraObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDletra4_95palavraObjects2[0].getVariables()).getFromIndex(0)))) ) {
        gdjs.GameCode.condition3IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).sub(1);
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.GameCode.GDletra4_95palavraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletra4_95palavraObjects2[i].setAnimationName((( gdjs.GameCode.GDletraObjects2.length === 0 ) ? "" :gdjs.GameCode.GDletraObjects2[0].getAnimationName()));
}
}}

}


{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));
gdjs.GameCode.GDletra5_95palavraObjects2.createFrom(runtimeScene.getObjects("letra5_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletra5_95palavraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletra5_95palavraObjects2[i].isVisible() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDletra5_95palavraObjects2[k] = gdjs.GameCode.GDletra5_95palavraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletra5_95palavraObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("letra5_palavra", gdjs.GameCode.GDletra5_95palavraObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].isCurrentAnimationName((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDletra5_95palavraObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDletra5_95palavraObjects2[0].getVariables()).getFromIndex(0)))) ) {
        gdjs.GameCode.condition3IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletra5_95palavraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletra5_95palavraObjects2[i].setAnimationName((( gdjs.GameCode.GDletraObjects2.length === 0 ) ? "" :gdjs.GameCode.GDletraObjects2[0].getAnimationName()));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).sub(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "entrega2.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));
gdjs.GameCode.GDletra6_95palavraObjects2.createFrom(runtimeScene.getObjects("letra6_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletra6_95palavraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletra6_95palavraObjects2[i].isVisible() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDletra6_95palavraObjects2[k] = gdjs.GameCode.GDletra6_95palavraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletra6_95palavraObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("letra6_palavra", gdjs.GameCode.GDletra6_95palavraObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].isCurrentAnimationName((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDletra6_95palavraObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDletra6_95palavraObjects2[0].getVariables()).getFromIndex(0)))) ) {
        gdjs.GameCode.condition3IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletra6_95palavraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletra6_95palavraObjects2[i].setAnimationName((( gdjs.GameCode.GDletraObjects2.length === 0 ) ? "" :gdjs.GameCode.GDletraObjects2[0].getAnimationName()));
}
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].deleteFromScene(runtimeScene);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "entrega2.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).sub(1);
}}

}


{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));
gdjs.GameCode.GDletra7_95palavraObjects2.createFrom(runtimeScene.getObjects("letra7_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletra7_95palavraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletra7_95palavraObjects2[i].isVisible() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDletra7_95palavraObjects2[k] = gdjs.GameCode.GDletra7_95palavraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletra7_95palavraObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("letra7_palavra", gdjs.GameCode.GDletra7_95palavraObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].isCurrentAnimationName((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDletra7_95palavraObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDletra7_95palavraObjects2[0].getVariables()).getFromIndex(0)))) ) {
        gdjs.GameCode.condition3IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletra7_95palavraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletra7_95palavraObjects2[i].setAnimationName((( gdjs.GameCode.GDletraObjects2.length === 0 ) ? "" :gdjs.GameCode.GDletraObjects2[0].getAnimationName()));
}
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].deleteFromScene(runtimeScene);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "entrega2.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).sub(1);
}}

}


{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));
gdjs.GameCode.GDletra8_95palavraObjects2.createFrom(runtimeScene.getObjects("letra8_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletra8_95palavraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletra8_95palavraObjects2[i].isVisible() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDletra8_95palavraObjects2[k] = gdjs.GameCode.GDletra8_95palavraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletra8_95palavraObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("letra8_palavra", gdjs.GameCode.GDletra8_95palavraObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].isCurrentAnimationName((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDletra8_95palavraObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDletra8_95palavraObjects2[0].getVariables()).getFromIndex(0)))) ) {
        gdjs.GameCode.condition3IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletra8_95palavraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletra8_95palavraObjects2[i].setAnimationName((( gdjs.GameCode.GDletraObjects2.length === 0 ) ? "" :gdjs.GameCode.GDletraObjects2[0].getAnimationName()));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).sub(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "entrega2.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.GameCode.GDletraObjects2.createFrom(runtimeScene.getObjects("letra"));
gdjs.GameCode.GDletra9_95palavraObjects2.createFrom(runtimeScene.getObjects("letra9_palavra"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletra9_95palavraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletra9_95palavraObjects2[i].isVisible() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDletra9_95palavraObjects2[k] = gdjs.GameCode.GDletra9_95palavraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletra9_95palavraObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].getVariableNumber(gdjs.GameCode.GDletraObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("letra", gdjs.GameCode.GDletraObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("letra9_palavra", gdjs.GameCode.GDletra9_95palavraObjects2).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDletraObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDletraObjects2[i].isCurrentAnimationName((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDletra9_95palavraObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDletra9_95palavraObjects2[0].getVariables()).getFromIndex(0)))) ) {
        gdjs.GameCode.condition3IsTrue_0.val = true;
        gdjs.GameCode.GDletraObjects2[k] = gdjs.GameCode.GDletraObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDletraObjects2.length = k;}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameCode.GDletra9_95palavraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletra9_95palavraObjects2[i].setAnimationName((( gdjs.GameCode.GDletraObjects2.length === 0 ) ? "" :gdjs.GameCode.GDletraObjects2[0].getAnimationName()));
}
}{for(var i = 0, len = gdjs.GameCode.GDletraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDletraObjects2[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).sub(1);
}}

}


}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "End", false);
}}

}

return;
}
gdjs['GameCode']= gdjs.GameCode;
