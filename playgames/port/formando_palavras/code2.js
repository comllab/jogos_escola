gdjs.EndCode = {};


gdjs.EndCode.GDgramaObjects1= [];
gdjs.EndCode.GDletraObjects1= [];
gdjs.EndCode.GDparedesObjects1= [];
gdjs.EndCode.GDimagem_95palavraObjects1= [];
gdjs.EndCode.GDparabensObjects1= [];
gdjs.EndCode.GDjogar_95denovoObjects1= [];
gdjs.EndCode.GDtxt_95jogar_95denovoObjects1= [];

gdjs.EndCode.conditionTrue_0 = {val:false};
gdjs.EndCode.condition0IsTrue_0 = {val:false};
gdjs.EndCode.condition1IsTrue_0 = {val:false};
gdjs.EndCode.condition2IsTrue_0 = {val:false};

gdjs.EndCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.EndCode.GDgramaObjects1.length = 0;
gdjs.EndCode.GDletraObjects1.length = 0;
gdjs.EndCode.GDparedesObjects1.length = 0;
gdjs.EndCode.GDimagem_95palavraObjects1.length = 0;
gdjs.EndCode.GDparabensObjects1.length = 0;
gdjs.EndCode.GDjogar_95denovoObjects1.length = 0;
gdjs.EndCode.GDtxt_95jogar_95denovoObjects1.length = 0;


{

gdjs.EndCode.GDimagem_95palavraObjects1.createFrom(runtimeScene.getObjects("imagem_palavra"));

gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.EndCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.EndCode.GDimagem_95palavraObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDimagem_95palavraObjects1[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "applause.ogg", false, 100, 1);
}}

}


{

gdjs.EndCode.GDjogar_95denovoObjects1.createFrom(runtimeScene.getObjects("jogar_denovo"));

gdjs.EndCode.condition0IsTrue_0.val = false;
gdjs.EndCode.condition1IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("jogar_denovo", gdjs.EndCode.GDjogar_95denovoObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if ( gdjs.EndCode.condition0IsTrue_0.val ) {
{
gdjs.EndCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.EndCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{

gdjs.EndCode.GDparabensObjects1.createFrom(runtimeScene.getObjects("parabens"));

{for(var i = 0, len = gdjs.EndCode.GDparabensObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDparabensObjects1[i].addForce(0, -75, 0);
}
}
}

return;
}
gdjs['EndCode']= gdjs.EndCode;
