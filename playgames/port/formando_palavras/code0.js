gdjs.StartCode = {};


gdjs.StartCode.GDNewObjectObjects1= [];
gdjs.StartCode.GDjogarObjects1= [];
gdjs.StartCode.GDtxt_95jogar_95denovoObjects1= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};

gdjs.StartCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.StartCode.GDNewObjectObjects1.length = 0;
gdjs.StartCode.GDjogarObjects1.length = 0;
gdjs.StartCode.GDtxt_95jogar_95denovoObjects1.length = 0;


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Grasslands Theme.ogg", 0, true, 100, 1);
}}

}


{

gdjs.StartCode.GDjogarObjects1.createFrom(runtimeScene.getObjects("jogar"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("jogar", gdjs.StartCode.GDjogarObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}

return;
}
gdjs['StartCode']= gdjs.StartCode;
