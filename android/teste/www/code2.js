gdjs.CompletouNivelCode = {};
gdjs.CompletouNivelCode.GDfadeObjects1= [];
gdjs.CompletouNivelCode.GDfadeObjects2= [];
gdjs.CompletouNivelCode.GDfadeObjects3= [];
gdjs.CompletouNivelCode.GDplataforma1Objects1= [];
gdjs.CompletouNivelCode.GDplataforma1Objects2= [];
gdjs.CompletouNivelCode.GDplataforma1Objects3= [];
gdjs.CompletouNivelCode.GDverdinhoObjects1= [];
gdjs.CompletouNivelCode.GDverdinhoObjects2= [];
gdjs.CompletouNivelCode.GDverdinhoObjects3= [];
gdjs.CompletouNivelCode.GDpanelObjects1= [];
gdjs.CompletouNivelCode.GDpanelObjects2= [];
gdjs.CompletouNivelCode.GDpanelObjects3= [];
gdjs.CompletouNivelCode.GDparabensObjects1= [];
gdjs.CompletouNivelCode.GDparabensObjects2= [];
gdjs.CompletouNivelCode.GDparabensObjects3= [];
gdjs.CompletouNivelCode.GDcompletouObjects1= [];
gdjs.CompletouNivelCode.GDcompletouObjects2= [];
gdjs.CompletouNivelCode.GDcompletouObjects3= [];
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects1= [];
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects2= [];
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects3= [];
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects1= [];
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects2= [];
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects3= [];
gdjs.CompletouNivelCode.GDbtVoltarObjects1= [];
gdjs.CompletouNivelCode.GDbtVoltarObjects2= [];
gdjs.CompletouNivelCode.GDbtVoltarObjects3= [];
gdjs.CompletouNivelCode.GDparedeObjects1= [];
gdjs.CompletouNivelCode.GDparedeObjects2= [];
gdjs.CompletouNivelCode.GDparedeObjects3= [];

gdjs.CompletouNivelCode.conditionTrue_0 = {val:false};
gdjs.CompletouNivelCode.condition0IsTrue_0 = {val:false};
gdjs.CompletouNivelCode.condition1IsTrue_0 = {val:false};
gdjs.CompletouNivelCode.condition2IsTrue_0 = {val:false};
gdjs.CompletouNivelCode.condition3IsTrue_0 = {val:false};
gdjs.CompletouNivelCode.conditionTrue_1 = {val:false};
gdjs.CompletouNivelCode.condition0IsTrue_1 = {val:false};
gdjs.CompletouNivelCode.condition1IsTrue_1 = {val:false};
gdjs.CompletouNivelCode.condition2IsTrue_1 = {val:false};
gdjs.CompletouNivelCode.condition3IsTrue_1 = {val:false};


gdjs.CompletouNivelCode.mapOfGDgdjs_46CompletouNivelCode_46GDtxtProximoNivelObjects1Objects = Hashtable.newFrom({"txtProximoNivel": gdjs.CompletouNivelCode.GDtxtProximoNivelObjects1});gdjs.CompletouNivelCode.eventsList0x6c20f0 = function(runtimeScene, context) {

{


gdjs.CompletouNivelCode.condition0IsTrue_0.val = false;
{
gdjs.CompletouNivelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) < 8;
}if (gdjs.CompletouNivelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(5).add(1);
}}

}


{


{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level", false);
}}

}


}; //End of gdjs.CompletouNivelCode.eventsList0x6c20f0
gdjs.CompletouNivelCode.mapOfGDgdjs_46CompletouNivelCode_46GDtxtOutroNivelObjects1Objects = Hashtable.newFrom({"txtOutroNivel": gdjs.CompletouNivelCode.GDtxtOutroNivelObjects1});gdjs.CompletouNivelCode.mapOfGDgdjs_46CompletouNivelCode_46GDfadeObjects1Objects = Hashtable.newFrom({"fade": gdjs.CompletouNivelCode.GDfadeObjects1});gdjs.CompletouNivelCode.eventsList0xaf580 = function(runtimeScene, context) {

{


gdjs.CompletouNivelCode.condition0IsTrue_0.val = false;
{
gdjs.CompletouNivelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.CompletouNivelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Intro Theme.ogg", 0, true, 50, 1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "applause.ogg", true, 150, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.CompletouNivelCode.GDverdinhoObjects1.createFrom(runtimeScene.getObjects("verdinho"));

gdjs.CompletouNivelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDverdinhoObjects1.length;i<l;++i) {
    if ( gdjs.CompletouNivelCode.GDverdinhoObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.CompletouNivelCode.condition0IsTrue_0.val = true;
        gdjs.CompletouNivelCode.GDverdinhoObjects1[k] = gdjs.CompletouNivelCode.GDverdinhoObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDverdinhoObjects1.length = k;}if (gdjs.CompletouNivelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.CompletouNivelCode.GDverdinhoObjects1 */
{for(var i = 0, len = gdjs.CompletouNivelCode.GDverdinhoObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDverdinhoObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.CompletouNivelCode.GDverdinhoObjects1.createFrom(runtimeScene.getObjects("verdinho"));

gdjs.CompletouNivelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDverdinhoObjects1.length;i<l;++i) {
    if ( !(gdjs.CompletouNivelCode.GDverdinhoObjects1[i].getBehavior("PlatformerObject").isFalling()) ) {
        gdjs.CompletouNivelCode.condition0IsTrue_0.val = true;
        gdjs.CompletouNivelCode.GDverdinhoObjects1[k] = gdjs.CompletouNivelCode.GDverdinhoObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDverdinhoObjects1.length = k;}if (gdjs.CompletouNivelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.CompletouNivelCode.GDverdinhoObjects1 */
{for(var i = 0, len = gdjs.CompletouNivelCode.GDverdinhoObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDverdinhoObjects1[i].setAnimation(0);
}
}}

}


{

gdjs.CompletouNivelCode.GDtxtProximoNivelObjects1.createFrom(runtimeScene.getObjects("txtProximoNivel"));

gdjs.CompletouNivelCode.condition0IsTrue_0.val = false;
gdjs.CompletouNivelCode.condition1IsTrue_0.val = false;
gdjs.CompletouNivelCode.condition2IsTrue_0.val = false;
{
gdjs.CompletouNivelCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.CompletouNivelCode.condition0IsTrue_0.val ) {
{
gdjs.CompletouNivelCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CompletouNivelCode.mapOfGDgdjs_46CompletouNivelCode_46GDtxtProximoNivelObjects1Objects, runtimeScene, true, false);
}if ( gdjs.CompletouNivelCode.condition1IsTrue_0.val ) {
{
{gdjs.CompletouNivelCode.conditionTrue_1 = gdjs.CompletouNivelCode.condition2IsTrue_0;
gdjs.CompletouNivelCode.conditionTrue_1.val = context.triggerOnce(7086756);
}
}}
}
if (gdjs.CompletouNivelCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.CompletouNivelCode.eventsList0x6c20f0(runtimeScene, context);} //End of subevents
}

}


{

gdjs.CompletouNivelCode.GDtxtOutroNivelObjects1.createFrom(runtimeScene.getObjects("txtOutroNivel"));

gdjs.CompletouNivelCode.condition0IsTrue_0.val = false;
gdjs.CompletouNivelCode.condition1IsTrue_0.val = false;
gdjs.CompletouNivelCode.condition2IsTrue_0.val = false;
{
gdjs.CompletouNivelCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.CompletouNivelCode.condition0IsTrue_0.val ) {
{
gdjs.CompletouNivelCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CompletouNivelCode.mapOfGDgdjs_46CompletouNivelCode_46GDtxtOutroNivelObjects1Objects, runtimeScene, true, false);
}if ( gdjs.CompletouNivelCode.condition1IsTrue_0.val ) {
{
{gdjs.CompletouNivelCode.conditionTrue_1 = gdjs.CompletouNivelCode.condition2IsTrue_0;
gdjs.CompletouNivelCode.conditionTrue_1.val = context.triggerOnce(7088268);
}
}}
}
if (gdjs.CompletouNivelCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


{

gdjs.CompletouNivelCode.GDverdinhoObjects1.createFrom(runtimeScene.getObjects("verdinho"));

gdjs.CompletouNivelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDverdinhoObjects1.length;i<l;++i) {
    if ( gdjs.CompletouNivelCode.GDverdinhoObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.CompletouNivelCode.condition0IsTrue_0.val = true;
        gdjs.CompletouNivelCode.GDverdinhoObjects1[k] = gdjs.CompletouNivelCode.GDverdinhoObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDverdinhoObjects1.length = k;}if (gdjs.CompletouNivelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.CompletouNivelCode.GDverdinhoObjects1 */
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "jump.wav", 0, false, 60, 1);
}{for(var i = 0, len = gdjs.CompletouNivelCode.GDverdinhoObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDverdinhoObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{



}


{


gdjs.CompletouNivelCode.condition0IsTrue_0.val = false;
{
gdjs.CompletouNivelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(7)) == 1;
}if (gdjs.CompletouNivelCode.condition0IsTrue_0.val) {
gdjs.CompletouNivelCode.GDfadeObjects1.length = 0;

{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(0);
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.CompletouNivelCode.mapOfGDgdjs_46CompletouNivelCode_46GDfadeObjects1Objects, 0, 0, "GUI");
}{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].setScaleX(gdjs.evtTools.window.getWindowWidth());
}
}{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].setScaleY(gdjs.evtTools.window.getWindowHeight());
}
}{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].setOpacity(255);
}
}}

}


{

gdjs.CompletouNivelCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));

gdjs.CompletouNivelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.CompletouNivelCode.GDfadeObjects1[i].getOpacity() == 255 ) {
        gdjs.CompletouNivelCode.condition0IsTrue_0.val = true;
        gdjs.CompletouNivelCode.GDfadeObjects1[k] = gdjs.CompletouNivelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDfadeObjects1.length = k;}if (gdjs.CompletouNivelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.CompletouNivelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].setOpacity(254);
}
}}

}


{

gdjs.CompletouNivelCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));

gdjs.CompletouNivelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.CompletouNivelCode.GDfadeObjects1[i].getOpacity() < 255 ) {
        gdjs.CompletouNivelCode.condition0IsTrue_0.val = true;
        gdjs.CompletouNivelCode.GDfadeObjects1[k] = gdjs.CompletouNivelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDfadeObjects1.length = k;}if (gdjs.CompletouNivelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.CompletouNivelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].setOpacity(gdjs.CompletouNivelCode.GDfadeObjects1[i].getOpacity() - (255*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{

gdjs.CompletouNivelCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));

gdjs.CompletouNivelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.CompletouNivelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.CompletouNivelCode.GDfadeObjects1[i].getOpacity() == 0 ) {
        gdjs.CompletouNivelCode.condition0IsTrue_0.val = true;
        gdjs.CompletouNivelCode.GDfadeObjects1[k] = gdjs.CompletouNivelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.CompletouNivelCode.GDfadeObjects1.length = k;}if (gdjs.CompletouNivelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.CompletouNivelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.CompletouNivelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.CompletouNivelCode.GDfadeObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.CompletouNivelCode.eventsList0xaf580


gdjs.CompletouNivelCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.CompletouNivelCode.GDfadeObjects1.length = 0;
gdjs.CompletouNivelCode.GDfadeObjects2.length = 0;
gdjs.CompletouNivelCode.GDfadeObjects3.length = 0;
gdjs.CompletouNivelCode.GDplataforma1Objects1.length = 0;
gdjs.CompletouNivelCode.GDplataforma1Objects2.length = 0;
gdjs.CompletouNivelCode.GDplataforma1Objects3.length = 0;
gdjs.CompletouNivelCode.GDverdinhoObjects1.length = 0;
gdjs.CompletouNivelCode.GDverdinhoObjects2.length = 0;
gdjs.CompletouNivelCode.GDverdinhoObjects3.length = 0;
gdjs.CompletouNivelCode.GDpanelObjects1.length = 0;
gdjs.CompletouNivelCode.GDpanelObjects2.length = 0;
gdjs.CompletouNivelCode.GDpanelObjects3.length = 0;
gdjs.CompletouNivelCode.GDparabensObjects1.length = 0;
gdjs.CompletouNivelCode.GDparabensObjects2.length = 0;
gdjs.CompletouNivelCode.GDparabensObjects3.length = 0;
gdjs.CompletouNivelCode.GDcompletouObjects1.length = 0;
gdjs.CompletouNivelCode.GDcompletouObjects2.length = 0;
gdjs.CompletouNivelCode.GDcompletouObjects3.length = 0;
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects1.length = 0;
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects2.length = 0;
gdjs.CompletouNivelCode.GDtxtProximoNivelObjects3.length = 0;
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects1.length = 0;
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects2.length = 0;
gdjs.CompletouNivelCode.GDtxtOutroNivelObjects3.length = 0;
gdjs.CompletouNivelCode.GDbtVoltarObjects1.length = 0;
gdjs.CompletouNivelCode.GDbtVoltarObjects2.length = 0;
gdjs.CompletouNivelCode.GDbtVoltarObjects3.length = 0;
gdjs.CompletouNivelCode.GDparedeObjects1.length = 0;
gdjs.CompletouNivelCode.GDparedeObjects2.length = 0;
gdjs.CompletouNivelCode.GDparedeObjects3.length = 0;

gdjs.CompletouNivelCode.eventsList0xaf580(runtimeScene, context);return;
}
gdjs['CompletouNivelCode']= gdjs.CompletouNivelCode;
