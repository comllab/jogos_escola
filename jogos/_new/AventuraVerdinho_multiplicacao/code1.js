gdjs.LevelCode = {};
gdjs.LevelCode.GDPlayerObjects1_1final = [];

gdjs.LevelCode.GDfadeObjects1= [];
gdjs.LevelCode.GDfadeObjects2= [];
gdjs.LevelCode.GDfadeObjects3= [];
gdjs.LevelCode.GDPlayerObjects1= [];
gdjs.LevelCode.GDPlayerObjects2= [];
gdjs.LevelCode.GDPlayerObjects3= [];
gdjs.LevelCode.GDJumpthruObjects1= [];
gdjs.LevelCode.GDJumpthruObjects2= [];
gdjs.LevelCode.GDJumpthruObjects3= [];
gdjs.LevelCode.GDPlatformObjects1= [];
gdjs.LevelCode.GDPlatformObjects2= [];
gdjs.LevelCode.GDPlatformObjects3= [];
gdjs.LevelCode.GDTiledGrassPlatformObjects1= [];
gdjs.LevelCode.GDTiledGrassPlatformObjects2= [];
gdjs.LevelCode.GDTiledGrassPlatformObjects3= [];
gdjs.LevelCode.GDTiledPlatformObjects1= [];
gdjs.LevelCode.GDTiledPlatformObjects2= [];
gdjs.LevelCode.GDTiledPlatformObjects3= [];
gdjs.LevelCode.GDTiledCastlePlatformObjects1= [];
gdjs.LevelCode.GDTiledCastlePlatformObjects2= [];
gdjs.LevelCode.GDTiledCastlePlatformObjects3= [];
gdjs.LevelCode.GDMovingPlatformObjects1= [];
gdjs.LevelCode.GDMovingPlatformObjects2= [];
gdjs.LevelCode.GDMovingPlatformObjects3= [];
gdjs.LevelCode.GDGoLeftObjects1= [];
gdjs.LevelCode.GDGoLeftObjects2= [];
gdjs.LevelCode.GDGoLeftObjects3= [];
gdjs.LevelCode.GDGoRightObjects1= [];
gdjs.LevelCode.GDGoRightObjects2= [];
gdjs.LevelCode.GDGoRightObjects3= [];
gdjs.LevelCode.GDLadderObjects1= [];
gdjs.LevelCode.GDLadderObjects2= [];
gdjs.LevelCode.GDLadderObjects3= [];
gdjs.LevelCode.GDPlayerHitBoxObjects1= [];
gdjs.LevelCode.GDPlayerHitBoxObjects2= [];
gdjs.LevelCode.GDPlayerHitBoxObjects3= [];
gdjs.LevelCode.GDSlimeWalkObjects1= [];
gdjs.LevelCode.GDSlimeWalkObjects2= [];
gdjs.LevelCode.GDSlimeWalkObjects3= [];
gdjs.LevelCode.GDFlyObjects1= [];
gdjs.LevelCode.GDFlyObjects2= [];
gdjs.LevelCode.GDFlyObjects3= [];
gdjs.LevelCode.GDCloudObjects1= [];
gdjs.LevelCode.GDCloudObjects2= [];
gdjs.LevelCode.GDCloudObjects3= [];
gdjs.LevelCode.GDBackgroundObjectsObjects1= [];
gdjs.LevelCode.GDBackgroundObjectsObjects2= [];
gdjs.LevelCode.GDBackgroundObjectsObjects3= [];
gdjs.LevelCode.GDScoreObjects1= [];
gdjs.LevelCode.GDScoreObjects2= [];
gdjs.LevelCode.GDScoreObjects3= [];
gdjs.LevelCode.GDCoinObjects1= [];
gdjs.LevelCode.GDCoinObjects2= [];
gdjs.LevelCode.GDCoinObjects3= [];
gdjs.LevelCode.GDCoinIconObjects1= [];
gdjs.LevelCode.GDCoinIconObjects2= [];
gdjs.LevelCode.GDCoinIconObjects3= [];
gdjs.LevelCode.GDLeftButtonObjects1= [];
gdjs.LevelCode.GDLeftButtonObjects2= [];
gdjs.LevelCode.GDLeftButtonObjects3= [];
gdjs.LevelCode.GDRightButtonObjects1= [];
gdjs.LevelCode.GDRightButtonObjects2= [];
gdjs.LevelCode.GDRightButtonObjects3= [];
gdjs.LevelCode.GDJumpButtonObjects1= [];
gdjs.LevelCode.GDJumpButtonObjects2= [];
gdjs.LevelCode.GDJumpButtonObjects3= [];
gdjs.LevelCode.GDArrowButtonsBgObjects1= [];
gdjs.LevelCode.GDArrowButtonsBgObjects2= [];
gdjs.LevelCode.GDArrowButtonsBgObjects3= [];
gdjs.LevelCode.GDtxtFaseObjects1= [];
gdjs.LevelCode.GDtxtFaseObjects2= [];
gdjs.LevelCode.GDtxtFaseObjects3= [];
gdjs.LevelCode.GDtxtNivelObjects1= [];
gdjs.LevelCode.GDtxtNivelObjects2= [];
gdjs.LevelCode.GDtxtNivelObjects3= [];
gdjs.LevelCode.GDpanelTopObjects1= [];
gdjs.LevelCode.GDpanelTopObjects2= [];
gdjs.LevelCode.GDpanelTopObjects3= [];
gdjs.LevelCode.GDcactusObjects1= [];
gdjs.LevelCode.GDcactusObjects2= [];
gdjs.LevelCode.GDcactusObjects3= [];
gdjs.LevelCode.GDexitObjects1= [];
gdjs.LevelCode.GDexitObjects2= [];
gdjs.LevelCode.GDexitObjects3= [];
gdjs.LevelCode.GDportaObjects1= [];
gdjs.LevelCode.GDportaObjects2= [];
gdjs.LevelCode.GDportaObjects3= [];
gdjs.LevelCode.GDfaseObjects1= [];
gdjs.LevelCode.GDfaseObjects2= [];
gdjs.LevelCode.GDfaseObjects3= [];
gdjs.LevelCode.GDNewObjectObjects1= [];
gdjs.LevelCode.GDNewObjectObjects2= [];
gdjs.LevelCode.GDNewObjectObjects3= [];
gdjs.LevelCode.GDponteObjects1= [];
gdjs.LevelCode.GDponteObjects2= [];
gdjs.LevelCode.GDponteObjects3= [];


gdjs.LevelCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.LevelCode.GDPlayerHitBoxObjects1, gdjs.LevelCode.GDPlayerHitBoxObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects2.length;i<l;++i) {
    if ( !(gdjs.LevelCode.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects2[k] = gdjs.LevelCode.GDPlayerHitBoxObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects2);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects2[i].setAnimation(0);
}
}}

}


{

/* Reuse gdjs.LevelCode.GDPlayerHitBoxObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isMoving() ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects1[k] = gdjs.LevelCode.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].setAnimation(2);
}
}}

}


};gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDGoLeftObjects1Objects = Hashtable.newFrom({"GoLeft": gdjs.LevelCode.GDGoLeftObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDMovingPlatformObjects1Objects = Hashtable.newFrom({"MovingPlatform": gdjs.LevelCode.GDMovingPlatformObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDGoRightObjects1Objects = Hashtable.newFrom({"GoRight": gdjs.LevelCode.GDGoRightObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDMovingPlatformObjects1Objects = Hashtable.newFrom({"MovingPlatform": gdjs.LevelCode.GDMovingPlatformObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDGoLeftObjects1Objects = Hashtable.newFrom({"GoLeft": gdjs.LevelCode.GDGoLeftObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDSlimeWalkObjects1ObjectsGDgdjs_9546LevelCode_9546GDFlyObjects1Objects = Hashtable.newFrom({"SlimeWalk": gdjs.LevelCode.GDSlimeWalkObjects1, "Fly": gdjs.LevelCode.GDFlyObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDGoRightObjects1Objects = Hashtable.newFrom({"GoRight": gdjs.LevelCode.GDGoRightObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDSlimeWalkObjects1ObjectsGDgdjs_9546LevelCode_9546GDFlyObjects1Objects = Hashtable.newFrom({"SlimeWalk": gdjs.LevelCode.GDSlimeWalkObjects1, "Fly": gdjs.LevelCode.GDFlyObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDPlayerHitBoxObjects1Objects = Hashtable.newFrom({"PlayerHitBox": gdjs.LevelCode.GDPlayerHitBoxObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDSlimeWalkObjects1ObjectsGDgdjs_9546LevelCode_9546GDFlyObjects1Objects = Hashtable.newFrom({"SlimeWalk": gdjs.LevelCode.GDSlimeWalkObjects1, "Fly": gdjs.LevelCode.GDFlyObjects1});
gdjs.LevelCode.eventsList1 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.LevelCode.GDPlayerHitBoxObjects1, gdjs.LevelCode.GDPlayerHitBoxObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").isFalling() ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects2[k] = gdjs.LevelCode.GDPlayerHitBoxObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.LevelCode.GDFlyObjects1, gdjs.LevelCode.GDFlyObjects2);

/* Reuse gdjs.LevelCode.GDPlayerHitBoxObjects2 */
gdjs.copyArray(gdjs.LevelCode.GDSlimeWalkObjects1, gdjs.LevelCode.GDSlimeWalkObjects2);

{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].setAnimation(1);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].activateBehavior("PlatformerObject", true);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].activateBehavior("PlatformerObject", true);
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].getBehavior("PlatformerObject").setGravity(1500);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].getBehavior("PlatformerObject").setGravity(1500);
}
}{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").setCanJump();
}
}{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}}

}


{

/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDPlayerHitBoxObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getY() >= (( gdjs.LevelCode.GDFlyObjects1.length === 0 ) ? (( gdjs.LevelCode.GDSlimeWalkObjects1.length === 0 ) ? 0 :gdjs.LevelCode.GDSlimeWalkObjects1[0].getPointY("")) :gdjs.LevelCode.GDFlyObjects1[0].getPointY("")) - (gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getHeight()) + (( gdjs.LevelCode.GDFlyObjects1.length === 0 ) ? (( gdjs.LevelCode.GDSlimeWalkObjects1.length === 0 ) ? 0 :gdjs.LevelCode.GDSlimeWalkObjects1[0].getHeight()) :gdjs.LevelCode.GDFlyObjects1[0].getHeight()) / 2 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects1[k] = gdjs.LevelCode.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = k;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level", false);
}{runtimeScene.getGame().getVariables().getFromIndex(6).sub(10);
}}

}


};gdjs.LevelCode.eventsList2 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.LevelCode.GDFlyObjects1, gdjs.LevelCode.GDFlyObjects2);

gdjs.copyArray(gdjs.LevelCode.GDSlimeWalkObjects1, gdjs.LevelCode.GDSlimeWalkObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects2[i].getVariableNumber(gdjs.LevelCode.GDSlimeWalkObjects2[i].getVariables().get("GoingLeft")) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDSlimeWalkObjects2[k] = gdjs.LevelCode.GDSlimeWalkObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects2[i].getVariableNumber(gdjs.LevelCode.GDFlyObjects2[i].getVariables().get("GoingLeft")) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDFlyObjects2[k] = gdjs.LevelCode.GDFlyObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDFlyObjects2 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects2 */
{for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].addForce(-(300), 0, 0);
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].getBehavior("PlatformerObject").simulateLeftKey();
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].getBehavior("PlatformerObject").simulateLeftKey();
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].flipX(false);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].flipX(false);
}
}}

}


{

gdjs.copyArray(gdjs.LevelCode.GDFlyObjects1, gdjs.LevelCode.GDFlyObjects2);

gdjs.copyArray(gdjs.LevelCode.GDSlimeWalkObjects1, gdjs.LevelCode.GDSlimeWalkObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects2[i].getVariableNumber(gdjs.LevelCode.GDSlimeWalkObjects2[i].getVariables().get("GoingLeft")) == 0 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDSlimeWalkObjects2[k] = gdjs.LevelCode.GDSlimeWalkObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects2[i].getVariableNumber(gdjs.LevelCode.GDFlyObjects2[i].getVariables().get("GoingLeft")) == 0 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDFlyObjects2[k] = gdjs.LevelCode.GDFlyObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDFlyObjects2 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects2 */
{for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].addForce(300, 0, 0);
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].getBehavior("PlatformerObject").simulateRightKey();
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].getBehavior("PlatformerObject").simulateRightKey();
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].flipX(true);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].flipX(true);
}
}}

}


{



}


{

/* Reuse gdjs.LevelCode.GDFlyObjects1 */
gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDPlayerHitBoxObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDSlimeWalkObjects1ObjectsGDgdjs_9546LevelCode_9546GDFlyObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_0) {

{ //Subevents
gdjs.LevelCode.eventsList1(runtimeScene);} //End of subevents
}

}


};gdjs.LevelCode.eventsList3 = function(runtimeScene) {

{

/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects1[i].getOpacity() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDSlimeWalkObjects1[k] = gdjs.LevelCode.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects1.length = k;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects1[i].getOpacity() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDFlyObjects1[k] = gdjs.LevelCode.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDfaseObjects1Objects = Hashtable.newFrom({"fase": gdjs.LevelCode.GDfaseObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDPlayerHitBoxObjects1Objects = Hashtable.newFrom({"PlayerHitBox": gdjs.LevelCode.GDPlayerHitBoxObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDCoinObjects1Objects = Hashtable.newFrom({"Coin": gdjs.LevelCode.GDCoinObjects1});
gdjs.LevelCode.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 3;
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDCoinObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDCoinObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDCoinObjects1[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(1);
}{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "Operacao");
}}

}


};gdjs.LevelCode.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = !(gdjs.evtTools.systemInfo.isMobile());
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("ArrowButtonsBg"), gdjs.LevelCode.GDArrowButtonsBgObjects1);
gdjs.copyArray(runtimeScene.getObjects("JumpButton"), gdjs.LevelCode.GDJumpButtonObjects1);
gdjs.copyArray(runtimeScene.getObjects("LeftButton"), gdjs.LevelCode.GDLeftButtonObjects1);
gdjs.copyArray(runtimeScene.getObjects("RightButton"), gdjs.LevelCode.GDRightButtonObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDLeftButtonObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDLeftButtonObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.LevelCode.GDRightButtonObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDRightButtonObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.LevelCode.GDJumpButtonObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDJumpButtonObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.LevelCode.GDArrowButtonsBgObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDArrowButtonsBgObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDLeftButtonObjects1Objects = Hashtable.newFrom({"LeftButton": gdjs.LevelCode.GDLeftButtonObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDRightButtonObjects1Objects = Hashtable.newFrom({"RightButton": gdjs.LevelCode.GDRightButtonObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDJumpButtonObjects1Objects = Hashtable.newFrom({"JumpButton": gdjs.LevelCode.GDJumpButtonObjects1});
gdjs.LevelCode.eventsList6 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(2) + 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(9) + 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(2) + 4);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(9) + 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 3;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(2) + 7);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(9) + 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 4;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(49) + 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(49) + 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 5;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(49) + 50);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(49) + 50);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 6;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(400) + 100);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(400) + 100);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 7;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(900) + 100);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(900) + 100);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 8;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(8999) + 1000);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(8999) + 1000);
}}

}


};gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.LevelCode.GDPlayerObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDexitObjects1Objects = Hashtable.newFrom({"exit": gdjs.LevelCode.GDexitObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.LevelCode.GDPlayerObjects2});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDportaObjects2Objects = Hashtable.newFrom({"porta": gdjs.LevelCode.GDportaObjects2});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.LevelCode.GDPlayerObjects1});
gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDportaObjects1Objects = Hashtable.newFrom({"porta": gdjs.LevelCode.GDportaObjects1});
gdjs.LevelCode.eventsList7 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("porta"), gdjs.LevelCode.GDportaObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDPlayerObjects2Objects, gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDportaObjects2Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 6;
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "CompletouNivel", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);
gdjs.copyArray(runtimeScene.getObjects("porta"), gdjs.LevelCode.GDportaObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDPlayerObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDportaObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) < 6;
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(4).add(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level", false);
}}

}


};gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDfadeObjects1Objects = Hashtable.newFrom({"fade": gdjs.LevelCode.GDfadeObjects1});
gdjs.LevelCode.eventsList8 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);
gdjs.copyArray(runtimeScene.getObjects("txtNivel"), gdjs.LevelCode.GDtxtNivelObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.LevelCode.GDtxtNivelObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDtxtNivelObjects1[i].setString("Nível " + gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(5)));
}
}{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "Fase" + gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)), 0, 0);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Intro Theme.ogg", 1, true, 100, 1);
}{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").setJumpSpeed(1000);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").setJumpSpeed(0);
}
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);
gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].setPosition((( gdjs.LevelCode.GDPlayerHitBoxObjects1.length === 0 ) ? 0 :gdjs.LevelCode.GDPlayerHitBoxObjects1[0].getPointX("")) - 12,(( gdjs.LevelCode.GDPlayerHitBoxObjects1.length === 0 ) ? 0 :gdjs.LevelCode.GDPlayerHitBoxObjects1[0].getPointY("")));
}
}}

}


{



}


{

gdjs.LevelCode.GDPlayerObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.LevelCode.GDPlayerObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects2);
for (var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerObjects2[i].getAnimation() == 0 ) {
        isConditionTrue_1 = true;
        gdjs.LevelCode.GDPlayerObjects2[k] = gdjs.LevelCode.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.LevelCode.GDPlayerObjects2.length; j < jLen ; ++j) {
        if ( gdjs.LevelCode.GDPlayerObjects1_1final.indexOf(gdjs.LevelCode.GDPlayerObjects2[j]) === -1 )
            gdjs.LevelCode.GDPlayerObjects1_1final.push(gdjs.LevelCode.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects2);
for (var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerObjects2[i].getAnimation() == 2 ) {
        isConditionTrue_1 = true;
        gdjs.LevelCode.GDPlayerObjects2[k] = gdjs.LevelCode.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.LevelCode.GDPlayerObjects2.length; j < jLen ; ++j) {
        if ( gdjs.LevelCode.GDPlayerObjects1_1final.indexOf(gdjs.LevelCode.GDPlayerObjects2[j]) === -1 )
            gdjs.LevelCode.GDPlayerObjects1_1final.push(gdjs.LevelCode.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.LevelCode.GDPlayerObjects1_1final, gdjs.LevelCode.GDPlayerObjects1);
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isJumping() ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects1[k] = gdjs.LevelCode.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects1[k] = gdjs.LevelCode.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects1[k] = gdjs.LevelCode.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.LevelCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 0;
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].flipX(true);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].flipX(false);
}
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);
{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.LevelCode.GDPlayerObjects1.length === 0 ) ? 0 :gdjs.LevelCode.GDPlayerObjects1[0].getPointX("")), "", 0);
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("GoLeft"), gdjs.LevelCode.GDGoLeftObjects1);
gdjs.copyArray(runtimeScene.getObjects("GoRight"), gdjs.LevelCode.GDGoRightObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDGoLeftObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDGoLeftObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.LevelCode.GDGoRightObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDGoRightObjects1[i].hide();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("GoLeft"), gdjs.LevelCode.GDGoLeftObjects1);
gdjs.copyArray(runtimeScene.getObjects("MovingPlatform"), gdjs.LevelCode.GDMovingPlatformObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDGoLeftObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDMovingPlatformObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDMovingPlatformObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDMovingPlatformObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.LevelCode.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDMovingPlatformObjects1[i].addForce(-(150), 0, 1);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("GoRight"), gdjs.LevelCode.GDGoRightObjects1);
gdjs.copyArray(runtimeScene.getObjects("MovingPlatform"), gdjs.LevelCode.GDMovingPlatformObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDGoRightObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDMovingPlatformObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDMovingPlatformObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDMovingPlatformObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.LevelCode.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDMovingPlatformObjects1[i].addForce(150, 0, 1);
}
}}

}


{



}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Fly"), gdjs.LevelCode.GDFlyObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].activateBehavior("PlatformerObject", false);
}
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Fly"), gdjs.LevelCode.GDFlyObjects1);
gdjs.copyArray(runtimeScene.getObjects("GoLeft"), gdjs.LevelCode.GDGoLeftObjects1);
gdjs.copyArray(runtimeScene.getObjects("SlimeWalk"), gdjs.LevelCode.GDSlimeWalkObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDGoLeftObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDSlimeWalkObjects1ObjectsGDgdjs_9546LevelCode_9546GDFlyObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects1[i].returnVariable(gdjs.LevelCode.GDSlimeWalkObjects1[i].getVariables().get("GoingLeft")).setNumber(1);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].returnVariable(gdjs.LevelCode.GDFlyObjects1[i].getVariables().get("GoingLeft")).setNumber(1);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Fly"), gdjs.LevelCode.GDFlyObjects1);
gdjs.copyArray(runtimeScene.getObjects("GoRight"), gdjs.LevelCode.GDGoRightObjects1);
gdjs.copyArray(runtimeScene.getObjects("SlimeWalk"), gdjs.LevelCode.GDSlimeWalkObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDGoRightObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDSlimeWalkObjects1ObjectsGDgdjs_9546LevelCode_9546GDFlyObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects1[i].returnVariable(gdjs.LevelCode.GDSlimeWalkObjects1[i].getVariables().get("GoingLeft")).setNumber(0);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].returnVariable(gdjs.LevelCode.GDFlyObjects1[i].getVariables().get("GoingLeft")).setNumber(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Fly"), gdjs.LevelCode.GDFlyObjects1);
gdjs.copyArray(runtimeScene.getObjects("SlimeWalk"), gdjs.LevelCode.GDSlimeWalkObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects1[i].getAnimation() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDSlimeWalkObjects1[k] = gdjs.LevelCode.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects1.length = k;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects1[i].getAnimation() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDFlyObjects1[k] = gdjs.LevelCode.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects1.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.LevelCode.eventsList2(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Fly"), gdjs.LevelCode.GDFlyObjects1);
gdjs.copyArray(runtimeScene.getObjects("SlimeWalk"), gdjs.LevelCode.GDSlimeWalkObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects1[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDSlimeWalkObjects1[k] = gdjs.LevelCode.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects1.length = k;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects1[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDFlyObjects1[k] = gdjs.LevelCode.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDSlimeWalkObjects1[k] = gdjs.LevelCode.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects1.length = k;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDFlyObjects1[k] = gdjs.LevelCode.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( !(gdjs.LevelCode.GDSlimeWalkObjects1[i].getBehavior("PlatformerObject").isMoving()) ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDSlimeWalkObjects1[k] = gdjs.LevelCode.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects1.length = k;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects1.length;i<l;++i) {
    if ( !(gdjs.LevelCode.GDFlyObjects1[i].getBehavior("PlatformerObject").isMoving()) ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDFlyObjects1[k] = gdjs.LevelCode.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects1.length = k;
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects1[i].activateBehavior("PlatformerObject", false);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].activateBehavior("PlatformerObject", false);
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects1[i].setOpacity(gdjs.LevelCode.GDSlimeWalkObjects1[i].getOpacity() - (50 * gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].setOpacity(gdjs.LevelCode.GDFlyObjects1[i].getOpacity() - (50 * gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}
{ //Subevents
gdjs.LevelCode.eventsList3(runtimeScene);} //End of subevents
}

}


{



}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.LevelCode.GDfaseObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDfaseObjects1Objects, 250, 620, "GUI");
}{for(var i = 0, len = gdjs.LevelCode.GDfaseObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfaseObjects1[i].setString("FASE " + gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Coin"), gdjs.LevelCode.GDCoinObjects1);
gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDPlayerHitBoxObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDCoinObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDCoinObjects1[i].getOpacity() == 255 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDCoinObjects1[k] = gdjs.LevelCode.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDCoinObjects1.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDCoinObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDCoinObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDCoinObjects1[i].setOpacity(254);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "coin.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(6).add(10);
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}
{ //Subevents
gdjs.LevelCode.eventsList4(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Coin"), gdjs.LevelCode.GDCoinObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDCoinObjects1[i].getOpacity() < 255 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDCoinObjects1[k] = gdjs.LevelCode.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDCoinObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDCoinObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDCoinObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDCoinObjects1[i].setOpacity(gdjs.LevelCode.GDCoinObjects1[i].getOpacity() - (255 * gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}{for(var i = 0, len = gdjs.LevelCode.GDCoinObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDCoinObjects1[i].addForce(0, -(30), 0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Coin"), gdjs.LevelCode.GDCoinObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDCoinObjects1[i].getOpacity() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDCoinObjects1[k] = gdjs.LevelCode.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDCoinObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDCoinObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDCoinObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDCoinObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("fase"), gdjs.LevelCode.GDfaseObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDfaseObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDfaseObjects1[i].getY() > 100 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDfaseObjects1[k] = gdjs.LevelCode.GDfaseObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDfaseObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDfaseObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDfaseObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfaseObjects1[i].addForce(0, -(200), 0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("fase"), gdjs.LevelCode.GDfaseObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDfaseObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDfaseObjects1[i].getY() < 100 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDfaseObjects1[k] = gdjs.LevelCode.GDfaseObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDfaseObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDfaseObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDfaseObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfaseObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("txtFase"), gdjs.LevelCode.GDtxtFaseObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDtxtFaseObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDtxtFaseObjects1[i].setString("Fase " + gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) < 0;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Score"), gdjs.LevelCode.GDScoreObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDScoreObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDScoreObjects1[i].setString("x " + gdjs.evtTools.variable.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(6)));
}
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.input.touchSimulateMouse(runtimeScene, false);
}
{ //Subevents
gdjs.LevelCode.eventsList5(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("LeftButton"), gdjs.LevelCode.GDLeftButtonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.systemInfo.isMobile();
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDLeftButtonObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);
gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateLeftKey();
}
}{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].flipX(true);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("RightButton"), gdjs.LevelCode.GDRightButtonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.systemInfo.isMobile();
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDRightButtonObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);
gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateRightKey();
}
}{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].flipX(false);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("JumpButton"), gdjs.LevelCode.GDJumpButtonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.systemInfo.isMobile();
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDJumpButtonObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9620268);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{isConditionTrue_0 = (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.LevelCode.eventsList6(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);
gdjs.copyArray(runtimeScene.getObjects("exit"), gdjs.LevelCode.GDexitObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDPlayerObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDexitObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9632364);
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "applause.ogg", false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("PlayerHitBox"), gdjs.LevelCode.GDPlayerHitBoxObjects1);
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateRightKey();
}
}
{ //Subevents
gdjs.LevelCode.eventsList7(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(7)) == 1;
if (isConditionTrue_0) {
gdjs.LevelCode.GDfadeObjects1.length = 0;

{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(0);
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.LevelCode.mapOfGDgdjs_9546LevelCode_9546GDfadeObjects1Objects, 0, 0, "GUI");
}{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].setScaleX(gdjs.evtTools.window.getWindowInnerWidth());
}
}{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].setScaleY(gdjs.evtTools.window.getWindowInnerHeight());
}
}{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].setOpacity(255);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("fade"), gdjs.LevelCode.GDfadeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDfadeObjects1[i].getOpacity() == 255 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDfadeObjects1[k] = gdjs.LevelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDfadeObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].setOpacity(254);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("fade"), gdjs.LevelCode.GDfadeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDfadeObjects1[i].getOpacity() < 255 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDfadeObjects1[k] = gdjs.LevelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDfadeObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].setOpacity(gdjs.LevelCode.GDfadeObjects1[i].getOpacity() - (255 * gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("fade"), gdjs.LevelCode.GDfadeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDfadeObjects1[i].getOpacity() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDfadeObjects1[k] = gdjs.LevelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDfadeObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.LevelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.LevelCode.GDPlayerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerObjects1[i].getY() > gdjs.evtTools.window.getWindowInnerHeight() ) {
        isConditionTrue_0 = true;
        gdjs.LevelCode.GDPlayerObjects1[k] = gdjs.LevelCode.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerObjects1.length = k;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(6).sub(10);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level", false);
}}

}


};

gdjs.LevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.LevelCode.GDfadeObjects1.length = 0;
gdjs.LevelCode.GDfadeObjects2.length = 0;
gdjs.LevelCode.GDfadeObjects3.length = 0;
gdjs.LevelCode.GDPlayerObjects1.length = 0;
gdjs.LevelCode.GDPlayerObjects2.length = 0;
gdjs.LevelCode.GDPlayerObjects3.length = 0;
gdjs.LevelCode.GDJumpthruObjects1.length = 0;
gdjs.LevelCode.GDJumpthruObjects2.length = 0;
gdjs.LevelCode.GDJumpthruObjects3.length = 0;
gdjs.LevelCode.GDPlatformObjects1.length = 0;
gdjs.LevelCode.GDPlatformObjects2.length = 0;
gdjs.LevelCode.GDPlatformObjects3.length = 0;
gdjs.LevelCode.GDTiledGrassPlatformObjects1.length = 0;
gdjs.LevelCode.GDTiledGrassPlatformObjects2.length = 0;
gdjs.LevelCode.GDTiledGrassPlatformObjects3.length = 0;
gdjs.LevelCode.GDTiledPlatformObjects1.length = 0;
gdjs.LevelCode.GDTiledPlatformObjects2.length = 0;
gdjs.LevelCode.GDTiledPlatformObjects3.length = 0;
gdjs.LevelCode.GDTiledCastlePlatformObjects1.length = 0;
gdjs.LevelCode.GDTiledCastlePlatformObjects2.length = 0;
gdjs.LevelCode.GDTiledCastlePlatformObjects3.length = 0;
gdjs.LevelCode.GDMovingPlatformObjects1.length = 0;
gdjs.LevelCode.GDMovingPlatformObjects2.length = 0;
gdjs.LevelCode.GDMovingPlatformObjects3.length = 0;
gdjs.LevelCode.GDGoLeftObjects1.length = 0;
gdjs.LevelCode.GDGoLeftObjects2.length = 0;
gdjs.LevelCode.GDGoLeftObjects3.length = 0;
gdjs.LevelCode.GDGoRightObjects1.length = 0;
gdjs.LevelCode.GDGoRightObjects2.length = 0;
gdjs.LevelCode.GDGoRightObjects3.length = 0;
gdjs.LevelCode.GDLadderObjects1.length = 0;
gdjs.LevelCode.GDLadderObjects2.length = 0;
gdjs.LevelCode.GDLadderObjects3.length = 0;
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = 0;
gdjs.LevelCode.GDPlayerHitBoxObjects2.length = 0;
gdjs.LevelCode.GDPlayerHitBoxObjects3.length = 0;
gdjs.LevelCode.GDSlimeWalkObjects1.length = 0;
gdjs.LevelCode.GDSlimeWalkObjects2.length = 0;
gdjs.LevelCode.GDSlimeWalkObjects3.length = 0;
gdjs.LevelCode.GDFlyObjects1.length = 0;
gdjs.LevelCode.GDFlyObjects2.length = 0;
gdjs.LevelCode.GDFlyObjects3.length = 0;
gdjs.LevelCode.GDCloudObjects1.length = 0;
gdjs.LevelCode.GDCloudObjects2.length = 0;
gdjs.LevelCode.GDCloudObjects3.length = 0;
gdjs.LevelCode.GDBackgroundObjectsObjects1.length = 0;
gdjs.LevelCode.GDBackgroundObjectsObjects2.length = 0;
gdjs.LevelCode.GDBackgroundObjectsObjects3.length = 0;
gdjs.LevelCode.GDScoreObjects1.length = 0;
gdjs.LevelCode.GDScoreObjects2.length = 0;
gdjs.LevelCode.GDScoreObjects3.length = 0;
gdjs.LevelCode.GDCoinObjects1.length = 0;
gdjs.LevelCode.GDCoinObjects2.length = 0;
gdjs.LevelCode.GDCoinObjects3.length = 0;
gdjs.LevelCode.GDCoinIconObjects1.length = 0;
gdjs.LevelCode.GDCoinIconObjects2.length = 0;
gdjs.LevelCode.GDCoinIconObjects3.length = 0;
gdjs.LevelCode.GDLeftButtonObjects1.length = 0;
gdjs.LevelCode.GDLeftButtonObjects2.length = 0;
gdjs.LevelCode.GDLeftButtonObjects3.length = 0;
gdjs.LevelCode.GDRightButtonObjects1.length = 0;
gdjs.LevelCode.GDRightButtonObjects2.length = 0;
gdjs.LevelCode.GDRightButtonObjects3.length = 0;
gdjs.LevelCode.GDJumpButtonObjects1.length = 0;
gdjs.LevelCode.GDJumpButtonObjects2.length = 0;
gdjs.LevelCode.GDJumpButtonObjects3.length = 0;
gdjs.LevelCode.GDArrowButtonsBgObjects1.length = 0;
gdjs.LevelCode.GDArrowButtonsBgObjects2.length = 0;
gdjs.LevelCode.GDArrowButtonsBgObjects3.length = 0;
gdjs.LevelCode.GDtxtFaseObjects1.length = 0;
gdjs.LevelCode.GDtxtFaseObjects2.length = 0;
gdjs.LevelCode.GDtxtFaseObjects3.length = 0;
gdjs.LevelCode.GDtxtNivelObjects1.length = 0;
gdjs.LevelCode.GDtxtNivelObjects2.length = 0;
gdjs.LevelCode.GDtxtNivelObjects3.length = 0;
gdjs.LevelCode.GDpanelTopObjects1.length = 0;
gdjs.LevelCode.GDpanelTopObjects2.length = 0;
gdjs.LevelCode.GDpanelTopObjects3.length = 0;
gdjs.LevelCode.GDcactusObjects1.length = 0;
gdjs.LevelCode.GDcactusObjects2.length = 0;
gdjs.LevelCode.GDcactusObjects3.length = 0;
gdjs.LevelCode.GDexitObjects1.length = 0;
gdjs.LevelCode.GDexitObjects2.length = 0;
gdjs.LevelCode.GDexitObjects3.length = 0;
gdjs.LevelCode.GDportaObjects1.length = 0;
gdjs.LevelCode.GDportaObjects2.length = 0;
gdjs.LevelCode.GDportaObjects3.length = 0;
gdjs.LevelCode.GDfaseObjects1.length = 0;
gdjs.LevelCode.GDfaseObjects2.length = 0;
gdjs.LevelCode.GDfaseObjects3.length = 0;
gdjs.LevelCode.GDNewObjectObjects1.length = 0;
gdjs.LevelCode.GDNewObjectObjects2.length = 0;
gdjs.LevelCode.GDNewObjectObjects3.length = 0;
gdjs.LevelCode.GDponteObjects1.length = 0;
gdjs.LevelCode.GDponteObjects2.length = 0;
gdjs.LevelCode.GDponteObjects3.length = 0;

gdjs.LevelCode.eventsList8(runtimeScene);

return;

}

gdjs['LevelCode'] = gdjs.LevelCode;
