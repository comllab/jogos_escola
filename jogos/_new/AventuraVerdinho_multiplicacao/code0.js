gdjs.StartCode = {};
gdjs.StartCode.GDfadeObjects1= [];
gdjs.StartCode.GDfadeObjects2= [];
gdjs.StartCode.GDtituloObjects1= [];
gdjs.StartCode.GDtituloObjects2= [];
gdjs.StartCode.GDadi_95231_95227oObjects1= [];
gdjs.StartCode.GDadi_95231_95227oObjects2= [];
gdjs.StartCode.GDverdinhoObjects1= [];
gdjs.StartCode.GDverdinhoObjects2= [];
gdjs.StartCode.GDbtJogarObjects1= [];
gdjs.StartCode.GDbtJogarObjects2= [];
gdjs.StartCode.GDtxtJogarObjects1= [];
gdjs.StartCode.GDtxtJogarObjects2= [];
gdjs.StartCode.GDckNivel_95951Objects1= [];
gdjs.StartCode.GDckNivel_95951Objects2= [];
gdjs.StartCode.GDckNivel_95952Objects1= [];
gdjs.StartCode.GDckNivel_95952Objects2= [];
gdjs.StartCode.GDckNivel_95953Objects1= [];
gdjs.StartCode.GDckNivel_95953Objects2= [];
gdjs.StartCode.GDckNivel_95954Objects1= [];
gdjs.StartCode.GDckNivel_95954Objects2= [];
gdjs.StartCode.GDckNivel_95955Objects1= [];
gdjs.StartCode.GDckNivel_95955Objects2= [];
gdjs.StartCode.GDckNivel_95956Objects1= [];
gdjs.StartCode.GDckNivel_95956Objects2= [];
gdjs.StartCode.GDckNivel_95957Objects1= [];
gdjs.StartCode.GDckNivel_95957Objects2= [];
gdjs.StartCode.GDckNivel_95958Objects1= [];
gdjs.StartCode.GDckNivel_95958Objects2= [];
gdjs.StartCode.GDtxtNivel_95951Objects1= [];
gdjs.StartCode.GDtxtNivel_95951Objects2= [];
gdjs.StartCode.GDtxtNivel_95952Objects1= [];
gdjs.StartCode.GDtxtNivel_95952Objects2= [];
gdjs.StartCode.GDtxtNivel_95953Objects1= [];
gdjs.StartCode.GDtxtNivel_95953Objects2= [];
gdjs.StartCode.GDtxtNivel_95954Objects1= [];
gdjs.StartCode.GDtxtNivel_95954Objects2= [];
gdjs.StartCode.GDtxtNivel_95955Objects1= [];
gdjs.StartCode.GDtxtNivel_95955Objects2= [];
gdjs.StartCode.GDtxtNivel_95956Objects1= [];
gdjs.StartCode.GDtxtNivel_95956Objects2= [];
gdjs.StartCode.GDtxtNivel_95957Objects1= [];
gdjs.StartCode.GDtxtNivel_95957Objects2= [];
gdjs.StartCode.GDtxtNivel_95958Objects1= [];
gdjs.StartCode.GDtxtNivel_95958Objects2= [];


gdjs.StartCode.mapOfGDgdjs_9546StartCode_9546GDbtJogarObjects1Objects = Hashtable.newFrom({"btJogar": gdjs.StartCode.GDbtJogarObjects1});
gdjs.StartCode.mapOfGDgdjs_9546StartCode_9546GDckNivel_959595951Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595952Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595953Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595954Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595955Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595956Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595957Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595958Objects1Objects = Hashtable.newFrom({"ckNivel_1": gdjs.StartCode.GDckNivel_95951Objects1, "ckNivel_2": gdjs.StartCode.GDckNivel_95952Objects1, "ckNivel_3": gdjs.StartCode.GDckNivel_95953Objects1, "ckNivel_4": gdjs.StartCode.GDckNivel_95954Objects1, "ckNivel_5": gdjs.StartCode.GDckNivel_95955Objects1, "ckNivel_6": gdjs.StartCode.GDckNivel_95956Objects1, "ckNivel_7": gdjs.StartCode.GDckNivel_95957Objects1, "ckNivel_8": gdjs.StartCode.GDckNivel_95958Objects1});
gdjs.StartCode.eventsList0 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "Mushroom Theme.ogg", true, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("ckNivel_1"), gdjs.StartCode.GDckNivel_95951Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_2"), gdjs.StartCode.GDckNivel_95952Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_3"), gdjs.StartCode.GDckNivel_95953Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_4"), gdjs.StartCode.GDckNivel_95954Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_5"), gdjs.StartCode.GDckNivel_95955Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_6"), gdjs.StartCode.GDckNivel_95956Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_7"), gdjs.StartCode.GDckNivel_95957Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_8"), gdjs.StartCode.GDckNivel_95958Objects1);
{for(var i = 0, len = gdjs.StartCode.GDckNivel_95951Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95951Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_95952Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95952Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_95953Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95953Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_95954Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95954Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_95955Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95955Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_95956Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95956Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_95957Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95957Objects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.StartCode.GDckNivel_95958Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95958Objects1[i].setAnimation(0);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("ckNivel_1"), gdjs.StartCode.GDckNivel_95951Objects1);
{for(var i = 0, len = gdjs.StartCode.GDckNivel_95951Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95951Objects1[i].setAnimation(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 2;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("ckNivel_2"), gdjs.StartCode.GDckNivel_95952Objects1);
{for(var i = 0, len = gdjs.StartCode.GDckNivel_95952Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95952Objects1[i].setAnimation(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("ckNivel_3"), gdjs.StartCode.GDckNivel_95953Objects1);
{for(var i = 0, len = gdjs.StartCode.GDckNivel_95953Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95953Objects1[i].setAnimation(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 4;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("ckNivel_4"), gdjs.StartCode.GDckNivel_95954Objects1);
{for(var i = 0, len = gdjs.StartCode.GDckNivel_95954Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95954Objects1[i].setAnimation(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("ckNivel_5"), gdjs.StartCode.GDckNivel_95955Objects1);
{for(var i = 0, len = gdjs.StartCode.GDckNivel_95955Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95955Objects1[i].setAnimation(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 6;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("ckNivel_6"), gdjs.StartCode.GDckNivel_95956Objects1);
{for(var i = 0, len = gdjs.StartCode.GDckNivel_95956Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95956Objects1[i].setAnimation(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 7;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("ckNivel_7"), gdjs.StartCode.GDckNivel_95957Objects1);
{for(var i = 0, len = gdjs.StartCode.GDckNivel_95957Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95957Objects1[i].setAnimation(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 8;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("ckNivel_8"), gdjs.StartCode.GDckNivel_95958Objects1);
{for(var i = 0, len = gdjs.StartCode.GDckNivel_95958Objects1.length ;i < len;++i) {
    gdjs.StartCode.GDckNivel_95958Objects1[i].setAnimation(1);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("btJogar"), gdjs.StartCode.GDbtJogarObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_9546StartCode_9546GDbtJogarObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("ckNivel_1"), gdjs.StartCode.GDckNivel_95951Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_2"), gdjs.StartCode.GDckNivel_95952Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_3"), gdjs.StartCode.GDckNivel_95953Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_4"), gdjs.StartCode.GDckNivel_95954Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_5"), gdjs.StartCode.GDckNivel_95955Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_6"), gdjs.StartCode.GDckNivel_95956Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_7"), gdjs.StartCode.GDckNivel_95957Objects1);
gdjs.copyArray(runtimeScene.getObjects("ckNivel_8"), gdjs.StartCode.GDckNivel_95958Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_9546StartCode_9546GDckNivel_959595951Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595952Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595953Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595954Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595955Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595956Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595957Objects1ObjectsGDgdjs_9546StartCode_9546GDckNivel_959595958Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9541476);
}
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.StartCode.GDckNivel_95951Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_95952Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_95953Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_95954Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_95955Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_95956Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_95957Objects1 */
/* Reuse gdjs.StartCode.GDckNivel_95958Objects1 */
{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.StartCode.GDckNivel_95958Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_95957Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_95956Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_95955Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_95954Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_95953Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_95952Objects1.length === 0 ) ? ((gdjs.StartCode.GDckNivel_95951Objects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.StartCode.GDckNivel_95951Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_95952Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_95953Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_95954Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_95955Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_95956Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_95957Objects1[0].getVariables()) : gdjs.StartCode.GDckNivel_95958Objects1[0].getVariables()).get("nivel"))));
}}

}


};

gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.StartCode.GDfadeObjects1.length = 0;
gdjs.StartCode.GDfadeObjects2.length = 0;
gdjs.StartCode.GDtituloObjects1.length = 0;
gdjs.StartCode.GDtituloObjects2.length = 0;
gdjs.StartCode.GDadi_95231_95227oObjects1.length = 0;
gdjs.StartCode.GDadi_95231_95227oObjects2.length = 0;
gdjs.StartCode.GDverdinhoObjects1.length = 0;
gdjs.StartCode.GDverdinhoObjects2.length = 0;
gdjs.StartCode.GDbtJogarObjects1.length = 0;
gdjs.StartCode.GDbtJogarObjects2.length = 0;
gdjs.StartCode.GDtxtJogarObjects1.length = 0;
gdjs.StartCode.GDtxtJogarObjects2.length = 0;
gdjs.StartCode.GDckNivel_95951Objects1.length = 0;
gdjs.StartCode.GDckNivel_95951Objects2.length = 0;
gdjs.StartCode.GDckNivel_95952Objects1.length = 0;
gdjs.StartCode.GDckNivel_95952Objects2.length = 0;
gdjs.StartCode.GDckNivel_95953Objects1.length = 0;
gdjs.StartCode.GDckNivel_95953Objects2.length = 0;
gdjs.StartCode.GDckNivel_95954Objects1.length = 0;
gdjs.StartCode.GDckNivel_95954Objects2.length = 0;
gdjs.StartCode.GDckNivel_95955Objects1.length = 0;
gdjs.StartCode.GDckNivel_95955Objects2.length = 0;
gdjs.StartCode.GDckNivel_95956Objects1.length = 0;
gdjs.StartCode.GDckNivel_95956Objects2.length = 0;
gdjs.StartCode.GDckNivel_95957Objects1.length = 0;
gdjs.StartCode.GDckNivel_95957Objects2.length = 0;
gdjs.StartCode.GDckNivel_95958Objects1.length = 0;
gdjs.StartCode.GDckNivel_95958Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_95951Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_95951Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_95952Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_95952Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_95953Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_95953Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_95954Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_95954Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_95955Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_95955Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_95956Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_95956Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_95957Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_95957Objects2.length = 0;
gdjs.StartCode.GDtxtNivel_95958Objects1.length = 0;
gdjs.StartCode.GDtxtNivel_95958Objects2.length = 0;

gdjs.StartCode.eventsList0(runtimeScene);

return;

}

gdjs['StartCode'] = gdjs.StartCode;
