gdjs.StartCode = {};
gdjs.StartCode.GDbtStartObjects1_1final = [];

gdjs.StartCode.GDstartObjects1_1final = [];

gdjs.StartCode.GDimagensObjects1= [];
gdjs.StartCode.GDimagensObjects2= [];
gdjs.StartCode.GDstartObjects1= [];
gdjs.StartCode.GDstartObjects2= [];
gdjs.StartCode.GDbtStartObjects1= [];
gdjs.StartCode.GDbtStartObjects2= [];
gdjs.StartCode.GDlblStartObjects1= [];
gdjs.StartCode.GDlblStartObjects2= [];
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDwwwObjects1= [];
gdjs.StartCode.GDwwwObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtStartObjects2Objects = Hashtable.newFrom({"btStart": gdjs.StartCode.GDbtStartObjects2});gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDstartObjects2Objects = Hashtable.newFrom({"start": gdjs.StartCode.GDstartObjects2});gdjs.StartCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDbtStartObjects1.createFrom(runtimeScene.getObjects("btStart"));
gdjs.StartCode.GDimagensObjects1.createFrom(runtimeScene.getObjects("imagens"));
gdjs.StartCode.GDlblStartObjects1.createFrom(runtimeScene.getObjects("lblStart"));
gdjs.StartCode.GDstartObjects1.createFrom(runtimeScene.getObjects("start"));
gdjs.StartCode.GDwwwObjects1.createFrom(runtimeScene.getObjects("www"));
{for(var i = 0, len = gdjs.StartCode.GDstartObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDstartObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.StartCode.GDstartObjects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.StartCode.GDwwwObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDwwwObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.StartCode.GDwwwObjects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.StartCode.GDimagensObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDimagensObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.StartCode.GDimagensObjects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.StartCode.GDbtStartObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDbtStartObjects1[i].setPosition(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.StartCode.GDbtStartObjects1[i].getWidth())/2,(( gdjs.StartCode.GDstartObjects1.length === 0 ) ? 0 :gdjs.StartCode.GDstartObjects1[0].getPointY("")) + (( gdjs.StartCode.GDstartObjects1.length === 0 ) ? 0 :gdjs.StartCode.GDstartObjects1[0].getHeight()) + 5);
}
}{for(var i = 0, len = gdjs.StartCode.GDlblStartObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDlblStartObjects1[i].setPosition(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.StartCode.GDlblStartObjects1[i].getWidth())/2,(( gdjs.StartCode.GDbtStartObjects1.length === 0 ) ? 0 :gdjs.StartCode.GDbtStartObjects1[0].getPointY("")) + (gdjs.StartCode.GDlblStartObjects1[i].getHeight())/2);
}
}}

}


{

gdjs.StartCode.GDbtStartObjects1.length = 0;

gdjs.StartCode.GDstartObjects1.length = 0;


gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition1IsTrue_0;
gdjs.StartCode.GDbtStartObjects1_1final.length = 0;gdjs.StartCode.GDstartObjects1_1final.length = 0;gdjs.StartCode.condition0IsTrue_1.val = false;
gdjs.StartCode.condition1IsTrue_1.val = false;
{
gdjs.StartCode.GDbtStartObjects2.createFrom(runtimeScene.getObjects("btStart"));
gdjs.StartCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtStartObjects2Objects, runtimeScene, true, false);
if( gdjs.StartCode.condition0IsTrue_1.val ) {
    gdjs.StartCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.StartCode.GDbtStartObjects2.length;j<jLen;++j) {
        if ( gdjs.StartCode.GDbtStartObjects1_1final.indexOf(gdjs.StartCode.GDbtStartObjects2[j]) === -1 )
            gdjs.StartCode.GDbtStartObjects1_1final.push(gdjs.StartCode.GDbtStartObjects2[j]);
    }
}
}
{
gdjs.StartCode.GDstartObjects2.createFrom(runtimeScene.getObjects("start"));
gdjs.StartCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDstartObjects2Objects, runtimeScene, true, false);
if( gdjs.StartCode.condition1IsTrue_1.val ) {
    gdjs.StartCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.StartCode.GDstartObjects2.length;j<jLen;++j) {
        if ( gdjs.StartCode.GDstartObjects1_1final.indexOf(gdjs.StartCode.GDstartObjects2[j]) === -1 )
            gdjs.StartCode.GDstartObjects1_1final.push(gdjs.StartCode.GDstartObjects2[j]);
    }
}
}
{
gdjs.StartCode.GDbtStartObjects1.createFrom(gdjs.StartCode.GDbtStartObjects1_1final);
gdjs.StartCode.GDstartObjects1.createFrom(gdjs.StartCode.GDstartObjects1_1final);
}
}
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MainScene", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xaff48


gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.StartCode.GDimagensObjects1.length = 0;
gdjs.StartCode.GDimagensObjects2.length = 0;
gdjs.StartCode.GDstartObjects1.length = 0;
gdjs.StartCode.GDstartObjects2.length = 0;
gdjs.StartCode.GDbtStartObjects1.length = 0;
gdjs.StartCode.GDbtStartObjects2.length = 0;
gdjs.StartCode.GDlblStartObjects1.length = 0;
gdjs.StartCode.GDlblStartObjects2.length = 0;
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDwwwObjects1.length = 0;
gdjs.StartCode.GDwwwObjects2.length = 0;

gdjs.StartCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['StartCode'] = gdjs.StartCode;
