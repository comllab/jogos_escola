gdjs.ResultadoCode = {};
gdjs.ResultadoCode.GDbackgroundObjects1= [];
gdjs.ResultadoCode.GDbackgroundObjects2= [];
gdjs.ResultadoCode.GDsimuladoObjects1= [];
gdjs.ResultadoCode.GDsimuladoObjects2= [];
gdjs.ResultadoCode.GDpaginaObjects1= [];
gdjs.ResultadoCode.GDpaginaObjects2= [];
gdjs.ResultadoCode.GDdisciplinaObjects1= [];
gdjs.ResultadoCode.GDdisciplinaObjects2= [];
gdjs.ResultadoCode.GDtituloObjects1= [];
gdjs.ResultadoCode.GDtituloObjects2= [];
gdjs.ResultadoCode.GDpainelQuestaoObjects1= [];
gdjs.ResultadoCode.GDpainelQuestaoObjects2= [];
gdjs.ResultadoCode.GDfadeObjects1= [];
gdjs.ResultadoCode.GDfadeObjects2= [];
gdjs.ResultadoCode.GDdialog_95respObjects1= [];
gdjs.ResultadoCode.GDdialog_95respObjects2= [];
gdjs.ResultadoCode.GDbtCorrigirQuestaoObjects1= [];
gdjs.ResultadoCode.GDbtCorrigirQuestaoObjects2= [];
gdjs.ResultadoCode.GDbtProximaQuestaoObjects1= [];
gdjs.ResultadoCode.GDbtProximaQuestaoObjects2= [];
gdjs.ResultadoCode.GDbtQuestaoFundoObjects1= [];
gdjs.ResultadoCode.GDbtQuestaoFundoObjects2= [];
gdjs.ResultadoCode.GDlblQuestaoObjects1= [];
gdjs.ResultadoCode.GDlblQuestaoObjects2= [];
gdjs.ResultadoCode.GDlblProximaObjects1= [];
gdjs.ResultadoCode.GDlblProximaObjects2= [];
gdjs.ResultadoCode.GDlblCorrigirObjects1= [];
gdjs.ResultadoCode.GDlblCorrigirObjects2= [];
gdjs.ResultadoCode.GDresultadoObjects1= [];
gdjs.ResultadoCode.GDresultadoObjects2= [];
gdjs.ResultadoCode.GDacertosObjects1= [];
gdjs.ResultadoCode.GDacertosObjects2= [];
gdjs.ResultadoCode.GDpercentual_95acertosObjects1= [];
gdjs.ResultadoCode.GDpercentual_95acertosObjects2= [];
gdjs.ResultadoCode.GDerrosObjects1= [];
gdjs.ResultadoCode.GDerrosObjects2= [];
gdjs.ResultadoCode.GDtotal_95questoesObjects1= [];
gdjs.ResultadoCode.GDtotal_95questoesObjects2= [];
gdjs.ResultadoCode.GDporcentagemObjects1= [];
gdjs.ResultadoCode.GDporcentagemObjects2= [];
gdjs.ResultadoCode.GDbtRefazerObjects1= [];
gdjs.ResultadoCode.GDbtRefazerObjects2= [];
gdjs.ResultadoCode.GDlblRefazerObjects1= [];
gdjs.ResultadoCode.GDlblRefazerObjects2= [];
gdjs.ResultadoCode.GDfundoPorcentagemObjects1= [];
gdjs.ResultadoCode.GDfundoPorcentagemObjects2= [];

gdjs.ResultadoCode.conditionTrue_0 = {val:false};
gdjs.ResultadoCode.condition0IsTrue_0 = {val:false};
gdjs.ResultadoCode.condition1IsTrue_0 = {val:false};
gdjs.ResultadoCode.condition2IsTrue_0 = {val:false};


gdjs.ResultadoCode.eventsList0x7de938 = function(runtimeScene, context) {

{


{
gdjs.ResultadoCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));
{for(var i = 0, len = gdjs.ResultadoCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.ResultadoCode.GDfadeObjects1[i].setPosition(0,0);
}
}{for(var i = 0, len = gdjs.ResultadoCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.ResultadoCode.GDfadeObjects1[i].setScale(25);
}
}{for(var i = 0, len = gdjs.ResultadoCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.ResultadoCode.GDfadeObjects1[i].setOpacity(255);
}
}}

}


}; //End of gdjs.ResultadoCode.eventsList0x7de938
gdjs.ResultadoCode.mapOfGDgdjs_46ResultadoCode_46GDbtRefazerObjects1Objects = Hashtable.newFrom({"btRefazer": gdjs.ResultadoCode.GDbtRefazerObjects1});gdjs.ResultadoCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.ResultadoCode.condition0IsTrue_0.val = false;
{
gdjs.ResultadoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.ResultadoCode.condition0IsTrue_0.val) {
gdjs.ResultadoCode.GDacertosObjects1.createFrom(runtimeScene.getObjects("acertos"));
gdjs.ResultadoCode.GDerrosObjects1.createFrom(runtimeScene.getObjects("erros"));
gdjs.ResultadoCode.GDfundoPorcentagemObjects1.createFrom(runtimeScene.getObjects("fundoPorcentagem"));
gdjs.ResultadoCode.GDporcentagemObjects1.createFrom(runtimeScene.getObjects("porcentagem"));
gdjs.ResultadoCode.GDtotal_95questoesObjects1.createFrom(runtimeScene.getObjects("total_questoes"));
{for(var i = 0, len = gdjs.ResultadoCode.GDtotal_95questoesObjects1.length ;i < len;++i) {
    gdjs.ResultadoCode.GDtotal_95questoesObjects1[i].setString("Total de questões: " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)));
}
}{for(var i = 0, len = gdjs.ResultadoCode.GDerrosObjects1.length ;i < len;++i) {
    gdjs.ResultadoCode.GDerrosObjects1[i].setString("Total de erros: " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}{for(var i = 0, len = gdjs.ResultadoCode.GDacertosObjects1.length ;i < len;++i) {
    gdjs.ResultadoCode.GDacertosObjects1[i].setString("Total de acertos: " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) - gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}{for(var i = 0, len = gdjs.ResultadoCode.GDporcentagemObjects1.length ;i < len;++i) {
    gdjs.ResultadoCode.GDporcentagemObjects1[i].setString(gdjs.evtTools.common.toString(Math.round((gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2))-gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)))/gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2))*100)) + "%");
}
}{for(var i = 0, len = gdjs.ResultadoCode.GDporcentagemObjects1.length ;i < len;++i) {
    gdjs.ResultadoCode.GDporcentagemObjects1[i].setX((( gdjs.ResultadoCode.GDfundoPorcentagemObjects1.length === 0 ) ? 0 :gdjs.ResultadoCode.GDfundoPorcentagemObjects1[0].getPointX("")) + (( gdjs.ResultadoCode.GDfundoPorcentagemObjects1.length === 0 ) ? 0 :gdjs.ResultadoCode.GDfundoPorcentagemObjects1[0].getWidth())/2 - (gdjs.ResultadoCode.GDporcentagemObjects1[i].getWidth())/2);
}
}
{ //Subevents
gdjs.ResultadoCode.eventsList0x7de938(runtimeScene, context);} //End of subevents
}

}


{

gdjs.ResultadoCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));

gdjs.ResultadoCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ResultadoCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.ResultadoCode.GDfadeObjects1[i].getOpacity() > 0 ) {
        gdjs.ResultadoCode.condition0IsTrue_0.val = true;
        gdjs.ResultadoCode.GDfadeObjects1[k] = gdjs.ResultadoCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.ResultadoCode.GDfadeObjects1.length = k;}if (gdjs.ResultadoCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ResultadoCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.ResultadoCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.ResultadoCode.GDfadeObjects1[i].setOpacity(gdjs.ResultadoCode.GDfadeObjects1[i].getOpacity() - (255 * gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{

gdjs.ResultadoCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));

gdjs.ResultadoCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ResultadoCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.ResultadoCode.GDfadeObjects1[i].getOpacity() <= 0 ) {
        gdjs.ResultadoCode.condition0IsTrue_0.val = true;
        gdjs.ResultadoCode.GDfadeObjects1[k] = gdjs.ResultadoCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.ResultadoCode.GDfadeObjects1.length = k;}if (gdjs.ResultadoCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ResultadoCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.ResultadoCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.ResultadoCode.GDfadeObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.ResultadoCode.GDbtRefazerObjects1.createFrom(runtimeScene.getObjects("btRefazer"));

gdjs.ResultadoCode.condition0IsTrue_0.val = false;
gdjs.ResultadoCode.condition1IsTrue_0.val = false;
{
gdjs.ResultadoCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ResultadoCode.mapOfGDgdjs_46ResultadoCode_46GDbtRefazerObjects1Objects, runtimeScene, true, false);
}if ( gdjs.ResultadoCode.condition0IsTrue_0.val ) {
{
gdjs.ResultadoCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.ResultadoCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Simulado", false);
}}

}


}; //End of gdjs.ResultadoCode.eventsList0xaf630


gdjs.ResultadoCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.ResultadoCode.GDbackgroundObjects1.length = 0;
gdjs.ResultadoCode.GDbackgroundObjects2.length = 0;
gdjs.ResultadoCode.GDsimuladoObjects1.length = 0;
gdjs.ResultadoCode.GDsimuladoObjects2.length = 0;
gdjs.ResultadoCode.GDpaginaObjects1.length = 0;
gdjs.ResultadoCode.GDpaginaObjects2.length = 0;
gdjs.ResultadoCode.GDdisciplinaObjects1.length = 0;
gdjs.ResultadoCode.GDdisciplinaObjects2.length = 0;
gdjs.ResultadoCode.GDtituloObjects1.length = 0;
gdjs.ResultadoCode.GDtituloObjects2.length = 0;
gdjs.ResultadoCode.GDpainelQuestaoObjects1.length = 0;
gdjs.ResultadoCode.GDpainelQuestaoObjects2.length = 0;
gdjs.ResultadoCode.GDfadeObjects1.length = 0;
gdjs.ResultadoCode.GDfadeObjects2.length = 0;
gdjs.ResultadoCode.GDdialog_95respObjects1.length = 0;
gdjs.ResultadoCode.GDdialog_95respObjects2.length = 0;
gdjs.ResultadoCode.GDbtCorrigirQuestaoObjects1.length = 0;
gdjs.ResultadoCode.GDbtCorrigirQuestaoObjects2.length = 0;
gdjs.ResultadoCode.GDbtProximaQuestaoObjects1.length = 0;
gdjs.ResultadoCode.GDbtProximaQuestaoObjects2.length = 0;
gdjs.ResultadoCode.GDbtQuestaoFundoObjects1.length = 0;
gdjs.ResultadoCode.GDbtQuestaoFundoObjects2.length = 0;
gdjs.ResultadoCode.GDlblQuestaoObjects1.length = 0;
gdjs.ResultadoCode.GDlblQuestaoObjects2.length = 0;
gdjs.ResultadoCode.GDlblProximaObjects1.length = 0;
gdjs.ResultadoCode.GDlblProximaObjects2.length = 0;
gdjs.ResultadoCode.GDlblCorrigirObjects1.length = 0;
gdjs.ResultadoCode.GDlblCorrigirObjects2.length = 0;
gdjs.ResultadoCode.GDresultadoObjects1.length = 0;
gdjs.ResultadoCode.GDresultadoObjects2.length = 0;
gdjs.ResultadoCode.GDacertosObjects1.length = 0;
gdjs.ResultadoCode.GDacertosObjects2.length = 0;
gdjs.ResultadoCode.GDpercentual_95acertosObjects1.length = 0;
gdjs.ResultadoCode.GDpercentual_95acertosObjects2.length = 0;
gdjs.ResultadoCode.GDerrosObjects1.length = 0;
gdjs.ResultadoCode.GDerrosObjects2.length = 0;
gdjs.ResultadoCode.GDtotal_95questoesObjects1.length = 0;
gdjs.ResultadoCode.GDtotal_95questoesObjects2.length = 0;
gdjs.ResultadoCode.GDporcentagemObjects1.length = 0;
gdjs.ResultadoCode.GDporcentagemObjects2.length = 0;
gdjs.ResultadoCode.GDbtRefazerObjects1.length = 0;
gdjs.ResultadoCode.GDbtRefazerObjects2.length = 0;
gdjs.ResultadoCode.GDlblRefazerObjects1.length = 0;
gdjs.ResultadoCode.GDlblRefazerObjects2.length = 0;
gdjs.ResultadoCode.GDfundoPorcentagemObjects1.length = 0;
gdjs.ResultadoCode.GDfundoPorcentagemObjects2.length = 0;

gdjs.ResultadoCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['ResultadoCode']= gdjs.ResultadoCode;
