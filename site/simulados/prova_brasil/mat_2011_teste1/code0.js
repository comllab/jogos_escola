gdjs.SimuladoCode = {};
gdjs.SimuladoCode.GDbackgroundObjects1= [];
gdjs.SimuladoCode.GDbackgroundObjects2= [];
gdjs.SimuladoCode.GDsimuladoObjects1= [];
gdjs.SimuladoCode.GDsimuladoObjects2= [];
gdjs.SimuladoCode.GDpaginaObjects1= [];
gdjs.SimuladoCode.GDpaginaObjects2= [];
gdjs.SimuladoCode.GDdisciplinaObjects1= [];
gdjs.SimuladoCode.GDdisciplinaObjects2= [];
gdjs.SimuladoCode.GDtituloObjects1= [];
gdjs.SimuladoCode.GDtituloObjects2= [];
gdjs.SimuladoCode.GDpainelQuestaoObjects1= [];
gdjs.SimuladoCode.GDpainelQuestaoObjects2= [];
gdjs.SimuladoCode.GDfadeObjects1= [];
gdjs.SimuladoCode.GDfadeObjects2= [];
gdjs.SimuladoCode.GDdialog_95respObjects1= [];
gdjs.SimuladoCode.GDdialog_95respObjects2= [];
gdjs.SimuladoCode.GDbtCorrigirQuestaoObjects1= [];
gdjs.SimuladoCode.GDbtCorrigirQuestaoObjects2= [];
gdjs.SimuladoCode.GDbtProximaQuestaoObjects1= [];
gdjs.SimuladoCode.GDbtProximaQuestaoObjects2= [];
gdjs.SimuladoCode.GDbtQuestaoFundoObjects1= [];
gdjs.SimuladoCode.GDbtQuestaoFundoObjects2= [];
gdjs.SimuladoCode.GDlblQuestaoObjects1= [];
gdjs.SimuladoCode.GDlblQuestaoObjects2= [];
gdjs.SimuladoCode.GDlblProximaObjects1= [];
gdjs.SimuladoCode.GDlblProximaObjects2= [];
gdjs.SimuladoCode.GDlblCorrigirObjects1= [];
gdjs.SimuladoCode.GDlblCorrigirObjects2= [];
gdjs.SimuladoCode.GDbackgroundObjects1= [];
gdjs.SimuladoCode.GDbackgroundObjects2= [];
gdjs.SimuladoCode.GDsimuladoObjects1= [];
gdjs.SimuladoCode.GDsimuladoObjects2= [];
gdjs.SimuladoCode.GDdisciplinaObjects1= [];
gdjs.SimuladoCode.GDdisciplinaObjects2= [];
gdjs.SimuladoCode.GDtituloObjects1= [];
gdjs.SimuladoCode.GDtituloObjects2= [];
gdjs.SimuladoCode.GDbtIniciarObjects1= [];
gdjs.SimuladoCode.GDbtIniciarObjects2= [];
gdjs.SimuladoCode.GDlblIniciarObjects1= [];
gdjs.SimuladoCode.GDlblIniciarObjects2= [];

gdjs.SimuladoCode.conditionTrue_0 = {val:false};
gdjs.SimuladoCode.condition0IsTrue_0 = {val:false};
gdjs.SimuladoCode.condition1IsTrue_0 = {val:false};
gdjs.SimuladoCode.condition2IsTrue_0 = {val:false};


gdjs.SimuladoCode.eventsList0x92bb68 = function(runtimeScene, context) {

{


{
gdjs.SimuladoCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));
{for(var i = 0, len = gdjs.SimuladoCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.SimuladoCode.GDfadeObjects1[i].setPosition(0,0);
}
}{for(var i = 0, len = gdjs.SimuladoCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.SimuladoCode.GDfadeObjects1[i].setOpacity(255);
}
}}

}


}; //End of gdjs.SimuladoCode.eventsList0x92bb68
gdjs.SimuladoCode.mapOfGDgdjs_46SimuladoCode_46GDbtIniciarObjects1Objects = Hashtable.newFrom({"btIniciar": gdjs.SimuladoCode.GDbtIniciarObjects1});gdjs.SimuladoCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.SimuladoCode.condition0IsTrue_0.val = false;
{
gdjs.SimuladoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.SimuladoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(1);
}
{ //Subevents
gdjs.SimuladoCode.eventsList0x92bb68(runtimeScene, context);} //End of subevents
}

}


{

gdjs.SimuladoCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));

gdjs.SimuladoCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SimuladoCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.SimuladoCode.GDfadeObjects1[i].getOpacity() > 0 ) {
        gdjs.SimuladoCode.condition0IsTrue_0.val = true;
        gdjs.SimuladoCode.GDfadeObjects1[k] = gdjs.SimuladoCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.SimuladoCode.GDfadeObjects1.length = k;}if (gdjs.SimuladoCode.condition0IsTrue_0.val) {
/* Reuse gdjs.SimuladoCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.SimuladoCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.SimuladoCode.GDfadeObjects1[i].setOpacity(gdjs.SimuladoCode.GDfadeObjects1[i].getOpacity() - (255*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{

gdjs.SimuladoCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));

gdjs.SimuladoCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SimuladoCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.SimuladoCode.GDfadeObjects1[i].getOpacity() == 0 ) {
        gdjs.SimuladoCode.condition0IsTrue_0.val = true;
        gdjs.SimuladoCode.GDfadeObjects1[k] = gdjs.SimuladoCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.SimuladoCode.GDfadeObjects1.length = k;}if (gdjs.SimuladoCode.condition0IsTrue_0.val) {
/* Reuse gdjs.SimuladoCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.SimuladoCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.SimuladoCode.GDfadeObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.SimuladoCode.GDbtIniciarObjects1.createFrom(runtimeScene.getObjects("btIniciar"));

gdjs.SimuladoCode.condition0IsTrue_0.val = false;
gdjs.SimuladoCode.condition1IsTrue_0.val = false;
{
gdjs.SimuladoCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SimuladoCode.mapOfGDgdjs_46SimuladoCode_46GDbtIniciarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.SimuladoCode.condition0IsTrue_0.val ) {
{
gdjs.SimuladoCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.SimuladoCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Questao_" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)), false);
}}

}


}; //End of gdjs.SimuladoCode.eventsList0xaf630


gdjs.SimuladoCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.SimuladoCode.GDbackgroundObjects1.length = 0;
gdjs.SimuladoCode.GDbackgroundObjects2.length = 0;
gdjs.SimuladoCode.GDsimuladoObjects1.length = 0;
gdjs.SimuladoCode.GDsimuladoObjects2.length = 0;
gdjs.SimuladoCode.GDpaginaObjects1.length = 0;
gdjs.SimuladoCode.GDpaginaObjects2.length = 0;
gdjs.SimuladoCode.GDdisciplinaObjects1.length = 0;
gdjs.SimuladoCode.GDdisciplinaObjects2.length = 0;
gdjs.SimuladoCode.GDtituloObjects1.length = 0;
gdjs.SimuladoCode.GDtituloObjects2.length = 0;
gdjs.SimuladoCode.GDpainelQuestaoObjects1.length = 0;
gdjs.SimuladoCode.GDpainelQuestaoObjects2.length = 0;
gdjs.SimuladoCode.GDfadeObjects1.length = 0;
gdjs.SimuladoCode.GDfadeObjects2.length = 0;
gdjs.SimuladoCode.GDdialog_95respObjects1.length = 0;
gdjs.SimuladoCode.GDdialog_95respObjects2.length = 0;
gdjs.SimuladoCode.GDbtCorrigirQuestaoObjects1.length = 0;
gdjs.SimuladoCode.GDbtCorrigirQuestaoObjects2.length = 0;
gdjs.SimuladoCode.GDbtProximaQuestaoObjects1.length = 0;
gdjs.SimuladoCode.GDbtProximaQuestaoObjects2.length = 0;
gdjs.SimuladoCode.GDbtQuestaoFundoObjects1.length = 0;
gdjs.SimuladoCode.GDbtQuestaoFundoObjects2.length = 0;
gdjs.SimuladoCode.GDlblQuestaoObjects1.length = 0;
gdjs.SimuladoCode.GDlblQuestaoObjects2.length = 0;
gdjs.SimuladoCode.GDlblProximaObjects1.length = 0;
gdjs.SimuladoCode.GDlblProximaObjects2.length = 0;
gdjs.SimuladoCode.GDlblCorrigirObjects1.length = 0;
gdjs.SimuladoCode.GDlblCorrigirObjects2.length = 0;
gdjs.SimuladoCode.GDbackgroundObjects1.length = 0;
gdjs.SimuladoCode.GDbackgroundObjects2.length = 0;
gdjs.SimuladoCode.GDsimuladoObjects1.length = 0;
gdjs.SimuladoCode.GDsimuladoObjects2.length = 0;
gdjs.SimuladoCode.GDdisciplinaObjects1.length = 0;
gdjs.SimuladoCode.GDdisciplinaObjects2.length = 0;
gdjs.SimuladoCode.GDtituloObjects1.length = 0;
gdjs.SimuladoCode.GDtituloObjects2.length = 0;
gdjs.SimuladoCode.GDbtIniciarObjects1.length = 0;
gdjs.SimuladoCode.GDbtIniciarObjects2.length = 0;
gdjs.SimuladoCode.GDlblIniciarObjects1.length = 0;
gdjs.SimuladoCode.GDlblIniciarObjects2.length = 0;

gdjs.SimuladoCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['SimuladoCode']= gdjs.SimuladoCode;
