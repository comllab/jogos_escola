gdjs.MainCode = {};
gdjs.MainCode.GDbackgroundObjects1= [];
gdjs.MainCode.GDbackgroundObjects2= [];
gdjs.MainCode.GDbackgroundObjects3= [];
gdjs.MainCode.GDbackgroundObjects4= [];
gdjs.MainCode.GDtrofeuObjects1= [];
gdjs.MainCode.GDtrofeuObjects2= [];
gdjs.MainCode.GDtrofeuObjects3= [];
gdjs.MainCode.GDtrofeuObjects4= [];
gdjs.MainCode.GDestrelaSequenciaObjects1= [];
gdjs.MainCode.GDestrelaSequenciaObjects2= [];
gdjs.MainCode.GDestrelaSequenciaObjects3= [];
gdjs.MainCode.GDestrelaSequenciaObjects4= [];
gdjs.MainCode.GDestrelaObjects1= [];
gdjs.MainCode.GDestrelaObjects2= [];
gdjs.MainCode.GDestrelaObjects3= [];
gdjs.MainCode.GDestrelaObjects4= [];
gdjs.MainCode.GDparabensObjects1= [];
gdjs.MainCode.GDparabensObjects2= [];
gdjs.MainCode.GDparabensObjects3= [];
gdjs.MainCode.GDparabensObjects4= [];
gdjs.MainCode.GDpecasObjects1= [];
gdjs.MainCode.GDpecasObjects2= [];
gdjs.MainCode.GDpecasObjects3= [];
gdjs.MainCode.GDpecasObjects4= [];
gdjs.MainCode.GDbtConferirObjects1= [];
gdjs.MainCode.GDbtConferirObjects2= [];
gdjs.MainCode.GDbtConferirObjects3= [];
gdjs.MainCode.GDbtConferirObjects4= [];
gdjs.MainCode.GDenunciadoObjects1= [];
gdjs.MainCode.GDenunciadoObjects2= [];
gdjs.MainCode.GDenunciadoObjects3= [];
gdjs.MainCode.GDenunciadoObjects4= [];
gdjs.MainCode.GDlblTrofeusObjects1= [];
gdjs.MainCode.GDlblTrofeusObjects2= [];
gdjs.MainCode.GDlblTrofeusObjects3= [];
gdjs.MainCode.GDlblTrofeusObjects4= [];
gdjs.MainCode.GDupObjects1= [];
gdjs.MainCode.GDupObjects2= [];
gdjs.MainCode.GDupObjects3= [];
gdjs.MainCode.GDupObjects4= [];
gdjs.MainCode.GDdownObjects1= [];
gdjs.MainCode.GDdownObjects2= [];
gdjs.MainCode.GDdownObjects3= [];
gdjs.MainCode.GDdownObjects4= [];
gdjs.MainCode.GDtente_95novamenteObjects1= [];
gdjs.MainCode.GDtente_95novamenteObjects2= [];
gdjs.MainCode.GDtente_95novamenteObjects3= [];
gdjs.MainCode.GDtente_95novamenteObjects4= [];
gdjs.MainCode.GDpanelInferiorObjects1= [];
gdjs.MainCode.GDpanelInferiorObjects2= [];
gdjs.MainCode.GDpanelInferiorObjects3= [];
gdjs.MainCode.GDpanelInferiorObjects4= [];
gdjs.MainCode.GDpanelSequenciaObjects1= [];
gdjs.MainCode.GDpanelSequenciaObjects2= [];
gdjs.MainCode.GDpanelSequenciaObjects3= [];
gdjs.MainCode.GDpanelSequenciaObjects4= [];
gdjs.MainCode.GDtrofeusObjects1= [];
gdjs.MainCode.GDtrofeusObjects2= [];
gdjs.MainCode.GDtrofeusObjects3= [];
gdjs.MainCode.GDtrofeusObjects4= [];
gdjs.MainCode.GDerrosObjects1= [];
gdjs.MainCode.GDerrosObjects2= [];
gdjs.MainCode.GDerrosObjects3= [];
gdjs.MainCode.GDerrosObjects4= [];

gdjs.MainCode.conditionTrue_0 = {val:false};
gdjs.MainCode.condition0IsTrue_0 = {val:false};
gdjs.MainCode.condition1IsTrue_0 = {val:false};
gdjs.MainCode.condition2IsTrue_0 = {val:false};
gdjs.MainCode.condition3IsTrue_0 = {val:false};
gdjs.MainCode.condition4IsTrue_0 = {val:false};
gdjs.MainCode.conditionTrue_1 = {val:false};
gdjs.MainCode.condition0IsTrue_1 = {val:false};
gdjs.MainCode.condition1IsTrue_1 = {val:false};
gdjs.MainCode.condition2IsTrue_1 = {val:false};
gdjs.MainCode.condition3IsTrue_1 = {val:false};
gdjs.MainCode.condition4IsTrue_1 = {val:false};


gdjs.MainCode.eventsList0x6b4aa8 = function(runtimeScene) {

{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(4)) == 180;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDpecasObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDpecasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDpecasObjects2[i].setAngle(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(4)));
}
}}

}


}; //End of gdjs.MainCode.eventsList0x6b4aa8
gdjs.MainCode.eventsList0x6b51a0 = function(runtimeScene) {

{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(5)) == 180;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDpecasObjects1 */
{for(var i = 0, len = gdjs.MainCode.GDpecasObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDpecasObjects1[i].setAngle(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(5)));
}
}}

}


}; //End of gdjs.MainCode.eventsList0x6b51a0
gdjs.MainCode.eventsList0x6b2048 = function(runtimeScene) {

{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(3,  8));
}}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 2;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(4,  8));
}}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 3;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(5,  8));
}}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 4;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(6,  8));
}}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 5;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(7,  8));
}}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 6;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(1,  4));
}}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 7;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(1,  5));
}}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 8;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(1,  6));
}}

}


{

gdjs.MainCode.GDpecasObjects2.createFrom(runtimeScene.getObjects("pecas"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects2[i].getVariableNumber(gdjs.MainCode.GDpecasObjects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects2[k] = gdjs.MainCode.GDpecasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDpecasObjects2 */
{runtimeScene.getVariables().getFromIndex(2).setNumber((( gdjs.MainCode.GDpecasObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDpecasObjects2[0].getAnimation()));
}{for(var i = 0, len = gdjs.MainCode.GDpecasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDpecasObjects2[i].setAnimation(0);
}
}{runtimeScene.getVariables().getFromIndex(4).setNumber((( gdjs.MainCode.GDpecasObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDpecasObjects2[0].getAngle()));
}}

}


{

gdjs.MainCode.GDpecasObjects2.createFrom(runtimeScene.getObjects("pecas"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects2[i].getVariableNumber(gdjs.MainCode.GDpecasObjects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects2[k] = gdjs.MainCode.GDpecasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDpecasObjects2 */
{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.MainCode.GDpecasObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDpecasObjects2[0].getAnimation()));
}{for(var i = 0, len = gdjs.MainCode.GDpecasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDpecasObjects2[i].setAnimation(0);
}
}{runtimeScene.getVariables().getFromIndex(5).setNumber((( gdjs.MainCode.GDpecasObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDpecasObjects2[0].getAngle()));
}}

}


{

gdjs.MainCode.GDpecasObjects2.createFrom(runtimeScene.getObjects("pecas"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects2[i].getVariableNumber(gdjs.MainCode.GDpecasObjects2[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("num")) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects2[k] = gdjs.MainCode.GDpecasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDpecasObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDpecasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDpecasObjects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)));
}
}{runtimeScene.getVariables().get("num").add(3);
}
{ //Subevents
gdjs.MainCode.eventsList0x6b4aa8(runtimeScene);} //End of subevents
}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("num")) > 5;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("num").setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("num")) - 5);
}}

}


{

gdjs.MainCode.GDpecasObjects1.createFrom(runtimeScene.getObjects("pecas"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects1.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects1[i].getVariableNumber(gdjs.MainCode.GDpecasObjects1[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("num")) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects1[k] = gdjs.MainCode.GDpecasObjects1[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects1.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDpecasObjects1 */
{for(var i = 0, len = gdjs.MainCode.GDpecasObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDpecasObjects1[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}
{ //Subevents
gdjs.MainCode.eventsList0x6b51a0(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.MainCode.eventsList0x6b2048
gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDpecasObjects2Objects = Hashtable.newFrom({"pecas": gdjs.MainCode.GDpecasObjects2});gdjs.MainCode.eventsList0x6b56f8 = function(runtimeScene) {

{

gdjs.MainCode.GDpecasObjects3.createFrom(gdjs.MainCode.GDpecasObjects2);


gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects3.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects3[i].getAnimation() == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects3[k] = gdjs.MainCode.GDpecasObjects3[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects3.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(6).setNumber(1);
}}

}


{

gdjs.MainCode.GDpecasObjects3.createFrom(gdjs.MainCode.GDpecasObjects2);


gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects3.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects3[i].getAnimation() == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects3[k] = gdjs.MainCode.GDpecasObjects3[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects3.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(7).setNumber(1);
}}

}


{

/* Reuse gdjs.MainCode.GDpecasObjects2 */

gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects2[i].getAnimation() != gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects2[k] = gdjs.MainCode.GDpecasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects2.length = k;}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects2[i].getAnimation() != gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) ) {
        gdjs.MainCode.condition1IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects2[k] = gdjs.MainCode.GDpecasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects2.length = k;}}
if (gdjs.MainCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(8).add(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "erro.ogg", false, 100, 1);
}}

}


}; //End of gdjs.MainCode.eventsList0x6b56f8
gdjs.MainCode.eventsList0x6b6350 = function(runtimeScene) {

{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) > 0;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).sub(1);
}}

}


}; //End of gdjs.MainCode.eventsList0x6b6350
gdjs.MainCode.eventsList0x6b6c48 = function(runtimeScene) {

{

gdjs.MainCode.GDpecasObjects2.createFrom(runtimeScene.getObjects("pecas"));

gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects2[i].getVariableNumber(gdjs.MainCode.GDpecasObjects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects2[k] = gdjs.MainCode.GDpecasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects2.length = k;}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects2[i].getVariableNumber(gdjs.MainCode.GDpecasObjects2[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.MainCode.condition1IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects2[k] = gdjs.MainCode.GDpecasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects2.length = k;}}
if (gdjs.MainCode.condition1IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDpecasObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDpecasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDpecasObjects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)));
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise06.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.MainCode.GDpecasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDpecasObjects2[i].returnVariable(gdjs.MainCode.GDpecasObjects2[i].getVariables().getFromIndex(0)).setNumber(-1);
}
}}

}


}; //End of gdjs.MainCode.eventsList0x6b6c48
gdjs.MainCode.eventsList0x6b71f8 = function(runtimeScene) {

{

gdjs.MainCode.GDpecasObjects2.createFrom(runtimeScene.getObjects("pecas"));

gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects2[i].getVariableNumber(gdjs.MainCode.GDpecasObjects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects2[k] = gdjs.MainCode.GDpecasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects2.length = k;}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects2[i].getVariableNumber(gdjs.MainCode.GDpecasObjects2[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.MainCode.condition1IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects2[k] = gdjs.MainCode.GDpecasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects2.length = k;}}
if (gdjs.MainCode.condition1IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDpecasObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDpecasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDpecasObjects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise06.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.MainCode.GDpecasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDpecasObjects2[i].returnVariable(gdjs.MainCode.GDpecasObjects2[i].getVariables().getFromIndex(0)).setNumber(-1);
}
}}

}


}; //End of gdjs.MainCode.eventsList0x6b71f8
gdjs.MainCode.eventsList0x7a3c40 = function(runtimeScene) {

{



}


{

gdjs.MainCode.GDpecasObjects2.createFrom(runtimeScene.getObjects("pecas"));

gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
gdjs.MainCode.condition2IsTrue_0.val = false;
gdjs.MainCode.condition3IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDpecasObjects2Objects, runtimeScene, true, false);
}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDpecasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDpecasObjects2[i].getVariableNumber(gdjs.MainCode.GDpecasObjects2[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.MainCode.condition1IsTrue_0.val = true;
        gdjs.MainCode.GDpecasObjects2[k] = gdjs.MainCode.GDpecasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDpecasObjects2.length = k;}if ( gdjs.MainCode.condition1IsTrue_0.val ) {
{
gdjs.MainCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.MainCode.condition2IsTrue_0.val ) {
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition3IsTrue_0;
gdjs.MainCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7035244);
}
}}
}
}
if (gdjs.MainCode.condition3IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x6b56f8(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(8)) >= 3;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDdownObjects2.createFrom(runtimeScene.getObjects("down"));
gdjs.MainCode.GDtente_95novamenteObjects2.createFrom(runtimeScene.getObjects("tente_novamente"));
{for(var i = 0, len = gdjs.MainCode.GDdownObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDdownObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.MainCode.GDtente_95novamenteObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDtente_95novamenteObjects2[i].hide(false);
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "conferir");
}{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_STEEL07.ogg", false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(8).setNumber(0);
}{runtimeScene.getVariables().getFromIndex(6).setNumber(0);
}{runtimeScene.getVariables().getFromIndex(7).setNumber(0);
}
{ //Subevents
gdjs.MainCode.eventsList0x6b6350(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(6)) == 1;
}if (gdjs.MainCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x6b6c48(runtimeScene);} //End of subevents
}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(7)) == 1;
}if (gdjs.MainCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x6b71f8(runtimeScene);} //End of subevents
}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
gdjs.MainCode.condition2IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(6)) == 1;
}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
gdjs.MainCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(7)) == 1;
}if ( gdjs.MainCode.condition1IsTrue_0.val ) {
{
gdjs.MainCode.condition2IsTrue_0.val = !(gdjs.evtTools.camera.layerIsVisible(runtimeScene, "conferir"));
}}
}
if (gdjs.MainCode.condition2IsTrue_0.val) {
gdjs.MainCode.GDparabensObjects1.createFrom(runtimeScene.getObjects("parabens"));
gdjs.MainCode.GDupObjects1.createFrom(runtimeScene.getObjects("up"));
{runtimeScene.getGame().getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.camera.showLayer(runtimeScene, "conferir");
}{gdjs.evtTools.sound.playSound(runtimeScene, "orchestra.ogg", false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(8).setNumber(0);
}{for(var i = 0, len = gdjs.MainCode.GDupObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDupObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.MainCode.GDparabensObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDparabensObjects1[i].hide(false);
}
}}

}


}; //End of gdjs.MainCode.eventsList0x7a3c40
gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDupObjects1ObjectsGDgdjs_46MainCode_46GDdownObjects1ObjectsGDgdjs_46MainCode_46GDtente_9595novamenteObjects1ObjectsGDgdjs_46MainCode_46GDparabensObjects1Objects = Hashtable.newFrom({"up": gdjs.MainCode.GDupObjects1, "down": gdjs.MainCode.GDdownObjects1, "tente_novamente": gdjs.MainCode.GDtente_95novamenteObjects1, "parabens": gdjs.MainCode.GDparabensObjects1});gdjs.MainCode.eventsList0x6b7f88 = function(runtimeScene) {

{

gdjs.MainCode.GDdownObjects1.createFrom(runtimeScene.getObjects("down"));
gdjs.MainCode.GDparabensObjects1.createFrom(runtimeScene.getObjects("parabens"));
gdjs.MainCode.GDtente_95novamenteObjects1.createFrom(runtimeScene.getObjects("tente_novamente"));
gdjs.MainCode.GDupObjects1.createFrom(runtimeScene.getObjects("up"));

gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
gdjs.MainCode.condition2IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDupObjects1ObjectsGDgdjs_46MainCode_46GDdownObjects1ObjectsGDgdjs_46MainCode_46GDtente_9595novamenteObjects1ObjectsGDgdjs_46MainCode_46GDparabensObjects1Objects, runtimeScene, true, false);
}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
gdjs.MainCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.MainCode.condition1IsTrue_0.val ) {
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition2IsTrue_0;
gdjs.MainCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7045716);
}
}}
}
if (gdjs.MainCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Main", false);
}}

}


}; //End of gdjs.MainCode.eventsList0x6b7f88
gdjs.MainCode.eventsList0x6b8870 = function(runtimeScene) {

{

gdjs.MainCode.GDestrelaObjects2.createFrom(runtimeScene.getObjects("estrela"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDestrelaObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDestrelaObjects2[i].getVariableNumber(gdjs.MainCode.GDestrelaObjects2[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDestrelaObjects2[k] = gdjs.MainCode.GDestrelaObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDestrelaObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDestrelaObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDestrelaObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDestrelaObjects2[i].setAnimation(2);
}
}}

}


{

gdjs.MainCode.GDestrelaObjects2.createFrom(runtimeScene.getObjects("estrela"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDestrelaObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDestrelaObjects2[i].getVariableNumber(gdjs.MainCode.GDestrelaObjects2[i].getVariables().getFromIndex(0)) > gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDestrelaObjects2[k] = gdjs.MainCode.GDestrelaObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDestrelaObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDestrelaObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDestrelaObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDestrelaObjects2[i].setAnimation(0);
}
}}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 10;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDlblTrofeusObjects2.createFrom(runtimeScene.getObjects("lblTrofeus"));
{for(var i = 0, len = gdjs.MainCode.GDlblTrofeusObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDlblTrofeusObjects2[i].setString("0" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 10;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDlblTrofeusObjects2.createFrom(runtimeScene.getObjects("lblTrofeus"));
{for(var i = 0, len = gdjs.MainCode.GDlblTrofeusObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDlblTrofeusObjects2[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}}

}


{

gdjs.MainCode.GDerrosObjects1.createFrom(runtimeScene.getObjects("erros"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDerrosObjects1.length;i<l;++i) {
    if ( gdjs.MainCode.GDerrosObjects1[i].getVariableNumber(gdjs.MainCode.GDerrosObjects1[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(8)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDerrosObjects1[k] = gdjs.MainCode.GDerrosObjects1[i];
        ++k;
    }
}
gdjs.MainCode.GDerrosObjects1.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDerrosObjects1 */
{for(var i = 0, len = gdjs.MainCode.GDerrosObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDerrosObjects1[i].hide();
}
}}

}


}; //End of gdjs.MainCode.eventsList0x6b8870
gdjs.MainCode.eventsList0xb1208 = function(runtimeScene) {

{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDdownObjects1.createFrom(runtimeScene.getObjects("down"));
gdjs.MainCode.GDerrosObjects1.createFrom(runtimeScene.getObjects("erros"));
gdjs.MainCode.GDlblTrofeusObjects1.createFrom(runtimeScene.getObjects("lblTrofeus"));
gdjs.MainCode.GDparabensObjects1.createFrom(runtimeScene.getObjects("parabens"));
gdjs.MainCode.GDtente_95novamenteObjects1.createFrom(runtimeScene.getObjects("tente_novamente"));
gdjs.MainCode.GDupObjects1.createFrom(runtimeScene.getObjects("up"));
{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "Layout" + gdjs.evtTools.common.toString(gdjs.randomInRange(1,  10)), 0, 0);
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "conferir");
}{for(var i = 0, len = gdjs.MainCode.GDlblTrofeusObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDlblTrofeusObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "glassbell.ogg", false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1,  8));
}{runtimeScene.getVariables().get("num").setNumber(gdjs.randomInRange(1,  5));
}{runtimeScene.getVariables().getFromIndex(6).setNumber(0);
}{runtimeScene.getVariables().getFromIndex(7).setNumber(0);
}{runtimeScene.getVariables().getFromIndex(8).setNumber(0);
}{for(var i = 0, len = gdjs.MainCode.GDupObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDupObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.MainCode.GDdownObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDdownObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.MainCode.GDparabensObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDparabensObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.MainCode.GDtente_95novamenteObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDtente_95novamenteObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.MainCode.GDerrosObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDerrosObjects1[i].hide(false);
}
}
{ //Subevents
gdjs.MainCode.eventsList0x6b2048(runtimeScene);} //End of subevents
}

}


{


gdjs.MainCode.eventsList0x7a3c40(runtimeScene);
}


{



}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "conferir");
}if (gdjs.MainCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x6b7f88(runtimeScene);} //End of subevents
}

}


{



}


{



}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.MainCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x6b8870(runtimeScene);} //End of subevents
}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 4;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Trofeu", false);
}}

}


}; //End of gdjs.MainCode.eventsList0xb1208


gdjs.MainCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.MainCode.GDbackgroundObjects1.length = 0;
gdjs.MainCode.GDbackgroundObjects2.length = 0;
gdjs.MainCode.GDbackgroundObjects3.length = 0;
gdjs.MainCode.GDbackgroundObjects4.length = 0;
gdjs.MainCode.GDtrofeuObjects1.length = 0;
gdjs.MainCode.GDtrofeuObjects2.length = 0;
gdjs.MainCode.GDtrofeuObjects3.length = 0;
gdjs.MainCode.GDtrofeuObjects4.length = 0;
gdjs.MainCode.GDestrelaSequenciaObjects1.length = 0;
gdjs.MainCode.GDestrelaSequenciaObjects2.length = 0;
gdjs.MainCode.GDestrelaSequenciaObjects3.length = 0;
gdjs.MainCode.GDestrelaSequenciaObjects4.length = 0;
gdjs.MainCode.GDestrelaObjects1.length = 0;
gdjs.MainCode.GDestrelaObjects2.length = 0;
gdjs.MainCode.GDestrelaObjects3.length = 0;
gdjs.MainCode.GDestrelaObjects4.length = 0;
gdjs.MainCode.GDparabensObjects1.length = 0;
gdjs.MainCode.GDparabensObjects2.length = 0;
gdjs.MainCode.GDparabensObjects3.length = 0;
gdjs.MainCode.GDparabensObjects4.length = 0;
gdjs.MainCode.GDpecasObjects1.length = 0;
gdjs.MainCode.GDpecasObjects2.length = 0;
gdjs.MainCode.GDpecasObjects3.length = 0;
gdjs.MainCode.GDpecasObjects4.length = 0;
gdjs.MainCode.GDbtConferirObjects1.length = 0;
gdjs.MainCode.GDbtConferirObjects2.length = 0;
gdjs.MainCode.GDbtConferirObjects3.length = 0;
gdjs.MainCode.GDbtConferirObjects4.length = 0;
gdjs.MainCode.GDenunciadoObjects1.length = 0;
gdjs.MainCode.GDenunciadoObjects2.length = 0;
gdjs.MainCode.GDenunciadoObjects3.length = 0;
gdjs.MainCode.GDenunciadoObjects4.length = 0;
gdjs.MainCode.GDlblTrofeusObjects1.length = 0;
gdjs.MainCode.GDlblTrofeusObjects2.length = 0;
gdjs.MainCode.GDlblTrofeusObjects3.length = 0;
gdjs.MainCode.GDlblTrofeusObjects4.length = 0;
gdjs.MainCode.GDupObjects1.length = 0;
gdjs.MainCode.GDupObjects2.length = 0;
gdjs.MainCode.GDupObjects3.length = 0;
gdjs.MainCode.GDupObjects4.length = 0;
gdjs.MainCode.GDdownObjects1.length = 0;
gdjs.MainCode.GDdownObjects2.length = 0;
gdjs.MainCode.GDdownObjects3.length = 0;
gdjs.MainCode.GDdownObjects4.length = 0;
gdjs.MainCode.GDtente_95novamenteObjects1.length = 0;
gdjs.MainCode.GDtente_95novamenteObjects2.length = 0;
gdjs.MainCode.GDtente_95novamenteObjects3.length = 0;
gdjs.MainCode.GDtente_95novamenteObjects4.length = 0;
gdjs.MainCode.GDpanelInferiorObjects1.length = 0;
gdjs.MainCode.GDpanelInferiorObjects2.length = 0;
gdjs.MainCode.GDpanelInferiorObjects3.length = 0;
gdjs.MainCode.GDpanelInferiorObjects4.length = 0;
gdjs.MainCode.GDpanelSequenciaObjects1.length = 0;
gdjs.MainCode.GDpanelSequenciaObjects2.length = 0;
gdjs.MainCode.GDpanelSequenciaObjects3.length = 0;
gdjs.MainCode.GDpanelSequenciaObjects4.length = 0;
gdjs.MainCode.GDtrofeusObjects1.length = 0;
gdjs.MainCode.GDtrofeusObjects2.length = 0;
gdjs.MainCode.GDtrofeusObjects3.length = 0;
gdjs.MainCode.GDtrofeusObjects4.length = 0;
gdjs.MainCode.GDerrosObjects1.length = 0;
gdjs.MainCode.GDerrosObjects2.length = 0;
gdjs.MainCode.GDerrosObjects3.length = 0;
gdjs.MainCode.GDerrosObjects4.length = 0;

gdjs.MainCode.eventsList0xb1208(runtimeScene);
return;
}
gdjs['MainCode'] = gdjs.MainCode;
