gdjs.TrofeuCode = {};
gdjs.TrofeuCode.GDparabensObjects1_1final = [];

gdjs.TrofeuCode.GDtrofeuObjects1_1final = [];

gdjs.TrofeuCode.GDbackgroundObjects1= [];
gdjs.TrofeuCode.GDbackgroundObjects2= [];
gdjs.TrofeuCode.GDtrofeuObjects1= [];
gdjs.TrofeuCode.GDtrofeuObjects2= [];
gdjs.TrofeuCode.GDestrelaSequenciaObjects1= [];
gdjs.TrofeuCode.GDestrelaSequenciaObjects2= [];
gdjs.TrofeuCode.GDestrelaObjects1= [];
gdjs.TrofeuCode.GDestrelaObjects2= [];
gdjs.TrofeuCode.GDparabensObjects1= [];
gdjs.TrofeuCode.GDparabensObjects2= [];
gdjs.TrofeuCode.GDvoltarObjects1= [];
gdjs.TrofeuCode.GDvoltarObjects2= [];
gdjs.TrofeuCode.GDlblTrofeusObjects1= [];
gdjs.TrofeuCode.GDlblTrofeusObjects2= [];

gdjs.TrofeuCode.conditionTrue_0 = {val:false};
gdjs.TrofeuCode.condition0IsTrue_0 = {val:false};
gdjs.TrofeuCode.condition1IsTrue_0 = {val:false};
gdjs.TrofeuCode.condition2IsTrue_0 = {val:false};
gdjs.TrofeuCode.conditionTrue_1 = {val:false};
gdjs.TrofeuCode.condition0IsTrue_1 = {val:false};
gdjs.TrofeuCode.condition1IsTrue_1 = {val:false};
gdjs.TrofeuCode.condition2IsTrue_1 = {val:false};


gdjs.TrofeuCode.mapOfGDgdjs_46TrofeuCode_46GDtrofeuObjects2Objects = Hashtable.newFrom({"trofeu": gdjs.TrofeuCode.GDtrofeuObjects2});gdjs.TrofeuCode.mapOfGDgdjs_46TrofeuCode_46GDparabensObjects2Objects = Hashtable.newFrom({"parabens": gdjs.TrofeuCode.GDparabensObjects2});gdjs.TrofeuCode.eventsList0xb1208 = function(runtimeScene) {

{


gdjs.TrofeuCode.condition0IsTrue_0.val = false;
{
gdjs.TrofeuCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.TrofeuCode.condition0IsTrue_0.val) {
gdjs.TrofeuCode.GDestrelaObjects1.createFrom(runtimeScene.getObjects("estrela"));
{for(var i = 0, len = gdjs.TrofeuCode.GDestrelaObjects1.length ;i < len;++i) {
    gdjs.TrofeuCode.GDestrelaObjects1[i].setAnimation(2);
}
}{for(var i = 0, len = gdjs.TrofeuCode.GDestrelaObjects1.length ;i < len;++i) {
    gdjs.TrofeuCode.GDestrelaObjects1[i].setAnimationSpeedScale(gdjs.randomFloatInRange(0.2,  0.4));
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "orchestra.ogg", false, 100, 1);
}}

}


{

gdjs.TrofeuCode.GDparabensObjects1.length = 0;

gdjs.TrofeuCode.GDtrofeuObjects1.length = 0;


gdjs.TrofeuCode.condition0IsTrue_0.val = false;
gdjs.TrofeuCode.condition1IsTrue_0.val = false;
{
gdjs.TrofeuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.TrofeuCode.condition0IsTrue_0.val ) {
{
{gdjs.TrofeuCode.conditionTrue_1 = gdjs.TrofeuCode.condition1IsTrue_0;
gdjs.TrofeuCode.GDparabensObjects1_1final.length = 0;gdjs.TrofeuCode.GDtrofeuObjects1_1final.length = 0;gdjs.TrofeuCode.condition0IsTrue_1.val = false;
gdjs.TrofeuCode.condition1IsTrue_1.val = false;
{
gdjs.TrofeuCode.GDtrofeuObjects2.createFrom(runtimeScene.getObjects("trofeu"));
gdjs.TrofeuCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.TrofeuCode.mapOfGDgdjs_46TrofeuCode_46GDtrofeuObjects2Objects, runtimeScene, true, false);
if( gdjs.TrofeuCode.condition0IsTrue_1.val ) {
    gdjs.TrofeuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.TrofeuCode.GDtrofeuObjects2.length;j<jLen;++j) {
        if ( gdjs.TrofeuCode.GDtrofeuObjects1_1final.indexOf(gdjs.TrofeuCode.GDtrofeuObjects2[j]) === -1 )
            gdjs.TrofeuCode.GDtrofeuObjects1_1final.push(gdjs.TrofeuCode.GDtrofeuObjects2[j]);
    }
}
}
{
gdjs.TrofeuCode.GDparabensObjects2.createFrom(runtimeScene.getObjects("parabens"));
gdjs.TrofeuCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.TrofeuCode.mapOfGDgdjs_46TrofeuCode_46GDparabensObjects2Objects, runtimeScene, true, false);
if( gdjs.TrofeuCode.condition1IsTrue_1.val ) {
    gdjs.TrofeuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.TrofeuCode.GDparabensObjects2.length;j<jLen;++j) {
        if ( gdjs.TrofeuCode.GDparabensObjects1_1final.indexOf(gdjs.TrofeuCode.GDparabensObjects2[j]) === -1 )
            gdjs.TrofeuCode.GDparabensObjects1_1final.push(gdjs.TrofeuCode.GDparabensObjects2[j]);
    }
}
}
{
gdjs.TrofeuCode.GDparabensObjects1.createFrom(gdjs.TrofeuCode.GDparabensObjects1_1final);
gdjs.TrofeuCode.GDtrofeuObjects1.createFrom(gdjs.TrofeuCode.GDtrofeuObjects1_1final);
}
}
}}
if (gdjs.TrofeuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Main", false);
}}

}


}; //End of gdjs.TrofeuCode.eventsList0xb1208


gdjs.TrofeuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.TrofeuCode.GDbackgroundObjects1.length = 0;
gdjs.TrofeuCode.GDbackgroundObjects2.length = 0;
gdjs.TrofeuCode.GDtrofeuObjects1.length = 0;
gdjs.TrofeuCode.GDtrofeuObjects2.length = 0;
gdjs.TrofeuCode.GDestrelaSequenciaObjects1.length = 0;
gdjs.TrofeuCode.GDestrelaSequenciaObjects2.length = 0;
gdjs.TrofeuCode.GDestrelaObjects1.length = 0;
gdjs.TrofeuCode.GDestrelaObjects2.length = 0;
gdjs.TrofeuCode.GDparabensObjects1.length = 0;
gdjs.TrofeuCode.GDparabensObjects2.length = 0;
gdjs.TrofeuCode.GDvoltarObjects1.length = 0;
gdjs.TrofeuCode.GDvoltarObjects2.length = 0;
gdjs.TrofeuCode.GDlblTrofeusObjects1.length = 0;
gdjs.TrofeuCode.GDlblTrofeusObjects2.length = 0;

gdjs.TrofeuCode.eventsList0xb1208(runtimeScene);
return;
}
gdjs['TrofeuCode'] = gdjs.TrofeuCode;
