gdjs.TrofeuCode = {};
gdjs.TrofeuCode.GDbackgroundObjects1= [];
gdjs.TrofeuCode.GDbackgroundObjects2= [];
gdjs.TrofeuCode.GDtrofeuObjects1= [];
gdjs.TrofeuCode.GDtrofeuObjects2= [];
gdjs.TrofeuCode.GDestrela1Objects1= [];
gdjs.TrofeuCode.GDestrela1Objects2= [];
gdjs.TrofeuCode.GDparabensObjects1= [];
gdjs.TrofeuCode.GDparabensObjects2= [];
gdjs.TrofeuCode.GDvoltarObjects1= [];
gdjs.TrofeuCode.GDvoltarObjects2= [];
gdjs.TrofeuCode.GDlblTrofeusObjects1= [];
gdjs.TrofeuCode.GDlblTrofeusObjects2= [];

gdjs.TrofeuCode.conditionTrue_0 = {val:false};
gdjs.TrofeuCode.condition0IsTrue_0 = {val:false};
gdjs.TrofeuCode.condition1IsTrue_0 = {val:false};
gdjs.TrofeuCode.condition2IsTrue_0 = {val:false};


gdjs.TrofeuCode.eventsList0x6e3ec0 = function(runtimeScene, context) {

{


gdjs.TrofeuCode.condition0IsTrue_0.val = false;
{
gdjs.TrofeuCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) < 9;
}if (gdjs.TrofeuCode.condition0IsTrue_0.val) {
gdjs.TrofeuCode.GDlblTrofeusObjects1.createFrom(runtimeScene.getObjects("lblTrofeus"));
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}{for(var i = 0, len = gdjs.TrofeuCode.GDlblTrofeusObjects1.length ;i < len;++i) {
    gdjs.TrofeuCode.GDlblTrofeusObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}}

}


}; //End of gdjs.TrofeuCode.eventsList0x6e3ec0
gdjs.TrofeuCode.mapOfGDgdjs_46TrofeuCode_46GDvoltarObjects1Objects = Hashtable.newFrom({"voltar": gdjs.TrofeuCode.GDvoltarObjects1});gdjs.TrofeuCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.TrofeuCode.condition0IsTrue_0.val = false;
{
gdjs.TrofeuCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.TrofeuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "orchestra.ogg", false, 100, 1);
}
{ //Subevents
gdjs.TrofeuCode.eventsList0x6e3ec0(runtimeScene, context);} //End of subevents
}

}


{

gdjs.TrofeuCode.GDvoltarObjects1.createFrom(runtimeScene.getObjects("voltar"));

gdjs.TrofeuCode.condition0IsTrue_0.val = false;
gdjs.TrofeuCode.condition1IsTrue_0.val = false;
{
gdjs.TrofeuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.TrofeuCode.condition0IsTrue_0.val ) {
{
gdjs.TrofeuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.TrofeuCode.mapOfGDgdjs_46TrofeuCode_46GDvoltarObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.TrofeuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Main", false);
}}

}


}; //End of gdjs.TrofeuCode.eventsList0xaf630


gdjs.TrofeuCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.TrofeuCode.GDbackgroundObjects1.length = 0;
gdjs.TrofeuCode.GDbackgroundObjects2.length = 0;
gdjs.TrofeuCode.GDtrofeuObjects1.length = 0;
gdjs.TrofeuCode.GDtrofeuObjects2.length = 0;
gdjs.TrofeuCode.GDestrela1Objects1.length = 0;
gdjs.TrofeuCode.GDestrela1Objects2.length = 0;
gdjs.TrofeuCode.GDparabensObjects1.length = 0;
gdjs.TrofeuCode.GDparabensObjects2.length = 0;
gdjs.TrofeuCode.GDvoltarObjects1.length = 0;
gdjs.TrofeuCode.GDvoltarObjects2.length = 0;
gdjs.TrofeuCode.GDlblTrofeusObjects1.length = 0;
gdjs.TrofeuCode.GDlblTrofeusObjects2.length = 0;

gdjs.TrofeuCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['TrofeuCode']= gdjs.TrofeuCode;
