gdjs.StartCode = {};
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDtrofeuObjects1= [];
gdjs.StartCode.GDtrofeuObjects2= [];
gdjs.StartCode.GDestrela1Objects1= [];
gdjs.StartCode.GDestrela1Objects2= [];
gdjs.StartCode.GDparabensObjects1= [];
gdjs.StartCode.GDparabensObjects2= [];
gdjs.StartCode.GDtituloObjects1= [];
gdjs.StartCode.GDtituloObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};


gdjs.StartCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "glassbell.ogg", false, 100, 1);
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Main", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xaf630


gdjs.StartCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDtrofeuObjects1.length = 0;
gdjs.StartCode.GDtrofeuObjects2.length = 0;
gdjs.StartCode.GDestrela1Objects1.length = 0;
gdjs.StartCode.GDestrela1Objects2.length = 0;
gdjs.StartCode.GDparabensObjects1.length = 0;
gdjs.StartCode.GDparabensObjects2.length = 0;
gdjs.StartCode.GDtituloObjects1.length = 0;
gdjs.StartCode.GDtituloObjects2.length = 0;

gdjs.StartCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['StartCode']= gdjs.StartCode;
