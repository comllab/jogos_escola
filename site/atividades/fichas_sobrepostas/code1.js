gdjs.MainCode = {};
gdjs.MainCode.GDcentenasObjects2_1final = [];

gdjs.MainCode.GDdezenasObjects2_1final = [];

gdjs.MainCode.GDmilharesObjects2_1final = [];

gdjs.MainCode.GDnumero_95centenaObjects2_1final = [];

gdjs.MainCode.GDnumero_95dezenaObjects2_1final = [];

gdjs.MainCode.GDnumero_95milharObjects2_1final = [];

gdjs.MainCode.GDnumero_95unidadeObjects2_1final = [];

gdjs.MainCode.GDparabensObjects1_1final = [];

gdjs.MainCode.GDtente_95novamenteObjects1_1final = [];

gdjs.MainCode.GDunidadesObjects2_1final = [];

gdjs.MainCode.GDbackgroundObjects1= [];
gdjs.MainCode.GDbackgroundObjects2= [];
gdjs.MainCode.GDbackgroundObjects3= [];
gdjs.MainCode.GDtrofeuObjects1= [];
gdjs.MainCode.GDtrofeuObjects2= [];
gdjs.MainCode.GDtrofeuObjects3= [];
gdjs.MainCode.GDestrela1Objects1= [];
gdjs.MainCode.GDestrela1Objects2= [];
gdjs.MainCode.GDestrela1Objects3= [];
gdjs.MainCode.GDparabensObjects1= [];
gdjs.MainCode.GDparabensObjects2= [];
gdjs.MainCode.GDparabensObjects3= [];
gdjs.MainCode.GDunidadesObjects1= [];
gdjs.MainCode.GDunidadesObjects2= [];
gdjs.MainCode.GDunidadesObjects3= [];
gdjs.MainCode.GDdezenasObjects1= [];
gdjs.MainCode.GDdezenasObjects2= [];
gdjs.MainCode.GDdezenasObjects3= [];
gdjs.MainCode.GDcentenasObjects1= [];
gdjs.MainCode.GDcentenasObjects2= [];
gdjs.MainCode.GDcentenasObjects3= [];
gdjs.MainCode.GDmilharesObjects1= [];
gdjs.MainCode.GDmilharesObjects2= [];
gdjs.MainCode.GDmilharesObjects3= [];
gdjs.MainCode.GDlblUnidadesObjects1= [];
gdjs.MainCode.GDlblUnidadesObjects2= [];
gdjs.MainCode.GDlblUnidadesObjects3= [];
gdjs.MainCode.GDlblDezenhasObjects1= [];
gdjs.MainCode.GDlblDezenhasObjects2= [];
gdjs.MainCode.GDlblDezenhasObjects3= [];
gdjs.MainCode.GDlblCentasObjects1= [];
gdjs.MainCode.GDlblCentasObjects2= [];
gdjs.MainCode.GDlblCentasObjects3= [];
gdjs.MainCode.GDlblMilharesObjects1= [];
gdjs.MainCode.GDlblMilharesObjects2= [];
gdjs.MainCode.GDlblMilharesObjects3= [];
gdjs.MainCode.GDbtConferirObjects1= [];
gdjs.MainCode.GDbtConferirObjects2= [];
gdjs.MainCode.GDbtConferirObjects3= [];
gdjs.MainCode.GDCONFERIRObjects1= [];
gdjs.MainCode.GDCONFERIRObjects2= [];
gdjs.MainCode.GDCONFERIRObjects3= [];
gdjs.MainCode.GDqtdTrofeusObjects1= [];
gdjs.MainCode.GDqtdTrofeusObjects2= [];
gdjs.MainCode.GDqtdTrofeusObjects3= [];
gdjs.MainCode.GDlblTrofeusObjects1= [];
gdjs.MainCode.GDlblTrofeusObjects2= [];
gdjs.MainCode.GDlblTrofeusObjects3= [];
gdjs.MainCode.GDnumero_95unidadeObjects1= [];
gdjs.MainCode.GDnumero_95unidadeObjects2= [];
gdjs.MainCode.GDnumero_95unidadeObjects3= [];
gdjs.MainCode.GDnumero_95dezenaObjects1= [];
gdjs.MainCode.GDnumero_95dezenaObjects2= [];
gdjs.MainCode.GDnumero_95dezenaObjects3= [];
gdjs.MainCode.GDnumero_95centenaObjects1= [];
gdjs.MainCode.GDnumero_95centenaObjects2= [];
gdjs.MainCode.GDnumero_95centenaObjects3= [];
gdjs.MainCode.GDnumero_95milharObjects1= [];
gdjs.MainCode.GDnumero_95milharObjects2= [];
gdjs.MainCode.GDnumero_95milharObjects3= [];
gdjs.MainCode.GDupObjects1= [];
gdjs.MainCode.GDupObjects2= [];
gdjs.MainCode.GDupObjects3= [];
gdjs.MainCode.GDSORTEARObjects1= [];
gdjs.MainCode.GDSORTEARObjects2= [];
gdjs.MainCode.GDSORTEARObjects3= [];
gdjs.MainCode.GDdownObjects1= [];
gdjs.MainCode.GDdownObjects2= [];
gdjs.MainCode.GDdownObjects3= [];
gdjs.MainCode.GDtente_95novamenteObjects1= [];
gdjs.MainCode.GDtente_95novamenteObjects2= [];
gdjs.MainCode.GDtente_95novamenteObjects3= [];

gdjs.MainCode.conditionTrue_0 = {val:false};
gdjs.MainCode.condition0IsTrue_0 = {val:false};
gdjs.MainCode.condition1IsTrue_0 = {val:false};
gdjs.MainCode.condition2IsTrue_0 = {val:false};
gdjs.MainCode.condition3IsTrue_0 = {val:false};
gdjs.MainCode.condition4IsTrue_0 = {val:false};
gdjs.MainCode.conditionTrue_1 = {val:false};
gdjs.MainCode.condition0IsTrue_1 = {val:false};
gdjs.MainCode.condition1IsTrue_1 = {val:false};
gdjs.MainCode.condition2IsTrue_1 = {val:false};
gdjs.MainCode.condition3IsTrue_1 = {val:false};
gdjs.MainCode.condition4IsTrue_1 = {val:false};


gdjs.MainCode.eventsList0x71ce70 = function(runtimeScene, context) {

{

gdjs.MainCode.GDestrela1Objects2.createFrom(runtimeScene.getObjects("estrela1"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDestrela1Objects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDestrela1Objects2[i].getVariableNumber(gdjs.MainCode.GDestrela1Objects2[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDestrela1Objects2[k] = gdjs.MainCode.GDestrela1Objects2[i];
        ++k;
    }
}
gdjs.MainCode.GDestrela1Objects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDestrela1Objects2 */
{for(var i = 0, len = gdjs.MainCode.GDestrela1Objects2.length ;i < len;++i) {
    gdjs.MainCode.GDestrela1Objects2[i].setAnimation(1);
}
}}

}


{

gdjs.MainCode.GDestrela1Objects1.createFrom(runtimeScene.getObjects("estrela1"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDestrela1Objects1.length;i<l;++i) {
    if ( gdjs.MainCode.GDestrela1Objects1[i].getVariableNumber(gdjs.MainCode.GDestrela1Objects1[i].getVariables().getFromIndex(0)) > gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDestrela1Objects1[k] = gdjs.MainCode.GDestrela1Objects1[i];
        ++k;
    }
}
gdjs.MainCode.GDestrela1Objects1.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDestrela1Objects1 */
{for(var i = 0, len = gdjs.MainCode.GDestrela1Objects1.length ;i < len;++i) {
    gdjs.MainCode.GDestrela1Objects1[i].setAnimation(0);
}
}}

}


}; //End of gdjs.MainCode.eventsList0x71ce70
gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDSORTEARObjects1Objects = Hashtable.newFrom({"SORTEAR": gdjs.MainCode.GDSORTEARObjects1});gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDCONFERIRObjects1Objects = Hashtable.newFrom({"CONFERIR": gdjs.MainCode.GDCONFERIRObjects1});gdjs.MainCode.eventsList0x6e6bb8 = function(runtimeScene, context) {

{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) > 0;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).sub(1);
}}

}


}; //End of gdjs.MainCode.eventsList0x6e6bb8
gdjs.MainCode.eventsList0x6e5f28 = function(runtimeScene, context) {

{



}


{

gdjs.MainCode.GDcentenasObjects2.createFrom(runtimeScene.getObjects("centenas"));
gdjs.MainCode.GDdezenasObjects2.createFrom(runtimeScene.getObjects("dezenas"));
gdjs.MainCode.GDmilharesObjects2.createFrom(runtimeScene.getObjects("milhares"));
gdjs.MainCode.GDnumero_95centenaObjects2.createFrom(runtimeScene.getObjects("numero_centena"));
gdjs.MainCode.GDnumero_95dezenaObjects2.createFrom(runtimeScene.getObjects("numero_dezena"));
gdjs.MainCode.GDnumero_95milharObjects2.createFrom(runtimeScene.getObjects("numero_milhar"));
gdjs.MainCode.GDnumero_95unidadeObjects2.createFrom(runtimeScene.getObjects("numero_unidade"));
gdjs.MainCode.GDunidadesObjects2.createFrom(runtimeScene.getObjects("unidades"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition0IsTrue_0;
gdjs.MainCode.condition0IsTrue_1.val = false;
gdjs.MainCode.condition1IsTrue_1.val = false;
gdjs.MainCode.condition2IsTrue_1.val = false;
gdjs.MainCode.condition3IsTrue_1.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDunidadesObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDunidadesObjects2[i].getAnimation() == (( gdjs.MainCode.GDnumero_95unidadeObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDnumero_95unidadeObjects2[0].getAnimation()) ) {
        gdjs.MainCode.condition0IsTrue_1.val = true;
        gdjs.MainCode.GDunidadesObjects2[k] = gdjs.MainCode.GDunidadesObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDunidadesObjects2.length = k;}if ( gdjs.MainCode.condition0IsTrue_1.val ) {
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDdezenasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDdezenasObjects2[i].getAnimation() == (( gdjs.MainCode.GDnumero_95dezenaObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDnumero_95dezenaObjects2[0].getAnimation()) ) {
        gdjs.MainCode.condition1IsTrue_1.val = true;
        gdjs.MainCode.GDdezenasObjects2[k] = gdjs.MainCode.GDdezenasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDdezenasObjects2.length = k;}if ( gdjs.MainCode.condition1IsTrue_1.val ) {
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDcentenasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDcentenasObjects2[i].getAnimation() == (( gdjs.MainCode.GDnumero_95centenaObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDnumero_95centenaObjects2[0].getAnimation()) ) {
        gdjs.MainCode.condition2IsTrue_1.val = true;
        gdjs.MainCode.GDcentenasObjects2[k] = gdjs.MainCode.GDcentenasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDcentenasObjects2.length = k;}if ( gdjs.MainCode.condition2IsTrue_1.val ) {
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDmilharesObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDmilharesObjects2[i].getAnimation() == (( gdjs.MainCode.GDnumero_95milharObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDnumero_95milharObjects2[0].getAnimation()) ) {
        gdjs.MainCode.condition3IsTrue_1.val = true;
        gdjs.MainCode.GDmilharesObjects2[k] = gdjs.MainCode.GDmilharesObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDmilharesObjects2.length = k;}}
}
}
gdjs.MainCode.conditionTrue_1.val = true && gdjs.MainCode.condition0IsTrue_1.val && gdjs.MainCode.condition1IsTrue_1.val && gdjs.MainCode.condition2IsTrue_1.val && gdjs.MainCode.condition3IsTrue_1.val;
}
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDdownObjects2.createFrom(runtimeScene.getObjects("down"));
gdjs.MainCode.GDparabensObjects2.createFrom(runtimeScene.getObjects("parabens"));
gdjs.MainCode.GDtente_95novamenteObjects2.createFrom(runtimeScene.getObjects("tente_novamente"));
gdjs.MainCode.GDupObjects2.createFrom(runtimeScene.getObjects("up"));
{for(var i = 0, len = gdjs.MainCode.GDupObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDupObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.MainCode.GDparabensObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDparabensObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.MainCode.GDdownObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDdownObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MainCode.GDtente_95novamenteObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDtente_95novamenteObjects2[i].hide();
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.camera.showLayer(runtimeScene, "conferir");
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise06.ogg", false, 100, 1);
}}

}


{



}


{

gdjs.MainCode.GDcentenasObjects2.length = 0;

gdjs.MainCode.GDdezenasObjects2.length = 0;

gdjs.MainCode.GDmilharesObjects2.length = 0;

gdjs.MainCode.GDnumero_95centenaObjects2.length = 0;

gdjs.MainCode.GDnumero_95dezenaObjects2.length = 0;

gdjs.MainCode.GDnumero_95milharObjects2.length = 0;

gdjs.MainCode.GDnumero_95unidadeObjects2.length = 0;

gdjs.MainCode.GDunidadesObjects2.length = 0;


gdjs.MainCode.condition0IsTrue_0.val = false;
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition0IsTrue_0;
gdjs.MainCode.GDcentenasObjects2_1final.length = 0;gdjs.MainCode.GDdezenasObjects2_1final.length = 0;gdjs.MainCode.GDmilharesObjects2_1final.length = 0;gdjs.MainCode.GDnumero_95centenaObjects2_1final.length = 0;gdjs.MainCode.GDnumero_95dezenaObjects2_1final.length = 0;gdjs.MainCode.GDnumero_95milharObjects2_1final.length = 0;gdjs.MainCode.GDnumero_95unidadeObjects2_1final.length = 0;gdjs.MainCode.GDunidadesObjects2_1final.length = 0;gdjs.MainCode.condition0IsTrue_1.val = false;
gdjs.MainCode.condition1IsTrue_1.val = false;
gdjs.MainCode.condition2IsTrue_1.val = false;
gdjs.MainCode.condition3IsTrue_1.val = false;
{
gdjs.MainCode.GDnumero_95unidadeObjects3.createFrom(runtimeScene.getObjects("numero_unidade"));
gdjs.MainCode.GDunidadesObjects3.createFrom(runtimeScene.getObjects("unidades"));
for(var i = 0, k = 0, l = gdjs.MainCode.GDunidadesObjects3.length;i<l;++i) {
    if ( gdjs.MainCode.GDunidadesObjects3[i].getAnimation() != (( gdjs.MainCode.GDnumero_95unidadeObjects3.length === 0 ) ? 0 :gdjs.MainCode.GDnumero_95unidadeObjects3[0].getAnimation()) ) {
        gdjs.MainCode.condition0IsTrue_1.val = true;
        gdjs.MainCode.GDunidadesObjects3[k] = gdjs.MainCode.GDunidadesObjects3[i];
        ++k;
    }
}
gdjs.MainCode.GDunidadesObjects3.length = k;if( gdjs.MainCode.condition0IsTrue_1.val ) {
    gdjs.MainCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.MainCode.GDnumero_95unidadeObjects3.length;j<jLen;++j) {
        if ( gdjs.MainCode.GDnumero_95unidadeObjects2_1final.indexOf(gdjs.MainCode.GDnumero_95unidadeObjects3[j]) === -1 )
            gdjs.MainCode.GDnumero_95unidadeObjects2_1final.push(gdjs.MainCode.GDnumero_95unidadeObjects3[j]);
    }
    for(var j = 0, jLen = gdjs.MainCode.GDunidadesObjects3.length;j<jLen;++j) {
        if ( gdjs.MainCode.GDunidadesObjects2_1final.indexOf(gdjs.MainCode.GDunidadesObjects3[j]) === -1 )
            gdjs.MainCode.GDunidadesObjects2_1final.push(gdjs.MainCode.GDunidadesObjects3[j]);
    }
}
}
{
gdjs.MainCode.GDdezenasObjects3.createFrom(runtimeScene.getObjects("dezenas"));
gdjs.MainCode.GDnumero_95dezenaObjects3.createFrom(runtimeScene.getObjects("numero_dezena"));
for(var i = 0, k = 0, l = gdjs.MainCode.GDdezenasObjects3.length;i<l;++i) {
    if ( gdjs.MainCode.GDdezenasObjects3[i].getAnimation() != (( gdjs.MainCode.GDnumero_95dezenaObjects3.length === 0 ) ? 0 :gdjs.MainCode.GDnumero_95dezenaObjects3[0].getAnimation()) ) {
        gdjs.MainCode.condition1IsTrue_1.val = true;
        gdjs.MainCode.GDdezenasObjects3[k] = gdjs.MainCode.GDdezenasObjects3[i];
        ++k;
    }
}
gdjs.MainCode.GDdezenasObjects3.length = k;if( gdjs.MainCode.condition1IsTrue_1.val ) {
    gdjs.MainCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.MainCode.GDdezenasObjects3.length;j<jLen;++j) {
        if ( gdjs.MainCode.GDdezenasObjects2_1final.indexOf(gdjs.MainCode.GDdezenasObjects3[j]) === -1 )
            gdjs.MainCode.GDdezenasObjects2_1final.push(gdjs.MainCode.GDdezenasObjects3[j]);
    }
    for(var j = 0, jLen = gdjs.MainCode.GDnumero_95dezenaObjects3.length;j<jLen;++j) {
        if ( gdjs.MainCode.GDnumero_95dezenaObjects2_1final.indexOf(gdjs.MainCode.GDnumero_95dezenaObjects3[j]) === -1 )
            gdjs.MainCode.GDnumero_95dezenaObjects2_1final.push(gdjs.MainCode.GDnumero_95dezenaObjects3[j]);
    }
}
}
{
gdjs.MainCode.GDcentenasObjects3.createFrom(runtimeScene.getObjects("centenas"));
gdjs.MainCode.GDnumero_95centenaObjects3.createFrom(runtimeScene.getObjects("numero_centena"));
for(var i = 0, k = 0, l = gdjs.MainCode.GDcentenasObjects3.length;i<l;++i) {
    if ( gdjs.MainCode.GDcentenasObjects3[i].getAnimation() != (( gdjs.MainCode.GDnumero_95centenaObjects3.length === 0 ) ? 0 :gdjs.MainCode.GDnumero_95centenaObjects3[0].getAnimation()) ) {
        gdjs.MainCode.condition2IsTrue_1.val = true;
        gdjs.MainCode.GDcentenasObjects3[k] = gdjs.MainCode.GDcentenasObjects3[i];
        ++k;
    }
}
gdjs.MainCode.GDcentenasObjects3.length = k;if( gdjs.MainCode.condition2IsTrue_1.val ) {
    gdjs.MainCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.MainCode.GDcentenasObjects3.length;j<jLen;++j) {
        if ( gdjs.MainCode.GDcentenasObjects2_1final.indexOf(gdjs.MainCode.GDcentenasObjects3[j]) === -1 )
            gdjs.MainCode.GDcentenasObjects2_1final.push(gdjs.MainCode.GDcentenasObjects3[j]);
    }
    for(var j = 0, jLen = gdjs.MainCode.GDnumero_95centenaObjects3.length;j<jLen;++j) {
        if ( gdjs.MainCode.GDnumero_95centenaObjects2_1final.indexOf(gdjs.MainCode.GDnumero_95centenaObjects3[j]) === -1 )
            gdjs.MainCode.GDnumero_95centenaObjects2_1final.push(gdjs.MainCode.GDnumero_95centenaObjects3[j]);
    }
}
}
{
gdjs.MainCode.GDmilharesObjects3.createFrom(runtimeScene.getObjects("milhares"));
gdjs.MainCode.GDnumero_95milharObjects3.createFrom(runtimeScene.getObjects("numero_milhar"));
for(var i = 0, k = 0, l = gdjs.MainCode.GDmilharesObjects3.length;i<l;++i) {
    if ( gdjs.MainCode.GDmilharesObjects3[i].getAnimation() != (( gdjs.MainCode.GDnumero_95milharObjects3.length === 0 ) ? 0 :gdjs.MainCode.GDnumero_95milharObjects3[0].getAnimation()) ) {
        gdjs.MainCode.condition3IsTrue_1.val = true;
        gdjs.MainCode.GDmilharesObjects3[k] = gdjs.MainCode.GDmilharesObjects3[i];
        ++k;
    }
}
gdjs.MainCode.GDmilharesObjects3.length = k;if( gdjs.MainCode.condition3IsTrue_1.val ) {
    gdjs.MainCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.MainCode.GDmilharesObjects3.length;j<jLen;++j) {
        if ( gdjs.MainCode.GDmilharesObjects2_1final.indexOf(gdjs.MainCode.GDmilharesObjects3[j]) === -1 )
            gdjs.MainCode.GDmilharesObjects2_1final.push(gdjs.MainCode.GDmilharesObjects3[j]);
    }
    for(var j = 0, jLen = gdjs.MainCode.GDnumero_95milharObjects3.length;j<jLen;++j) {
        if ( gdjs.MainCode.GDnumero_95milharObjects2_1final.indexOf(gdjs.MainCode.GDnumero_95milharObjects3[j]) === -1 )
            gdjs.MainCode.GDnumero_95milharObjects2_1final.push(gdjs.MainCode.GDnumero_95milharObjects3[j]);
    }
}
}
{
gdjs.MainCode.GDcentenasObjects2.createFrom(gdjs.MainCode.GDcentenasObjects2_1final);
gdjs.MainCode.GDdezenasObjects2.createFrom(gdjs.MainCode.GDdezenasObjects2_1final);
gdjs.MainCode.GDmilharesObjects2.createFrom(gdjs.MainCode.GDmilharesObjects2_1final);
gdjs.MainCode.GDnumero_95centenaObjects2.createFrom(gdjs.MainCode.GDnumero_95centenaObjects2_1final);
gdjs.MainCode.GDnumero_95dezenaObjects2.createFrom(gdjs.MainCode.GDnumero_95dezenaObjects2_1final);
gdjs.MainCode.GDnumero_95milharObjects2.createFrom(gdjs.MainCode.GDnumero_95milharObjects2_1final);
gdjs.MainCode.GDnumero_95unidadeObjects2.createFrom(gdjs.MainCode.GDnumero_95unidadeObjects2_1final);
gdjs.MainCode.GDunidadesObjects2.createFrom(gdjs.MainCode.GDunidadesObjects2_1final);
}
}
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDdownObjects2.createFrom(runtimeScene.getObjects("down"));
gdjs.MainCode.GDparabensObjects2.createFrom(runtimeScene.getObjects("parabens"));
gdjs.MainCode.GDtente_95novamenteObjects2.createFrom(runtimeScene.getObjects("tente_novamente"));
gdjs.MainCode.GDupObjects2.createFrom(runtimeScene.getObjects("up"));
{for(var i = 0, len = gdjs.MainCode.GDdownObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDdownObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.MainCode.GDtente_95novamenteObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDtente_95novamenteObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.MainCode.GDupObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDupObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MainCode.GDparabensObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDparabensObjects2[i].hide();
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "conferir");
}{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_STEEL07.ogg", false, 100, 1);
}
{ //Subevents
gdjs.MainCode.eventsList0x6e6bb8(runtimeScene, context);} //End of subevents
}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 5;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Trofeu", false);
}}

}


}; //End of gdjs.MainCode.eventsList0x6e5f28
gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDparabensObjects2Objects = Hashtable.newFrom({"parabens": gdjs.MainCode.GDparabensObjects2});gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDtente_9595novamenteObjects2Objects = Hashtable.newFrom({"tente_novamente": gdjs.MainCode.GDtente_95novamenteObjects2});gdjs.MainCode.eventsList0x6e7728 = function(runtimeScene, context) {

{

gdjs.MainCode.GDupObjects2.createFrom(runtimeScene.getObjects("up"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDupObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDupObjects2[i].isVisible() ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDupObjects2[k] = gdjs.MainCode.GDupObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDupObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDcentenasObjects2.createFrom(runtimeScene.getObjects("centenas"));
gdjs.MainCode.GDdezenasObjects2.createFrom(runtimeScene.getObjects("dezenas"));
gdjs.MainCode.GDlblCentasObjects2.createFrom(runtimeScene.getObjects("lblCentas"));
gdjs.MainCode.GDlblDezenhasObjects2.createFrom(runtimeScene.getObjects("lblDezenhas"));
gdjs.MainCode.GDlblMilharesObjects2.createFrom(runtimeScene.getObjects("lblMilhares"));
gdjs.MainCode.GDlblUnidadesObjects2.createFrom(runtimeScene.getObjects("lblUnidades"));
gdjs.MainCode.GDmilharesObjects2.createFrom(runtimeScene.getObjects("milhares"));
gdjs.MainCode.GDnumero_95centenaObjects2.createFrom(runtimeScene.getObjects("numero_centena"));
gdjs.MainCode.GDnumero_95dezenaObjects2.createFrom(runtimeScene.getObjects("numero_dezena"));
gdjs.MainCode.GDnumero_95milharObjects2.createFrom(runtimeScene.getObjects("numero_milhar"));
gdjs.MainCode.GDnumero_95unidadeObjects2.createFrom(runtimeScene.getObjects("numero_unidade"));
gdjs.MainCode.GDunidadesObjects2.createFrom(runtimeScene.getObjects("unidades"));
{for(var i = 0, len = gdjs.MainCode.GDunidadesObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDunidadesObjects2[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.MainCode.GDdezenasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDdezenasObjects2[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.MainCode.GDcentenasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDcentenasObjects2[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.MainCode.GDmilharesObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDmilharesObjects2[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.MainCode.GDlblCentasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDlblCentasObjects2[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDcentenasObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDcentenasObjects2[0].getAnimation())) + " CENTENA(S)");
}
}{for(var i = 0, len = gdjs.MainCode.GDlblDezenhasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDlblDezenhasObjects2[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDdezenasObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDdezenasObjects2[0].getAnimation())) + " DEZENA(S)");
}
}{for(var i = 0, len = gdjs.MainCode.GDlblMilharesObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDlblMilharesObjects2[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDmilharesObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDmilharesObjects2[0].getAnimation())) + " MILHAR");
}
}{for(var i = 0, len = gdjs.MainCode.GDlblUnidadesObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDlblUnidadesObjects2[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDunidadesObjects2.length === 0 ) ? 0 :gdjs.MainCode.GDunidadesObjects2[0].getAnimation())) + " UNIDADE(S)");
}
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95unidadeObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95unidadeObjects2[i].setAnimation(gdjs.random(9));
}
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95dezenaObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95dezenaObjects2[i].setAnimation(gdjs.random(9));
}
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95centenaObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95centenaObjects2[i].setAnimation(gdjs.random(9));
}
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95milharObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95milharObjects2[i].setAnimation(gdjs.random(9));
}
}}

}


{


{
{gdjs.evtTools.camera.hideLayer(runtimeScene, "conferir");
}}

}


}; //End of gdjs.MainCode.eventsList0x6e7728
gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDunidadesObjects1Objects = Hashtable.newFrom({"unidades": gdjs.MainCode.GDunidadesObjects1});gdjs.MainCode.eventsList0x6e89d0 = function(runtimeScene, context) {

{

gdjs.MainCode.GDunidadesObjects2.createFrom(gdjs.MainCode.GDunidadesObjects1);


gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDunidadesObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDunidadesObjects2[i].getAnimation() <= 9 ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDunidadesObjects2[k] = gdjs.MainCode.GDunidadesObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDunidadesObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDunidadesObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDunidadesObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDunidadesObjects2[i].setAnimation((gdjs.MainCode.GDunidadesObjects2[i].getAnimation()) + 1);
}
}}

}


{

gdjs.MainCode.GDunidadesObjects2.createFrom(gdjs.MainCode.GDunidadesObjects1);


gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDunidadesObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDunidadesObjects2[i].getAnimation() > 9 ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDunidadesObjects2[k] = gdjs.MainCode.GDunidadesObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDunidadesObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDunidadesObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDunidadesObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDunidadesObjects2[i].setAnimation(0);
}
}}

}


{


{
gdjs.MainCode.GDlblUnidadesObjects1.createFrom(runtimeScene.getObjects("lblUnidades"));
/* Reuse gdjs.MainCode.GDunidadesObjects1 */
{runtimeScene.getGame().getVariables().get("numero_unidade").setNumber((( gdjs.MainCode.GDunidadesObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDunidadesObjects1[0].getAnimation()));
}{for(var i = 0, len = gdjs.MainCode.GDlblUnidadesObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDlblUnidadesObjects1[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDunidadesObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDunidadesObjects1[0].getAnimation())) + " UNIDADE(S)");
}
}}

}


}; //End of gdjs.MainCode.eventsList0x6e89d0
gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDdezenasObjects1Objects = Hashtable.newFrom({"dezenas": gdjs.MainCode.GDdezenasObjects1});gdjs.MainCode.eventsList0x6e93b0 = function(runtimeScene, context) {

{

gdjs.MainCode.GDdezenasObjects2.createFrom(gdjs.MainCode.GDdezenasObjects1);


gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDdezenasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDdezenasObjects2[i].getAnimation() <= 9 ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDdezenasObjects2[k] = gdjs.MainCode.GDdezenasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDdezenasObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDdezenasObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDdezenasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDdezenasObjects2[i].setAnimation((gdjs.MainCode.GDdezenasObjects2[i].getAnimation()) + 1);
}
}}

}


{

gdjs.MainCode.GDdezenasObjects2.createFrom(gdjs.MainCode.GDdezenasObjects1);


gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDdezenasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDdezenasObjects2[i].getAnimation() > 9 ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDdezenasObjects2[k] = gdjs.MainCode.GDdezenasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDdezenasObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDdezenasObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDdezenasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDdezenasObjects2[i].setAnimation(0);
}
}}

}


{


{
/* Reuse gdjs.MainCode.GDdezenasObjects1 */
gdjs.MainCode.GDlblDezenhasObjects1.createFrom(runtimeScene.getObjects("lblDezenhas"));
{runtimeScene.getGame().getVariables().get("numero_dezena").setNumber((( gdjs.MainCode.GDdezenasObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDdezenasObjects1[0].getAnimation()));
}{for(var i = 0, len = gdjs.MainCode.GDlblDezenhasObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDlblDezenhasObjects1[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDdezenasObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDdezenasObjects1[0].getAnimation())) + " DEZENA(S)");
}
}}

}


}; //End of gdjs.MainCode.eventsList0x6e93b0
gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDcentenasObjects1Objects = Hashtable.newFrom({"centenas": gdjs.MainCode.GDcentenasObjects1});gdjs.MainCode.eventsList0x6e9d90 = function(runtimeScene, context) {

{

gdjs.MainCode.GDcentenasObjects2.createFrom(gdjs.MainCode.GDcentenasObjects1);


gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDcentenasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDcentenasObjects2[i].getAnimation() <= 9 ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDcentenasObjects2[k] = gdjs.MainCode.GDcentenasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDcentenasObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDcentenasObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDcentenasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDcentenasObjects2[i].setAnimation((gdjs.MainCode.GDcentenasObjects2[i].getAnimation()) + 1);
}
}}

}


{

gdjs.MainCode.GDcentenasObjects2.createFrom(gdjs.MainCode.GDcentenasObjects1);


gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDcentenasObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDcentenasObjects2[i].getAnimation() > 9 ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDcentenasObjects2[k] = gdjs.MainCode.GDcentenasObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDcentenasObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDcentenasObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDcentenasObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDcentenasObjects2[i].setAnimation(0);
}
}}

}


{


{
/* Reuse gdjs.MainCode.GDcentenasObjects1 */
gdjs.MainCode.GDlblCentasObjects1.createFrom(runtimeScene.getObjects("lblCentas"));
{runtimeScene.getGame().getVariables().get("numero_centena").setNumber((( gdjs.MainCode.GDcentenasObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDcentenasObjects1[0].getAnimation()));
}{for(var i = 0, len = gdjs.MainCode.GDlblCentasObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDlblCentasObjects1[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDcentenasObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDcentenasObjects1[0].getAnimation())) + " CENTENA(S)");
}
}}

}


}; //End of gdjs.MainCode.eventsList0x6e9d90
gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDmilharesObjects1Objects = Hashtable.newFrom({"milhares": gdjs.MainCode.GDmilharesObjects1});gdjs.MainCode.eventsList0x6ea7a0 = function(runtimeScene, context) {

{

gdjs.MainCode.GDmilharesObjects2.createFrom(gdjs.MainCode.GDmilharesObjects1);


gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDmilharesObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDmilharesObjects2[i].getAnimation() <= 9 ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDmilharesObjects2[k] = gdjs.MainCode.GDmilharesObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDmilharesObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDmilharesObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDmilharesObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDmilharesObjects2[i].setAnimation((gdjs.MainCode.GDmilharesObjects2[i].getAnimation()) + 1);
}
}}

}


{

gdjs.MainCode.GDmilharesObjects2.createFrom(gdjs.MainCode.GDmilharesObjects1);


gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDmilharesObjects2.length;i<l;++i) {
    if ( gdjs.MainCode.GDmilharesObjects2[i].getAnimation() > 9 ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDmilharesObjects2[k] = gdjs.MainCode.GDmilharesObjects2[i];
        ++k;
    }
}
gdjs.MainCode.GDmilharesObjects2.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDmilharesObjects2 */
{for(var i = 0, len = gdjs.MainCode.GDmilharesObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDmilharesObjects2[i].setAnimation(0);
}
}}

}


{


{
gdjs.MainCode.GDlblMilharesObjects1.createFrom(runtimeScene.getObjects("lblMilhares"));
/* Reuse gdjs.MainCode.GDmilharesObjects1 */
{runtimeScene.getGame().getVariables().get("numero_milhar").setNumber((( gdjs.MainCode.GDmilharesObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDmilharesObjects1[0].getAnimation()));
}{for(var i = 0, len = gdjs.MainCode.GDlblMilharesObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDlblMilharesObjects1[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDmilharesObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDmilharesObjects1[0].getAnimation())) + " MILHAR");
}
}}

}


}; //End of gdjs.MainCode.eventsList0x6ea7a0
gdjs.MainCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDcentenasObjects1.createFrom(runtimeScene.getObjects("centenas"));
gdjs.MainCode.GDdezenasObjects1.createFrom(runtimeScene.getObjects("dezenas"));
gdjs.MainCode.GDlblCentasObjects1.createFrom(runtimeScene.getObjects("lblCentas"));
gdjs.MainCode.GDlblDezenhasObjects1.createFrom(runtimeScene.getObjects("lblDezenhas"));
gdjs.MainCode.GDlblMilharesObjects1.createFrom(runtimeScene.getObjects("lblMilhares"));
gdjs.MainCode.GDlblTrofeusObjects1.createFrom(runtimeScene.getObjects("lblTrofeus"));
gdjs.MainCode.GDlblUnidadesObjects1.createFrom(runtimeScene.getObjects("lblUnidades"));
gdjs.MainCode.GDmilharesObjects1.createFrom(runtimeScene.getObjects("milhares"));
gdjs.MainCode.GDnumero_95centenaObjects1.createFrom(runtimeScene.getObjects("numero_centena"));
gdjs.MainCode.GDnumero_95dezenaObjects1.createFrom(runtimeScene.getObjects("numero_dezena"));
gdjs.MainCode.GDnumero_95milharObjects1.createFrom(runtimeScene.getObjects("numero_milhar"));
gdjs.MainCode.GDnumero_95unidadeObjects1.createFrom(runtimeScene.getObjects("numero_unidade"));
gdjs.MainCode.GDunidadesObjects1.createFrom(runtimeScene.getObjects("unidades"));
{gdjs.evtTools.camera.hideLayer(runtimeScene, "conferir");
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95unidadeObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95unidadeObjects1[i].setAnimation(gdjs.random(9));
}
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95dezenaObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95dezenaObjects1[i].setAnimation(gdjs.random(9));
}
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95centenaObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95centenaObjects1[i].setAnimation(gdjs.random(9));
}
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95milharObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95milharObjects1[i].setAnimation(gdjs.random(9));
}
}{for(var i = 0, len = gdjs.MainCode.GDlblTrofeusObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDlblTrofeusObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}{for(var i = 0, len = gdjs.MainCode.GDunidadesObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDunidadesObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.MainCode.GDdezenasObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDdezenasObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.MainCode.GDcentenasObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDcentenasObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.MainCode.GDmilharesObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDmilharesObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.MainCode.GDlblMilharesObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDlblMilharesObjects1[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDmilharesObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDmilharesObjects1[0].getAnimation())) + " MILHAR");
}
}{for(var i = 0, len = gdjs.MainCode.GDlblCentasObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDlblCentasObjects1[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDcentenasObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDcentenasObjects1[0].getAnimation())) + " CENTENA(S)");
}
}{for(var i = 0, len = gdjs.MainCode.GDlblDezenhasObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDlblDezenhasObjects1[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDdezenasObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDdezenasObjects1[0].getAnimation())) + " DEZENA(S)");
}
}{for(var i = 0, len = gdjs.MainCode.GDlblUnidadesObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDlblUnidadesObjects1[i].setString(gdjs.evtTools.common.toString((( gdjs.MainCode.GDunidadesObjects1.length === 0 ) ? 0 :gdjs.MainCode.GDunidadesObjects1[0].getAnimation())) + " UNIDADE(S)");
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "glassbell.ogg", false, 100, 1);
}}

}


{



}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.MainCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x71ce70(runtimeScene, context);} //End of subevents
}

}


{

gdjs.MainCode.GDSORTEARObjects1.createFrom(runtimeScene.getObjects("SORTEAR"));

gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
gdjs.MainCode.condition2IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDSORTEARObjects1Objects, runtimeScene, true, false);
}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
gdjs.MainCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.MainCode.condition1IsTrue_0.val ) {
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition2IsTrue_0;
gdjs.MainCode.conditionTrue_1.val = context.triggerOnce(7232084);
}
}}
}
if (gdjs.MainCode.condition2IsTrue_0.val) {
gdjs.MainCode.GDnumero_95centenaObjects1.createFrom(runtimeScene.getObjects("numero_centena"));
gdjs.MainCode.GDnumero_95dezenaObjects1.createFrom(runtimeScene.getObjects("numero_dezena"));
gdjs.MainCode.GDnumero_95milharObjects1.createFrom(runtimeScene.getObjects("numero_milhar"));
gdjs.MainCode.GDnumero_95unidadeObjects1.createFrom(runtimeScene.getObjects("numero_unidade"));
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95unidadeObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95unidadeObjects1[i].setAnimation(gdjs.random(9));
}
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95dezenaObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95dezenaObjects1[i].setAnimation(gdjs.random(9));
}
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95centenaObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95centenaObjects1[i].setAnimation(gdjs.random(9));
}
}{for(var i = 0, len = gdjs.MainCode.GDnumero_95milharObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDnumero_95milharObjects1[i].setAnimation(gdjs.random(9));
}
}}

}


{

gdjs.MainCode.GDCONFERIRObjects1.createFrom(runtimeScene.getObjects("CONFERIR"));

gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
gdjs.MainCode.condition2IsTrue_0.val = false;
gdjs.MainCode.condition3IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDCONFERIRObjects1Objects, runtimeScene, true, false);
}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
gdjs.MainCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.MainCode.condition1IsTrue_0.val ) {
{
gdjs.MainCode.condition2IsTrue_0.val = !(gdjs.evtTools.camera.layerIsVisible(runtimeScene, "conferir"));
}if ( gdjs.MainCode.condition2IsTrue_0.val ) {
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition3IsTrue_0;
gdjs.MainCode.conditionTrue_1.val = context.triggerOnce(7233900);
}
}}
}
}
if (gdjs.MainCode.condition3IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x6e5f28(runtimeScene, context);} //End of subevents
}

}


{

gdjs.MainCode.GDparabensObjects1.length = 0;

gdjs.MainCode.GDtente_95novamenteObjects1.length = 0;


gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
gdjs.MainCode.condition2IsTrue_0.val = false;
gdjs.MainCode.condition3IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "conferir");
}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
gdjs.MainCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.MainCode.condition1IsTrue_0.val ) {
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition2IsTrue_0;
gdjs.MainCode.conditionTrue_1.val = context.triggerOnce(7239828);
}
}if ( gdjs.MainCode.condition2IsTrue_0.val ) {
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition3IsTrue_0;
gdjs.MainCode.GDparabensObjects1_1final.length = 0;gdjs.MainCode.GDtente_95novamenteObjects1_1final.length = 0;gdjs.MainCode.condition0IsTrue_1.val = false;
gdjs.MainCode.condition1IsTrue_1.val = false;
{
gdjs.MainCode.GDparabensObjects2.createFrom(runtimeScene.getObjects("parabens"));
gdjs.MainCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDparabensObjects2Objects, runtimeScene, true, false);
if( gdjs.MainCode.condition0IsTrue_1.val ) {
    gdjs.MainCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.MainCode.GDparabensObjects2.length;j<jLen;++j) {
        if ( gdjs.MainCode.GDparabensObjects1_1final.indexOf(gdjs.MainCode.GDparabensObjects2[j]) === -1 )
            gdjs.MainCode.GDparabensObjects1_1final.push(gdjs.MainCode.GDparabensObjects2[j]);
    }
}
}
{
gdjs.MainCode.GDtente_95novamenteObjects2.createFrom(runtimeScene.getObjects("tente_novamente"));
gdjs.MainCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDtente_9595novamenteObjects2Objects, runtimeScene, true, false);
if( gdjs.MainCode.condition1IsTrue_1.val ) {
    gdjs.MainCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.MainCode.GDtente_95novamenteObjects2.length;j<jLen;++j) {
        if ( gdjs.MainCode.GDtente_95novamenteObjects1_1final.indexOf(gdjs.MainCode.GDtente_95novamenteObjects2[j]) === -1 )
            gdjs.MainCode.GDtente_95novamenteObjects1_1final.push(gdjs.MainCode.GDtente_95novamenteObjects2[j]);
    }
}
}
{
gdjs.MainCode.GDparabensObjects1.createFrom(gdjs.MainCode.GDparabensObjects1_1final);
gdjs.MainCode.GDtente_95novamenteObjects1.createFrom(gdjs.MainCode.GDtente_95novamenteObjects1_1final);
}
}
}}
}
}
if (gdjs.MainCode.condition3IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x6e7728(runtimeScene, context);} //End of subevents
}

}


{



}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.MainCode.condition0IsTrue_0.val) {
}

}


{



}


{

gdjs.MainCode.GDunidadesObjects1.createFrom(runtimeScene.getObjects("unidades"));

gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
gdjs.MainCode.condition2IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDunidadesObjects1Objects, runtimeScene, true, false);
}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
gdjs.MainCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.MainCode.condition1IsTrue_0.val ) {
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition2IsTrue_0;
gdjs.MainCode.conditionTrue_1.val = context.triggerOnce(7244604);
}
}}
}
if (gdjs.MainCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x6e89d0(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.MainCode.GDdezenasObjects1.createFrom(runtimeScene.getObjects("dezenas"));

gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
gdjs.MainCode.condition2IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDdezenasObjects1Objects, runtimeScene, true, false);
}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
gdjs.MainCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.MainCode.condition1IsTrue_0.val ) {
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition2IsTrue_0;
gdjs.MainCode.conditionTrue_1.val = context.triggerOnce(7247132);
}
}}
}
if (gdjs.MainCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x6e93b0(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.MainCode.GDcentenasObjects1.createFrom(runtimeScene.getObjects("centenas"));

gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
gdjs.MainCode.condition2IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDcentenasObjects1Objects, runtimeScene, true, false);
}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
gdjs.MainCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.MainCode.condition1IsTrue_0.val ) {
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition2IsTrue_0;
gdjs.MainCode.conditionTrue_1.val = context.triggerOnce(7249732);
}
}}
}
if (gdjs.MainCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x6e9d90(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.MainCode.GDmilharesObjects1.createFrom(runtimeScene.getObjects("milhares"));

gdjs.MainCode.condition0IsTrue_0.val = false;
gdjs.MainCode.condition1IsTrue_0.val = false;
gdjs.MainCode.condition2IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDmilharesObjects1Objects, runtimeScene, true, false);
}if ( gdjs.MainCode.condition0IsTrue_0.val ) {
{
gdjs.MainCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.MainCode.condition1IsTrue_0.val ) {
{
{gdjs.MainCode.conditionTrue_1 = gdjs.MainCode.condition2IsTrue_0;
gdjs.MainCode.conditionTrue_1.val = context.triggerOnce(7252308);
}
}}
}
if (gdjs.MainCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.MainCode.eventsList0x6ea7a0(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.MainCode.eventsList0xaf630


gdjs.MainCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.MainCode.GDbackgroundObjects1.length = 0;
gdjs.MainCode.GDbackgroundObjects2.length = 0;
gdjs.MainCode.GDbackgroundObjects3.length = 0;
gdjs.MainCode.GDtrofeuObjects1.length = 0;
gdjs.MainCode.GDtrofeuObjects2.length = 0;
gdjs.MainCode.GDtrofeuObjects3.length = 0;
gdjs.MainCode.GDestrela1Objects1.length = 0;
gdjs.MainCode.GDestrela1Objects2.length = 0;
gdjs.MainCode.GDestrela1Objects3.length = 0;
gdjs.MainCode.GDparabensObjects1.length = 0;
gdjs.MainCode.GDparabensObjects2.length = 0;
gdjs.MainCode.GDparabensObjects3.length = 0;
gdjs.MainCode.GDunidadesObjects1.length = 0;
gdjs.MainCode.GDunidadesObjects2.length = 0;
gdjs.MainCode.GDunidadesObjects3.length = 0;
gdjs.MainCode.GDdezenasObjects1.length = 0;
gdjs.MainCode.GDdezenasObjects2.length = 0;
gdjs.MainCode.GDdezenasObjects3.length = 0;
gdjs.MainCode.GDcentenasObjects1.length = 0;
gdjs.MainCode.GDcentenasObjects2.length = 0;
gdjs.MainCode.GDcentenasObjects3.length = 0;
gdjs.MainCode.GDmilharesObjects1.length = 0;
gdjs.MainCode.GDmilharesObjects2.length = 0;
gdjs.MainCode.GDmilharesObjects3.length = 0;
gdjs.MainCode.GDlblUnidadesObjects1.length = 0;
gdjs.MainCode.GDlblUnidadesObjects2.length = 0;
gdjs.MainCode.GDlblUnidadesObjects3.length = 0;
gdjs.MainCode.GDlblDezenhasObjects1.length = 0;
gdjs.MainCode.GDlblDezenhasObjects2.length = 0;
gdjs.MainCode.GDlblDezenhasObjects3.length = 0;
gdjs.MainCode.GDlblCentasObjects1.length = 0;
gdjs.MainCode.GDlblCentasObjects2.length = 0;
gdjs.MainCode.GDlblCentasObjects3.length = 0;
gdjs.MainCode.GDlblMilharesObjects1.length = 0;
gdjs.MainCode.GDlblMilharesObjects2.length = 0;
gdjs.MainCode.GDlblMilharesObjects3.length = 0;
gdjs.MainCode.GDbtConferirObjects1.length = 0;
gdjs.MainCode.GDbtConferirObjects2.length = 0;
gdjs.MainCode.GDbtConferirObjects3.length = 0;
gdjs.MainCode.GDCONFERIRObjects1.length = 0;
gdjs.MainCode.GDCONFERIRObjects2.length = 0;
gdjs.MainCode.GDCONFERIRObjects3.length = 0;
gdjs.MainCode.GDqtdTrofeusObjects1.length = 0;
gdjs.MainCode.GDqtdTrofeusObjects2.length = 0;
gdjs.MainCode.GDqtdTrofeusObjects3.length = 0;
gdjs.MainCode.GDlblTrofeusObjects1.length = 0;
gdjs.MainCode.GDlblTrofeusObjects2.length = 0;
gdjs.MainCode.GDlblTrofeusObjects3.length = 0;
gdjs.MainCode.GDnumero_95unidadeObjects1.length = 0;
gdjs.MainCode.GDnumero_95unidadeObjects2.length = 0;
gdjs.MainCode.GDnumero_95unidadeObjects3.length = 0;
gdjs.MainCode.GDnumero_95dezenaObjects1.length = 0;
gdjs.MainCode.GDnumero_95dezenaObjects2.length = 0;
gdjs.MainCode.GDnumero_95dezenaObjects3.length = 0;
gdjs.MainCode.GDnumero_95centenaObjects1.length = 0;
gdjs.MainCode.GDnumero_95centenaObjects2.length = 0;
gdjs.MainCode.GDnumero_95centenaObjects3.length = 0;
gdjs.MainCode.GDnumero_95milharObjects1.length = 0;
gdjs.MainCode.GDnumero_95milharObjects2.length = 0;
gdjs.MainCode.GDnumero_95milharObjects3.length = 0;
gdjs.MainCode.GDupObjects1.length = 0;
gdjs.MainCode.GDupObjects2.length = 0;
gdjs.MainCode.GDupObjects3.length = 0;
gdjs.MainCode.GDSORTEARObjects1.length = 0;
gdjs.MainCode.GDSORTEARObjects2.length = 0;
gdjs.MainCode.GDSORTEARObjects3.length = 0;
gdjs.MainCode.GDdownObjects1.length = 0;
gdjs.MainCode.GDdownObjects2.length = 0;
gdjs.MainCode.GDdownObjects3.length = 0;
gdjs.MainCode.GDtente_95novamenteObjects1.length = 0;
gdjs.MainCode.GDtente_95novamenteObjects2.length = 0;
gdjs.MainCode.GDtente_95novamenteObjects3.length = 0;

gdjs.MainCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['MainCode']= gdjs.MainCode;
