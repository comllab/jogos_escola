gdjs.StartCode = {};
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDtrofeuObjects1= [];
gdjs.StartCode.GDtrofeuObjects2= [];
gdjs.StartCode.GDestrelaSequenciaObjects1= [];
gdjs.StartCode.GDestrelaSequenciaObjects2= [];
gdjs.StartCode.GDestrelaObjects1= [];
gdjs.StartCode.GDestrelaObjects2= [];
gdjs.StartCode.GDparabensObjects1= [];
gdjs.StartCode.GDparabensObjects2= [];
gdjs.StartCode.GDtituloObjects1= [];
gdjs.StartCode.GDtituloObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};


gdjs.StartCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "glassbell.ogg", false, 100, 1);
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Main", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xaff48


gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDtrofeuObjects1.length = 0;
gdjs.StartCode.GDtrofeuObjects2.length = 0;
gdjs.StartCode.GDestrelaSequenciaObjects1.length = 0;
gdjs.StartCode.GDestrelaSequenciaObjects2.length = 0;
gdjs.StartCode.GDestrelaObjects1.length = 0;
gdjs.StartCode.GDestrelaObjects2.length = 0;
gdjs.StartCode.GDparabensObjects1.length = 0;
gdjs.StartCode.GDparabensObjects2.length = 0;
gdjs.StartCode.GDtituloObjects1.length = 0;
gdjs.StartCode.GDtituloObjects2.length = 0;

gdjs.StartCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['StartCode'] = gdjs.StartCode;
