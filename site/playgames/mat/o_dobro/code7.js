gdjs.FinalCode = {};


gdjs.FinalCode.GDfundoObjects1= [];
gdjs.FinalCode.GDfaseObjects1= [];
gdjs.FinalCode.GDparabensObjects1= [];
gdjs.FinalCode.GDbtnProximaObjects1= [];
gdjs.FinalCode.GDreiniciarObjects1= [];
gdjs.FinalCode.GDautorObjects1= [];
gdjs.FinalCode.GDmissaoObjects1= [];

gdjs.FinalCode.conditionTrue_0 = {val:false};
gdjs.FinalCode.condition0IsTrue_0 = {val:false};
gdjs.FinalCode.condition1IsTrue_0 = {val:false};
gdjs.FinalCode.condition2IsTrue_0 = {val:false};

gdjs.FinalCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.FinalCode.GDfundoObjects1.length = 0;
gdjs.FinalCode.GDfaseObjects1.length = 0;
gdjs.FinalCode.GDparabensObjects1.length = 0;
gdjs.FinalCode.GDbtnProximaObjects1.length = 0;
gdjs.FinalCode.GDreiniciarObjects1.length = 0;
gdjs.FinalCode.GDautorObjects1.length = 0;
gdjs.FinalCode.GDmissaoObjects1.length = 0;


{

gdjs.FinalCode.GDfundoObjects1.createFrom(runtimeScene.getObjects("fundo"));

{for(var i = 0, len = gdjs.FinalCode.GDfundoObjects1.length ;i < len;++i) {
    gdjs.FinalCode.GDfundoObjects1[i].setYOffset(gdjs.FinalCode.GDfundoObjects1[i].getYOffset() + (5));
}
}
}


{

gdjs.FinalCode.GDbtnProximaObjects1.createFrom(runtimeScene.getObjects("btnProxima"));
gdjs.FinalCode.GDreiniciarObjects1.createFrom(runtimeScene.getObjects("reiniciar"));

gdjs.FinalCode.condition0IsTrue_0.val = false;
{
gdjs.FinalCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.FinalCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.FinalCode.GDreiniciarObjects1.length ;i < len;++i) {
    gdjs.FinalCode.GDreiniciarObjects1[i].setPosition((( gdjs.FinalCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.FinalCode.GDbtnProximaObjects1[0].getPointX("")) + (( gdjs.FinalCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.FinalCode.GDbtnProximaObjects1[0].getWidth())/2 - (gdjs.FinalCode.GDreiniciarObjects1[i].getWidth())/2,(( gdjs.FinalCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.FinalCode.GDbtnProximaObjects1[0].getPointY("")) + (( gdjs.FinalCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.FinalCode.GDbtnProximaObjects1[0].getHeight())/2 - (gdjs.FinalCode.GDreiniciarObjects1[i].getHeight())/2);
}
}{gdjs.evtTools.sound.playMusic(runtimeScene, "1x level win.ogg", true, 100, 1);
}}

}


{

gdjs.FinalCode.GDbtnProximaObjects1.createFrom(runtimeScene.getObjects("btnProxima"));

gdjs.FinalCode.condition0IsTrue_0.val = false;
gdjs.FinalCode.condition1IsTrue_0.val = false;
{
gdjs.FinalCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("btnProxima", gdjs.FinalCode.GDbtnProximaObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if ( gdjs.FinalCode.condition0IsTrue_0.val ) {
{
gdjs.FinalCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.FinalCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).add(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}

return;
}
gdjs['FinalCode']= gdjs.FinalCode;
