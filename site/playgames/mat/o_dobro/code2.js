gdjs.EndCode = {};
gdjs.EndCode.GDendObjects1= [];
gdjs.EndCode.GDendObjects2= [];
gdjs.EndCode.GDendObjects3= [];
gdjs.EndCode.GDgreen_95buttonObjects1= [];
gdjs.EndCode.GDgreen_95buttonObjects2= [];
gdjs.EndCode.GDgreen_95buttonObjects3= [];
gdjs.EndCode.GDlbl_95jogarObjects1= [];
gdjs.EndCode.GDlbl_95jogarObjects2= [];
gdjs.EndCode.GDlbl_95jogarObjects3= [];
gdjs.EndCode.GDbackgroundObjects1= [];
gdjs.EndCode.GDbackgroundObjects2= [];
gdjs.EndCode.GDbackgroundObjects3= [];

gdjs.EndCode.conditionTrue_0 = {val:false};
gdjs.EndCode.condition0IsTrue_0 = {val:false};
gdjs.EndCode.condition1IsTrue_0 = {val:false};
gdjs.EndCode.condition2IsTrue_0 = {val:false};
gdjs.EndCode.condition3IsTrue_0 = {val:false};


gdjs.EndCode.eventsList0x7e2538 = function(runtimeScene, context) {

{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) == 4;
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDendObjects2.createFrom(runtimeScene.getObjects("end"));
gdjs.EndCode.GDlbl_95jogarObjects2.createFrom(runtimeScene.getObjects("lbl_jogar"));
{for(var i = 0, len = gdjs.EndCode.GDendObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDendObjects2[i].setString("Fase " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(8)) + " completa!");
}
}{gdjs.evtTools.sound.playMusic(runtimeScene, "1x level win.ogg", true, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(8).add(1);
}{for(var i = 0, len = gdjs.EndCode.GDlbl_95jogarObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDlbl_95jogarObjects2[i].setString("FASE " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(8)));
}
}{for(var i = 0, len = gdjs.EndCode.GDendObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDendObjects2[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.EndCode.GDendObjects2[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.EndCode.GDlbl_95jogarObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDlbl_95jogarObjects2[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.EndCode.GDlbl_95jogarObjects2[i].getWidth())/2);
}
}}

}


{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) < 4;
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDendObjects2.createFrom(runtimeScene.getObjects("end"));
gdjs.EndCode.GDlbl_95jogarObjects2.createFrom(runtimeScene.getObjects("lbl_jogar"));
{for(var i = 0, len = gdjs.EndCode.GDendObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDendObjects2[i].setString("GAME OVER");
}
}{for(var i = 0, len = gdjs.EndCode.GDlbl_95jogarObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDlbl_95jogarObjects2[i].setString("MENU");
}
}{for(var i = 0, len = gdjs.EndCode.GDendObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDendObjects2[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.EndCode.GDendObjects2[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.EndCode.GDlbl_95jogarObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDlbl_95jogarObjects2[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.EndCode.GDlbl_95jogarObjects2[i].getWidth())/2);
}
}{gdjs.evtTools.sound.playMusic(runtimeScene, "1x game over.ogg", true, 100, 1);
}}

}


{


{
gdjs.EndCode.GDendObjects2.createFrom(runtimeScene.getObjects("end"));
{for(var i = 0, len = gdjs.EndCode.GDendObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDendObjects2[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.EndCode.GDendObjects2[i].getWidth())/2);
}
}}

}


{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(8)) > 4;
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDendObjects1.createFrom(runtimeScene.getObjects("end"));
gdjs.EndCode.GDlbl_95jogarObjects1.createFrom(runtimeScene.getObjects("lbl_jogar"));
{for(var i = 0, len = gdjs.EndCode.GDendObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDendObjects1[i].setString("Nivel completado!");
}
}{for(var i = 0, len = gdjs.EndCode.GDlbl_95jogarObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDlbl_95jogarObjects1[i].setString("MENU");
}
}{for(var i = 0, len = gdjs.EndCode.GDlbl_95jogarObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDlbl_95jogarObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.EndCode.GDlbl_95jogarObjects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.EndCode.GDendObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDendObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.EndCode.GDendObjects1[i].getWidth())/2);
}
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(0);
}}

}


}; //End of gdjs.EndCode.eventsList0x7e2538
gdjs.EndCode.eventsList0x7ba478 = function(runtimeScene, context) {

{

gdjs.EndCode.GDendObjects2.createFrom(runtimeScene.getObjects("end"));

gdjs.EndCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.EndCode.GDendObjects2.length;i<l;++i) {
    if ( gdjs.EndCode.GDendObjects2[i].getY() > 185 ) {
        gdjs.EndCode.condition0IsTrue_0.val = true;
        gdjs.EndCode.GDendObjects2[k] = gdjs.EndCode.GDendObjects2[i];
        ++k;
    }
}
gdjs.EndCode.GDendObjects2.length = k;}if (gdjs.EndCode.condition0IsTrue_0.val) {
/* Reuse gdjs.EndCode.GDendObjects2 */
{for(var i = 0, len = gdjs.EndCode.GDendObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDendObjects2[i].addForce(0, -15, 1);
}
}}

}


{

gdjs.EndCode.GDendObjects1.createFrom(runtimeScene.getObjects("end"));

gdjs.EndCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.EndCode.GDendObjects1.length;i<l;++i) {
    if ( gdjs.EndCode.GDendObjects1[i].getY() <= 185 ) {
        gdjs.EndCode.condition0IsTrue_0.val = true;
        gdjs.EndCode.GDendObjects1[k] = gdjs.EndCode.GDendObjects1[i];
        ++k;
    }
}
gdjs.EndCode.GDendObjects1.length = k;}if (gdjs.EndCode.condition0IsTrue_0.val) {
/* Reuse gdjs.EndCode.GDendObjects1 */
{for(var i = 0, len = gdjs.EndCode.GDendObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDendObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.EndCode.GDendObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDendObjects1[i].setY(185);
}
}}

}


}; //End of gdjs.EndCode.eventsList0x7ba478
gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDgreen_9595buttonObjects1Objects = Hashtable.newFrom({"green_button": gdjs.EndCode.GDgreen_95buttonObjects1});gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDgreen_9595buttonObjects1Objects = Hashtable.newFrom({"green_button": gdjs.EndCode.GDgreen_95buttonObjects1});gdjs.EndCode.eventsList0xaf580 = function(runtimeScene, context) {

{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDgreen_95buttonObjects1.createFrom(runtimeScene.getObjects("green_button"));
{for(var i = 0, len = gdjs.EndCode.GDgreen_95buttonObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDgreen_95buttonObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.EndCode.GDgreen_95buttonObjects1[i].getWidth())/2);
}
}
{ //Subevents
gdjs.EndCode.eventsList0x7e2538(runtimeScene, context);} //End of subevents
}

}


{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDbackgroundObjects1.createFrom(runtimeScene.getObjects("background"));
{for(var i = 0, len = gdjs.EndCode.GDbackgroundObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDbackgroundObjects1[i].setYOffset(gdjs.EndCode.GDbackgroundObjects1[i].getYOffset() + (3));
}
}
{ //Subevents
gdjs.EndCode.eventsList0x7ba478(runtimeScene, context);} //End of subevents
}

}


{

gdjs.EndCode.GDgreen_95buttonObjects1.createFrom(runtimeScene.getObjects("green_button"));

gdjs.EndCode.condition0IsTrue_0.val = false;
gdjs.EndCode.condition1IsTrue_0.val = false;
gdjs.EndCode.condition2IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDgreen_9595buttonObjects1Objects, runtimeScene, true, false);
}if ( gdjs.EndCode.condition0IsTrue_0.val ) {
{
gdjs.EndCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.EndCode.condition1IsTrue_0.val ) {
{
gdjs.EndCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) == 4;
}}
}
if (gdjs.EndCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{

gdjs.EndCode.GDgreen_95buttonObjects1.createFrom(runtimeScene.getObjects("green_button"));

gdjs.EndCode.condition0IsTrue_0.val = false;
gdjs.EndCode.condition1IsTrue_0.val = false;
gdjs.EndCode.condition2IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDgreen_9595buttonObjects1Objects, runtimeScene, true, false);
}if ( gdjs.EndCode.condition0IsTrue_0.val ) {
{
gdjs.EndCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.EndCode.condition1IsTrue_0.val ) {
{
gdjs.EndCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) < 4;
}}
}
if (gdjs.EndCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


}; //End of gdjs.EndCode.eventsList0xaf580


gdjs.EndCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.EndCode.GDendObjects1.length = 0;
gdjs.EndCode.GDendObjects2.length = 0;
gdjs.EndCode.GDendObjects3.length = 0;
gdjs.EndCode.GDgreen_95buttonObjects1.length = 0;
gdjs.EndCode.GDgreen_95buttonObjects2.length = 0;
gdjs.EndCode.GDgreen_95buttonObjects3.length = 0;
gdjs.EndCode.GDlbl_95jogarObjects1.length = 0;
gdjs.EndCode.GDlbl_95jogarObjects2.length = 0;
gdjs.EndCode.GDlbl_95jogarObjects3.length = 0;
gdjs.EndCode.GDbackgroundObjects1.length = 0;
gdjs.EndCode.GDbackgroundObjects2.length = 0;
gdjs.EndCode.GDbackgroundObjects3.length = 0;

gdjs.EndCode.eventsList0xaf580(runtimeScene, context);return;
}
gdjs['EndCode']= gdjs.EndCode;
