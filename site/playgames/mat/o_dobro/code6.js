gdjs.ReiniciarFaseCode = {};


gdjs.ReiniciarFaseCode.GDfundoObjects1= [];
gdjs.ReiniciarFaseCode.GDfaseObjects1= [];
gdjs.ReiniciarFaseCode.GDconcluidaObjects1= [];
gdjs.ReiniciarFaseCode.GDbtnProximaObjects1= [];
gdjs.ReiniciarFaseCode.GDtxtProximaObjects1= [];

gdjs.ReiniciarFaseCode.conditionTrue_0 = {val:false};
gdjs.ReiniciarFaseCode.condition0IsTrue_0 = {val:false};
gdjs.ReiniciarFaseCode.condition1IsTrue_0 = {val:false};
gdjs.ReiniciarFaseCode.condition2IsTrue_0 = {val:false};

gdjs.ReiniciarFaseCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.ReiniciarFaseCode.GDfundoObjects1.length = 0;
gdjs.ReiniciarFaseCode.GDfaseObjects1.length = 0;
gdjs.ReiniciarFaseCode.GDconcluidaObjects1.length = 0;
gdjs.ReiniciarFaseCode.GDbtnProximaObjects1.length = 0;
gdjs.ReiniciarFaseCode.GDtxtProximaObjects1.length = 0;


{

gdjs.ReiniciarFaseCode.GDfundoObjects1.createFrom(runtimeScene.getObjects("fundo"));

{for(var i = 0, len = gdjs.ReiniciarFaseCode.GDfundoObjects1.length ;i < len;++i) {
    gdjs.ReiniciarFaseCode.GDfundoObjects1[i].setYOffset(gdjs.ReiniciarFaseCode.GDfundoObjects1[i].getYOffset() + (5));
}
}
}


{

gdjs.ReiniciarFaseCode.GDbtnProximaObjects1.createFrom(runtimeScene.getObjects("btnProxima"));
gdjs.ReiniciarFaseCode.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));
gdjs.ReiniciarFaseCode.GDtxtProximaObjects1.createFrom(runtimeScene.getObjects("txtProxima"));

gdjs.ReiniciarFaseCode.condition0IsTrue_0.val = false;
{
gdjs.ReiniciarFaseCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.ReiniciarFaseCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.ReiniciarFaseCode.GDfaseObjects1.length ;i < len;++i) {
    gdjs.ReiniciarFaseCode.GDfaseObjects1[i].setString("FASE " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(3)));
}
}{for(var i = 0, len = gdjs.ReiniciarFaseCode.GDtxtProximaObjects1.length ;i < len;++i) {
    gdjs.ReiniciarFaseCode.GDtxtProximaObjects1[i].setString("FASE " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3))));
}
}{for(var i = 0, len = gdjs.ReiniciarFaseCode.GDtxtProximaObjects1.length ;i < len;++i) {
    gdjs.ReiniciarFaseCode.GDtxtProximaObjects1[i].setPosition((( gdjs.ReiniciarFaseCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.ReiniciarFaseCode.GDbtnProximaObjects1[0].getPointX("")) + (( gdjs.ReiniciarFaseCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.ReiniciarFaseCode.GDbtnProximaObjects1[0].getWidth())/2 - (gdjs.ReiniciarFaseCode.GDtxtProximaObjects1[i].getWidth())/2,(( gdjs.ReiniciarFaseCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.ReiniciarFaseCode.GDbtnProximaObjects1[0].getPointY("")) + (( gdjs.ReiniciarFaseCode.GDbtnProximaObjects1.length === 0 ) ? 0 :gdjs.ReiniciarFaseCode.GDbtnProximaObjects1[0].getHeight())/2 - (gdjs.ReiniciarFaseCode.GDtxtProximaObjects1[i].getHeight())/2);
}
}{gdjs.evtTools.sound.playMusic(runtimeScene, "1x game over.ogg", true, 100, 1);
}}

}


{

gdjs.ReiniciarFaseCode.GDbtnProximaObjects1.createFrom(runtimeScene.getObjects("btnProxima"));

gdjs.ReiniciarFaseCode.condition0IsTrue_0.val = false;
gdjs.ReiniciarFaseCode.condition1IsTrue_0.val = false;
{
gdjs.ReiniciarFaseCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("btnProxima", gdjs.ReiniciarFaseCode.GDbtnProximaObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if ( gdjs.ReiniciarFaseCode.condition0IsTrue_0.val ) {
{
gdjs.ReiniciarFaseCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.ReiniciarFaseCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Fase_" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(3)), false);
}}

}

return;
}
gdjs['ReiniciarFaseCode']= gdjs.ReiniciarFaseCode;
