gdjs.GameOverCode = {};
gdjs.GameOverCode.GDbackgroundObjects1= [];
gdjs.GameOverCode.GDbackgroundObjects2= [];
gdjs.GameOverCode.GDNewObjectObjects1= [];
gdjs.GameOverCode.GDNewObjectObjects2= [];
gdjs.GameOverCode.GDNewObject2Objects1= [];
gdjs.GameOverCode.GDNewObject2Objects2= [];
gdjs.GameOverCode.GDrestartObjects1= [];
gdjs.GameOverCode.GDrestartObjects2= [];
gdjs.GameOverCode.GDNewObject3Objects1= [];
gdjs.GameOverCode.GDNewObject3Objects2= [];

gdjs.GameOverCode.conditionTrue_0 = {val:false};
gdjs.GameOverCode.condition0IsTrue_0 = {val:false};
gdjs.GameOverCode.condition1IsTrue_0 = {val:false};
gdjs.GameOverCode.condition2IsTrue_0 = {val:false};
gdjs.GameOverCode.condition3IsTrue_0 = {val:false};
gdjs.GameOverCode.conditionTrue_1 = {val:false};
gdjs.GameOverCode.condition0IsTrue_1 = {val:false};
gdjs.GameOverCode.condition1IsTrue_1 = {val:false};
gdjs.GameOverCode.condition2IsTrue_1 = {val:false};
gdjs.GameOverCode.condition3IsTrue_1 = {val:false};


gdjs.GameOverCode.eventsList0x933b50 = function(runtimeScene, context) {

{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}if (gdjs.GameOverCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Boss Theme.ogg", 1, false, 40, 1);
}}

}


}; //End of gdjs.GameOverCode.eventsList0x933b50
gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDrestartObjects1Objects = Hashtable.newFrom({"restart": gdjs.GameOverCode.GDrestartObjects1});gdjs.GameOverCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameOverCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}
{ //Subevents
gdjs.GameOverCode.eventsList0x933b50(runtimeScene, context);} //End of subevents
}

}


{

gdjs.GameOverCode.GDrestartObjects1.createFrom(runtimeScene.getObjects("restart"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
gdjs.GameOverCode.condition2IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDrestartObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameOverCode.condition1IsTrue_0.val ) {
{
{gdjs.GameOverCode.conditionTrue_1 = gdjs.GameOverCode.condition2IsTrue_0;
gdjs.GameOverCode.conditionTrue_1.val = context.triggerOnce(8467588);
}
}}
}
if (gdjs.GameOverCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.GameOverCode.eventsList0xaf630


gdjs.GameOverCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GameOverCode.GDbackgroundObjects1.length = 0;
gdjs.GameOverCode.GDbackgroundObjects2.length = 0;
gdjs.GameOverCode.GDNewObjectObjects1.length = 0;
gdjs.GameOverCode.GDNewObjectObjects2.length = 0;
gdjs.GameOverCode.GDNewObject2Objects1.length = 0;
gdjs.GameOverCode.GDNewObject2Objects2.length = 0;
gdjs.GameOverCode.GDrestartObjects1.length = 0;
gdjs.GameOverCode.GDrestartObjects2.length = 0;
gdjs.GameOverCode.GDNewObject3Objects1.length = 0;
gdjs.GameOverCode.GDNewObject3Objects2.length = 0;

gdjs.GameOverCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['GameOverCode']= gdjs.GameOverCode;
