gdjs.GameCode = {};
gdjs.GameCode.repeatCount3 = 0;

gdjs.GameCode.repeatIndex3 = 0;

gdjs.GameCode.GDbackgroundObjects1= [];
gdjs.GameCode.GDbackgroundObjects2= [];
gdjs.GameCode.GDbackgroundObjects3= [];
gdjs.GameCode.GDbackgroundObjects4= [];
gdjs.GameCode.GDbackgroundObjects5= [];
gdjs.GameCode.GDmoldesObjects1= [];
gdjs.GameCode.GDmoldesObjects2= [];
gdjs.GameCode.GDmoldesObjects3= [];
gdjs.GameCode.GDmoldesObjects4= [];
gdjs.GameCode.GDmoldesObjects5= [];
gdjs.GameCode.GDquebra_95cabeca6Objects1= [];
gdjs.GameCode.GDquebra_95cabeca6Objects2= [];
gdjs.GameCode.GDquebra_95cabeca6Objects3= [];
gdjs.GameCode.GDquebra_95cabeca6Objects4= [];
gdjs.GameCode.GDquebra_95cabeca6Objects5= [];
gdjs.GameCode.GDquebra_95cabeca5Objects1= [];
gdjs.GameCode.GDquebra_95cabeca5Objects2= [];
gdjs.GameCode.GDquebra_95cabeca5Objects3= [];
gdjs.GameCode.GDquebra_95cabeca5Objects4= [];
gdjs.GameCode.GDquebra_95cabeca5Objects5= [];
gdjs.GameCode.GDquebra_95cabeca4Objects1= [];
gdjs.GameCode.GDquebra_95cabeca4Objects2= [];
gdjs.GameCode.GDquebra_95cabeca4Objects3= [];
gdjs.GameCode.GDquebra_95cabeca4Objects4= [];
gdjs.GameCode.GDquebra_95cabeca4Objects5= [];
gdjs.GameCode.GDquebra_95cabeca3Objects1= [];
gdjs.GameCode.GDquebra_95cabeca3Objects2= [];
gdjs.GameCode.GDquebra_95cabeca3Objects3= [];
gdjs.GameCode.GDquebra_95cabeca3Objects4= [];
gdjs.GameCode.GDquebra_95cabeca3Objects5= [];
gdjs.GameCode.GDquebra_95cabeca2Objects1= [];
gdjs.GameCode.GDquebra_95cabeca2Objects2= [];
gdjs.GameCode.GDquebra_95cabeca2Objects3= [];
gdjs.GameCode.GDquebra_95cabeca2Objects4= [];
gdjs.GameCode.GDquebra_95cabeca2Objects5= [];
gdjs.GameCode.GDquebra_95cabeca1Objects1= [];
gdjs.GameCode.GDquebra_95cabeca1Objects2= [];
gdjs.GameCode.GDquebra_95cabeca1Objects3= [];
gdjs.GameCode.GDquebra_95cabeca1Objects4= [];
gdjs.GameCode.GDquebra_95cabeca1Objects5= [];
gdjs.GameCode.GDpanelObjects1= [];
gdjs.GameCode.GDpanelObjects2= [];
gdjs.GameCode.GDpanelObjects3= [];
gdjs.GameCode.GDpanelObjects4= [];
gdjs.GameCode.GDpanelObjects5= [];
gdjs.GameCode.GDtrofeuObjects1= [];
gdjs.GameCode.GDtrofeuObjects2= [];
gdjs.GameCode.GDtrofeuObjects3= [];
gdjs.GameCode.GDtrofeuObjects4= [];
gdjs.GameCode.GDtrofeuObjects5= [];
gdjs.GameCode.GDajudaObjects1= [];
gdjs.GameCode.GDajudaObjects2= [];
gdjs.GameCode.GDajudaObjects3= [];
gdjs.GameCode.GDajudaObjects4= [];
gdjs.GameCode.GDajudaObjects5= [];
gdjs.GameCode.GDpanel2Objects1= [];
gdjs.GameCode.GDpanel2Objects2= [];
gdjs.GameCode.GDpanel2Objects3= [];
gdjs.GameCode.GDpanel2Objects4= [];
gdjs.GameCode.GDpanel2Objects5= [];
gdjs.GameCode.GDparabensObjects1= [];
gdjs.GameCode.GDparabensObjects2= [];
gdjs.GameCode.GDparabensObjects3= [];
gdjs.GameCode.GDparabensObjects4= [];
gdjs.GameCode.GDparabensObjects5= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.condition4IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};
gdjs.GameCode.condition4IsTrue_1 = {val:false};


gdjs.GameCode.eventsList0x79de78 = function(runtimeScene) {

{

gdjs.GameCode.GDtrofeuObjects1.createFrom(runtimeScene.getObjects("trofeu"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDtrofeuObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDtrofeuObjects1[i].getVariableNumber(gdjs.GameCode.GDtrofeuObjects1[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDtrofeuObjects1[k] = gdjs.GameCode.GDtrofeuObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDtrofeuObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDtrofeuObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDtrofeuObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtrofeuObjects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x79de78
gdjs.GameCode.eventsList0x7b9b90 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca1Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getAnimation() < 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects4 */
{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca1Objects4.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca1Objects4[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca1Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca1Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca1Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca1Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca1Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca1Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca1Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca1Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects4[k] = gdjs.GameCode.GDquebra_95cabeca1Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects4 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca1Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca1Objects4[i].setAnimation(16);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x7b9b90
gdjs.GameCode.eventsList0x7b9aa8 = function(runtimeScene) {

{


gdjs.GameCode.repeatCount3 = 500;
for(gdjs.GameCode.repeatIndex3 = 0;gdjs.GameCode.repeatIndex3 < gdjs.GameCode.repeatCount3;++gdjs.GameCode.repeatIndex3) {

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val)
{
{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1,  4));
}{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(1,  4));
}
{ //Subevents: 
gdjs.GameCode.eventsList0x7b9b90(runtimeScene);} //Subevents end.
}
}

}


}; //End of gdjs.GameCode.eventsList0x7b9aa8
gdjs.GameCode.eventsList0x7b9978 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7b9aa8(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7b9978
gdjs.GameCode.eventsList0x7bbcd0 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca2Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getAnimation() < 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects4 */
{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca2Objects4.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca2Objects4[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca2Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca2Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca2Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca2Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca2Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca2Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca2Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca2Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects4[k] = gdjs.GameCode.GDquebra_95cabeca2Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects4 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca2Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca2Objects4[i].setAnimation(16);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x7bbcd0
gdjs.GameCode.eventsList0x7bbbe8 = function(runtimeScene) {

{


gdjs.GameCode.repeatCount3 = 500;
for(gdjs.GameCode.repeatIndex3 = 0;gdjs.GameCode.repeatIndex3 < gdjs.GameCode.repeatCount3;++gdjs.GameCode.repeatIndex3) {

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val)
{
{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1,  4));
}{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(1,  4));
}
{ //Subevents: 
gdjs.GameCode.eventsList0x7bbcd0(runtimeScene);} //Subevents end.
}
}

}


}; //End of gdjs.GameCode.eventsList0x7bbbe8
gdjs.GameCode.eventsList0x7bbab8 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7bbbe8(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7bbab8
gdjs.GameCode.eventsList0x7bde08 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca3Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getAnimation() < 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects4 */
{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca3Objects4.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca3Objects4[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca3Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca3Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca3Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca3Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca3Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca3Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca3Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca3Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects4[k] = gdjs.GameCode.GDquebra_95cabeca3Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects4 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca3Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca3Objects4[i].setAnimation(16);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x7bde08
gdjs.GameCode.eventsList0x7bdd20 = function(runtimeScene) {

{


gdjs.GameCode.repeatCount3 = 500;
for(gdjs.GameCode.repeatIndex3 = 0;gdjs.GameCode.repeatIndex3 < gdjs.GameCode.repeatCount3;++gdjs.GameCode.repeatIndex3) {

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val)
{
{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1,  4));
}{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(1,  4));
}
{ //Subevents: 
gdjs.GameCode.eventsList0x7bde08(runtimeScene);} //Subevents end.
}
}

}


}; //End of gdjs.GameCode.eventsList0x7bdd20
gdjs.GameCode.eventsList0x7bdc00 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7bdd20(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7bdc00
gdjs.GameCode.eventsList0x7bff48 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca4Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getAnimation() < 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects4 */
{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca4Objects4.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca4Objects4[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca4Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca4Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca4Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca4Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca4Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca4Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca4Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca4Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects4[k] = gdjs.GameCode.GDquebra_95cabeca4Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects4 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca4Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca4Objects4[i].setAnimation(16);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x7bff48
gdjs.GameCode.eventsList0x7bfe60 = function(runtimeScene) {

{


gdjs.GameCode.repeatCount3 = 500;
for(gdjs.GameCode.repeatIndex3 = 0;gdjs.GameCode.repeatIndex3 < gdjs.GameCode.repeatCount3;++gdjs.GameCode.repeatIndex3) {

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val)
{
{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1,  4));
}{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(1,  4));
}
{ //Subevents: 
gdjs.GameCode.eventsList0x7bff48(runtimeScene);} //Subevents end.
}
}

}


}; //End of gdjs.GameCode.eventsList0x7bfe60
gdjs.GameCode.eventsList0x7bfd48 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7bfe60(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7bfd48
gdjs.GameCode.eventsList0x7c2078 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca5Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getAnimation() < 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects4 */
{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca5Objects4.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca5Objects4[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca5Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca5Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca5Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca5Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca5Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca5Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca5Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca5Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects4[k] = gdjs.GameCode.GDquebra_95cabeca5Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects4 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca5Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca5Objects4[i].setAnimation(16);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x7c2078
gdjs.GameCode.eventsList0x7c1f90 = function(runtimeScene) {

{


gdjs.GameCode.repeatCount3 = 500;
for(gdjs.GameCode.repeatIndex3 = 0;gdjs.GameCode.repeatIndex3 < gdjs.GameCode.repeatCount3;++gdjs.GameCode.repeatIndex3) {

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val)
{
{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1,  4));
}{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(1,  4));
}
{ //Subevents: 
gdjs.GameCode.eventsList0x7c2078(runtimeScene);} //Subevents end.
}
}

}


}; //End of gdjs.GameCode.eventsList0x7c1f90
gdjs.GameCode.eventsList0x7c1e60 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7c1f90(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7c1e60
gdjs.GameCode.eventsList0x7c41f0 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca6Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getAnimation() < 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects4 */
{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca6Objects4.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca6Objects4[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca6Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca6Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca6Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca6Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca6Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca6Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects4 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca6Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca6Objects4[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects4.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects4.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects4[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects4[k] = gdjs.GameCode.GDquebra_95cabeca6Objects4[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects4 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca6Objects4.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca6Objects4[i].setAnimation(16);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x7c41f0
gdjs.GameCode.eventsList0x7c4108 = function(runtimeScene) {

{


gdjs.GameCode.repeatCount3 = 500;
for(gdjs.GameCode.repeatIndex3 = 0;gdjs.GameCode.repeatIndex3 < gdjs.GameCode.repeatCount3;++gdjs.GameCode.repeatIndex3) {

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val)
{
{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1,  4));
}{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(1,  4));
}
{ //Subevents: 
gdjs.GameCode.eventsList0x7c41f0(runtimeScene);} //Subevents end.
}
}

}


}; //End of gdjs.GameCode.eventsList0x7c4108
gdjs.GameCode.eventsList0x7c3fd8 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7c4108(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7c3fd8
gdjs.GameCode.eventsList0x7b9858 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7b9978(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 2;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7bbab8(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 3;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7bdc00(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 4;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7bfd48(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 5;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7c1e60(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 6;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7c3fd8(runtimeScene);} //End of subevents
}

}


{


{
{runtimeScene.getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}


}; //End of gdjs.GameCode.eventsList0x7b9858
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca1Objects2Objects = Hashtable.newFrom({"quebra_cabeca1": gdjs.GameCode.GDquebra_95cabeca1Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects = Hashtable.newFrom({"panel": gdjs.GameCode.GDpanelObjects1});gdjs.GameCode.eventsList0x7c6c40 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca1Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca1Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca1Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca1Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects2 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca1Objects2[i].setAnimation(16);
}
}}

}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


}; //End of gdjs.GameCode.eventsList0x7c6c40
gdjs.GameCode.eventsList0x7c6540 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca1Objects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8153076);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca1Objects2 */
{gdjs.evtTools.sound.playSound(runtimeScene, "qubodupImpactWood.ogg", false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(0).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca1Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca1Objects2[0].getVariables()).getFromIndex(2))));
}{runtimeScene.getVariables().getFromIndex(1).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca1Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca1Objects2[0].getVariables()).getFromIndex(1))));
}{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca1Objects2[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDpanelObjects1.createFrom(runtimeScene.getObjects("panel"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8154628);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7c6c40(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7c6540
gdjs.GameCode.eventsList0x7c6420 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7c6540(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7c6420
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca2Objects2Objects = Hashtable.newFrom({"quebra_cabeca2": gdjs.GameCode.GDquebra_95cabeca2Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects = Hashtable.newFrom({"panel": gdjs.GameCode.GDpanelObjects1});gdjs.GameCode.eventsList0x7c9050 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca2Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca2Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca2Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca2Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects2 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca2Objects2[i].setAnimation(16);
}
}}

}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


}; //End of gdjs.GameCode.eventsList0x7c9050
gdjs.GameCode.eventsList0x7c89a8 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca2Objects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8162396);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca2Objects2 */
{gdjs.evtTools.sound.playSound(runtimeScene, "qubodupImpactWood.ogg", false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(0).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca2Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca2Objects2[0].getVariables()).getFromIndex(2))));
}{runtimeScene.getVariables().getFromIndex(1).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca2Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca2Objects2[0].getVariables()).getFromIndex(1))));
}{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca2Objects2[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDpanelObjects1.createFrom(runtimeScene.getObjects("panel"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8163860);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7c9050(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7c89a8
gdjs.GameCode.eventsList0x7c8888 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7c89a8(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7c8888
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca3Objects2Objects = Hashtable.newFrom({"quebra_cabeca3": gdjs.GameCode.GDquebra_95cabeca3Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects = Hashtable.newFrom({"panel": gdjs.GameCode.GDpanelObjects1});gdjs.GameCode.eventsList0x7cb4b8 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca3Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca3Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca3Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca3Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects2 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca3Objects2[i].setAnimation(16);
}
}}

}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


}; //End of gdjs.GameCode.eventsList0x7cb4b8
gdjs.GameCode.eventsList0x7cadb8 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca3Objects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8171628);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca3Objects2 */
{gdjs.evtTools.sound.playSound(runtimeScene, "qubodupImpactWood.ogg", false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(0).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca3Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca3Objects2[0].getVariables()).getFromIndex(2))));
}{runtimeScene.getVariables().getFromIndex(1).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca3Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca3Objects2[0].getVariables()).getFromIndex(1))));
}{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca3Objects2[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDpanelObjects1.createFrom(runtimeScene.getObjects("panel"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8173180);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7cb4b8(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7cadb8
gdjs.GameCode.eventsList0x7cac98 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7cadb8(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7cac98
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca4Objects2Objects = Hashtable.newFrom({"quebra_cabeca4": gdjs.GameCode.GDquebra_95cabeca4Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects = Hashtable.newFrom({"panel": gdjs.GameCode.GDpanelObjects1});gdjs.GameCode.eventsList0x7cd920 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca4Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca4Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca4Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca4Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects2 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca4Objects2[i].setAnimation(16);
}
}}

}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


}; //End of gdjs.GameCode.eventsList0x7cd920
gdjs.GameCode.eventsList0x7cd220 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca4Objects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8180948);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca4Objects2 */
{gdjs.evtTools.sound.playSound(runtimeScene, "qubodupImpactWood.ogg", false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(0).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca4Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca4Objects2[0].getVariables()).getFromIndex(2))));
}{runtimeScene.getVariables().getFromIndex(1).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca4Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca4Objects2[0].getVariables()).getFromIndex(1))));
}{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca4Objects2[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDpanelObjects1.createFrom(runtimeScene.getObjects("panel"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8182500);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7cd920(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7cd220
gdjs.GameCode.eventsList0x7cd100 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7cd220(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7cd100
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca5Objects2Objects = Hashtable.newFrom({"quebra_cabeca5": gdjs.GameCode.GDquebra_95cabeca5Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects = Hashtable.newFrom({"panel": gdjs.GameCode.GDpanelObjects1});gdjs.GameCode.eventsList0x7cfd88 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca5Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca5Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca5Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca5Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects2 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca5Objects2[i].setAnimation(16);
}
}}

}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


}; //End of gdjs.GameCode.eventsList0x7cfd88
gdjs.GameCode.eventsList0x7cf688 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca5Objects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8190268);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca5Objects2 */
{gdjs.evtTools.sound.playSound(runtimeScene, "qubodupImpactWood.ogg", false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(0).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca5Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca5Objects2[0].getVariables()).getFromIndex(2))));
}{runtimeScene.getVariables().getFromIndex(1).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca5Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca5Objects2[0].getVariables()).getFromIndex(1))));
}{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca5Objects2[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDpanelObjects1.createFrom(runtimeScene.getObjects("panel"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8191820);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7cfd88(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7cf688
gdjs.GameCode.eventsList0x7cf568 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7cf688(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7cf568
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca6Objects2Objects = Hashtable.newFrom({"quebra_cabeca6": gdjs.GameCode.GDquebra_95cabeca6Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects = Hashtable.newFrom({"panel": gdjs.GameCode.GDpanelObjects1});gdjs.GameCode.eventsList0x7d21f0 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca6Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca6Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) - 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca6Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca6Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) + 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca6Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca6Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) - 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca6Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca6Objects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects2 */
{runtimeScene.getVariables().getFromIndex(2).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDquebra_95cabeca6Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDquebra_95cabeca6Objects2[i].setAnimation(16);
}
}}

}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


}; //End of gdjs.GameCode.eventsList0x7d21f0
gdjs.GameCode.eventsList0x7d1af0 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquebra_9595cabeca6Objects2Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8199588);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDquebra_95cabeca6Objects2 */
{gdjs.evtTools.sound.playSound(runtimeScene, "qubodupImpactWood.ogg", false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(0).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca6Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca6Objects2[0].getVariables()).getFromIndex(2))));
}{runtimeScene.getVariables().getFromIndex(1).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDquebra_95cabeca6Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDquebra_95cabeca6Objects2[0].getVariables()).getFromIndex(1))));
}{runtimeScene.getVariables().getFromIndex(3).setNumber((( gdjs.GameCode.GDquebra_95cabeca6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDquebra_95cabeca6Objects2[0].getAnimation()));
}}

}


{

gdjs.GameCode.GDpanelObjects1.createFrom(runtimeScene.getObjects("panel"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanelObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8201140);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7d21f0(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7d1af0
gdjs.GameCode.eventsList0x7d19d0 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7d1af0(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7d19d0
gdjs.GameCode.eventsList0x7a3450 = function(runtimeScene) {

{

gdjs.GameCode.GDtrofeuObjects1.createFrom(runtimeScene.getObjects("trofeu"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDtrofeuObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDtrofeuObjects1[i].getVariableNumber(gdjs.GameCode.GDtrofeuObjects1[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDtrofeuObjects1[k] = gdjs.GameCode.GDtrofeuObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDtrofeuObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDtrofeuObjects1 */
{gdjs.evtTools.sound.playSound(runtimeScene, "applause.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDtrofeuObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtrofeuObjects1[i].setAnimation(1);
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "Ganhou");
}}

}


}; //End of gdjs.GameCode.eventsList0x7a3450
gdjs.GameCode.eventsList0x79f870 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 1 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 2 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 3 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 4 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 5 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 6 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 7 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 8 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 9 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 10 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 11 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 12 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 13 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 14 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 15 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca1Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca1Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca1Objects2[k] = gdjs.GameCode.GDquebra_95cabeca1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 16;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}
{ //Subevents
gdjs.GameCode.eventsList0x7a3450(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x79f870
gdjs.GameCode.eventsList0x79f798 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}
{ //Subevents
gdjs.GameCode.eventsList0x79f870(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x79f798
gdjs.GameCode.eventsList0x79f690 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x79f798(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x79f690
gdjs.GameCode.eventsList0x7a3c60 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 1 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 2 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 3 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 4 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 5 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 6 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 7 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 8 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 9 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 10 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 11 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 12 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 13 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 14 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 15 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca2Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca2Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca2Objects2[k] = gdjs.GameCode.GDquebra_95cabeca2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 16;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "applause.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}{gdjs.evtTools.camera.showLayer(runtimeScene, "Ganhou");
}}

}


}; //End of gdjs.GameCode.eventsList0x7a3c60
gdjs.GameCode.eventsList0x7a3b88 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}
{ //Subevents
gdjs.GameCode.eventsList0x7a3c60(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7a3b88
gdjs.GameCode.eventsList0x7a3a90 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7a3b88(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7a3a90
gdjs.GameCode.eventsList0x7a7e38 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 1 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 2 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 3 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 4 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 5 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 6 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 7 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 8 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 9 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 10 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 11 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 12 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 13 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 14 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 15 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca3Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca3Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca3Objects2[k] = gdjs.GameCode.GDquebra_95cabeca3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 16;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "applause.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}{gdjs.evtTools.camera.showLayer(runtimeScene, "Ganhou");
}}

}


}; //End of gdjs.GameCode.eventsList0x7a7e38
gdjs.GameCode.eventsList0x7a7d60 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}
{ //Subevents
gdjs.GameCode.eventsList0x7a7e38(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7a7d60
gdjs.GameCode.eventsList0x7a7c68 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7a7d60(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7a7c68
gdjs.GameCode.eventsList0x7afb80 = function(runtimeScene) {

{

gdjs.GameCode.GDtrofeuObjects1.createFrom(runtimeScene.getObjects("trofeu"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDtrofeuObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDtrofeuObjects1[i].getVariableNumber(gdjs.GameCode.GDtrofeuObjects1[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDtrofeuObjects1[k] = gdjs.GameCode.GDtrofeuObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDtrofeuObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDtrofeuObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDtrofeuObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtrofeuObjects1[i].setAnimation(1);
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "Ganhou");
}}

}


}; //End of gdjs.GameCode.eventsList0x7afb80
gdjs.GameCode.eventsList0x7abfa0 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 1 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 2 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 3 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 4 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 5 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 6 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 7 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 8 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 9 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 10 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 11 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 12 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 13 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 14 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 15 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca4Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca4Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca4Objects2[k] = gdjs.GameCode.GDquebra_95cabeca4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 16;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "applause.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}
{ //Subevents
gdjs.GameCode.eventsList0x7afb80(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7abfa0
gdjs.GameCode.eventsList0x7abec8 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}
{ //Subevents
gdjs.GameCode.eventsList0x7abfa0(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7abec8
gdjs.GameCode.eventsList0x7abdd0 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7abec8(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7abdd0
gdjs.GameCode.eventsList0x7b3f58 = function(runtimeScene) {

{

gdjs.GameCode.GDtrofeuObjects1.createFrom(runtimeScene.getObjects("trofeu"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDtrofeuObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDtrofeuObjects1[i].getVariableNumber(gdjs.GameCode.GDtrofeuObjects1[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDtrofeuObjects1[k] = gdjs.GameCode.GDtrofeuObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDtrofeuObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDtrofeuObjects1 */
{gdjs.evtTools.sound.playSound(runtimeScene, "applause.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDtrofeuObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtrofeuObjects1[i].setAnimation(1);
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "Ganhou");
}}

}


}; //End of gdjs.GameCode.eventsList0x7b3f58
gdjs.GameCode.eventsList0x7b03c0 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 1 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 2 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 3 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 4 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 5 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 6 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 7 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 8 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 9 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 10 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 11 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 12 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 13 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 14 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 15 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca5Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca5Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca5Objects2[k] = gdjs.GameCode.GDquebra_95cabeca5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 16;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}
{ //Subevents
gdjs.GameCode.eventsList0x7b3f58(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7b03c0
gdjs.GameCode.eventsList0x7b02e8 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}
{ //Subevents
gdjs.GameCode.eventsList0x7b03c0(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7b02e8
gdjs.GameCode.eventsList0x7b01b8 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7b02e8(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7b01b8
gdjs.GameCode.eventsList0x7b8338 = function(runtimeScene) {

{

gdjs.GameCode.GDtrofeuObjects1.createFrom(runtimeScene.getObjects("trofeu"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDtrofeuObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDtrofeuObjects1[i].getVariableNumber(gdjs.GameCode.GDtrofeuObjects1[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDtrofeuObjects1[k] = gdjs.GameCode.GDtrofeuObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDtrofeuObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDtrofeuObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDtrofeuObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtrofeuObjects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x7b8338
gdjs.GameCode.eventsList0x7b47a0 = function(runtimeScene) {

{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 1 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 2 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 3 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 4 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 5 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 6 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 7 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 8 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 9 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 10 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 11 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 3 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 12 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 13 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 2 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 14 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 3 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 15 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{

gdjs.GameCode.GDquebra_95cabeca6Objects2.createFrom(runtimeScene.getObjects("quebra_cabeca6"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(1)) == 4 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariableNumber(gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getVariables().getFromIndex(2)) == 4 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDquebra_95cabeca6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDquebra_95cabeca6Objects2[i].getAnimation() == 16 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDquebra_95cabeca6Objects2[k] = gdjs.GameCode.GDquebra_95cabeca6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 16;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "applause.ogg", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}{gdjs.evtTools.camera.showLayer(runtimeScene, "Ganhou");
}
{ //Subevents
gdjs.GameCode.eventsList0x7b8338(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7b47a0
gdjs.GameCode.eventsList0x7b46c8 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}
{ //Subevents
gdjs.GameCode.eventsList0x7b47a0(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x7b46c8
gdjs.GameCode.eventsList0x7b4598 = function(runtimeScene) {

{


gdjs.GameCode.eventsList0x7b46c8(runtimeScene);
}


}; //End of gdjs.GameCode.eventsList0x7b4598
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDajudaObjects1Objects = Hashtable.newFrom({"ajuda": gdjs.GameCode.GDajudaObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmoldesObjects1Objects = Hashtable.newFrom({"moldes": gdjs.GameCode.GDmoldesObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanel2Objects1Objects = Hashtable.newFrom({"panel2": gdjs.GameCode.GDpanel2Objects1});gdjs.GameCode.eventsList0x7b8ff8 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) < 6;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).add(1);
}}

}


{


{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.GameCode.eventsList0x7b8ff8
gdjs.GameCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDmoldesObjects1.createFrom(runtimeScene.getObjects("moldes"));
{for(var i = 0, len = gdjs.GameCode.GDmoldesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmoldesObjects1[i].hide();
}
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "Ganhou");
}{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "QuebraCabeca" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(3)), 0, 0);
}{for(var i = 0, len = gdjs.GameCode.GDmoldesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmoldesObjects1[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3))-1);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x79de78(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7b9858(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7c6420(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 2;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7c8888(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 3;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7cac98(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 4;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7cd100(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 5;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7cf568(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 6;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7d19d0(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x79f690(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 2;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7a3a90(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 3;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7a7c68(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 4;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7abdd0(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 5;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7b01b8(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 6;
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x7b4598(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDajudaObjects1.createFrom(runtimeScene.getObjects("ajuda"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDajudaObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8096724);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
gdjs.GameCode.GDmoldesObjects1.createFrom(runtimeScene.getObjects("moldes"));
{for(var i = 0, len = gdjs.GameCode.GDmoldesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmoldesObjects1[i].hide(false);
}
}}

}


{

gdjs.GameCode.GDmoldesObjects1.createFrom(runtimeScene.getObjects("moldes"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmoldesObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8097452);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDmoldesObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDmoldesObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmoldesObjects1[i].hide();
}
}}

}


{

gdjs.GameCode.GDpanel2Objects1.createFrom(runtimeScene.getObjects("panel2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Ganhou");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpanel2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition3IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8098420);
}
}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
{gdjs.evtTools.camera.hideLayer(runtimeScene, "Ganhou");
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}
{ //Subevents
gdjs.GameCode.eventsList0x7b8ff8(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0xaff48


gdjs.GameCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.GameCode.GDbackgroundObjects1.length = 0;
gdjs.GameCode.GDbackgroundObjects2.length = 0;
gdjs.GameCode.GDbackgroundObjects3.length = 0;
gdjs.GameCode.GDbackgroundObjects4.length = 0;
gdjs.GameCode.GDbackgroundObjects5.length = 0;
gdjs.GameCode.GDmoldesObjects1.length = 0;
gdjs.GameCode.GDmoldesObjects2.length = 0;
gdjs.GameCode.GDmoldesObjects3.length = 0;
gdjs.GameCode.GDmoldesObjects4.length = 0;
gdjs.GameCode.GDmoldesObjects5.length = 0;
gdjs.GameCode.GDquebra_95cabeca6Objects1.length = 0;
gdjs.GameCode.GDquebra_95cabeca6Objects2.length = 0;
gdjs.GameCode.GDquebra_95cabeca6Objects3.length = 0;
gdjs.GameCode.GDquebra_95cabeca6Objects4.length = 0;
gdjs.GameCode.GDquebra_95cabeca6Objects5.length = 0;
gdjs.GameCode.GDquebra_95cabeca5Objects1.length = 0;
gdjs.GameCode.GDquebra_95cabeca5Objects2.length = 0;
gdjs.GameCode.GDquebra_95cabeca5Objects3.length = 0;
gdjs.GameCode.GDquebra_95cabeca5Objects4.length = 0;
gdjs.GameCode.GDquebra_95cabeca5Objects5.length = 0;
gdjs.GameCode.GDquebra_95cabeca4Objects1.length = 0;
gdjs.GameCode.GDquebra_95cabeca4Objects2.length = 0;
gdjs.GameCode.GDquebra_95cabeca4Objects3.length = 0;
gdjs.GameCode.GDquebra_95cabeca4Objects4.length = 0;
gdjs.GameCode.GDquebra_95cabeca4Objects5.length = 0;
gdjs.GameCode.GDquebra_95cabeca3Objects1.length = 0;
gdjs.GameCode.GDquebra_95cabeca3Objects2.length = 0;
gdjs.GameCode.GDquebra_95cabeca3Objects3.length = 0;
gdjs.GameCode.GDquebra_95cabeca3Objects4.length = 0;
gdjs.GameCode.GDquebra_95cabeca3Objects5.length = 0;
gdjs.GameCode.GDquebra_95cabeca2Objects1.length = 0;
gdjs.GameCode.GDquebra_95cabeca2Objects2.length = 0;
gdjs.GameCode.GDquebra_95cabeca2Objects3.length = 0;
gdjs.GameCode.GDquebra_95cabeca2Objects4.length = 0;
gdjs.GameCode.GDquebra_95cabeca2Objects5.length = 0;
gdjs.GameCode.GDquebra_95cabeca1Objects1.length = 0;
gdjs.GameCode.GDquebra_95cabeca1Objects2.length = 0;
gdjs.GameCode.GDquebra_95cabeca1Objects3.length = 0;
gdjs.GameCode.GDquebra_95cabeca1Objects4.length = 0;
gdjs.GameCode.GDquebra_95cabeca1Objects5.length = 0;
gdjs.GameCode.GDpanelObjects1.length = 0;
gdjs.GameCode.GDpanelObjects2.length = 0;
gdjs.GameCode.GDpanelObjects3.length = 0;
gdjs.GameCode.GDpanelObjects4.length = 0;
gdjs.GameCode.GDpanelObjects5.length = 0;
gdjs.GameCode.GDtrofeuObjects1.length = 0;
gdjs.GameCode.GDtrofeuObjects2.length = 0;
gdjs.GameCode.GDtrofeuObjects3.length = 0;
gdjs.GameCode.GDtrofeuObjects4.length = 0;
gdjs.GameCode.GDtrofeuObjects5.length = 0;
gdjs.GameCode.GDajudaObjects1.length = 0;
gdjs.GameCode.GDajudaObjects2.length = 0;
gdjs.GameCode.GDajudaObjects3.length = 0;
gdjs.GameCode.GDajudaObjects4.length = 0;
gdjs.GameCode.GDajudaObjects5.length = 0;
gdjs.GameCode.GDpanel2Objects1.length = 0;
gdjs.GameCode.GDpanel2Objects2.length = 0;
gdjs.GameCode.GDpanel2Objects3.length = 0;
gdjs.GameCode.GDpanel2Objects4.length = 0;
gdjs.GameCode.GDpanel2Objects5.length = 0;
gdjs.GameCode.GDparabensObjects1.length = 0;
gdjs.GameCode.GDparabensObjects2.length = 0;
gdjs.GameCode.GDparabensObjects3.length = 0;
gdjs.GameCode.GDparabensObjects4.length = 0;
gdjs.GameCode.GDparabensObjects5.length = 0;

gdjs.GameCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['GameCode'] = gdjs.GameCode;
