gdjs.InitCode = {};
gdjs.InitCode.GDtopoObjects1= [];
gdjs.InitCode.GDtopoObjects2= [];
gdjs.InitCode.GDcortina_95LObjects1= [];
gdjs.InitCode.GDcortina_95LObjects2= [];
gdjs.InitCode.GDcortina_95DObjects1= [];
gdjs.InitCode.GDcortina_95DObjects2= [];
gdjs.InitCode.GDfundo_95madeiraObjects1= [];
gdjs.InitCode.GDfundo_95madeiraObjects2= [];
gdjs.InitCode.GDcontinaObjects1= [];
gdjs.InitCode.GDcontinaObjects2= [];
gdjs.InitCode.GDagua_951Objects1= [];
gdjs.InitCode.GDagua_951Objects2= [];
gdjs.InitCode.GDagua_952Objects1= [];
gdjs.InitCode.GDagua_952Objects2= [];
gdjs.InitCode.GDbase_95madeiraObjects1= [];
gdjs.InitCode.GDbase_95madeiraObjects2= [];
gdjs.InitCode.GDrifleObjects1= [];
gdjs.InitCode.GDrifleObjects2= [];
gdjs.InitCode.GDmiraObjects1= [];
gdjs.InitCode.GDmiraObjects2= [];
gdjs.InitCode.GDalvoIniciarObjects1= [];
gdjs.InitCode.GDalvoIniciarObjects2= [];
gdjs.InitCode.GDiniciarObjects1= [];
gdjs.InitCode.GDiniciarObjects2= [];
gdjs.InitCode.GDjogo_95tabuadaObjects1= [];
gdjs.InitCode.GDjogo_95tabuadaObjects2= [];
gdjs.InitCode.GDsiteObjects1= [];
gdjs.InitCode.GDsiteObjects2= [];
gdjs.InitCode.GDautorObjects1= [];
gdjs.InitCode.GDautorObjects2= [];
gdjs.InitCode.GDtabuada1Objects1= [];
gdjs.InitCode.GDtabuada1Objects2= [];
gdjs.InitCode.GDtabuada2Objects1= [];
gdjs.InitCode.GDtabuada2Objects2= [];
gdjs.InitCode.GDtabuada3Objects1= [];
gdjs.InitCode.GDtabuada3Objects2= [];
gdjs.InitCode.GDtabuada4Objects1= [];
gdjs.InitCode.GDtabuada4Objects2= [];
gdjs.InitCode.GDtabuada5Objects1= [];
gdjs.InitCode.GDtabuada5Objects2= [];
gdjs.InitCode.GDtabuada6Objects1= [];
gdjs.InitCode.GDtabuada6Objects2= [];
gdjs.InitCode.GDtabuada7Objects1= [];
gdjs.InitCode.GDtabuada7Objects2= [];
gdjs.InitCode.GDtabuada8Objects1= [];
gdjs.InitCode.GDtabuada8Objects2= [];
gdjs.InitCode.GDtabuada9Objects1= [];
gdjs.InitCode.GDtabuada9Objects2= [];
gdjs.InitCode.GDtabuada10Objects1= [];
gdjs.InitCode.GDtabuada10Objects2= [];
gdjs.InitCode.GDescolhaObjects1= [];
gdjs.InitCode.GDescolhaObjects2= [];
gdjs.InitCode.GDtiroObjects1= [];
gdjs.InitCode.GDtiroObjects2= [];
gdjs.InitCode.GDfadeInObjects1= [];
gdjs.InitCode.GDfadeInObjects2= [];

gdjs.InitCode.conditionTrue_0 = {val:false};
gdjs.InitCode.condition0IsTrue_0 = {val:false};
gdjs.InitCode.condition1IsTrue_0 = {val:false};
gdjs.InitCode.condition2IsTrue_0 = {val:false};
gdjs.InitCode.condition3IsTrue_0 = {val:false};
gdjs.InitCode.conditionTrue_1 = {val:false};
gdjs.InitCode.condition0IsTrue_1 = {val:false};
gdjs.InitCode.condition1IsTrue_1 = {val:false};
gdjs.InitCode.condition2IsTrue_1 = {val:false};
gdjs.InitCode.condition3IsTrue_1 = {val:false};
gdjs.InitCode.conditionTrue_2 = {val:false};
gdjs.InitCode.condition0IsTrue_2 = {val:false};
gdjs.InitCode.condition1IsTrue_2 = {val:false};
gdjs.InitCode.condition2IsTrue_2 = {val:false};
gdjs.InitCode.condition3IsTrue_2 = {val:false};


gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDalvoIniciarObjects1Objects = Hashtable.newFrom({"alvoIniciar": gdjs.InitCode.GDalvoIniciarObjects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada1Objects1ObjectsGDgdjs_46InitCode_46GDtabuada2Objects1ObjectsGDgdjs_46InitCode_46GDtabuada3Objects1ObjectsGDgdjs_46InitCode_46GDtabuada4Objects1ObjectsGDgdjs_46InitCode_46GDtabuada5Objects1ObjectsGDgdjs_46InitCode_46GDtabuada6Objects1ObjectsGDgdjs_46InitCode_46GDtabuada7Objects1ObjectsGDgdjs_46InitCode_46GDtabuada8Objects1ObjectsGDgdjs_46InitCode_46GDtabuada9Objects1ObjectsGDgdjs_46InitCode_46GDtabuada10Objects1Objects = Hashtable.newFrom({"tabuada1": gdjs.InitCode.GDtabuada1Objects1, "tabuada2": gdjs.InitCode.GDtabuada2Objects1, "tabuada3": gdjs.InitCode.GDtabuada3Objects1, "tabuada4": gdjs.InitCode.GDtabuada4Objects1, "tabuada5": gdjs.InitCode.GDtabuada5Objects1, "tabuada6": gdjs.InitCode.GDtabuada6Objects1, "tabuada7": gdjs.InitCode.GDtabuada7Objects1, "tabuada8": gdjs.InitCode.GDtabuada8Objects1, "tabuada9": gdjs.InitCode.GDtabuada9Objects1, "tabuada10": gdjs.InitCode.GDtabuada10Objects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada1Objects1Objects = Hashtable.newFrom({"tabuada1": gdjs.InitCode.GDtabuada1Objects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada2Objects1Objects = Hashtable.newFrom({"tabuada2": gdjs.InitCode.GDtabuada2Objects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada3Objects1Objects = Hashtable.newFrom({"tabuada3": gdjs.InitCode.GDtabuada3Objects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada4Objects1Objects = Hashtable.newFrom({"tabuada4": gdjs.InitCode.GDtabuada4Objects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada5Objects1Objects = Hashtable.newFrom({"tabuada5": gdjs.InitCode.GDtabuada5Objects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada6Objects1Objects = Hashtable.newFrom({"tabuada6": gdjs.InitCode.GDtabuada6Objects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada7Objects1Objects = Hashtable.newFrom({"tabuada7": gdjs.InitCode.GDtabuada7Objects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada8Objects1Objects = Hashtable.newFrom({"tabuada8": gdjs.InitCode.GDtabuada8Objects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada9Objects1Objects = Hashtable.newFrom({"tabuada9": gdjs.InitCode.GDtabuada9Objects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada10Objects1Objects = Hashtable.newFrom({"tabuada10": gdjs.InitCode.GDtabuada10Objects1});gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtiroObjects1Objects = Hashtable.newFrom({"tiro": gdjs.InitCode.GDtiroObjects1});gdjs.InitCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.InitCode.condition0IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.InitCode.condition0IsTrue_0.val) {
gdjs.InitCode.GDescolhaObjects1.createFrom(runtimeScene.getObjects("escolha"));
gdjs.InitCode.GDjogo_95tabuadaObjects1.createFrom(runtimeScene.getObjects("jogo_tabuada"));
{gdjs.evtTools.sound.playMusic(runtimeScene, "CircusDilemmaInit.ogg", true, 70, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getVariables().getFromIndex(0).setNumber(0);
}{for(var i = 0, len = gdjs.InitCode.GDjogo_95tabuadaObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDjogo_95tabuadaObjects1[i].setBold(false);
}
}{for(var i = 0, len = gdjs.InitCode.GDescolhaObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDescolhaObjects1[i].setBold(false);
}
}}

}


{

gdjs.InitCode.GDfadeInObjects1.createFrom(runtimeScene.getObjects("fadeIn"));

gdjs.InitCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.InitCode.GDfadeInObjects1.length;i<l;++i) {
    if ( gdjs.InitCode.GDfadeInObjects1[i].getOpacity() > 0 ) {
        gdjs.InitCode.condition0IsTrue_0.val = true;
        gdjs.InitCode.GDfadeInObjects1[k] = gdjs.InitCode.GDfadeInObjects1[i];
        ++k;
    }
}
gdjs.InitCode.GDfadeInObjects1.length = k;}if (gdjs.InitCode.condition0IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDfadeInObjects1 */
{for(var i = 0, len = gdjs.InitCode.GDfadeInObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDfadeInObjects1[i].setOpacity(gdjs.InitCode.GDfadeInObjects1[i].getOpacity() - (100*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{

gdjs.InitCode.GDalvoIniciarObjects1.createFrom(runtimeScene.getObjects("alvoIniciar"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
gdjs.InitCode.condition2IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) != 0;
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDalvoIniciarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition1IsTrue_0.val ) {
{
gdjs.InitCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
if (gdjs.InitCode.condition2IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{



}


{


gdjs.InitCode.condition0IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.InitCode.condition0IsTrue_0.val) {
gdjs.InitCode.GDagua_951Objects1.createFrom(runtimeScene.getObjects("agua_1"));
gdjs.InitCode.GDagua_952Objects1.createFrom(runtimeScene.getObjects("agua_2"));
gdjs.InitCode.GDmiraObjects1.createFrom(runtimeScene.getObjects("mira"));
gdjs.InitCode.GDrifleObjects1.createFrom(runtimeScene.getObjects("rifle"));
{for(var i = 0, len = gdjs.InitCode.GDagua_951Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDagua_951Objects1[i].setXOffset(gdjs.InitCode.GDagua_951Objects1[i].getXOffset() - (0.7));
}
}{for(var i = 0, len = gdjs.InitCode.GDagua_952Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDagua_952Objects1[i].setXOffset(gdjs.InitCode.GDagua_952Objects1[i].getXOffset() + (0.5));
}
}{for(var i = 0, len = gdjs.InitCode.GDmiraObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDmiraObjects1[i].setPosition(gdjs.evtTools.input.getMouseX(runtimeScene, "", 0) - 27,gdjs.evtTools.input.getMouseY(runtimeScene, "", 0) - 27);
}
}{for(var i = 0, len = gdjs.InitCode.GDrifleObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDrifleObjects1[i].setX((( gdjs.InitCode.GDmiraObjects1.length === 0 ) ? 0 :gdjs.InitCode.GDmiraObjects1[0].getPointX("")) + 50);
}
}}

}


{

gdjs.InitCode.GDrifleObjects1.createFrom(runtimeScene.getObjects("rifle"));

gdjs.InitCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.InitCode.GDrifleObjects1.length;i<l;++i) {
    if ( gdjs.InitCode.GDrifleObjects1[i].getX() <= 70 ) {
        gdjs.InitCode.condition0IsTrue_0.val = true;
        gdjs.InitCode.GDrifleObjects1[k] = gdjs.InitCode.GDrifleObjects1[i];
        ++k;
    }
}
gdjs.InitCode.GDrifleObjects1.length = k;}if (gdjs.InitCode.condition0IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDrifleObjects1 */
{for(var i = 0, len = gdjs.InitCode.GDrifleObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDrifleObjects1[i].setX(70);
}
}}

}


{

gdjs.InitCode.GDrifleObjects1.createFrom(runtimeScene.getObjects("rifle"));

gdjs.InitCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.InitCode.GDrifleObjects1.length;i<l;++i) {
    if ( gdjs.InitCode.GDrifleObjects1[i].getX() >= 660 ) {
        gdjs.InitCode.condition0IsTrue_0.val = true;
        gdjs.InitCode.GDrifleObjects1[k] = gdjs.InitCode.GDrifleObjects1[i];
        ++k;
    }
}
gdjs.InitCode.GDrifleObjects1.length = k;}if (gdjs.InitCode.condition0IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDrifleObjects1 */
{for(var i = 0, len = gdjs.InitCode.GDrifleObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDrifleObjects1[i].setX(660);
}
}}

}


{


gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
gdjs.InitCode.condition2IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 0;
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.InitCode.condition1IsTrue_0.val ) {
{
{gdjs.InitCode.conditionTrue_1 = gdjs.InitCode.condition2IsTrue_0;
gdjs.InitCode.conditionTrue_1.val = context.triggerOnce(8424844);
}
}}
}
if (gdjs.InitCode.condition2IsTrue_0.val) {
gdjs.InitCode.GDalvoIniciarObjects1.createFrom(runtimeScene.getObjects("alvoIniciar"));
gdjs.InitCode.GDiniciarObjects1.createFrom(runtimeScene.getObjects("iniciar"));
gdjs.InitCode.GDtabuada1Objects1.createFrom(runtimeScene.getObjects("tabuada1"));
gdjs.InitCode.GDtabuada10Objects1.createFrom(runtimeScene.getObjects("tabuada10"));
gdjs.InitCode.GDtabuada2Objects1.createFrom(runtimeScene.getObjects("tabuada2"));
gdjs.InitCode.GDtabuada3Objects1.createFrom(runtimeScene.getObjects("tabuada3"));
gdjs.InitCode.GDtabuada4Objects1.createFrom(runtimeScene.getObjects("tabuada4"));
gdjs.InitCode.GDtabuada5Objects1.createFrom(runtimeScene.getObjects("tabuada5"));
gdjs.InitCode.GDtabuada6Objects1.createFrom(runtimeScene.getObjects("tabuada6"));
gdjs.InitCode.GDtabuada7Objects1.createFrom(runtimeScene.getObjects("tabuada7"));
gdjs.InitCode.GDtabuada8Objects1.createFrom(runtimeScene.getObjects("tabuada8"));
gdjs.InitCode.GDtabuada9Objects1.createFrom(runtimeScene.getObjects("tabuada9"));
{for(var i = 0, len = gdjs.InitCode.GDtabuada1Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada1Objects1[i].setBold(false);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada2Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada2Objects1[i].setBold(false);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada3Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada3Objects1[i].setBold(false);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada4Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada4Objects1[i].setBold(false);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada5Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada5Objects1[i].setBold(false);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada6Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada6Objects1[i].setBold(false);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada7Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada7Objects1[i].setBold(false);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada8Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada8Objects1[i].setBold(false);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada9Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada9Objects1[i].setBold(false);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada10Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada10Objects1[i].setBold(false);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada1Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada1Objects1[i].setCharacterSize(32);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada2Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada2Objects1[i].setCharacterSize(32);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada3Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada3Objects1[i].setCharacterSize(32);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada4Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada4Objects1[i].setCharacterSize(32);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada5Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada5Objects1[i].setCharacterSize(32);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada6Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada6Objects1[i].setCharacterSize(32);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada7Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada7Objects1[i].setCharacterSize(32);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada8Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada8Objects1[i].setCharacterSize(32);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada9Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada9Objects1[i].setCharacterSize(32);
}
}{for(var i = 0, len = gdjs.InitCode.GDtabuada10Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada10Objects1[i].setCharacterSize(32);
}
}{for(var i = 0, len = gdjs.InitCode.GDiniciarObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDiniciarObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.InitCode.GDalvoIniciarObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDalvoIniciarObjects1[i].setPosition(745,-100);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}}

}


{



}


{

gdjs.InitCode.GDtabuada1Objects1.createFrom(runtimeScene.getObjects("tabuada1"));
gdjs.InitCode.GDtabuada10Objects1.createFrom(runtimeScene.getObjects("tabuada10"));
gdjs.InitCode.GDtabuada2Objects1.createFrom(runtimeScene.getObjects("tabuada2"));
gdjs.InitCode.GDtabuada3Objects1.createFrom(runtimeScene.getObjects("tabuada3"));
gdjs.InitCode.GDtabuada4Objects1.createFrom(runtimeScene.getObjects("tabuada4"));
gdjs.InitCode.GDtabuada5Objects1.createFrom(runtimeScene.getObjects("tabuada5"));
gdjs.InitCode.GDtabuada6Objects1.createFrom(runtimeScene.getObjects("tabuada6"));
gdjs.InitCode.GDtabuada7Objects1.createFrom(runtimeScene.getObjects("tabuada7"));
gdjs.InitCode.GDtabuada8Objects1.createFrom(runtimeScene.getObjects("tabuada8"));
gdjs.InitCode.GDtabuada9Objects1.createFrom(runtimeScene.getObjects("tabuada9"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada1Objects1ObjectsGDgdjs_46InitCode_46GDtabuada2Objects1ObjectsGDgdjs_46InitCode_46GDtabuada3Objects1ObjectsGDgdjs_46InitCode_46GDtabuada4Objects1ObjectsGDgdjs_46InitCode_46GDtabuada5Objects1ObjectsGDgdjs_46InitCode_46GDtabuada6Objects1ObjectsGDgdjs_46InitCode_46GDtabuada7Objects1ObjectsGDgdjs_46InitCode_46GDtabuada8Objects1ObjectsGDgdjs_46InitCode_46GDtabuada9Objects1ObjectsGDgdjs_46InitCode_46GDtabuada10Objects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
gdjs.InitCode.GDalvoIniciarObjects1.createFrom(runtimeScene.getObjects("alvoIniciar"));
{for(var i = 0, len = gdjs.InitCode.GDalvoIniciarObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDalvoIniciarObjects1[i].addForceTowardPosition(745, 85, 1600, 0);
}
}}

}


{

gdjs.InitCode.GDtabuada1Objects1.createFrom(runtimeScene.getObjects("tabuada1"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada1Objects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtabuada1Objects1 */
{for(var i = 0, len = gdjs.InitCode.GDtabuada1Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada1Objects1[i].setBold(true);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}{for(var i = 0, len = gdjs.InitCode.GDtabuada1Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada1Objects1[i].setCharacterSize(38);
}
}}

}


{

gdjs.InitCode.GDtabuada2Objects1.createFrom(runtimeScene.getObjects("tabuada2"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtabuada2Objects1 */
{for(var i = 0, len = gdjs.InitCode.GDtabuada2Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada2Objects1[i].setBold(true);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(2);
}{for(var i = 0, len = gdjs.InitCode.GDtabuada2Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada2Objects1[i].setCharacterSize(38);
}
}}

}


{

gdjs.InitCode.GDtabuada3Objects1.createFrom(runtimeScene.getObjects("tabuada3"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada3Objects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtabuada3Objects1 */
{for(var i = 0, len = gdjs.InitCode.GDtabuada3Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada3Objects1[i].setBold(true);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(3);
}{for(var i = 0, len = gdjs.InitCode.GDtabuada3Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada3Objects1[i].setCharacterSize(38);
}
}}

}


{

gdjs.InitCode.GDtabuada4Objects1.createFrom(runtimeScene.getObjects("tabuada4"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada4Objects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtabuada4Objects1 */
{for(var i = 0, len = gdjs.InitCode.GDtabuada4Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada4Objects1[i].setBold(true);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(4);
}{for(var i = 0, len = gdjs.InitCode.GDtabuada4Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada4Objects1[i].setCharacterSize(38);
}
}}

}


{

gdjs.InitCode.GDtabuada5Objects1.createFrom(runtimeScene.getObjects("tabuada5"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada5Objects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtabuada5Objects1 */
{for(var i = 0, len = gdjs.InitCode.GDtabuada5Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada5Objects1[i].setBold(true);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(5);
}{for(var i = 0, len = gdjs.InitCode.GDtabuada5Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada5Objects1[i].setCharacterSize(38);
}
}}

}


{

gdjs.InitCode.GDtabuada6Objects1.createFrom(runtimeScene.getObjects("tabuada6"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada6Objects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtabuada6Objects1 */
{for(var i = 0, len = gdjs.InitCode.GDtabuada6Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada6Objects1[i].setBold(true);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(6);
}{for(var i = 0, len = gdjs.InitCode.GDtabuada6Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada6Objects1[i].setCharacterSize(38);
}
}}

}


{

gdjs.InitCode.GDtabuada7Objects1.createFrom(runtimeScene.getObjects("tabuada7"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada7Objects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtabuada7Objects1 */
{for(var i = 0, len = gdjs.InitCode.GDtabuada7Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada7Objects1[i].setBold(true);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(7);
}{for(var i = 0, len = gdjs.InitCode.GDtabuada7Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada7Objects1[i].setCharacterSize(38);
}
}}

}


{

gdjs.InitCode.GDtabuada8Objects1.createFrom(runtimeScene.getObjects("tabuada8"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada8Objects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtabuada8Objects1 */
{for(var i = 0, len = gdjs.InitCode.GDtabuada8Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada8Objects1[i].setBold(true);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(8);
}{for(var i = 0, len = gdjs.InitCode.GDtabuada8Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada8Objects1[i].setCharacterSize(38);
}
}}

}


{

gdjs.InitCode.GDtabuada9Objects1.createFrom(runtimeScene.getObjects("tabuada9"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada9Objects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtabuada9Objects1 */
{for(var i = 0, len = gdjs.InitCode.GDtabuada9Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada9Objects1[i].setBold(true);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(9);
}{for(var i = 0, len = gdjs.InitCode.GDtabuada9Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada9Objects1[i].setCharacterSize(38);
}
}}

}


{

gdjs.InitCode.GDtabuada10Objects1.createFrom(runtimeScene.getObjects("tabuada10"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtabuada10Objects1Objects, runtimeScene, true, false);
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtabuada10Objects1 */
{for(var i = 0, len = gdjs.InitCode.GDtabuada10Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada10Objects1[i].setBold(true);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(10);
}{for(var i = 0, len = gdjs.InitCode.GDtabuada10Objects1.length ;i < len;++i) {
    gdjs.InitCode.GDtabuada10Objects1[i].setCharacterSize(38);
}
}}

}


{

gdjs.InitCode.GDalvoIniciarObjects1.createFrom(runtimeScene.getObjects("alvoIniciar"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) != 0;
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.InitCode.GDalvoIniciarObjects1.length;i<l;++i) {
    if ( gdjs.InitCode.GDalvoIniciarObjects1[i].getY() > 85 ) {
        gdjs.InitCode.condition1IsTrue_0.val = true;
        gdjs.InitCode.GDalvoIniciarObjects1[k] = gdjs.InitCode.GDalvoIniciarObjects1[i];
        ++k;
    }
}
gdjs.InitCode.GDalvoIniciarObjects1.length = k;}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDalvoIniciarObjects1 */
{for(var i = 0, len = gdjs.InitCode.GDalvoIniciarObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDalvoIniciarObjects1[i].setY(85);
}
}}

}


{

gdjs.InitCode.GDalvoIniciarObjects1.createFrom(runtimeScene.getObjects("alvoIniciar"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) != 0;
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.InitCode.GDalvoIniciarObjects1.length;i<l;++i) {
    if ( gdjs.InitCode.GDalvoIniciarObjects1[i].getY() > 85 ) {
        gdjs.InitCode.condition1IsTrue_0.val = true;
        gdjs.InitCode.GDalvoIniciarObjects1[k] = gdjs.InitCode.GDalvoIniciarObjects1[i];
        ++k;
    }
}
gdjs.InitCode.GDalvoIniciarObjects1.length = k;}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDalvoIniciarObjects1 */
{for(var i = 0, len = gdjs.InitCode.GDalvoIniciarObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDalvoIniciarObjects1[i].setY(85);
}
}}

}


{

gdjs.InitCode.GDalvoIniciarObjects1.createFrom(runtimeScene.getObjects("alvoIniciar"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
gdjs.InitCode.condition2IsTrue_0.val = false;
{
gdjs.InitCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
gdjs.InitCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) != 0;
}if ( gdjs.InitCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.InitCode.GDalvoIniciarObjects1.length;i<l;++i) {
    if ( gdjs.InitCode.GDalvoIniciarObjects1[i].getY() != 85 ) {
        gdjs.InitCode.condition2IsTrue_0.val = true;
        gdjs.InitCode.GDalvoIniciarObjects1[k] = gdjs.InitCode.GDalvoIniciarObjects1[i];
        ++k;
    }
}
gdjs.InitCode.GDalvoIniciarObjects1.length = k;}}
}
if (gdjs.InitCode.condition2IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDalvoIniciarObjects1 */
{for(var i = 0, len = gdjs.InitCode.GDalvoIniciarObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDalvoIniciarObjects1[i].setY(85);
}
}}

}


{


gdjs.InitCode.condition0IsTrue_0.val = false;
{
{gdjs.InitCode.conditionTrue_1 = gdjs.InitCode.condition0IsTrue_0;
gdjs.InitCode.condition0IsTrue_1.val = false;
gdjs.InitCode.condition1IsTrue_1.val = false;
gdjs.InitCode.condition2IsTrue_1.val = false;
{
{gdjs.InitCode.conditionTrue_2 = gdjs.InitCode.condition0IsTrue_1;
gdjs.InitCode.conditionTrue_2.val = context.triggerOnce(8442852);
}
if( gdjs.InitCode.condition0IsTrue_1.val ) {
    gdjs.InitCode.conditionTrue_1.val = true;
}
}
{
gdjs.InitCode.condition1IsTrue_1.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if( gdjs.InitCode.condition1IsTrue_1.val ) {
    gdjs.InitCode.conditionTrue_1.val = true;
}
}
{
gdjs.InitCode.condition2IsTrue_1.val = gdjs.evtTools.input.popEndedTouch(runtimeScene);
if( gdjs.InitCode.condition2IsTrue_1.val ) {
    gdjs.InitCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.InitCode.condition0IsTrue_0.val) {
gdjs.InitCode.GDtiroObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.InitCode.mapOfGDgdjs_46InitCode_46GDtiroObjects1Objects, gdjs.evtTools.input.getMouseX(runtimeScene, "tiro", 0) - (( gdjs.InitCode.GDtiroObjects1.length === 0 ) ? 0 :gdjs.InitCode.GDtiroObjects1[0].getWidth()), gdjs.evtTools.input.getMouseY(runtimeScene, "tiro", 0) - (( gdjs.InitCode.GDtiroObjects1.length === 0 ) ? 0 :gdjs.InitCode.GDtiroObjects1[0].getHeight()), "");
}{gdjs.evtTools.sound.playSound(runtimeScene, "cg1.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.InitCode.GDtiroObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDtiroObjects1[i].setLayer("tiro");
}
}}

}


{

gdjs.InitCode.GDtiroObjects1.createFrom(runtimeScene.getObjects("tiro"));

gdjs.InitCode.condition0IsTrue_0.val = false;
gdjs.InitCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.InitCode.GDtiroObjects1.length;i<l;++i) {
    if ( gdjs.InitCode.GDtiroObjects1[i].getOpacity() == 255 ) {
        gdjs.InitCode.condition0IsTrue_0.val = true;
        gdjs.InitCode.GDtiroObjects1[k] = gdjs.InitCode.GDtiroObjects1[i];
        ++k;
    }
}
gdjs.InitCode.GDtiroObjects1.length = k;}if ( gdjs.InitCode.condition0IsTrue_0.val ) {
{
{gdjs.InitCode.conditionTrue_1 = gdjs.InitCode.condition1IsTrue_0;
gdjs.InitCode.conditionTrue_1.val = context.triggerOnce(8444220);
}
}}
if (gdjs.InitCode.condition1IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtiroObjects1 */
{for(var i = 0, len = gdjs.InitCode.GDtiroObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDtiroObjects1[i].setOpacity(254);
}
}}

}


{

gdjs.InitCode.GDtiroObjects1.createFrom(runtimeScene.getObjects("tiro"));

gdjs.InitCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.InitCode.GDtiroObjects1.length;i<l;++i) {
    if ( gdjs.InitCode.GDtiroObjects1[i].getOpacity() < 255 ) {
        gdjs.InitCode.condition0IsTrue_0.val = true;
        gdjs.InitCode.GDtiroObjects1[k] = gdjs.InitCode.GDtiroObjects1[i];
        ++k;
    }
}
gdjs.InitCode.GDtiroObjects1.length = k;}if (gdjs.InitCode.condition0IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtiroObjects1 */
{for(var i = 0, len = gdjs.InitCode.GDtiroObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDtiroObjects1[i].setOpacity(gdjs.InitCode.GDtiroObjects1[i].getOpacity() - (255*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{

gdjs.InitCode.GDtiroObjects1.createFrom(runtimeScene.getObjects("tiro"));

gdjs.InitCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.InitCode.GDtiroObjects1.length;i<l;++i) {
    if ( gdjs.InitCode.GDtiroObjects1[i].getOpacity() <= 0 ) {
        gdjs.InitCode.condition0IsTrue_0.val = true;
        gdjs.InitCode.GDtiroObjects1[k] = gdjs.InitCode.GDtiroObjects1[i];
        ++k;
    }
}
gdjs.InitCode.GDtiroObjects1.length = k;}if (gdjs.InitCode.condition0IsTrue_0.val) {
/* Reuse gdjs.InitCode.GDtiroObjects1 */
{for(var i = 0, len = gdjs.InitCode.GDtiroObjects1.length ;i < len;++i) {
    gdjs.InitCode.GDtiroObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.InitCode.eventsList0xaf630


gdjs.InitCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.InitCode.GDtopoObjects1.length = 0;
gdjs.InitCode.GDtopoObjects2.length = 0;
gdjs.InitCode.GDcortina_95LObjects1.length = 0;
gdjs.InitCode.GDcortina_95LObjects2.length = 0;
gdjs.InitCode.GDcortina_95DObjects1.length = 0;
gdjs.InitCode.GDcortina_95DObjects2.length = 0;
gdjs.InitCode.GDfundo_95madeiraObjects1.length = 0;
gdjs.InitCode.GDfundo_95madeiraObjects2.length = 0;
gdjs.InitCode.GDcontinaObjects1.length = 0;
gdjs.InitCode.GDcontinaObjects2.length = 0;
gdjs.InitCode.GDagua_951Objects1.length = 0;
gdjs.InitCode.GDagua_951Objects2.length = 0;
gdjs.InitCode.GDagua_952Objects1.length = 0;
gdjs.InitCode.GDagua_952Objects2.length = 0;
gdjs.InitCode.GDbase_95madeiraObjects1.length = 0;
gdjs.InitCode.GDbase_95madeiraObjects2.length = 0;
gdjs.InitCode.GDrifleObjects1.length = 0;
gdjs.InitCode.GDrifleObjects2.length = 0;
gdjs.InitCode.GDmiraObjects1.length = 0;
gdjs.InitCode.GDmiraObjects2.length = 0;
gdjs.InitCode.GDalvoIniciarObjects1.length = 0;
gdjs.InitCode.GDalvoIniciarObjects2.length = 0;
gdjs.InitCode.GDiniciarObjects1.length = 0;
gdjs.InitCode.GDiniciarObjects2.length = 0;
gdjs.InitCode.GDjogo_95tabuadaObjects1.length = 0;
gdjs.InitCode.GDjogo_95tabuadaObjects2.length = 0;
gdjs.InitCode.GDsiteObjects1.length = 0;
gdjs.InitCode.GDsiteObjects2.length = 0;
gdjs.InitCode.GDautorObjects1.length = 0;
gdjs.InitCode.GDautorObjects2.length = 0;
gdjs.InitCode.GDtabuada1Objects1.length = 0;
gdjs.InitCode.GDtabuada1Objects2.length = 0;
gdjs.InitCode.GDtabuada2Objects1.length = 0;
gdjs.InitCode.GDtabuada2Objects2.length = 0;
gdjs.InitCode.GDtabuada3Objects1.length = 0;
gdjs.InitCode.GDtabuada3Objects2.length = 0;
gdjs.InitCode.GDtabuada4Objects1.length = 0;
gdjs.InitCode.GDtabuada4Objects2.length = 0;
gdjs.InitCode.GDtabuada5Objects1.length = 0;
gdjs.InitCode.GDtabuada5Objects2.length = 0;
gdjs.InitCode.GDtabuada6Objects1.length = 0;
gdjs.InitCode.GDtabuada6Objects2.length = 0;
gdjs.InitCode.GDtabuada7Objects1.length = 0;
gdjs.InitCode.GDtabuada7Objects2.length = 0;
gdjs.InitCode.GDtabuada8Objects1.length = 0;
gdjs.InitCode.GDtabuada8Objects2.length = 0;
gdjs.InitCode.GDtabuada9Objects1.length = 0;
gdjs.InitCode.GDtabuada9Objects2.length = 0;
gdjs.InitCode.GDtabuada10Objects1.length = 0;
gdjs.InitCode.GDtabuada10Objects2.length = 0;
gdjs.InitCode.GDescolhaObjects1.length = 0;
gdjs.InitCode.GDescolhaObjects2.length = 0;
gdjs.InitCode.GDtiroObjects1.length = 0;
gdjs.InitCode.GDtiroObjects2.length = 0;
gdjs.InitCode.GDfadeInObjects1.length = 0;
gdjs.InitCode.GDfadeInObjects2.length = 0;

gdjs.InitCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['InitCode']= gdjs.InitCode;
