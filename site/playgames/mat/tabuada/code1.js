gdjs.GameCode = {};
gdjs.GameCode.GDtopoObjects1= [];
gdjs.GameCode.GDtopoObjects2= [];
gdjs.GameCode.GDtopoObjects3= [];
gdjs.GameCode.GDcortina_95LObjects1= [];
gdjs.GameCode.GDcortina_95LObjects2= [];
gdjs.GameCode.GDcortina_95LObjects3= [];
gdjs.GameCode.GDcortina_95DObjects1= [];
gdjs.GameCode.GDcortina_95DObjects2= [];
gdjs.GameCode.GDcortina_95DObjects3= [];
gdjs.GameCode.GDfundo_95madeiraObjects1= [];
gdjs.GameCode.GDfundo_95madeiraObjects2= [];
gdjs.GameCode.GDfundo_95madeiraObjects3= [];
gdjs.GameCode.GDcontinaObjects1= [];
gdjs.GameCode.GDcontinaObjects2= [];
gdjs.GameCode.GDcontinaObjects3= [];
gdjs.GameCode.GDagua_951Objects1= [];
gdjs.GameCode.GDagua_951Objects2= [];
gdjs.GameCode.GDagua_951Objects3= [];
gdjs.GameCode.GDagua_952Objects1= [];
gdjs.GameCode.GDagua_952Objects2= [];
gdjs.GameCode.GDagua_952Objects3= [];
gdjs.GameCode.GDbase_95madeiraObjects1= [];
gdjs.GameCode.GDbase_95madeiraObjects2= [];
gdjs.GameCode.GDbase_95madeiraObjects3= [];
gdjs.GameCode.GDrifleObjects1= [];
gdjs.GameCode.GDrifleObjects2= [];
gdjs.GameCode.GDrifleObjects3= [];
gdjs.GameCode.GDmiraObjects1= [];
gdjs.GameCode.GDmiraObjects2= [];
gdjs.GameCode.GDmiraObjects3= [];
gdjs.GameCode.GDtabuadaObjects1= [];
gdjs.GameCode.GDtabuadaObjects2= [];
gdjs.GameCode.GDtabuadaObjects3= [];
gdjs.GameCode.GDtabuadaOpObjects1= [];
gdjs.GameCode.GDtabuadaOpObjects2= [];
gdjs.GameCode.GDtabuadaOpObjects3= [];
gdjs.GameCode.GDsiteObjects1= [];
gdjs.GameCode.GDsiteObjects2= [];
gdjs.GameCode.GDsiteObjects3= [];
gdjs.GameCode.GDautorObjects1= [];
gdjs.GameCode.GDautorObjects2= [];
gdjs.GameCode.GDautorObjects3= [];
gdjs.GameCode.GDalvo1Objects1= [];
gdjs.GameCode.GDalvo1Objects2= [];
gdjs.GameCode.GDalvo1Objects3= [];
gdjs.GameCode.GDalvo2Objects1= [];
gdjs.GameCode.GDalvo2Objects2= [];
gdjs.GameCode.GDalvo2Objects3= [];
gdjs.GameCode.GDalvo3Objects1= [];
gdjs.GameCode.GDalvo3Objects2= [];
gdjs.GameCode.GDalvo3Objects3= [];
gdjs.GameCode.GDalvo4Objects1= [];
gdjs.GameCode.GDalvo4Objects2= [];
gdjs.GameCode.GDalvo4Objects3= [];
gdjs.GameCode.GDalvo5Objects1= [];
gdjs.GameCode.GDalvo5Objects2= [];
gdjs.GameCode.GDalvo5Objects3= [];
gdjs.GameCode.GDnumero1Objects1= [];
gdjs.GameCode.GDnumero1Objects2= [];
gdjs.GameCode.GDnumero1Objects3= [];
gdjs.GameCode.GDnumero2Objects1= [];
gdjs.GameCode.GDnumero2Objects2= [];
gdjs.GameCode.GDnumero2Objects3= [];
gdjs.GameCode.GDnumero3Objects1= [];
gdjs.GameCode.GDnumero3Objects2= [];
gdjs.GameCode.GDnumero3Objects3= [];
gdjs.GameCode.GDnumero4Objects1= [];
gdjs.GameCode.GDnumero4Objects2= [];
gdjs.GameCode.GDnumero4Objects3= [];
gdjs.GameCode.GDnumero5Objects1= [];
gdjs.GameCode.GDnumero5Objects2= [];
gdjs.GameCode.GDnumero5Objects3= [];
gdjs.GameCode.GDtiroObjects1= [];
gdjs.GameCode.GDtiroObjects2= [];
gdjs.GameCode.GDtiroObjects3= [];
gdjs.GameCode.GDbala1Objects1= [];
gdjs.GameCode.GDbala1Objects2= [];
gdjs.GameCode.GDbala1Objects3= [];
gdjs.GameCode.GDbala2Objects1= [];
gdjs.GameCode.GDbala2Objects2= [];
gdjs.GameCode.GDbala2Objects3= [];
gdjs.GameCode.GDbala3Objects1= [];
gdjs.GameCode.GDbala3Objects2= [];
gdjs.GameCode.GDbala3Objects3= [];
gdjs.GameCode.GDgame_95overObjects1= [];
gdjs.GameCode.GDgame_95overObjects2= [];
gdjs.GameCode.GDgame_95overObjects3= [];
gdjs.GameCode.GDgoObjects1= [];
gdjs.GameCode.GDgoObjects2= [];
gdjs.GameCode.GDgoObjects3= [];
gdjs.GameCode.GDtabuadaOpInitObjects1= [];
gdjs.GameCode.GDtabuadaOpInitObjects2= [];
gdjs.GameCode.GDtabuadaOpInitObjects3= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.condition4IsTrue_0 = {val:false};
gdjs.GameCode.condition5IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};
gdjs.GameCode.condition4IsTrue_1 = {val:false};
gdjs.GameCode.condition5IsTrue_1 = {val:false};


gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo1Objects1ObjectsGDgdjs_46GameCode_46GDalvo2Objects1ObjectsGDgdjs_46GameCode_46GDalvo3Objects1ObjectsGDgdjs_46GameCode_46GDalvo4Objects1ObjectsGDgdjs_46GameCode_46GDalvo5Objects1Objects = Hashtable.newFrom({"alvo1": gdjs.GameCode.GDalvo1Objects1, "alvo2": gdjs.GameCode.GDalvo2Objects1, "alvo3": gdjs.GameCode.GDalvo3Objects1, "alvo4": gdjs.GameCode.GDalvo4Objects1, "alvo5": gdjs.GameCode.GDalvo5Objects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDtiroObjects1Objects = Hashtable.newFrom({"tiro": gdjs.GameCode.GDtiroObjects1});gdjs.GameCode.eventsList0x686f88 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) == 2;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbala1Objects2.createFrom(runtimeScene.getObjects("bala1"));
{for(var i = 0, len = gdjs.GameCode.GDbala1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDbala1Objects2[i].deleteFromScene(runtimeScene);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbala2Objects2.createFrom(runtimeScene.getObjects("bala2"));
{for(var i = 0, len = gdjs.GameCode.GDbala2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDbala2Objects2[i].deleteFromScene(runtimeScene);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) == 0;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbala3Objects1.createFrom(runtimeScene.getObjects("bala3"));
{for(var i = 0, len = gdjs.GameCode.GDbala3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDbala3Objects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x686f88
gdjs.GameCode.eventsList0x67d008 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDalvo1Objects2.createFrom(runtimeScene.getObjects("alvo1"));
gdjs.GameCode.GDalvo2Objects2.createFrom(runtimeScene.getObjects("alvo2"));
gdjs.GameCode.GDalvo3Objects2.createFrom(runtimeScene.getObjects("alvo3"));
gdjs.GameCode.GDalvo4Objects2.createFrom(runtimeScene.getObjects("alvo4"));
gdjs.GameCode.GDalvo5Objects2.createFrom(runtimeScene.getObjects("alvo5"));
gdjs.GameCode.GDnumero1Objects2.createFrom(runtimeScene.getObjects("numero1"));
gdjs.GameCode.GDnumero2Objects2.createFrom(runtimeScene.getObjects("numero2"));
gdjs.GameCode.GDnumero3Objects2.createFrom(runtimeScene.getObjects("numero3"));
gdjs.GameCode.GDnumero4Objects2.createFrom(runtimeScene.getObjects("numero4"));
gdjs.GameCode.GDnumero5Objects2.createFrom(runtimeScene.getObjects("numero5"));
{for(var i = 0, len = gdjs.GameCode.GDalvo1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo1Objects2[i].returnVariable(gdjs.GameCode.GDalvo1Objects2[i].getVariables().getFromIndex(1)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo2Objects2[i].returnVariable(gdjs.GameCode.GDalvo2Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo3Objects2[i].returnVariable(gdjs.GameCode.GDalvo3Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo4Objects2[i].returnVariable(gdjs.GameCode.GDalvo4Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo5Objects2[i].returnVariable(gdjs.GameCode.GDalvo5Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero1Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo1Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo1Objects2[0].getVariables()).getFromIndex(1))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero2Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo2Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo2Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero3Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo3Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo3Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero4Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo4Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo4Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero5Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo5Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo5Objects2[0].getVariables()).getFromIndex(0))));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 2;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDalvo1Objects2.createFrom(runtimeScene.getObjects("alvo1"));
gdjs.GameCode.GDalvo2Objects2.createFrom(runtimeScene.getObjects("alvo2"));
gdjs.GameCode.GDalvo3Objects2.createFrom(runtimeScene.getObjects("alvo3"));
gdjs.GameCode.GDalvo4Objects2.createFrom(runtimeScene.getObjects("alvo4"));
gdjs.GameCode.GDalvo5Objects2.createFrom(runtimeScene.getObjects("alvo5"));
gdjs.GameCode.GDnumero1Objects2.createFrom(runtimeScene.getObjects("numero1"));
gdjs.GameCode.GDnumero2Objects2.createFrom(runtimeScene.getObjects("numero2"));
gdjs.GameCode.GDnumero3Objects2.createFrom(runtimeScene.getObjects("numero3"));
gdjs.GameCode.GDnumero4Objects2.createFrom(runtimeScene.getObjects("numero4"));
gdjs.GameCode.GDnumero5Objects2.createFrom(runtimeScene.getObjects("numero5"));
{for(var i = 0, len = gdjs.GameCode.GDalvo2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo2Objects2[i].returnVariable(gdjs.GameCode.GDalvo2Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo1Objects2[i].returnVariable(gdjs.GameCode.GDalvo1Objects2[i].getVariables().getFromIndex(1)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo3Objects2[i].returnVariable(gdjs.GameCode.GDalvo3Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo4Objects2[i].returnVariable(gdjs.GameCode.GDalvo4Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo5Objects2[i].returnVariable(gdjs.GameCode.GDalvo5Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero1Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo1Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo1Objects2[0].getVariables()).getFromIndex(1))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero2Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo2Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo2Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero3Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo3Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo3Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero4Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo4Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo4Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero5Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo5Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo5Objects2[0].getVariables()).getFromIndex(0))));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 3;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDalvo1Objects2.createFrom(runtimeScene.getObjects("alvo1"));
gdjs.GameCode.GDalvo2Objects2.createFrom(runtimeScene.getObjects("alvo2"));
gdjs.GameCode.GDalvo3Objects2.createFrom(runtimeScene.getObjects("alvo3"));
gdjs.GameCode.GDalvo4Objects2.createFrom(runtimeScene.getObjects("alvo4"));
gdjs.GameCode.GDalvo5Objects2.createFrom(runtimeScene.getObjects("alvo5"));
gdjs.GameCode.GDnumero1Objects2.createFrom(runtimeScene.getObjects("numero1"));
gdjs.GameCode.GDnumero2Objects2.createFrom(runtimeScene.getObjects("numero2"));
gdjs.GameCode.GDnumero3Objects2.createFrom(runtimeScene.getObjects("numero3"));
gdjs.GameCode.GDnumero4Objects2.createFrom(runtimeScene.getObjects("numero4"));
gdjs.GameCode.GDnumero5Objects2.createFrom(runtimeScene.getObjects("numero5"));
{for(var i = 0, len = gdjs.GameCode.GDalvo3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo3Objects2[i].returnVariable(gdjs.GameCode.GDalvo3Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo1Objects2[i].returnVariable(gdjs.GameCode.GDalvo1Objects2[i].getVariables().getFromIndex(1)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo2Objects2[i].returnVariable(gdjs.GameCode.GDalvo2Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo4Objects2[i].returnVariable(gdjs.GameCode.GDalvo4Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo5Objects2[i].returnVariable(gdjs.GameCode.GDalvo5Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero1Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo1Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo1Objects2[0].getVariables()).getFromIndex(1))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero2Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo2Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo2Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero3Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo3Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo3Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero4Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo4Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo4Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero5Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo5Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo5Objects2[0].getVariables()).getFromIndex(0))));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 4;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDalvo1Objects2.createFrom(runtimeScene.getObjects("alvo1"));
gdjs.GameCode.GDalvo2Objects2.createFrom(runtimeScene.getObjects("alvo2"));
gdjs.GameCode.GDalvo3Objects2.createFrom(runtimeScene.getObjects("alvo3"));
gdjs.GameCode.GDalvo4Objects2.createFrom(runtimeScene.getObjects("alvo4"));
gdjs.GameCode.GDalvo5Objects2.createFrom(runtimeScene.getObjects("alvo5"));
gdjs.GameCode.GDnumero1Objects2.createFrom(runtimeScene.getObjects("numero1"));
gdjs.GameCode.GDnumero2Objects2.createFrom(runtimeScene.getObjects("numero2"));
gdjs.GameCode.GDnumero3Objects2.createFrom(runtimeScene.getObjects("numero3"));
gdjs.GameCode.GDnumero4Objects2.createFrom(runtimeScene.getObjects("numero4"));
gdjs.GameCode.GDnumero5Objects2.createFrom(runtimeScene.getObjects("numero5"));
{for(var i = 0, len = gdjs.GameCode.GDalvo4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo4Objects2[i].returnVariable(gdjs.GameCode.GDalvo4Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo1Objects2[i].returnVariable(gdjs.GameCode.GDalvo1Objects2[i].getVariables().getFromIndex(1)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo2Objects2[i].returnVariable(gdjs.GameCode.GDalvo2Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo3Objects2[i].returnVariable(gdjs.GameCode.GDalvo3Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo5Objects2[i].returnVariable(gdjs.GameCode.GDalvo5Objects2[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero1Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo1Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo1Objects2[0].getVariables()).getFromIndex(1))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero2Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo2Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo2Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero3Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo3Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo3Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero4Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo4Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo4Objects2[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDnumero5Objects2[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo5Objects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo5Objects2[0].getVariables()).getFromIndex(0))));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 5;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDalvo1Objects1.createFrom(runtimeScene.getObjects("alvo1"));
gdjs.GameCode.GDalvo2Objects1.createFrom(runtimeScene.getObjects("alvo2"));
gdjs.GameCode.GDalvo3Objects1.createFrom(runtimeScene.getObjects("alvo3"));
gdjs.GameCode.GDalvo4Objects1.createFrom(runtimeScene.getObjects("alvo4"));
gdjs.GameCode.GDalvo5Objects1.createFrom(runtimeScene.getObjects("alvo5"));
gdjs.GameCode.GDnumero1Objects1.createFrom(runtimeScene.getObjects("numero1"));
gdjs.GameCode.GDnumero2Objects1.createFrom(runtimeScene.getObjects("numero2"));
gdjs.GameCode.GDnumero3Objects1.createFrom(runtimeScene.getObjects("numero3"));
gdjs.GameCode.GDnumero4Objects1.createFrom(runtimeScene.getObjects("numero4"));
gdjs.GameCode.GDnumero5Objects1.createFrom(runtimeScene.getObjects("numero5"));
{for(var i = 0, len = gdjs.GameCode.GDalvo5Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo5Objects1[i].returnVariable(gdjs.GameCode.GDalvo5Objects1[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo1Objects1[i].returnVariable(gdjs.GameCode.GDalvo1Objects1[i].getVariables().getFromIndex(1)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo2Objects1[i].returnVariable(gdjs.GameCode.GDalvo2Objects1[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo3Objects1[i].returnVariable(gdjs.GameCode.GDalvo3Objects1[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDalvo4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo4Objects1[i].returnVariable(gdjs.GameCode.GDalvo4Objects1[i].getVariables().getFromIndex(0)).setNumber(gdjs.random(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1)*gdjs.random(12));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero1Objects1[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo1Objects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo1Objects1[0].getVariables()).getFromIndex(1))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero2Objects1[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo2Objects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo2Objects1[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero3Objects1[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo3Objects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo3Objects1[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero4Objects1[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo4Objects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo4Objects1[0].getVariables()).getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero5Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero5Objects1[i].setString((gdjs.RuntimeObject.getVariableString(((gdjs.GameCode.GDalvo5Objects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDalvo5Objects1[0].getVariables()).getFromIndex(0))));
}
}}

}


}; //End of gdjs.GameCode.eventsList0x67d008
gdjs.GameCode.eventsList0x653e20 = function(runtimeScene, context) {

{

gdjs.GameCode.GDalvo1Objects2.createFrom(gdjs.GameCode.GDalvo1Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo1Objects2[i].getVariableNumber(gdjs.GameCode.GDalvo1Objects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo1Objects2[k] = gdjs.GameCode.GDalvo1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo1Objects2[i].getY() >= 130 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvo1Objects2[k] = gdjs.GameCode.GDalvo1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo1Objects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo1Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo1Objects2[i].addForce(0, -1*(gdjs.random(15) + 60), 0);
}
}}

}


{

gdjs.GameCode.GDalvo1Objects2.createFrom(gdjs.GameCode.GDalvo1Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo1Objects2[i].getVariableNumber(gdjs.GameCode.GDalvo1Objects2[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo1Objects2[k] = gdjs.GameCode.GDalvo1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo1Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo1Objects2[i].getY() <= 205 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvo1Objects2[k] = gdjs.GameCode.GDalvo1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo1Objects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo1Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo1Objects2[i].addForce(0, 1*(gdjs.random(15) + 60), 0);
}
}}

}


{

gdjs.GameCode.GDalvo1Objects2.createFrom(gdjs.GameCode.GDalvo1Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo1Objects2[i].getY() <= 130 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo1Objects2[k] = gdjs.GameCode.GDalvo1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo1Objects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo1Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo1Objects2[i].returnVariable(gdjs.GameCode.GDalvo1Objects2[i].getVariables().getFromIndex(0)).setNumber(0);
}
}}

}


{

/* Reuse gdjs.GameCode.GDalvo1Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo1Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo1Objects1[i].getY() >= 205 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo1Objects1[k] = gdjs.GameCode.GDalvo1Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo1Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo1Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDalvo1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo1Objects1[i].returnVariable(gdjs.GameCode.GDalvo1Objects1[i].getVariables().getFromIndex(0)).setNumber(1);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x653e20
gdjs.GameCode.eventsList0x654f20 = function(runtimeScene, context) {

{

gdjs.GameCode.GDalvo2Objects2.createFrom(gdjs.GameCode.GDalvo2Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo2Objects2[i].getVariableNumber(gdjs.GameCode.GDalvo2Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo2Objects2[k] = gdjs.GameCode.GDalvo2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo2Objects2[i].getY() >= 130 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvo2Objects2[k] = gdjs.GameCode.GDalvo2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo2Objects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo2Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo2Objects2[i].addForce(0, -1*(gdjs.random(15) + 60), 0);
}
}}

}


{

gdjs.GameCode.GDalvo2Objects2.createFrom(gdjs.GameCode.GDalvo2Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo2Objects2[i].getVariableNumber(gdjs.GameCode.GDalvo2Objects2[i].getVariables().getFromIndex(1)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo2Objects2[k] = gdjs.GameCode.GDalvo2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo2Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo2Objects2[i].getY() <= 205 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvo2Objects2[k] = gdjs.GameCode.GDalvo2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo2Objects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo2Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo2Objects2[i].addForce(0, 1*(gdjs.random(15) + 60), 0);
}
}}

}


{

gdjs.GameCode.GDalvo2Objects2.createFrom(gdjs.GameCode.GDalvo2Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo2Objects2[i].getY() <= 130 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo2Objects2[k] = gdjs.GameCode.GDalvo2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo2Objects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo2Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo2Objects2[i].returnVariable(gdjs.GameCode.GDalvo2Objects2[i].getVariables().getFromIndex(1)).setNumber(0);
}
}}

}


{

/* Reuse gdjs.GameCode.GDalvo2Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo2Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo2Objects1[i].getY() >= 205 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo2Objects1[k] = gdjs.GameCode.GDalvo2Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo2Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo2Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDalvo2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo2Objects1[i].returnVariable(gdjs.GameCode.GDalvo2Objects1[i].getVariables().getFromIndex(1)).setNumber(1);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x654f20
gdjs.GameCode.eventsList0x656020 = function(runtimeScene, context) {

{

gdjs.GameCode.GDalvo3Objects2.createFrom(gdjs.GameCode.GDalvo3Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo3Objects2[i].getVariableNumber(gdjs.GameCode.GDalvo3Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo3Objects2[k] = gdjs.GameCode.GDalvo3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo3Objects2[i].getY() >= 130 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvo3Objects2[k] = gdjs.GameCode.GDalvo3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo3Objects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo3Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo3Objects2[i].addForce(0, -1*(gdjs.random(15) + 60), 0);
}
}}

}


{

gdjs.GameCode.GDalvo3Objects2.createFrom(gdjs.GameCode.GDalvo3Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo3Objects2[i].getVariableNumber(gdjs.GameCode.GDalvo3Objects2[i].getVariables().getFromIndex(1)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo3Objects2[k] = gdjs.GameCode.GDalvo3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo3Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo3Objects2[i].getY() <= 205 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvo3Objects2[k] = gdjs.GameCode.GDalvo3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo3Objects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo3Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo3Objects2[i].addForce(0, 1*(gdjs.random(15) + 60), 0);
}
}}

}


{

gdjs.GameCode.GDalvo3Objects2.createFrom(gdjs.GameCode.GDalvo3Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo3Objects2[i].getY() <= 130 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo3Objects2[k] = gdjs.GameCode.GDalvo3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo3Objects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo3Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo3Objects2[i].returnVariable(gdjs.GameCode.GDalvo3Objects2[i].getVariables().getFromIndex(1)).setNumber(0);
}
}}

}


{

/* Reuse gdjs.GameCode.GDalvo3Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo3Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo3Objects1[i].getY() >= 205 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo3Objects1[k] = gdjs.GameCode.GDalvo3Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo3Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo3Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDalvo3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo3Objects1[i].returnVariable(gdjs.GameCode.GDalvo3Objects1[i].getVariables().getFromIndex(1)).setNumber(1);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x656020
gdjs.GameCode.eventsList0x657120 = function(runtimeScene, context) {

{

gdjs.GameCode.GDalvo4Objects2.createFrom(gdjs.GameCode.GDalvo4Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo4Objects2[i].getVariableNumber(gdjs.GameCode.GDalvo4Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo4Objects2[k] = gdjs.GameCode.GDalvo4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo4Objects2[i].getY() >= 130 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvo4Objects2[k] = gdjs.GameCode.GDalvo4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo4Objects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo4Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo4Objects2[i].addForce(0, -1*(gdjs.random(15) + 60), 0);
}
}}

}


{

gdjs.GameCode.GDalvo4Objects2.createFrom(gdjs.GameCode.GDalvo4Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo4Objects2[i].getVariableNumber(gdjs.GameCode.GDalvo4Objects2[i].getVariables().getFromIndex(1)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo4Objects2[k] = gdjs.GameCode.GDalvo4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo4Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo4Objects2[i].getY() <= 205 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvo4Objects2[k] = gdjs.GameCode.GDalvo4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo4Objects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo4Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo4Objects2[i].addForce(0, 1*(gdjs.random(15) + 60), 0);
}
}}

}


{

gdjs.GameCode.GDalvo4Objects2.createFrom(gdjs.GameCode.GDalvo4Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo4Objects2[i].getY() <= 130 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo4Objects2[k] = gdjs.GameCode.GDalvo4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo4Objects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo4Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo4Objects2[i].returnVariable(gdjs.GameCode.GDalvo4Objects2[i].getVariables().getFromIndex(1)).setNumber(0);
}
}}

}


{

/* Reuse gdjs.GameCode.GDalvo4Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo4Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo4Objects1[i].getY() >= 205 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo4Objects1[k] = gdjs.GameCode.GDalvo4Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo4Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo4Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDalvo4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo4Objects1[i].returnVariable(gdjs.GameCode.GDalvo4Objects1[i].getVariables().getFromIndex(1)).setNumber(1);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x657120
gdjs.GameCode.eventsList0x658428 = function(runtimeScene, context) {

{

gdjs.GameCode.GDalvo5Objects2.createFrom(gdjs.GameCode.GDalvo5Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo5Objects2[i].getVariableNumber(gdjs.GameCode.GDalvo5Objects2[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo5Objects2[k] = gdjs.GameCode.GDalvo5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo5Objects2[i].getY() >= 130 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvo5Objects2[k] = gdjs.GameCode.GDalvo5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo5Objects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo5Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo5Objects2[i].addForce(0, -1*(gdjs.random(15) + 60), 0);
}
}}

}


{

gdjs.GameCode.GDalvo5Objects2.createFrom(gdjs.GameCode.GDalvo5Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo5Objects2[i].getVariableNumber(gdjs.GameCode.GDalvo5Objects2[i].getVariables().getFromIndex(1)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo5Objects2[k] = gdjs.GameCode.GDalvo5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo5Objects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo5Objects2[i].getY() <= 205 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDalvo5Objects2[k] = gdjs.GameCode.GDalvo5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo5Objects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo5Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo5Objects2[i].addForce(0, 1*(gdjs.random(15) + 60), 0);
}
}}

}


{

gdjs.GameCode.GDalvo5Objects2.createFrom(gdjs.GameCode.GDalvo5Objects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo5Objects2[i].getY() <= 130 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo5Objects2[k] = gdjs.GameCode.GDalvo5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo5Objects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo5Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDalvo5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDalvo5Objects2[i].returnVariable(gdjs.GameCode.GDalvo5Objects2[i].getVariables().getFromIndex(1)).setNumber(0);
}
}}

}


{

/* Reuse gdjs.GameCode.GDalvo5Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo5Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo5Objects1[i].getY() >= 205 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo5Objects1[k] = gdjs.GameCode.GDalvo5Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo5Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo5Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDalvo5Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo5Objects1[i].returnVariable(gdjs.GameCode.GDalvo5Objects1[i].getVariables().getFromIndex(1)).setNumber(1);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x658428
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo1Objects1Objects = Hashtable.newFrom({"alvo1": gdjs.GameCode.GDalvo1Objects1});gdjs.GameCode.eventsList0x6591d8 = function(runtimeScene, context) {

{



}


{

/* Reuse gdjs.GameCode.GDalvo1Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo1Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo1Objects1[i].getVariableNumber(gdjs.GameCode.GDalvo1Objects1[i].getVariables().getFromIndex(1)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo1Objects1[k] = gdjs.GameCode.GDalvo1Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo1Objects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(6657484);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDgoObjects1.createFrom(runtimeScene.getObjects("go"));
gdjs.GameCode.GDnumero1Objects1.createFrom(runtimeScene.getObjects("numero1"));
gdjs.GameCode.GDtabuadaOpObjects1.createFrom(runtimeScene.getObjects("tabuadaOp"));
{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}{for(var i = 0, len = gdjs.GameCode.GDtabuadaOpObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtabuadaOpObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) + " X " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)) + " = " + gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(0)));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "reiniciar");
}{gdjs.evtTools.sound.playSound(runtimeScene, "chipquest.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDgoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDgoObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.GameCode.GDnumero1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero1Objects1[i].setColor("0;128;0");
}
}}

}


}; //End of gdjs.GameCode.eventsList0x6591d8
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo2Objects1Objects = Hashtable.newFrom({"alvo2": gdjs.GameCode.GDalvo2Objects1});gdjs.GameCode.eventsList0x659cd0 = function(runtimeScene, context) {

{



}


{

/* Reuse gdjs.GameCode.GDalvo2Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo2Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo2Objects1[i].getVariableNumber(gdjs.GameCode.GDalvo2Objects1[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo2Objects1[k] = gdjs.GameCode.GDalvo2Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo2Objects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(6660292);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDgoObjects1.createFrom(runtimeScene.getObjects("go"));
gdjs.GameCode.GDnumero2Objects1.createFrom(runtimeScene.getObjects("numero2"));
gdjs.GameCode.GDtabuadaOpObjects1.createFrom(runtimeScene.getObjects("tabuadaOp"));
{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}{for(var i = 0, len = gdjs.GameCode.GDtabuadaOpObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtabuadaOpObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) + " X " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)) + " = " + gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(0)));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}{for(var i = 0, len = gdjs.GameCode.GDgoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDgoObjects1[i].hide(false);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "chipquest.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDnumero2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero2Objects1[i].setColor("0;128;0");
}
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "reiniciar");
}}

}


}; //End of gdjs.GameCode.eventsList0x659cd0
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo3Objects1Objects = Hashtable.newFrom({"alvo3": gdjs.GameCode.GDalvo3Objects1});gdjs.GameCode.eventsList0x65a7c8 = function(runtimeScene, context) {

{



}


{

/* Reuse gdjs.GameCode.GDalvo3Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo3Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo3Objects1[i].getVariableNumber(gdjs.GameCode.GDalvo3Objects1[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo3Objects1[k] = gdjs.GameCode.GDalvo3Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo3Objects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(6663100);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDgoObjects1.createFrom(runtimeScene.getObjects("go"));
gdjs.GameCode.GDnumero3Objects1.createFrom(runtimeScene.getObjects("numero3"));
gdjs.GameCode.GDtabuadaOpObjects1.createFrom(runtimeScene.getObjects("tabuadaOp"));
{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}{for(var i = 0, len = gdjs.GameCode.GDtabuadaOpObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtabuadaOpObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) + " X " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)) + " = " + gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(0)));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}{for(var i = 0, len = gdjs.GameCode.GDgoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDgoObjects1[i].hide(false);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "chipquest.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDnumero3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero3Objects1[i].setColor("0;128;0");
}
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "reiniciar");
}}

}


}; //End of gdjs.GameCode.eventsList0x65a7c8
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo4Objects1Objects = Hashtable.newFrom({"alvo4": gdjs.GameCode.GDalvo4Objects1});gdjs.GameCode.eventsList0x65b2c0 = function(runtimeScene, context) {

{



}


{

/* Reuse gdjs.GameCode.GDalvo4Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo4Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo4Objects1[i].getVariableNumber(gdjs.GameCode.GDalvo4Objects1[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo4Objects1[k] = gdjs.GameCode.GDalvo4Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo4Objects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(6665908);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDgoObjects1.createFrom(runtimeScene.getObjects("go"));
gdjs.GameCode.GDnumero4Objects1.createFrom(runtimeScene.getObjects("numero4"));
gdjs.GameCode.GDtabuadaOpObjects1.createFrom(runtimeScene.getObjects("tabuadaOp"));
{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}{for(var i = 0, len = gdjs.GameCode.GDtabuadaOpObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtabuadaOpObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) + " X " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)) + " = " + gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(0)));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}{for(var i = 0, len = gdjs.GameCode.GDgoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDgoObjects1[i].hide(false);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "chipquest.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDnumero4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero4Objects1[i].setColor("0;128;0");
}
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "reiniciar");
}}

}


}; //End of gdjs.GameCode.eventsList0x65b2c0
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo5Objects1Objects = Hashtable.newFrom({"alvo5": gdjs.GameCode.GDalvo5Objects1});gdjs.GameCode.eventsList0x65bdb8 = function(runtimeScene, context) {

{



}


{

/* Reuse gdjs.GameCode.GDalvo5Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo5Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo5Objects1[i].getVariableNumber(gdjs.GameCode.GDalvo5Objects1[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo5Objects1[k] = gdjs.GameCode.GDalvo5Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo5Objects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(6668716);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDgoObjects1.createFrom(runtimeScene.getObjects("go"));
gdjs.GameCode.GDnumero5Objects1.createFrom(runtimeScene.getObjects("numero5"));
gdjs.GameCode.GDtabuadaOpObjects1.createFrom(runtimeScene.getObjects("tabuadaOp"));
{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}{for(var i = 0, len = gdjs.GameCode.GDtabuadaOpObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtabuadaOpObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) + " X " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)) + " = " + gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(0)));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}{for(var i = 0, len = gdjs.GameCode.GDgoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDgoObjects1[i].hide(false);
}
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "reiniciar");
}{gdjs.evtTools.sound.playSound(runtimeScene, "chipquest.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDnumero5Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero5Objects1[i].setColor("0;128;0");
}
}}

}


}; //End of gdjs.GameCode.eventsList0x65bdb8
gdjs.GameCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "reiniciar");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "finalizar");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "End", false);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDgame_95overObjects1.createFrom(runtimeScene.getObjects("game_over"));
gdjs.GameCode.GDgoObjects1.createFrom(runtimeScene.getObjects("go"));
gdjs.GameCode.GDtabuadaObjects1.createFrom(runtimeScene.getObjects("tabuada"));
gdjs.GameCode.GDtabuadaOpInitObjects1.createFrom(runtimeScene.getObjects("tabuadaOpInit"));
{for(var i = 0, len = gdjs.GameCode.GDtabuadaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtabuadaObjects1[i].setString("TABUADA DO " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "reiniciar");
}{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "finalizar");
}{for(var i = 0, len = gdjs.GameCode.GDgame_95overObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDgame_95overObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDgoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDgoObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDtabuadaOpInitObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtabuadaOpInitObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) + " X " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}{gdjs.evtTools.sound.playMusic(runtimeScene, "RollUp.ogg", true, 80, 1);
}{runtimeScene.getVariables().getFromIndex(3).setNumber(3);
}{runtimeScene.getVariables().getFromIndex(0).setNumber(-1);
}}

}


{



}


{

gdjs.GameCode.GDalvo1Objects1.createFrom(runtimeScene.getObjects("alvo1"));
gdjs.GameCode.GDalvo2Objects1.createFrom(runtimeScene.getObjects("alvo2"));
gdjs.GameCode.GDalvo3Objects1.createFrom(runtimeScene.getObjects("alvo3"));
gdjs.GameCode.GDalvo4Objects1.createFrom(runtimeScene.getObjects("alvo4"));
gdjs.GameCode.GDalvo5Objects1.createFrom(runtimeScene.getObjects("alvo5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
gdjs.GameCode.condition4IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.getMouseY(runtimeScene, "", 0) <= 320;
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.input.getMouseY(runtimeScene, "", 0) >= 150;
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
gdjs.GameCode.condition3IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo1Objects1ObjectsGDgdjs_46GameCode_46GDalvo2Objects1ObjectsGDgdjs_46GameCode_46GDalvo3Objects1ObjectsGDgdjs_46GameCode_46GDalvo4Objects1ObjectsGDgdjs_46GameCode_46GDalvo5Objects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition3IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition4IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(6620412);
}
}}
}
}
}
if (gdjs.GameCode.condition4IsTrue_0.val) {
gdjs.GameCode.GDtiroObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDtiroObjects1Objects, gdjs.evtTools.input.getMouseX(runtimeScene, "topo", 0) - (( gdjs.GameCode.GDtiroObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDtiroObjects1[0].getWidth()), gdjs.evtTools.input.getMouseY(runtimeScene, "topo", 0) - (( gdjs.GameCode.GDtiroObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDtiroObjects1[0].getHeight()), "");
}{runtimeScene.getVariables().getFromIndex(3).sub(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "cg1.ogg", false, 100, 1);
}
{ //Subevents
gdjs.GameCode.eventsList0x686f88(runtimeScene, context);} //End of subevents
}

}


{

gdjs.GameCode.GDtiroObjects1.createFrom(runtimeScene.getObjects("tiro"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDtiroObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDtiroObjects1[i].getOpacity() == 255 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDtiroObjects1[k] = gdjs.GameCode.GDtiroObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDtiroObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 0;
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(6802332);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDtiroObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDtiroObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtiroObjects1[i].setOpacity(254);
}
}}

}


{

gdjs.GameCode.GDtiroObjects1.createFrom(runtimeScene.getObjects("tiro"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDtiroObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDtiroObjects1[i].getOpacity() < 255 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDtiroObjects1[k] = gdjs.GameCode.GDtiroObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDtiroObjects1.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDtiroObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDtiroObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtiroObjects1[i].setOpacity(gdjs.GameCode.GDtiroObjects1[i].getOpacity() - (255*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) < 0;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDtabuadaOpObjects1.createFrom(runtimeScene.getObjects("tabuadaOp"));
{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))*gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)));
}{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.random(4) + 1);
}{for(var i = 0, len = gdjs.GameCode.GDtabuadaOpObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtabuadaOpObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) + " X " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x67d008(runtimeScene, context);} //End of subevents
}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDagua_951Objects1.createFrom(runtimeScene.getObjects("agua_1"));
gdjs.GameCode.GDagua_952Objects1.createFrom(runtimeScene.getObjects("agua_2"));
gdjs.GameCode.GDmiraObjects1.createFrom(runtimeScene.getObjects("mira"));
gdjs.GameCode.GDrifleObjects1.createFrom(runtimeScene.getObjects("rifle"));
{for(var i = 0, len = gdjs.GameCode.GDmiraObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmiraObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.GameCode.GDrifleObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDrifleObjects1[i].returnVariable(gdjs.GameCode.GDrifleObjects1[i].getVariables().getFromIndex(0)).setNumber(1);
}
}{for(var i = 0, len = gdjs.GameCode.GDagua_951Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDagua_951Objects1[i].setXOffset(gdjs.GameCode.GDagua_951Objects1[i].getXOffset() - (0.7));
}
}{for(var i = 0, len = gdjs.GameCode.GDagua_952Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDagua_952Objects1[i].setXOffset(gdjs.GameCode.GDagua_952Objects1[i].getXOffset() + (0.5));
}
}{for(var i = 0, len = gdjs.GameCode.GDmiraObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmiraObjects1[i].setPosition(gdjs.evtTools.input.getMouseX(runtimeScene, "", 0) - 27,gdjs.evtTools.input.getMouseY(runtimeScene, "", 0) - 27);
}
}{for(var i = 0, len = gdjs.GameCode.GDrifleObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDrifleObjects1[i].setX((( gdjs.GameCode.GDmiraObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDmiraObjects1[0].getPointX("")) + 50);
}
}}

}


{

gdjs.GameCode.GDmiraObjects1.createFrom(runtimeScene.getObjects("mira"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDmiraObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDmiraObjects1[i].getY() >= 300 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDmiraObjects1[k] = gdjs.GameCode.GDmiraObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDmiraObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDmiraObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDmiraObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmiraObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDmiraObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmiraObjects1[i].setY(300);
}
}}

}


{

gdjs.GameCode.GDmiraObjects1.createFrom(runtimeScene.getObjects("mira"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDmiraObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDmiraObjects1[i].getY() <= 120 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDmiraObjects1[k] = gdjs.GameCode.GDmiraObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDmiraObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDmiraObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDmiraObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmiraObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDmiraObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmiraObjects1[i].setY(120);
}
}}

}


{

gdjs.GameCode.GDrifleObjects1.createFrom(runtimeScene.getObjects("rifle"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDrifleObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDrifleObjects1[i].getX() <= 270 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDrifleObjects1[k] = gdjs.GameCode.GDrifleObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDrifleObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDrifleObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDrifleObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDrifleObjects1[i].returnVariable(gdjs.GameCode.GDrifleObjects1[i].getVariables().getFromIndex(0)).setNumber(0);
}
}}

}


{

gdjs.GameCode.GDrifleObjects1.createFrom(runtimeScene.getObjects("rifle"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDrifleObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDrifleObjects1[i].getX() >= 660 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDrifleObjects1[k] = gdjs.GameCode.GDrifleObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDrifleObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDrifleObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDrifleObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDrifleObjects1[i].returnVariable(gdjs.GameCode.GDrifleObjects1[i].getVariables().getFromIndex(0)).setNumber(0);
}
}}

}


{



}


{

gdjs.GameCode.GDalvo1Objects1.createFrom(runtimeScene.getObjects("alvo1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo1Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo1Objects1[i].getX() > gdjs.evtTools.window.getCanvasWidth(runtimeScene) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo1Objects1[k] = gdjs.GameCode.GDalvo1Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo1Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo1Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDalvo1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo1Objects1[i].setX(0 - (gdjs.GameCode.GDalvo1Objects1[i].getWidth()));
}
}}

}


{


{
gdjs.GameCode.GDalvo1Objects1.createFrom(runtimeScene.getObjects("alvo1"));
gdjs.GameCode.GDnumero1Objects1.createFrom(runtimeScene.getObjects("numero1"));
{for(var i = 0, len = gdjs.GameCode.GDnumero1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero1Objects1[i].setPosition((( gdjs.GameCode.GDalvo1Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo1Objects1[0].getPointX("numero1")) + (( gdjs.GameCode.GDalvo1Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo1Objects1[0].getWidth())/2 - (gdjs.GameCode.GDnumero1Objects1[i].getWidth())/2,(( gdjs.GameCode.GDalvo1Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo1Objects1[0].getPointY("numero1")) + (( gdjs.GameCode.GDalvo1Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo1Objects1[0].getHeight())/4 - (gdjs.GameCode.GDnumero1Objects1[i].getHeight())/2);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) > 0;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDalvo1Objects1.createFrom(runtimeScene.getObjects("alvo1"));
{for(var i = 0, len = gdjs.GameCode.GDalvo1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo1Objects1[i].addForce(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 0, 0);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x653e20(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDalvo2Objects1.createFrom(runtimeScene.getObjects("alvo2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo2Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo2Objects1[i].getX() > gdjs.evtTools.window.getCanvasWidth(runtimeScene) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo2Objects1[k] = gdjs.GameCode.GDalvo2Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo2Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo2Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDalvo2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo2Objects1[i].setX(0 - (gdjs.GameCode.GDalvo2Objects1[i].getWidth()));
}
}}

}


{


{
gdjs.GameCode.GDalvo2Objects1.createFrom(runtimeScene.getObjects("alvo2"));
gdjs.GameCode.GDnumero2Objects1.createFrom(runtimeScene.getObjects("numero2"));
{for(var i = 0, len = gdjs.GameCode.GDnumero2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero2Objects1[i].setPosition((( gdjs.GameCode.GDalvo2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo2Objects1[0].getPointX("numero2")) + (( gdjs.GameCode.GDalvo2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo2Objects1[0].getWidth())/2 - (gdjs.GameCode.GDnumero2Objects1[i].getWidth())/2,(( gdjs.GameCode.GDalvo2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo2Objects1[0].getPointY("numero2")) + (( gdjs.GameCode.GDalvo2Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo2Objects1[0].getHeight())/4 - (gdjs.GameCode.GDnumero2Objects1[i].getHeight())/2);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) > 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 0;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDalvo2Objects1.createFrom(runtimeScene.getObjects("alvo2"));
{for(var i = 0, len = gdjs.GameCode.GDalvo2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo2Objects1[i].addForce(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 0, 0);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x654f20(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDalvo3Objects1.createFrom(runtimeScene.getObjects("alvo3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo3Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo3Objects1[i].getX() > gdjs.evtTools.window.getCanvasWidth(runtimeScene) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo3Objects1[k] = gdjs.GameCode.GDalvo3Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo3Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo3Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDalvo3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo3Objects1[i].setX(0 - (gdjs.GameCode.GDalvo3Objects1[i].getWidth()));
}
}}

}


{


{
gdjs.GameCode.GDalvo3Objects1.createFrom(runtimeScene.getObjects("alvo3"));
gdjs.GameCode.GDnumero3Objects1.createFrom(runtimeScene.getObjects("numero3"));
{for(var i = 0, len = gdjs.GameCode.GDnumero3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero3Objects1[i].setPosition((( gdjs.GameCode.GDalvo3Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo3Objects1[0].getPointX("numero3")) + (( gdjs.GameCode.GDalvo3Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo3Objects1[0].getWidth())/2 - (gdjs.GameCode.GDnumero3Objects1[i].getWidth())/2,(( gdjs.GameCode.GDalvo3Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo3Objects1[0].getPointY("numero3")) + (( gdjs.GameCode.GDalvo3Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo3Objects1[0].getHeight())/4 - (gdjs.GameCode.GDnumero3Objects1[i].getHeight())/2);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) > 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 0;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDalvo3Objects1.createFrom(runtimeScene.getObjects("alvo3"));
{for(var i = 0, len = gdjs.GameCode.GDalvo3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo3Objects1[i].addForce(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 0, 0);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x656020(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDalvo4Objects1.createFrom(runtimeScene.getObjects("alvo4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo4Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo4Objects1[i].getX() > gdjs.evtTools.window.getCanvasWidth(runtimeScene) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo4Objects1[k] = gdjs.GameCode.GDalvo4Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo4Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo4Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDalvo4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo4Objects1[i].setX(0 - (gdjs.GameCode.GDalvo4Objects1[i].getWidth()));
}
}}

}


{


{
gdjs.GameCode.GDalvo4Objects1.createFrom(runtimeScene.getObjects("alvo4"));
gdjs.GameCode.GDnumero4Objects1.createFrom(runtimeScene.getObjects("numero4"));
{for(var i = 0, len = gdjs.GameCode.GDnumero4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero4Objects1[i].setPosition((( gdjs.GameCode.GDalvo4Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo4Objects1[0].getPointX("numero4")) + (( gdjs.GameCode.GDalvo4Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo4Objects1[0].getWidth())/2 - (gdjs.GameCode.GDnumero4Objects1[i].getWidth())/2,(( gdjs.GameCode.GDalvo4Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo4Objects1[0].getPointY("numero4")) + (( gdjs.GameCode.GDalvo4Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo4Objects1[0].getHeight())/4 - (gdjs.GameCode.GDnumero4Objects1[i].getHeight())/2);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) > 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 0;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDalvo4Objects1.createFrom(runtimeScene.getObjects("alvo4"));
{for(var i = 0, len = gdjs.GameCode.GDalvo4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo4Objects1[i].addForce(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 0, 0);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x657120(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDalvo5Objects1.createFrom(runtimeScene.getObjects("alvo5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDalvo5Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDalvo5Objects1[i].getX() > gdjs.evtTools.window.getCanvasWidth(runtimeScene) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDalvo5Objects1[k] = gdjs.GameCode.GDalvo5Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDalvo5Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalvo5Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDalvo5Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo5Objects1[i].setX(0 - (gdjs.GameCode.GDalvo5Objects1[i].getWidth()));
}
}}

}


{


{
gdjs.GameCode.GDalvo5Objects1.createFrom(runtimeScene.getObjects("alvo5"));
gdjs.GameCode.GDnumero5Objects1.createFrom(runtimeScene.getObjects("numero5"));
{for(var i = 0, len = gdjs.GameCode.GDnumero5Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDnumero5Objects1[i].setPosition((( gdjs.GameCode.GDalvo5Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo5Objects1[0].getPointX("numero5")) + (( gdjs.GameCode.GDalvo5Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo5Objects1[0].getWidth())/2 - (gdjs.GameCode.GDnumero5Objects1[i].getWidth())/2,(( gdjs.GameCode.GDalvo5Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo5Objects1[0].getPointY("numero5")) + (( gdjs.GameCode.GDalvo5Objects1.length === 0 ) ? 0 :gdjs.GameCode.GDalvo5Objects1[0].getHeight())/4 - (gdjs.GameCode.GDnumero5Objects1[i].getHeight())/2);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) > 0;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDalvo5Objects1.createFrom(runtimeScene.getObjects("alvo5"));
{for(var i = 0, len = gdjs.GameCode.GDalvo5Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDalvo5Objects1[i].addForce(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 0, 0);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x658428(runtimeScene, context);} //End of subevents
}

}


{



}


{



}


{

gdjs.GameCode.GDalvo1Objects1.createFrom(runtimeScene.getObjects("alvo1"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo1Objects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x6591d8(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDalvo2Objects1.createFrom(runtimeScene.getObjects("alvo2"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x659cd0(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDalvo3Objects1.createFrom(runtimeScene.getObjects("alvo3"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo3Objects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x65a7c8(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDalvo4Objects1.createFrom(runtimeScene.getObjects("alvo4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo4Objects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x65b2c0(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDalvo5Objects1.createFrom(runtimeScene.getObjects("alvo5"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalvo5Objects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x65bdb8(runtimeScene, context);} //End of subevents
}

}


{

gdjs.GameCode.GDgame_95overObjects1.createFrom(runtimeScene.getObjects("game_over"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) == 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) == 0;
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDgame_95overObjects1.length;i<l;++i) {
    if ( !(gdjs.GameCode.GDgame_95overObjects1[i].isVisible()) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDgame_95overObjects1[k] = gdjs.GameCode.GDgame_95overObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDgame_95overObjects1.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDgame_95overObjects1 */
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "reiniciar");
}{for(var i = 0, len = gdjs.GameCode.GDgame_95overObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDgame_95overObjects1[i].hide(false);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}}

}


{


{
gdjs.GameCode.GDtabuadaOpInitObjects1.createFrom(runtimeScene.getObjects("tabuadaOpInit"));
{for(var i = 0, len = gdjs.GameCode.GDtabuadaOpInitObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtabuadaOpInitObjects1[i].addForce(0, -100, 0);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) > 10;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "finalizar");
}}

}


}; //End of gdjs.GameCode.eventsList0xaf630


gdjs.GameCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GameCode.GDtopoObjects1.length = 0;
gdjs.GameCode.GDtopoObjects2.length = 0;
gdjs.GameCode.GDtopoObjects3.length = 0;
gdjs.GameCode.GDcortina_95LObjects1.length = 0;
gdjs.GameCode.GDcortina_95LObjects2.length = 0;
gdjs.GameCode.GDcortina_95LObjects3.length = 0;
gdjs.GameCode.GDcortina_95DObjects1.length = 0;
gdjs.GameCode.GDcortina_95DObjects2.length = 0;
gdjs.GameCode.GDcortina_95DObjects3.length = 0;
gdjs.GameCode.GDfundo_95madeiraObjects1.length = 0;
gdjs.GameCode.GDfundo_95madeiraObjects2.length = 0;
gdjs.GameCode.GDfundo_95madeiraObjects3.length = 0;
gdjs.GameCode.GDcontinaObjects1.length = 0;
gdjs.GameCode.GDcontinaObjects2.length = 0;
gdjs.GameCode.GDcontinaObjects3.length = 0;
gdjs.GameCode.GDagua_951Objects1.length = 0;
gdjs.GameCode.GDagua_951Objects2.length = 0;
gdjs.GameCode.GDagua_951Objects3.length = 0;
gdjs.GameCode.GDagua_952Objects1.length = 0;
gdjs.GameCode.GDagua_952Objects2.length = 0;
gdjs.GameCode.GDagua_952Objects3.length = 0;
gdjs.GameCode.GDbase_95madeiraObjects1.length = 0;
gdjs.GameCode.GDbase_95madeiraObjects2.length = 0;
gdjs.GameCode.GDbase_95madeiraObjects3.length = 0;
gdjs.GameCode.GDrifleObjects1.length = 0;
gdjs.GameCode.GDrifleObjects2.length = 0;
gdjs.GameCode.GDrifleObjects3.length = 0;
gdjs.GameCode.GDmiraObjects1.length = 0;
gdjs.GameCode.GDmiraObjects2.length = 0;
gdjs.GameCode.GDmiraObjects3.length = 0;
gdjs.GameCode.GDtabuadaObjects1.length = 0;
gdjs.GameCode.GDtabuadaObjects2.length = 0;
gdjs.GameCode.GDtabuadaObjects3.length = 0;
gdjs.GameCode.GDtabuadaOpObjects1.length = 0;
gdjs.GameCode.GDtabuadaOpObjects2.length = 0;
gdjs.GameCode.GDtabuadaOpObjects3.length = 0;
gdjs.GameCode.GDsiteObjects1.length = 0;
gdjs.GameCode.GDsiteObjects2.length = 0;
gdjs.GameCode.GDsiteObjects3.length = 0;
gdjs.GameCode.GDautorObjects1.length = 0;
gdjs.GameCode.GDautorObjects2.length = 0;
gdjs.GameCode.GDautorObjects3.length = 0;
gdjs.GameCode.GDalvo1Objects1.length = 0;
gdjs.GameCode.GDalvo1Objects2.length = 0;
gdjs.GameCode.GDalvo1Objects3.length = 0;
gdjs.GameCode.GDalvo2Objects1.length = 0;
gdjs.GameCode.GDalvo2Objects2.length = 0;
gdjs.GameCode.GDalvo2Objects3.length = 0;
gdjs.GameCode.GDalvo3Objects1.length = 0;
gdjs.GameCode.GDalvo3Objects2.length = 0;
gdjs.GameCode.GDalvo3Objects3.length = 0;
gdjs.GameCode.GDalvo4Objects1.length = 0;
gdjs.GameCode.GDalvo4Objects2.length = 0;
gdjs.GameCode.GDalvo4Objects3.length = 0;
gdjs.GameCode.GDalvo5Objects1.length = 0;
gdjs.GameCode.GDalvo5Objects2.length = 0;
gdjs.GameCode.GDalvo5Objects3.length = 0;
gdjs.GameCode.GDnumero1Objects1.length = 0;
gdjs.GameCode.GDnumero1Objects2.length = 0;
gdjs.GameCode.GDnumero1Objects3.length = 0;
gdjs.GameCode.GDnumero2Objects1.length = 0;
gdjs.GameCode.GDnumero2Objects2.length = 0;
gdjs.GameCode.GDnumero2Objects3.length = 0;
gdjs.GameCode.GDnumero3Objects1.length = 0;
gdjs.GameCode.GDnumero3Objects2.length = 0;
gdjs.GameCode.GDnumero3Objects3.length = 0;
gdjs.GameCode.GDnumero4Objects1.length = 0;
gdjs.GameCode.GDnumero4Objects2.length = 0;
gdjs.GameCode.GDnumero4Objects3.length = 0;
gdjs.GameCode.GDnumero5Objects1.length = 0;
gdjs.GameCode.GDnumero5Objects2.length = 0;
gdjs.GameCode.GDnumero5Objects3.length = 0;
gdjs.GameCode.GDtiroObjects1.length = 0;
gdjs.GameCode.GDtiroObjects2.length = 0;
gdjs.GameCode.GDtiroObjects3.length = 0;
gdjs.GameCode.GDbala1Objects1.length = 0;
gdjs.GameCode.GDbala1Objects2.length = 0;
gdjs.GameCode.GDbala1Objects3.length = 0;
gdjs.GameCode.GDbala2Objects1.length = 0;
gdjs.GameCode.GDbala2Objects2.length = 0;
gdjs.GameCode.GDbala2Objects3.length = 0;
gdjs.GameCode.GDbala3Objects1.length = 0;
gdjs.GameCode.GDbala3Objects2.length = 0;
gdjs.GameCode.GDbala3Objects3.length = 0;
gdjs.GameCode.GDgame_95overObjects1.length = 0;
gdjs.GameCode.GDgame_95overObjects2.length = 0;
gdjs.GameCode.GDgame_95overObjects3.length = 0;
gdjs.GameCode.GDgoObjects1.length = 0;
gdjs.GameCode.GDgoObjects2.length = 0;
gdjs.GameCode.GDgoObjects3.length = 0;
gdjs.GameCode.GDtabuadaOpInitObjects1.length = 0;
gdjs.GameCode.GDtabuadaOpInitObjects2.length = 0;
gdjs.GameCode.GDtabuadaOpInitObjects3.length = 0;

gdjs.GameCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['GameCode']= gdjs.GameCode;
