gdjs.StartCode = {};
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDescolaPlayObjects1= [];
gdjs.StartCode.GDescolaPlayObjects2= [];
gdjs.StartCode.GDJOGOObjects1= [];
gdjs.StartCode.GDJOGOObjects2= [];
gdjs.StartCode.GDtitleObjects1= [];
gdjs.StartCode.GDtitleObjects2= [];
gdjs.StartCode.GDbtnStartObjects1= [];
gdjs.StartCode.GDbtnStartObjects2= [];
gdjs.StartCode.GDlblStartObjects1= [];
gdjs.StartCode.GDlblStartObjects2= [];
gdjs.StartCode.GDautorObjects1= [];
gdjs.StartCode.GDautorObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};
gdjs.StartCode.condition3IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtnStartObjects1Objects = Hashtable.newFrom({"btnStart": gdjs.StartCode.GDbtnStartObjects1});gdjs.StartCode.eventsList0 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "SzymonMatuszewski-Art.ogg", 0, true, 30, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("btnStart"), gdjs.StartCode.GDbtnStartObjects1);

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtnStartObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8342060);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


};

gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDescolaPlayObjects1.length = 0;
gdjs.StartCode.GDescolaPlayObjects2.length = 0;
gdjs.StartCode.GDJOGOObjects1.length = 0;
gdjs.StartCode.GDJOGOObjects2.length = 0;
gdjs.StartCode.GDtitleObjects1.length = 0;
gdjs.StartCode.GDtitleObjects2.length = 0;
gdjs.StartCode.GDbtnStartObjects1.length = 0;
gdjs.StartCode.GDbtnStartObjects2.length = 0;
gdjs.StartCode.GDlblStartObjects1.length = 0;
gdjs.StartCode.GDlblStartObjects2.length = 0;
gdjs.StartCode.GDautorObjects1.length = 0;
gdjs.StartCode.GDautorObjects2.length = 0;

gdjs.StartCode.eventsList0(runtimeScene);
return;

}

gdjs['StartCode'] = gdjs.StartCode;
