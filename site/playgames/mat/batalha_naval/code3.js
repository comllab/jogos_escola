gdjs.GameOverCode = {};
gdjs.GameOverCode.GDinimigosObjects1= [];
gdjs.GameOverCode.GDinimigosObjects2= [];
gdjs.GameOverCode.GDajudaObjects1= [];
gdjs.GameOverCode.GDajudaObjects2= [];
gdjs.GameOverCode.GDbackgroundObjects1= [];
gdjs.GameOverCode.GDbackgroundObjects2= [];
gdjs.GameOverCode.GDbtContinuarObjects1= [];
gdjs.GameOverCode.GDbtContinuarObjects2= [];
gdjs.GameOverCode.GDCopyOflblJogarObjects1= [];
gdjs.GameOverCode.GDCopyOflblJogarObjects2= [];
gdjs.GameOverCode.GDlblJogarObjects1= [];
gdjs.GameOverCode.GDlblJogarObjects2= [];
gdjs.GameOverCode.GDtituloObjects1= [];
gdjs.GameOverCode.GDtituloObjects2= [];
gdjs.GameOverCode.GDgameoverObjects1= [];
gdjs.GameOverCode.GDgameoverObjects2= [];

gdjs.GameOverCode.conditionTrue_0 = {val:false};
gdjs.GameOverCode.condition0IsTrue_0 = {val:false};
gdjs.GameOverCode.condition1IsTrue_0 = {val:false};
gdjs.GameOverCode.condition2IsTrue_0 = {val:false};
gdjs.GameOverCode.condition3IsTrue_0 = {val:false};
gdjs.GameOverCode.conditionTrue_1 = {val:false};
gdjs.GameOverCode.condition0IsTrue_1 = {val:false};
gdjs.GameOverCode.condition1IsTrue_1 = {val:false};
gdjs.GameOverCode.condition2IsTrue_1 = {val:false};
gdjs.GameOverCode.condition3IsTrue_1 = {val:false};


gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbtContinuarObjects1Objects = Hashtable.newFrom({"btContinuar": gdjs.GameOverCode.GDbtContinuarObjects1});gdjs.GameOverCode.eventsList0xafd70 = function(runtimeScene, context) {

{


{
gdjs.GameOverCode.GDbackgroundObjects1.createFrom(runtimeScene.getObjects("background"));
{for(var i = 0, len = gdjs.GameOverCode.GDbackgroundObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDbackgroundObjects1[i].setXOffset(gdjs.GameOverCode.GDbackgroundObjects1[i].getXOffset() - (0.25));
}
}}

}


{

gdjs.GameOverCode.GDbtContinuarObjects1.createFrom(runtimeScene.getObjects("btContinuar"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
gdjs.GameOverCode.condition2IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbtContinuarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameOverCode.condition1IsTrue_0.val ) {
{
{gdjs.GameOverCode.conditionTrue_1 = gdjs.GameOverCode.condition2IsTrue_0;
gdjs.GameOverCode.conditionTrue_1.val = context.triggerOnce(9058932);
}
}}
}
if (gdjs.GameOverCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.GameOverCode.eventsList0xafd70


gdjs.GameOverCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GameOverCode.GDinimigosObjects1.length = 0;
gdjs.GameOverCode.GDinimigosObjects2.length = 0;
gdjs.GameOverCode.GDajudaObjects1.length = 0;
gdjs.GameOverCode.GDajudaObjects2.length = 0;
gdjs.GameOverCode.GDbackgroundObjects1.length = 0;
gdjs.GameOverCode.GDbackgroundObjects2.length = 0;
gdjs.GameOverCode.GDbtContinuarObjects1.length = 0;
gdjs.GameOverCode.GDbtContinuarObjects2.length = 0;
gdjs.GameOverCode.GDCopyOflblJogarObjects1.length = 0;
gdjs.GameOverCode.GDCopyOflblJogarObjects2.length = 0;
gdjs.GameOverCode.GDlblJogarObjects1.length = 0;
gdjs.GameOverCode.GDlblJogarObjects2.length = 0;
gdjs.GameOverCode.GDtituloObjects1.length = 0;
gdjs.GameOverCode.GDtituloObjects2.length = 0;
gdjs.GameOverCode.GDgameoverObjects1.length = 0;
gdjs.GameOverCode.GDgameoverObjects2.length = 0;

gdjs.GameOverCode.eventsList0xafd70(runtimeScene, context);return;
}
gdjs['GameOverCode']= gdjs.GameOverCode;
