gdjs.AjudaCode = {};
gdjs.AjudaCode.GDinimigosObjects1= [];
gdjs.AjudaCode.GDinimigosObjects2= [];
gdjs.AjudaCode.GDajudaObjects1= [];
gdjs.AjudaCode.GDajudaObjects2= [];
gdjs.AjudaCode.GDbackgroundObjects1= [];
gdjs.AjudaCode.GDbackgroundObjects2= [];
gdjs.AjudaCode.GDbtContinuarObjects1= [];
gdjs.AjudaCode.GDbtContinuarObjects2= [];
gdjs.AjudaCode.GDlblJogarObjects1= [];
gdjs.AjudaCode.GDlblJogarObjects2= [];
gdjs.AjudaCode.GDtituloObjects1= [];
gdjs.AjudaCode.GDtituloObjects2= [];
gdjs.AjudaCode.GDgameoverObjects1= [];
gdjs.AjudaCode.GDgameoverObjects2= [];
gdjs.AjudaCode.GDtxtAjuda5Objects1= [];
gdjs.AjudaCode.GDtxtAjuda5Objects2= [];
gdjs.AjudaCode.GDtxtAjuda4Objects1= [];
gdjs.AjudaCode.GDtxtAjuda4Objects2= [];
gdjs.AjudaCode.GDtxtAjuda3Objects1= [];
gdjs.AjudaCode.GDtxtAjuda3Objects2= [];
gdjs.AjudaCode.GDtxtAjuda2Objects1= [];
gdjs.AjudaCode.GDtxtAjuda2Objects2= [];
gdjs.AjudaCode.GDtxtAjudaObjects1= [];
gdjs.AjudaCode.GDtxtAjudaObjects2= [];
gdjs.AjudaCode.GDajuda1Objects1= [];
gdjs.AjudaCode.GDajuda1Objects2= [];
gdjs.AjudaCode.GDajuda3Objects1= [];
gdjs.AjudaCode.GDajuda3Objects2= [];

gdjs.AjudaCode.conditionTrue_0 = {val:false};
gdjs.AjudaCode.condition0IsTrue_0 = {val:false};
gdjs.AjudaCode.condition1IsTrue_0 = {val:false};
gdjs.AjudaCode.condition2IsTrue_0 = {val:false};
gdjs.AjudaCode.condition3IsTrue_0 = {val:false};
gdjs.AjudaCode.conditionTrue_1 = {val:false};
gdjs.AjudaCode.condition0IsTrue_1 = {val:false};
gdjs.AjudaCode.condition1IsTrue_1 = {val:false};
gdjs.AjudaCode.condition2IsTrue_1 = {val:false};
gdjs.AjudaCode.condition3IsTrue_1 = {val:false};


gdjs.AjudaCode.mapOfGDgdjs_46AjudaCode_46GDbtContinuarObjects1Objects = Hashtable.newFrom({"btContinuar": gdjs.AjudaCode.GDbtContinuarObjects1});gdjs.AjudaCode.eventsList0xafd70 = function(runtimeScene, context) {

{


{
gdjs.AjudaCode.GDbackgroundObjects1.createFrom(runtimeScene.getObjects("background"));
{for(var i = 0, len = gdjs.AjudaCode.GDbackgroundObjects1.length ;i < len;++i) {
    gdjs.AjudaCode.GDbackgroundObjects1[i].setXOffset(gdjs.AjudaCode.GDbackgroundObjects1[i].getXOffset() - (0.25));
}
}}

}


{

gdjs.AjudaCode.GDbtContinuarObjects1.createFrom(runtimeScene.getObjects("btContinuar"));

gdjs.AjudaCode.condition0IsTrue_0.val = false;
gdjs.AjudaCode.condition1IsTrue_0.val = false;
gdjs.AjudaCode.condition2IsTrue_0.val = false;
{
gdjs.AjudaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.AjudaCode.mapOfGDgdjs_46AjudaCode_46GDbtContinuarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.AjudaCode.condition0IsTrue_0.val ) {
{
gdjs.AjudaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.AjudaCode.condition1IsTrue_0.val ) {
{
{gdjs.AjudaCode.conditionTrue_1 = gdjs.AjudaCode.condition2IsTrue_0;
gdjs.AjudaCode.conditionTrue_1.val = context.triggerOnce(9063028);
}
}}
}
if (gdjs.AjudaCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.popScene(runtimeScene);
}}

}


}; //End of gdjs.AjudaCode.eventsList0xafd70


gdjs.AjudaCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.AjudaCode.GDinimigosObjects1.length = 0;
gdjs.AjudaCode.GDinimigosObjects2.length = 0;
gdjs.AjudaCode.GDajudaObjects1.length = 0;
gdjs.AjudaCode.GDajudaObjects2.length = 0;
gdjs.AjudaCode.GDbackgroundObjects1.length = 0;
gdjs.AjudaCode.GDbackgroundObjects2.length = 0;
gdjs.AjudaCode.GDbtContinuarObjects1.length = 0;
gdjs.AjudaCode.GDbtContinuarObjects2.length = 0;
gdjs.AjudaCode.GDlblJogarObjects1.length = 0;
gdjs.AjudaCode.GDlblJogarObjects2.length = 0;
gdjs.AjudaCode.GDtituloObjects1.length = 0;
gdjs.AjudaCode.GDtituloObjects2.length = 0;
gdjs.AjudaCode.GDgameoverObjects1.length = 0;
gdjs.AjudaCode.GDgameoverObjects2.length = 0;
gdjs.AjudaCode.GDtxtAjuda5Objects1.length = 0;
gdjs.AjudaCode.GDtxtAjuda5Objects2.length = 0;
gdjs.AjudaCode.GDtxtAjuda4Objects1.length = 0;
gdjs.AjudaCode.GDtxtAjuda4Objects2.length = 0;
gdjs.AjudaCode.GDtxtAjuda3Objects1.length = 0;
gdjs.AjudaCode.GDtxtAjuda3Objects2.length = 0;
gdjs.AjudaCode.GDtxtAjuda2Objects1.length = 0;
gdjs.AjudaCode.GDtxtAjuda2Objects2.length = 0;
gdjs.AjudaCode.GDtxtAjudaObjects1.length = 0;
gdjs.AjudaCode.GDtxtAjudaObjects2.length = 0;
gdjs.AjudaCode.GDajuda1Objects1.length = 0;
gdjs.AjudaCode.GDajuda1Objects2.length = 0;
gdjs.AjudaCode.GDajuda3Objects1.length = 0;
gdjs.AjudaCode.GDajuda3Objects2.length = 0;

gdjs.AjudaCode.eventsList0xafd70(runtimeScene, context);return;
}
gdjs['AjudaCode']= gdjs.AjudaCode;
