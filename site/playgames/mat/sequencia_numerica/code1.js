gdjs.SequenciaCode = {};
gdjs.SequenciaCode.GDnumerosObjects2_1final = [];

gdjs.SequenciaCode.repeatCount2 = 0;

gdjs.SequenciaCode.repeatIndex2 = 0;

gdjs.SequenciaCode.GDCar3Objects1= [];
gdjs.SequenciaCode.GDCar3Objects2= [];
gdjs.SequenciaCode.GDCar3Objects3= [];
gdjs.SequenciaCode.GDCar3Objects4= [];
gdjs.SequenciaCode.GDCar2Objects1= [];
gdjs.SequenciaCode.GDCar2Objects2= [];
gdjs.SequenciaCode.GDCar2Objects3= [];
gdjs.SequenciaCode.GDCar2Objects4= [];
gdjs.SequenciaCode.GDCar1Objects1= [];
gdjs.SequenciaCode.GDCar1Objects2= [];
gdjs.SequenciaCode.GDCar1Objects3= [];
gdjs.SequenciaCode.GDCar1Objects4= [];
gdjs.SequenciaCode.GDTire3Objects1= [];
gdjs.SequenciaCode.GDTire3Objects2= [];
gdjs.SequenciaCode.GDTire3Objects3= [];
gdjs.SequenciaCode.GDTire3Objects4= [];
gdjs.SequenciaCode.GDTire2Objects1= [];
gdjs.SequenciaCode.GDTire2Objects2= [];
gdjs.SequenciaCode.GDTire2Objects3= [];
gdjs.SequenciaCode.GDTire2Objects4= [];
gdjs.SequenciaCode.GDTire1Objects1= [];
gdjs.SequenciaCode.GDTire1Objects2= [];
gdjs.SequenciaCode.GDTire1Objects3= [];
gdjs.SequenciaCode.GDTire1Objects4= [];
gdjs.SequenciaCode.GDgroundBObjects1= [];
gdjs.SequenciaCode.GDgroundBObjects2= [];
gdjs.SequenciaCode.GDgroundBObjects3= [];
gdjs.SequenciaCode.GDgroundBObjects4= [];
gdjs.SequenciaCode.GDgroundObjects1= [];
gdjs.SequenciaCode.GDgroundObjects2= [];
gdjs.SequenciaCode.GDgroundObjects3= [];
gdjs.SequenciaCode.GDgroundObjects4= [];
gdjs.SequenciaCode.GDbackgroundObjects1= [];
gdjs.SequenciaCode.GDbackgroundObjects2= [];
gdjs.SequenciaCode.GDbackgroundObjects3= [];
gdjs.SequenciaCode.GDbackgroundObjects4= [];
gdjs.SequenciaCode.GDnumerosObjects1= [];
gdjs.SequenciaCode.GDnumerosObjects2= [];
gdjs.SequenciaCode.GDnumerosObjects3= [];
gdjs.SequenciaCode.GDnumerosObjects4= [];
gdjs.SequenciaCode.GDlblCompleteObjects1= [];
gdjs.SequenciaCode.GDlblCompleteObjects2= [];
gdjs.SequenciaCode.GDlblCompleteObjects3= [];
gdjs.SequenciaCode.GDlblCompleteObjects4= [];
gdjs.SequenciaCode.GDinputObjects1= [];
gdjs.SequenciaCode.GDinputObjects2= [];
gdjs.SequenciaCode.GDinputObjects3= [];
gdjs.SequenciaCode.GDinputObjects4= [];
gdjs.SequenciaCode.GDtxtNumeroObjects1= [];
gdjs.SequenciaCode.GDtxtNumeroObjects2= [];
gdjs.SequenciaCode.GDtxtNumeroObjects3= [];
gdjs.SequenciaCode.GDtxtNumeroObjects4= [];
gdjs.SequenciaCode.GDbtReloadObjects1= [];
gdjs.SequenciaCode.GDbtReloadObjects2= [];
gdjs.SequenciaCode.GDbtReloadObjects3= [];
gdjs.SequenciaCode.GDbtReloadObjects4= [];
gdjs.SequenciaCode.GDbtContinuarObjects1= [];
gdjs.SequenciaCode.GDbtContinuarObjects2= [];
gdjs.SequenciaCode.GDbtContinuarObjects3= [];
gdjs.SequenciaCode.GDbtContinuarObjects4= [];
gdjs.SequenciaCode.GDlblAlertObjects1= [];
gdjs.SequenciaCode.GDlblAlertObjects2= [];
gdjs.SequenciaCode.GDlblAlertObjects3= [];
gdjs.SequenciaCode.GDlblAlertObjects4= [];
gdjs.SequenciaCode.GDdialogObjects1= [];
gdjs.SequenciaCode.GDdialogObjects2= [];
gdjs.SequenciaCode.GDdialogObjects3= [];
gdjs.SequenciaCode.GDdialogObjects4= [];

gdjs.SequenciaCode.conditionTrue_0 = {val:false};
gdjs.SequenciaCode.condition0IsTrue_0 = {val:false};
gdjs.SequenciaCode.condition1IsTrue_0 = {val:false};
gdjs.SequenciaCode.condition2IsTrue_0 = {val:false};
gdjs.SequenciaCode.condition3IsTrue_0 = {val:false};
gdjs.SequenciaCode.condition4IsTrue_0 = {val:false};
gdjs.SequenciaCode.condition5IsTrue_0 = {val:false};
gdjs.SequenciaCode.conditionTrue_1 = {val:false};
gdjs.SequenciaCode.condition0IsTrue_1 = {val:false};
gdjs.SequenciaCode.condition1IsTrue_1 = {val:false};
gdjs.SequenciaCode.condition2IsTrue_1 = {val:false};
gdjs.SequenciaCode.condition3IsTrue_1 = {val:false};
gdjs.SequenciaCode.condition4IsTrue_1 = {val:false};
gdjs.SequenciaCode.condition5IsTrue_1 = {val:false};


gdjs.SequenciaCode.eventsList0x6c91c8 = function(runtimeScene) {

{

gdjs.SequenciaCode.GDnumerosObjects3.createFrom(runtimeScene.getObjects("numeros"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects3.length;i<l;++i) {
    if ( gdjs.SequenciaCode.GDnumerosObjects3[i].getAnimation() == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("n")) ) {
        gdjs.SequenciaCode.condition0IsTrue_0.val = true;
        gdjs.SequenciaCode.GDnumerosObjects3[k] = gdjs.SequenciaCode.GDnumerosObjects3[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects3.length = k;}if (gdjs.SequenciaCode.condition0IsTrue_0.val) {
/* Reuse gdjs.SequenciaCode.GDnumerosObjects3 */
{for(var i = 0, len = gdjs.SequenciaCode.GDnumerosObjects3.length ;i < len;++i) {
    gdjs.SequenciaCode.GDnumerosObjects3[i].setAnimation(0);
}
}}

}


}; //End of gdjs.SequenciaCode.eventsList0x6c91c8
gdjs.SequenciaCode.eventsList0x6c6118 = function(runtimeScene) {

{


{
gdjs.SequenciaCode.GDnumerosObjects2.createFrom(runtimeScene.getObjects("numeros"));
{for(var i = 0, len = gdjs.SequenciaCode.GDnumerosObjects2.length ;i < len;++i) {
    gdjs.SequenciaCode.GDnumerosObjects2[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.SequenciaCode.GDnumerosObjects2[i].getVariables().get("numero"))));
}
}}

}


{


gdjs.SequenciaCode.repeatCount2 = 10;
for(gdjs.SequenciaCode.repeatIndex2 = 0;gdjs.SequenciaCode.repeatIndex2 < gdjs.SequenciaCode.repeatCount2;++gdjs.SequenciaCode.repeatIndex2) {

if (true)
{
{runtimeScene.getVariables().get("n").setNumber(gdjs.random(49) + 1);
}
{ //Subevents: 
gdjs.SequenciaCode.eventsList0x6c91c8(runtimeScene);} //Subevents end.
}
}

}


}; //End of gdjs.SequenciaCode.eventsList0x6c6118
gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDCar1Objects1Objects = Hashtable.newFrom({"Car1": gdjs.SequenciaCode.GDCar1Objects1});gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDCar2Objects1Objects = Hashtable.newFrom({"Car2": gdjs.SequenciaCode.GDCar2Objects1});gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDCar3Objects1Objects = Hashtable.newFrom({"Car3": gdjs.SequenciaCode.GDCar3Objects1});gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDnumerosObjects1Objects = Hashtable.newFrom({"numeros": gdjs.SequenciaCode.GDnumerosObjects1});gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDnumerosObjects1Objects = Hashtable.newFrom({"numeros": gdjs.SequenciaCode.GDnumerosObjects1});gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDbtReloadObjects1Objects = Hashtable.newFrom({"btReload": gdjs.SequenciaCode.GDbtReloadObjects1});gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDbtContinuarObjects1Objects = Hashtable.newFrom({"btContinuar": gdjs.SequenciaCode.GDbtContinuarObjects1});gdjs.SequenciaCode.eventsList0x6cbe90 = function(runtimeScene) {

{

gdjs.SequenciaCode.GDnumerosObjects2.createFrom(runtimeScene.getObjects("numeros"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
gdjs.SequenciaCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects2.length;i<l;++i) {
    if ( !(gdjs.SequenciaCode.GDnumerosObjects2[i].getAnimation() == 0) ) {
        gdjs.SequenciaCode.condition0IsTrue_0.val = true;
        gdjs.SequenciaCode.GDnumerosObjects2[k] = gdjs.SequenciaCode.GDnumerosObjects2[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects2.length = k;}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects2.length;i<l;++i) {
    if ( !(gdjs.SequenciaCode.GDnumerosObjects2[i].getAnimation() == 51) ) {
        gdjs.SequenciaCode.condition1IsTrue_0.val = true;
        gdjs.SequenciaCode.GDnumerosObjects2[k] = gdjs.SequenciaCode.GDnumerosObjects2[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects2.length = k;}if ( gdjs.SequenciaCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects2.length;i<l;++i) {
    if ( !(gdjs.SequenciaCode.GDnumerosObjects2[i].getAnimation() == (gdjs.RuntimeObject.getVariableNumber(gdjs.SequenciaCode.GDnumerosObjects2[i].getVariables().get("numero")))) ) {
        gdjs.SequenciaCode.condition2IsTrue_0.val = true;
        gdjs.SequenciaCode.GDnumerosObjects2[k] = gdjs.SequenciaCode.GDnumerosObjects2[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects2.length = k;}}
}
if (gdjs.SequenciaCode.condition2IsTrue_0.val) {
/* Reuse gdjs.SequenciaCode.GDnumerosObjects2 */
{for(var i = 0, len = gdjs.SequenciaCode.GDnumerosObjects2.length ;i < len;++i) {
    gdjs.SequenciaCode.GDnumerosObjects2[i].setAnimation(0);
}
}}

}


{

gdjs.SequenciaCode.GDnumerosObjects2.length = 0;


gdjs.SequenciaCode.condition0IsTrue_0.val = false;
{
{gdjs.SequenciaCode.conditionTrue_1 = gdjs.SequenciaCode.condition0IsTrue_0;
gdjs.SequenciaCode.GDnumerosObjects2_1final.length = 0;gdjs.SequenciaCode.condition0IsTrue_1.val = false;
gdjs.SequenciaCode.condition1IsTrue_1.val = false;
{
gdjs.SequenciaCode.GDnumerosObjects3.createFrom(runtimeScene.getObjects("numeros"));
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects3.length;i<l;++i) {
    if ( gdjs.SequenciaCode.GDnumerosObjects3[i].getAnimation() == 0 ) {
        gdjs.SequenciaCode.condition0IsTrue_1.val = true;
        gdjs.SequenciaCode.GDnumerosObjects3[k] = gdjs.SequenciaCode.GDnumerosObjects3[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects3.length = k;if( gdjs.SequenciaCode.condition0IsTrue_1.val ) {
    gdjs.SequenciaCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.SequenciaCode.GDnumerosObjects3.length;j<jLen;++j) {
        if ( gdjs.SequenciaCode.GDnumerosObjects2_1final.indexOf(gdjs.SequenciaCode.GDnumerosObjects3[j]) === -1 )
            gdjs.SequenciaCode.GDnumerosObjects2_1final.push(gdjs.SequenciaCode.GDnumerosObjects3[j]);
    }
}
}
{
gdjs.SequenciaCode.GDnumerosObjects3.createFrom(runtimeScene.getObjects("numeros"));
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects3.length;i<l;++i) {
    if ( gdjs.SequenciaCode.GDnumerosObjects3[i].getAnimation() == 51 ) {
        gdjs.SequenciaCode.condition1IsTrue_1.val = true;
        gdjs.SequenciaCode.GDnumerosObjects3[k] = gdjs.SequenciaCode.GDnumerosObjects3[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects3.length = k;if( gdjs.SequenciaCode.condition1IsTrue_1.val ) {
    gdjs.SequenciaCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.SequenciaCode.GDnumerosObjects3.length;j<jLen;++j) {
        if ( gdjs.SequenciaCode.GDnumerosObjects2_1final.indexOf(gdjs.SequenciaCode.GDnumerosObjects3[j]) === -1 )
            gdjs.SequenciaCode.GDnumerosObjects2_1final.push(gdjs.SequenciaCode.GDnumerosObjects3[j]);
    }
}
}
{
gdjs.SequenciaCode.GDnumerosObjects2.createFrom(gdjs.SequenciaCode.GDnumerosObjects2_1final);
}
}
}if (gdjs.SequenciaCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("check").add(1);
}}

}


{


gdjs.SequenciaCode.condition0IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("check")) == 0;
}if (gdjs.SequenciaCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{


gdjs.SequenciaCode.condition0IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("check")) > 0;
}if (gdjs.SequenciaCode.condition0IsTrue_0.val) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "Dialog");
}}

}


}; //End of gdjs.SequenciaCode.eventsList0x6cbe90
gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDdialogObjects1Objects = Hashtable.newFrom({"dialog": gdjs.SequenciaCode.GDdialogObjects1});gdjs.SequenciaCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.SequenciaCode.condition0IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.SequenciaCode.condition0IsTrue_0.val) {
gdjs.SequenciaCode.GDtxtNumeroObjects1.createFrom(runtimeScene.getObjects("txtNumero"));
{gdjs.evtTools.camera.hideLayer(runtimeScene, "Dialog");
}{for(var i = 0, len = gdjs.SequenciaCode.GDtxtNumeroObjects1.length ;i < len;++i) {
    gdjs.SequenciaCode.GDtxtNumeroObjects1[i].hide();
}
}
{ //Subevents
gdjs.SequenciaCode.eventsList0x6c6118(runtimeScene);} //End of subevents
}

}


{

gdjs.SequenciaCode.GDCar1Objects1.createFrom(runtimeScene.getObjects("Car1"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
gdjs.SequenciaCode.condition2IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDCar1Objects1Objects, runtimeScene, true, false);
}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
gdjs.SequenciaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.SequenciaCode.condition1IsTrue_0.val ) {
{
{gdjs.SequenciaCode.conditionTrue_1 = gdjs.SequenciaCode.condition2IsTrue_0;
gdjs.SequenciaCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7116572);
}
}}
}
if (gdjs.SequenciaCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{

gdjs.SequenciaCode.GDCar2Objects1.createFrom(runtimeScene.getObjects("Car2"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
gdjs.SequenciaCode.condition2IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDCar2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
gdjs.SequenciaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.SequenciaCode.condition1IsTrue_0.val ) {
{
{gdjs.SequenciaCode.conditionTrue_1 = gdjs.SequenciaCode.condition2IsTrue_0;
gdjs.SequenciaCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7117580);
}
}}
}
if (gdjs.SequenciaCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(2);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{

gdjs.SequenciaCode.GDCar3Objects1.createFrom(runtimeScene.getObjects("Car3"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
gdjs.SequenciaCode.condition2IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDCar3Objects1Objects, runtimeScene, true, false);
}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
gdjs.SequenciaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.SequenciaCode.condition1IsTrue_0.val ) {
{
{gdjs.SequenciaCode.conditionTrue_1 = gdjs.SequenciaCode.condition2IsTrue_0;
gdjs.SequenciaCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7118580);
}
}}
}
if (gdjs.SequenciaCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(3);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{



}


{

gdjs.SequenciaCode.GDinputObjects1.createFrom(runtimeScene.getObjects("input"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDinputObjects1.length;i<l;++i) {
    if ( gdjs.SequenciaCode.GDinputObjects1[i].isActivated() ) {
        gdjs.SequenciaCode.condition0IsTrue_0.val = true;
        gdjs.SequenciaCode.GDinputObjects1[k] = gdjs.SequenciaCode.GDinputObjects1[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDinputObjects1.length = k;}if (gdjs.SequenciaCode.condition0IsTrue_0.val) {
/* Reuse gdjs.SequenciaCode.GDinputObjects1 */
gdjs.SequenciaCode.GDtxtNumeroObjects1.createFrom(runtimeScene.getObjects("txtNumero"));
{for(var i = 0, len = gdjs.SequenciaCode.GDtxtNumeroObjects1.length ;i < len;++i) {
    gdjs.SequenciaCode.GDtxtNumeroObjects1[i].setString((( gdjs.SequenciaCode.GDinputObjects1.length === 0 ) ? "" :gdjs.SequenciaCode.GDinputObjects1[0].getString()));
}
}}

}


{

gdjs.SequenciaCode.GDnumerosObjects1.createFrom(runtimeScene.getObjects("numeros"));
gdjs.SequenciaCode.GDtxtNumeroObjects1.createFrom(runtimeScene.getObjects("txtNumero"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDtxtNumeroObjects1.length;i<l;++i) {
    if ( gdjs.SequenciaCode.GDtxtNumeroObjects1[i].getString() != "" ) {
        gdjs.SequenciaCode.condition0IsTrue_0.val = true;
        gdjs.SequenciaCode.GDtxtNumeroObjects1[k] = gdjs.SequenciaCode.GDtxtNumeroObjects1[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDtxtNumeroObjects1.length = k;}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects1.length;i<l;++i) {
    if ( gdjs.SequenciaCode.GDnumerosObjects1[i].getAnimation() == 51 ) {
        gdjs.SequenciaCode.condition1IsTrue_0.val = true;
        gdjs.SequenciaCode.GDnumerosObjects1[k] = gdjs.SequenciaCode.GDnumerosObjects1[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects1.length = k;}}
if (gdjs.SequenciaCode.condition1IsTrue_0.val) {
gdjs.SequenciaCode.GDinputObjects1.createFrom(runtimeScene.getObjects("input"));
/* Reuse gdjs.SequenciaCode.GDnumerosObjects1 */
{for(var i = 0, len = gdjs.SequenciaCode.GDnumerosObjects1.length ;i < len;++i) {
    gdjs.SequenciaCode.GDnumerosObjects1[i].setAnimation(gdjs.evtTools.common.toNumber((( gdjs.SequenciaCode.GDinputObjects1.length === 0 ) ? "" :gdjs.SequenciaCode.GDinputObjects1[0].getString())));
}
}{runtimeScene.getVariables().getFromIndex(1).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.SequenciaCode.GDnumerosObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.SequenciaCode.GDnumerosObjects1[0].getVariables()).get("numero"))));
}}

}


{

gdjs.SequenciaCode.GDnumerosObjects1.createFrom(runtimeScene.getObjects("numeros"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) > 0;
}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects1.length;i<l;++i) {
    if ( gdjs.SequenciaCode.GDnumerosObjects1[i].getVariableNumber(gdjs.SequenciaCode.GDnumerosObjects1[i].getVariables().get("numero")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.SequenciaCode.condition1IsTrue_0.val = true;
        gdjs.SequenciaCode.GDnumerosObjects1[k] = gdjs.SequenciaCode.GDnumerosObjects1[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects1.length = k;}}
if (gdjs.SequenciaCode.condition1IsTrue_0.val) {
/* Reuse gdjs.SequenciaCode.GDnumerosObjects1 */
gdjs.SequenciaCode.GDtxtNumeroObjects1.createFrom(runtimeScene.getObjects("txtNumero"));
{for(var i = 0, len = gdjs.SequenciaCode.GDnumerosObjects1.length ;i < len;++i) {
    gdjs.SequenciaCode.GDnumerosObjects1[i].setAnimation(gdjs.evtTools.common.toNumber((( gdjs.SequenciaCode.GDtxtNumeroObjects1.length === 0 ) ? "" :gdjs.SequenciaCode.GDtxtNumeroObjects1[0].getString())));
}
}}

}


{

gdjs.SequenciaCode.GDnumerosObjects1.createFrom(runtimeScene.getObjects("numeros"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
gdjs.SequenciaCode.condition2IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects1.length;i<l;++i) {
    if ( gdjs.SequenciaCode.GDnumerosObjects1[i].getAnimation() == 51 ) {
        gdjs.SequenciaCode.condition1IsTrue_0.val = true;
        gdjs.SequenciaCode.GDnumerosObjects1[k] = gdjs.SequenciaCode.GDnumerosObjects1[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects1.length = k;}if ( gdjs.SequenciaCode.condition1IsTrue_0.val ) {
{
{gdjs.SequenciaCode.conditionTrue_1 = gdjs.SequenciaCode.condition2IsTrue_0;
gdjs.SequenciaCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7122036);
}
}}
}
if (gdjs.SequenciaCode.condition2IsTrue_0.val) {
/* Reuse gdjs.SequenciaCode.GDnumerosObjects1 */
{for(var i = 0, len = gdjs.SequenciaCode.GDnumerosObjects1.length ;i < len;++i) {
    gdjs.SequenciaCode.GDnumerosObjects1[i].setAnimation(0);
}
}}

}


{

gdjs.SequenciaCode.GDnumerosObjects1.createFrom(runtimeScene.getObjects("numeros"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
gdjs.SequenciaCode.condition2IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDnumerosObjects1Objects, runtimeScene, true, false);
}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects1.length;i<l;++i) {
    if ( gdjs.SequenciaCode.GDnumerosObjects1[i].getAnimation() == 0 ) {
        gdjs.SequenciaCode.condition1IsTrue_0.val = true;
        gdjs.SequenciaCode.GDnumerosObjects1[k] = gdjs.SequenciaCode.GDnumerosObjects1[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects1.length = k;}if ( gdjs.SequenciaCode.condition1IsTrue_0.val ) {
{
gdjs.SequenciaCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
if (gdjs.SequenciaCode.condition2IsTrue_0.val) {
gdjs.SequenciaCode.GDinputObjects1.createFrom(runtimeScene.getObjects("input"));
/* Reuse gdjs.SequenciaCode.GDnumerosObjects1 */
gdjs.SequenciaCode.GDtxtNumeroObjects1.createFrom(runtimeScene.getObjects("txtNumero"));
{for(var i = 0, len = gdjs.SequenciaCode.GDnumerosObjects1.length ;i < len;++i) {
    gdjs.SequenciaCode.GDnumerosObjects1[i].setAnimation(51);
}
}{for(var i = 0, len = gdjs.SequenciaCode.GDtxtNumeroObjects1.length ;i < len;++i) {
    gdjs.SequenciaCode.GDtxtNumeroObjects1[i].setString("");
}
}{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}{for(var i = 0, len = gdjs.SequenciaCode.GDinputObjects1.length ;i < len;++i) {
    gdjs.SequenciaCode.GDinputObjects1[i].setString("");
}
}}

}


{

gdjs.SequenciaCode.GDnumerosObjects1.createFrom(runtimeScene.getObjects("numeros"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
gdjs.SequenciaCode.condition2IsTrue_0.val = false;
gdjs.SequenciaCode.condition3IsTrue_0.val = false;
gdjs.SequenciaCode.condition4IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDnumerosObjects1Objects, runtimeScene, true, false);
}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
gdjs.SequenciaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.SequenciaCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects1.length;i<l;++i) {
    if ( !(gdjs.SequenciaCode.GDnumerosObjects1[i].getAnimation() == 0) ) {
        gdjs.SequenciaCode.condition2IsTrue_0.val = true;
        gdjs.SequenciaCode.GDnumerosObjects1[k] = gdjs.SequenciaCode.GDnumerosObjects1[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects1.length = k;}if ( gdjs.SequenciaCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects1.length;i<l;++i) {
    if ( !(gdjs.SequenciaCode.GDnumerosObjects1[i].getAnimation() == 51) ) {
        gdjs.SequenciaCode.condition3IsTrue_0.val = true;
        gdjs.SequenciaCode.GDnumerosObjects1[k] = gdjs.SequenciaCode.GDnumerosObjects1[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects1.length = k;}if ( gdjs.SequenciaCode.condition3IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.SequenciaCode.GDnumerosObjects1.length;i<l;++i) {
    if ( !(gdjs.SequenciaCode.GDnumerosObjects1[i].getAnimation() == (gdjs.RuntimeObject.getVariableNumber(gdjs.SequenciaCode.GDnumerosObjects1[i].getVariables().get("numero")))) ) {
        gdjs.SequenciaCode.condition4IsTrue_0.val = true;
        gdjs.SequenciaCode.GDnumerosObjects1[k] = gdjs.SequenciaCode.GDnumerosObjects1[i];
        ++k;
    }
}
gdjs.SequenciaCode.GDnumerosObjects1.length = k;}}
}
}
}
if (gdjs.SequenciaCode.condition4IsTrue_0.val) {
gdjs.SequenciaCode.GDinputObjects1.createFrom(runtimeScene.getObjects("input"));
/* Reuse gdjs.SequenciaCode.GDnumerosObjects1 */
gdjs.SequenciaCode.GDtxtNumeroObjects1.createFrom(runtimeScene.getObjects("txtNumero"));
{for(var i = 0, len = gdjs.SequenciaCode.GDnumerosObjects1.length ;i < len;++i) {
    gdjs.SequenciaCode.GDnumerosObjects1[i].setAnimation(51);
}
}{for(var i = 0, len = gdjs.SequenciaCode.GDtxtNumeroObjects1.length ;i < len;++i) {
    gdjs.SequenciaCode.GDtxtNumeroObjects1[i].setString("");
}
}{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}{for(var i = 0, len = gdjs.SequenciaCode.GDinputObjects1.length ;i < len;++i) {
    gdjs.SequenciaCode.GDinputObjects1[i].setString("");
}
}}

}


{



}


{

gdjs.SequenciaCode.GDbtReloadObjects1.createFrom(runtimeScene.getObjects("btReload"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
gdjs.SequenciaCode.condition2IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDbtReloadObjects1Objects, runtimeScene, true, false);
}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
gdjs.SequenciaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.SequenciaCode.condition1IsTrue_0.val ) {
{
{gdjs.SequenciaCode.conditionTrue_1 = gdjs.SequenciaCode.condition2IsTrue_0;
gdjs.SequenciaCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7126284);
}
}}
}
if (gdjs.SequenciaCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Sequencia", false);
}}

}


{

gdjs.SequenciaCode.GDbtContinuarObjects1.createFrom(runtimeScene.getObjects("btContinuar"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
gdjs.SequenciaCode.condition2IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDbtContinuarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
gdjs.SequenciaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.SequenciaCode.condition1IsTrue_0.val ) {
{
{gdjs.SequenciaCode.conditionTrue_1 = gdjs.SequenciaCode.condition2IsTrue_0;
gdjs.SequenciaCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7127132);
}
}}
}
if (gdjs.SequenciaCode.condition2IsTrue_0.val) {
{runtimeScene.getVariables().get("check").setNumber(0);
}
{ //Subevents
gdjs.SequenciaCode.eventsList0x6cbe90(runtimeScene);} //End of subevents
}

}


{

gdjs.SequenciaCode.GDdialogObjects1.createFrom(runtimeScene.getObjects("dialog"));

gdjs.SequenciaCode.condition0IsTrue_0.val = false;
gdjs.SequenciaCode.condition1IsTrue_0.val = false;
{
gdjs.SequenciaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SequenciaCode.mapOfGDgdjs_46SequenciaCode_46GDdialogObjects1Objects, runtimeScene, true, false);
}if ( gdjs.SequenciaCode.condition0IsTrue_0.val ) {
{
gdjs.SequenciaCode.condition1IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Dialog");
}}
if (gdjs.SequenciaCode.condition1IsTrue_0.val) {
{gdjs.evtTools.camera.hideLayer(runtimeScene, "Dialog");
}}

}


}; //End of gdjs.SequenciaCode.eventsList0xaff48


gdjs.SequenciaCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.SequenciaCode.GDCar3Objects1.length = 0;
gdjs.SequenciaCode.GDCar3Objects2.length = 0;
gdjs.SequenciaCode.GDCar3Objects3.length = 0;
gdjs.SequenciaCode.GDCar3Objects4.length = 0;
gdjs.SequenciaCode.GDCar2Objects1.length = 0;
gdjs.SequenciaCode.GDCar2Objects2.length = 0;
gdjs.SequenciaCode.GDCar2Objects3.length = 0;
gdjs.SequenciaCode.GDCar2Objects4.length = 0;
gdjs.SequenciaCode.GDCar1Objects1.length = 0;
gdjs.SequenciaCode.GDCar1Objects2.length = 0;
gdjs.SequenciaCode.GDCar1Objects3.length = 0;
gdjs.SequenciaCode.GDCar1Objects4.length = 0;
gdjs.SequenciaCode.GDTire3Objects1.length = 0;
gdjs.SequenciaCode.GDTire3Objects2.length = 0;
gdjs.SequenciaCode.GDTire3Objects3.length = 0;
gdjs.SequenciaCode.GDTire3Objects4.length = 0;
gdjs.SequenciaCode.GDTire2Objects1.length = 0;
gdjs.SequenciaCode.GDTire2Objects2.length = 0;
gdjs.SequenciaCode.GDTire2Objects3.length = 0;
gdjs.SequenciaCode.GDTire2Objects4.length = 0;
gdjs.SequenciaCode.GDTire1Objects1.length = 0;
gdjs.SequenciaCode.GDTire1Objects2.length = 0;
gdjs.SequenciaCode.GDTire1Objects3.length = 0;
gdjs.SequenciaCode.GDTire1Objects4.length = 0;
gdjs.SequenciaCode.GDgroundBObjects1.length = 0;
gdjs.SequenciaCode.GDgroundBObjects2.length = 0;
gdjs.SequenciaCode.GDgroundBObjects3.length = 0;
gdjs.SequenciaCode.GDgroundBObjects4.length = 0;
gdjs.SequenciaCode.GDgroundObjects1.length = 0;
gdjs.SequenciaCode.GDgroundObjects2.length = 0;
gdjs.SequenciaCode.GDgroundObjects3.length = 0;
gdjs.SequenciaCode.GDgroundObjects4.length = 0;
gdjs.SequenciaCode.GDbackgroundObjects1.length = 0;
gdjs.SequenciaCode.GDbackgroundObjects2.length = 0;
gdjs.SequenciaCode.GDbackgroundObjects3.length = 0;
gdjs.SequenciaCode.GDbackgroundObjects4.length = 0;
gdjs.SequenciaCode.GDnumerosObjects1.length = 0;
gdjs.SequenciaCode.GDnumerosObjects2.length = 0;
gdjs.SequenciaCode.GDnumerosObjects3.length = 0;
gdjs.SequenciaCode.GDnumerosObjects4.length = 0;
gdjs.SequenciaCode.GDlblCompleteObjects1.length = 0;
gdjs.SequenciaCode.GDlblCompleteObjects2.length = 0;
gdjs.SequenciaCode.GDlblCompleteObjects3.length = 0;
gdjs.SequenciaCode.GDlblCompleteObjects4.length = 0;
gdjs.SequenciaCode.GDinputObjects1.length = 0;
gdjs.SequenciaCode.GDinputObjects2.length = 0;
gdjs.SequenciaCode.GDinputObjects3.length = 0;
gdjs.SequenciaCode.GDinputObjects4.length = 0;
gdjs.SequenciaCode.GDtxtNumeroObjects1.length = 0;
gdjs.SequenciaCode.GDtxtNumeroObjects2.length = 0;
gdjs.SequenciaCode.GDtxtNumeroObjects3.length = 0;
gdjs.SequenciaCode.GDtxtNumeroObjects4.length = 0;
gdjs.SequenciaCode.GDbtReloadObjects1.length = 0;
gdjs.SequenciaCode.GDbtReloadObjects2.length = 0;
gdjs.SequenciaCode.GDbtReloadObjects3.length = 0;
gdjs.SequenciaCode.GDbtReloadObjects4.length = 0;
gdjs.SequenciaCode.GDbtContinuarObjects1.length = 0;
gdjs.SequenciaCode.GDbtContinuarObjects2.length = 0;
gdjs.SequenciaCode.GDbtContinuarObjects3.length = 0;
gdjs.SequenciaCode.GDbtContinuarObjects4.length = 0;
gdjs.SequenciaCode.GDlblAlertObjects1.length = 0;
gdjs.SequenciaCode.GDlblAlertObjects2.length = 0;
gdjs.SequenciaCode.GDlblAlertObjects3.length = 0;
gdjs.SequenciaCode.GDlblAlertObjects4.length = 0;
gdjs.SequenciaCode.GDdialogObjects1.length = 0;
gdjs.SequenciaCode.GDdialogObjects2.length = 0;
gdjs.SequenciaCode.GDdialogObjects3.length = 0;
gdjs.SequenciaCode.GDdialogObjects4.length = 0;

gdjs.SequenciaCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['SequenciaCode'] = gdjs.SequenciaCode;
