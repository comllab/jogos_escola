gdjs.Fase9Code = {};
gdjs.Fase9Code.GDdinoObjects1= [];
gdjs.Fase9Code.GDdinoObjects2= [];
gdjs.Fase9Code.GDdinoObjects3= [];
gdjs.Fase9Code.GDplayerObjects1= [];
gdjs.Fase9Code.GDplayerObjects2= [];
gdjs.Fase9Code.GDplayerObjects3= [];
gdjs.Fase9Code.GDground1Objects1= [];
gdjs.Fase9Code.GDground1Objects2= [];
gdjs.Fase9Code.GDground1Objects3= [];
gdjs.Fase9Code.GDwaterObjects1= [];
gdjs.Fase9Code.GDwaterObjects2= [];
gdjs.Fase9Code.GDwaterObjects3= [];
gdjs.Fase9Code.GDfade_95faseObjects1= [];
gdjs.Fase9Code.GDfade_95faseObjects2= [];
gdjs.Fase9Code.GDfade_95faseObjects3= [];
gdjs.Fase9Code.GDlblFaseObjects1= [];
gdjs.Fase9Code.GDlblFaseObjects2= [];
gdjs.Fase9Code.GDlblFaseObjects3= [];
gdjs.Fase9Code.GDbg1Objects1= [];
gdjs.Fase9Code.GDbg1Objects2= [];
gdjs.Fase9Code.GDbg1Objects3= [];
gdjs.Fase9Code.GDboxObjects1= [];
gdjs.Fase9Code.GDboxObjects2= [];
gdjs.Fase9Code.GDboxObjects3= [];
gdjs.Fase9Code.GDplacaObjects1= [];
gdjs.Fase9Code.GDplacaObjects2= [];
gdjs.Fase9Code.GDplacaObjects3= [];
gdjs.Fase9Code.GDthreeObjects1= [];
gdjs.Fase9Code.GDthreeObjects2= [];
gdjs.Fase9Code.GDthreeObjects3= [];
gdjs.Fase9Code.GDenemy1Objects1= [];
gdjs.Fase9Code.GDenemy1Objects2= [];
gdjs.Fase9Code.GDenemy1Objects3= [];
gdjs.Fase9Code.GDflipEnemyObjects1= [];
gdjs.Fase9Code.GDflipEnemyObjects2= [];
gdjs.Fase9Code.GDflipEnemyObjects3= [];
gdjs.Fase9Code.GDground1bordasObjects1= [];
gdjs.Fase9Code.GDground1bordasObjects2= [];
gdjs.Fase9Code.GDground1bordasObjects3= [];
gdjs.Fase9Code.GDbigGroundObjects1= [];
gdjs.Fase9Code.GDbigGroundObjects2= [];
gdjs.Fase9Code.GDbigGroundObjects3= [];
gdjs.Fase9Code.GDplataforma1Objects1= [];
gdjs.Fase9Code.GDplataforma1Objects2= [];
gdjs.Fase9Code.GDplataforma1Objects3= [];
gdjs.Fase9Code.GDendObjects1= [];
gdjs.Fase9Code.GDendObjects2= [];
gdjs.Fase9Code.GDendObjects3= [];

gdjs.Fase9Code.conditionTrue_0 = {val:false};
gdjs.Fase9Code.condition0IsTrue_0 = {val:false};
gdjs.Fase9Code.condition1IsTrue_0 = {val:false};
gdjs.Fase9Code.condition2IsTrue_0 = {val:false};
gdjs.Fase9Code.condition3IsTrue_0 = {val:false};
gdjs.Fase9Code.condition4IsTrue_0 = {val:false};
gdjs.Fase9Code.condition5IsTrue_0 = {val:false};
gdjs.Fase9Code.conditionTrue_1 = {val:false};
gdjs.Fase9Code.condition0IsTrue_1 = {val:false};
gdjs.Fase9Code.condition1IsTrue_1 = {val:false};
gdjs.Fase9Code.condition2IsTrue_1 = {val:false};
gdjs.Fase9Code.condition3IsTrue_1 = {val:false};
gdjs.Fase9Code.condition4IsTrue_1 = {val:false};
gdjs.Fase9Code.condition5IsTrue_1 = {val:false};


gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDwaterObjects1Objects = Hashtable.newFrom({"water": gdjs.Fase9Code.GDwaterObjects1});gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase9Code.GDplayerObjects1});gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDendObjects1Objects = Hashtable.newFrom({"end": gdjs.Fase9Code.GDendObjects1});gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase9Code.GDenemy1Objects1});gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase9Code.GDplayerObjects1});gdjs.Fase9Code.eventsList0x7ed714 = function(runtimeScene) {

{

gdjs.Fase9Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase9Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
gdjs.Fase9Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase9Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDdinoObjects2[k] = gdjs.Fase9Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase9Code.GDdinoObjects2.length = k;}if ( gdjs.Fase9Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.Fase9Code.condition1IsTrue_0.val = true;
        gdjs.Fase9Code.GDplayerObjects2[k] = gdjs.Fase9Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase9Code.GDplayerObjects2.length = k;}}
if (gdjs.Fase9Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase9Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase9Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase9Code.GDdinoObjects2[i].setAnimationName("jump");
}
}}

}


{

gdjs.Fase9Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase9Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
gdjs.Fase9Code.condition1IsTrue_0.val = false;
gdjs.Fase9Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase9Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDdinoObjects2[k] = gdjs.Fase9Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase9Code.GDdinoObjects2.length = k;}if ( gdjs.Fase9Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving() ) {
        gdjs.Fase9Code.condition1IsTrue_0.val = true;
        gdjs.Fase9Code.GDplayerObjects2[k] = gdjs.Fase9Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase9Code.GDplayerObjects2.length = k;}if ( gdjs.Fase9Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase9Code.condition2IsTrue_0.val = true;
        gdjs.Fase9Code.GDplayerObjects2[k] = gdjs.Fase9Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase9Code.GDplayerObjects2.length = k;}}
}
if (gdjs.Fase9Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Fase9Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase9Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase9Code.GDdinoObjects2[i].setAnimationName("run");
}
}}

}


{

gdjs.Fase9Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase9Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
gdjs.Fase9Code.condition1IsTrue_0.val = false;
gdjs.Fase9Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase9Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDdinoObjects2[k] = gdjs.Fase9Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase9Code.GDdinoObjects2.length = k;}if ( gdjs.Fase9Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDplayerObjects2.length;i<l;++i) {
    if ( !(gdjs.Fase9Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Fase9Code.condition1IsTrue_0.val = true;
        gdjs.Fase9Code.GDplayerObjects2[k] = gdjs.Fase9Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase9Code.GDplayerObjects2.length = k;}if ( gdjs.Fase9Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase9Code.condition2IsTrue_0.val = true;
        gdjs.Fase9Code.GDplayerObjects2[k] = gdjs.Fase9Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase9Code.GDplayerObjects2.length = k;}}
}
if (gdjs.Fase9Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Fase9Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase9Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase9Code.GDdinoObjects2[i].setAnimationName("idle");
}
}}

}


{

gdjs.Fase9Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
gdjs.Fase9Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase9Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isJumping()) ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDplayerObjects1[k] = gdjs.Fase9Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDplayerObjects1.length = k;}if ( gdjs.Fase9Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase9Code.condition1IsTrue_0.val = true;
        gdjs.Fase9Code.GDplayerObjects1[k] = gdjs.Fase9Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase9Code.condition1IsTrue_0.val) {
gdjs.Fase9Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
{for(var i = 0, len = gdjs.Fase9Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDdinoObjects1[i].setAnimationName("falling");
}
}}

}


}; //End of gdjs.Fase9Code.eventsList0x7ed714
gdjs.Fase9Code.eventsList0x7cccb4 = function(runtimeScene) {

{


gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
gdjs.Fase9Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if (gdjs.Fase9Code.condition0IsTrue_0.val) {
gdjs.Fase9Code.GDdinoObjects2.createFrom(gdjs.Fase9Code.GDdinoObjects1);

{for(var i = 0, len = gdjs.Fase9Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase9Code.GDdinoObjects2[i].flipX(true);
}
}}

}


{


gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
gdjs.Fase9Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if (gdjs.Fase9Code.condition0IsTrue_0.val) {
gdjs.Fase9Code.GDdinoObjects2.createFrom(gdjs.Fase9Code.GDdinoObjects1);

{for(var i = 0, len = gdjs.Fase9Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase9Code.GDdinoObjects2[i].flipX(false);
}
}}

}


{

gdjs.Fase9Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
gdjs.Fase9Code.condition1IsTrue_0.val = false;
{
gdjs.Fase9Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.Fase9Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase9Code.condition1IsTrue_0.val = true;
        gdjs.Fase9Code.GDplayerObjects1[k] = gdjs.Fase9Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase9Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase9Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase9Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDplayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


}; //End of gdjs.Fase9Code.eventsList0x7cccb4
gdjs.Fase9Code.eventsList0x8b46b4 = function(runtimeScene) {

{

gdjs.Fase9Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDdinoObjects1[i].getVariableNumber(gdjs.Fase9Code.GDdinoObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDdinoObjects1[k] = gdjs.Fase9Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDdinoObjects1.length = k;}if (gdjs.Fase9Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.Fase9Code.eventsList0x7cccb4(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.Fase9Code.eventsList0x8b46b4
gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase9Code.GDenemy1Objects1});gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDflipEnemyObjects1Objects = Hashtable.newFrom({"flipEnemy": gdjs.Fase9Code.GDflipEnemyObjects1});gdjs.Fase9Code.eventsList0x8a8ea4 = function(runtimeScene) {

{

gdjs.Fase9Code.GDenemy1Objects2.createFrom(gdjs.Fase9Code.GDenemy1Objects1);


gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDenemy1Objects2.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDenemy1Objects2[i].getVariableNumber(gdjs.Fase9Code.GDenemy1Objects2[i].getVariables().getFromIndex(0)) < 0 ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDenemy1Objects2[k] = gdjs.Fase9Code.GDenemy1Objects2[i];
        ++k;
    }
}
gdjs.Fase9Code.GDenemy1Objects2.length = k;}if (gdjs.Fase9Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase9Code.GDenemy1Objects2 */
{for(var i = 0, len = gdjs.Fase9Code.GDenemy1Objects2.length ;i < len;++i) {
    gdjs.Fase9Code.GDenemy1Objects2[i].flipX(false);
}
}}

}


{

/* Reuse gdjs.Fase9Code.GDenemy1Objects1 */

gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase9Code.GDenemy1Objects1[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDenemy1Objects1[k] = gdjs.Fase9Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDenemy1Objects1.length = k;}if (gdjs.Fase9Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase9Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase9Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDenemy1Objects1[i].flipX(true);
}
}}

}


}; //End of gdjs.Fase9Code.eventsList0x8a8ea4
gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase9Code.GDplayerObjects1});gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase9Code.GDenemy1Objects1});gdjs.Fase9Code.eventsList0x5b6e18 = function(runtimeScene) {

{



}


{


gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
gdjs.Fase9Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase9Code.condition0IsTrue_0.val) {
gdjs.Fase9Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));
gdjs.Fase9Code.GDlblFaseObjects1.createFrom(runtimeScene.getObjects("lblFase"));
{for(var i = 0, len = gdjs.Fase9Code.GDlblFaseObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDlblFaseObjects1[i].setString("FASE " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{gdjs.evtTools.camera.showLayer(runtimeScene, "UI");
}{for(var i = 0, len = gdjs.Fase9Code.GDfade_95faseObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDfade_95faseObjects1[i].getBehavior("Tween").addObjectOpacityTween("fadeout", 0, "linear", 2000, false);
}
}}

}


{

gdjs.Fase9Code.GDwaterObjects1.createFrom(runtimeScene.getObjects("water"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
gdjs.Fase9Code.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDwaterObjects1Objects) > 0;
}if (gdjs.Fase9Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase9Code.GDwaterObjects1 */
{for(var i = 0, len = gdjs.Fase9Code.GDwaterObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDwaterObjects1[i].setXOffset(gdjs.Fase9Code.GDwaterObjects1[i].getXOffset() + (0.5));
}
}}

}


{

gdjs.Fase9Code.GDendObjects1.createFrom(runtimeScene.getObjects("end"));
gdjs.Fase9Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
gdjs.Fase9Code.condition1IsTrue_0.val = false;
{
gdjs.Fase9Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDplayerObjects1Objects, gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDendObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase9Code.condition0IsTrue_0.val ) {
{
gdjs.Fase9Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 0;
}}
if (gdjs.Fase9Code.condition1IsTrue_0.val) {
gdjs.Fase9Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}{for(var i = 0, len = gdjs.Fase9Code.GDfade_95faseObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDfade_95faseObjects1[i].getBehavior("Tween").addObjectOpacityTween("fadein", 255, "linear", 2000, false);
}
}}

}


{

gdjs.Fase9Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
gdjs.Fase9Code.condition1IsTrue_0.val = false;
{
gdjs.Fase9Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if ( gdjs.Fase9Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDfade_95faseObjects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDfade_95faseObjects1[i].getOpacity() >= 255 ) {
        gdjs.Fase9Code.condition1IsTrue_0.val = true;
        gdjs.Fase9Code.GDfade_95faseObjects1[k] = gdjs.Fase9Code.GDfade_95faseObjects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDfade_95faseObjects1.length = k;}}
if (gdjs.Fase9Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(2);
}}

}


{


gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
gdjs.Fase9Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 2;
}if (gdjs.Fase9Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


{



}


{


gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
gdjs.Fase9Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase9Code.condition0IsTrue_0.val) {
gdjs.Fase9Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.Fase9Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDplayerObjects1[i].hide();
}
}}

}


{



}


{


{
gdjs.Fase9Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase9Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.Fase9Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDdinoObjects1[i].setPosition((( gdjs.Fase9Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase9Code.GDplayerObjects1[0].getPointX("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase9Code.GDdinoObjects1[i].getVariables().getFromIndex(0))),(( gdjs.Fase9Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase9Code.GDplayerObjects1[0].getPointY("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase9Code.GDdinoObjects1[i].getVariables().getFromIndex(1))));
}
}{gdjs.evtTools.camera.centerCameraWithinLimits(runtimeScene, (gdjs.Fase9Code.GDdinoObjects1.length !== 0 ? gdjs.Fase9Code.GDdinoObjects1[0] : null), 0, 0, gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)), 768, true, "", 0);
}{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.Fase9Code.GDdinoObjects1.length === 0 ) ? 0 :gdjs.Fase9Code.GDdinoObjects1[0].getPointX("")) * 0.15, "Background", 0);
}}

}


{



}


{



}


{

gdjs.Fase9Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase9Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase9Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
gdjs.Fase9Code.condition1IsTrue_0.val = false;
gdjs.Fase9Code.condition2IsTrue_0.val = false;
gdjs.Fase9Code.condition3IsTrue_0.val = false;
gdjs.Fase9Code.condition4IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase9Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling()) ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDplayerObjects1[k] = gdjs.Fase9Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDplayerObjects1.length = k;}if ( gdjs.Fase9Code.condition0IsTrue_0.val ) {
{
gdjs.Fase9Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDenemy1Objects1Objects, gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDplayerObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase9Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase9Code.GDenemy1Objects1[i].getVariables().get("die")) == 0 ) {
        gdjs.Fase9Code.condition2IsTrue_0.val = true;
        gdjs.Fase9Code.GDenemy1Objects1[k] = gdjs.Fase9Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDenemy1Objects1.length = k;}if ( gdjs.Fase9Code.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDdinoObjects1[i].getVariableNumber(gdjs.Fase9Code.GDdinoObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase9Code.condition3IsTrue_0.val = true;
        gdjs.Fase9Code.GDdinoObjects1[k] = gdjs.Fase9Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDdinoObjects1.length = k;}if ( gdjs.Fase9Code.condition3IsTrue_0.val ) {
{
{gdjs.Fase9Code.conditionTrue_1 = gdjs.Fase9Code.condition4IsTrue_0;
gdjs.Fase9Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7037948);
}
}}
}
}
}
if (gdjs.Fase9Code.condition4IsTrue_0.val) {
/* Reuse gdjs.Fase9Code.GDdinoObjects1 */
/* Reuse gdjs.Fase9Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase9Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDplayerObjects1[i].getBehavior("PlatformerObject").ignoreDefaultControls(true);
}
}{for(var i = 0, len = gdjs.Fase9Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDdinoObjects1[i].returnVariable(gdjs.Fase9Code.GDdinoObjects1[i].getVariables().getFromIndex(2)).setNumber(1);
}
}{for(var i = 0, len = gdjs.Fase9Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDdinoObjects1[i].setAnimationName("die");
}
}}

}


{



}


{

gdjs.Fase9Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
gdjs.Fase9Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDdinoObjects1[i].isCurrentAnimationName("die") ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDdinoObjects1[k] = gdjs.Fase9Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDdinoObjects1.length = k;}if ( gdjs.Fase9Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDdinoObjects1[i].hasAnimationEnded() ) {
        gdjs.Fase9Code.condition1IsTrue_0.val = true;
        gdjs.Fase9Code.GDdinoObjects1[k] = gdjs.Fase9Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDdinoObjects1.length = k;}}
if (gdjs.Fase9Code.condition1IsTrue_0.val) {
{gdjs.evtsExt__Util__setFase.func(runtimeScene, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.Fase9Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDdinoObjects1[i].getY() > gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDdinoObjects1[k] = gdjs.Fase9Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDdinoObjects1.length = k;}if (gdjs.Fase9Code.condition0IsTrue_0.val) {
{gdjs.evtsExt__Util__setFase.func(runtimeScene, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Fase9Code.eventsList0x7ed714(runtimeScene);
}


{


gdjs.Fase9Code.eventsList0x8b46b4(runtimeScene);
}


{



}


{


gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
gdjs.Fase9Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase9Code.condition0IsTrue_0.val) {
gdjs.Fase9Code.GDflipEnemyObjects1.createFrom(runtimeScene.getObjects("flipEnemy"));
{for(var i = 0, len = gdjs.Fase9Code.GDflipEnemyObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDflipEnemyObjects1[i].hide();
}
}}

}


{



}


{


{
gdjs.Fase9Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
{for(var i = 0, len = gdjs.Fase9Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDenemy1Objects1[i].setX(gdjs.Fase9Code.GDenemy1Objects1[i].getX() + ((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase9Code.GDenemy1Objects1[i].getVariables().getFromIndex(0))) * gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{



}


{

gdjs.Fase9Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase9Code.GDflipEnemyObjects1.createFrom(runtimeScene.getObjects("flipEnemy"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
gdjs.Fase9Code.condition1IsTrue_0.val = false;
{
gdjs.Fase9Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDenemy1Objects1Objects, gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDflipEnemyObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase9Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase9Code.conditionTrue_1 = gdjs.Fase9Code.condition1IsTrue_0;
gdjs.Fase9Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(9624828);
}
}}
if (gdjs.Fase9Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase9Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase9Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDenemy1Objects1[i].returnVariable(gdjs.Fase9Code.GDenemy1Objects1[i].getVariables().getFromIndex(0)).mul(-(1));
}
}
{ //Subevents
gdjs.Fase9Code.eventsList0x8a8ea4(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.Fase9Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase9Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
gdjs.Fase9Code.condition1IsTrue_0.val = false;
gdjs.Fase9Code.condition2IsTrue_0.val = false;
gdjs.Fase9Code.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDplayerObjects1[k] = gdjs.Fase9Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDplayerObjects1.length = k;}if ( gdjs.Fase9Code.condition0IsTrue_0.val ) {
{
gdjs.Fase9Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDplayerObjects1Objects, gdjs.Fase9Code.mapOfGDgdjs_46Fase9Code_46GDenemy1Objects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase9Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase9Code.GDenemy1Objects1[i].getVariables().get("die")) == 0 ) {
        gdjs.Fase9Code.condition2IsTrue_0.val = true;
        gdjs.Fase9Code.GDenemy1Objects1[k] = gdjs.Fase9Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDenemy1Objects1.length = k;}if ( gdjs.Fase9Code.condition2IsTrue_0.val ) {
{
{gdjs.Fase9Code.conditionTrue_1 = gdjs.Fase9Code.condition3IsTrue_0;
gdjs.Fase9Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(9584428);
}
}}
}
}
if (gdjs.Fase9Code.condition3IsTrue_0.val) {
/* Reuse gdjs.Fase9Code.GDenemy1Objects1 */
/* Reuse gdjs.Fase9Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase9Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDplayerObjects1[i].getBehavior("PlatformerObject").setCanJump();
}
}{for(var i = 0, len = gdjs.Fase9Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDplayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}{for(var i = 0, len = gdjs.Fase9Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDenemy1Objects1[i].returnVariable(gdjs.Fase9Code.GDenemy1Objects1[i].getVariables().get("die")).setNumber(1);
}
}{for(var i = 0, len = gdjs.Fase9Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDenemy1Objects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.Fase9Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDenemy1Objects1[i].getBehavior("Tween").addObjectOpacityTween("enemy1_die", 0, "linear", 1000, false);
}
}}

}


{

gdjs.Fase9Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));

gdjs.Fase9Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase9Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase9Code.GDenemy1Objects1[i].getOpacity() <= 0 ) {
        gdjs.Fase9Code.condition0IsTrue_0.val = true;
        gdjs.Fase9Code.GDenemy1Objects1[k] = gdjs.Fase9Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase9Code.GDenemy1Objects1.length = k;}if (gdjs.Fase9Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase9Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase9Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase9Code.GDenemy1Objects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.Fase9Code.eventsList0x5b6e18


gdjs.Fase9Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Fase9Code.GDdinoObjects1.length = 0;
gdjs.Fase9Code.GDdinoObjects2.length = 0;
gdjs.Fase9Code.GDdinoObjects3.length = 0;
gdjs.Fase9Code.GDplayerObjects1.length = 0;
gdjs.Fase9Code.GDplayerObjects2.length = 0;
gdjs.Fase9Code.GDplayerObjects3.length = 0;
gdjs.Fase9Code.GDground1Objects1.length = 0;
gdjs.Fase9Code.GDground1Objects2.length = 0;
gdjs.Fase9Code.GDground1Objects3.length = 0;
gdjs.Fase9Code.GDwaterObjects1.length = 0;
gdjs.Fase9Code.GDwaterObjects2.length = 0;
gdjs.Fase9Code.GDwaterObjects3.length = 0;
gdjs.Fase9Code.GDfade_95faseObjects1.length = 0;
gdjs.Fase9Code.GDfade_95faseObjects2.length = 0;
gdjs.Fase9Code.GDfade_95faseObjects3.length = 0;
gdjs.Fase9Code.GDlblFaseObjects1.length = 0;
gdjs.Fase9Code.GDlblFaseObjects2.length = 0;
gdjs.Fase9Code.GDlblFaseObjects3.length = 0;
gdjs.Fase9Code.GDbg1Objects1.length = 0;
gdjs.Fase9Code.GDbg1Objects2.length = 0;
gdjs.Fase9Code.GDbg1Objects3.length = 0;
gdjs.Fase9Code.GDboxObjects1.length = 0;
gdjs.Fase9Code.GDboxObjects2.length = 0;
gdjs.Fase9Code.GDboxObjects3.length = 0;
gdjs.Fase9Code.GDplacaObjects1.length = 0;
gdjs.Fase9Code.GDplacaObjects2.length = 0;
gdjs.Fase9Code.GDplacaObjects3.length = 0;
gdjs.Fase9Code.GDthreeObjects1.length = 0;
gdjs.Fase9Code.GDthreeObjects2.length = 0;
gdjs.Fase9Code.GDthreeObjects3.length = 0;
gdjs.Fase9Code.GDenemy1Objects1.length = 0;
gdjs.Fase9Code.GDenemy1Objects2.length = 0;
gdjs.Fase9Code.GDenemy1Objects3.length = 0;
gdjs.Fase9Code.GDflipEnemyObjects1.length = 0;
gdjs.Fase9Code.GDflipEnemyObjects2.length = 0;
gdjs.Fase9Code.GDflipEnemyObjects3.length = 0;
gdjs.Fase9Code.GDground1bordasObjects1.length = 0;
gdjs.Fase9Code.GDground1bordasObjects2.length = 0;
gdjs.Fase9Code.GDground1bordasObjects3.length = 0;
gdjs.Fase9Code.GDbigGroundObjects1.length = 0;
gdjs.Fase9Code.GDbigGroundObjects2.length = 0;
gdjs.Fase9Code.GDbigGroundObjects3.length = 0;
gdjs.Fase9Code.GDplataforma1Objects1.length = 0;
gdjs.Fase9Code.GDplataforma1Objects2.length = 0;
gdjs.Fase9Code.GDplataforma1Objects3.length = 0;
gdjs.Fase9Code.GDendObjects1.length = 0;
gdjs.Fase9Code.GDendObjects2.length = 0;
gdjs.Fase9Code.GDendObjects3.length = 0;

gdjs.Fase9Code.eventsList0x5b6e18(runtimeScene);
return;

}

gdjs['Fase9Code'] = gdjs.Fase9Code;
