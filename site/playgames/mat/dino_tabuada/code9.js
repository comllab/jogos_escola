gdjs.Fase8Code = {};
gdjs.Fase8Code.GDdinoObjects1= [];
gdjs.Fase8Code.GDdinoObjects2= [];
gdjs.Fase8Code.GDdinoObjects3= [];
gdjs.Fase8Code.GDplayerObjects1= [];
gdjs.Fase8Code.GDplayerObjects2= [];
gdjs.Fase8Code.GDplayerObjects3= [];
gdjs.Fase8Code.GDground1Objects1= [];
gdjs.Fase8Code.GDground1Objects2= [];
gdjs.Fase8Code.GDground1Objects3= [];
gdjs.Fase8Code.GDwaterObjects1= [];
gdjs.Fase8Code.GDwaterObjects2= [];
gdjs.Fase8Code.GDwaterObjects3= [];
gdjs.Fase8Code.GDfade_95faseObjects1= [];
gdjs.Fase8Code.GDfade_95faseObjects2= [];
gdjs.Fase8Code.GDfade_95faseObjects3= [];
gdjs.Fase8Code.GDlblFaseObjects1= [];
gdjs.Fase8Code.GDlblFaseObjects2= [];
gdjs.Fase8Code.GDlblFaseObjects3= [];
gdjs.Fase8Code.GDbg1Objects1= [];
gdjs.Fase8Code.GDbg1Objects2= [];
gdjs.Fase8Code.GDbg1Objects3= [];
gdjs.Fase8Code.GDboxObjects1= [];
gdjs.Fase8Code.GDboxObjects2= [];
gdjs.Fase8Code.GDboxObjects3= [];
gdjs.Fase8Code.GDplacaObjects1= [];
gdjs.Fase8Code.GDplacaObjects2= [];
gdjs.Fase8Code.GDplacaObjects3= [];
gdjs.Fase8Code.GDthreeObjects1= [];
gdjs.Fase8Code.GDthreeObjects2= [];
gdjs.Fase8Code.GDthreeObjects3= [];
gdjs.Fase8Code.GDenemy2Objects1= [];
gdjs.Fase8Code.GDenemy2Objects2= [];
gdjs.Fase8Code.GDenemy2Objects3= [];
gdjs.Fase8Code.GDenemy1Objects1= [];
gdjs.Fase8Code.GDenemy1Objects2= [];
gdjs.Fase8Code.GDenemy1Objects3= [];
gdjs.Fase8Code.GDflipEnemyObjects1= [];
gdjs.Fase8Code.GDflipEnemyObjects2= [];
gdjs.Fase8Code.GDflipEnemyObjects3= [];
gdjs.Fase8Code.GDground1bordasObjects1= [];
gdjs.Fase8Code.GDground1bordasObjects2= [];
gdjs.Fase8Code.GDground1bordasObjects3= [];
gdjs.Fase8Code.GDbigGroundObjects1= [];
gdjs.Fase8Code.GDbigGroundObjects2= [];
gdjs.Fase8Code.GDbigGroundObjects3= [];
gdjs.Fase8Code.GDplataforma1Objects1= [];
gdjs.Fase8Code.GDplataforma1Objects2= [];
gdjs.Fase8Code.GDplataforma1Objects3= [];
gdjs.Fase8Code.GDendObjects1= [];
gdjs.Fase8Code.GDendObjects2= [];
gdjs.Fase8Code.GDendObjects3= [];

gdjs.Fase8Code.conditionTrue_0 = {val:false};
gdjs.Fase8Code.condition0IsTrue_0 = {val:false};
gdjs.Fase8Code.condition1IsTrue_0 = {val:false};
gdjs.Fase8Code.condition2IsTrue_0 = {val:false};
gdjs.Fase8Code.condition3IsTrue_0 = {val:false};
gdjs.Fase8Code.condition4IsTrue_0 = {val:false};
gdjs.Fase8Code.condition5IsTrue_0 = {val:false};
gdjs.Fase8Code.conditionTrue_1 = {val:false};
gdjs.Fase8Code.condition0IsTrue_1 = {val:false};
gdjs.Fase8Code.condition1IsTrue_1 = {val:false};
gdjs.Fase8Code.condition2IsTrue_1 = {val:false};
gdjs.Fase8Code.condition3IsTrue_1 = {val:false};
gdjs.Fase8Code.condition4IsTrue_1 = {val:false};
gdjs.Fase8Code.condition5IsTrue_1 = {val:false};


gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDwaterObjects1Objects = Hashtable.newFrom({"water": gdjs.Fase8Code.GDwaterObjects1});gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase8Code.GDplayerObjects1});gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDendObjects1Objects = Hashtable.newFrom({"end": gdjs.Fase8Code.GDendObjects1});gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase8Code.GDenemy1Objects1});gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase8Code.GDplayerObjects1});gdjs.Fase8Code.eventsList0x80fde4 = function(runtimeScene) {

{

gdjs.Fase8Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase8Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
gdjs.Fase8Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase8Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDdinoObjects2[k] = gdjs.Fase8Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase8Code.GDdinoObjects2.length = k;}if ( gdjs.Fase8Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.Fase8Code.condition1IsTrue_0.val = true;
        gdjs.Fase8Code.GDplayerObjects2[k] = gdjs.Fase8Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase8Code.GDplayerObjects2.length = k;}}
if (gdjs.Fase8Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase8Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects2[i].setAnimationName("jump");
}
}}

}


{

gdjs.Fase8Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase8Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
gdjs.Fase8Code.condition1IsTrue_0.val = false;
gdjs.Fase8Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase8Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDdinoObjects2[k] = gdjs.Fase8Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase8Code.GDdinoObjects2.length = k;}if ( gdjs.Fase8Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving() ) {
        gdjs.Fase8Code.condition1IsTrue_0.val = true;
        gdjs.Fase8Code.GDplayerObjects2[k] = gdjs.Fase8Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase8Code.GDplayerObjects2.length = k;}if ( gdjs.Fase8Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase8Code.condition2IsTrue_0.val = true;
        gdjs.Fase8Code.GDplayerObjects2[k] = gdjs.Fase8Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase8Code.GDplayerObjects2.length = k;}}
}
if (gdjs.Fase8Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Fase8Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects2[i].setAnimationName("run");
}
}}

}


{

gdjs.Fase8Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase8Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
gdjs.Fase8Code.condition1IsTrue_0.val = false;
gdjs.Fase8Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase8Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDdinoObjects2[k] = gdjs.Fase8Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase8Code.GDdinoObjects2.length = k;}if ( gdjs.Fase8Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDplayerObjects2.length;i<l;++i) {
    if ( !(gdjs.Fase8Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Fase8Code.condition1IsTrue_0.val = true;
        gdjs.Fase8Code.GDplayerObjects2[k] = gdjs.Fase8Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase8Code.GDplayerObjects2.length = k;}if ( gdjs.Fase8Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase8Code.condition2IsTrue_0.val = true;
        gdjs.Fase8Code.GDplayerObjects2[k] = gdjs.Fase8Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase8Code.GDplayerObjects2.length = k;}}
}
if (gdjs.Fase8Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Fase8Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects2[i].setAnimationName("idle");
}
}}

}


{

gdjs.Fase8Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
gdjs.Fase8Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase8Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isJumping()) ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDplayerObjects1[k] = gdjs.Fase8Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDplayerObjects1.length = k;}if ( gdjs.Fase8Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase8Code.condition1IsTrue_0.val = true;
        gdjs.Fase8Code.GDplayerObjects1[k] = gdjs.Fase8Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase8Code.condition1IsTrue_0.val) {
gdjs.Fase8Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects1[i].setAnimationName("falling");
}
}}

}


}; //End of gdjs.Fase8Code.eventsList0x80fde4
gdjs.Fase8Code.eventsList0x973b3c = function(runtimeScene) {

{


gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
gdjs.Fase8Code.GDdinoObjects2.createFrom(gdjs.Fase8Code.GDdinoObjects1);

{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects2[i].flipX(true);
}
}}

}


{


gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
gdjs.Fase8Code.GDdinoObjects2.createFrom(gdjs.Fase8Code.GDdinoObjects1);

{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects2[i].flipX(false);
}
}}

}


{

gdjs.Fase8Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
gdjs.Fase8Code.condition1IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.Fase8Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase8Code.condition1IsTrue_0.val = true;
        gdjs.Fase8Code.GDplayerObjects1[k] = gdjs.Fase8Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase8Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase8Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase8Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDplayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


}; //End of gdjs.Fase8Code.eventsList0x973b3c
gdjs.Fase8Code.eventsList0x812de4 = function(runtimeScene) {

{

gdjs.Fase8Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDdinoObjects1[i].getVariableNumber(gdjs.Fase8Code.GDdinoObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDdinoObjects1[k] = gdjs.Fase8Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDdinoObjects1.length = k;}if (gdjs.Fase8Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.Fase8Code.eventsList0x973b3c(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.Fase8Code.eventsList0x812de4
gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase8Code.GDenemy1Objects1});gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDflipEnemyObjects1Objects = Hashtable.newFrom({"flipEnemy": gdjs.Fase8Code.GDflipEnemyObjects1});gdjs.Fase8Code.eventsList0x882e54 = function(runtimeScene) {

{

gdjs.Fase8Code.GDenemy1Objects2.createFrom(gdjs.Fase8Code.GDenemy1Objects1);


gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDenemy1Objects2.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDenemy1Objects2[i].getVariableNumber(gdjs.Fase8Code.GDenemy1Objects2[i].getVariables().getFromIndex(0)) < 0 ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDenemy1Objects2[k] = gdjs.Fase8Code.GDenemy1Objects2[i];
        ++k;
    }
}
gdjs.Fase8Code.GDenemy1Objects2.length = k;}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase8Code.GDenemy1Objects2 */
{for(var i = 0, len = gdjs.Fase8Code.GDenemy1Objects2.length ;i < len;++i) {
    gdjs.Fase8Code.GDenemy1Objects2[i].flipX(false);
}
}}

}


{

/* Reuse gdjs.Fase8Code.GDenemy1Objects1 */

gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase8Code.GDenemy1Objects1[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDenemy1Objects1[k] = gdjs.Fase8Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDenemy1Objects1.length = k;}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase8Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase8Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDenemy1Objects1[i].flipX(true);
}
}}

}


}; //End of gdjs.Fase8Code.eventsList0x882e54
gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase8Code.GDplayerObjects1});gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase8Code.GDenemy1Objects1});gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDenemy2Objects1Objects = Hashtable.newFrom({"enemy2": gdjs.Fase8Code.GDenemy2Objects1});gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase8Code.GDplayerObjects1});gdjs.Fase8Code.eventsList0x5b6e18 = function(runtimeScene) {

{



}


{


gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
gdjs.Fase8Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));
gdjs.Fase8Code.GDlblFaseObjects1.createFrom(runtimeScene.getObjects("lblFase"));
{for(var i = 0, len = gdjs.Fase8Code.GDlblFaseObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDlblFaseObjects1[i].setString("FASE " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{gdjs.evtTools.camera.showLayer(runtimeScene, "UI");
}{for(var i = 0, len = gdjs.Fase8Code.GDfade_95faseObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDfade_95faseObjects1[i].getBehavior("Tween").addObjectOpacityTween("fadeout", 0, "linear", 2000, false);
}
}}

}


{

gdjs.Fase8Code.GDwaterObjects1.createFrom(runtimeScene.getObjects("water"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDwaterObjects1Objects) > 0;
}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase8Code.GDwaterObjects1 */
{for(var i = 0, len = gdjs.Fase8Code.GDwaterObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDwaterObjects1[i].setXOffset(gdjs.Fase8Code.GDwaterObjects1[i].getXOffset() + (0.5));
}
}}

}


{

gdjs.Fase8Code.GDendObjects1.createFrom(runtimeScene.getObjects("end"));
gdjs.Fase8Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
gdjs.Fase8Code.condition1IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDplayerObjects1Objects, gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDendObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase8Code.condition0IsTrue_0.val ) {
{
gdjs.Fase8Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 0;
}}
if (gdjs.Fase8Code.condition1IsTrue_0.val) {
gdjs.Fase8Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}{for(var i = 0, len = gdjs.Fase8Code.GDfade_95faseObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDfade_95faseObjects1[i].getBehavior("Tween").addObjectOpacityTween("fadein", 255, "linear", 2000, false);
}
}}

}


{

gdjs.Fase8Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
gdjs.Fase8Code.condition1IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if ( gdjs.Fase8Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDfade_95faseObjects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDfade_95faseObjects1[i].getOpacity() >= 255 ) {
        gdjs.Fase8Code.condition1IsTrue_0.val = true;
        gdjs.Fase8Code.GDfade_95faseObjects1[k] = gdjs.Fase8Code.GDfade_95faseObjects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDfade_95faseObjects1.length = k;}}
if (gdjs.Fase8Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(2);
}}

}


{


gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 2;
}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


{


{
gdjs.Fase8Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase8Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects1[i].setPosition((( gdjs.Fase8Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase8Code.GDplayerObjects1[0].getPointX("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase8Code.GDdinoObjects1[i].getVariables().getFromIndex(0))),(( gdjs.Fase8Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase8Code.GDplayerObjects1[0].getPointY("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase8Code.GDdinoObjects1[i].getVariables().getFromIndex(1))));
}
}{gdjs.evtTools.camera.centerCameraWithinLimits(runtimeScene, (gdjs.Fase8Code.GDdinoObjects1.length !== 0 ? gdjs.Fase8Code.GDdinoObjects1[0] : null), 0, 0, gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)), 768, true, "enemys", 0);
}{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.Fase8Code.GDdinoObjects1.length === 0 ) ? 0 :gdjs.Fase8Code.GDdinoObjects1[0].getPointX("")) * 0.15, "Background", 0);
}}

}


{



}


{


gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
gdjs.Fase8Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.Fase8Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDplayerObjects1[i].hide();
}
}}

}


{



}


{


{
gdjs.Fase8Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase8Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects1[i].setPosition((( gdjs.Fase8Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase8Code.GDplayerObjects1[0].getPointX("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase8Code.GDdinoObjects1[i].getVariables().getFromIndex(0))),(( gdjs.Fase8Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase8Code.GDplayerObjects1[0].getPointY("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase8Code.GDdinoObjects1[i].getVariables().getFromIndex(1))));
}
}{gdjs.evtTools.camera.centerCameraWithinLimits(runtimeScene, (gdjs.Fase8Code.GDdinoObjects1.length !== 0 ? gdjs.Fase8Code.GDdinoObjects1[0] : null), 0, 0, gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)), 768, true, "", 0);
}{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.Fase8Code.GDdinoObjects1.length === 0 ) ? 0 :gdjs.Fase8Code.GDdinoObjects1[0].getPointX("")) * 0.15, "Background", 0);
}}

}


{



}


{



}


{

gdjs.Fase8Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase8Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase8Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
gdjs.Fase8Code.condition1IsTrue_0.val = false;
gdjs.Fase8Code.condition2IsTrue_0.val = false;
gdjs.Fase8Code.condition3IsTrue_0.val = false;
gdjs.Fase8Code.condition4IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase8Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling()) ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDplayerObjects1[k] = gdjs.Fase8Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDplayerObjects1.length = k;}if ( gdjs.Fase8Code.condition0IsTrue_0.val ) {
{
gdjs.Fase8Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDenemy1Objects1Objects, gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDplayerObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase8Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase8Code.GDenemy1Objects1[i].getVariables().get("die")) == 0 ) {
        gdjs.Fase8Code.condition2IsTrue_0.val = true;
        gdjs.Fase8Code.GDenemy1Objects1[k] = gdjs.Fase8Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDenemy1Objects1.length = k;}if ( gdjs.Fase8Code.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDdinoObjects1[i].getVariableNumber(gdjs.Fase8Code.GDdinoObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase8Code.condition3IsTrue_0.val = true;
        gdjs.Fase8Code.GDdinoObjects1[k] = gdjs.Fase8Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDdinoObjects1.length = k;}if ( gdjs.Fase8Code.condition3IsTrue_0.val ) {
{
{gdjs.Fase8Code.conditionTrue_1 = gdjs.Fase8Code.condition4IsTrue_0;
gdjs.Fase8Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8399180);
}
}}
}
}
}
if (gdjs.Fase8Code.condition4IsTrue_0.val) {
/* Reuse gdjs.Fase8Code.GDdinoObjects1 */
/* Reuse gdjs.Fase8Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase8Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDplayerObjects1[i].getBehavior("PlatformerObject").ignoreDefaultControls(true);
}
}{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects1[i].returnVariable(gdjs.Fase8Code.GDdinoObjects1[i].getVariables().getFromIndex(2)).setNumber(1);
}
}{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects1[i].setAnimationName("die");
}
}}

}


{



}


{

gdjs.Fase8Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
gdjs.Fase8Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDdinoObjects1[i].isCurrentAnimationName("die") ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDdinoObjects1[k] = gdjs.Fase8Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDdinoObjects1.length = k;}if ( gdjs.Fase8Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDdinoObjects1[i].hasAnimationEnded() ) {
        gdjs.Fase8Code.condition1IsTrue_0.val = true;
        gdjs.Fase8Code.GDdinoObjects1[k] = gdjs.Fase8Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDdinoObjects1.length = k;}}
if (gdjs.Fase8Code.condition1IsTrue_0.val) {
{gdjs.evtsExt__Util__setFase.func(runtimeScene, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.Fase8Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDdinoObjects1[i].getY() > gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDdinoObjects1[k] = gdjs.Fase8Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDdinoObjects1.length = k;}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
{gdjs.evtsExt__Util__setFase.func(runtimeScene, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Fase8Code.eventsList0x80fde4(runtimeScene);
}


{


gdjs.Fase8Code.eventsList0x812de4(runtimeScene);
}


{



}


{


gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
gdjs.Fase8Code.GDflipEnemyObjects1.createFrom(runtimeScene.getObjects("flipEnemy"));
{for(var i = 0, len = gdjs.Fase8Code.GDflipEnemyObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDflipEnemyObjects1[i].hide();
}
}}

}


{



}


{


{
gdjs.Fase8Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
{for(var i = 0, len = gdjs.Fase8Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDenemy1Objects1[i].setX(gdjs.Fase8Code.GDenemy1Objects1[i].getX() + ((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase8Code.GDenemy1Objects1[i].getVariables().getFromIndex(0))) * gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{



}


{

gdjs.Fase8Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase8Code.GDflipEnemyObjects1.createFrom(runtimeScene.getObjects("flipEnemy"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
gdjs.Fase8Code.condition1IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDenemy1Objects1Objects, gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDflipEnemyObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase8Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase8Code.conditionTrue_1 = gdjs.Fase8Code.condition1IsTrue_0;
gdjs.Fase8Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8404668);
}
}}
if (gdjs.Fase8Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase8Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase8Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDenemy1Objects1[i].returnVariable(gdjs.Fase8Code.GDenemy1Objects1[i].getVariables().getFromIndex(0)).mul(-(1));
}
}
{ //Subevents
gdjs.Fase8Code.eventsList0x882e54(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.Fase8Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase8Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
gdjs.Fase8Code.condition1IsTrue_0.val = false;
gdjs.Fase8Code.condition2IsTrue_0.val = false;
gdjs.Fase8Code.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDplayerObjects1[k] = gdjs.Fase8Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDplayerObjects1.length = k;}if ( gdjs.Fase8Code.condition0IsTrue_0.val ) {
{
gdjs.Fase8Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDplayerObjects1Objects, gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDenemy1Objects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase8Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase8Code.GDenemy1Objects1[i].getVariables().get("die")) == 0 ) {
        gdjs.Fase8Code.condition2IsTrue_0.val = true;
        gdjs.Fase8Code.GDenemy1Objects1[k] = gdjs.Fase8Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDenemy1Objects1.length = k;}if ( gdjs.Fase8Code.condition2IsTrue_0.val ) {
{
{gdjs.Fase8Code.conditionTrue_1 = gdjs.Fase8Code.condition3IsTrue_0;
gdjs.Fase8Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8396060);
}
}}
}
}
if (gdjs.Fase8Code.condition3IsTrue_0.val) {
/* Reuse gdjs.Fase8Code.GDenemy1Objects1 */
/* Reuse gdjs.Fase8Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase8Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDplayerObjects1[i].getBehavior("PlatformerObject").setCanJump();
}
}{for(var i = 0, len = gdjs.Fase8Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDplayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}{for(var i = 0, len = gdjs.Fase8Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDenemy1Objects1[i].returnVariable(gdjs.Fase8Code.GDenemy1Objects1[i].getVariables().get("die")).setNumber(1);
}
}{for(var i = 0, len = gdjs.Fase8Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDenemy1Objects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.Fase8Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDenemy1Objects1[i].getBehavior("Tween").addObjectOpacityTween("enemy1_die", 0, "linear", 1000, false);
}
}}

}


{

gdjs.Fase8Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase8Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase8Code.GDenemy1Objects1[i].getOpacity() <= 0 ) {
        gdjs.Fase8Code.condition0IsTrue_0.val = true;
        gdjs.Fase8Code.GDenemy1Objects1[k] = gdjs.Fase8Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase8Code.GDenemy1Objects1.length = k;}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase8Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase8Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDenemy1Objects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.Fase8Code.GDenemy2Objects1.createFrom(runtimeScene.getObjects("enemy2"));
gdjs.Fase8Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase8Code.condition0IsTrue_0.val = false;
{
gdjs.Fase8Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDenemy2Objects1Objects, gdjs.Fase8Code.mapOfGDgdjs_46Fase8Code_46GDplayerObjects1Objects, false, runtimeScene, false);
}if (gdjs.Fase8Code.condition0IsTrue_0.val) {
gdjs.Fase8Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
/* Reuse gdjs.Fase8Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase8Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDplayerObjects1[i].getBehavior("PlatformerObject").ignoreDefaultControls(true);
}
}{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects1[i].returnVariable(gdjs.Fase8Code.GDdinoObjects1[i].getVariables().getFromIndex(2)).setNumber(1);
}
}{for(var i = 0, len = gdjs.Fase8Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase8Code.GDdinoObjects1[i].setAnimationName("die");
}
}}

}


}; //End of gdjs.Fase8Code.eventsList0x5b6e18


gdjs.Fase8Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Fase8Code.GDdinoObjects1.length = 0;
gdjs.Fase8Code.GDdinoObjects2.length = 0;
gdjs.Fase8Code.GDdinoObjects3.length = 0;
gdjs.Fase8Code.GDplayerObjects1.length = 0;
gdjs.Fase8Code.GDplayerObjects2.length = 0;
gdjs.Fase8Code.GDplayerObjects3.length = 0;
gdjs.Fase8Code.GDground1Objects1.length = 0;
gdjs.Fase8Code.GDground1Objects2.length = 0;
gdjs.Fase8Code.GDground1Objects3.length = 0;
gdjs.Fase8Code.GDwaterObjects1.length = 0;
gdjs.Fase8Code.GDwaterObjects2.length = 0;
gdjs.Fase8Code.GDwaterObjects3.length = 0;
gdjs.Fase8Code.GDfade_95faseObjects1.length = 0;
gdjs.Fase8Code.GDfade_95faseObjects2.length = 0;
gdjs.Fase8Code.GDfade_95faseObjects3.length = 0;
gdjs.Fase8Code.GDlblFaseObjects1.length = 0;
gdjs.Fase8Code.GDlblFaseObjects2.length = 0;
gdjs.Fase8Code.GDlblFaseObjects3.length = 0;
gdjs.Fase8Code.GDbg1Objects1.length = 0;
gdjs.Fase8Code.GDbg1Objects2.length = 0;
gdjs.Fase8Code.GDbg1Objects3.length = 0;
gdjs.Fase8Code.GDboxObjects1.length = 0;
gdjs.Fase8Code.GDboxObjects2.length = 0;
gdjs.Fase8Code.GDboxObjects3.length = 0;
gdjs.Fase8Code.GDplacaObjects1.length = 0;
gdjs.Fase8Code.GDplacaObjects2.length = 0;
gdjs.Fase8Code.GDplacaObjects3.length = 0;
gdjs.Fase8Code.GDthreeObjects1.length = 0;
gdjs.Fase8Code.GDthreeObjects2.length = 0;
gdjs.Fase8Code.GDthreeObjects3.length = 0;
gdjs.Fase8Code.GDenemy2Objects1.length = 0;
gdjs.Fase8Code.GDenemy2Objects2.length = 0;
gdjs.Fase8Code.GDenemy2Objects3.length = 0;
gdjs.Fase8Code.GDenemy1Objects1.length = 0;
gdjs.Fase8Code.GDenemy1Objects2.length = 0;
gdjs.Fase8Code.GDenemy1Objects3.length = 0;
gdjs.Fase8Code.GDflipEnemyObjects1.length = 0;
gdjs.Fase8Code.GDflipEnemyObjects2.length = 0;
gdjs.Fase8Code.GDflipEnemyObjects3.length = 0;
gdjs.Fase8Code.GDground1bordasObjects1.length = 0;
gdjs.Fase8Code.GDground1bordasObjects2.length = 0;
gdjs.Fase8Code.GDground1bordasObjects3.length = 0;
gdjs.Fase8Code.GDbigGroundObjects1.length = 0;
gdjs.Fase8Code.GDbigGroundObjects2.length = 0;
gdjs.Fase8Code.GDbigGroundObjects3.length = 0;
gdjs.Fase8Code.GDplataforma1Objects1.length = 0;
gdjs.Fase8Code.GDplataforma1Objects2.length = 0;
gdjs.Fase8Code.GDplataforma1Objects3.length = 0;
gdjs.Fase8Code.GDendObjects1.length = 0;
gdjs.Fase8Code.GDendObjects2.length = 0;
gdjs.Fase8Code.GDendObjects3.length = 0;

gdjs.Fase8Code.eventsList0x5b6e18(runtimeScene);
return;

}

gdjs['Fase8Code'] = gdjs.Fase8Code;
