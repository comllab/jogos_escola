gdjs.Fase1Code = {};
gdjs.Fase1Code.GDdinoObjects1= [];
gdjs.Fase1Code.GDdinoObjects2= [];
gdjs.Fase1Code.GDdinoObjects3= [];
gdjs.Fase1Code.GDplayerObjects1= [];
gdjs.Fase1Code.GDplayerObjects2= [];
gdjs.Fase1Code.GDplayerObjects3= [];
gdjs.Fase1Code.GDground1Objects1= [];
gdjs.Fase1Code.GDground1Objects2= [];
gdjs.Fase1Code.GDground1Objects3= [];
gdjs.Fase1Code.GDwaterObjects1= [];
gdjs.Fase1Code.GDwaterObjects2= [];
gdjs.Fase1Code.GDwaterObjects3= [];
gdjs.Fase1Code.GDfade_95faseObjects1= [];
gdjs.Fase1Code.GDfade_95faseObjects2= [];
gdjs.Fase1Code.GDfade_95faseObjects3= [];
gdjs.Fase1Code.GDlblFaseObjects1= [];
gdjs.Fase1Code.GDlblFaseObjects2= [];
gdjs.Fase1Code.GDlblFaseObjects3= [];
gdjs.Fase1Code.GDbg1Objects1= [];
gdjs.Fase1Code.GDbg1Objects2= [];
gdjs.Fase1Code.GDbg1Objects3= [];
gdjs.Fase1Code.GDboxObjects1= [];
gdjs.Fase1Code.GDboxObjects2= [];
gdjs.Fase1Code.GDboxObjects3= [];
gdjs.Fase1Code.GDplacaObjects1= [];
gdjs.Fase1Code.GDplacaObjects2= [];
gdjs.Fase1Code.GDplacaObjects3= [];
gdjs.Fase1Code.GDthreeObjects1= [];
gdjs.Fase1Code.GDthreeObjects2= [];
gdjs.Fase1Code.GDthreeObjects3= [];
gdjs.Fase1Code.GDenemy1Objects1= [];
gdjs.Fase1Code.GDenemy1Objects2= [];
gdjs.Fase1Code.GDenemy1Objects3= [];
gdjs.Fase1Code.GDflipEnemyObjects1= [];
gdjs.Fase1Code.GDflipEnemyObjects2= [];
gdjs.Fase1Code.GDflipEnemyObjects3= [];
gdjs.Fase1Code.GDground1bordasObjects1= [];
gdjs.Fase1Code.GDground1bordasObjects2= [];
gdjs.Fase1Code.GDground1bordasObjects3= [];
gdjs.Fase1Code.GDbigGroundObjects1= [];
gdjs.Fase1Code.GDbigGroundObjects2= [];
gdjs.Fase1Code.GDbigGroundObjects3= [];
gdjs.Fase1Code.GDplataforma1Objects1= [];
gdjs.Fase1Code.GDplataforma1Objects2= [];
gdjs.Fase1Code.GDplataforma1Objects3= [];
gdjs.Fase1Code.GDendObjects1= [];
gdjs.Fase1Code.GDendObjects2= [];
gdjs.Fase1Code.GDendObjects3= [];

gdjs.Fase1Code.conditionTrue_0 = {val:false};
gdjs.Fase1Code.condition0IsTrue_0 = {val:false};
gdjs.Fase1Code.condition1IsTrue_0 = {val:false};
gdjs.Fase1Code.condition2IsTrue_0 = {val:false};
gdjs.Fase1Code.condition3IsTrue_0 = {val:false};
gdjs.Fase1Code.condition4IsTrue_0 = {val:false};
gdjs.Fase1Code.condition5IsTrue_0 = {val:false};
gdjs.Fase1Code.conditionTrue_1 = {val:false};
gdjs.Fase1Code.condition0IsTrue_1 = {val:false};
gdjs.Fase1Code.condition1IsTrue_1 = {val:false};
gdjs.Fase1Code.condition2IsTrue_1 = {val:false};
gdjs.Fase1Code.condition3IsTrue_1 = {val:false};
gdjs.Fase1Code.condition4IsTrue_1 = {val:false};
gdjs.Fase1Code.condition5IsTrue_1 = {val:false};


gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDwaterObjects1Objects = Hashtable.newFrom({"water": gdjs.Fase1Code.GDwaterObjects1});gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase1Code.GDplayerObjects1});gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDendObjects1Objects = Hashtable.newFrom({"end": gdjs.Fase1Code.GDendObjects1});gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase1Code.GDenemy1Objects1});gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase1Code.GDplayerObjects1});gdjs.Fase1Code.eventsList0x7b9ebc = function(runtimeScene) {

{

gdjs.Fase1Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase1Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
gdjs.Fase1Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase1Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDdinoObjects2[k] = gdjs.Fase1Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase1Code.GDdinoObjects2.length = k;}if ( gdjs.Fase1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.Fase1Code.condition1IsTrue_0.val = true;
        gdjs.Fase1Code.GDplayerObjects2[k] = gdjs.Fase1Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase1Code.GDplayerObjects2.length = k;}}
if (gdjs.Fase1Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase1Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase1Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase1Code.GDdinoObjects2[i].setAnimationName("jump");
}
}}

}


{

gdjs.Fase1Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase1Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
gdjs.Fase1Code.condition1IsTrue_0.val = false;
gdjs.Fase1Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase1Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDdinoObjects2[k] = gdjs.Fase1Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase1Code.GDdinoObjects2.length = k;}if ( gdjs.Fase1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving() ) {
        gdjs.Fase1Code.condition1IsTrue_0.val = true;
        gdjs.Fase1Code.GDplayerObjects2[k] = gdjs.Fase1Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase1Code.GDplayerObjects2.length = k;}if ( gdjs.Fase1Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase1Code.condition2IsTrue_0.val = true;
        gdjs.Fase1Code.GDplayerObjects2[k] = gdjs.Fase1Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase1Code.GDplayerObjects2.length = k;}}
}
if (gdjs.Fase1Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Fase1Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase1Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase1Code.GDdinoObjects2[i].setAnimationName("run");
}
}}

}


{

gdjs.Fase1Code.GDdinoObjects2.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase1Code.GDplayerObjects2.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
gdjs.Fase1Code.condition1IsTrue_0.val = false;
gdjs.Fase1Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDdinoObjects2.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDdinoObjects2[i].getVariableNumber(gdjs.Fase1Code.GDdinoObjects2[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDdinoObjects2[k] = gdjs.Fase1Code.GDdinoObjects2[i];
        ++k;
    }
}
gdjs.Fase1Code.GDdinoObjects2.length = k;}if ( gdjs.Fase1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDplayerObjects2.length;i<l;++i) {
    if ( !(gdjs.Fase1Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Fase1Code.condition1IsTrue_0.val = true;
        gdjs.Fase1Code.GDplayerObjects2[k] = gdjs.Fase1Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase1Code.GDplayerObjects2.length = k;}if ( gdjs.Fase1Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDplayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDplayerObjects2[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase1Code.condition2IsTrue_0.val = true;
        gdjs.Fase1Code.GDplayerObjects2[k] = gdjs.Fase1Code.GDplayerObjects2[i];
        ++k;
    }
}
gdjs.Fase1Code.GDplayerObjects2.length = k;}}
}
if (gdjs.Fase1Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Fase1Code.GDdinoObjects2 */
{for(var i = 0, len = gdjs.Fase1Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase1Code.GDdinoObjects2[i].setAnimationName("idle");
}
}}

}


{

gdjs.Fase1Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
gdjs.Fase1Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase1Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isJumping()) ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDplayerObjects1[k] = gdjs.Fase1Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDplayerObjects1.length = k;}if ( gdjs.Fase1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase1Code.condition1IsTrue_0.val = true;
        gdjs.Fase1Code.GDplayerObjects1[k] = gdjs.Fase1Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase1Code.condition1IsTrue_0.val) {
gdjs.Fase1Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
{for(var i = 0, len = gdjs.Fase1Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDdinoObjects1[i].setAnimationName("falling");
}
}}

}


}; //End of gdjs.Fase1Code.eventsList0x7b9ebc
gdjs.Fase1Code.eventsList0x973b3c = function(runtimeScene) {

{


gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
gdjs.Fase1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if (gdjs.Fase1Code.condition0IsTrue_0.val) {
gdjs.Fase1Code.GDdinoObjects2.createFrom(gdjs.Fase1Code.GDdinoObjects1);

{for(var i = 0, len = gdjs.Fase1Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase1Code.GDdinoObjects2[i].flipX(true);
}
}}

}


{


gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
gdjs.Fase1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if (gdjs.Fase1Code.condition0IsTrue_0.val) {
gdjs.Fase1Code.GDdinoObjects2.createFrom(gdjs.Fase1Code.GDdinoObjects1);

{for(var i = 0, len = gdjs.Fase1Code.GDdinoObjects2.length ;i < len;++i) {
    gdjs.Fase1Code.GDdinoObjects2[i].flipX(false);
}
}}

}


{

gdjs.Fase1Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
gdjs.Fase1Code.condition1IsTrue_0.val = false;
{
gdjs.Fase1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.Fase1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase1Code.condition1IsTrue_0.val = true;
        gdjs.Fase1Code.GDplayerObjects1[k] = gdjs.Fase1Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDplayerObjects1.length = k;}}
if (gdjs.Fase1Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase1Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase1Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDplayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


}; //End of gdjs.Fase1Code.eventsList0x973b3c
gdjs.Fase1Code.eventsList0x7cc97c = function(runtimeScene) {

{

gdjs.Fase1Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDdinoObjects1[i].getVariableNumber(gdjs.Fase1Code.GDdinoObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDdinoObjects1[k] = gdjs.Fase1Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDdinoObjects1.length = k;}if (gdjs.Fase1Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.Fase1Code.eventsList0x973b3c(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.Fase1Code.eventsList0x7cc97c
gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase1Code.GDenemy1Objects1});gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDflipEnemyObjects1Objects = Hashtable.newFrom({"flipEnemy": gdjs.Fase1Code.GDflipEnemyObjects1});gdjs.Fase1Code.eventsList0x923f44 = function(runtimeScene) {

{

gdjs.Fase1Code.GDenemy1Objects2.createFrom(gdjs.Fase1Code.GDenemy1Objects1);


gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDenemy1Objects2.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDenemy1Objects2[i].getVariableNumber(gdjs.Fase1Code.GDenemy1Objects2[i].getVariables().getFromIndex(0)) < 0 ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDenemy1Objects2[k] = gdjs.Fase1Code.GDenemy1Objects2[i];
        ++k;
    }
}
gdjs.Fase1Code.GDenemy1Objects2.length = k;}if (gdjs.Fase1Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase1Code.GDenemy1Objects2 */
{for(var i = 0, len = gdjs.Fase1Code.GDenemy1Objects2.length ;i < len;++i) {
    gdjs.Fase1Code.GDenemy1Objects2[i].flipX(false);
}
}}

}


{

/* Reuse gdjs.Fase1Code.GDenemy1Objects1 */

gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase1Code.GDenemy1Objects1[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDenemy1Objects1[k] = gdjs.Fase1Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDenemy1Objects1.length = k;}if (gdjs.Fase1Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase1Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase1Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDenemy1Objects1[i].flipX(true);
}
}}

}


}; //End of gdjs.Fase1Code.eventsList0x923f44
gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.Fase1Code.GDplayerObjects1});gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDenemy1Objects1Objects = Hashtable.newFrom({"enemy1": gdjs.Fase1Code.GDenemy1Objects1});gdjs.Fase1Code.eventsList0x5b6e18 = function(runtimeScene) {

{



}


{


gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
gdjs.Fase1Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase1Code.condition0IsTrue_0.val) {
gdjs.Fase1Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));
gdjs.Fase1Code.GDlblFaseObjects1.createFrom(runtimeScene.getObjects("lblFase"));
{for(var i = 0, len = gdjs.Fase1Code.GDlblFaseObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDlblFaseObjects1[i].setString("FASE " + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{gdjs.evtTools.camera.showLayer(runtimeScene, "UI");
}{for(var i = 0, len = gdjs.Fase1Code.GDfade_95faseObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDfade_95faseObjects1[i].getBehavior("Tween").addObjectOpacityTween("fadeout", 0, "linear", 2000, false);
}
}}

}


{

gdjs.Fase1Code.GDwaterObjects1.createFrom(runtimeScene.getObjects("water"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
gdjs.Fase1Code.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDwaterObjects1Objects) > 0;
}if (gdjs.Fase1Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase1Code.GDwaterObjects1 */
{for(var i = 0, len = gdjs.Fase1Code.GDwaterObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDwaterObjects1[i].setXOffset(gdjs.Fase1Code.GDwaterObjects1[i].getXOffset() + (0.5));
}
}}

}


{

gdjs.Fase1Code.GDendObjects1.createFrom(runtimeScene.getObjects("end"));
gdjs.Fase1Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
gdjs.Fase1Code.condition1IsTrue_0.val = false;
{
gdjs.Fase1Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDplayerObjects1Objects, gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDendObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase1Code.condition0IsTrue_0.val ) {
{
gdjs.Fase1Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 0;
}}
if (gdjs.Fase1Code.condition1IsTrue_0.val) {
gdjs.Fase1Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}{for(var i = 0, len = gdjs.Fase1Code.GDfade_95faseObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDfade_95faseObjects1[i].getBehavior("Tween").addObjectOpacityTween("fadein", 255, "linear", 2000, false);
}
}}

}


{

gdjs.Fase1Code.GDfade_95faseObjects1.createFrom(runtimeScene.getObjects("fade_fase"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
gdjs.Fase1Code.condition1IsTrue_0.val = false;
{
gdjs.Fase1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if ( gdjs.Fase1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDfade_95faseObjects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDfade_95faseObjects1[i].getOpacity() >= 255 ) {
        gdjs.Fase1Code.condition1IsTrue_0.val = true;
        gdjs.Fase1Code.GDfade_95faseObjects1[k] = gdjs.Fase1Code.GDfade_95faseObjects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDfade_95faseObjects1.length = k;}}
if (gdjs.Fase1Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(2);
}}

}


{


gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
gdjs.Fase1Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 2;
}if (gdjs.Fase1Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


{



}


{


gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
gdjs.Fase1Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase1Code.condition0IsTrue_0.val) {
gdjs.Fase1Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.Fase1Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDplayerObjects1[i].hide();
}
}}

}


{



}


{


{
gdjs.Fase1Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase1Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.Fase1Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDdinoObjects1[i].setPosition((( gdjs.Fase1Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase1Code.GDplayerObjects1[0].getPointX("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase1Code.GDdinoObjects1[i].getVariables().getFromIndex(0))),(( gdjs.Fase1Code.GDplayerObjects1.length === 0 ) ? 0 :gdjs.Fase1Code.GDplayerObjects1[0].getPointY("")) - (gdjs.RuntimeObject.getVariableNumber(gdjs.Fase1Code.GDdinoObjects1[i].getVariables().getFromIndex(1))));
}
}{gdjs.evtTools.camera.centerCameraWithinLimits(runtimeScene, (gdjs.Fase1Code.GDdinoObjects1.length !== 0 ? gdjs.Fase1Code.GDdinoObjects1[0] : null), 0, 0, gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)), 768, true, "", 0);
}{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.Fase1Code.GDdinoObjects1.length === 0 ) ? 0 :gdjs.Fase1Code.GDdinoObjects1[0].getPointX("")) * 0.15, "Background", 0);
}}

}


{



}


{



}


{

gdjs.Fase1Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));
gdjs.Fase1Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase1Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
gdjs.Fase1Code.condition1IsTrue_0.val = false;
gdjs.Fase1Code.condition2IsTrue_0.val = false;
gdjs.Fase1Code.condition3IsTrue_0.val = false;
gdjs.Fase1Code.condition4IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDplayerObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase1Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling()) ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDplayerObjects1[k] = gdjs.Fase1Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDplayerObjects1.length = k;}if ( gdjs.Fase1Code.condition0IsTrue_0.val ) {
{
gdjs.Fase1Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDenemy1Objects1Objects, gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDplayerObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase1Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase1Code.GDenemy1Objects1[i].getVariables().get("die")) == 0 ) {
        gdjs.Fase1Code.condition2IsTrue_0.val = true;
        gdjs.Fase1Code.GDenemy1Objects1[k] = gdjs.Fase1Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDenemy1Objects1.length = k;}if ( gdjs.Fase1Code.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDdinoObjects1[i].getVariableNumber(gdjs.Fase1Code.GDdinoObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.Fase1Code.condition3IsTrue_0.val = true;
        gdjs.Fase1Code.GDdinoObjects1[k] = gdjs.Fase1Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDdinoObjects1.length = k;}if ( gdjs.Fase1Code.condition3IsTrue_0.val ) {
{
{gdjs.Fase1Code.conditionTrue_1 = gdjs.Fase1Code.condition4IsTrue_0;
gdjs.Fase1Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8312564);
}
}}
}
}
}
if (gdjs.Fase1Code.condition4IsTrue_0.val) {
/* Reuse gdjs.Fase1Code.GDdinoObjects1 */
/* Reuse gdjs.Fase1Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase1Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDplayerObjects1[i].getBehavior("PlatformerObject").ignoreDefaultControls(true);
}
}{for(var i = 0, len = gdjs.Fase1Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDdinoObjects1[i].returnVariable(gdjs.Fase1Code.GDdinoObjects1[i].getVariables().getFromIndex(2)).setNumber(1);
}
}{for(var i = 0, len = gdjs.Fase1Code.GDdinoObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDdinoObjects1[i].setAnimationName("die");
}
}}

}


{



}


{

gdjs.Fase1Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
gdjs.Fase1Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDdinoObjects1[i].isCurrentAnimationName("die") ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDdinoObjects1[k] = gdjs.Fase1Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDdinoObjects1.length = k;}if ( gdjs.Fase1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDdinoObjects1[i].hasAnimationEnded() ) {
        gdjs.Fase1Code.condition1IsTrue_0.val = true;
        gdjs.Fase1Code.GDdinoObjects1[k] = gdjs.Fase1Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDdinoObjects1.length = k;}}
if (gdjs.Fase1Code.condition1IsTrue_0.val) {
{gdjs.evtsExt__Util__setFase.func(runtimeScene, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.Fase1Code.GDdinoObjects1.createFrom(runtimeScene.getObjects("dino"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDdinoObjects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDdinoObjects1[i].getY() > gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDdinoObjects1[k] = gdjs.Fase1Code.GDdinoObjects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDdinoObjects1.length = k;}if (gdjs.Fase1Code.condition0IsTrue_0.val) {
{gdjs.evtsExt__Util__setFase.func(runtimeScene, gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Fase1Code.eventsList0x7b9ebc(runtimeScene);
}


{


gdjs.Fase1Code.eventsList0x7cc97c(runtimeScene);
}


{



}


{


gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
gdjs.Fase1Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase1Code.condition0IsTrue_0.val) {
gdjs.Fase1Code.GDflipEnemyObjects1.createFrom(runtimeScene.getObjects("flipEnemy"));
{for(var i = 0, len = gdjs.Fase1Code.GDflipEnemyObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDflipEnemyObjects1[i].hide();
}
}}

}


{



}


{


{
gdjs.Fase1Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
{for(var i = 0, len = gdjs.Fase1Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDenemy1Objects1[i].setX(gdjs.Fase1Code.GDenemy1Objects1[i].getX() + ((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase1Code.GDenemy1Objects1[i].getVariables().getFromIndex(0))) * gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{



}


{

gdjs.Fase1Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase1Code.GDflipEnemyObjects1.createFrom(runtimeScene.getObjects("flipEnemy"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
gdjs.Fase1Code.condition1IsTrue_0.val = false;
{
gdjs.Fase1Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDenemy1Objects1Objects, gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDflipEnemyObjects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase1Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase1Code.conditionTrue_1 = gdjs.Fase1Code.condition1IsTrue_0;
gdjs.Fase1Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8404668);
}
}}
if (gdjs.Fase1Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Fase1Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase1Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDenemy1Objects1[i].returnVariable(gdjs.Fase1Code.GDenemy1Objects1[i].getVariables().getFromIndex(0)).mul(-(1));
}
}
{ //Subevents
gdjs.Fase1Code.eventsList0x923f44(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.Fase1Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));
gdjs.Fase1Code.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
gdjs.Fase1Code.condition1IsTrue_0.val = false;
gdjs.Fase1Code.condition2IsTrue_0.val = false;
gdjs.Fase1Code.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDplayerObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDplayerObjects1[k] = gdjs.Fase1Code.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDplayerObjects1.length = k;}if ( gdjs.Fase1Code.condition0IsTrue_0.val ) {
{
gdjs.Fase1Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDplayerObjects1Objects, gdjs.Fase1Code.mapOfGDgdjs_46Fase1Code_46GDenemy1Objects1Objects, false, runtimeScene, false);
}if ( gdjs.Fase1Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDenemy1Objects1[i].getVariableNumber(gdjs.Fase1Code.GDenemy1Objects1[i].getVariables().get("die")) == 0 ) {
        gdjs.Fase1Code.condition2IsTrue_0.val = true;
        gdjs.Fase1Code.GDenemy1Objects1[k] = gdjs.Fase1Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDenemy1Objects1.length = k;}if ( gdjs.Fase1Code.condition2IsTrue_0.val ) {
{
{gdjs.Fase1Code.conditionTrue_1 = gdjs.Fase1Code.condition3IsTrue_0;
gdjs.Fase1Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8396060);
}
}}
}
}
if (gdjs.Fase1Code.condition3IsTrue_0.val) {
/* Reuse gdjs.Fase1Code.GDenemy1Objects1 */
/* Reuse gdjs.Fase1Code.GDplayerObjects1 */
{for(var i = 0, len = gdjs.Fase1Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDplayerObjects1[i].getBehavior("PlatformerObject").setCanJump();
}
}{for(var i = 0, len = gdjs.Fase1Code.GDplayerObjects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDplayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}{for(var i = 0, len = gdjs.Fase1Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDenemy1Objects1[i].returnVariable(gdjs.Fase1Code.GDenemy1Objects1[i].getVariables().get("die")).setNumber(1);
}
}{for(var i = 0, len = gdjs.Fase1Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDenemy1Objects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.Fase1Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDenemy1Objects1[i].getBehavior("Tween").addObjectOpacityTween("enemy1_die", 0, "linear", 1000, false);
}
}}

}


{

gdjs.Fase1Code.GDenemy1Objects1.createFrom(runtimeScene.getObjects("enemy1"));

gdjs.Fase1Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase1Code.GDenemy1Objects1.length;i<l;++i) {
    if ( gdjs.Fase1Code.GDenemy1Objects1[i].getOpacity() <= 0 ) {
        gdjs.Fase1Code.condition0IsTrue_0.val = true;
        gdjs.Fase1Code.GDenemy1Objects1[k] = gdjs.Fase1Code.GDenemy1Objects1[i];
        ++k;
    }
}
gdjs.Fase1Code.GDenemy1Objects1.length = k;}if (gdjs.Fase1Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Fase1Code.GDenemy1Objects1 */
{for(var i = 0, len = gdjs.Fase1Code.GDenemy1Objects1.length ;i < len;++i) {
    gdjs.Fase1Code.GDenemy1Objects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.Fase1Code.eventsList0x5b6e18


gdjs.Fase1Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Fase1Code.GDdinoObjects1.length = 0;
gdjs.Fase1Code.GDdinoObjects2.length = 0;
gdjs.Fase1Code.GDdinoObjects3.length = 0;
gdjs.Fase1Code.GDplayerObjects1.length = 0;
gdjs.Fase1Code.GDplayerObjects2.length = 0;
gdjs.Fase1Code.GDplayerObjects3.length = 0;
gdjs.Fase1Code.GDground1Objects1.length = 0;
gdjs.Fase1Code.GDground1Objects2.length = 0;
gdjs.Fase1Code.GDground1Objects3.length = 0;
gdjs.Fase1Code.GDwaterObjects1.length = 0;
gdjs.Fase1Code.GDwaterObjects2.length = 0;
gdjs.Fase1Code.GDwaterObjects3.length = 0;
gdjs.Fase1Code.GDfade_95faseObjects1.length = 0;
gdjs.Fase1Code.GDfade_95faseObjects2.length = 0;
gdjs.Fase1Code.GDfade_95faseObjects3.length = 0;
gdjs.Fase1Code.GDlblFaseObjects1.length = 0;
gdjs.Fase1Code.GDlblFaseObjects2.length = 0;
gdjs.Fase1Code.GDlblFaseObjects3.length = 0;
gdjs.Fase1Code.GDbg1Objects1.length = 0;
gdjs.Fase1Code.GDbg1Objects2.length = 0;
gdjs.Fase1Code.GDbg1Objects3.length = 0;
gdjs.Fase1Code.GDboxObjects1.length = 0;
gdjs.Fase1Code.GDboxObjects2.length = 0;
gdjs.Fase1Code.GDboxObjects3.length = 0;
gdjs.Fase1Code.GDplacaObjects1.length = 0;
gdjs.Fase1Code.GDplacaObjects2.length = 0;
gdjs.Fase1Code.GDplacaObjects3.length = 0;
gdjs.Fase1Code.GDthreeObjects1.length = 0;
gdjs.Fase1Code.GDthreeObjects2.length = 0;
gdjs.Fase1Code.GDthreeObjects3.length = 0;
gdjs.Fase1Code.GDenemy1Objects1.length = 0;
gdjs.Fase1Code.GDenemy1Objects2.length = 0;
gdjs.Fase1Code.GDenemy1Objects3.length = 0;
gdjs.Fase1Code.GDflipEnemyObjects1.length = 0;
gdjs.Fase1Code.GDflipEnemyObjects2.length = 0;
gdjs.Fase1Code.GDflipEnemyObjects3.length = 0;
gdjs.Fase1Code.GDground1bordasObjects1.length = 0;
gdjs.Fase1Code.GDground1bordasObjects2.length = 0;
gdjs.Fase1Code.GDground1bordasObjects3.length = 0;
gdjs.Fase1Code.GDbigGroundObjects1.length = 0;
gdjs.Fase1Code.GDbigGroundObjects2.length = 0;
gdjs.Fase1Code.GDbigGroundObjects3.length = 0;
gdjs.Fase1Code.GDplataforma1Objects1.length = 0;
gdjs.Fase1Code.GDplataforma1Objects2.length = 0;
gdjs.Fase1Code.GDplataforma1Objects3.length = 0;
gdjs.Fase1Code.GDendObjects1.length = 0;
gdjs.Fase1Code.GDendObjects2.length = 0;
gdjs.Fase1Code.GDendObjects3.length = 0;

gdjs.Fase1Code.eventsList0x5b6e18(runtimeScene);
return;

}

gdjs['Fase1Code'] = gdjs.Fase1Code;
