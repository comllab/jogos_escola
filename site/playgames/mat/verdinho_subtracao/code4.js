gdjs.Fase4Code = {};
gdjs.Fase4Code.GDPlayerObjects1_1final = [];

gdjs.Fase4Code.forEachCount0_5 = 0;

gdjs.Fase4Code.forEachCount1_5 = 0;

gdjs.Fase4Code.forEachIndex5 = 0;

gdjs.Fase4Code.forEachObjects5 = [];

gdjs.Fase4Code.forEachTotalCount5 = 0;



gdjs.Fase4Code.GDPlayerObjects1= [];
gdjs.Fase4Code.GDPlayerObjects2= [];
gdjs.Fase4Code.GDPlayerObjects3= [];
gdjs.Fase4Code.GDPlayerObjects4= [];
gdjs.Fase4Code.GDPlayerObjects5= [];
gdjs.Fase4Code.GDPlatformObjects1= [];
gdjs.Fase4Code.GDPlatformObjects2= [];
gdjs.Fase4Code.GDPlatformObjects3= [];
gdjs.Fase4Code.GDPlatformObjects4= [];
gdjs.Fase4Code.GDPlatformObjects5= [];
gdjs.Fase4Code.GDJumpthruObjects1= [];
gdjs.Fase4Code.GDJumpthruObjects2= [];
gdjs.Fase4Code.GDJumpthruObjects3= [];
gdjs.Fase4Code.GDJumpthruObjects4= [];
gdjs.Fase4Code.GDJumpthruObjects5= [];
gdjs.Fase4Code.GDTiledGrassPlatformObjects1= [];
gdjs.Fase4Code.GDTiledGrassPlatformObjects2= [];
gdjs.Fase4Code.GDTiledGrassPlatformObjects3= [];
gdjs.Fase4Code.GDTiledGrassPlatformObjects4= [];
gdjs.Fase4Code.GDTiledGrassPlatformObjects5= [];
gdjs.Fase4Code.GDTiledCastlePlatformObjects1= [];
gdjs.Fase4Code.GDTiledCastlePlatformObjects2= [];
gdjs.Fase4Code.GDTiledCastlePlatformObjects3= [];
gdjs.Fase4Code.GDTiledCastlePlatformObjects4= [];
gdjs.Fase4Code.GDTiledCastlePlatformObjects5= [];
gdjs.Fase4Code.GDMovingPlatformObjects1= [];
gdjs.Fase4Code.GDMovingPlatformObjects2= [];
gdjs.Fase4Code.GDMovingPlatformObjects3= [];
gdjs.Fase4Code.GDMovingPlatformObjects4= [];
gdjs.Fase4Code.GDMovingPlatformObjects5= [];
gdjs.Fase4Code.GDGoLeftObjects1= [];
gdjs.Fase4Code.GDGoLeftObjects2= [];
gdjs.Fase4Code.GDGoLeftObjects3= [];
gdjs.Fase4Code.GDGoLeftObjects4= [];
gdjs.Fase4Code.GDGoLeftObjects5= [];
gdjs.Fase4Code.GDGoRightObjects1= [];
gdjs.Fase4Code.GDGoRightObjects2= [];
gdjs.Fase4Code.GDGoRightObjects3= [];
gdjs.Fase4Code.GDGoRightObjects4= [];
gdjs.Fase4Code.GDGoRightObjects5= [];
gdjs.Fase4Code.GDLadderObjects1= [];
gdjs.Fase4Code.GDLadderObjects2= [];
gdjs.Fase4Code.GDLadderObjects3= [];
gdjs.Fase4Code.GDLadderObjects4= [];
gdjs.Fase4Code.GDLadderObjects5= [];
gdjs.Fase4Code.GDPlayerHitBoxObjects1= [];
gdjs.Fase4Code.GDPlayerHitBoxObjects2= [];
gdjs.Fase4Code.GDPlayerHitBoxObjects3= [];
gdjs.Fase4Code.GDPlayerHitBoxObjects4= [];
gdjs.Fase4Code.GDPlayerHitBoxObjects5= [];
gdjs.Fase4Code.GDSlimeWalkObjects1= [];
gdjs.Fase4Code.GDSlimeWalkObjects2= [];
gdjs.Fase4Code.GDSlimeWalkObjects3= [];
gdjs.Fase4Code.GDSlimeWalkObjects4= [];
gdjs.Fase4Code.GDSlimeWalkObjects5= [];
gdjs.Fase4Code.GDFlyObjects1= [];
gdjs.Fase4Code.GDFlyObjects2= [];
gdjs.Fase4Code.GDFlyObjects3= [];
gdjs.Fase4Code.GDFlyObjects4= [];
gdjs.Fase4Code.GDFlyObjects5= [];
gdjs.Fase4Code.GDCloudObjects1= [];
gdjs.Fase4Code.GDCloudObjects2= [];
gdjs.Fase4Code.GDCloudObjects3= [];
gdjs.Fase4Code.GDCloudObjects4= [];
gdjs.Fase4Code.GDCloudObjects5= [];
gdjs.Fase4Code.GDBackgroundObjectsObjects1= [];
gdjs.Fase4Code.GDBackgroundObjectsObjects2= [];
gdjs.Fase4Code.GDBackgroundObjectsObjects3= [];
gdjs.Fase4Code.GDBackgroundObjectsObjects4= [];
gdjs.Fase4Code.GDBackgroundObjectsObjects5= [];
gdjs.Fase4Code.GDScoreObjects1= [];
gdjs.Fase4Code.GDScoreObjects2= [];
gdjs.Fase4Code.GDScoreObjects3= [];
gdjs.Fase4Code.GDScoreObjects4= [];
gdjs.Fase4Code.GDScoreObjects5= [];
gdjs.Fase4Code.GDCoinObjects1= [];
gdjs.Fase4Code.GDCoinObjects2= [];
gdjs.Fase4Code.GDCoinObjects3= [];
gdjs.Fase4Code.GDCoinObjects4= [];
gdjs.Fase4Code.GDCoinObjects5= [];
gdjs.Fase4Code.GDCoinIconObjects1= [];
gdjs.Fase4Code.GDCoinIconObjects2= [];
gdjs.Fase4Code.GDCoinIconObjects3= [];
gdjs.Fase4Code.GDCoinIconObjects4= [];
gdjs.Fase4Code.GDCoinIconObjects5= [];
gdjs.Fase4Code.GDLeftButtonObjects1= [];
gdjs.Fase4Code.GDLeftButtonObjects2= [];
gdjs.Fase4Code.GDLeftButtonObjects3= [];
gdjs.Fase4Code.GDLeftButtonObjects4= [];
gdjs.Fase4Code.GDLeftButtonObjects5= [];
gdjs.Fase4Code.GDRightButtonObjects1= [];
gdjs.Fase4Code.GDRightButtonObjects2= [];
gdjs.Fase4Code.GDRightButtonObjects3= [];
gdjs.Fase4Code.GDRightButtonObjects4= [];
gdjs.Fase4Code.GDRightButtonObjects5= [];
gdjs.Fase4Code.GDJumpButtonObjects1= [];
gdjs.Fase4Code.GDJumpButtonObjects2= [];
gdjs.Fase4Code.GDJumpButtonObjects3= [];
gdjs.Fase4Code.GDJumpButtonObjects4= [];
gdjs.Fase4Code.GDJumpButtonObjects5= [];
gdjs.Fase4Code.GDArrowButtonsBgObjects1= [];
gdjs.Fase4Code.GDArrowButtonsBgObjects2= [];
gdjs.Fase4Code.GDArrowButtonsBgObjects3= [];
gdjs.Fase4Code.GDArrowButtonsBgObjects4= [];
gdjs.Fase4Code.GDArrowButtonsBgObjects5= [];
gdjs.Fase4Code.GDlockObjects1= [];
gdjs.Fase4Code.GDlockObjects2= [];
gdjs.Fase4Code.GDlockObjects3= [];
gdjs.Fase4Code.GDlockObjects4= [];
gdjs.Fase4Code.GDlockObjects5= [];
gdjs.Fase4Code.GDfaseObjects1= [];
gdjs.Fase4Code.GDfaseObjects2= [];
gdjs.Fase4Code.GDfaseObjects3= [];
gdjs.Fase4Code.GDfaseObjects4= [];
gdjs.Fase4Code.GDfaseObjects5= [];

gdjs.Fase4Code.conditionTrue_0 = {val:false};
gdjs.Fase4Code.condition0IsTrue_0 = {val:false};
gdjs.Fase4Code.condition1IsTrue_0 = {val:false};
gdjs.Fase4Code.condition2IsTrue_0 = {val:false};
gdjs.Fase4Code.condition3IsTrue_0 = {val:false};
gdjs.Fase4Code.conditionTrue_1 = {val:false};
gdjs.Fase4Code.condition0IsTrue_1 = {val:false};
gdjs.Fase4Code.condition1IsTrue_1 = {val:false};
gdjs.Fase4Code.condition2IsTrue_1 = {val:false};
gdjs.Fase4Code.condition3IsTrue_1 = {val:false};

gdjs.Fase4Code.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.Fase4Code.GDPlayerObjects1.length = 0;
gdjs.Fase4Code.GDPlayerObjects2.length = 0;
gdjs.Fase4Code.GDPlayerObjects3.length = 0;
gdjs.Fase4Code.GDPlayerObjects4.length = 0;
gdjs.Fase4Code.GDPlayerObjects5.length = 0;
gdjs.Fase4Code.GDPlatformObjects1.length = 0;
gdjs.Fase4Code.GDPlatformObjects2.length = 0;
gdjs.Fase4Code.GDPlatformObjects3.length = 0;
gdjs.Fase4Code.GDPlatformObjects4.length = 0;
gdjs.Fase4Code.GDPlatformObjects5.length = 0;
gdjs.Fase4Code.GDJumpthruObjects1.length = 0;
gdjs.Fase4Code.GDJumpthruObjects2.length = 0;
gdjs.Fase4Code.GDJumpthruObjects3.length = 0;
gdjs.Fase4Code.GDJumpthruObjects4.length = 0;
gdjs.Fase4Code.GDJumpthruObjects5.length = 0;
gdjs.Fase4Code.GDTiledGrassPlatformObjects1.length = 0;
gdjs.Fase4Code.GDTiledGrassPlatformObjects2.length = 0;
gdjs.Fase4Code.GDTiledGrassPlatformObjects3.length = 0;
gdjs.Fase4Code.GDTiledGrassPlatformObjects4.length = 0;
gdjs.Fase4Code.GDTiledGrassPlatformObjects5.length = 0;
gdjs.Fase4Code.GDTiledCastlePlatformObjects1.length = 0;
gdjs.Fase4Code.GDTiledCastlePlatformObjects2.length = 0;
gdjs.Fase4Code.GDTiledCastlePlatformObjects3.length = 0;
gdjs.Fase4Code.GDTiledCastlePlatformObjects4.length = 0;
gdjs.Fase4Code.GDTiledCastlePlatformObjects5.length = 0;
gdjs.Fase4Code.GDMovingPlatformObjects1.length = 0;
gdjs.Fase4Code.GDMovingPlatformObjects2.length = 0;
gdjs.Fase4Code.GDMovingPlatformObjects3.length = 0;
gdjs.Fase4Code.GDMovingPlatformObjects4.length = 0;
gdjs.Fase4Code.GDMovingPlatformObjects5.length = 0;
gdjs.Fase4Code.GDGoLeftObjects1.length = 0;
gdjs.Fase4Code.GDGoLeftObjects2.length = 0;
gdjs.Fase4Code.GDGoLeftObjects3.length = 0;
gdjs.Fase4Code.GDGoLeftObjects4.length = 0;
gdjs.Fase4Code.GDGoLeftObjects5.length = 0;
gdjs.Fase4Code.GDGoRightObjects1.length = 0;
gdjs.Fase4Code.GDGoRightObjects2.length = 0;
gdjs.Fase4Code.GDGoRightObjects3.length = 0;
gdjs.Fase4Code.GDGoRightObjects4.length = 0;
gdjs.Fase4Code.GDGoRightObjects5.length = 0;
gdjs.Fase4Code.GDLadderObjects1.length = 0;
gdjs.Fase4Code.GDLadderObjects2.length = 0;
gdjs.Fase4Code.GDLadderObjects3.length = 0;
gdjs.Fase4Code.GDLadderObjects4.length = 0;
gdjs.Fase4Code.GDLadderObjects5.length = 0;
gdjs.Fase4Code.GDPlayerHitBoxObjects1.length = 0;
gdjs.Fase4Code.GDPlayerHitBoxObjects2.length = 0;
gdjs.Fase4Code.GDPlayerHitBoxObjects3.length = 0;
gdjs.Fase4Code.GDPlayerHitBoxObjects4.length = 0;
gdjs.Fase4Code.GDPlayerHitBoxObjects5.length = 0;
gdjs.Fase4Code.GDSlimeWalkObjects1.length = 0;
gdjs.Fase4Code.GDSlimeWalkObjects2.length = 0;
gdjs.Fase4Code.GDSlimeWalkObjects3.length = 0;
gdjs.Fase4Code.GDSlimeWalkObjects4.length = 0;
gdjs.Fase4Code.GDSlimeWalkObjects5.length = 0;
gdjs.Fase4Code.GDFlyObjects1.length = 0;
gdjs.Fase4Code.GDFlyObjects2.length = 0;
gdjs.Fase4Code.GDFlyObjects3.length = 0;
gdjs.Fase4Code.GDFlyObjects4.length = 0;
gdjs.Fase4Code.GDFlyObjects5.length = 0;
gdjs.Fase4Code.GDCloudObjects1.length = 0;
gdjs.Fase4Code.GDCloudObjects2.length = 0;
gdjs.Fase4Code.GDCloudObjects3.length = 0;
gdjs.Fase4Code.GDCloudObjects4.length = 0;
gdjs.Fase4Code.GDCloudObjects5.length = 0;
gdjs.Fase4Code.GDBackgroundObjectsObjects1.length = 0;
gdjs.Fase4Code.GDBackgroundObjectsObjects2.length = 0;
gdjs.Fase4Code.GDBackgroundObjectsObjects3.length = 0;
gdjs.Fase4Code.GDBackgroundObjectsObjects4.length = 0;
gdjs.Fase4Code.GDBackgroundObjectsObjects5.length = 0;
gdjs.Fase4Code.GDScoreObjects1.length = 0;
gdjs.Fase4Code.GDScoreObjects2.length = 0;
gdjs.Fase4Code.GDScoreObjects3.length = 0;
gdjs.Fase4Code.GDScoreObjects4.length = 0;
gdjs.Fase4Code.GDScoreObjects5.length = 0;
gdjs.Fase4Code.GDCoinObjects1.length = 0;
gdjs.Fase4Code.GDCoinObjects2.length = 0;
gdjs.Fase4Code.GDCoinObjects3.length = 0;
gdjs.Fase4Code.GDCoinObjects4.length = 0;
gdjs.Fase4Code.GDCoinObjects5.length = 0;
gdjs.Fase4Code.GDCoinIconObjects1.length = 0;
gdjs.Fase4Code.GDCoinIconObjects2.length = 0;
gdjs.Fase4Code.GDCoinIconObjects3.length = 0;
gdjs.Fase4Code.GDCoinIconObjects4.length = 0;
gdjs.Fase4Code.GDCoinIconObjects5.length = 0;
gdjs.Fase4Code.GDLeftButtonObjects1.length = 0;
gdjs.Fase4Code.GDLeftButtonObjects2.length = 0;
gdjs.Fase4Code.GDLeftButtonObjects3.length = 0;
gdjs.Fase4Code.GDLeftButtonObjects4.length = 0;
gdjs.Fase4Code.GDLeftButtonObjects5.length = 0;
gdjs.Fase4Code.GDRightButtonObjects1.length = 0;
gdjs.Fase4Code.GDRightButtonObjects2.length = 0;
gdjs.Fase4Code.GDRightButtonObjects3.length = 0;
gdjs.Fase4Code.GDRightButtonObjects4.length = 0;
gdjs.Fase4Code.GDRightButtonObjects5.length = 0;
gdjs.Fase4Code.GDJumpButtonObjects1.length = 0;
gdjs.Fase4Code.GDJumpButtonObjects2.length = 0;
gdjs.Fase4Code.GDJumpButtonObjects3.length = 0;
gdjs.Fase4Code.GDJumpButtonObjects4.length = 0;
gdjs.Fase4Code.GDJumpButtonObjects5.length = 0;
gdjs.Fase4Code.GDArrowButtonsBgObjects1.length = 0;
gdjs.Fase4Code.GDArrowButtonsBgObjects2.length = 0;
gdjs.Fase4Code.GDArrowButtonsBgObjects3.length = 0;
gdjs.Fase4Code.GDArrowButtonsBgObjects4.length = 0;
gdjs.Fase4Code.GDArrowButtonsBgObjects5.length = 0;
gdjs.Fase4Code.GDlockObjects1.length = 0;
gdjs.Fase4Code.GDlockObjects2.length = 0;
gdjs.Fase4Code.GDlockObjects3.length = 0;
gdjs.Fase4Code.GDlockObjects4.length = 0;
gdjs.Fase4Code.GDlockObjects5.length = 0;
gdjs.Fase4Code.GDfaseObjects1.length = 0;
gdjs.Fase4Code.GDfaseObjects2.length = 0;
gdjs.Fase4Code.GDfaseObjects3.length = 0;
gdjs.Fase4Code.GDfaseObjects4.length = 0;
gdjs.Fase4Code.GDfaseObjects5.length = 0;


{

gdjs.Fase4Code.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));
gdjs.Fase4Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerHitBoxObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.Fase4Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDCoinObjects1[i].setVariableNumber(gdjs.Fase4Code.GDCoinObjects1[i].getVariables().getFromIndex(0), gdjs.random(8) + 1);
}
}{for(var i = 0, len = gdjs.Fase4Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDCoinObjects1[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.Fase4Code.GDCoinObjects1[i].getVariables().getFromIndex(0))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(4);
}{gdjs.evtTools.sound.playMusic(runtimeScene, "Intro Theme.ogg", true, 100, 1);
}}

}


{

gdjs.Fase4Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase4Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

{for(var i = 0, len = gdjs.Fase4Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerObjects1[i].setPosition((( gdjs.Fase4Code.GDPlayerHitBoxObjects1.length === 0 ) ? 0 :gdjs.Fase4Code.GDPlayerHitBoxObjects1[0].getPointX(""))-12,(( gdjs.Fase4Code.GDPlayerHitBoxObjects1.length === 0 ) ? 0 :gdjs.Fase4Code.GDPlayerHitBoxObjects1[0].getPointY("")));
}
}
}


{



}


{

gdjs.Fase4Code.GDPlayerObjects1.length = 0;

gdjs.Fase4Code.condition0IsTrue_0.val = false;
gdjs.Fase4Code.condition1IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "RShift");
}if ( gdjs.Fase4Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase4Code.conditionTrue_1 = gdjs.Fase4Code.condition1IsTrue_0;
gdjs.Fase4Code.GDPlayerObjects1_1final.length = 0;gdjs.Fase4Code.condition0IsTrue_1.val = false;
gdjs.Fase4Code.condition1IsTrue_1.val = false;
{
gdjs.Fase4Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDPlayerObjects2[i].getAnimation() == 0 ) {
        gdjs.Fase4Code.condition0IsTrue_1.val = true;
        gdjs.Fase4Code.GDPlayerObjects2[k] = gdjs.Fase4Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Fase4Code.GDPlayerObjects2.length = k;if( gdjs.Fase4Code.condition0IsTrue_1.val ) {
    gdjs.Fase4Code.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.Fase4Code.GDPlayerObjects2.length;j<jLen;++j) {
        if ( gdjs.Fase4Code.GDPlayerObjects1_1final.indexOf(gdjs.Fase4Code.GDPlayerObjects2[j]) === -1 )
            gdjs.Fase4Code.GDPlayerObjects1_1final.push(gdjs.Fase4Code.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.Fase4Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDPlayerObjects2[i].getAnimation() == 2 ) {
        gdjs.Fase4Code.condition1IsTrue_1.val = true;
        gdjs.Fase4Code.GDPlayerObjects2[k] = gdjs.Fase4Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Fase4Code.GDPlayerObjects2.length = k;if( gdjs.Fase4Code.condition1IsTrue_1.val ) {
    gdjs.Fase4Code.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.Fase4Code.GDPlayerObjects2.length;j<jLen;++j) {
        if ( gdjs.Fase4Code.GDPlayerObjects1_1final.indexOf(gdjs.Fase4Code.GDPlayerObjects2[j]) === -1 )
            gdjs.Fase4Code.GDPlayerObjects1_1final.push(gdjs.Fase4Code.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.Fase4Code.GDPlayerObjects1.createFrom(gdjs.Fase4Code.GDPlayerObjects1_1final);
}
}
}}
if (gdjs.Fase4Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}}

}


{

gdjs.Fase4Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
gdjs.Fase4Code.condition1IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.Fase4Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase4Code.conditionTrue_1 = gdjs.Fase4Code.condition1IsTrue_0;
gdjs.Fase4Code.conditionTrue_1.val = context.triggerOnce(239485036);
}
}}
if (gdjs.Fase4Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}{for(var i = 0, len = gdjs.Fase4Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{

gdjs.Fase4Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase4Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDPlayerHitBoxObjects1[k] = gdjs.Fase4Code.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDPlayerHitBoxObjects1.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.Fase4Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase4Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDPlayerHitBoxObjects1[k] = gdjs.Fase4Code.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDPlayerHitBoxObjects1.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.Fase4Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDPlayerHitBoxObjects1[k] = gdjs.Fase4Code.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDPlayerHitBoxObjects1.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {

{ //Subevents

{

gdjs.Fase4Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase4Code.GDPlayerHitBoxObjects2.createFrom(gdjs.Fase4Code.GDPlayerHitBoxObjects1);

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDPlayerHitBoxObjects2.length;i<l;++i) {
    if ( !(gdjs.Fase4Code.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDPlayerHitBoxObjects2[k] = gdjs.Fase4Code.GDPlayerHitBoxObjects2[i];
        ++k;
    }
}
gdjs.Fase4Code.GDPlayerHitBoxObjects2.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerObjects2[i].setAnimation(0);
}
}}

}


{

gdjs.Fase4Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase4Code.GDPlayerHitBoxObjects2.createFrom(gdjs.Fase4Code.GDPlayerHitBoxObjects1);

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDPlayerHitBoxObjects2.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").isMoving() ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDPlayerHitBoxObjects2[k] = gdjs.Fase4Code.GDPlayerHitBoxObjects2[i];
        ++k;
    }
}
gdjs.Fase4Code.GDPlayerHitBoxObjects2.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerObjects2[i].setAnimation(2);
}
}}

}

} //End of subevents
}

}


{

gdjs.Fase4Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerObjects1[i].flipX(true);
}
}}

}


{

gdjs.Fase4Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerObjects1[i].flipX(false);
}
}}

}


{



}


{

gdjs.Fase4Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));

{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.Fase4Code.GDPlayerObjects1.length === 0 ) ? 0 :gdjs.Fase4Code.GDPlayerObjects1[0].getPointX("")), "", 0);
}
}


{



}


{

gdjs.Fase4Code.GDGoLeftObjects1.createFrom(runtimeScene.getObjects("GoLeft"));
gdjs.Fase4Code.GDGoRightObjects1.createFrom(runtimeScene.getObjects("GoRight"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDGoLeftObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDGoLeftObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.Fase4Code.GDGoRightObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDGoRightObjects1[i].hide();
}
}}

}


{

gdjs.Fase4Code.GDGoLeftObjects1.createFrom(runtimeScene.getObjects("GoLeft"));
gdjs.Fase4Code.GDMovingPlatformObjects1.createFrom(runtimeScene.getObjects("MovingPlatform"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("GoLeft", gdjs.Fase4Code.GDGoLeftObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("MovingPlatform", gdjs.Fase4Code.GDMovingPlatformObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDMovingPlatformObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.Fase4Code.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDMovingPlatformObjects1[i].addForce(-150, 0, 1);
}
}}

}


{

gdjs.Fase4Code.GDGoRightObjects1.createFrom(runtimeScene.getObjects("GoRight"));
gdjs.Fase4Code.GDMovingPlatformObjects1.createFrom(runtimeScene.getObjects("MovingPlatform"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("GoRight", gdjs.Fase4Code.GDGoRightObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("MovingPlatform", gdjs.Fase4Code.GDMovingPlatformObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDMovingPlatformObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.Fase4Code.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDMovingPlatformObjects1[i].addForce(150, 0, 1);
}
}}

}


{



}


{



}


{

gdjs.Fase4Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects1[i].activateBehavior("PlatformerObject", false);
}
}}

}


{



}


{

gdjs.Fase4Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.Fase4Code.GDGoLeftObjects1.createFrom(runtimeScene.getObjects("GoLeft"));
gdjs.Fase4Code.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("GoLeft", gdjs.Fase4Code.GDGoLeftObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("SlimeWalk", gdjs.Fase4Code.GDSlimeWalkObjects1).addObjectsToEventsMap("Fly", gdjs.Fase4Code.GDFlyObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects1[i].setVariableNumber(gdjs.Fase4Code.GDSlimeWalkObjects1[i].getVariables().get("GoingLeft"), 1);
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects1[i].setVariableNumber(gdjs.Fase4Code.GDFlyObjects1[i].getVariables().get("GoingLeft"), 1);
}
}}

}


{

gdjs.Fase4Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.Fase4Code.GDGoRightObjects1.createFrom(runtimeScene.getObjects("GoRight"));
gdjs.Fase4Code.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("GoRight", gdjs.Fase4Code.GDGoRightObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("SlimeWalk", gdjs.Fase4Code.GDSlimeWalkObjects1).addObjectsToEventsMap("Fly", gdjs.Fase4Code.GDFlyObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects1[i].setVariableNumber(gdjs.Fase4Code.GDSlimeWalkObjects1[i].getVariables().get("GoingLeft"), 0);
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects1[i].setVariableNumber(gdjs.Fase4Code.GDFlyObjects1[i].getVariables().get("GoingLeft"), 0);
}
}}

}


{

gdjs.Fase4Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.Fase4Code.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDSlimeWalkObjects1[i].getAnimation() == 0 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDSlimeWalkObjects1[k] = gdjs.Fase4Code.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase4Code.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDFlyObjects1[i].getAnimation() == 0 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDFlyObjects1[k] = gdjs.Fase4Code.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDFlyObjects1.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {

{ //Subevents

{

gdjs.Fase4Code.GDFlyObjects2.createFrom(gdjs.Fase4Code.GDFlyObjects1);
gdjs.Fase4Code.GDSlimeWalkObjects2.createFrom(gdjs.Fase4Code.GDSlimeWalkObjects1);

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDSlimeWalkObjects2.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDSlimeWalkObjects2[i].getVariableNumber(gdjs.Fase4Code.GDSlimeWalkObjects2[i].getVariables().get("GoingLeft")) == 1 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDSlimeWalkObjects2[k] = gdjs.Fase4Code.GDSlimeWalkObjects2[i];
        ++k;
    }
}
gdjs.Fase4Code.GDSlimeWalkObjects2.length = k;for(var i = 0, k = 0, l = gdjs.Fase4Code.GDFlyObjects2.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDFlyObjects2[i].getVariableNumber(gdjs.Fase4Code.GDFlyObjects2[i].getVariables().get("GoingLeft")) == 1 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDFlyObjects2[k] = gdjs.Fase4Code.GDFlyObjects2[i];
        ++k;
    }
}
gdjs.Fase4Code.GDFlyObjects2.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects2[i].addForce(-300, 0, 0);
}
}{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects2[i].getBehavior("PlatformerObject").simulateLeftKey();
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects2[i].getBehavior("PlatformerObject").simulateLeftKey();
}
}{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects2[i].flipX(false);
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects2[i].flipX(false);
}
}}

}


{

gdjs.Fase4Code.GDFlyObjects2.createFrom(gdjs.Fase4Code.GDFlyObjects1);
gdjs.Fase4Code.GDSlimeWalkObjects2.createFrom(gdjs.Fase4Code.GDSlimeWalkObjects1);

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDSlimeWalkObjects2.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDSlimeWalkObjects2[i].getVariableNumber(gdjs.Fase4Code.GDSlimeWalkObjects2[i].getVariables().get("GoingLeft")) == 0 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDSlimeWalkObjects2[k] = gdjs.Fase4Code.GDSlimeWalkObjects2[i];
        ++k;
    }
}
gdjs.Fase4Code.GDSlimeWalkObjects2.length = k;for(var i = 0, k = 0, l = gdjs.Fase4Code.GDFlyObjects2.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDFlyObjects2[i].getVariableNumber(gdjs.Fase4Code.GDFlyObjects2[i].getVariables().get("GoingLeft")) == 0 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDFlyObjects2[k] = gdjs.Fase4Code.GDFlyObjects2[i];
        ++k;
    }
}
gdjs.Fase4Code.GDFlyObjects2.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects2[i].addForce(300, 0, 0);
}
}{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects2[i].getBehavior("PlatformerObject").simulateRightKey();
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects2[i].getBehavior("PlatformerObject").simulateRightKey();
}
}{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects2[i].flipX(true);
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects2[i].flipX(true);
}
}}

}


{



}


{

gdjs.Fase4Code.GDFlyObjects2.createFrom(gdjs.Fase4Code.GDFlyObjects1);
gdjs.Fase4Code.GDPlayerHitBoxObjects2.createFrom(runtimeScene.getObjects("PlayerHitBox"));
gdjs.Fase4Code.GDSlimeWalkObjects2.createFrom(gdjs.Fase4Code.GDSlimeWalkObjects1);

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("PlayerHitBox", gdjs.Fase4Code.GDPlayerHitBoxObjects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("SlimeWalk", gdjs.Fase4Code.GDSlimeWalkObjects2).addObjectsToEventsMap("Fly", gdjs.Fase4Code.GDFlyObjects2).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {

{ //Subevents

{

gdjs.Fase4Code.GDFlyObjects3.createFrom(gdjs.Fase4Code.GDFlyObjects2);
gdjs.Fase4Code.GDPlayerHitBoxObjects3.createFrom(gdjs.Fase4Code.GDPlayerHitBoxObjects2);
gdjs.Fase4Code.GDSlimeWalkObjects3.createFrom(gdjs.Fase4Code.GDSlimeWalkObjects2);

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDPlayerHitBoxObjects3.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDPlayerHitBoxObjects3[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDPlayerHitBoxObjects3[k] = gdjs.Fase4Code.GDPlayerHitBoxObjects3[i];
        ++k;
    }
}
gdjs.Fase4Code.GDPlayerHitBoxObjects3.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects3.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects3[i].setAnimation(1);
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects3.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects3[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects3.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects3[i].activateBehavior("PlatformerObject", true);
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects3.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects3[i].activateBehavior("PlatformerObject", true);
}
}{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects3.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects3[i].getBehavior("PlatformerObject").setGravity(1500);
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects3.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects3[i].getBehavior("PlatformerObject").setGravity(1500);
}
}{for(var i = 0, len = gdjs.Fase4Code.GDPlayerHitBoxObjects3.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerHitBoxObjects3[i].getBehavior("PlatformerObject").setCanJump();
}
}{for(var i = 0, len = gdjs.Fase4Code.GDPlayerHitBoxObjects3.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerHitBoxObjects3[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}
{ //Subevents

{

gdjs.Fase4Code.GDFlyObjects4.createFrom(gdjs.Fase4Code.GDFlyObjects3);
gdjs.Fase4Code.GDSlimeWalkObjects4.createFrom(gdjs.Fase4Code.GDSlimeWalkObjects3);

gdjs.Fase4Code.forEachTotalCount5 = 0;
gdjs.Fase4Code.forEachObjects5.length = 0;
gdjs.Fase4Code.forEachCount0_5 = gdjs.Fase4Code.GDSlimeWalkObjects4.length;
gdjs.Fase4Code.forEachTotalCount5 += gdjs.Fase4Code.forEachCount0_5;
gdjs.Fase4Code.forEachObjects5.push.apply(gdjs.Fase4Code.forEachObjects5,gdjs.Fase4Code.GDSlimeWalkObjects4);
gdjs.Fase4Code.forEachCount1_5 = gdjs.Fase4Code.GDFlyObjects4.length;
gdjs.Fase4Code.forEachTotalCount5 += gdjs.Fase4Code.forEachCount1_5;
gdjs.Fase4Code.forEachObjects5.push.apply(gdjs.Fase4Code.forEachObjects5,gdjs.Fase4Code.GDFlyObjects4);
for(gdjs.Fase4Code.forEachIndex5 = 0;gdjs.Fase4Code.forEachIndex5 < gdjs.Fase4Code.forEachTotalCount5;++gdjs.Fase4Code.forEachIndex5) {
gdjs.Fase4Code.GDFlyObjects5.createFrom(gdjs.Fase4Code.GDFlyObjects4);
gdjs.Fase4Code.GDSlimeWalkObjects5.createFrom(gdjs.Fase4Code.GDSlimeWalkObjects4);

gdjs.Fase4Code.GDSlimeWalkObjects5.length = 0;
gdjs.Fase4Code.GDFlyObjects5.length = 0;
if (gdjs.Fase4Code.forEachIndex5 < gdjs.Fase4Code.forEachCount0_5) {
    gdjs.Fase4Code.GDSlimeWalkObjects5.push(gdjs.Fase4Code.forEachObjects5[gdjs.Fase4Code.forEachIndex5]);
}
else if (gdjs.Fase4Code.forEachIndex5 < gdjs.Fase4Code.forEachCount0_5+gdjs.Fase4Code.forEachCount1_5) {
    gdjs.Fase4Code.GDFlyObjects5.push(gdjs.Fase4Code.forEachObjects5[gdjs.Fase4Code.forEachIndex5]);
}
if (true) {
{runtimeScene.getVariables().get("Score").add(0);
}}
}

}

} //End of subevents
}

}


{

gdjs.Fase4Code.GDPlayerHitBoxObjects3.createFrom(gdjs.Fase4Code.GDPlayerHitBoxObjects2);

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDPlayerHitBoxObjects3.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDPlayerHitBoxObjects3[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDPlayerHitBoxObjects3[k] = gdjs.Fase4Code.GDPlayerHitBoxObjects3[i];
        ++k;
    }
}
gdjs.Fase4Code.GDPlayerHitBoxObjects3.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).sub(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Fase" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)), false);
}}

}


{

gdjs.Fase4Code.GDFlyObjects3.createFrom(gdjs.Fase4Code.GDFlyObjects2);
gdjs.Fase4Code.GDPlayerHitBoxObjects3.createFrom(gdjs.Fase4Code.GDPlayerHitBoxObjects2);
gdjs.Fase4Code.GDSlimeWalkObjects3.createFrom(gdjs.Fase4Code.GDSlimeWalkObjects2);

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDPlayerHitBoxObjects3.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDPlayerHitBoxObjects3[i].getY() >= (( gdjs.Fase4Code.GDFlyObjects3.length === 0 ) ? (( gdjs.Fase4Code.GDSlimeWalkObjects3.length === 0 ) ? 0 :gdjs.Fase4Code.GDSlimeWalkObjects3[0].getPointY("")) :gdjs.Fase4Code.GDFlyObjects3[0].getPointY(""))-(gdjs.Fase4Code.GDPlayerHitBoxObjects3[i].getHeight())+(( gdjs.Fase4Code.GDFlyObjects3.length === 0 ) ? (( gdjs.Fase4Code.GDSlimeWalkObjects3.length === 0 ) ? 0 :gdjs.Fase4Code.GDSlimeWalkObjects3[0].getHeight()) :gdjs.Fase4Code.GDFlyObjects3[0].getHeight())/2 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDPlayerHitBoxObjects3[k] = gdjs.Fase4Code.GDPlayerHitBoxObjects3[i];
        ++k;
    }
}
gdjs.Fase4Code.GDPlayerHitBoxObjects3.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
}

}

} //End of subevents
}

}

} //End of subevents
}

}


{



}


{

gdjs.Fase4Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.Fase4Code.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
gdjs.Fase4Code.condition1IsTrue_0.val = false;
gdjs.Fase4Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDSlimeWalkObjects1[i].getAnimation() == 1 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDSlimeWalkObjects1[k] = gdjs.Fase4Code.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase4Code.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDFlyObjects1[i].getAnimation() == 1 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDFlyObjects1[k] = gdjs.Fase4Code.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDFlyObjects1.length = k;}if ( gdjs.Fase4Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDSlimeWalkObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase4Code.condition1IsTrue_0.val = true;
        gdjs.Fase4Code.GDSlimeWalkObjects1[k] = gdjs.Fase4Code.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase4Code.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDFlyObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Fase4Code.condition1IsTrue_0.val = true;
        gdjs.Fase4Code.GDFlyObjects1[k] = gdjs.Fase4Code.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDFlyObjects1.length = k;}if ( gdjs.Fase4Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase4Code.GDSlimeWalkObjects1[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Fase4Code.condition2IsTrue_0.val = true;
        gdjs.Fase4Code.GDSlimeWalkObjects1[k] = gdjs.Fase4Code.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Fase4Code.GDFlyObjects1.length;i<l;++i) {
    if ( !(gdjs.Fase4Code.GDFlyObjects1[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Fase4Code.condition2IsTrue_0.val = true;
        gdjs.Fase4Code.GDFlyObjects1[k] = gdjs.Fase4Code.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDFlyObjects1.length = k;}}
}
if (gdjs.Fase4Code.condition2IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects1[i].activateBehavior("PlatformerObject", false);
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects1[i].activateBehavior("PlatformerObject", false);
}
}{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects1[i].setOpacity(gdjs.Fase4Code.GDSlimeWalkObjects1[i].getOpacity() - (50*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects1[i].setOpacity(gdjs.Fase4Code.GDFlyObjects1[i].getOpacity() - (50*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}
{ //Subevents

{

gdjs.Fase4Code.GDFlyObjects2.createFrom(gdjs.Fase4Code.GDFlyObjects1);
gdjs.Fase4Code.GDSlimeWalkObjects2.createFrom(gdjs.Fase4Code.GDSlimeWalkObjects1);

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDSlimeWalkObjects2.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDSlimeWalkObjects2[i].getOpacity() == 0 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDSlimeWalkObjects2[k] = gdjs.Fase4Code.GDSlimeWalkObjects2[i];
        ++k;
    }
}
gdjs.Fase4Code.GDSlimeWalkObjects2.length = k;for(var i = 0, k = 0, l = gdjs.Fase4Code.GDFlyObjects2.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDFlyObjects2[i].getOpacity() == 0 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDFlyObjects2[k] = gdjs.Fase4Code.GDFlyObjects2[i];
        ++k;
    }
}
gdjs.Fase4Code.GDFlyObjects2.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDSlimeWalkObjects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects2.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects2[i].deleteFromScene(runtimeScene);
}
}}

}

} //End of subevents
}

}


{

gdjs.Fase4Code.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.Fase4Code.GDTiledCastlePlatformObjects1.createFrom(runtimeScene.getObjects("TiledCastlePlatform"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("Fly", gdjs.Fase4Code.GDFlyObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("TiledCastlePlatform", gdjs.Fase4Code.GDTiledCastlePlatformObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDFlyObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDFlyObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.Fase4Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDPlayerObjects1[i].getY() > gdjs.evtTools.window.getCanvasHeight(runtimeScene) ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDPlayerObjects1[k] = gdjs.Fase4Code.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDPlayerObjects1.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).sub(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Fase" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)), false);
}}

}


{



}


{



}


{

gdjs.Fase4Code.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));
gdjs.Fase4Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
gdjs.Fase4Code.condition1IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("PlayerHitBox", gdjs.Fase4Code.GDPlayerHitBoxObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("Coin", gdjs.Fase4Code.GDCoinObjects1).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.Fase4Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDCoinObjects1[i].getOpacity() == 255 ) {
        gdjs.Fase4Code.condition1IsTrue_0.val = true;
        gdjs.Fase4Code.GDCoinObjects1[k] = gdjs.Fase4Code.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDCoinObjects1.length = k;}}
if (gdjs.Fase4Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDCoinObjects1[i].setOpacity(254);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "coin.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).add((gdjs.RuntimeObject.getVariableNumber(((gdjs.Fase4Code.GDCoinObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase4Code.GDCoinObjects1[0].getVariables()).getFromIndex(0))));
}{runtimeScene.getGame().getVariables().getFromIndex(2).add((gdjs.RuntimeObject.getVariableNumber(((gdjs.Fase4Code.GDCoinObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Fase4Code.GDCoinObjects1[0].getVariables()).getFromIndex(0))));
}}

}


{

gdjs.Fase4Code.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDCoinObjects1[i].getOpacity() < 255 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDCoinObjects1[k] = gdjs.Fase4Code.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDCoinObjects1.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDCoinObjects1[i].setOpacity(gdjs.Fase4Code.GDCoinObjects1[i].getOpacity() - (255*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}{for(var i = 0, len = gdjs.Fase4Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDCoinObjects1[i].addForce(0, -30, 0);
}
}}

}


{

gdjs.Fase4Code.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDCoinObjects1[i].getOpacity() == 0 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDCoinObjects1[k] = gdjs.Fase4Code.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDCoinObjects1.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDCoinObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{



}


{

gdjs.Fase4Code.GDScoreObjects1.createFrom(runtimeScene.getObjects("Score"));

{for(var i = 0, len = gdjs.Fase4Code.GDScoreObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDScoreObjects1[i].setString("$ "+gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}
}


{



}


{

gdjs.Fase4Code.GDArrowButtonsBgObjects1.createFrom(runtimeScene.getObjects("ArrowButtonsBg"));
gdjs.Fase4Code.GDJumpButtonObjects1.createFrom(runtimeScene.getObjects("JumpButton"));
gdjs.Fase4Code.GDLeftButtonObjects1.createFrom(runtimeScene.getObjects("LeftButton"));
gdjs.Fase4Code.GDRightButtonObjects1.createFrom(runtimeScene.getObjects("RightButton"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.input.anyKeyPressed(runtimeScene);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDLeftButtonObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDLeftButtonObjects1[i].hide();
}
for(var i = 0, len = gdjs.Fase4Code.GDRightButtonObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDRightButtonObjects1[i].hide();
}
for(var i = 0, len = gdjs.Fase4Code.GDJumpButtonObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDJumpButtonObjects1[i].hide();
}
for(var i = 0, len = gdjs.Fase4Code.GDArrowButtonsBgObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDArrowButtonsBgObjects1[i].hide();
}
}}

}


{


gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{gdjs.evtTools.input.touchSimulateMouse(runtimeScene, false);
}}

}


{

gdjs.Fase4Code.GDLeftButtonObjects1.createFrom(runtimeScene.getObjects("LeftButton"));
gdjs.Fase4Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("LeftButton", gdjs.Fase4Code.GDLeftButtonObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateLeftKey();
}
}}

}


{

gdjs.Fase4Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
gdjs.Fase4Code.GDRightButtonObjects1.createFrom(runtimeScene.getObjects("RightButton"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("RightButton", gdjs.Fase4Code.GDRightButtonObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateRightKey();
}
}}

}


{

gdjs.Fase4Code.GDJumpButtonObjects1.createFrom(runtimeScene.getObjects("JumpButton"));
gdjs.Fase4Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("JumpButton", gdjs.Fase4Code.GDJumpButtonObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{



}


{

gdjs.Fase4Code.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));

{for(var i = 0, len = gdjs.Fase4Code.GDfaseObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDfaseObjects1[i].setCharacterSize(gdjs.Fase4Code.GDfaseObjects1[i].getCharacterSize() + (0.1));
}
}{for(var i = 0, len = gdjs.Fase4Code.GDfaseObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDfaseObjects1[i].addForce(0, -220, 0);
}
}
}


{

gdjs.Fase4Code.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Fase4Code.GDfaseObjects1.length;i<l;++i) {
    if ( gdjs.Fase4Code.GDfaseObjects1[i].getOpacity() <= 0 ) {
        gdjs.Fase4Code.condition0IsTrue_0.val = true;
        gdjs.Fase4Code.GDfaseObjects1[k] = gdjs.Fase4Code.GDfaseObjects1[i];
        ++k;
    }
}
gdjs.Fase4Code.GDfaseObjects1.length = k;}if (gdjs.Fase4Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Fase4Code.GDfaseObjects1.length ;i < len;++i) {
    gdjs.Fase4Code.GDfaseObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{

gdjs.Fase4Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.Fase4Code.GDlockObjects1.createFrom(runtimeScene.getObjects("lock"));

gdjs.Fase4Code.condition0IsTrue_0.val = false;
gdjs.Fase4Code.condition1IsTrue_0.val = false;
{
gdjs.Fase4Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("Player", gdjs.Fase4Code.GDPlayerObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("lock", gdjs.Fase4Code.GDlockObjects1).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.Fase4Code.condition0IsTrue_0.val ) {
{
{gdjs.Fase4Code.conditionTrue_1 = gdjs.Fase4Code.condition1IsTrue_0;
gdjs.Fase4Code.condition0IsTrue_1.val = false;
{
gdjs.Fase4Code.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) >= 25;
}gdjs.Fase4Code.conditionTrue_1.val = true && gdjs.Fase4Code.condition0IsTrue_1.val;
}
}}
if (gdjs.Fase4Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Chave4", false);
}}

}

return;
}
gdjs['Fase4Code']= gdjs.Fase4Code;
