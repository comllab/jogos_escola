gdjs.Chave3Code = {};
gdjs.Chave3Code.GDPlayerObjects1_1final = [];



gdjs.Chave3Code.GDdoorObjects1= [];
gdjs.Chave3Code.GDdoorObjects2= [];
gdjs.Chave3Code.GDkeyObjects1= [];
gdjs.Chave3Code.GDkeyObjects2= [];
gdjs.Chave3Code.GDPlatformObjects1= [];
gdjs.Chave3Code.GDPlatformObjects2= [];
gdjs.Chave3Code.GDbigCoinObjects1= [];
gdjs.Chave3Code.GDbigCoinObjects2= [];
gdjs.Chave3Code.GDlockObjects1= [];
gdjs.Chave3Code.GDlockObjects2= [];
gdjs.Chave3Code.GDgroundObjects1= [];
gdjs.Chave3Code.GDgroundObjects2= [];
gdjs.Chave3Code.GDPlayerObjects1= [];
gdjs.Chave3Code.GDPlayerObjects2= [];
gdjs.Chave3Code.GDPlayerHitBoxObjects1= [];
gdjs.Chave3Code.GDPlayerHitBoxObjects2= [];
gdjs.Chave3Code.GDkeyCostObjects1= [];
gdjs.Chave3Code.GDkeyCostObjects2= [];
gdjs.Chave3Code.GDCoinObjects1= [];
gdjs.Chave3Code.GDCoinObjects2= [];
gdjs.Chave3Code.GDmenosObjects1= [];
gdjs.Chave3Code.GDmenosObjects2= [];
gdjs.Chave3Code.GDigualObjects1= [];
gdjs.Chave3Code.GDigualObjects2= [];
gdjs.Chave3Code.GDvalorCoin2Objects1= [];
gdjs.Chave3Code.GDvalorCoin2Objects2= [];
gdjs.Chave3Code.GDvalorCoin3Objects1= [];
gdjs.Chave3Code.GDvalorCoin3Objects2= [];
gdjs.Chave3Code.GDcloseObjects1= [];
gdjs.Chave3Code.GDcloseObjects2= [];
gdjs.Chave3Code.GDsairObjects1= [];
gdjs.Chave3Code.GDsairObjects2= [];
gdjs.Chave3Code.GDerro_95subtracaoObjects1= [];
gdjs.Chave3Code.GDerro_95subtracaoObjects2= [];
gdjs.Chave3Code.GDresultadoObjects1= [];
gdjs.Chave3Code.GDresultadoObjects2= [];

gdjs.Chave3Code.conditionTrue_0 = {val:false};
gdjs.Chave3Code.condition0IsTrue_0 = {val:false};
gdjs.Chave3Code.condition1IsTrue_0 = {val:false};
gdjs.Chave3Code.condition2IsTrue_0 = {val:false};
gdjs.Chave3Code.condition3IsTrue_0 = {val:false};
gdjs.Chave3Code.conditionTrue_1 = {val:false};
gdjs.Chave3Code.condition0IsTrue_1 = {val:false};
gdjs.Chave3Code.condition1IsTrue_1 = {val:false};
gdjs.Chave3Code.condition2IsTrue_1 = {val:false};
gdjs.Chave3Code.condition3IsTrue_1 = {val:false};

gdjs.Chave3Code.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.Chave3Code.GDdoorObjects1.length = 0;
gdjs.Chave3Code.GDdoorObjects2.length = 0;
gdjs.Chave3Code.GDkeyObjects1.length = 0;
gdjs.Chave3Code.GDkeyObjects2.length = 0;
gdjs.Chave3Code.GDPlatformObjects1.length = 0;
gdjs.Chave3Code.GDPlatformObjects2.length = 0;
gdjs.Chave3Code.GDbigCoinObjects1.length = 0;
gdjs.Chave3Code.GDbigCoinObjects2.length = 0;
gdjs.Chave3Code.GDlockObjects1.length = 0;
gdjs.Chave3Code.GDlockObjects2.length = 0;
gdjs.Chave3Code.GDgroundObjects1.length = 0;
gdjs.Chave3Code.GDgroundObjects2.length = 0;
gdjs.Chave3Code.GDPlayerObjects1.length = 0;
gdjs.Chave3Code.GDPlayerObjects2.length = 0;
gdjs.Chave3Code.GDPlayerHitBoxObjects1.length = 0;
gdjs.Chave3Code.GDPlayerHitBoxObjects2.length = 0;
gdjs.Chave3Code.GDkeyCostObjects1.length = 0;
gdjs.Chave3Code.GDkeyCostObjects2.length = 0;
gdjs.Chave3Code.GDCoinObjects1.length = 0;
gdjs.Chave3Code.GDCoinObjects2.length = 0;
gdjs.Chave3Code.GDmenosObjects1.length = 0;
gdjs.Chave3Code.GDmenosObjects2.length = 0;
gdjs.Chave3Code.GDigualObjects1.length = 0;
gdjs.Chave3Code.GDigualObjects2.length = 0;
gdjs.Chave3Code.GDvalorCoin2Objects1.length = 0;
gdjs.Chave3Code.GDvalorCoin2Objects2.length = 0;
gdjs.Chave3Code.GDvalorCoin3Objects1.length = 0;
gdjs.Chave3Code.GDvalorCoin3Objects2.length = 0;
gdjs.Chave3Code.GDcloseObjects1.length = 0;
gdjs.Chave3Code.GDcloseObjects2.length = 0;
gdjs.Chave3Code.GDsairObjects1.length = 0;
gdjs.Chave3Code.GDsairObjects2.length = 0;
gdjs.Chave3Code.GDerro_95subtracaoObjects1.length = 0;
gdjs.Chave3Code.GDerro_95subtracaoObjects2.length = 0;
gdjs.Chave3Code.GDresultadoObjects1.length = 0;
gdjs.Chave3Code.GDresultadoObjects2.length = 0;


{

gdjs.Chave3Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
gdjs.Chave3Code.GDsairObjects1.createFrom(runtimeScene.getObjects("sair"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
gdjs.Chave3Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Chave3Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDPlayerHitBoxObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.Chave3Code.GDsairObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDsairObjects1[i].hide();
}
}{runtimeScene.getVariables().getFromIndex(5).setNumber(0);
}{gdjs.evtTools.sound.playMusic(runtimeScene, "Mushroom Theme.ogg", true, 100, 1);
}}

}


{



}


{

gdjs.Chave3Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.Chave3Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

{for(var i = 0, len = gdjs.Chave3Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDPlayerObjects1[i].setPosition((( gdjs.Chave3Code.GDPlayerHitBoxObjects1.length === 0 ) ? 0 :gdjs.Chave3Code.GDPlayerHitBoxObjects1[0].getPointX(""))-12,(( gdjs.Chave3Code.GDPlayerHitBoxObjects1.length === 0 ) ? 0 :gdjs.Chave3Code.GDPlayerHitBoxObjects1[0].getPointY("")));
}
}
}


{

gdjs.Chave3Code.GDPlayerObjects1.length = 0;

gdjs.Chave3Code.condition0IsTrue_0.val = false;
gdjs.Chave3Code.condition1IsTrue_0.val = false;
{
gdjs.Chave3Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "RShift");
}if ( gdjs.Chave3Code.condition0IsTrue_0.val ) {
{
{gdjs.Chave3Code.conditionTrue_1 = gdjs.Chave3Code.condition1IsTrue_0;
gdjs.Chave3Code.GDPlayerObjects1_1final.length = 0;gdjs.Chave3Code.condition0IsTrue_1.val = false;
gdjs.Chave3Code.condition1IsTrue_1.val = false;
{
gdjs.Chave3Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDPlayerObjects2[i].getAnimation() == 0 ) {
        gdjs.Chave3Code.condition0IsTrue_1.val = true;
        gdjs.Chave3Code.GDPlayerObjects2[k] = gdjs.Chave3Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Chave3Code.GDPlayerObjects2.length = k;if( gdjs.Chave3Code.condition0IsTrue_1.val ) {
    gdjs.Chave3Code.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.Chave3Code.GDPlayerObjects2.length;j<jLen;++j) {
        if ( gdjs.Chave3Code.GDPlayerObjects1_1final.indexOf(gdjs.Chave3Code.GDPlayerObjects2[j]) === -1 )
            gdjs.Chave3Code.GDPlayerObjects1_1final.push(gdjs.Chave3Code.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.Chave3Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDPlayerObjects2[i].getAnimation() == 2 ) {
        gdjs.Chave3Code.condition1IsTrue_1.val = true;
        gdjs.Chave3Code.GDPlayerObjects2[k] = gdjs.Chave3Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Chave3Code.GDPlayerObjects2.length = k;if( gdjs.Chave3Code.condition1IsTrue_1.val ) {
    gdjs.Chave3Code.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.Chave3Code.GDPlayerObjects2.length;j<jLen;++j) {
        if ( gdjs.Chave3Code.GDPlayerObjects1_1final.indexOf(gdjs.Chave3Code.GDPlayerObjects2[j]) === -1 )
            gdjs.Chave3Code.GDPlayerObjects1_1final.push(gdjs.Chave3Code.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.Chave3Code.GDPlayerObjects1.createFrom(gdjs.Chave3Code.GDPlayerObjects1_1final);
}
}
}}
if (gdjs.Chave3Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}}

}


{

gdjs.Chave3Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
gdjs.Chave3Code.condition1IsTrue_0.val = false;
{
gdjs.Chave3Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.Chave3Code.condition0IsTrue_0.val ) {
{
{gdjs.Chave3Code.conditionTrue_1 = gdjs.Chave3Code.condition1IsTrue_0;
gdjs.Chave3Code.conditionTrue_1.val = context.triggerOnce(239483596);
}
}}
if (gdjs.Chave3Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}{for(var i = 0, len = gdjs.Chave3Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{

gdjs.Chave3Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.Chave3Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDPlayerHitBoxObjects1[k] = gdjs.Chave3Code.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDPlayerHitBoxObjects1.length = k;}if (gdjs.Chave3Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDPlayerObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.Chave3Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.Chave3Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDPlayerHitBoxObjects1[k] = gdjs.Chave3Code.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDPlayerHitBoxObjects1.length = k;}if (gdjs.Chave3Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDPlayerObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.Chave3Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDPlayerHitBoxObjects1[k] = gdjs.Chave3Code.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDPlayerHitBoxObjects1.length = k;}if (gdjs.Chave3Code.condition0IsTrue_0.val) {

{ //Subevents

{

gdjs.Chave3Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
gdjs.Chave3Code.GDPlayerHitBoxObjects2.createFrom(gdjs.Chave3Code.GDPlayerHitBoxObjects1);

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDPlayerHitBoxObjects2.length;i<l;++i) {
    if ( !(gdjs.Chave3Code.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDPlayerHitBoxObjects2[k] = gdjs.Chave3Code.GDPlayerHitBoxObjects2[i];
        ++k;
    }
}
gdjs.Chave3Code.GDPlayerHitBoxObjects2.length = k;}if (gdjs.Chave3Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Chave3Code.GDPlayerObjects2[i].setAnimation(0);
}
}}

}


{

gdjs.Chave3Code.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
gdjs.Chave3Code.GDPlayerHitBoxObjects2.createFrom(gdjs.Chave3Code.GDPlayerHitBoxObjects1);

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDPlayerHitBoxObjects2.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").isMoving() ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDPlayerHitBoxObjects2[k] = gdjs.Chave3Code.GDPlayerHitBoxObjects2[i];
        ++k;
    }
}
gdjs.Chave3Code.GDPlayerHitBoxObjects2.length = k;}if (gdjs.Chave3Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Chave3Code.GDPlayerObjects2[i].setAnimation(2);
}
}}

}

} //End of subevents
}

}


{

gdjs.Chave3Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
gdjs.Chave3Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if (gdjs.Chave3Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDPlayerObjects1[i].flipX(true);
}
}}

}


{

gdjs.Chave3Code.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
gdjs.Chave3Code.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if (gdjs.Chave3Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDPlayerObjects1[i].flipX(false);
}
}}

}


{



}


{

gdjs.Chave3Code.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));
gdjs.Chave3Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
gdjs.Chave3Code.GDresultadoObjects1.createFrom(runtimeScene.getObjects("resultado"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
gdjs.Chave3Code.condition1IsTrue_0.val = false;
gdjs.Chave3Code.condition2IsTrue_0.val = false;
{
gdjs.Chave3Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("PlayerHitBox", gdjs.Chave3Code.GDPlayerHitBoxObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("Coin", gdjs.Chave3Code.GDCoinObjects1).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.Chave3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDCoinObjects1[i].getOpacity() == 255 ) {
        gdjs.Chave3Code.condition1IsTrue_0.val = true;
        gdjs.Chave3Code.GDCoinObjects1[k] = gdjs.Chave3Code.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDCoinObjects1.length = k;}if ( gdjs.Chave3Code.condition1IsTrue_0.val ) {
{
{gdjs.Chave3Code.conditionTrue_1 = gdjs.Chave3Code.condition2IsTrue_0;
gdjs.Chave3Code.conditionTrue_1.val = context.triggerOnce(239505052);
}
}}
}
if (gdjs.Chave3Code.condition2IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDCoinObjects1[i].setOpacity(254);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "coin.wav", false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(5).add((gdjs.RuntimeObject.getVariableNumber(((gdjs.Chave3Code.GDCoinObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Chave3Code.GDCoinObjects1[0].getVariables()).getFromIndex(0))));
}{runtimeScene.getVariables().getFromIndex(1).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.Chave3Code.GDCoinObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Chave3Code.GDCoinObjects1[0].getVariables()).getFromIndex(0))));
}{runtimeScene.getVariables().getFromIndex(2).sub(1);
}{for(var i = 0, len = gdjs.Chave3Code.GDresultadoObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDresultadoObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(5)));
}
}}

}


{

gdjs.Chave3Code.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDCoinObjects1[i].getOpacity() < 255 ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDCoinObjects1[k] = gdjs.Chave3Code.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDCoinObjects1.length = k;}if (gdjs.Chave3Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDCoinObjects1[i].setOpacity(gdjs.Chave3Code.GDCoinObjects1[i].getOpacity() - (255*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}{for(var i = 0, len = gdjs.Chave3Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDCoinObjects1[i].addForce(0, -30, 0);
}
}}

}


{

gdjs.Chave3Code.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDCoinObjects1[i].getOpacity() == 0 ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDCoinObjects1[k] = gdjs.Chave3Code.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDCoinObjects1.length = k;}if (gdjs.Chave3Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDCoinObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDCoinObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{

gdjs.Chave3Code.GDkeyObjects1.createFrom(runtimeScene.getObjects("key"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
gdjs.Chave3Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDkeyObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDkeyObjects1[i].getVariableNumber(gdjs.Chave3Code.GDkeyObjects1[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDkeyObjects1[k] = gdjs.Chave3Code.GDkeyObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDkeyObjects1.length = k;}if ( gdjs.Chave3Code.condition0IsTrue_0.val ) {
{
gdjs.Chave3Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(5)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0));
}}
if (gdjs.Chave3Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDkeyObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDkeyObjects1[i].getBehavior("Physics").setDynamic(runtimeScene);
}
}{for(var i = 0, len = gdjs.Chave3Code.GDkeyObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDkeyObjects1[i].getBehavior("Physics").setGravity(0, 30, runtimeScene);
}
}{for(var i = 0, len = gdjs.Chave3Code.GDkeyObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDkeyObjects1[i].setVariableNumber(gdjs.Chave3Code.GDkeyObjects1[i].getVariables().getFromIndex(0), 1);
}
}}

}


{



}


{

gdjs.Chave3Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
gdjs.Chave3Code.GDcloseObjects1.createFrom(runtimeScene.getObjects("close"));
gdjs.Chave3Code.GDkeyObjects1.createFrom(runtimeScene.getObjects("key"));
gdjs.Chave3Code.GDlockObjects1.createFrom(runtimeScene.getObjects("lock"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
gdjs.Chave3Code.condition1IsTrue_0.val = false;
gdjs.Chave3Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDkeyObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDkeyObjects1[i].isVisible() ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDkeyObjects1[k] = gdjs.Chave3Code.GDkeyObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDkeyObjects1.length = k;}if ( gdjs.Chave3Code.condition0IsTrue_0.val ) {
{
gdjs.Chave3Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("key", gdjs.Chave3Code.GDkeyObjects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("lock", gdjs.Chave3Code.GDlockObjects1).getEventsObjectsMap(), false, runtimeScene);
}if ( gdjs.Chave3Code.condition1IsTrue_0.val ) {
{
{gdjs.Chave3Code.conditionTrue_1 = gdjs.Chave3Code.condition2IsTrue_0;
gdjs.Chave3Code.conditionTrue_1.val = context.triggerOnce(239501380);
}
}}
}
if (gdjs.Chave3Code.condition2IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDkeyObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDkeyObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.Chave3Code.GDlockObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDlockObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.Chave3Code.GDcloseObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDcloseObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.Chave3Code.GDcloseObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDcloseObjects1[i].setAnimationFrame(0);
}
}{for(var i = 0, len = gdjs.Chave3Code.GDcloseObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDcloseObjects1[i].playAnimation();
}
}{for(var i = 0, len = gdjs.Chave3Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").setJumpSpeed(0);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise06.ogg", false, 100, 1);
}}

}


{

gdjs.Chave3Code.GDcloseObjects1.createFrom(runtimeScene.getObjects("close"));
gdjs.Chave3Code.GDsairObjects1.createFrom(runtimeScene.getObjects("sair"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDcloseObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDcloseObjects1[i].hasAnimationEnded() ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDcloseObjects1[k] = gdjs.Chave3Code.GDcloseObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDcloseObjects1.length = k;}if (gdjs.Chave3Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDsairObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDsairObjects1[i].hide(false);
}
}}

}


{

gdjs.Chave3Code.GDsairObjects1.createFrom(runtimeScene.getObjects("sair"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
gdjs.Chave3Code.condition1IsTrue_0.val = false;
gdjs.Chave3Code.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDsairObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDsairObjects1[i].isVisible() ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDsairObjects1[k] = gdjs.Chave3Code.GDsairObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDsairObjects1.length = k;}if ( gdjs.Chave3Code.condition0IsTrue_0.val ) {
{
gdjs.Chave3Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(context.clearEventsObjectsMap().addObjectsToEventsMap("sair", gdjs.Chave3Code.GDsairObjects1).getEventsObjectsMap(), runtimeScene, true, false);
}if ( gdjs.Chave3Code.condition1IsTrue_0.val ) {
{
gdjs.Chave3Code.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
if (gdjs.Chave3Code.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).sub(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("valorChaveFase")));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(4)), false);
}}

}


{

gdjs.Chave3Code.GDsairObjects1.createFrom(runtimeScene.getObjects("sair"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
gdjs.Chave3Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDsairObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDsairObjects1[i].isVisible() ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDsairObjects1[k] = gdjs.Chave3Code.GDsairObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDsairObjects1.length = k;}if ( gdjs.Chave3Code.condition0IsTrue_0.val ) {
{
gdjs.Chave3Code.condition1IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 1, "passar_fase");
}}
if (gdjs.Chave3Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).sub(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("valorChaveFase")));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(4)), false);
}}

}


{



}


{

gdjs.Chave3Code.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
gdjs.Chave3Code.GDerro_95subtracaoObjects1.createFrom(runtimeScene.getObjects("erro_subtracao"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
gdjs.Chave3Code.condition1IsTrue_0.val = false;
{
gdjs.Chave3Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) <= 0;
}if ( gdjs.Chave3Code.condition0IsTrue_0.val ) {
{
{gdjs.Chave3Code.conditionTrue_1 = gdjs.Chave3Code.condition1IsTrue_0;
gdjs.Chave3Code.condition0IsTrue_1.val = false;
{
gdjs.Chave3Code.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(5)) != gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0));
}gdjs.Chave3Code.conditionTrue_1.val = true && gdjs.Chave3Code.condition0IsTrue_1.val;
}
}}
if (gdjs.Chave3Code.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.Chave3Code.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").setJumpSpeed(0);
}
}{for(var i = 0, len = gdjs.Chave3Code.GDerro_95subtracaoObjects1.length ;i < len;++i) {
    gdjs.Chave3Code.GDerro_95subtracaoObjects1[i].setY(gdjs.Chave3Code.GDerro_95subtracaoObjects1[i].getY() - (1));
}
}}

}


{

gdjs.Chave3Code.GDerro_95subtracaoObjects1.createFrom(runtimeScene.getObjects("erro_subtracao"));

gdjs.Chave3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Chave3Code.GDerro_95subtracaoObjects1.length;i<l;++i) {
    if ( gdjs.Chave3Code.GDerro_95subtracaoObjects1[i].getY() <= 0 ) {
        gdjs.Chave3Code.condition0IsTrue_0.val = true;
        gdjs.Chave3Code.GDerro_95subtracaoObjects1[k] = gdjs.Chave3Code.GDerro_95subtracaoObjects1[i];
        ++k;
    }
}
gdjs.Chave3Code.GDerro_95subtracaoObjects1.length = k;}if (gdjs.Chave3Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(3)), false);
}{runtimeScene.getGame().getVariables().getFromIndex(1).sub(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)));
}}

}

return;
}
gdjs['Chave3Code']= gdjs.Chave3Code;
