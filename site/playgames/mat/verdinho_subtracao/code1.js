gdjs.LevelCode = {};
gdjs.LevelCode.GDPlayerObjects1_1final = [];

gdjs.LevelCode.GDfadeObjects1= [];
gdjs.LevelCode.GDfadeObjects2= [];
gdjs.LevelCode.GDfadeObjects3= [];
gdjs.LevelCode.GDPlayerObjects1= [];
gdjs.LevelCode.GDPlayerObjects2= [];
gdjs.LevelCode.GDPlayerObjects3= [];
gdjs.LevelCode.GDJumpthruObjects1= [];
gdjs.LevelCode.GDJumpthruObjects2= [];
gdjs.LevelCode.GDJumpthruObjects3= [];
gdjs.LevelCode.GDPlatformObjects1= [];
gdjs.LevelCode.GDPlatformObjects2= [];
gdjs.LevelCode.GDPlatformObjects3= [];
gdjs.LevelCode.GDTiledGrassPlatformObjects1= [];
gdjs.LevelCode.GDTiledGrassPlatformObjects2= [];
gdjs.LevelCode.GDTiledGrassPlatformObjects3= [];
gdjs.LevelCode.GDTiledPlatformObjects1= [];
gdjs.LevelCode.GDTiledPlatformObjects2= [];
gdjs.LevelCode.GDTiledPlatformObjects3= [];
gdjs.LevelCode.GDTiledCastlePlatformObjects1= [];
gdjs.LevelCode.GDTiledCastlePlatformObjects2= [];
gdjs.LevelCode.GDTiledCastlePlatformObjects3= [];
gdjs.LevelCode.GDMovingPlatformObjects1= [];
gdjs.LevelCode.GDMovingPlatformObjects2= [];
gdjs.LevelCode.GDMovingPlatformObjects3= [];
gdjs.LevelCode.GDGoLeftObjects1= [];
gdjs.LevelCode.GDGoLeftObjects2= [];
gdjs.LevelCode.GDGoLeftObjects3= [];
gdjs.LevelCode.GDGoRightObjects1= [];
gdjs.LevelCode.GDGoRightObjects2= [];
gdjs.LevelCode.GDGoRightObjects3= [];
gdjs.LevelCode.GDLadderObjects1= [];
gdjs.LevelCode.GDLadderObjects2= [];
gdjs.LevelCode.GDLadderObjects3= [];
gdjs.LevelCode.GDPlayerHitBoxObjects1= [];
gdjs.LevelCode.GDPlayerHitBoxObjects2= [];
gdjs.LevelCode.GDPlayerHitBoxObjects3= [];
gdjs.LevelCode.GDSlimeWalkObjects1= [];
gdjs.LevelCode.GDSlimeWalkObjects2= [];
gdjs.LevelCode.GDSlimeWalkObjects3= [];
gdjs.LevelCode.GDFlyObjects1= [];
gdjs.LevelCode.GDFlyObjects2= [];
gdjs.LevelCode.GDFlyObjects3= [];
gdjs.LevelCode.GDCloudObjects1= [];
gdjs.LevelCode.GDCloudObjects2= [];
gdjs.LevelCode.GDCloudObjects3= [];
gdjs.LevelCode.GDBackgroundObjectsObjects1= [];
gdjs.LevelCode.GDBackgroundObjectsObjects2= [];
gdjs.LevelCode.GDBackgroundObjectsObjects3= [];
gdjs.LevelCode.GDScoreObjects1= [];
gdjs.LevelCode.GDScoreObjects2= [];
gdjs.LevelCode.GDScoreObjects3= [];
gdjs.LevelCode.GDCoinObjects1= [];
gdjs.LevelCode.GDCoinObjects2= [];
gdjs.LevelCode.GDCoinObjects3= [];
gdjs.LevelCode.GDCoinIconObjects1= [];
gdjs.LevelCode.GDCoinIconObjects2= [];
gdjs.LevelCode.GDCoinIconObjects3= [];
gdjs.LevelCode.GDLeftButtonObjects1= [];
gdjs.LevelCode.GDLeftButtonObjects2= [];
gdjs.LevelCode.GDLeftButtonObjects3= [];
gdjs.LevelCode.GDRightButtonObjects1= [];
gdjs.LevelCode.GDRightButtonObjects2= [];
gdjs.LevelCode.GDRightButtonObjects3= [];
gdjs.LevelCode.GDJumpButtonObjects1= [];
gdjs.LevelCode.GDJumpButtonObjects2= [];
gdjs.LevelCode.GDJumpButtonObjects3= [];
gdjs.LevelCode.GDArrowButtonsBgObjects1= [];
gdjs.LevelCode.GDArrowButtonsBgObjects2= [];
gdjs.LevelCode.GDArrowButtonsBgObjects3= [];
gdjs.LevelCode.GDtxtFaseObjects1= [];
gdjs.LevelCode.GDtxtFaseObjects2= [];
gdjs.LevelCode.GDtxtFaseObjects3= [];
gdjs.LevelCode.GDtxtNivelObjects1= [];
gdjs.LevelCode.GDtxtNivelObjects2= [];
gdjs.LevelCode.GDtxtNivelObjects3= [];
gdjs.LevelCode.GDpanelTopObjects1= [];
gdjs.LevelCode.GDpanelTopObjects2= [];
gdjs.LevelCode.GDpanelTopObjects3= [];
gdjs.LevelCode.GDcactusObjects1= [];
gdjs.LevelCode.GDcactusObjects2= [];
gdjs.LevelCode.GDcactusObjects3= [];
gdjs.LevelCode.GDexitObjects1= [];
gdjs.LevelCode.GDexitObjects2= [];
gdjs.LevelCode.GDexitObjects3= [];
gdjs.LevelCode.GDportaObjects1= [];
gdjs.LevelCode.GDportaObjects2= [];
gdjs.LevelCode.GDportaObjects3= [];
gdjs.LevelCode.GDfaseObjects1= [];
gdjs.LevelCode.GDfaseObjects2= [];
gdjs.LevelCode.GDfaseObjects3= [];
gdjs.LevelCode.GDNewObjectObjects1= [];
gdjs.LevelCode.GDNewObjectObjects2= [];
gdjs.LevelCode.GDNewObjectObjects3= [];
gdjs.LevelCode.GDponteObjects1= [];
gdjs.LevelCode.GDponteObjects2= [];
gdjs.LevelCode.GDponteObjects3= [];

gdjs.LevelCode.conditionTrue_0 = {val:false};
gdjs.LevelCode.condition0IsTrue_0 = {val:false};
gdjs.LevelCode.condition1IsTrue_0 = {val:false};
gdjs.LevelCode.condition2IsTrue_0 = {val:false};
gdjs.LevelCode.condition3IsTrue_0 = {val:false};
gdjs.LevelCode.conditionTrue_1 = {val:false};
gdjs.LevelCode.condition0IsTrue_1 = {val:false};
gdjs.LevelCode.condition1IsTrue_1 = {val:false};
gdjs.LevelCode.condition2IsTrue_1 = {val:false};
gdjs.LevelCode.condition3IsTrue_1 = {val:false};


gdjs.LevelCode.eventsList0x69b0a0 = function(runtimeScene, context) {

{

gdjs.LevelCode.GDPlayerHitBoxObjects2.createFrom(gdjs.LevelCode.GDPlayerHitBoxObjects1);


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects2.length;i<l;++i) {
    if ( !(gdjs.LevelCode.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects2[k] = gdjs.LevelCode.GDPlayerHitBoxObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects2.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects2[i].setAnimation(0);
}
}}

}


{

/* Reuse gdjs.LevelCode.GDPlayerHitBoxObjects1 */

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isMoving() ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects1[k] = gdjs.LevelCode.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].setAnimation(2);
}
}}

}


}; //End of gdjs.LevelCode.eventsList0x69b0a0
gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDGoLeftObjects1Objects = Hashtable.newFrom({"GoLeft": gdjs.LevelCode.GDGoLeftObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDMovingPlatformObjects1Objects = Hashtable.newFrom({"MovingPlatform": gdjs.LevelCode.GDMovingPlatformObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDGoRightObjects1Objects = Hashtable.newFrom({"GoRight": gdjs.LevelCode.GDGoRightObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDMovingPlatformObjects1Objects = Hashtable.newFrom({"MovingPlatform": gdjs.LevelCode.GDMovingPlatformObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDGoLeftObjects1Objects = Hashtable.newFrom({"GoLeft": gdjs.LevelCode.GDGoLeftObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDSlimeWalkObjects1ObjectsGDgdjs_46LevelCode_46GDFlyObjects1Objects = Hashtable.newFrom({"SlimeWalk": gdjs.LevelCode.GDSlimeWalkObjects1, "Fly": gdjs.LevelCode.GDFlyObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDGoRightObjects1Objects = Hashtable.newFrom({"GoRight": gdjs.LevelCode.GDGoRightObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDSlimeWalkObjects1ObjectsGDgdjs_46LevelCode_46GDFlyObjects1Objects = Hashtable.newFrom({"SlimeWalk": gdjs.LevelCode.GDSlimeWalkObjects1, "Fly": gdjs.LevelCode.GDFlyObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDPlayerHitBoxObjects1Objects = Hashtable.newFrom({"PlayerHitBox": gdjs.LevelCode.GDPlayerHitBoxObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDSlimeWalkObjects1ObjectsGDgdjs_46LevelCode_46GDFlyObjects1Objects = Hashtable.newFrom({"SlimeWalk": gdjs.LevelCode.GDSlimeWalkObjects1, "Fly": gdjs.LevelCode.GDFlyObjects1});gdjs.LevelCode.eventsList0x6a1ba8 = function(runtimeScene, context) {

{

gdjs.LevelCode.GDPlayerHitBoxObjects2.createFrom(gdjs.LevelCode.GDPlayerHitBoxObjects1);


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects2[k] = gdjs.LevelCode.GDPlayerHitBoxObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects2.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDFlyObjects2.createFrom(gdjs.LevelCode.GDFlyObjects1);

/* Reuse gdjs.LevelCode.GDPlayerHitBoxObjects2 */
gdjs.LevelCode.GDSlimeWalkObjects2.createFrom(gdjs.LevelCode.GDSlimeWalkObjects1);

{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].setAnimation(1);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].activateBehavior("PlatformerObject", true);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].activateBehavior("PlatformerObject", true);
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].getBehavior("PlatformerObject").setGravity(1500);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].getBehavior("PlatformerObject").setGravity(1500);
}
}{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").setCanJump();
}
}{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects2[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}}

}


{

/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDPlayerHitBoxObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getY() >= (( gdjs.LevelCode.GDFlyObjects1.length === 0 ) ? (( gdjs.LevelCode.GDSlimeWalkObjects1.length === 0 ) ? 0 :gdjs.LevelCode.GDSlimeWalkObjects1[0].getPointY("")) :gdjs.LevelCode.GDFlyObjects1[0].getPointY(""))-(gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getHeight())+(( gdjs.LevelCode.GDFlyObjects1.length === 0 ) ? (( gdjs.LevelCode.GDSlimeWalkObjects1.length === 0 ) ? 0 :gdjs.LevelCode.GDSlimeWalkObjects1[0].getHeight()) :gdjs.LevelCode.GDFlyObjects1[0].getHeight())/2 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects1[k] = gdjs.LevelCode.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level", false);
}{runtimeScene.getGame().getVariables().getFromIndex(6).sub(10);
}}

}


}; //End of gdjs.LevelCode.eventsList0x6a1ba8
gdjs.LevelCode.eventsList0x6a1268 = function(runtimeScene, context) {

{

gdjs.LevelCode.GDFlyObjects2.createFrom(gdjs.LevelCode.GDFlyObjects1);

gdjs.LevelCode.GDSlimeWalkObjects2.createFrom(gdjs.LevelCode.GDSlimeWalkObjects1);


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects2[i].getVariableNumber(gdjs.LevelCode.GDSlimeWalkObjects2[i].getVariables().get("GoingLeft")) == 1 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDSlimeWalkObjects2[k] = gdjs.LevelCode.GDSlimeWalkObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects2.length = k;for(var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects2[i].getVariableNumber(gdjs.LevelCode.GDFlyObjects2[i].getVariables().get("GoingLeft")) == 1 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDFlyObjects2[k] = gdjs.LevelCode.GDFlyObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects2.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDFlyObjects2 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects2 */
{for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].addForce(-300, 0, 0);
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].getBehavior("PlatformerObject").simulateLeftKey();
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].getBehavior("PlatformerObject").simulateLeftKey();
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].flipX(false);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].flipX(false);
}
}}

}


{

gdjs.LevelCode.GDFlyObjects2.createFrom(gdjs.LevelCode.GDFlyObjects1);

gdjs.LevelCode.GDSlimeWalkObjects2.createFrom(gdjs.LevelCode.GDSlimeWalkObjects1);


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects2[i].getVariableNumber(gdjs.LevelCode.GDSlimeWalkObjects2[i].getVariables().get("GoingLeft")) == 0 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDSlimeWalkObjects2[k] = gdjs.LevelCode.GDSlimeWalkObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects2.length = k;for(var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects2[i].getVariableNumber(gdjs.LevelCode.GDFlyObjects2[i].getVariables().get("GoingLeft")) == 0 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDFlyObjects2[k] = gdjs.LevelCode.GDFlyObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects2.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDFlyObjects2 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects2 */
{for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].addForce(300, 0, 0);
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].getBehavior("PlatformerObject").simulateRightKey();
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].getBehavior("PlatformerObject").simulateRightKey();
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects2[i].flipX(true);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects2.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects2[i].flipX(true);
}
}}

}


{



}


{

/* Reuse gdjs.LevelCode.GDFlyObjects1 */
gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDPlayerHitBoxObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDSlimeWalkObjects1ObjectsGDgdjs_46LevelCode_46GDFlyObjects1Objects, false, runtimeScene);
}if (gdjs.LevelCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.LevelCode.eventsList0x6a1ba8(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.LevelCode.eventsList0x6a1268
gdjs.LevelCode.eventsList0x6a2870 = function(runtimeScene, context) {

{

/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects1[i].getOpacity() == 0 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDSlimeWalkObjects1[k] = gdjs.LevelCode.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects1[i].getOpacity() == 0 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDFlyObjects1[k] = gdjs.LevelCode.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.LevelCode.eventsList0x6a2870
gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDfaseObjects1Objects = Hashtable.newFrom({"fase": gdjs.LevelCode.GDfaseObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDPlayerHitBoxObjects1Objects = Hashtable.newFrom({"PlayerHitBox": gdjs.LevelCode.GDPlayerHitBoxObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDCoinObjects1Objects = Hashtable.newFrom({"Coin": gdjs.LevelCode.GDCoinObjects1});gdjs.LevelCode.eventsList0x6a31e0 = function(runtimeScene, context) {

{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 3;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDCoinObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDCoinObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDCoinObjects1[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(1);
}{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "Operacao");
}}

}


}; //End of gdjs.LevelCode.eventsList0x6a31e0
gdjs.LevelCode.eventsList0x69cd10 = function(runtimeScene, context) {

{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = !(gdjs.evtTools.systemInfo.isMobile());
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDArrowButtonsBgObjects1.createFrom(runtimeScene.getObjects("ArrowButtonsBg"));
gdjs.LevelCode.GDJumpButtonObjects1.createFrom(runtimeScene.getObjects("JumpButton"));
gdjs.LevelCode.GDLeftButtonObjects1.createFrom(runtimeScene.getObjects("LeftButton"));
gdjs.LevelCode.GDRightButtonObjects1.createFrom(runtimeScene.getObjects("RightButton"));
{for(var i = 0, len = gdjs.LevelCode.GDLeftButtonObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDLeftButtonObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.LevelCode.GDRightButtonObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDRightButtonObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.LevelCode.GDJumpButtonObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDJumpButtonObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.LevelCode.GDArrowButtonsBgObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDArrowButtonsBgObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


}; //End of gdjs.LevelCode.eventsList0x69cd10
gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDLeftButtonObjects1Objects = Hashtable.newFrom({"LeftButton": gdjs.LevelCode.GDLeftButtonObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDRightButtonObjects1Objects = Hashtable.newFrom({"RightButton": gdjs.LevelCode.GDRightButtonObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDJumpButtonObjects1Objects = Hashtable.newFrom({"JumpButton": gdjs.LevelCode.GDJumpButtonObjects1});gdjs.LevelCode.eventsList0x69ddd8 = function(runtimeScene, context) {

{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 1;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(4) + 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(4) + 1);
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 2;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(9) + 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(9) + 1);
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 3;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(14) + 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(14) + 1);
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 4;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(49) + 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(49) + 1);
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 5;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(49) + 50);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(49) + 50);
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 6;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(400) + 100);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(400) + 100);
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 7;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(900) + 100);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(900) + 100);
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 8;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.random(8999) + 1000);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.random(8999) + 1000);
}}

}


{



}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1));
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)));
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)));
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(8)));
}}

}


}; //End of gdjs.LevelCode.eventsList0x69ddd8
gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.LevelCode.GDPlayerObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDexitObjects1Objects = Hashtable.newFrom({"exit": gdjs.LevelCode.GDexitObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.LevelCode.GDPlayerObjects2});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDportaObjects2Objects = Hashtable.newFrom({"porta": gdjs.LevelCode.GDportaObjects2});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.LevelCode.GDPlayerObjects1});gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDportaObjects1Objects = Hashtable.newFrom({"porta": gdjs.LevelCode.GDportaObjects1});gdjs.LevelCode.eventsList0x69fe90 = function(runtimeScene, context) {

{

gdjs.LevelCode.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
gdjs.LevelCode.GDportaObjects2.createFrom(runtimeScene.getObjects("porta"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDPlayerObjects2Objects, gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDportaObjects2Objects, false, runtimeScene);
}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
gdjs.LevelCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 6;
}}
if (gdjs.LevelCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "CompletouNivel", false);
}}

}


{

gdjs.LevelCode.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.LevelCode.GDportaObjects1.createFrom(runtimeScene.getObjects("porta"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDPlayerObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDportaObjects1Objects, false, runtimeScene);
}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
gdjs.LevelCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) < 6;
}}
if (gdjs.LevelCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(4).add(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level", false);
}}

}


}; //End of gdjs.LevelCode.eventsList0x69fe90
gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDfadeObjects1Objects = Hashtable.newFrom({"fade": gdjs.LevelCode.GDfadeObjects1});gdjs.LevelCode.eventsList0xa86e0 = function(runtimeScene, context) {

{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
gdjs.LevelCode.GDtxtNivelObjects1.createFrom(runtimeScene.getObjects("txtNivel"));
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.LevelCode.GDtxtNivelObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDtxtNivelObjects1[i].setString("Nível " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(5)));
}
}{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "Fase" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)), 0, 0);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Intro Theme.ogg", 1, true, 100, 1);
}{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").setJumpSpeed(1000);
}
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").setJumpSpeed(0);
}
}}

}


{


{
gdjs.LevelCode.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].setPosition((( gdjs.LevelCode.GDPlayerHitBoxObjects1.length === 0 ) ? 0 :gdjs.LevelCode.GDPlayerHitBoxObjects1[0].getPointX(""))-12,(( gdjs.LevelCode.GDPlayerHitBoxObjects1.length === 0 ) ? 0 :gdjs.LevelCode.GDPlayerHitBoxObjects1[0].getPointY("")));
}
}}

}


{



}


{

gdjs.LevelCode.GDPlayerObjects1.length = 0;


gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
gdjs.LevelCode.condition2IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 0;
}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
gdjs.LevelCode.condition1IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.LevelCode.condition1IsTrue_0.val ) {
{
{gdjs.LevelCode.conditionTrue_1 = gdjs.LevelCode.condition2IsTrue_0;
gdjs.LevelCode.GDPlayerObjects1_1final.length = 0;gdjs.LevelCode.condition0IsTrue_1.val = false;
gdjs.LevelCode.condition1IsTrue_1.val = false;
{
gdjs.LevelCode.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
for(var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerObjects2[i].getAnimation() == 0 ) {
        gdjs.LevelCode.condition0IsTrue_1.val = true;
        gdjs.LevelCode.GDPlayerObjects2[k] = gdjs.LevelCode.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerObjects2.length = k;if( gdjs.LevelCode.condition0IsTrue_1.val ) {
    gdjs.LevelCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.LevelCode.GDPlayerObjects2.length;j<jLen;++j) {
        if ( gdjs.LevelCode.GDPlayerObjects1_1final.indexOf(gdjs.LevelCode.GDPlayerObjects2[j]) === -1 )
            gdjs.LevelCode.GDPlayerObjects1_1final.push(gdjs.LevelCode.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.LevelCode.GDPlayerObjects2.createFrom(runtimeScene.getObjects("Player"));
for(var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerObjects2[i].getAnimation() == 2 ) {
        gdjs.LevelCode.condition1IsTrue_1.val = true;
        gdjs.LevelCode.GDPlayerObjects2[k] = gdjs.LevelCode.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerObjects2.length = k;if( gdjs.LevelCode.condition1IsTrue_1.val ) {
    gdjs.LevelCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.LevelCode.GDPlayerObjects2.length;j<jLen;++j) {
        if ( gdjs.LevelCode.GDPlayerObjects1_1final.indexOf(gdjs.LevelCode.GDPlayerObjects2[j]) === -1 )
            gdjs.LevelCode.GDPlayerObjects1_1final.push(gdjs.LevelCode.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.LevelCode.GDPlayerObjects1.createFrom(gdjs.LevelCode.GDPlayerObjects1_1final);
}
}
}}
}
if (gdjs.LevelCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jump.wav", false, 100, 1);
}}

}


{

gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects1[k] = gdjs.LevelCode.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects1[k] = gdjs.LevelCode.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerHitBoxObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDPlayerHitBoxObjects1[k] = gdjs.LevelCode.GDPlayerHitBoxObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.LevelCode.eventsList0x69b0a0(runtimeScene, context);} //End of subevents
}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
gdjs.LevelCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 0;
}}
if (gdjs.LevelCode.condition1IsTrue_0.val) {
gdjs.LevelCode.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].flipX(true);
}
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 0;
}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
gdjs.LevelCode.condition1IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}}
if (gdjs.LevelCode.condition1IsTrue_0.val) {
gdjs.LevelCode.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerObjects1[i].flipX(false);
}
}}

}


{



}


{


{
gdjs.LevelCode.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.LevelCode.GDPlayerObjects1.length === 0 ) ? 0 :gdjs.LevelCode.GDPlayerObjects1[0].getPointX("")), "", 0);
}}

}


{



}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDGoLeftObjects1.createFrom(runtimeScene.getObjects("GoLeft"));
gdjs.LevelCode.GDGoRightObjects1.createFrom(runtimeScene.getObjects("GoRight"));
{for(var i = 0, len = gdjs.LevelCode.GDGoLeftObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDGoLeftObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.LevelCode.GDGoRightObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDGoRightObjects1[i].hide();
}
}}

}


{

gdjs.LevelCode.GDGoLeftObjects1.createFrom(runtimeScene.getObjects("GoLeft"));
gdjs.LevelCode.GDMovingPlatformObjects1.createFrom(runtimeScene.getObjects("MovingPlatform"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDGoLeftObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDMovingPlatformObjects1Objects, false, runtimeScene);
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDMovingPlatformObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDMovingPlatformObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.LevelCode.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDMovingPlatformObjects1[i].addForce(-150, 0, 1);
}
}}

}


{

gdjs.LevelCode.GDGoRightObjects1.createFrom(runtimeScene.getObjects("GoRight"));
gdjs.LevelCode.GDMovingPlatformObjects1.createFrom(runtimeScene.getObjects("MovingPlatform"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDGoRightObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDMovingPlatformObjects1Objects, false, runtimeScene);
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDMovingPlatformObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDMovingPlatformObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.LevelCode.GDMovingPlatformObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDMovingPlatformObjects1[i].addForce(150, 0, 1);
}
}}

}


{



}


{



}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
{for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].activateBehavior("PlatformerObject", false);
}
}}

}


{



}


{

gdjs.LevelCode.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.LevelCode.GDGoLeftObjects1.createFrom(runtimeScene.getObjects("GoLeft"));
gdjs.LevelCode.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDGoLeftObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDSlimeWalkObjects1ObjectsGDgdjs_46LevelCode_46GDFlyObjects1Objects, false, runtimeScene);
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects1[i].returnVariable(gdjs.LevelCode.GDSlimeWalkObjects1[i].getVariables().get("GoingLeft")).setNumber(1);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].returnVariable(gdjs.LevelCode.GDFlyObjects1[i].getVariables().get("GoingLeft")).setNumber(1);
}
}}

}


{

gdjs.LevelCode.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.LevelCode.GDGoRightObjects1.createFrom(runtimeScene.getObjects("GoRight"));
gdjs.LevelCode.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDGoRightObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDSlimeWalkObjects1ObjectsGDgdjs_46LevelCode_46GDFlyObjects1Objects, false, runtimeScene);
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects1[i].returnVariable(gdjs.LevelCode.GDSlimeWalkObjects1[i].getVariables().get("GoingLeft")).setNumber(0);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].returnVariable(gdjs.LevelCode.GDFlyObjects1[i].getVariables().get("GoingLeft")).setNumber(0);
}
}}

}


{

gdjs.LevelCode.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.LevelCode.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects1[i].getAnimation() == 0 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDSlimeWalkObjects1[k] = gdjs.LevelCode.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects1[i].getAnimation() == 0 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDFlyObjects1[k] = gdjs.LevelCode.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.LevelCode.eventsList0x6a1268(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.LevelCode.GDFlyObjects1.createFrom(runtimeScene.getObjects("Fly"));
gdjs.LevelCode.GDSlimeWalkObjects1.createFrom(runtimeScene.getObjects("SlimeWalk"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
gdjs.LevelCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects1[i].getAnimation() == 1 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDSlimeWalkObjects1[k] = gdjs.LevelCode.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects1[i].getAnimation() == 1 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDFlyObjects1[k] = gdjs.LevelCode.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects1.length = k;}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDSlimeWalkObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.LevelCode.condition1IsTrue_0.val = true;
        gdjs.LevelCode.GDSlimeWalkObjects1[k] = gdjs.LevelCode.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDFlyObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.LevelCode.condition1IsTrue_0.val = true;
        gdjs.LevelCode.GDFlyObjects1[k] = gdjs.LevelCode.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects1.length = k;}if ( gdjs.LevelCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDSlimeWalkObjects1.length;i<l;++i) {
    if ( !(gdjs.LevelCode.GDSlimeWalkObjects1[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.LevelCode.condition2IsTrue_0.val = true;
        gdjs.LevelCode.GDSlimeWalkObjects1[k] = gdjs.LevelCode.GDSlimeWalkObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDSlimeWalkObjects1.length = k;for(var i = 0, k = 0, l = gdjs.LevelCode.GDFlyObjects1.length;i<l;++i) {
    if ( !(gdjs.LevelCode.GDFlyObjects1[i].getBehavior("PlatformerObject").isMoving()) ) {
        gdjs.LevelCode.condition2IsTrue_0.val = true;
        gdjs.LevelCode.GDFlyObjects1[k] = gdjs.LevelCode.GDFlyObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDFlyObjects1.length = k;}}
}
if (gdjs.LevelCode.condition2IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDFlyObjects1 */
/* Reuse gdjs.LevelCode.GDSlimeWalkObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects1[i].activateBehavior("PlatformerObject", false);
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].activateBehavior("PlatformerObject", false);
}
}{for(var i = 0, len = gdjs.LevelCode.GDSlimeWalkObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDSlimeWalkObjects1[i].setOpacity(gdjs.LevelCode.GDSlimeWalkObjects1[i].getOpacity() - (50*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
for(var i = 0, len = gdjs.LevelCode.GDFlyObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDFlyObjects1[i].setOpacity(gdjs.LevelCode.GDFlyObjects1[i].getOpacity() - (50*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}
{ //Subevents
gdjs.LevelCode.eventsList0x6a2870(runtimeScene, context);} //End of subevents
}

}


{



}


{



}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDfaseObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDfaseObjects1Objects, 250, 620, "GUI");
}{for(var i = 0, len = gdjs.LevelCode.GDfaseObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfaseObjects1[i].setString("FASE " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}
}}

}


{

gdjs.LevelCode.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));
gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDPlayerHitBoxObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDCoinObjects1Objects, false, runtimeScene);
}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDCoinObjects1[i].getOpacity() == 255 ) {
        gdjs.LevelCode.condition1IsTrue_0.val = true;
        gdjs.LevelCode.GDCoinObjects1[k] = gdjs.LevelCode.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDCoinObjects1.length = k;}}
if (gdjs.LevelCode.condition1IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDCoinObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDCoinObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDCoinObjects1[i].setOpacity(254);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "coin.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(6).add(10);
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}
{ //Subevents
gdjs.LevelCode.eventsList0x6a31e0(runtimeScene, context);} //End of subevents
}

}


{

gdjs.LevelCode.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDCoinObjects1[i].getOpacity() < 255 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDCoinObjects1[k] = gdjs.LevelCode.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDCoinObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDCoinObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDCoinObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDCoinObjects1[i].setOpacity(gdjs.LevelCode.GDCoinObjects1[i].getOpacity() - (255*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}{for(var i = 0, len = gdjs.LevelCode.GDCoinObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDCoinObjects1[i].addForce(0, -30, 0);
}
}}

}


{

gdjs.LevelCode.GDCoinObjects1.createFrom(runtimeScene.getObjects("Coin"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDCoinObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDCoinObjects1[i].getOpacity() == 0 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDCoinObjects1[k] = gdjs.LevelCode.GDCoinObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDCoinObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDCoinObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDCoinObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDCoinObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.LevelCode.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDfaseObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDfaseObjects1[i].getY() > 100 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDfaseObjects1[k] = gdjs.LevelCode.GDfaseObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDfaseObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDfaseObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDfaseObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfaseObjects1[i].addForce(0, -200, 0);
}
}}

}


{

gdjs.LevelCode.GDfaseObjects1.createFrom(runtimeScene.getObjects("fase"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDfaseObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDfaseObjects1[i].getY() < 100 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDfaseObjects1[k] = gdjs.LevelCode.GDfaseObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDfaseObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDfaseObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDfaseObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfaseObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{



}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDtxtFaseObjects1.createFrom(runtimeScene.getObjects("txtFase"));
{for(var i = 0, len = gdjs.LevelCode.GDtxtFaseObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDtxtFaseObjects1[i].setString("Fase " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) < 0;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(0);
}}

}


{


{
gdjs.LevelCode.GDScoreObjects1.createFrom(runtimeScene.getObjects("Score"));
{for(var i = 0, len = gdjs.LevelCode.GDScoreObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDScoreObjects1[i].setString("x "+ gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(6)));
}
}}

}


{



}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.input.touchSimulateMouse(runtimeScene, false);
}
{ //Subevents
gdjs.LevelCode.eventsList0x69cd10(runtimeScene, context);} //End of subevents
}

}


{

gdjs.LevelCode.GDLeftButtonObjects1.createFrom(runtimeScene.getObjects("LeftButton"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
gdjs.LevelCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDLeftButtonObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.LevelCode.condition1IsTrue_0.val) {
gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateLeftKey();
}
}}

}


{

gdjs.LevelCode.GDRightButtonObjects1.createFrom(runtimeScene.getObjects("RightButton"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
gdjs.LevelCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDRightButtonObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.LevelCode.condition1IsTrue_0.val) {
gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateRightKey();
}
}}

}


{

gdjs.LevelCode.GDJumpButtonObjects1.createFrom(runtimeScene.getObjects("JumpButton"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
gdjs.LevelCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDJumpButtonObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.LevelCode.condition1IsTrue_0.val) {
gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
{gdjs.LevelCode.conditionTrue_1 = gdjs.LevelCode.condition1IsTrue_0;
gdjs.LevelCode.conditionTrue_1.val = context.triggerOnce(6937164);
}
}}
if (gdjs.LevelCode.condition1IsTrue_0.val) {
gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{



}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
{gdjs.LevelCode.conditionTrue_1 = gdjs.LevelCode.condition0IsTrue_0;
gdjs.LevelCode.conditionTrue_1.val = (gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0);
}
}if (gdjs.LevelCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.LevelCode.eventsList0x69ddd8(runtimeScene, context);} //End of subevents
}

}


{

gdjs.LevelCode.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));
gdjs.LevelCode.GDexitObjects1.createFrom(runtimeScene.getObjects("exit"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
gdjs.LevelCode.condition1IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDPlayerObjects1Objects, gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDexitObjects1Objects, false, runtimeScene);
}if ( gdjs.LevelCode.condition0IsTrue_0.val ) {
{
{gdjs.LevelCode.conditionTrue_1 = gdjs.LevelCode.condition1IsTrue_0;
gdjs.LevelCode.conditionTrue_1.val = context.triggerOnce(6945836);
}
}}
if (gdjs.LevelCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "applause.ogg", false, 100, 1);
}}

}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDPlayerHitBoxObjects1.createFrom(runtimeScene.getObjects("PlayerHitBox"));
{for(var i = 0, len = gdjs.LevelCode.GDPlayerHitBoxObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDPlayerHitBoxObjects1[i].getBehavior("PlatformerObject").simulateRightKey();
}
}
{ //Subevents
gdjs.LevelCode.eventsList0x69fe90(runtimeScene, context);} //End of subevents
}

}


{



}


{


gdjs.LevelCode.condition0IsTrue_0.val = false;
{
gdjs.LevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(7)) == 1;
}if (gdjs.LevelCode.condition0IsTrue_0.val) {
gdjs.LevelCode.GDfadeObjects1.length = 0;

{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(0);
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.LevelCode.mapOfGDgdjs_46LevelCode_46GDfadeObjects1Objects, 0, 0, "GUI");
}{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].setScaleX(gdjs.evtTools.window.getWindowWidth());
}
}{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].setScaleY(gdjs.evtTools.window.getWindowHeight());
}
}{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].setOpacity(255);
}
}}

}


{

gdjs.LevelCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDfadeObjects1[i].getOpacity() == 255 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDfadeObjects1[k] = gdjs.LevelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDfadeObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].setOpacity(254);
}
}}

}


{

gdjs.LevelCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDfadeObjects1[i].getOpacity() < 255 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDfadeObjects1[k] = gdjs.LevelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDfadeObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].setOpacity(gdjs.LevelCode.GDfadeObjects1[i].getOpacity() - (255*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene)));
}
}}

}


{

gdjs.LevelCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDfadeObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDfadeObjects1[i].getOpacity() == 0 ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDfadeObjects1[k] = gdjs.LevelCode.GDfadeObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDfadeObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
/* Reuse gdjs.LevelCode.GDfadeObjects1 */
{for(var i = 0, len = gdjs.LevelCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.LevelCode.GDfadeObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.LevelCode.GDPlayerObjects1.createFrom(runtimeScene.getObjects("Player"));

gdjs.LevelCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.LevelCode.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.LevelCode.GDPlayerObjects1[i].getY() > gdjs.evtTools.window.getWindowHeight() ) {
        gdjs.LevelCode.condition0IsTrue_0.val = true;
        gdjs.LevelCode.GDPlayerObjects1[k] = gdjs.LevelCode.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.LevelCode.GDPlayerObjects1.length = k;}if (gdjs.LevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(6).sub(10);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level", false);
}}

}


}; //End of gdjs.LevelCode.eventsList0xa86e0


gdjs.LevelCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.LevelCode.GDfadeObjects1.length = 0;
gdjs.LevelCode.GDfadeObjects2.length = 0;
gdjs.LevelCode.GDfadeObjects3.length = 0;
gdjs.LevelCode.GDPlayerObjects1.length = 0;
gdjs.LevelCode.GDPlayerObjects2.length = 0;
gdjs.LevelCode.GDPlayerObjects3.length = 0;
gdjs.LevelCode.GDJumpthruObjects1.length = 0;
gdjs.LevelCode.GDJumpthruObjects2.length = 0;
gdjs.LevelCode.GDJumpthruObjects3.length = 0;
gdjs.LevelCode.GDPlatformObjects1.length = 0;
gdjs.LevelCode.GDPlatformObjects2.length = 0;
gdjs.LevelCode.GDPlatformObjects3.length = 0;
gdjs.LevelCode.GDTiledGrassPlatformObjects1.length = 0;
gdjs.LevelCode.GDTiledGrassPlatformObjects2.length = 0;
gdjs.LevelCode.GDTiledGrassPlatformObjects3.length = 0;
gdjs.LevelCode.GDTiledPlatformObjects1.length = 0;
gdjs.LevelCode.GDTiledPlatformObjects2.length = 0;
gdjs.LevelCode.GDTiledPlatformObjects3.length = 0;
gdjs.LevelCode.GDTiledCastlePlatformObjects1.length = 0;
gdjs.LevelCode.GDTiledCastlePlatformObjects2.length = 0;
gdjs.LevelCode.GDTiledCastlePlatformObjects3.length = 0;
gdjs.LevelCode.GDMovingPlatformObjects1.length = 0;
gdjs.LevelCode.GDMovingPlatformObjects2.length = 0;
gdjs.LevelCode.GDMovingPlatformObjects3.length = 0;
gdjs.LevelCode.GDGoLeftObjects1.length = 0;
gdjs.LevelCode.GDGoLeftObjects2.length = 0;
gdjs.LevelCode.GDGoLeftObjects3.length = 0;
gdjs.LevelCode.GDGoRightObjects1.length = 0;
gdjs.LevelCode.GDGoRightObjects2.length = 0;
gdjs.LevelCode.GDGoRightObjects3.length = 0;
gdjs.LevelCode.GDLadderObjects1.length = 0;
gdjs.LevelCode.GDLadderObjects2.length = 0;
gdjs.LevelCode.GDLadderObjects3.length = 0;
gdjs.LevelCode.GDPlayerHitBoxObjects1.length = 0;
gdjs.LevelCode.GDPlayerHitBoxObjects2.length = 0;
gdjs.LevelCode.GDPlayerHitBoxObjects3.length = 0;
gdjs.LevelCode.GDSlimeWalkObjects1.length = 0;
gdjs.LevelCode.GDSlimeWalkObjects2.length = 0;
gdjs.LevelCode.GDSlimeWalkObjects3.length = 0;
gdjs.LevelCode.GDFlyObjects1.length = 0;
gdjs.LevelCode.GDFlyObjects2.length = 0;
gdjs.LevelCode.GDFlyObjects3.length = 0;
gdjs.LevelCode.GDCloudObjects1.length = 0;
gdjs.LevelCode.GDCloudObjects2.length = 0;
gdjs.LevelCode.GDCloudObjects3.length = 0;
gdjs.LevelCode.GDBackgroundObjectsObjects1.length = 0;
gdjs.LevelCode.GDBackgroundObjectsObjects2.length = 0;
gdjs.LevelCode.GDBackgroundObjectsObjects3.length = 0;
gdjs.LevelCode.GDScoreObjects1.length = 0;
gdjs.LevelCode.GDScoreObjects2.length = 0;
gdjs.LevelCode.GDScoreObjects3.length = 0;
gdjs.LevelCode.GDCoinObjects1.length = 0;
gdjs.LevelCode.GDCoinObjects2.length = 0;
gdjs.LevelCode.GDCoinObjects3.length = 0;
gdjs.LevelCode.GDCoinIconObjects1.length = 0;
gdjs.LevelCode.GDCoinIconObjects2.length = 0;
gdjs.LevelCode.GDCoinIconObjects3.length = 0;
gdjs.LevelCode.GDLeftButtonObjects1.length = 0;
gdjs.LevelCode.GDLeftButtonObjects2.length = 0;
gdjs.LevelCode.GDLeftButtonObjects3.length = 0;
gdjs.LevelCode.GDRightButtonObjects1.length = 0;
gdjs.LevelCode.GDRightButtonObjects2.length = 0;
gdjs.LevelCode.GDRightButtonObjects3.length = 0;
gdjs.LevelCode.GDJumpButtonObjects1.length = 0;
gdjs.LevelCode.GDJumpButtonObjects2.length = 0;
gdjs.LevelCode.GDJumpButtonObjects3.length = 0;
gdjs.LevelCode.GDArrowButtonsBgObjects1.length = 0;
gdjs.LevelCode.GDArrowButtonsBgObjects2.length = 0;
gdjs.LevelCode.GDArrowButtonsBgObjects3.length = 0;
gdjs.LevelCode.GDtxtFaseObjects1.length = 0;
gdjs.LevelCode.GDtxtFaseObjects2.length = 0;
gdjs.LevelCode.GDtxtFaseObjects3.length = 0;
gdjs.LevelCode.GDtxtNivelObjects1.length = 0;
gdjs.LevelCode.GDtxtNivelObjects2.length = 0;
gdjs.LevelCode.GDtxtNivelObjects3.length = 0;
gdjs.LevelCode.GDpanelTopObjects1.length = 0;
gdjs.LevelCode.GDpanelTopObjects2.length = 0;
gdjs.LevelCode.GDpanelTopObjects3.length = 0;
gdjs.LevelCode.GDcactusObjects1.length = 0;
gdjs.LevelCode.GDcactusObjects2.length = 0;
gdjs.LevelCode.GDcactusObjects3.length = 0;
gdjs.LevelCode.GDexitObjects1.length = 0;
gdjs.LevelCode.GDexitObjects2.length = 0;
gdjs.LevelCode.GDexitObjects3.length = 0;
gdjs.LevelCode.GDportaObjects1.length = 0;
gdjs.LevelCode.GDportaObjects2.length = 0;
gdjs.LevelCode.GDportaObjects3.length = 0;
gdjs.LevelCode.GDfaseObjects1.length = 0;
gdjs.LevelCode.GDfaseObjects2.length = 0;
gdjs.LevelCode.GDfaseObjects3.length = 0;
gdjs.LevelCode.GDNewObjectObjects1.length = 0;
gdjs.LevelCode.GDNewObjectObjects2.length = 0;
gdjs.LevelCode.GDNewObjectObjects3.length = 0;
gdjs.LevelCode.GDponteObjects1.length = 0;
gdjs.LevelCode.GDponteObjects2.length = 0;
gdjs.LevelCode.GDponteObjects3.length = 0;

gdjs.LevelCode.eventsList0xa86e0(runtimeScene, context);return;
}
gdjs['LevelCode']= gdjs.LevelCode;
