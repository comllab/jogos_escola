gdjs.DeadCode = {};
gdjs.DeadCode.GDplayerObjects1= [];
gdjs.DeadCode.GDplayerObjects2= [];
gdjs.DeadCode.GDgroundObjects1= [];
gdjs.DeadCode.GDgroundObjects2= [];
gdjs.DeadCode.GDbuttomObjects1= [];
gdjs.DeadCode.GDbuttomObjects2= [];
gdjs.DeadCode.GDbackgroundObjects1= [];
gdjs.DeadCode.GDbackgroundObjects2= [];
gdjs.DeadCode.GDbackground2Objects1= [];
gdjs.DeadCode.GDbackground2Objects2= [];
gdjs.DeadCode.GDgroundBordaObjects1= [];
gdjs.DeadCode.GDgroundBordaObjects2= [];
gdjs.DeadCode.GDobjects_950Objects1= [];
gdjs.DeadCode.GDobjects_950Objects2= [];
gdjs.DeadCode.GDnuvens1Objects1= [];
gdjs.DeadCode.GDnuvens1Objects2= [];
gdjs.DeadCode.GDlblJogarNovamenteObjects1= [];
gdjs.DeadCode.GDlblJogarNovamenteObjects2= [];

gdjs.DeadCode.conditionTrue_0 = {val:false};
gdjs.DeadCode.condition0IsTrue_0 = {val:false};
gdjs.DeadCode.condition1IsTrue_0 = {val:false};
gdjs.DeadCode.condition2IsTrue_0 = {val:false};
gdjs.DeadCode.condition3IsTrue_0 = {val:false};


gdjs.DeadCode.mapOfGDgdjs_46DeadCode_46GDbuttomObjects1Objects = Hashtable.newFrom({"buttom": gdjs.DeadCode.GDbuttomObjects1});gdjs.DeadCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.DeadCode.condition0IsTrue_0.val = false;
{
gdjs.DeadCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.DeadCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "dead.ogg", false, 100, 1);
}}

}


{


{
gdjs.DeadCode.GDnuvens1Objects1.createFrom(runtimeScene.getObjects("nuvens1"));
{for(var i = 0, len = gdjs.DeadCode.GDnuvens1Objects1.length ;i < len;++i) {
    gdjs.DeadCode.GDnuvens1Objects1[i].setXOffset(gdjs.DeadCode.GDnuvens1Objects1[i].getXOffset() + (0.5));
}
}}

}


{

gdjs.DeadCode.GDbuttomObjects1.createFrom(runtimeScene.getObjects("buttom"));

gdjs.DeadCode.condition0IsTrue_0.val = false;
gdjs.DeadCode.condition1IsTrue_0.val = false;
gdjs.DeadCode.condition2IsTrue_0.val = false;
{
gdjs.DeadCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Buttom");
}if ( gdjs.DeadCode.condition0IsTrue_0.val ) {
{
gdjs.DeadCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.DeadCode.mapOfGDgdjs_46DeadCode_46GDbuttomObjects1Objects, runtimeScene, true, false);
}if ( gdjs.DeadCode.condition1IsTrue_0.val ) {
{
gdjs.DeadCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
if (gdjs.DeadCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Activity", false);
}}

}


{

gdjs.DeadCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.DeadCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.DeadCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.DeadCode.GDplayerObjects1[i].hasAnimationEnded() ) {
        gdjs.DeadCode.condition0IsTrue_0.val = true;
        gdjs.DeadCode.GDplayerObjects1[k] = gdjs.DeadCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.DeadCode.GDplayerObjects1.length = k;}if (gdjs.DeadCode.condition0IsTrue_0.val) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "Buttom");
}}

}


}; //End of gdjs.DeadCode.eventsList0xaff48


gdjs.DeadCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.DeadCode.GDplayerObjects1.length = 0;
gdjs.DeadCode.GDplayerObjects2.length = 0;
gdjs.DeadCode.GDgroundObjects1.length = 0;
gdjs.DeadCode.GDgroundObjects2.length = 0;
gdjs.DeadCode.GDbuttomObjects1.length = 0;
gdjs.DeadCode.GDbuttomObjects2.length = 0;
gdjs.DeadCode.GDbackgroundObjects1.length = 0;
gdjs.DeadCode.GDbackgroundObjects2.length = 0;
gdjs.DeadCode.GDbackground2Objects1.length = 0;
gdjs.DeadCode.GDbackground2Objects2.length = 0;
gdjs.DeadCode.GDgroundBordaObjects1.length = 0;
gdjs.DeadCode.GDgroundBordaObjects2.length = 0;
gdjs.DeadCode.GDobjects_950Objects1.length = 0;
gdjs.DeadCode.GDobjects_950Objects2.length = 0;
gdjs.DeadCode.GDnuvens1Objects1.length = 0;
gdjs.DeadCode.GDnuvens1Objects2.length = 0;
gdjs.DeadCode.GDlblJogarNovamenteObjects1.length = 0;
gdjs.DeadCode.GDlblJogarNovamenteObjects2.length = 0;

gdjs.DeadCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['DeadCode'] = gdjs.DeadCode;
