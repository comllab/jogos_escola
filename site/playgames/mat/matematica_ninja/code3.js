gdjs.ActivityCode = {};
gdjs.ActivityCode.GDlblDezenasRespObjects1_1final = [];

gdjs.ActivityCode.GDlblUnidadesRespObjects1_1final = [];

gdjs.ActivityCode.GDplayerObjects1= [];
gdjs.ActivityCode.GDplayerObjects2= [];
gdjs.ActivityCode.GDplayerObjects3= [];
gdjs.ActivityCode.GDgroundObjects1= [];
gdjs.ActivityCode.GDgroundObjects2= [];
gdjs.ActivityCode.GDgroundObjects3= [];
gdjs.ActivityCode.GDbuttomObjects1= [];
gdjs.ActivityCode.GDbuttomObjects2= [];
gdjs.ActivityCode.GDbuttomObjects3= [];
gdjs.ActivityCode.GDbackgroundObjects1= [];
gdjs.ActivityCode.GDbackgroundObjects2= [];
gdjs.ActivityCode.GDbackgroundObjects3= [];
gdjs.ActivityCode.GDbackground2Objects1= [];
gdjs.ActivityCode.GDbackground2Objects2= [];
gdjs.ActivityCode.GDbackground2Objects3= [];
gdjs.ActivityCode.GDblocosObjects1= [];
gdjs.ActivityCode.GDblocosObjects2= [];
gdjs.ActivityCode.GDblocosObjects3= [];
gdjs.ActivityCode.GDdezenasObjects1= [];
gdjs.ActivityCode.GDdezenasObjects2= [];
gdjs.ActivityCode.GDdezenasObjects3= [];
gdjs.ActivityCode.GDunidadesObjects1= [];
gdjs.ActivityCode.GDunidadesObjects2= [];
gdjs.ActivityCode.GDunidadesObjects3= [];
gdjs.ActivityCode.GDdebugObjects1= [];
gdjs.ActivityCode.GDdebugObjects2= [];
gdjs.ActivityCode.GDdebugObjects3= [];
gdjs.ActivityCode.GDlblUnidadesRespObjects1= [];
gdjs.ActivityCode.GDlblUnidadesRespObjects2= [];
gdjs.ActivityCode.GDlblUnidadesRespObjects3= [];
gdjs.ActivityCode.GDlblDezenasRespObjects1= [];
gdjs.ActivityCode.GDlblDezenasRespObjects2= [];
gdjs.ActivityCode.GDlblDezenasRespObjects3= [];
gdjs.ActivityCode.GDlblEnunciadoObjects1= [];
gdjs.ActivityCode.GDlblEnunciadoObjects2= [];
gdjs.ActivityCode.GDlblEnunciadoObjects3= [];
gdjs.ActivityCode.GDlblConferirObjects1= [];
gdjs.ActivityCode.GDlblConferirObjects2= [];
gdjs.ActivityCode.GDlblConferirObjects3= [];
gdjs.ActivityCode.GDlblUnidadesObjects1= [];
gdjs.ActivityCode.GDlblUnidadesObjects2= [];
gdjs.ActivityCode.GDlblUnidadesObjects3= [];
gdjs.ActivityCode.GDlblDezenasObjects1= [];
gdjs.ActivityCode.GDlblDezenasObjects2= [];
gdjs.ActivityCode.GDlblDezenasObjects3= [];
gdjs.ActivityCode.GDinputObjects1= [];
gdjs.ActivityCode.GDinputObjects2= [];
gdjs.ActivityCode.GDinputObjects3= [];
gdjs.ActivityCode.GDNewObjectObjects1= [];
gdjs.ActivityCode.GDNewObjectObjects2= [];
gdjs.ActivityCode.GDNewObjectObjects3= [];
gdjs.ActivityCode.GDhandObjects1= [];
gdjs.ActivityCode.GDhandObjects2= [];
gdjs.ActivityCode.GDhandObjects3= [];

gdjs.ActivityCode.conditionTrue_0 = {val:false};
gdjs.ActivityCode.condition0IsTrue_0 = {val:false};
gdjs.ActivityCode.condition1IsTrue_0 = {val:false};
gdjs.ActivityCode.condition2IsTrue_0 = {val:false};
gdjs.ActivityCode.condition3IsTrue_0 = {val:false};
gdjs.ActivityCode.condition4IsTrue_0 = {val:false};
gdjs.ActivityCode.conditionTrue_1 = {val:false};
gdjs.ActivityCode.condition0IsTrue_1 = {val:false};
gdjs.ActivityCode.condition1IsTrue_1 = {val:false};
gdjs.ActivityCode.condition2IsTrue_1 = {val:false};
gdjs.ActivityCode.condition3IsTrue_1 = {val:false};
gdjs.ActivityCode.condition4IsTrue_1 = {val:false};


gdjs.ActivityCode.eventsList0x950d10 = function(runtimeScene) {

{

gdjs.ActivityCode.GDblocosObjects2.createFrom(gdjs.ActivityCode.GDblocosObjects1);


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDblocosObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDblocosObjects2[i].getVariableNumber(gdjs.ActivityCode.GDblocosObjects2[i].getVariables().getFromIndex(0)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDblocosObjects2[k] = gdjs.ActivityCode.GDblocosObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDblocosObjects2.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDblocosObjects2 */
{for(var i = 0, len = gdjs.ActivityCode.GDblocosObjects2.length ;i < len;++i) {
    gdjs.ActivityCode.GDblocosObjects2[i].hide(false);
}
}}

}


{

/* Reuse gdjs.ActivityCode.GDblocosObjects1 */

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) > 0;
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDblocosObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDblocosObjects1[i].getVariableNumber(gdjs.ActivityCode.GDblocosObjects1[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + 1 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDblocosObjects1[k] = gdjs.ActivityCode.GDblocosObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDblocosObjects1.length = k;}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDblocosObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDblocosObjects1[i].getVariableNumber(gdjs.ActivityCode.GDblocosObjects1[i].getVariables().getFromIndex(1)) <= gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) ) {
        gdjs.ActivityCode.condition2IsTrue_0.val = true;
        gdjs.ActivityCode.GDblocosObjects1[k] = gdjs.ActivityCode.GDblocosObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDblocosObjects1.length = k;}}
}
if (gdjs.ActivityCode.condition2IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDblocosObjects1 */
{for(var i = 0, len = gdjs.ActivityCode.GDblocosObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDblocosObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.ActivityCode.GDblocosObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDblocosObjects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.ActivityCode.eventsList0x950d10
gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDdezenasObjects1Objects = Hashtable.newFrom({"dezenas": gdjs.ActivityCode.GDdezenasObjects1});gdjs.ActivityCode.eventsList0x9957a8 = function(runtimeScene) {

{


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) > 9;
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(2).setNumber(1);
}}

}


{


{
/* Reuse gdjs.ActivityCode.GDdezenasObjects1 */
/* Reuse gdjs.ActivityCode.GDinputObjects1 */
/* Reuse gdjs.ActivityCode.GDlblDezenasRespObjects1 */
{for(var i = 0, len = gdjs.ActivityCode.GDinputObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDinputObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(2)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDdezenasObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDdezenasObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.ActivityCode.GDlblDezenasRespObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblDezenasRespObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(2)));
}
}}

}


}; //End of gdjs.ActivityCode.eventsList0x9957a8
gdjs.ActivityCode.eventsList0x996a40 = function(runtimeScene) {

{


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(2).add(1);
}
{ //Subevents
gdjs.ActivityCode.eventsList0x9957a8(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.ActivityCode.eventsList0x996a40
gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDunidadesObjects1Objects = Hashtable.newFrom({"unidades": gdjs.ActivityCode.GDunidadesObjects1});gdjs.ActivityCode.eventsList0x993128 = function(runtimeScene) {

{


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) > 9;
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(3).setNumber(1);
}}

}


{


{
/* Reuse gdjs.ActivityCode.GDinputObjects1 */
/* Reuse gdjs.ActivityCode.GDlblUnidadesRespObjects1 */
/* Reuse gdjs.ActivityCode.GDunidadesObjects1 */
{for(var i = 0, len = gdjs.ActivityCode.GDinputObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDinputObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(3)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDunidadesObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDunidadesObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.ActivityCode.GDlblUnidadesRespObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblUnidadesRespObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(3)));
}
}}

}


}; //End of gdjs.ActivityCode.eventsList0x993128
gdjs.ActivityCode.eventsList0x993c00 = function(runtimeScene) {

{


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(3).add(1);
}
{ //Subevents
gdjs.ActivityCode.eventsList0x993128(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.ActivityCode.eventsList0x993c00
gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDbuttomObjects1Objects = Hashtable.newFrom({"buttom": gdjs.ActivityCode.GDbuttomObjects1});gdjs.ActivityCode.eventsList0x98ff28 = function(runtimeScene) {

{

gdjs.ActivityCode.GDhandObjects2.createFrom(gdjs.ActivityCode.GDhandObjects1);


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDhandObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDhandObjects2[i].getVariableNumber(gdjs.ActivityCode.GDhandObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDhandObjects2[k] = gdjs.ActivityCode.GDhandObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDhandObjects2.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDhandObjects2 */
{for(var i = 0, len = gdjs.ActivityCode.GDhandObjects2.length ;i < len;++i) {
    gdjs.ActivityCode.GDhandObjects2[i].hide(false);
}
}}

}


}; //End of gdjs.ActivityCode.eventsList0x98ff28
gdjs.ActivityCode.eventsList0x98f068 = function(runtimeScene) {

{

/* Reuse gdjs.ActivityCode.GDhandObjects1 */

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDhandObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDhandObjects1[i].getVariableNumber(gdjs.ActivityCode.GDhandObjects1[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDhandObjects1[k] = gdjs.ActivityCode.GDhandObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDhandObjects1.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDhandObjects1 */
{for(var i = 0, len = gdjs.ActivityCode.GDhandObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDhandObjects1[i].hide(false);
}
}}

}


}; //End of gdjs.ActivityCode.eventsList0x98f068
gdjs.ActivityCode.eventsList0x990a38 = function(runtimeScene) {

{

gdjs.ActivityCode.GDlblDezenasRespObjects2.createFrom(runtimeScene.getObjects("lblDezenasResp"));
gdjs.ActivityCode.GDlblUnidadesRespObjects2.createFrom(runtimeScene.getObjects("lblUnidadesResp"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDlblDezenasRespObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDlblDezenasRespObjects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0))) ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDlblDezenasRespObjects2[k] = gdjs.ActivityCode.GDlblDezenasRespObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDlblDezenasRespObjects2.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDlblUnidadesRespObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDlblUnidadesRespObjects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1))) ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDlblUnidadesRespObjects2[k] = gdjs.ActivityCode.GDlblUnidadesRespObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDlblUnidadesRespObjects2.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.ActivityCode.eventsList0x98ff28(runtimeScene);} //End of subevents
}

}


{

gdjs.ActivityCode.GDlblDezenasRespObjects1.length = 0;

gdjs.ActivityCode.GDlblUnidadesRespObjects1.length = 0;


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
{gdjs.ActivityCode.conditionTrue_1 = gdjs.ActivityCode.condition0IsTrue_0;
gdjs.ActivityCode.GDlblDezenasRespObjects1_1final.length = 0;gdjs.ActivityCode.GDlblUnidadesRespObjects1_1final.length = 0;gdjs.ActivityCode.condition0IsTrue_1.val = false;
gdjs.ActivityCode.condition1IsTrue_1.val = false;
{
gdjs.ActivityCode.GDlblDezenasRespObjects2.createFrom(runtimeScene.getObjects("lblDezenasResp"));
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDlblDezenasRespObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDlblDezenasRespObjects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0))) ) {
        gdjs.ActivityCode.condition0IsTrue_1.val = true;
        gdjs.ActivityCode.GDlblDezenasRespObjects2[k] = gdjs.ActivityCode.GDlblDezenasRespObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDlblDezenasRespObjects2.length = k;if( gdjs.ActivityCode.condition0IsTrue_1.val ) {
    gdjs.ActivityCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.ActivityCode.GDlblDezenasRespObjects2.length;j<jLen;++j) {
        if ( gdjs.ActivityCode.GDlblDezenasRespObjects1_1final.indexOf(gdjs.ActivityCode.GDlblDezenasRespObjects2[j]) === -1 )
            gdjs.ActivityCode.GDlblDezenasRespObjects1_1final.push(gdjs.ActivityCode.GDlblDezenasRespObjects2[j]);
    }
}
}
{
gdjs.ActivityCode.GDlblUnidadesRespObjects2.createFrom(runtimeScene.getObjects("lblUnidadesResp"));
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDlblUnidadesRespObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDlblUnidadesRespObjects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1))) ) {
        gdjs.ActivityCode.condition1IsTrue_1.val = true;
        gdjs.ActivityCode.GDlblUnidadesRespObjects2[k] = gdjs.ActivityCode.GDlblUnidadesRespObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDlblUnidadesRespObjects2.length = k;if( gdjs.ActivityCode.condition1IsTrue_1.val ) {
    gdjs.ActivityCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.ActivityCode.GDlblUnidadesRespObjects2.length;j<jLen;++j) {
        if ( gdjs.ActivityCode.GDlblUnidadesRespObjects1_1final.indexOf(gdjs.ActivityCode.GDlblUnidadesRespObjects2[j]) === -1 )
            gdjs.ActivityCode.GDlblUnidadesRespObjects1_1final.push(gdjs.ActivityCode.GDlblUnidadesRespObjects2[j]);
    }
}
}
{
gdjs.ActivityCode.GDlblDezenasRespObjects1.createFrom(gdjs.ActivityCode.GDlblDezenasRespObjects1_1final);
gdjs.ActivityCode.GDlblUnidadesRespObjects1.createFrom(gdjs.ActivityCode.GDlblUnidadesRespObjects1_1final);
}
}
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.ActivityCode.eventsList0x98f068(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.ActivityCode.eventsList0x990a38
gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDhandObjects1Objects = Hashtable.newFrom({"hand": gdjs.ActivityCode.GDhandObjects1});gdjs.ActivityCode.eventsList0x97cc60 = function(runtimeScene) {

{

gdjs.ActivityCode.GDhandObjects2.createFrom(gdjs.ActivityCode.GDhandObjects1);


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDhandObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDhandObjects2[i].getVariableNumber(gdjs.ActivityCode.GDhandObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDhandObjects2[k] = gdjs.ActivityCode.GDhandObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDhandObjects2.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{

/* Reuse gdjs.ActivityCode.GDhandObjects1 */

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDhandObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDhandObjects1[i].getVariableNumber(gdjs.ActivityCode.GDhandObjects1[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDhandObjects1[k] = gdjs.ActivityCode.GDhandObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDhandObjects1.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Activity", false);
}}

}


}; //End of gdjs.ActivityCode.eventsList0x97cc60
gdjs.ActivityCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
gdjs.ActivityCode.GDblocosObjects1.createFrom(runtimeScene.getObjects("blocos"));
gdjs.ActivityCode.GDdebugObjects1.createFrom(runtimeScene.getObjects("debug"));
gdjs.ActivityCode.GDlblDezenasRespObjects1.createFrom(runtimeScene.getObjects("lblDezenasResp"));
gdjs.ActivityCode.GDlblUnidadesRespObjects1.createFrom(runtimeScene.getObjects("lblUnidadesResp"));
{for(var i = 0, len = gdjs.ActivityCode.GDblocosObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDblocosObjects1[i].hide();
}
}{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.random(9));
}{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.random(9));
}{for(var i = 0, len = gdjs.ActivityCode.GDdebugObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDdebugObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0))) + gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1))));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDlblDezenasRespObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblDezenasRespObjects1[i].setString("");
}
}{for(var i = 0, len = gdjs.ActivityCode.GDlblUnidadesRespObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblUnidadesRespObjects1[i].setString("");
}
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "Buttom");
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "Hand");
}
{ //Subevents
gdjs.ActivityCode.eventsList0x950d10(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.ActivityCode.GDdezenasObjects1.createFrom(runtimeScene.getObjects("dezenas"));
gdjs.ActivityCode.GDinputObjects1.createFrom(runtimeScene.getObjects("input"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDinputObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDinputObjects1[i].getString() != "" ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDinputObjects1[k] = gdjs.ActivityCode.GDinputObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDinputObjects1.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDdezenasObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDdezenasObjects1[i].getAnimation() == 1 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDdezenasObjects1[k] = gdjs.ActivityCode.GDdezenasObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDdezenasObjects1.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDdezenasObjects1 */
/* Reuse gdjs.ActivityCode.GDinputObjects1 */
gdjs.ActivityCode.GDlblDezenasRespObjects1.createFrom(runtimeScene.getObjects("lblDezenasResp"));
{for(var i = 0, len = gdjs.ActivityCode.GDlblDezenasRespObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblDezenasRespObjects1[i].setString((( gdjs.ActivityCode.GDinputObjects1.length === 0 ) ? "" :gdjs.ActivityCode.GDinputObjects1[0].getString()));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDdezenasObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDdezenasObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.ActivityCode.GDinputObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDinputObjects1[i].setString("");
}
}}

}


{

gdjs.ActivityCode.GDinputObjects1.createFrom(runtimeScene.getObjects("input"));
gdjs.ActivityCode.GDunidadesObjects1.createFrom(runtimeScene.getObjects("unidades"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDinputObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDinputObjects1[i].getString() != "" ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDinputObjects1[k] = gdjs.ActivityCode.GDinputObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDinputObjects1.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDunidadesObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDunidadesObjects1[i].getAnimation() == 1 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDunidadesObjects1[k] = gdjs.ActivityCode.GDunidadesObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDunidadesObjects1.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDinputObjects1 */
gdjs.ActivityCode.GDlblUnidadesRespObjects1.createFrom(runtimeScene.getObjects("lblUnidadesResp"));
/* Reuse gdjs.ActivityCode.GDunidadesObjects1 */
{for(var i = 0, len = gdjs.ActivityCode.GDlblUnidadesRespObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblUnidadesRespObjects1[i].setString((( gdjs.ActivityCode.GDinputObjects1.length === 0 ) ? "" :gdjs.ActivityCode.GDinputObjects1[0].getString()));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDunidadesObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDunidadesObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.ActivityCode.GDinputObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDinputObjects1[i].setString("");
}
}}

}


{

gdjs.ActivityCode.GDdezenasObjects1.createFrom(runtimeScene.getObjects("dezenas"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDdezenasObjects1Objects, runtimeScene, true, false);
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
{gdjs.ActivityCode.conditionTrue_1 = gdjs.ActivityCode.condition2IsTrue_0;
gdjs.ActivityCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10061220);
}
}}
}
if (gdjs.ActivityCode.condition2IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDdezenasObjects1 */
gdjs.ActivityCode.GDinputObjects1.createFrom(runtimeScene.getObjects("input"));
gdjs.ActivityCode.GDlblDezenasRespObjects1.createFrom(runtimeScene.getObjects("lblDezenasResp"));
gdjs.ActivityCode.GDunidadesObjects1.createFrom(runtimeScene.getObjects("unidades"));
{for(var i = 0, len = gdjs.ActivityCode.GDinputObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDinputObjects1[i].setString("");
}
}{for(var i = 0, len = gdjs.ActivityCode.GDdezenasObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDdezenasObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.ActivityCode.GDunidadesObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDunidadesObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.ActivityCode.GDlblDezenasRespObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblDezenasRespObjects1[i].setString("");
}
}
{ //Subevents
gdjs.ActivityCode.eventsList0x996a40(runtimeScene);} //End of subevents
}

}


{

gdjs.ActivityCode.GDunidadesObjects1.createFrom(runtimeScene.getObjects("unidades"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDunidadesObjects1Objects, runtimeScene, true, false);
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
{gdjs.ActivityCode.conditionTrue_1 = gdjs.ActivityCode.condition2IsTrue_0;
gdjs.ActivityCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10043244);
}
}}
}
if (gdjs.ActivityCode.condition2IsTrue_0.val) {
gdjs.ActivityCode.GDdezenasObjects1.createFrom(runtimeScene.getObjects("dezenas"));
gdjs.ActivityCode.GDinputObjects1.createFrom(runtimeScene.getObjects("input"));
gdjs.ActivityCode.GDlblUnidadesRespObjects1.createFrom(runtimeScene.getObjects("lblUnidadesResp"));
/* Reuse gdjs.ActivityCode.GDunidadesObjects1 */
{for(var i = 0, len = gdjs.ActivityCode.GDinputObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDinputObjects1[i].setString("");
}
}{for(var i = 0, len = gdjs.ActivityCode.GDunidadesObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDunidadesObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.ActivityCode.GDdezenasObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDdezenasObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.ActivityCode.GDlblUnidadesRespObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblUnidadesRespObjects1[i].setString("");
}
}
{ //Subevents
gdjs.ActivityCode.eventsList0x993c00(runtimeScene);} //End of subevents
}

}


{

gdjs.ActivityCode.GDlblDezenasRespObjects1.createFrom(runtimeScene.getObjects("lblDezenasResp"));
gdjs.ActivityCode.GDlblUnidadesRespObjects1.createFrom(runtimeScene.getObjects("lblUnidadesResp"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDlblDezenasRespObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDlblDezenasRespObjects1[i].getString() != "" ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDlblDezenasRespObjects1[k] = gdjs.ActivityCode.GDlblDezenasRespObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDlblDezenasRespObjects1.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDlblUnidadesRespObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDlblUnidadesRespObjects1[i].getString() != "" ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDlblUnidadesRespObjects1[k] = gdjs.ActivityCode.GDlblUnidadesRespObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDlblUnidadesRespObjects1.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "Buttom");
}}

}


{

gdjs.ActivityCode.GDlblDezenasRespObjects1.length = 0;

gdjs.ActivityCode.GDlblUnidadesRespObjects1.length = 0;


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
{gdjs.ActivityCode.conditionTrue_1 = gdjs.ActivityCode.condition0IsTrue_0;
gdjs.ActivityCode.GDlblDezenasRespObjects1_1final.length = 0;gdjs.ActivityCode.GDlblUnidadesRespObjects1_1final.length = 0;gdjs.ActivityCode.condition0IsTrue_1.val = false;
gdjs.ActivityCode.condition1IsTrue_1.val = false;
{
gdjs.ActivityCode.GDlblDezenasRespObjects2.createFrom(runtimeScene.getObjects("lblDezenasResp"));
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDlblDezenasRespObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDlblDezenasRespObjects2[i].getString() == "" ) {
        gdjs.ActivityCode.condition0IsTrue_1.val = true;
        gdjs.ActivityCode.GDlblDezenasRespObjects2[k] = gdjs.ActivityCode.GDlblDezenasRespObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDlblDezenasRespObjects2.length = k;if( gdjs.ActivityCode.condition0IsTrue_1.val ) {
    gdjs.ActivityCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.ActivityCode.GDlblDezenasRespObjects2.length;j<jLen;++j) {
        if ( gdjs.ActivityCode.GDlblDezenasRespObjects1_1final.indexOf(gdjs.ActivityCode.GDlblDezenasRespObjects2[j]) === -1 )
            gdjs.ActivityCode.GDlblDezenasRespObjects1_1final.push(gdjs.ActivityCode.GDlblDezenasRespObjects2[j]);
    }
}
}
{
gdjs.ActivityCode.GDlblUnidadesRespObjects2.createFrom(runtimeScene.getObjects("lblUnidadesResp"));
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDlblUnidadesRespObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDlblUnidadesRespObjects2[i].getString() == "" ) {
        gdjs.ActivityCode.condition1IsTrue_1.val = true;
        gdjs.ActivityCode.GDlblUnidadesRespObjects2[k] = gdjs.ActivityCode.GDlblUnidadesRespObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDlblUnidadesRespObjects2.length = k;if( gdjs.ActivityCode.condition1IsTrue_1.val ) {
    gdjs.ActivityCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.ActivityCode.GDlblUnidadesRespObjects2.length;j<jLen;++j) {
        if ( gdjs.ActivityCode.GDlblUnidadesRespObjects1_1final.indexOf(gdjs.ActivityCode.GDlblUnidadesRespObjects2[j]) === -1 )
            gdjs.ActivityCode.GDlblUnidadesRespObjects1_1final.push(gdjs.ActivityCode.GDlblUnidadesRespObjects2[j]);
    }
}
}
{
gdjs.ActivityCode.GDlblDezenasRespObjects1.createFrom(gdjs.ActivityCode.GDlblDezenasRespObjects1_1final);
gdjs.ActivityCode.GDlblUnidadesRespObjects1.createFrom(gdjs.ActivityCode.GDlblUnidadesRespObjects1_1final);
}
}
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
{gdjs.evtTools.camera.hideLayer(runtimeScene, "Buttom");
}}

}


{

gdjs.ActivityCode.GDbuttomObjects1.createFrom(runtimeScene.getObjects("buttom"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Buttom");
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDbuttomObjects1Objects, runtimeScene, true, false);
}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
gdjs.ActivityCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
if (gdjs.ActivityCode.condition2IsTrue_0.val) {
gdjs.ActivityCode.GDhandObjects1.createFrom(runtimeScene.getObjects("hand"));
{for(var i = 0, len = gdjs.ActivityCode.GDhandObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDhandObjects1[i].hide();
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "Hand");
}
{ //Subevents
gdjs.ActivityCode.eventsList0x990a38(runtimeScene);} //End of subevents
}

}


{

gdjs.ActivityCode.GDhandObjects1.createFrom(runtimeScene.getObjects("hand"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
gdjs.ActivityCode.condition3IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Hand");
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDhandObjects1Objects, runtimeScene, true, false);
}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
gdjs.ActivityCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.ActivityCode.condition2IsTrue_0.val ) {
{
{gdjs.ActivityCode.conditionTrue_1 = gdjs.ActivityCode.condition3IsTrue_0;
gdjs.ActivityCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10075260);
}
}}
}
}
if (gdjs.ActivityCode.condition3IsTrue_0.val) {

{ //Subevents
gdjs.ActivityCode.eventsList0x97cc60(runtimeScene);} //End of subevents
}

}


{



}


}; //End of gdjs.ActivityCode.eventsList0xaff48


gdjs.ActivityCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.ActivityCode.GDplayerObjects1.length = 0;
gdjs.ActivityCode.GDplayerObjects2.length = 0;
gdjs.ActivityCode.GDplayerObjects3.length = 0;
gdjs.ActivityCode.GDgroundObjects1.length = 0;
gdjs.ActivityCode.GDgroundObjects2.length = 0;
gdjs.ActivityCode.GDgroundObjects3.length = 0;
gdjs.ActivityCode.GDbuttomObjects1.length = 0;
gdjs.ActivityCode.GDbuttomObjects2.length = 0;
gdjs.ActivityCode.GDbuttomObjects3.length = 0;
gdjs.ActivityCode.GDbackgroundObjects1.length = 0;
gdjs.ActivityCode.GDbackgroundObjects2.length = 0;
gdjs.ActivityCode.GDbackgroundObjects3.length = 0;
gdjs.ActivityCode.GDbackground2Objects1.length = 0;
gdjs.ActivityCode.GDbackground2Objects2.length = 0;
gdjs.ActivityCode.GDbackground2Objects3.length = 0;
gdjs.ActivityCode.GDblocosObjects1.length = 0;
gdjs.ActivityCode.GDblocosObjects2.length = 0;
gdjs.ActivityCode.GDblocosObjects3.length = 0;
gdjs.ActivityCode.GDdezenasObjects1.length = 0;
gdjs.ActivityCode.GDdezenasObjects2.length = 0;
gdjs.ActivityCode.GDdezenasObjects3.length = 0;
gdjs.ActivityCode.GDunidadesObjects1.length = 0;
gdjs.ActivityCode.GDunidadesObjects2.length = 0;
gdjs.ActivityCode.GDunidadesObjects3.length = 0;
gdjs.ActivityCode.GDdebugObjects1.length = 0;
gdjs.ActivityCode.GDdebugObjects2.length = 0;
gdjs.ActivityCode.GDdebugObjects3.length = 0;
gdjs.ActivityCode.GDlblUnidadesRespObjects1.length = 0;
gdjs.ActivityCode.GDlblUnidadesRespObjects2.length = 0;
gdjs.ActivityCode.GDlblUnidadesRespObjects3.length = 0;
gdjs.ActivityCode.GDlblDezenasRespObjects1.length = 0;
gdjs.ActivityCode.GDlblDezenasRespObjects2.length = 0;
gdjs.ActivityCode.GDlblDezenasRespObjects3.length = 0;
gdjs.ActivityCode.GDlblEnunciadoObjects1.length = 0;
gdjs.ActivityCode.GDlblEnunciadoObjects2.length = 0;
gdjs.ActivityCode.GDlblEnunciadoObjects3.length = 0;
gdjs.ActivityCode.GDlblConferirObjects1.length = 0;
gdjs.ActivityCode.GDlblConferirObjects2.length = 0;
gdjs.ActivityCode.GDlblConferirObjects3.length = 0;
gdjs.ActivityCode.GDlblUnidadesObjects1.length = 0;
gdjs.ActivityCode.GDlblUnidadesObjects2.length = 0;
gdjs.ActivityCode.GDlblUnidadesObjects3.length = 0;
gdjs.ActivityCode.GDlblDezenasObjects1.length = 0;
gdjs.ActivityCode.GDlblDezenasObjects2.length = 0;
gdjs.ActivityCode.GDlblDezenasObjects3.length = 0;
gdjs.ActivityCode.GDinputObjects1.length = 0;
gdjs.ActivityCode.GDinputObjects2.length = 0;
gdjs.ActivityCode.GDinputObjects3.length = 0;
gdjs.ActivityCode.GDNewObjectObjects1.length = 0;
gdjs.ActivityCode.GDNewObjectObjects2.length = 0;
gdjs.ActivityCode.GDNewObjectObjects3.length = 0;
gdjs.ActivityCode.GDhandObjects1.length = 0;
gdjs.ActivityCode.GDhandObjects2.length = 0;
gdjs.ActivityCode.GDhandObjects3.length = 0;

gdjs.ActivityCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['ActivityCode'] = gdjs.ActivityCode;
