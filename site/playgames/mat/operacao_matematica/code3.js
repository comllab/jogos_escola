gdjs.EndCode = {};
gdjs.EndCode.GDbuttomObjects1= [];
gdjs.EndCode.GDbuttomObjects2= [];
gdjs.EndCode.GDbackgroundObjects1= [];
gdjs.EndCode.GDbackgroundObjects2= [];
gdjs.EndCode.GDsiteObjects1= [];
gdjs.EndCode.GDsiteObjects2= [];
gdjs.EndCode.GDlblJogarObjects1= [];
gdjs.EndCode.GDlblJogarObjects2= [];
gdjs.EndCode.GDtituloObjects1= [];
gdjs.EndCode.GDtituloObjects2= [];

gdjs.EndCode.conditionTrue_0 = {val:false};
gdjs.EndCode.condition0IsTrue_0 = {val:false};
gdjs.EndCode.condition1IsTrue_0 = {val:false};
gdjs.EndCode.condition2IsTrue_0 = {val:false};
gdjs.EndCode.condition3IsTrue_0 = {val:false};
gdjs.EndCode.conditionTrue_1 = {val:false};
gdjs.EndCode.condition0IsTrue_1 = {val:false};
gdjs.EndCode.condition1IsTrue_1 = {val:false};
gdjs.EndCode.condition2IsTrue_1 = {val:false};
gdjs.EndCode.condition3IsTrue_1 = {val:false};


gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDbuttomObjects1Objects = Hashtable.newFrom({"buttom": gdjs.EndCode.GDbuttomObjects1});gdjs.EndCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.EndCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "preview.ogg", false, 100, 1);
}}

}


{

gdjs.EndCode.GDbuttomObjects1.createFrom(runtimeScene.getObjects("buttom"));

gdjs.EndCode.condition0IsTrue_0.val = false;
gdjs.EndCode.condition1IsTrue_0.val = false;
gdjs.EndCode.condition2IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDbuttomObjects1Objects, runtimeScene, true, false);
}if ( gdjs.EndCode.condition0IsTrue_0.val ) {
{
gdjs.EndCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.EndCode.condition1IsTrue_0.val ) {
{
{gdjs.EndCode.conditionTrue_1 = gdjs.EndCode.condition2IsTrue_0;
gdjs.EndCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(9810876);
}
}}
}
if (gdjs.EndCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Activity", false);
}}

}


}; //End of gdjs.EndCode.eventsList0xaff48


gdjs.EndCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.EndCode.GDbuttomObjects1.length = 0;
gdjs.EndCode.GDbuttomObjects2.length = 0;
gdjs.EndCode.GDbackgroundObjects1.length = 0;
gdjs.EndCode.GDbackgroundObjects2.length = 0;
gdjs.EndCode.GDsiteObjects1.length = 0;
gdjs.EndCode.GDsiteObjects2.length = 0;
gdjs.EndCode.GDlblJogarObjects1.length = 0;
gdjs.EndCode.GDlblJogarObjects2.length = 0;
gdjs.EndCode.GDtituloObjects1.length = 0;
gdjs.EndCode.GDtituloObjects2.length = 0;

gdjs.EndCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['EndCode'] = gdjs.EndCode;
