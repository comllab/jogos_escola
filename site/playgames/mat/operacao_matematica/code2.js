gdjs.ActivityCode = {};
gdjs.ActivityCode.GDbuttomObjects1= [];
gdjs.ActivityCode.GDbuttomObjects2= [];
gdjs.ActivityCode.GDbuttomObjects3= [];
gdjs.ActivityCode.GDbuttomObjects4= [];
gdjs.ActivityCode.GDbackgroundObjects1= [];
gdjs.ActivityCode.GDbackgroundObjects2= [];
gdjs.ActivityCode.GDbackgroundObjects3= [];
gdjs.ActivityCode.GDbackgroundObjects4= [];
gdjs.ActivityCode.GDnumerosObjects1= [];
gdjs.ActivityCode.GDnumerosObjects2= [];
gdjs.ActivityCode.GDnumerosObjects3= [];
gdjs.ActivityCode.GDnumerosObjects4= [];
gdjs.ActivityCode.GDoperandosObjects1= [];
gdjs.ActivityCode.GDoperandosObjects2= [];
gdjs.ActivityCode.GDoperandosObjects3= [];
gdjs.ActivityCode.GDoperandosObjects4= [];
gdjs.ActivityCode.GDoperadorObjects1= [];
gdjs.ActivityCode.GDoperadorObjects2= [];
gdjs.ActivityCode.GDoperadorObjects3= [];
gdjs.ActivityCode.GDoperadorObjects4= [];
gdjs.ActivityCode.GDlblResultado6Objects1= [];
gdjs.ActivityCode.GDlblResultado6Objects2= [];
gdjs.ActivityCode.GDlblResultado6Objects3= [];
gdjs.ActivityCode.GDlblResultado6Objects4= [];
gdjs.ActivityCode.GDlblResultado5Objects1= [];
gdjs.ActivityCode.GDlblResultado5Objects2= [];
gdjs.ActivityCode.GDlblResultado5Objects3= [];
gdjs.ActivityCode.GDlblResultado5Objects4= [];
gdjs.ActivityCode.GDlblResultado4Objects1= [];
gdjs.ActivityCode.GDlblResultado4Objects2= [];
gdjs.ActivityCode.GDlblResultado4Objects3= [];
gdjs.ActivityCode.GDlblResultado4Objects4= [];
gdjs.ActivityCode.GDlblResultado3Objects1= [];
gdjs.ActivityCode.GDlblResultado3Objects2= [];
gdjs.ActivityCode.GDlblResultado3Objects3= [];
gdjs.ActivityCode.GDlblResultado3Objects4= [];
gdjs.ActivityCode.GDlblResultado2Objects1= [];
gdjs.ActivityCode.GDlblResultado2Objects2= [];
gdjs.ActivityCode.GDlblResultado2Objects3= [];
gdjs.ActivityCode.GDlblResultado2Objects4= [];
gdjs.ActivityCode.GDlblResultado1Objects1= [];
gdjs.ActivityCode.GDlblResultado1Objects2= [];
gdjs.ActivityCode.GDlblResultado1Objects3= [];
gdjs.ActivityCode.GDlblResultado1Objects4= [];
gdjs.ActivityCode.GDlblTentativasObjects1= [];
gdjs.ActivityCode.GDlblTentativasObjects2= [];
gdjs.ActivityCode.GDlblTentativasObjects3= [];
gdjs.ActivityCode.GDlblTentativasObjects4= [];
gdjs.ActivityCode.GDlblTentarObjects1= [];
gdjs.ActivityCode.GDlblTentarObjects2= [];
gdjs.ActivityCode.GDlblTentarObjects3= [];
gdjs.ActivityCode.GDlblTentarObjects4= [];
gdjs.ActivityCode.GDlblContinuarObjects1= [];
gdjs.ActivityCode.GDlblContinuarObjects2= [];
gdjs.ActivityCode.GDlblContinuarObjects3= [];
gdjs.ActivityCode.GDlblContinuarObjects4= [];
gdjs.ActivityCode.GDresultadoObjects1= [];
gdjs.ActivityCode.GDresultadoObjects2= [];
gdjs.ActivityCode.GDresultadoObjects3= [];
gdjs.ActivityCode.GDresultadoObjects4= [];
gdjs.ActivityCode.GDtentativasObjects1= [];
gdjs.ActivityCode.GDtentativasObjects2= [];
gdjs.ActivityCode.GDtentativasObjects3= [];
gdjs.ActivityCode.GDtentativasObjects4= [];
gdjs.ActivityCode.GDfalhouObjects1= [];
gdjs.ActivityCode.GDfalhouObjects2= [];
gdjs.ActivityCode.GDfalhouObjects3= [];
gdjs.ActivityCode.GDfalhouObjects4= [];

gdjs.ActivityCode.conditionTrue_0 = {val:false};
gdjs.ActivityCode.condition0IsTrue_0 = {val:false};
gdjs.ActivityCode.condition1IsTrue_0 = {val:false};
gdjs.ActivityCode.condition2IsTrue_0 = {val:false};
gdjs.ActivityCode.condition3IsTrue_0 = {val:false};
gdjs.ActivityCode.condition4IsTrue_0 = {val:false};


gdjs.ActivityCode.eventsList0x6a0518 = function(runtimeScene) {

{


{
gdjs.ActivityCode.GDlblResultado1Objects3.createFrom(runtimeScene.getObjects("lblResultado1"));
gdjs.ActivityCode.GDlblResultado3Objects3.createFrom(runtimeScene.getObjects("lblResultado3"));
gdjs.ActivityCode.GDlblResultado4Objects3.createFrom(runtimeScene.getObjects("lblResultado4"));
gdjs.ActivityCode.GDlblResultado6Objects3.createFrom(runtimeScene.getObjects("lblResultado6"));
{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(0,  9));
}{runtimeScene.getVariables().getFromIndex(1).setNumber(gdjs.randomInRange(0,  9));
}{runtimeScene.getVariables().getFromIndex(6).setNumber(gdjs.randomInRange(0,  9));
}{runtimeScene.getVariables().getFromIndex(7).setNumber(gdjs.randomInRange(0,  9));
}{runtimeScene.getVariables().getFromIndex(2).setNumber(gdjs.randomInRange(0,  9));
}{runtimeScene.getVariables().getFromIndex(3).setNumber(gdjs.randomInRange(0,  9));
}{runtimeScene.getVariables().getFromIndex(8).setNumber(gdjs.randomInRange(0,  9));
}{runtimeScene.getVariables().getFromIndex(9).setNumber(gdjs.randomInRange(0,  9));
}{runtimeScene.getVariables().getFromIndex(4).setNumber(gdjs.randomInRange(0,  9));
}{runtimeScene.getVariables().getFromIndex(5).setNumber(gdjs.randomInRange(0,  9));
}{runtimeScene.getVariables().getFromIndex(10).setNumber(gdjs.randomInRange(0,  9));
}{runtimeScene.getVariables().getFromIndex(11).setNumber(gdjs.randomInRange(0,  9));
}{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado1Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado1Objects3[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) + gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1))));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado4Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado4Objects3[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(6)) + gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(7))));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado3Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado3Objects3[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(4))*gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(5))));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado6Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado6Objects3[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(10))*gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(11))));
}
}}

}


{


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) >= gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3));
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
gdjs.ActivityCode.GDlblResultado2Objects3.createFrom(runtimeScene.getObjects("lblResultado2"));
{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado2Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado2Objects3[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) - gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3))));
}
}}

}


{


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3));
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
gdjs.ActivityCode.GDlblResultado2Objects3.createFrom(runtimeScene.getObjects("lblResultado2"));
{runtimeScene.getVariables().get("t").setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}{runtimeScene.getVariables().getFromIndex(3).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)));
}{runtimeScene.getVariables().getFromIndex(2).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("t")));
}{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado2Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado2Objects3[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)) - gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3))));
}
}}

}


{


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(8)) >= gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(9));
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
gdjs.ActivityCode.GDlblResultado5Objects3.createFrom(runtimeScene.getObjects("lblResultado5"));
{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado5Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado5Objects3[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(8)) - gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(9))));
}
}}

}


{


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(8)) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(9));
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
gdjs.ActivityCode.GDlblResultado5Objects2.createFrom(runtimeScene.getObjects("lblResultado5"));
{runtimeScene.getVariables().get("t").setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(9)));
}{runtimeScene.getVariables().getFromIndex(9).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(8)));
}{runtimeScene.getVariables().getFromIndex(8).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("t")));
}{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado5Objects2.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado5Objects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(8)) - gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(9))));
}
}}

}


}; //End of gdjs.ActivityCode.eventsList0x6a0518
gdjs.ActivityCode.eventsList0xafc620 = function(runtimeScene) {

{



}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2))));
}
}}

}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation(10);
}
}}

}


{



}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 2 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(2)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2))));
}
}}

}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 2 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation(10);
}
}}

}


{



}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 3 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(4)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2))));
}
}}

}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 3 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(5)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation(10);
}
}}

}


{



}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 4 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(6)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation(10);
}
}}

}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 4 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(7)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2))));
}
}}

}


{



}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 5 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(8)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation(10);
}
}}

}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 5 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(9)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2))));
}
}}

}


{



}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 6 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(10)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation(10);
}
}}

}


{

gdjs.ActivityCode.GDoperandosObjects3.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(0)) == 6 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(1)) == 2 ) {
        gdjs.ActivityCode.condition1IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].returnVariable(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(11)));
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2))));
}
}}

}


{


{
}

}


}; //End of gdjs.ActivityCode.eventsList0xafc620
gdjs.ActivityCode.eventsList0xab2ef8 = function(runtimeScene) {

{



}


{

gdjs.ActivityCode.GDresultadoObjects3.createFrom(runtimeScene.getObjects("resultado"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDresultadoObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDresultadoObjects3[i].getVariableNumber(gdjs.ActivityCode.GDresultadoObjects3[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDresultadoObjects3[k] = gdjs.ActivityCode.GDresultadoObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDresultadoObjects3.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
gdjs.ActivityCode.GDlblResultado1Objects3.createFrom(runtimeScene.getObjects("lblResultado1"));
/* Reuse gdjs.ActivityCode.GDresultadoObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado1Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado1Objects3[i].setX((( gdjs.ActivityCode.GDresultadoObjects3.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects3[0].getPointX("")) + (( gdjs.ActivityCode.GDresultadoObjects3.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects3[0].getWidth())/2 - (gdjs.ActivityCode.GDlblResultado1Objects3[i].getWidth())/2);
}
}}

}


{

gdjs.ActivityCode.GDresultadoObjects3.createFrom(runtimeScene.getObjects("resultado"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDresultadoObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDresultadoObjects3[i].getVariableNumber(gdjs.ActivityCode.GDresultadoObjects3[i].getVariables().getFromIndex(0)) == 2 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDresultadoObjects3[k] = gdjs.ActivityCode.GDresultadoObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDresultadoObjects3.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
gdjs.ActivityCode.GDlblResultado2Objects3.createFrom(runtimeScene.getObjects("lblResultado2"));
/* Reuse gdjs.ActivityCode.GDresultadoObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado2Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado2Objects3[i].setX((( gdjs.ActivityCode.GDresultadoObjects3.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects3[0].getPointX("")) + (( gdjs.ActivityCode.GDresultadoObjects3.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects3[0].getWidth())/2 - (gdjs.ActivityCode.GDlblResultado2Objects3[i].getWidth())/2);
}
}}

}


{

gdjs.ActivityCode.GDresultadoObjects3.createFrom(runtimeScene.getObjects("resultado"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDresultadoObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDresultadoObjects3[i].getVariableNumber(gdjs.ActivityCode.GDresultadoObjects3[i].getVariables().getFromIndex(0)) == 3 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDresultadoObjects3[k] = gdjs.ActivityCode.GDresultadoObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDresultadoObjects3.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
gdjs.ActivityCode.GDlblResultado3Objects3.createFrom(runtimeScene.getObjects("lblResultado3"));
/* Reuse gdjs.ActivityCode.GDresultadoObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado3Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado3Objects3[i].setX((( gdjs.ActivityCode.GDresultadoObjects3.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects3[0].getPointX("")) + (( gdjs.ActivityCode.GDresultadoObjects3.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects3[0].getWidth())/2 - (gdjs.ActivityCode.GDlblResultado3Objects3[i].getWidth())/2);
}
}}

}


{

gdjs.ActivityCode.GDresultadoObjects3.createFrom(runtimeScene.getObjects("resultado"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDresultadoObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDresultadoObjects3[i].getVariableNumber(gdjs.ActivityCode.GDresultadoObjects3[i].getVariables().getFromIndex(0)) == 4 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDresultadoObjects3[k] = gdjs.ActivityCode.GDresultadoObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDresultadoObjects3.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
gdjs.ActivityCode.GDlblResultado4Objects3.createFrom(runtimeScene.getObjects("lblResultado4"));
/* Reuse gdjs.ActivityCode.GDresultadoObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado4Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado4Objects3[i].setX((( gdjs.ActivityCode.GDresultadoObjects3.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects3[0].getPointX("")) + (( gdjs.ActivityCode.GDresultadoObjects3.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects3[0].getWidth())/2 - (gdjs.ActivityCode.GDlblResultado4Objects3[i].getWidth())/2);
}
}}

}


{

gdjs.ActivityCode.GDresultadoObjects3.createFrom(runtimeScene.getObjects("resultado"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDresultadoObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDresultadoObjects3[i].getVariableNumber(gdjs.ActivityCode.GDresultadoObjects3[i].getVariables().getFromIndex(0)) == 5 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDresultadoObjects3[k] = gdjs.ActivityCode.GDresultadoObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDresultadoObjects3.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
gdjs.ActivityCode.GDlblResultado5Objects3.createFrom(runtimeScene.getObjects("lblResultado5"));
/* Reuse gdjs.ActivityCode.GDresultadoObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado5Objects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado5Objects3[i].setX((( gdjs.ActivityCode.GDresultadoObjects3.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects3[0].getPointX("")) + (( gdjs.ActivityCode.GDresultadoObjects3.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects3[0].getWidth())/2 - (gdjs.ActivityCode.GDlblResultado5Objects3[i].getWidth())/2);
}
}}

}


{

gdjs.ActivityCode.GDresultadoObjects2.createFrom(runtimeScene.getObjects("resultado"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDresultadoObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDresultadoObjects2[i].getVariableNumber(gdjs.ActivityCode.GDresultadoObjects2[i].getVariables().getFromIndex(0)) == 6 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDresultadoObjects2[k] = gdjs.ActivityCode.GDresultadoObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDresultadoObjects2.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
gdjs.ActivityCode.GDlblResultado6Objects2.createFrom(runtimeScene.getObjects("lblResultado6"));
/* Reuse gdjs.ActivityCode.GDresultadoObjects2 */
{for(var i = 0, len = gdjs.ActivityCode.GDlblResultado6Objects2.length ;i < len;++i) {
    gdjs.ActivityCode.GDlblResultado6Objects2[i].setX((( gdjs.ActivityCode.GDresultadoObjects2.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects2[0].getPointX("")) + (( gdjs.ActivityCode.GDresultadoObjects2.length === 0 ) ? 0 :gdjs.ActivityCode.GDresultadoObjects2[0].getWidth())/2 - (gdjs.ActivityCode.GDlblResultado6Objects2[i].getWidth())/2);
}
}}

}


}; //End of gdjs.ActivityCode.eventsList0xab2ef8
gdjs.ActivityCode.eventsList0xaa5ed0 = function(runtimeScene) {

{

gdjs.ActivityCode.GDnumerosObjects2.createFrom(runtimeScene.getObjects("numeros"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDnumerosObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDnumerosObjects2[i].getVariableNumber(gdjs.ActivityCode.GDnumerosObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDnumerosObjects2[k] = gdjs.ActivityCode.GDnumerosObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDnumerosObjects2.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDnumerosObjects2 */
{for(var i = 0, len = gdjs.ActivityCode.GDnumerosObjects2.length ;i < len;++i) {
    gdjs.ActivityCode.GDnumerosObjects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)));
}
}}

}


{

gdjs.ActivityCode.GDnumerosObjects2.createFrom(runtimeScene.getObjects("numeros"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDnumerosObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDnumerosObjects2[i].getVariableNumber(gdjs.ActivityCode.GDnumerosObjects2[i].getVariables().getFromIndex(0)) == 2 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDnumerosObjects2[k] = gdjs.ActivityCode.GDnumerosObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDnumerosObjects2.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDnumerosObjects2 */
{for(var i = 0, len = gdjs.ActivityCode.GDnumerosObjects2.length ;i < len;++i) {
    gdjs.ActivityCode.GDnumerosObjects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(5)));
}
}}

}


{

gdjs.ActivityCode.GDnumerosObjects2.createFrom(runtimeScene.getObjects("numeros"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDnumerosObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDnumerosObjects2[i].getVariableNumber(gdjs.ActivityCode.GDnumerosObjects2[i].getVariables().getFromIndex(0)) == 3 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDnumerosObjects2[k] = gdjs.ActivityCode.GDnumerosObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDnumerosObjects2.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDnumerosObjects2 */
{for(var i = 0, len = gdjs.ActivityCode.GDnumerosObjects2.length ;i < len;++i) {
    gdjs.ActivityCode.GDnumerosObjects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)));
}
}}

}


{

gdjs.ActivityCode.GDnumerosObjects2.createFrom(runtimeScene.getObjects("numeros"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDnumerosObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDnumerosObjects2[i].getVariableNumber(gdjs.ActivityCode.GDnumerosObjects2[i].getVariables().getFromIndex(0)) == 4 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDnumerosObjects2[k] = gdjs.ActivityCode.GDnumerosObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDnumerosObjects2.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDnumerosObjects2 */
{for(var i = 0, len = gdjs.ActivityCode.GDnumerosObjects2.length ;i < len;++i) {
    gdjs.ActivityCode.GDnumerosObjects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(10)));
}
}}

}


{

gdjs.ActivityCode.GDnumerosObjects2.createFrom(runtimeScene.getObjects("numeros"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDnumerosObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDnumerosObjects2[i].getVariableNumber(gdjs.ActivityCode.GDnumerosObjects2[i].getVariables().getFromIndex(0)) == 5 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDnumerosObjects2[k] = gdjs.ActivityCode.GDnumerosObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDnumerosObjects2.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDnumerosObjects2 */
{for(var i = 0, len = gdjs.ActivityCode.GDnumerosObjects2.length ;i < len;++i) {
    gdjs.ActivityCode.GDnumerosObjects2[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(6)));
}
}}

}


{

gdjs.ActivityCode.GDnumerosObjects1.createFrom(runtimeScene.getObjects("numeros"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDnumerosObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDnumerosObjects1[i].getVariableNumber(gdjs.ActivityCode.GDnumerosObjects1[i].getVariables().getFromIndex(0)) == 6 ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDnumerosObjects1[k] = gdjs.ActivityCode.GDnumerosObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDnumerosObjects1.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDnumerosObjects1 */
{for(var i = 0, len = gdjs.ActivityCode.GDnumerosObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDnumerosObjects1[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(8)));
}
}}

}


}; //End of gdjs.ActivityCode.eventsList0xaa5ed0
gdjs.ActivityCode.eventsList0x6a01f8 = function(runtimeScene) {

{


gdjs.ActivityCode.eventsList0x6a0518(runtimeScene);
}


{


gdjs.ActivityCode.eventsList0xafc620(runtimeScene);
}


{



}


{


gdjs.ActivityCode.eventsList0xab2ef8(runtimeScene);
}


{


gdjs.ActivityCode.eventsList0xaa5ed0(runtimeScene);
}


}; //End of gdjs.ActivityCode.eventsList0x6a01f8
gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDnumerosObjects1Objects = Hashtable.newFrom({"numeros": gdjs.ActivityCode.GDnumerosObjects1});gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDnumerosObjects2Objects = Hashtable.newFrom({"numeros": gdjs.ActivityCode.GDnumerosObjects2});gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDoperandosObjects2Objects = Hashtable.newFrom({"operandos": gdjs.ActivityCode.GDoperandosObjects2});gdjs.ActivityCode.eventsList0x6aead8 = function(runtimeScene) {

{

gdjs.ActivityCode.GDnumerosObjects3.createFrom(gdjs.ActivityCode.GDnumerosObjects2);

gdjs.ActivityCode.GDoperandosObjects3.createFrom(gdjs.ActivityCode.GDoperandosObjects2);


gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
gdjs.ActivityCode.condition3IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggX")) != 0;
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggY")) != 0;
}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects3[i].getAnimation() == 10 ) {
        gdjs.ActivityCode.condition2IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects3[k] = gdjs.ActivityCode.GDoperandosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects3.length = k;}if ( gdjs.ActivityCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDnumerosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDnumerosObjects3[i].getAnimation() == (gdjs.RuntimeObject.getVariableNumber(((gdjs.ActivityCode.GDoperandosObjects3.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.ActivityCode.GDoperandosObjects3[0].getVariables()).getFromIndex(2))) ) {
        gdjs.ActivityCode.condition3IsTrue_0.val = true;
        gdjs.ActivityCode.GDnumerosObjects3[k] = gdjs.ActivityCode.GDnumerosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDnumerosObjects3.length = k;}}
}
}
if (gdjs.ActivityCode.condition3IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDnumerosObjects3 */
/* Reuse gdjs.ActivityCode.GDoperandosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDnumerosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDnumerosObjects3[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.ActivityCode.GDoperandosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDoperandosObjects3[i].setAnimation((gdjs.RuntimeObject.getVariableNumber(gdjs.ActivityCode.GDoperandosObjects3[i].getVariables().getFromIndex(2))));
}
}{runtimeScene.getVariables().get("draggX").setNumber(0);
}{runtimeScene.getVariables().get("draggY").setNumber(0);
}}

}


{

gdjs.ActivityCode.GDnumerosObjects3.createFrom(gdjs.ActivityCode.GDnumerosObjects2);

gdjs.ActivityCode.GDoperandosObjects3.createFrom(gdjs.ActivityCode.GDoperandosObjects2);


gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggX")) != 0;
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggY")) != 0;
}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDnumerosObjects3.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDnumerosObjects3[i].getAnimation() != (gdjs.RuntimeObject.getVariableNumber(((gdjs.ActivityCode.GDoperandosObjects3.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.ActivityCode.GDoperandosObjects3[0].getVariables()).getFromIndex(2))) ) {
        gdjs.ActivityCode.condition2IsTrue_0.val = true;
        gdjs.ActivityCode.GDnumerosObjects3[k] = gdjs.ActivityCode.GDnumerosObjects3[i];
        ++k;
    }
}
gdjs.ActivityCode.GDnumerosObjects3.length = k;}}
}
if (gdjs.ActivityCode.condition2IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDnumerosObjects3 */
{for(var i = 0, len = gdjs.ActivityCode.GDnumerosObjects3.length ;i < len;++i) {
    gdjs.ActivityCode.GDnumerosObjects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggY")));
}
}{runtimeScene.getVariables().get("draggX").setNumber(0);
}{runtimeScene.getVariables().get("draggY").setNumber(0);
}{runtimeScene.getVariables().getFromIndex(12).sub(1);
}}

}


{

/* Reuse gdjs.ActivityCode.GDoperandosObjects2 */

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggX")) != 0;
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggY")) != 0;
}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDoperandosObjects2.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDoperandosObjects2[i].getAnimation() != 10 ) {
        gdjs.ActivityCode.condition2IsTrue_0.val = true;
        gdjs.ActivityCode.GDoperandosObjects2[k] = gdjs.ActivityCode.GDoperandosObjects2[i];
        ++k;
    }
}
gdjs.ActivityCode.GDoperandosObjects2.length = k;}}
}
if (gdjs.ActivityCode.condition2IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDnumerosObjects2 */
{for(var i = 0, len = gdjs.ActivityCode.GDnumerosObjects2.length ;i < len;++i) {
    gdjs.ActivityCode.GDnumerosObjects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggY")));
}
}{runtimeScene.getVariables().get("draggX").setNumber(0);
}{runtimeScene.getVariables().get("draggY").setNumber(0);
}{runtimeScene.getVariables().getFromIndex(12).sub(1);
}}

}


}; //End of gdjs.ActivityCode.eventsList0x6aead8
gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDnumerosObjects1Objects = Hashtable.newFrom({"numeros": gdjs.ActivityCode.GDnumerosObjects1});gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDoperandosObjects1Objects = Hashtable.newFrom({"operandos": gdjs.ActivityCode.GDoperandosObjects1});gdjs.ActivityCode.eventsList0x6ae920 = function(runtimeScene) {

{

gdjs.ActivityCode.GDnumerosObjects2.createFrom(gdjs.ActivityCode.GDnumerosObjects1);

gdjs.ActivityCode.GDoperandosObjects2.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDnumerosObjects2Objects, gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDoperandosObjects2Objects, false, runtimeScene);
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.ActivityCode.eventsList0x6aead8(runtimeScene);} //End of subevents
}

}


{

/* Reuse gdjs.ActivityCode.GDnumerosObjects1 */
gdjs.ActivityCode.GDoperandosObjects1.createFrom(runtimeScene.getObjects("operandos"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggX")) != 0;
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggY")) != 0;
}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
gdjs.ActivityCode.condition2IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDnumerosObjects1Objects, gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDoperandosObjects1Objects, true, runtimeScene);
}}
}
if (gdjs.ActivityCode.condition2IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDnumerosObjects1 */
{for(var i = 0, len = gdjs.ActivityCode.GDnumerosObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDnumerosObjects1[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggY")));
}
}{runtimeScene.getVariables().get("draggX").setNumber(0);
}{runtimeScene.getVariables().get("draggY").setNumber(0);
}{runtimeScene.getVariables().getFromIndex(12).sub(1);
}}

}


}; //End of gdjs.ActivityCode.eventsList0x6ae920
gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDlblTentarObjects1Objects = Hashtable.newFrom({"lblTentar": gdjs.ActivityCode.GDlblTentarObjects1});gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDlblContinuarObjects1Objects = Hashtable.newFrom({"lblContinuar": gdjs.ActivityCode.GDlblContinuarObjects1});gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDnumerosObjects1Objects = Hashtable.newFrom({"numeros": gdjs.ActivityCode.GDnumerosObjects1});gdjs.ActivityCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "Numeros1", 0, 0);
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "Falhou");
}{runtimeScene.getVariables().getFromIndex(12).setNumber(6);
}{gdjs.evtTools.sound.playSound(runtimeScene, "preview.ogg", false, 100, 1);
}
{ //Subevents
gdjs.ActivityCode.eventsList0x6a01f8(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.ActivityCode.GDnumerosObjects1.createFrom(runtimeScene.getObjects("numeros"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDnumerosObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDnumerosObjects1[i].getBehavior("Draggable").isDragged() ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDnumerosObjects1[k] = gdjs.ActivityCode.GDnumerosObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDnumerosObjects1.length = k;}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggX")) == 0;
}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
gdjs.ActivityCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("draggY")) == 0;
}}
}
if (gdjs.ActivityCode.condition2IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDnumerosObjects1 */
{runtimeScene.getVariables().get("draggX").setNumber((( gdjs.ActivityCode.GDnumerosObjects1.length === 0 ) ? 0 :gdjs.ActivityCode.GDnumerosObjects1[0].getPointX("")));
}{runtimeScene.getVariables().get("draggY").setNumber((( gdjs.ActivityCode.GDnumerosObjects1.length === 0 ) ? 0 :gdjs.ActivityCode.GDnumerosObjects1[0].getPointY("")));
}}

}


{

gdjs.ActivityCode.GDnumerosObjects1.createFrom(runtimeScene.getObjects("numeros"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDnumerosObjects1Objects, runtimeScene, true, false);
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.ActivityCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.ActivityCode.eventsList0x6ae920(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.ActivityCode.GDtentativasObjects1.createFrom(runtimeScene.getObjects("tentativas"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ActivityCode.GDtentativasObjects1.length;i<l;++i) {
    if ( gdjs.ActivityCode.GDtentativasObjects1[i].getVariableNumber(gdjs.ActivityCode.GDtentativasObjects1[i].getVariables().getFromIndex(0)) > gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(12)) ) {
        gdjs.ActivityCode.condition0IsTrue_0.val = true;
        gdjs.ActivityCode.GDtentativasObjects1[k] = gdjs.ActivityCode.GDtentativasObjects1[i];
        ++k;
    }
}
gdjs.ActivityCode.GDtentativasObjects1.length = k;}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ActivityCode.GDtentativasObjects1 */
{for(var i = 0, len = gdjs.ActivityCode.GDtentativasObjects1.length ;i < len;++i) {
    gdjs.ActivityCode.GDtentativasObjects1[i].hide();
}
}}

}


{


gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(12)) == 0;
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "Falhou");
}}

}


{

gdjs.ActivityCode.GDlblTentarObjects1.createFrom(runtimeScene.getObjects("lblTentar"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Falhou");
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDlblTentarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
gdjs.ActivityCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
if (gdjs.ActivityCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Activity", false);
}}

}


{

gdjs.ActivityCode.GDlblContinuarObjects1.createFrom(runtimeScene.getObjects("lblContinuar"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
gdjs.ActivityCode.condition1IsTrue_0.val = false;
gdjs.ActivityCode.condition2IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Continuar");
}if ( gdjs.ActivityCode.condition0IsTrue_0.val ) {
{
gdjs.ActivityCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDlblContinuarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.ActivityCode.condition1IsTrue_0.val ) {
{
gdjs.ActivityCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
if (gdjs.ActivityCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(5).add(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{



}


{

gdjs.ActivityCode.GDnumerosObjects1.createFrom(runtimeScene.getObjects("numeros"));

gdjs.ActivityCode.condition0IsTrue_0.val = false;
{
gdjs.ActivityCode.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.ActivityCode.mapOfGDgdjs_46ActivityCode_46GDnumerosObjects1Objects) == 0;
}if (gdjs.ActivityCode.condition0IsTrue_0.val) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "Continuar");
}}

}


}; //End of gdjs.ActivityCode.eventsList0xaff48


gdjs.ActivityCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.ActivityCode.GDbuttomObjects1.length = 0;
gdjs.ActivityCode.GDbuttomObjects2.length = 0;
gdjs.ActivityCode.GDbuttomObjects3.length = 0;
gdjs.ActivityCode.GDbuttomObjects4.length = 0;
gdjs.ActivityCode.GDbackgroundObjects1.length = 0;
gdjs.ActivityCode.GDbackgroundObjects2.length = 0;
gdjs.ActivityCode.GDbackgroundObjects3.length = 0;
gdjs.ActivityCode.GDbackgroundObjects4.length = 0;
gdjs.ActivityCode.GDnumerosObjects1.length = 0;
gdjs.ActivityCode.GDnumerosObjects2.length = 0;
gdjs.ActivityCode.GDnumerosObjects3.length = 0;
gdjs.ActivityCode.GDnumerosObjects4.length = 0;
gdjs.ActivityCode.GDoperandosObjects1.length = 0;
gdjs.ActivityCode.GDoperandosObjects2.length = 0;
gdjs.ActivityCode.GDoperandosObjects3.length = 0;
gdjs.ActivityCode.GDoperandosObjects4.length = 0;
gdjs.ActivityCode.GDoperadorObjects1.length = 0;
gdjs.ActivityCode.GDoperadorObjects2.length = 0;
gdjs.ActivityCode.GDoperadorObjects3.length = 0;
gdjs.ActivityCode.GDoperadorObjects4.length = 0;
gdjs.ActivityCode.GDlblResultado6Objects1.length = 0;
gdjs.ActivityCode.GDlblResultado6Objects2.length = 0;
gdjs.ActivityCode.GDlblResultado6Objects3.length = 0;
gdjs.ActivityCode.GDlblResultado6Objects4.length = 0;
gdjs.ActivityCode.GDlblResultado5Objects1.length = 0;
gdjs.ActivityCode.GDlblResultado5Objects2.length = 0;
gdjs.ActivityCode.GDlblResultado5Objects3.length = 0;
gdjs.ActivityCode.GDlblResultado5Objects4.length = 0;
gdjs.ActivityCode.GDlblResultado4Objects1.length = 0;
gdjs.ActivityCode.GDlblResultado4Objects2.length = 0;
gdjs.ActivityCode.GDlblResultado4Objects3.length = 0;
gdjs.ActivityCode.GDlblResultado4Objects4.length = 0;
gdjs.ActivityCode.GDlblResultado3Objects1.length = 0;
gdjs.ActivityCode.GDlblResultado3Objects2.length = 0;
gdjs.ActivityCode.GDlblResultado3Objects3.length = 0;
gdjs.ActivityCode.GDlblResultado3Objects4.length = 0;
gdjs.ActivityCode.GDlblResultado2Objects1.length = 0;
gdjs.ActivityCode.GDlblResultado2Objects2.length = 0;
gdjs.ActivityCode.GDlblResultado2Objects3.length = 0;
gdjs.ActivityCode.GDlblResultado2Objects4.length = 0;
gdjs.ActivityCode.GDlblResultado1Objects1.length = 0;
gdjs.ActivityCode.GDlblResultado1Objects2.length = 0;
gdjs.ActivityCode.GDlblResultado1Objects3.length = 0;
gdjs.ActivityCode.GDlblResultado1Objects4.length = 0;
gdjs.ActivityCode.GDlblTentativasObjects1.length = 0;
gdjs.ActivityCode.GDlblTentativasObjects2.length = 0;
gdjs.ActivityCode.GDlblTentativasObjects3.length = 0;
gdjs.ActivityCode.GDlblTentativasObjects4.length = 0;
gdjs.ActivityCode.GDlblTentarObjects1.length = 0;
gdjs.ActivityCode.GDlblTentarObjects2.length = 0;
gdjs.ActivityCode.GDlblTentarObjects3.length = 0;
gdjs.ActivityCode.GDlblTentarObjects4.length = 0;
gdjs.ActivityCode.GDlblContinuarObjects1.length = 0;
gdjs.ActivityCode.GDlblContinuarObjects2.length = 0;
gdjs.ActivityCode.GDlblContinuarObjects3.length = 0;
gdjs.ActivityCode.GDlblContinuarObjects4.length = 0;
gdjs.ActivityCode.GDresultadoObjects1.length = 0;
gdjs.ActivityCode.GDresultadoObjects2.length = 0;
gdjs.ActivityCode.GDresultadoObjects3.length = 0;
gdjs.ActivityCode.GDresultadoObjects4.length = 0;
gdjs.ActivityCode.GDtentativasObjects1.length = 0;
gdjs.ActivityCode.GDtentativasObjects2.length = 0;
gdjs.ActivityCode.GDtentativasObjects3.length = 0;
gdjs.ActivityCode.GDtentativasObjects4.length = 0;
gdjs.ActivityCode.GDfalhouObjects1.length = 0;
gdjs.ActivityCode.GDfalhouObjects2.length = 0;
gdjs.ActivityCode.GDfalhouObjects3.length = 0;
gdjs.ActivityCode.GDfalhouObjects4.length = 0;

gdjs.ActivityCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['ActivityCode'] = gdjs.ActivityCode;
