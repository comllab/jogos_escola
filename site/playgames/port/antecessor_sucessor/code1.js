gdjs.GameCode = {};
gdjs.GameCode.GDbackgroundObjects1= [];
gdjs.GameCode.GDbackgroundObjects2= [];
gdjs.GameCode.GDbackgroundObjects3= [];
gdjs.GameCode.GDbarreObjects1= [];
gdjs.GameCode.GDbarreObjects2= [];
gdjs.GameCode.GDbarreObjects3= [];
gdjs.GameCode.GDbrickObjects1= [];
gdjs.GameCode.GDbrickObjects2= [];
gdjs.GameCode.GDbrickObjects3= [];
gdjs.GameCode.GDballeObjects1= [];
gdjs.GameCode.GDballeObjects2= [];
gdjs.GameCode.GDballeObjects3= [];
gdjs.GameCode.GDopObjects1= [];
gdjs.GameCode.GDopObjects2= [];
gdjs.GameCode.GDopObjects3= [];
gdjs.GameCode.GDwallObjects1= [];
gdjs.GameCode.GDwallObjects2= [];
gdjs.GameCode.GDwallObjects3= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};


gdjs.GameCode.eventsList0x8130f8 = function(runtimeScene, context) {

{

gdjs.GameCode.GDbrickObjects1.createFrom(runtimeScene.getObjects("brick"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbrickObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbrickObjects1[i].getVariableNumber(gdjs.GameCode.GDbrickObjects1[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbrickObjects1[k] = gdjs.GameCode.GDbrickObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbrickObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbrickObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbrickObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbrickObjects1[i].returnVariable(gdjs.GameCode.GDbrickObjects1[i].getVariables().getFromIndex(0)).setNumber(gdjs.randomInRange(11,  98));
}
}}

}


}; //End of gdjs.GameCode.eventsList0x8130f8
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDballeObjects1Objects = Hashtable.newFrom({"balle": gdjs.GameCode.GDballeObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbrickObjects1Objects = Hashtable.newFrom({"brick": gdjs.GameCode.GDbrickObjects1});gdjs.GameCode.eventsList0x817938 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("device").setString("mobile");
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = !(gdjs.evtTools.systemInfo.isMobile());
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("device").setString("pc");
}}

}


}; //End of gdjs.GameCode.eventsList0x817938
gdjs.GameCode.eventsList0x81e848 = function(runtimeScene, context) {

{

gdjs.GameCode.GDbarreObjects2.createFrom(runtimeScene.getObjects("barre"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = !(gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right"));
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbarreObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDbarreObjects2[i].getX() > 1 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDbarreObjects2[k] = gdjs.GameCode.GDbarreObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDbarreObjects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbarreObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDbarreObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbarreObjects2[i].setX(gdjs.GameCode.GDbarreObjects2[i].getX() - (10));
}
}}

}


{

gdjs.GameCode.GDbarreObjects2.createFrom(runtimeScene.getObjects("barre"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = !(gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left"));
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbarreObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDbarreObjects2[i].getX() < gdjs.evtTools.window.getCanvasWidth(runtimeScene) - (gdjs.GameCode.GDbarreObjects2[i].getWidth()) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDbarreObjects2[k] = gdjs.GameCode.GDbarreObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDbarreObjects2.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbarreObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDbarreObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbarreObjects2[i].setX(gdjs.GameCode.GDbarreObjects2[i].getX() + (10));
}
}}

}


{

gdjs.GameCode.GDbarreObjects1.createFrom(runtimeScene.getObjects("barre"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.getMouseX(runtimeScene, "", 0) > 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.getMouseX(runtimeScene, "", 0) < gdjs.evtTools.window.getCanvasWidth(runtimeScene) - (( gdjs.GameCode.GDbarreObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDbarreObjects1[0].getWidth());
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbarreObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbarreObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbarreObjects1[i].setX(gdjs.evtTools.input.getMouseX(runtimeScene, "", 0));
}
}}

}


}; //End of gdjs.GameCode.eventsList0x81e848
gdjs.GameCode.eventsList0x81b688 = function(runtimeScene, context) {

{

gdjs.GameCode.GDbarreObjects2.createFrom(runtimeScene.getObjects("barre"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbarreObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDbarreObjects2[i].getX() < gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 45 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbarreObjects2[k] = gdjs.GameCode.GDbarreObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDbarreObjects2.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbarreObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDbarreObjects2[i].getX() < gdjs.evtTools.input.getMouseX(runtimeScene, "", 0) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDbarreObjects2[k] = gdjs.GameCode.GDbarreObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDbarreObjects2.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbarreObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDbarreObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbarreObjects2[i].setX(gdjs.GameCode.GDbarreObjects2[i].getX() + (10));
}
}}

}


{

gdjs.GameCode.GDbarreObjects1.createFrom(runtimeScene.getObjects("barre"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbarreObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbarreObjects1[i].getX() > 38 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbarreObjects1[k] = gdjs.GameCode.GDbarreObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbarreObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbarreObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbarreObjects1[i].getX() > gdjs.evtTools.input.getMouseX(runtimeScene, "", 0) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDbarreObjects1[k] = gdjs.GameCode.GDbarreObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbarreObjects1.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbarreObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbarreObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbarreObjects1[i].setX(gdjs.GameCode.GDbarreObjects1[i].getX() - (10));
}
}}

}


}; //End of gdjs.GameCode.eventsList0x81b688
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDballeObjects1Objects = Hashtable.newFrom({"balle": gdjs.GameCode.GDballeObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbrickObjects1Objects = Hashtable.newFrom({"brick": gdjs.GameCode.GDbrickObjects1});gdjs.GameCode.eventsList0x935ab8 = function(runtimeScene, context) {

{

gdjs.GameCode.GDballeObjects2.createFrom(gdjs.GameCode.GDballeObjects1);

gdjs.GameCode.GDbrickObjects2.createFrom(gdjs.GameCode.GDbrickObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDballeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDballeObjects2[i].getX() < (( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getPointX(""))+2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDballeObjects2[k] = gdjs.GameCode.GDballeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDballeObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDballeObjects2 */
/* Reuse gdjs.GameCode.GDbrickObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].clearForces();
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].addPolarForce(180-(gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDballeObjects2[i].getVariables().get("ValeurAngle")))+gdjs.random(5)-gdjs.random(5), 250, 1);
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].setX((( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getPointX(""))-(gdjs.GameCode.GDballeObjects2[i].getWidth()));
}
}}

}


{

gdjs.GameCode.GDballeObjects2.createFrom(gdjs.GameCode.GDballeObjects1);

gdjs.GameCode.GDbrickObjects2.createFrom(gdjs.GameCode.GDbrickObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDballeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDballeObjects2[i].getX() > (( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getPointX(""))+(( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getWidth())-(( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getWidth())/2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDballeObjects2[k] = gdjs.GameCode.GDballeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDballeObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDballeObjects2 */
/* Reuse gdjs.GameCode.GDbrickObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].clearForces();
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].addPolarForce(180-(gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDballeObjects2[i].getVariables().get("ValeurAngle")))+gdjs.random(5)-gdjs.random(5), 250, 1);
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].setX((( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getPointX(""))+(( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getWidth()));
}
}}

}


{

gdjs.GameCode.GDballeObjects2.createFrom(gdjs.GameCode.GDballeObjects1);

gdjs.GameCode.GDbrickObjects2.createFrom(gdjs.GameCode.GDbrickObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDballeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDballeObjects2[i].getY() < (( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getPointY(""))+1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDballeObjects2[k] = gdjs.GameCode.GDballeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDballeObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDballeObjects2 */
/* Reuse gdjs.GameCode.GDbrickObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].clearForces();
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].addPolarForce(0-(gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDballeObjects2[i].getVariables().get("ValeurAngle")))+gdjs.random(5)-gdjs.random(5), 250, 1);
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].setY((( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getPointY(""))-(gdjs.GameCode.GDballeObjects2[i].getHeight()));
}
}}

}


{

gdjs.GameCode.GDballeObjects2.createFrom(gdjs.GameCode.GDballeObjects1);

gdjs.GameCode.GDbrickObjects2.createFrom(gdjs.GameCode.GDbrickObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDballeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDballeObjects2[i].getY() > (( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getPointY(""))+(( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getHeight())-(( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getHeight())/2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDballeObjects2[k] = gdjs.GameCode.GDballeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDballeObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDballeObjects2 */
/* Reuse gdjs.GameCode.GDbrickObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].clearForces();
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].addPolarForce(0-(gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDballeObjects2[i].getVariables().get("ValeurAngle")))+gdjs.random(5)-gdjs.random(5), 250, 1);
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects2[i].setY((( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getPointY(""))+(( gdjs.GameCode.GDbrickObjects2.length === 0 ) ? 0 :gdjs.GameCode.GDbrickObjects2[0].getHeight()));
}
}}

}


{


{
gdjs.GameCode.GDbrickObjects2.createFrom(gdjs.GameCode.GDbrickObjects1);

{for(var i = 0, len = gdjs.GameCode.GDbrickObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbrickObjects2[i].deleteFromScene(runtimeScene);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(7469228);
}
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ball_sound.ogg", false, 100, 1);
}}

}


}; //End of gdjs.GameCode.eventsList0x935ab8
gdjs.GameCode.eventsList0x9329c0 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(9636460);
}
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ball_sound.ogg", false, 100, 1);
}}

}


}; //End of gdjs.GameCode.eventsList0x9329c0
gdjs.GameCode.eventsList0x925ad0 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(9637668);
}
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ball_sound.ogg", false, 100, 1);
}}

}


}; //End of gdjs.GameCode.eventsList0x925ad0
gdjs.GameCode.eventsList0x9351b8 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(9661068);
}
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ball_sound.ogg", false, 100, 1);
}}

}


}; //End of gdjs.GameCode.eventsList0x9351b8
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDballeObjects1Objects = Hashtable.newFrom({"balle": gdjs.GameCode.GDballeObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbarreObjects1Objects = Hashtable.newFrom({"barre": gdjs.GameCode.GDbarreObjects1});gdjs.GameCode.eventsList0x936588 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(9651124);
}
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ball_sound.ogg", false, 100, 1);
}}

}


}; //End of gdjs.GameCode.eventsList0x936588
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbrickObjects1Objects = Hashtable.newFrom({"brick": gdjs.GameCode.GDbrickObjects1});gdjs.GameCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "senario" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)), 0, 0);
}
{ //Subevents
gdjs.GameCode.eventsList0x8130f8(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Boss Theme.ogg", 1, false, 40, 1);
}}

}


{

gdjs.GameCode.GDballeObjects1.createFrom(runtimeScene.getObjects("balle"));
gdjs.GameCode.GDbrickObjects1.createFrom(runtimeScene.getObjects("brick"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDballeObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbrickObjects1Objects, false, runtimeScene);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbrickObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbrickObjects1[i].getVariableNumber(gdjs.GameCode.GDbrickObjects1[i].getVariables().getFromIndex(0)) > 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDbrickObjects1[k] = gdjs.GameCode.GDbrickObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbrickObjects1.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbrickObjects1 */
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber((gdjs.RuntimeObject.getVariableNumber(((gdjs.GameCode.GDbrickObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.GameCode.GDbrickObjects1[0].getVariables()).getFromIndex(0))));
}{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "Resolva");
}}

}


{



}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x817938(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("device")) == "pc";
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x81e848(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("device")) == "mobile";
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x81b688(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDballeObjects1.createFrom(runtimeScene.getObjects("balle"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDballeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDballeObjects1[i].hasNoForces() ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDballeObjects1[k] = gdjs.GameCode.GDballeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDballeObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDballeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].addPolarForce(-45+gdjs.random(5)-gdjs.random(5)
, 250, 1);
}
}}

}


{



}


{

gdjs.GameCode.GDballeObjects1.createFrom(runtimeScene.getObjects("balle"));
gdjs.GameCode.GDbrickObjects1.createFrom(runtimeScene.getObjects("brick"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDballeObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbrickObjects1Objects, false, runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDballeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].returnVariable(gdjs.GameCode.GDballeObjects1[i].getVariables().get("ValeurAngle")).setNumber((gdjs.GameCode.GDballeObjects1[i].getAverageForce().getAngle()));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x935ab8(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDballeObjects1.createFrom(runtimeScene.getObjects("balle"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDballeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDballeObjects1[i].getY() < 10 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDballeObjects1[k] = gdjs.GameCode.GDballeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDballeObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDballeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].returnVariable(gdjs.GameCode.GDballeObjects1[i].getVariables().get("ValeurAngle")).setNumber((gdjs.GameCode.GDballeObjects1[i].getAverageForce().getAngle()));
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].addPolarForce(0-(gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDballeObjects1[i].getVariables().get("ValeurAngle")))+gdjs.random(5)-gdjs.random(5), 250, 1);
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].setY(11);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x9329c0(runtimeScene, context);} //End of subevents
}

}


{

gdjs.GameCode.GDballeObjects1.createFrom(runtimeScene.getObjects("balle"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDballeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDballeObjects1[i].getX() > gdjs.evtTools.window.getCanvasWidth(runtimeScene) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDballeObjects1[k] = gdjs.GameCode.GDballeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDballeObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDballeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].returnVariable(gdjs.GameCode.GDballeObjects1[i].getVariables().get("ValeurAngle")).setNumber((gdjs.GameCode.GDballeObjects1[i].getAverageForce().getAngle()));
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].addPolarForce(180-(gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDballeObjects1[i].getVariables().get("ValeurAngle")))+gdjs.random(5)-gdjs.random(5), 250, 1);
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].setX(789);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x925ad0(runtimeScene, context);} //End of subevents
}

}


{

gdjs.GameCode.GDballeObjects1.createFrom(runtimeScene.getObjects("balle"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDballeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDballeObjects1[i].getX() < 10 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDballeObjects1[k] = gdjs.GameCode.GDballeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDballeObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDballeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].returnVariable(gdjs.GameCode.GDballeObjects1[i].getVariables().get("ValeurAngle")).setNumber((gdjs.GameCode.GDballeObjects1[i].getAverageForce().getAngle()));
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].addPolarForce(180-(gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDballeObjects1[i].getVariables().get("ValeurAngle")))+gdjs.random(5)-gdjs.random(5), 250, 1);
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].setX(11);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x9351b8(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDballeObjects1.createFrom(runtimeScene.getObjects("balle"));
gdjs.GameCode.GDbarreObjects1.createFrom(runtimeScene.getObjects("barre"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDballeObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbarreObjects1Objects, false, runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDballeObjects1 */
/* Reuse gdjs.GameCode.GDbarreObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].returnVariable(gdjs.GameCode.GDballeObjects1[i].getVariables().get("ValeurAngle")).setNumber((gdjs.GameCode.GDballeObjects1[i].getAverageForce().getAngle()));
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].addPolarForce(-(gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDballeObjects1[i].getVariables().get("ValeurAngle")))+gdjs.random(5)-gdjs.random(5), 250, 1);
}
}{for(var i = 0, len = gdjs.GameCode.GDballeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDballeObjects1[i].setY((( gdjs.GameCode.GDbarreObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDbarreObjects1[0].getPointY(""))- (gdjs.GameCode.GDballeObjects1[i].getHeight()));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x936588(runtimeScene, context);} //End of subevents
}

}


{

gdjs.GameCode.GDballeObjects1.createFrom(runtimeScene.getObjects("balle"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDballeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDballeObjects1[i].getY() > gdjs.evtTools.window.getCanvasHeight(runtimeScene) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDballeObjects1[k] = gdjs.GameCode.GDballeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDballeObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GameOver", false);
}}

}


{

gdjs.GameCode.GDbrickObjects1.createFrom(runtimeScene.getObjects("brick"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbrickObjects1Objects) == 0;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Win", false);
}}

}


}; //End of gdjs.GameCode.eventsList0xaf630


gdjs.GameCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GameCode.GDbackgroundObjects1.length = 0;
gdjs.GameCode.GDbackgroundObjects2.length = 0;
gdjs.GameCode.GDbackgroundObjects3.length = 0;
gdjs.GameCode.GDbarreObjects1.length = 0;
gdjs.GameCode.GDbarreObjects2.length = 0;
gdjs.GameCode.GDbarreObjects3.length = 0;
gdjs.GameCode.GDbrickObjects1.length = 0;
gdjs.GameCode.GDbrickObjects2.length = 0;
gdjs.GameCode.GDbrickObjects3.length = 0;
gdjs.GameCode.GDballeObjects1.length = 0;
gdjs.GameCode.GDballeObjects2.length = 0;
gdjs.GameCode.GDballeObjects3.length = 0;
gdjs.GameCode.GDopObjects1.length = 0;
gdjs.GameCode.GDopObjects2.length = 0;
gdjs.GameCode.GDopObjects3.length = 0;
gdjs.GameCode.GDwallObjects1.length = 0;
gdjs.GameCode.GDwallObjects2.length = 0;
gdjs.GameCode.GDwallObjects3.length = 0;

gdjs.GameCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['GameCode']= gdjs.GameCode;
