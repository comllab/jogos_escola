gdjs.ResolvaCode = {};
gdjs.ResolvaCode.GDbackgroundObjects1= [];
gdjs.ResolvaCode.GDbackgroundObjects2= [];
gdjs.ResolvaCode.GDbackgroundObjects3= [];
gdjs.ResolvaCode.GDbackgroundObjects4= [];
gdjs.ResolvaCode.GDboxNumeroObjects1= [];
gdjs.ResolvaCode.GDboxNumeroObjects2= [];
gdjs.ResolvaCode.GDboxNumeroObjects3= [];
gdjs.ResolvaCode.GDboxNumeroObjects4= [];
gdjs.ResolvaCode.GDboxSucessorObjects1= [];
gdjs.ResolvaCode.GDboxSucessorObjects2= [];
gdjs.ResolvaCode.GDboxSucessorObjects3= [];
gdjs.ResolvaCode.GDboxSucessorObjects4= [];
gdjs.ResolvaCode.GDboxAntecessorObjects1= [];
gdjs.ResolvaCode.GDboxAntecessorObjects2= [];
gdjs.ResolvaCode.GDboxAntecessorObjects3= [];
gdjs.ResolvaCode.GDboxAntecessorObjects4= [];
gdjs.ResolvaCode.GDlblNumeroObjects1= [];
gdjs.ResolvaCode.GDlblNumeroObjects2= [];
gdjs.ResolvaCode.GDlblNumeroObjects3= [];
gdjs.ResolvaCode.GDlblNumeroObjects4= [];
gdjs.ResolvaCode.GDlblSucessorObjects1= [];
gdjs.ResolvaCode.GDlblSucessorObjects2= [];
gdjs.ResolvaCode.GDlblSucessorObjects3= [];
gdjs.ResolvaCode.GDlblSucessorObjects4= [];
gdjs.ResolvaCode.GDlblAntecessorObjects1= [];
gdjs.ResolvaCode.GDlblAntecessorObjects2= [];
gdjs.ResolvaCode.GDlblAntecessorObjects3= [];
gdjs.ResolvaCode.GDlblAntecessorObjects4= [];
gdjs.ResolvaCode.GDNewObjectObjects1= [];
gdjs.ResolvaCode.GDNewObjectObjects2= [];
gdjs.ResolvaCode.GDNewObjectObjects3= [];
gdjs.ResolvaCode.GDNewObjectObjects4= [];
gdjs.ResolvaCode.GDNewObject2Objects1= [];
gdjs.ResolvaCode.GDNewObject2Objects2= [];
gdjs.ResolvaCode.GDNewObject2Objects3= [];
gdjs.ResolvaCode.GDNewObject2Objects4= [];
gdjs.ResolvaCode.GDNewObject3Objects1= [];
gdjs.ResolvaCode.GDNewObject3Objects2= [];
gdjs.ResolvaCode.GDNewObject3Objects3= [];
gdjs.ResolvaCode.GDNewObject3Objects4= [];
gdjs.ResolvaCode.GDnumero1Objects1= [];
gdjs.ResolvaCode.GDnumero1Objects2= [];
gdjs.ResolvaCode.GDnumero1Objects3= [];
gdjs.ResolvaCode.GDnumero1Objects4= [];
gdjs.ResolvaCode.GDnumero2Objects1= [];
gdjs.ResolvaCode.GDnumero2Objects2= [];
gdjs.ResolvaCode.GDnumero2Objects3= [];
gdjs.ResolvaCode.GDnumero2Objects4= [];
gdjs.ResolvaCode.GDnumero3Objects1= [];
gdjs.ResolvaCode.GDnumero3Objects2= [];
gdjs.ResolvaCode.GDnumero3Objects3= [];
gdjs.ResolvaCode.GDnumero3Objects4= [];
gdjs.ResolvaCode.GDnumero4Objects1= [];
gdjs.ResolvaCode.GDnumero4Objects2= [];
gdjs.ResolvaCode.GDnumero4Objects3= [];
gdjs.ResolvaCode.GDnumero4Objects4= [];
gdjs.ResolvaCode.GDnumero5Objects1= [];
gdjs.ResolvaCode.GDnumero5Objects2= [];
gdjs.ResolvaCode.GDnumero5Objects3= [];
gdjs.ResolvaCode.GDnumero5Objects4= [];
gdjs.ResolvaCode.GDnumero6Objects1= [];
gdjs.ResolvaCode.GDnumero6Objects2= [];
gdjs.ResolvaCode.GDnumero6Objects3= [];
gdjs.ResolvaCode.GDnumero6Objects4= [];
gdjs.ResolvaCode.GDdialogObjects1= [];
gdjs.ResolvaCode.GDdialogObjects2= [];
gdjs.ResolvaCode.GDdialogObjects3= [];
gdjs.ResolvaCode.GDdialogObjects4= [];
gdjs.ResolvaCode.GDdialogMensagemObjects1= [];
gdjs.ResolvaCode.GDdialogMensagemObjects2= [];
gdjs.ResolvaCode.GDdialogMensagemObjects3= [];
gdjs.ResolvaCode.GDdialogMensagemObjects4= [];
gdjs.ResolvaCode.GDdialogAcertouObjects1= [];
gdjs.ResolvaCode.GDdialogAcertouObjects2= [];
gdjs.ResolvaCode.GDdialogAcertouObjects3= [];
gdjs.ResolvaCode.GDdialogAcertouObjects4= [];

gdjs.ResolvaCode.conditionTrue_0 = {val:false};
gdjs.ResolvaCode.condition0IsTrue_0 = {val:false};
gdjs.ResolvaCode.condition1IsTrue_0 = {val:false};
gdjs.ResolvaCode.condition2IsTrue_0 = {val:false};
gdjs.ResolvaCode.condition3IsTrue_0 = {val:false};
gdjs.ResolvaCode.conditionTrue_1 = {val:false};
gdjs.ResolvaCode.condition0IsTrue_1 = {val:false};
gdjs.ResolvaCode.condition1IsTrue_1 = {val:false};
gdjs.ResolvaCode.condition2IsTrue_1 = {val:false};
gdjs.ResolvaCode.condition3IsTrue_1 = {val:false};


gdjs.ResolvaCode.eventsList0x92e970 = function(runtimeScene, context) {

{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Boss Theme.ogg", 1, false, 40, 1);
}}

}


{


{
{runtimeScene.getVariables().get("num").setNumber(gdjs.random(6) + 1);
}}

}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("num")) == 1;
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDnumero1Objects2.createFrom(gdjs.ResolvaCode.GDnumero1Objects1);

gdjs.ResolvaCode.GDnumero6Objects2.createFrom(gdjs.ResolvaCode.GDnumero6Objects1);

{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1));
}
}}

}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("num")) == 2;
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDnumero1Objects2.createFrom(gdjs.ResolvaCode.GDnumero1Objects1);

gdjs.ResolvaCode.GDnumero2Objects2.createFrom(gdjs.ResolvaCode.GDnumero2Objects1);

{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero2Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero2Objects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1));
}
}}

}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("num")) == 3;
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDnumero3Objects2.createFrom(gdjs.ResolvaCode.GDnumero3Objects1);

gdjs.ResolvaCode.GDnumero4Objects2.createFrom(gdjs.ResolvaCode.GDnumero4Objects1);

{for(var i = 0, len = gdjs.ResolvaCode.GDnumero3Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero3Objects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero4Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero4Objects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1));
}
}}

}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("num")) == 4;
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDnumero4Objects2.createFrom(gdjs.ResolvaCode.GDnumero4Objects1);

gdjs.ResolvaCode.GDnumero5Objects2.createFrom(gdjs.ResolvaCode.GDnumero5Objects1);

{for(var i = 0, len = gdjs.ResolvaCode.GDnumero4Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero4Objects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero5Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero5Objects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1));
}
}}

}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("num")) == 5;
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDnumero5Objects2.createFrom(gdjs.ResolvaCode.GDnumero5Objects1);

gdjs.ResolvaCode.GDnumero6Objects2.createFrom(gdjs.ResolvaCode.GDnumero6Objects1);

{for(var i = 0, len = gdjs.ResolvaCode.GDnumero5Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero5Objects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1));
}
}}

}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("num")) >= 6;
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
/* Reuse gdjs.ResolvaCode.GDnumero1Objects1 */
/* Reuse gdjs.ResolvaCode.GDnumero6Objects1 */
{for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1));
}
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x92e970
gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDdialogAcertouObjects1Objects = Hashtable.newFrom({"dialogAcertou": gdjs.ResolvaCode.GDdialogAcertouObjects1});gdjs.ResolvaCode.eventsList0x936988 = function(runtimeScene, context) {

{

gdjs.ResolvaCode.GDlblAntecessorObjects1.createFrom(runtimeScene.getObjects("lblAntecessor"));
gdjs.ResolvaCode.GDlblSucessorObjects1.createFrom(runtimeScene.getObjects("lblSucessor"));

gdjs.ResolvaCode.condition0IsTrue_0.val = false;
gdjs.ResolvaCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDlblSucessorObjects1.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDlblSucessorObjects1[i].isVisible() ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDlblSucessorObjects1[k] = gdjs.ResolvaCode.GDlblSucessorObjects1[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDlblSucessorObjects1.length = k;}if ( gdjs.ResolvaCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDlblAntecessorObjects1.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDlblAntecessorObjects1[i].isVisible() ) {
        gdjs.ResolvaCode.condition1IsTrue_0.val = true;
        gdjs.ResolvaCode.GDlblAntecessorObjects1[k] = gdjs.ResolvaCode.GDlblAntecessorObjects1[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDlblAntecessorObjects1.length = k;}}
if (gdjs.ResolvaCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.popScene(runtimeScene);
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x936988
gdjs.ResolvaCode.eventsList0x936858 = function(runtimeScene, context) {

{

gdjs.ResolvaCode.GDdialogAcertouObjects1.createFrom(runtimeScene.getObjects("dialogAcertou"));

gdjs.ResolvaCode.condition0IsTrue_0.val = false;
gdjs.ResolvaCode.condition1IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDdialogAcertouObjects1Objects, runtimeScene, true, false);
}if ( gdjs.ResolvaCode.condition0IsTrue_0.val ) {
{
gdjs.ResolvaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.ResolvaCode.condition1IsTrue_0.val) {
{gdjs.evtTools.camera.hideLayer(runtimeScene, "Dialog");
}
{ //Subevents
gdjs.ResolvaCode.eventsList0x936988(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.ResolvaCode.eventsList0x936858
gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDboxAntecessorObjects1Objects = Hashtable.newFrom({"boxAntecessor": gdjs.ResolvaCode.GDboxAntecessorObjects1});gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDnumero1Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero2Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero3Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero4Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero5Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero6Objects1Objects = Hashtable.newFrom({"numero1": gdjs.ResolvaCode.GDnumero1Objects1, "numero2": gdjs.ResolvaCode.GDnumero2Objects1, "numero3": gdjs.ResolvaCode.GDnumero3Objects1, "numero4": gdjs.ResolvaCode.GDnumero4Objects1, "numero5": gdjs.ResolvaCode.GDnumero5Objects1, "numero6": gdjs.ResolvaCode.GDnumero6Objects1});gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDboxAntecessorObjects1Objects = Hashtable.newFrom({"boxAntecessor": gdjs.ResolvaCode.GDboxAntecessorObjects1});gdjs.ResolvaCode.eventsList0x92fa50 = function(runtimeScene, context) {

{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
{gdjs.ResolvaCode.conditionTrue_1 = gdjs.ResolvaCode.condition0IsTrue_0;
gdjs.ResolvaCode.conditionTrue_1.val = context.triggerOnce(9650396);
}
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise06.ogg", false, 100, 1);
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x92fa50
gdjs.ResolvaCode.eventsList0x8145c8 = function(runtimeScene, context) {

{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
{gdjs.ResolvaCode.conditionTrue_1 = gdjs.ResolvaCode.condition0IsTrue_0;
gdjs.ResolvaCode.conditionTrue_1.val = context.triggerOnce(7469284);
}
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x8145c8
gdjs.ResolvaCode.eventsList0x9320d0 = function(runtimeScene, context) {

{

gdjs.ResolvaCode.GDnumero1Objects3.createFrom(gdjs.ResolvaCode.GDnumero1Objects1);

gdjs.ResolvaCode.GDnumero2Objects3.createFrom(gdjs.ResolvaCode.GDnumero2Objects1);

gdjs.ResolvaCode.GDnumero3Objects3.createFrom(gdjs.ResolvaCode.GDnumero3Objects1);

gdjs.ResolvaCode.GDnumero4Objects3.createFrom(gdjs.ResolvaCode.GDnumero4Objects1);

gdjs.ResolvaCode.GDnumero5Objects3.createFrom(gdjs.ResolvaCode.GDnumero5Objects1);

gdjs.ResolvaCode.GDnumero6Objects3.createFrom(gdjs.ResolvaCode.GDnumero6Objects1);


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero1Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero1Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero1Objects3[k] = gdjs.ResolvaCode.GDnumero1Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero1Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero2Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero2Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero2Objects3[k] = gdjs.ResolvaCode.GDnumero2Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero2Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero3Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero3Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero3Objects3[k] = gdjs.ResolvaCode.GDnumero3Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero3Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero4Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero4Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero4Objects3[k] = gdjs.ResolvaCode.GDnumero4Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero4Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero5Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero5Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero5Objects3[k] = gdjs.ResolvaCode.GDnumero5Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero5Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero6Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero6Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero6Objects3[k] = gdjs.ResolvaCode.GDnumero6Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero6Objects3.length = k;}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDdialogAcertouObjects3.createFrom(runtimeScene.getObjects("dialogAcertou"));
gdjs.ResolvaCode.GDdialogMensagemObjects3.createFrom(runtimeScene.getObjects("dialogMensagem"));
gdjs.ResolvaCode.GDlblAntecessorObjects3.createFrom(gdjs.ResolvaCode.GDlblAntecessorObjects1);

/* Reuse gdjs.ResolvaCode.GDnumero1Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero2Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero3Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero4Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero5Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero6Objects3 */
{for(var i = 0, len = gdjs.ResolvaCode.GDdialogMensagemObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogMensagemObjects3[i].setString("Parabéns, você acertou!");
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDdialogAcertouObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogAcertouObjects3[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblAntecessorObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblAntecessorObjects3[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblAntecessorObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblAntecessorObjects3[i].hide(false);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects3[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero2Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero2Objects3[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero3Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero3Objects3[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero4Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero4Objects3[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero5Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero5Objects3[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects3[i].deleteFromScene(runtimeScene);
}
}
{ //Subevents
gdjs.ResolvaCode.eventsList0x92fa50(runtimeScene, context);} //End of subevents
}

}


{

gdjs.ResolvaCode.GDnumero1Objects3.createFrom(gdjs.ResolvaCode.GDnumero1Objects1);

gdjs.ResolvaCode.GDnumero2Objects3.createFrom(gdjs.ResolvaCode.GDnumero2Objects1);

gdjs.ResolvaCode.GDnumero3Objects3.createFrom(gdjs.ResolvaCode.GDnumero3Objects1);

gdjs.ResolvaCode.GDnumero4Objects3.createFrom(gdjs.ResolvaCode.GDnumero4Objects1);

gdjs.ResolvaCode.GDnumero5Objects3.createFrom(gdjs.ResolvaCode.GDnumero5Objects1);

gdjs.ResolvaCode.GDnumero6Objects3.createFrom(gdjs.ResolvaCode.GDnumero6Objects1);


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero1Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero1Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero1Objects3[k] = gdjs.ResolvaCode.GDnumero1Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero1Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero2Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero2Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero2Objects3[k] = gdjs.ResolvaCode.GDnumero2Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero2Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero3Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero3Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero3Objects3[k] = gdjs.ResolvaCode.GDnumero3Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero3Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero4Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero4Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero4Objects3[k] = gdjs.ResolvaCode.GDnumero4Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero4Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero5Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero5Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero5Objects3[k] = gdjs.ResolvaCode.GDnumero5Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero5Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero6Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero6Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero6Objects3[k] = gdjs.ResolvaCode.GDnumero6Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero6Objects3.length = k;}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDdialogAcertouObjects3.createFrom(runtimeScene.getObjects("dialogAcertou"));
gdjs.ResolvaCode.GDdialogMensagemObjects3.createFrom(runtimeScene.getObjects("dialogMensagem"));
/* Reuse gdjs.ResolvaCode.GDnumero1Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero2Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero3Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero4Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero5Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero6Objects3 */
{for(var i = 0, len = gdjs.ResolvaCode.GDdialogMensagemObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogMensagemObjects3[i].setString("Que pena, voce errou!");
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDdialogAcertouObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogAcertouObjects3[i].setAnimation(1);
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero2Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero2Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero3Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero3Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero4Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero4Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero5Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero5Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
}
{ //Subevents
gdjs.ResolvaCode.eventsList0x8145c8(runtimeScene, context);} //End of subevents
}

}


{


{
{gdjs.evtTools.camera.showLayer(runtimeScene, "Dialog");
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x9320d0
gdjs.ResolvaCode.eventsList0x930a88 = function(runtimeScene, context) {

{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
{gdjs.ResolvaCode.conditionTrue_1 = gdjs.ResolvaCode.condition0IsTrue_0;
gdjs.ResolvaCode.conditionTrue_1.val = context.triggerOnce(9662740);
}
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise06.ogg", false, 100, 1);
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x930a88
gdjs.ResolvaCode.eventsList0x932a88 = function(runtimeScene, context) {

{

gdjs.ResolvaCode.GDnumero1Objects2.createFrom(gdjs.ResolvaCode.GDnumero1Objects1);

gdjs.ResolvaCode.GDnumero2Objects2.createFrom(gdjs.ResolvaCode.GDnumero2Objects1);

gdjs.ResolvaCode.GDnumero3Objects2.createFrom(gdjs.ResolvaCode.GDnumero3Objects1);

gdjs.ResolvaCode.GDnumero4Objects2.createFrom(gdjs.ResolvaCode.GDnumero4Objects1);

gdjs.ResolvaCode.GDnumero5Objects2.createFrom(gdjs.ResolvaCode.GDnumero5Objects1);

gdjs.ResolvaCode.GDnumero6Objects2.createFrom(gdjs.ResolvaCode.GDnumero6Objects1);


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero1Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero1Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero1Objects2[k] = gdjs.ResolvaCode.GDnumero1Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero1Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero2Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero2Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero2Objects2[k] = gdjs.ResolvaCode.GDnumero2Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero2Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero3Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero3Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero3Objects2[k] = gdjs.ResolvaCode.GDnumero3Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero3Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero4Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero4Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero4Objects2[k] = gdjs.ResolvaCode.GDnumero4Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero4Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero5Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero5Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero5Objects2[k] = gdjs.ResolvaCode.GDnumero5Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero5Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero6Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero6Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero6Objects2[k] = gdjs.ResolvaCode.GDnumero6Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero6Objects2.length = k;}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDdialogAcertouObjects2.createFrom(runtimeScene.getObjects("dialogAcertou"));
gdjs.ResolvaCode.GDdialogMensagemObjects2.createFrom(runtimeScene.getObjects("dialogMensagem"));
gdjs.ResolvaCode.GDlblAntecessorObjects2.createFrom(gdjs.ResolvaCode.GDlblAntecessorObjects1);

/* Reuse gdjs.ResolvaCode.GDnumero1Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero2Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero3Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero4Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero5Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero6Objects2 */
{for(var i = 0, len = gdjs.ResolvaCode.GDdialogMensagemObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogMensagemObjects2[i].setString("Parabéns, você acertou!");
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDdialogAcertouObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogAcertouObjects2[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblAntecessorObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblAntecessorObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblAntecessorObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblAntecessorObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero2Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero2Objects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero3Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero3Objects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero4Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero4Objects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero5Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero5Objects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects2[i].deleteFromScene(runtimeScene);
}
}
{ //Subevents
gdjs.ResolvaCode.eventsList0x930a88(runtimeScene, context);} //End of subevents
}

}


{

gdjs.ResolvaCode.GDnumero1Objects2.createFrom(gdjs.ResolvaCode.GDnumero1Objects1);

gdjs.ResolvaCode.GDnumero2Objects2.createFrom(gdjs.ResolvaCode.GDnumero2Objects1);

gdjs.ResolvaCode.GDnumero3Objects2.createFrom(gdjs.ResolvaCode.GDnumero3Objects1);

gdjs.ResolvaCode.GDnumero4Objects2.createFrom(gdjs.ResolvaCode.GDnumero4Objects1);

gdjs.ResolvaCode.GDnumero5Objects2.createFrom(gdjs.ResolvaCode.GDnumero5Objects1);

gdjs.ResolvaCode.GDnumero6Objects2.createFrom(gdjs.ResolvaCode.GDnumero6Objects1);


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero1Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero1Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero1Objects2[k] = gdjs.ResolvaCode.GDnumero1Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero1Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero2Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero2Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero2Objects2[k] = gdjs.ResolvaCode.GDnumero2Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero2Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero3Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero3Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero3Objects2[k] = gdjs.ResolvaCode.GDnumero3Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero3Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero4Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero4Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero4Objects2[k] = gdjs.ResolvaCode.GDnumero4Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero4Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero5Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero5Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero5Objects2[k] = gdjs.ResolvaCode.GDnumero5Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero5Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero6Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero6Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero6Objects2[k] = gdjs.ResolvaCode.GDnumero6Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero6Objects2.length = k;}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDdialogAcertouObjects2.createFrom(runtimeScene.getObjects("dialogAcertou"));
gdjs.ResolvaCode.GDdialogMensagemObjects2.createFrom(runtimeScene.getObjects("dialogMensagem"));
/* Reuse gdjs.ResolvaCode.GDnumero1Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero2Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero3Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero4Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero5Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero6Objects2 */
{for(var i = 0, len = gdjs.ResolvaCode.GDdialogMensagemObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogMensagemObjects2[i].setString("Que pena, voce errou!");
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDdialogAcertouObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogAcertouObjects2[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero2Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero2Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero3Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero3Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero4Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero4Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero5Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero5Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}

}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
{gdjs.ResolvaCode.conditionTrue_1 = gdjs.ResolvaCode.condition0IsTrue_0;
gdjs.ResolvaCode.conditionTrue_1.val = context.triggerOnce(6881876);
}
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}}

}


{


{
{gdjs.evtTools.camera.showLayer(runtimeScene, "Dialog");
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x932a88
gdjs.ResolvaCode.eventsList0x934ed0 = function(runtimeScene, context) {

{



}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
gdjs.ResolvaCode.condition1IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = !(gdjs.evtTools.systemInfo.isMobile());
}if ( gdjs.ResolvaCode.condition0IsTrue_0.val ) {
{
gdjs.ResolvaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.ResolvaCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.ResolvaCode.eventsList0x9320d0(runtimeScene, context);} //End of subevents
}

}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
gdjs.ResolvaCode.condition1IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if ( gdjs.ResolvaCode.condition0IsTrue_0.val ) {
{
gdjs.ResolvaCode.condition1IsTrue_0.val = gdjs.evtTools.input.popEndedTouch(runtimeScene);
}}
if (gdjs.ResolvaCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.ResolvaCode.eventsList0x932a88(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.ResolvaCode.eventsList0x934ed0
gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDboxSucessorObjects1Objects = Hashtable.newFrom({"boxSucessor": gdjs.ResolvaCode.GDboxSucessorObjects1});gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDnumero1Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero2Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero3Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero4Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero5Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero6Objects1Objects = Hashtable.newFrom({"numero1": gdjs.ResolvaCode.GDnumero1Objects1, "numero2": gdjs.ResolvaCode.GDnumero2Objects1, "numero3": gdjs.ResolvaCode.GDnumero3Objects1, "numero4": gdjs.ResolvaCode.GDnumero4Objects1, "numero5": gdjs.ResolvaCode.GDnumero5Objects1, "numero6": gdjs.ResolvaCode.GDnumero6Objects1});gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDboxSucessorObjects1Objects = Hashtable.newFrom({"boxSucessor": gdjs.ResolvaCode.GDboxSucessorObjects1});gdjs.ResolvaCode.eventsList0x934358 = function(runtimeScene, context) {

{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
{gdjs.ResolvaCode.conditionTrue_1 = gdjs.ResolvaCode.condition0IsTrue_0;
gdjs.ResolvaCode.conditionTrue_1.val = context.triggerOnce(6873092);
}
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise06.ogg", false, 100, 1);
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x934358
gdjs.ResolvaCode.eventsList0x6914e0 = function(runtimeScene, context) {

{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
{gdjs.ResolvaCode.conditionTrue_1 = gdjs.ResolvaCode.condition0IsTrue_0;
gdjs.ResolvaCode.conditionTrue_1.val = context.triggerOnce(8518428);
}
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x6914e0
gdjs.ResolvaCode.eventsList0x9341c8 = function(runtimeScene, context) {

{

gdjs.ResolvaCode.GDnumero1Objects3.createFrom(gdjs.ResolvaCode.GDnumero1Objects1);

gdjs.ResolvaCode.GDnumero2Objects3.createFrom(gdjs.ResolvaCode.GDnumero2Objects1);

gdjs.ResolvaCode.GDnumero3Objects3.createFrom(gdjs.ResolvaCode.GDnumero3Objects1);

gdjs.ResolvaCode.GDnumero4Objects3.createFrom(gdjs.ResolvaCode.GDnumero4Objects1);

gdjs.ResolvaCode.GDnumero5Objects3.createFrom(gdjs.ResolvaCode.GDnumero5Objects1);

gdjs.ResolvaCode.GDnumero6Objects3.createFrom(gdjs.ResolvaCode.GDnumero6Objects1);


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero1Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero1Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero1Objects3[k] = gdjs.ResolvaCode.GDnumero1Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero1Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero2Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero2Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero2Objects3[k] = gdjs.ResolvaCode.GDnumero2Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero2Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero3Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero3Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero3Objects3[k] = gdjs.ResolvaCode.GDnumero3Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero3Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero4Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero4Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero4Objects3[k] = gdjs.ResolvaCode.GDnumero4Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero4Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero5Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero5Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero5Objects3[k] = gdjs.ResolvaCode.GDnumero5Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero5Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero6Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero6Objects3[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero6Objects3[k] = gdjs.ResolvaCode.GDnumero6Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero6Objects3.length = k;}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDdialogAcertouObjects3.createFrom(runtimeScene.getObjects("dialogAcertou"));
gdjs.ResolvaCode.GDdialogMensagemObjects3.createFrom(runtimeScene.getObjects("dialogMensagem"));
gdjs.ResolvaCode.GDlblSucessorObjects3.createFrom(gdjs.ResolvaCode.GDlblSucessorObjects1);

/* Reuse gdjs.ResolvaCode.GDnumero1Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero2Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero3Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero4Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero5Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero6Objects3 */
{for(var i = 0, len = gdjs.ResolvaCode.GDdialogMensagemObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogMensagemObjects3[i].setString("Parabéns, você acertou!");
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDdialogAcertouObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogAcertouObjects3[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblSucessorObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblSucessorObjects3[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblSucessorObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblSucessorObjects3[i].hide(false);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects3[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero2Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero2Objects3[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero3Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero3Objects3[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero4Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero4Objects3[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero5Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero5Objects3[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects3[i].deleteFromScene(runtimeScene);
}
}
{ //Subevents
gdjs.ResolvaCode.eventsList0x934358(runtimeScene, context);} //End of subevents
}

}


{

gdjs.ResolvaCode.GDnumero1Objects3.createFrom(gdjs.ResolvaCode.GDnumero1Objects1);

gdjs.ResolvaCode.GDnumero2Objects3.createFrom(gdjs.ResolvaCode.GDnumero2Objects1);

gdjs.ResolvaCode.GDnumero3Objects3.createFrom(gdjs.ResolvaCode.GDnumero3Objects1);

gdjs.ResolvaCode.GDnumero4Objects3.createFrom(gdjs.ResolvaCode.GDnumero4Objects1);

gdjs.ResolvaCode.GDnumero5Objects3.createFrom(gdjs.ResolvaCode.GDnumero5Objects1);

gdjs.ResolvaCode.GDnumero6Objects3.createFrom(gdjs.ResolvaCode.GDnumero6Objects1);


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero1Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero1Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero1Objects3[k] = gdjs.ResolvaCode.GDnumero1Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero1Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero2Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero2Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero2Objects3[k] = gdjs.ResolvaCode.GDnumero2Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero2Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero3Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero3Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero3Objects3[k] = gdjs.ResolvaCode.GDnumero3Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero3Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero4Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero4Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero4Objects3[k] = gdjs.ResolvaCode.GDnumero4Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero4Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero5Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero5Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero5Objects3[k] = gdjs.ResolvaCode.GDnumero5Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero5Objects3.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero6Objects3.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero6Objects3[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) + 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero6Objects3[k] = gdjs.ResolvaCode.GDnumero6Objects3[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero6Objects3.length = k;}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDdialogAcertouObjects3.createFrom(runtimeScene.getObjects("dialogAcertou"));
gdjs.ResolvaCode.GDdialogMensagemObjects3.createFrom(runtimeScene.getObjects("dialogMensagem"));
/* Reuse gdjs.ResolvaCode.GDnumero1Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero2Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero3Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero4Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero5Objects3 */
/* Reuse gdjs.ResolvaCode.GDnumero6Objects3 */
{for(var i = 0, len = gdjs.ResolvaCode.GDdialogMensagemObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogMensagemObjects3[i].setString("Que pena, voce errou!");
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDdialogAcertouObjects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogAcertouObjects3[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero2Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero2Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero3Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero3Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero4Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero4Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero5Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero5Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects3.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects3[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}
{ //Subevents
gdjs.ResolvaCode.eventsList0x6914e0(runtimeScene, context);} //End of subevents
}

}


{


{
{gdjs.evtTools.camera.showLayer(runtimeScene, "Dialog");
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x9341c8
gdjs.ResolvaCode.eventsList0x92ed58 = function(runtimeScene, context) {

{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
{gdjs.ResolvaCode.conditionTrue_1 = gdjs.ResolvaCode.condition0IsTrue_0;
gdjs.ResolvaCode.conditionTrue_1.val = context.triggerOnce(9671508);
}
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise06.ogg", false, 100, 1);
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x92ed58
gdjs.ResolvaCode.eventsList0x939518 = function(runtimeScene, context) {

{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
{gdjs.ResolvaCode.conditionTrue_1 = gdjs.ResolvaCode.condition0IsTrue_0;
gdjs.ResolvaCode.conditionTrue_1.val = context.triggerOnce(6616804);
}
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x939518
gdjs.ResolvaCode.eventsList0x92eba8 = function(runtimeScene, context) {

{

gdjs.ResolvaCode.GDnumero1Objects2.createFrom(gdjs.ResolvaCode.GDnumero1Objects1);

gdjs.ResolvaCode.GDnumero2Objects2.createFrom(gdjs.ResolvaCode.GDnumero2Objects1);

gdjs.ResolvaCode.GDnumero3Objects2.createFrom(gdjs.ResolvaCode.GDnumero3Objects1);

gdjs.ResolvaCode.GDnumero4Objects2.createFrom(gdjs.ResolvaCode.GDnumero4Objects1);

gdjs.ResolvaCode.GDnumero5Objects2.createFrom(gdjs.ResolvaCode.GDnumero5Objects1);

gdjs.ResolvaCode.GDnumero6Objects2.createFrom(gdjs.ResolvaCode.GDnumero6Objects1);


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero1Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero1Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero1Objects2[k] = gdjs.ResolvaCode.GDnumero1Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero1Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero2Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero2Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero2Objects2[k] = gdjs.ResolvaCode.GDnumero2Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero2Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero3Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero3Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero3Objects2[k] = gdjs.ResolvaCode.GDnumero3Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero3Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero4Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero4Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero4Objects2[k] = gdjs.ResolvaCode.GDnumero4Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero4Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero5Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero5Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero5Objects2[k] = gdjs.ResolvaCode.GDnumero5Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero5Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero6Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero6Objects2[i].getString() == gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero6Objects2[k] = gdjs.ResolvaCode.GDnumero6Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero6Objects2.length = k;}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDdialogAcertouObjects2.createFrom(runtimeScene.getObjects("dialogAcertou"));
gdjs.ResolvaCode.GDdialogMensagemObjects2.createFrom(runtimeScene.getObjects("dialogMensagem"));
gdjs.ResolvaCode.GDlblAntecessorObjects2.createFrom(runtimeScene.getObjects("lblAntecessor"));
/* Reuse gdjs.ResolvaCode.GDnumero1Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero2Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero3Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero4Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero5Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero6Objects2 */
{for(var i = 0, len = gdjs.ResolvaCode.GDdialogMensagemObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogMensagemObjects2[i].setString("Parabéns, você acertou!");
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDdialogAcertouObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogAcertouObjects2[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblAntecessorObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblAntecessorObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblAntecessorObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblAntecessorObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero2Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero2Objects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero3Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero3Objects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero4Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero4Objects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero5Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero5Objects2[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects2[i].deleteFromScene(runtimeScene);
}
}
{ //Subevents
gdjs.ResolvaCode.eventsList0x92ed58(runtimeScene, context);} //End of subevents
}

}


{

gdjs.ResolvaCode.GDnumero1Objects2.createFrom(gdjs.ResolvaCode.GDnumero1Objects1);

gdjs.ResolvaCode.GDnumero2Objects2.createFrom(gdjs.ResolvaCode.GDnumero2Objects1);

gdjs.ResolvaCode.GDnumero3Objects2.createFrom(gdjs.ResolvaCode.GDnumero3Objects1);

gdjs.ResolvaCode.GDnumero4Objects2.createFrom(gdjs.ResolvaCode.GDnumero4Objects1);

gdjs.ResolvaCode.GDnumero5Objects2.createFrom(gdjs.ResolvaCode.GDnumero5Objects1);

gdjs.ResolvaCode.GDnumero6Objects2.createFrom(gdjs.ResolvaCode.GDnumero6Objects1);


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero1Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero1Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero1Objects2[k] = gdjs.ResolvaCode.GDnumero1Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero1Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero2Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero2Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero2Objects2[k] = gdjs.ResolvaCode.GDnumero2Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero2Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero3Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero3Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero3Objects2[k] = gdjs.ResolvaCode.GDnumero3Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero3Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero4Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero4Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero4Objects2[k] = gdjs.ResolvaCode.GDnumero4Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero4Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero5Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero5Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero5Objects2[k] = gdjs.ResolvaCode.GDnumero5Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero5Objects2.length = k;for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDnumero6Objects2.length;i<l;++i) {
    if ( gdjs.ResolvaCode.GDnumero6Objects2[i].getString() != gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) - 1) ) {
        gdjs.ResolvaCode.condition0IsTrue_0.val = true;
        gdjs.ResolvaCode.GDnumero6Objects2[k] = gdjs.ResolvaCode.GDnumero6Objects2[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDnumero6Objects2.length = k;}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDdialogAcertouObjects2.createFrom(runtimeScene.getObjects("dialogAcertou"));
gdjs.ResolvaCode.GDdialogMensagemObjects2.createFrom(runtimeScene.getObjects("dialogMensagem"));
/* Reuse gdjs.ResolvaCode.GDnumero1Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero2Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero3Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero4Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero5Objects2 */
/* Reuse gdjs.ResolvaCode.GDnumero6Objects2 */
{for(var i = 0, len = gdjs.ResolvaCode.GDdialogMensagemObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogMensagemObjects2[i].setString("Que pena, voce errou!");
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDdialogAcertouObjects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDdialogAcertouObjects2[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero2Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero2Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero3Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero3Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero4Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero4Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero5Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero5Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects2.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects2[i].setPosition(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabX")),gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("grabY")));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}
{ //Subevents
gdjs.ResolvaCode.eventsList0x939518(runtimeScene, context);} //End of subevents
}

}


{


{
{gdjs.evtTools.camera.showLayer(runtimeScene, "Dialog");
}}

}


}; //End of gdjs.ResolvaCode.eventsList0x92eba8
gdjs.ResolvaCode.eventsList0x936bf8 = function(runtimeScene, context) {

{



}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
gdjs.ResolvaCode.condition1IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = !(gdjs.evtTools.systemInfo.isMobile());
}if ( gdjs.ResolvaCode.condition0IsTrue_0.val ) {
{
gdjs.ResolvaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.ResolvaCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.ResolvaCode.eventsList0x9341c8(runtimeScene, context);} //End of subevents
}

}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
gdjs.ResolvaCode.condition1IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if ( gdjs.ResolvaCode.condition0IsTrue_0.val ) {
{
gdjs.ResolvaCode.condition1IsTrue_0.val = gdjs.evtTools.input.popEndedTouch(runtimeScene);
}}
if (gdjs.ResolvaCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.ResolvaCode.eventsList0x92eba8(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.ResolvaCode.eventsList0x936bf8
gdjs.ResolvaCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDlblAntecessorObjects1.createFrom(runtimeScene.getObjects("lblAntecessor"));
gdjs.ResolvaCode.GDlblNumeroObjects1.createFrom(runtimeScene.getObjects("lblNumero"));
gdjs.ResolvaCode.GDlblSucessorObjects1.createFrom(runtimeScene.getObjects("lblSucessor"));
gdjs.ResolvaCode.GDnumero1Objects1.createFrom(runtimeScene.getObjects("numero1"));
gdjs.ResolvaCode.GDnumero2Objects1.createFrom(runtimeScene.getObjects("numero2"));
gdjs.ResolvaCode.GDnumero3Objects1.createFrom(runtimeScene.getObjects("numero3"));
gdjs.ResolvaCode.GDnumero4Objects1.createFrom(runtimeScene.getObjects("numero4"));
gdjs.ResolvaCode.GDnumero5Objects1.createFrom(runtimeScene.getObjects("numero5"));
gdjs.ResolvaCode.GDnumero6Objects1.createFrom(runtimeScene.getObjects("numero6"));
{for(var i = 0, len = gdjs.ResolvaCode.GDlblSucessorObjects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblSucessorObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblAntecessorObjects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblAntecessorObjects1[i].hide();
}
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "Dialog");
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblNumeroObjects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblNumeroObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero1Objects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero1Objects1[i].setString(gdjs.evtTools.common.toString(gdjs.randomInRange(10,  99)));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero2Objects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero2Objects1[i].setString(gdjs.evtTools.common.toString(gdjs.randomInRange(10,  99)));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero3Objects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero3Objects1[i].setString(gdjs.evtTools.common.toString(gdjs.randomInRange(10,  99)));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero4Objects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero4Objects1[i].setString(gdjs.evtTools.common.toString(gdjs.randomInRange(10,  99)));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero5Objects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero5Objects1[i].setString(gdjs.evtTools.common.toString(gdjs.randomInRange(10,  99)));
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDnumero6Objects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDnumero6Objects1[i].setString(gdjs.evtTools.common.toString(gdjs.randomInRange(10,  99)));
}
}
{ //Subevents
gdjs.ResolvaCode.eventsList0x92e970(runtimeScene, context);} //End of subevents
}

}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {
gdjs.ResolvaCode.GDboxAntecessorObjects1.createFrom(runtimeScene.getObjects("boxAntecessor"));
gdjs.ResolvaCode.GDboxNumeroObjects1.createFrom(runtimeScene.getObjects("boxNumero"));
gdjs.ResolvaCode.GDboxSucessorObjects1.createFrom(runtimeScene.getObjects("boxSucessor"));
gdjs.ResolvaCode.GDlblAntecessorObjects1.createFrom(runtimeScene.getObjects("lblAntecessor"));
gdjs.ResolvaCode.GDlblNumeroObjects1.createFrom(runtimeScene.getObjects("lblNumero"));
gdjs.ResolvaCode.GDlblSucessorObjects1.createFrom(runtimeScene.getObjects("lblSucessor"));
{for(var i = 0, len = gdjs.ResolvaCode.GDlblAntecessorObjects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblAntecessorObjects1[i].setX((( gdjs.ResolvaCode.GDboxAntecessorObjects1.length === 0 ) ? 0 :gdjs.ResolvaCode.GDboxAntecessorObjects1[0].getPointX("")) + (( gdjs.ResolvaCode.GDboxAntecessorObjects1.length === 0 ) ? 0 :gdjs.ResolvaCode.GDboxAntecessorObjects1[0].getWidth())/2 - (gdjs.ResolvaCode.GDlblAntecessorObjects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblNumeroObjects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblNumeroObjects1[i].setX((( gdjs.ResolvaCode.GDboxNumeroObjects1.length === 0 ) ? 0 :gdjs.ResolvaCode.GDboxNumeroObjects1[0].getPointX("")) + (( gdjs.ResolvaCode.GDboxNumeroObjects1.length === 0 ) ? 0 :gdjs.ResolvaCode.GDboxNumeroObjects1[0].getWidth())/2 - (gdjs.ResolvaCode.GDlblNumeroObjects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.ResolvaCode.GDlblSucessorObjects1.length ;i < len;++i) {
    gdjs.ResolvaCode.GDlblSucessorObjects1[i].setX((( gdjs.ResolvaCode.GDboxSucessorObjects1.length === 0 ) ? 0 :gdjs.ResolvaCode.GDboxSucessorObjects1[0].getPointX("")) + (( gdjs.ResolvaCode.GDboxSucessorObjects1.length === 0 ) ? 0 :gdjs.ResolvaCode.GDboxSucessorObjects1[0].getWidth())/2 - (gdjs.ResolvaCode.GDlblSucessorObjects1[i].getWidth())/2);
}
}}

}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
gdjs.ResolvaCode.condition1IsTrue_0.val = false;
gdjs.ResolvaCode.condition2IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = !(gdjs.evtTools.systemInfo.isMobile());
}if ( gdjs.ResolvaCode.condition0IsTrue_0.val ) {
{
gdjs.ResolvaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.ResolvaCode.condition1IsTrue_0.val ) {
{
{gdjs.ResolvaCode.conditionTrue_1 = gdjs.ResolvaCode.condition2IsTrue_0;
gdjs.ResolvaCode.conditionTrue_1.val = context.triggerOnce(7283004);
}
}}
}
if (gdjs.ResolvaCode.condition2IsTrue_0.val) {
gdjs.ResolvaCode.GDnumero1Objects1.createFrom(runtimeScene.getObjects("numero1"));
gdjs.ResolvaCode.GDnumero2Objects1.createFrom(runtimeScene.getObjects("numero2"));
gdjs.ResolvaCode.GDnumero3Objects1.createFrom(runtimeScene.getObjects("numero3"));
gdjs.ResolvaCode.GDnumero4Objects1.createFrom(runtimeScene.getObjects("numero4"));
gdjs.ResolvaCode.GDnumero5Objects1.createFrom(runtimeScene.getObjects("numero5"));
gdjs.ResolvaCode.GDnumero6Objects1.createFrom(runtimeScene.getObjects("numero6"));
{runtimeScene.getVariables().get("grabX").setNumber(gdjs.evtTools.input.getMouseX(runtimeScene, "", 0) - (( gdjs.ResolvaCode.GDnumero6Objects1.length === 0 ) ? (( gdjs.ResolvaCode.GDnumero5Objects1.length === 0 ) ? (( gdjs.ResolvaCode.GDnumero4Objects1.length === 0 ) ? (( gdjs.ResolvaCode.GDnumero3Objects1.length === 0 ) ? (( gdjs.ResolvaCode.GDnumero2Objects1.length === 0 ) ? (( gdjs.ResolvaCode.GDnumero1Objects1.length === 0 ) ? 0 :gdjs.ResolvaCode.GDnumero1Objects1[0].getWidth()) :gdjs.ResolvaCode.GDnumero2Objects1[0].getWidth()) :gdjs.ResolvaCode.GDnumero3Objects1[0].getWidth()) :gdjs.ResolvaCode.GDnumero4Objects1[0].getWidth()) :gdjs.ResolvaCode.GDnumero5Objects1[0].getWidth()) :gdjs.ResolvaCode.GDnumero6Objects1[0].getWidth())/2);
}{runtimeScene.getVariables().get("grabY").setNumber(gdjs.evtTools.input.getMouseY(runtimeScene, "", 0) - (( gdjs.ResolvaCode.GDnumero6Objects1.length === 0 ) ? (( gdjs.ResolvaCode.GDnumero5Objects1.length === 0 ) ? (( gdjs.ResolvaCode.GDnumero4Objects1.length === 0 ) ? (( gdjs.ResolvaCode.GDnumero3Objects1.length === 0 ) ? (( gdjs.ResolvaCode.GDnumero2Objects1.length === 0 ) ? (( gdjs.ResolvaCode.GDnumero1Objects1.length === 0 ) ? 0 :gdjs.ResolvaCode.GDnumero1Objects1[0].getHeight()) :gdjs.ResolvaCode.GDnumero2Objects1[0].getHeight()) :gdjs.ResolvaCode.GDnumero3Objects1[0].getHeight()) :gdjs.ResolvaCode.GDnumero4Objects1[0].getHeight()) :gdjs.ResolvaCode.GDnumero5Objects1[0].getHeight()) :gdjs.ResolvaCode.GDnumero6Objects1[0].getHeight())/2);
}}

}


{



}


{


gdjs.ResolvaCode.condition0IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Dialog");
}if (gdjs.ResolvaCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.ResolvaCode.eventsList0x936858(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.ResolvaCode.GDboxAntecessorObjects1.createFrom(runtimeScene.getObjects("boxAntecessor"));
gdjs.ResolvaCode.GDlblAntecessorObjects1.createFrom(runtimeScene.getObjects("lblAntecessor"));
gdjs.ResolvaCode.GDnumero1Objects1.createFrom(runtimeScene.getObjects("numero1"));
gdjs.ResolvaCode.GDnumero2Objects1.createFrom(runtimeScene.getObjects("numero2"));
gdjs.ResolvaCode.GDnumero3Objects1.createFrom(runtimeScene.getObjects("numero3"));
gdjs.ResolvaCode.GDnumero4Objects1.createFrom(runtimeScene.getObjects("numero4"));
gdjs.ResolvaCode.GDnumero5Objects1.createFrom(runtimeScene.getObjects("numero5"));
gdjs.ResolvaCode.GDnumero6Objects1.createFrom(runtimeScene.getObjects("numero6"));

gdjs.ResolvaCode.condition0IsTrue_0.val = false;
gdjs.ResolvaCode.condition1IsTrue_0.val = false;
gdjs.ResolvaCode.condition2IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDboxAntecessorObjects1Objects, runtimeScene, true, false);
}if ( gdjs.ResolvaCode.condition0IsTrue_0.val ) {
{
gdjs.ResolvaCode.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDnumero1Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero2Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero3Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero4Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero5Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero6Objects1Objects, gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDboxAntecessorObjects1Objects, false, runtimeScene);
}if ( gdjs.ResolvaCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDlblAntecessorObjects1.length;i<l;++i) {
    if ( !(gdjs.ResolvaCode.GDlblAntecessorObjects1[i].isVisible()) ) {
        gdjs.ResolvaCode.condition2IsTrue_0.val = true;
        gdjs.ResolvaCode.GDlblAntecessorObjects1[k] = gdjs.ResolvaCode.GDlblAntecessorObjects1[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDlblAntecessorObjects1.length = k;}}
}
if (gdjs.ResolvaCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.ResolvaCode.eventsList0x934ed0(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.ResolvaCode.GDboxSucessorObjects1.createFrom(runtimeScene.getObjects("boxSucessor"));
gdjs.ResolvaCode.GDlblSucessorObjects1.createFrom(runtimeScene.getObjects("lblSucessor"));
gdjs.ResolvaCode.GDnumero1Objects1.createFrom(runtimeScene.getObjects("numero1"));
gdjs.ResolvaCode.GDnumero2Objects1.createFrom(runtimeScene.getObjects("numero2"));
gdjs.ResolvaCode.GDnumero3Objects1.createFrom(runtimeScene.getObjects("numero3"));
gdjs.ResolvaCode.GDnumero4Objects1.createFrom(runtimeScene.getObjects("numero4"));
gdjs.ResolvaCode.GDnumero5Objects1.createFrom(runtimeScene.getObjects("numero5"));
gdjs.ResolvaCode.GDnumero6Objects1.createFrom(runtimeScene.getObjects("numero6"));

gdjs.ResolvaCode.condition0IsTrue_0.val = false;
gdjs.ResolvaCode.condition1IsTrue_0.val = false;
gdjs.ResolvaCode.condition2IsTrue_0.val = false;
{
gdjs.ResolvaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDboxSucessorObjects1Objects, runtimeScene, true, false);
}if ( gdjs.ResolvaCode.condition0IsTrue_0.val ) {
{
gdjs.ResolvaCode.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDnumero1Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero2Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero3Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero4Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero5Objects1ObjectsGDgdjs_46ResolvaCode_46GDnumero6Objects1Objects, gdjs.ResolvaCode.mapOfGDgdjs_46ResolvaCode_46GDboxSucessorObjects1Objects, false, runtimeScene);
}if ( gdjs.ResolvaCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.ResolvaCode.GDlblSucessorObjects1.length;i<l;++i) {
    if ( !(gdjs.ResolvaCode.GDlblSucessorObjects1[i].isVisible()) ) {
        gdjs.ResolvaCode.condition2IsTrue_0.val = true;
        gdjs.ResolvaCode.GDlblSucessorObjects1[k] = gdjs.ResolvaCode.GDlblSucessorObjects1[i];
        ++k;
    }
}
gdjs.ResolvaCode.GDlblSucessorObjects1.length = k;}}
}
if (gdjs.ResolvaCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.ResolvaCode.eventsList0x936bf8(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.ResolvaCode.eventsList0xaf630


gdjs.ResolvaCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.ResolvaCode.GDbackgroundObjects1.length = 0;
gdjs.ResolvaCode.GDbackgroundObjects2.length = 0;
gdjs.ResolvaCode.GDbackgroundObjects3.length = 0;
gdjs.ResolvaCode.GDbackgroundObjects4.length = 0;
gdjs.ResolvaCode.GDboxNumeroObjects1.length = 0;
gdjs.ResolvaCode.GDboxNumeroObjects2.length = 0;
gdjs.ResolvaCode.GDboxNumeroObjects3.length = 0;
gdjs.ResolvaCode.GDboxNumeroObjects4.length = 0;
gdjs.ResolvaCode.GDboxSucessorObjects1.length = 0;
gdjs.ResolvaCode.GDboxSucessorObjects2.length = 0;
gdjs.ResolvaCode.GDboxSucessorObjects3.length = 0;
gdjs.ResolvaCode.GDboxSucessorObjects4.length = 0;
gdjs.ResolvaCode.GDboxAntecessorObjects1.length = 0;
gdjs.ResolvaCode.GDboxAntecessorObjects2.length = 0;
gdjs.ResolvaCode.GDboxAntecessorObjects3.length = 0;
gdjs.ResolvaCode.GDboxAntecessorObjects4.length = 0;
gdjs.ResolvaCode.GDlblNumeroObjects1.length = 0;
gdjs.ResolvaCode.GDlblNumeroObjects2.length = 0;
gdjs.ResolvaCode.GDlblNumeroObjects3.length = 0;
gdjs.ResolvaCode.GDlblNumeroObjects4.length = 0;
gdjs.ResolvaCode.GDlblSucessorObjects1.length = 0;
gdjs.ResolvaCode.GDlblSucessorObjects2.length = 0;
gdjs.ResolvaCode.GDlblSucessorObjects3.length = 0;
gdjs.ResolvaCode.GDlblSucessorObjects4.length = 0;
gdjs.ResolvaCode.GDlblAntecessorObjects1.length = 0;
gdjs.ResolvaCode.GDlblAntecessorObjects2.length = 0;
gdjs.ResolvaCode.GDlblAntecessorObjects3.length = 0;
gdjs.ResolvaCode.GDlblAntecessorObjects4.length = 0;
gdjs.ResolvaCode.GDNewObjectObjects1.length = 0;
gdjs.ResolvaCode.GDNewObjectObjects2.length = 0;
gdjs.ResolvaCode.GDNewObjectObjects3.length = 0;
gdjs.ResolvaCode.GDNewObjectObjects4.length = 0;
gdjs.ResolvaCode.GDNewObject2Objects1.length = 0;
gdjs.ResolvaCode.GDNewObject2Objects2.length = 0;
gdjs.ResolvaCode.GDNewObject2Objects3.length = 0;
gdjs.ResolvaCode.GDNewObject2Objects4.length = 0;
gdjs.ResolvaCode.GDNewObject3Objects1.length = 0;
gdjs.ResolvaCode.GDNewObject3Objects2.length = 0;
gdjs.ResolvaCode.GDNewObject3Objects3.length = 0;
gdjs.ResolvaCode.GDNewObject3Objects4.length = 0;
gdjs.ResolvaCode.GDnumero1Objects1.length = 0;
gdjs.ResolvaCode.GDnumero1Objects2.length = 0;
gdjs.ResolvaCode.GDnumero1Objects3.length = 0;
gdjs.ResolvaCode.GDnumero1Objects4.length = 0;
gdjs.ResolvaCode.GDnumero2Objects1.length = 0;
gdjs.ResolvaCode.GDnumero2Objects2.length = 0;
gdjs.ResolvaCode.GDnumero2Objects3.length = 0;
gdjs.ResolvaCode.GDnumero2Objects4.length = 0;
gdjs.ResolvaCode.GDnumero3Objects1.length = 0;
gdjs.ResolvaCode.GDnumero3Objects2.length = 0;
gdjs.ResolvaCode.GDnumero3Objects3.length = 0;
gdjs.ResolvaCode.GDnumero3Objects4.length = 0;
gdjs.ResolvaCode.GDnumero4Objects1.length = 0;
gdjs.ResolvaCode.GDnumero4Objects2.length = 0;
gdjs.ResolvaCode.GDnumero4Objects3.length = 0;
gdjs.ResolvaCode.GDnumero4Objects4.length = 0;
gdjs.ResolvaCode.GDnumero5Objects1.length = 0;
gdjs.ResolvaCode.GDnumero5Objects2.length = 0;
gdjs.ResolvaCode.GDnumero5Objects3.length = 0;
gdjs.ResolvaCode.GDnumero5Objects4.length = 0;
gdjs.ResolvaCode.GDnumero6Objects1.length = 0;
gdjs.ResolvaCode.GDnumero6Objects2.length = 0;
gdjs.ResolvaCode.GDnumero6Objects3.length = 0;
gdjs.ResolvaCode.GDnumero6Objects4.length = 0;
gdjs.ResolvaCode.GDdialogObjects1.length = 0;
gdjs.ResolvaCode.GDdialogObjects2.length = 0;
gdjs.ResolvaCode.GDdialogObjects3.length = 0;
gdjs.ResolvaCode.GDdialogObjects4.length = 0;
gdjs.ResolvaCode.GDdialogMensagemObjects1.length = 0;
gdjs.ResolvaCode.GDdialogMensagemObjects2.length = 0;
gdjs.ResolvaCode.GDdialogMensagemObjects3.length = 0;
gdjs.ResolvaCode.GDdialogMensagemObjects4.length = 0;
gdjs.ResolvaCode.GDdialogAcertouObjects1.length = 0;
gdjs.ResolvaCode.GDdialogAcertouObjects2.length = 0;
gdjs.ResolvaCode.GDdialogAcertouObjects3.length = 0;
gdjs.ResolvaCode.GDdialogAcertouObjects4.length = 0;

gdjs.ResolvaCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['ResolvaCode']= gdjs.ResolvaCode;
