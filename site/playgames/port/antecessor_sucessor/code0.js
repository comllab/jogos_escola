gdjs.StartCode = {};
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDplayerObjects1= [];
gdjs.StartCode.GDplayerObjects2= [];
gdjs.StartCode.GDballObjects1= [];
gdjs.StartCode.GDballObjects2= [];
gdjs.StartCode.GDNewObjectObjects1= [];
gdjs.StartCode.GDNewObjectObjects2= [];
gdjs.StartCode.GDNewObject2Objects1= [];
gdjs.StartCode.GDNewObject2Objects2= [];
gdjs.StartCode.GDNewObject3Objects1= [];
gdjs.StartCode.GDNewObject3Objects2= [];
gdjs.StartCode.GDNewObject4Objects1= [];
gdjs.StartCode.GDNewObject4Objects2= [];
gdjs.StartCode.GDNewObject5Objects1= [];
gdjs.StartCode.GDNewObject5Objects2= [];
gdjs.StartCode.GDNewObject6Objects1= [];
gdjs.StartCode.GDNewObject6Objects2= [];
gdjs.StartCode.GDNewObject7Objects1= [];
gdjs.StartCode.GDNewObject7Objects2= [];
gdjs.StartCode.GDstartObjects1= [];
gdjs.StartCode.GDstartObjects2= [];
gdjs.StartCode.GDlblJogarObjects1= [];
gdjs.StartCode.GDlblJogarObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};
gdjs.StartCode.condition3IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDstartObjects1Objects = Hashtable.newFrom({"start": gdjs.StartCode.GDstartObjects1});gdjs.StartCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Boss Theme.ogg", 1, false, 40, 1);
}}

}


{

gdjs.StartCode.GDstartObjects1.createFrom(runtimeScene.getObjects("start"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDstartObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = context.triggerOnce(8469676);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xaf630


gdjs.StartCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDplayerObjects1.length = 0;
gdjs.StartCode.GDplayerObjects2.length = 0;
gdjs.StartCode.GDballObjects1.length = 0;
gdjs.StartCode.GDballObjects2.length = 0;
gdjs.StartCode.GDNewObjectObjects1.length = 0;
gdjs.StartCode.GDNewObjectObjects2.length = 0;
gdjs.StartCode.GDNewObject2Objects1.length = 0;
gdjs.StartCode.GDNewObject2Objects2.length = 0;
gdjs.StartCode.GDNewObject3Objects1.length = 0;
gdjs.StartCode.GDNewObject3Objects2.length = 0;
gdjs.StartCode.GDNewObject4Objects1.length = 0;
gdjs.StartCode.GDNewObject4Objects2.length = 0;
gdjs.StartCode.GDNewObject5Objects1.length = 0;
gdjs.StartCode.GDNewObject5Objects2.length = 0;
gdjs.StartCode.GDNewObject6Objects1.length = 0;
gdjs.StartCode.GDNewObject6Objects2.length = 0;
gdjs.StartCode.GDNewObject7Objects1.length = 0;
gdjs.StartCode.GDNewObject7Objects2.length = 0;
gdjs.StartCode.GDstartObjects1.length = 0;
gdjs.StartCode.GDstartObjects2.length = 0;
gdjs.StartCode.GDlblJogarObjects1.length = 0;
gdjs.StartCode.GDlblJogarObjects2.length = 0;

gdjs.StartCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['StartCode']= gdjs.StartCode;
