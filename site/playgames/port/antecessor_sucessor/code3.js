gdjs.WinCode = {};
gdjs.WinCode.GDbackgroundObjects1= [];
gdjs.WinCode.GDbackgroundObjects2= [];
gdjs.WinCode.GDbackgroundObjects3= [];
gdjs.WinCode.GDNewObjectObjects1= [];
gdjs.WinCode.GDNewObjectObjects2= [];
gdjs.WinCode.GDNewObjectObjects3= [];
gdjs.WinCode.GDNewObject2Objects1= [];
gdjs.WinCode.GDNewObject2Objects2= [];
gdjs.WinCode.GDNewObject2Objects3= [];
gdjs.WinCode.GDrestartObjects1= [];
gdjs.WinCode.GDrestartObjects2= [];
gdjs.WinCode.GDrestartObjects3= [];
gdjs.WinCode.GDNewObject3Objects1= [];
gdjs.WinCode.GDNewObject3Objects2= [];
gdjs.WinCode.GDNewObject3Objects3= [];
gdjs.WinCode.GDcompletouObjects1= [];
gdjs.WinCode.GDcompletouObjects2= [];
gdjs.WinCode.GDcompletouObjects3= [];

gdjs.WinCode.conditionTrue_0 = {val:false};
gdjs.WinCode.condition0IsTrue_0 = {val:false};
gdjs.WinCode.condition1IsTrue_0 = {val:false};
gdjs.WinCode.condition2IsTrue_0 = {val:false};
gdjs.WinCode.condition3IsTrue_0 = {val:false};
gdjs.WinCode.conditionTrue_1 = {val:false};
gdjs.WinCode.condition0IsTrue_1 = {val:false};
gdjs.WinCode.condition1IsTrue_1 = {val:false};
gdjs.WinCode.condition2IsTrue_1 = {val:false};
gdjs.WinCode.condition3IsTrue_1 = {val:false};


gdjs.WinCode.eventsList0x8130f8 = function(runtimeScene, context) {

{


gdjs.WinCode.condition0IsTrue_0.val = false;
{
gdjs.WinCode.condition0IsTrue_0.val = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}if (gdjs.WinCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Boss Theme.ogg", 1, false, 40, 1);
}}

}


{


gdjs.WinCode.condition0IsTrue_0.val = false;
{
gdjs.WinCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3));
}if (gdjs.WinCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}}

}


{


gdjs.WinCode.condition0IsTrue_0.val = false;
{
gdjs.WinCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3));
}if (gdjs.WinCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}}

}


{


gdjs.WinCode.condition0IsTrue_0.val = false;
{
{gdjs.WinCode.conditionTrue_1 = gdjs.WinCode.condition0IsTrue_0;
gdjs.WinCode.conditionTrue_1.val = context.triggerOnce(8468956);
}
}if (gdjs.WinCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise06.ogg", false, 100, 1);
}}

}


}; //End of gdjs.WinCode.eventsList0x8130f8
gdjs.WinCode.mapOfGDgdjs_46WinCode_46GDrestartObjects1Objects = Hashtable.newFrom({"restart": gdjs.WinCode.GDrestartObjects1});gdjs.WinCode.eventsList0xaf630 = function(runtimeScene, context) {

{


gdjs.WinCode.condition0IsTrue_0.val = false;
{
gdjs.WinCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.WinCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.WinCode.eventsList0x8130f8(runtimeScene, context);} //End of subevents
}

}


{

gdjs.WinCode.GDrestartObjects1.createFrom(runtimeScene.getObjects("restart"));

gdjs.WinCode.condition0IsTrue_0.val = false;
gdjs.WinCode.condition1IsTrue_0.val = false;
gdjs.WinCode.condition2IsTrue_0.val = false;
{
gdjs.WinCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.WinCode.mapOfGDgdjs_46WinCode_46GDrestartObjects1Objects, runtimeScene, true, false);
}if ( gdjs.WinCode.condition0IsTrue_0.val ) {
{
gdjs.WinCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.WinCode.condition1IsTrue_0.val ) {
{
{gdjs.WinCode.conditionTrue_1 = gdjs.WinCode.condition2IsTrue_0;
gdjs.WinCode.conditionTrue_1.val = context.triggerOnce(8487044);
}
}}
}
if (gdjs.WinCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.WinCode.eventsList0xaf630


gdjs.WinCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.WinCode.GDbackgroundObjects1.length = 0;
gdjs.WinCode.GDbackgroundObjects2.length = 0;
gdjs.WinCode.GDbackgroundObjects3.length = 0;
gdjs.WinCode.GDNewObjectObjects1.length = 0;
gdjs.WinCode.GDNewObjectObjects2.length = 0;
gdjs.WinCode.GDNewObjectObjects3.length = 0;
gdjs.WinCode.GDNewObject2Objects1.length = 0;
gdjs.WinCode.GDNewObject2Objects2.length = 0;
gdjs.WinCode.GDNewObject2Objects3.length = 0;
gdjs.WinCode.GDrestartObjects1.length = 0;
gdjs.WinCode.GDrestartObjects2.length = 0;
gdjs.WinCode.GDrestartObjects3.length = 0;
gdjs.WinCode.GDNewObject3Objects1.length = 0;
gdjs.WinCode.GDNewObject3Objects2.length = 0;
gdjs.WinCode.GDNewObject3Objects3.length = 0;
gdjs.WinCode.GDcompletouObjects1.length = 0;
gdjs.WinCode.GDcompletouObjects2.length = 0;
gdjs.WinCode.GDcompletouObjects3.length = 0;

gdjs.WinCode.eventsList0xaf630(runtimeScene, context);return;
}
gdjs['WinCode']= gdjs.WinCode;
