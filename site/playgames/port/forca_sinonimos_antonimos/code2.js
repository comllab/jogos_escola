gdjs.GameOverCode = {};
gdjs.GameOverCode.GDletrasObjects1= [];
gdjs.GameOverCode.GDletrasObjects2= [];
gdjs.GameOverCode.GDforcaObjects1= [];
gdjs.GameOverCode.GDforcaObjects2= [];
gdjs.GameOverCode.GDbackgroundObjects1= [];
gdjs.GameOverCode.GDbackgroundObjects2= [];
gdjs.GameOverCode.GDdicasObjects1= [];
gdjs.GameOverCode.GDdicasObjects2= [];
gdjs.GameOverCode.GDtecladoObjects1= [];
gdjs.GameOverCode.GDtecladoObjects2= [];
gdjs.GameOverCode.GDbtAjudaObjects1= [];
gdjs.GameOverCode.GDbtAjudaObjects2= [];
gdjs.GameOverCode.GDbtPlayObjects1= [];
gdjs.GameOverCode.GDbtPlayObjects2= [];
gdjs.GameOverCode.GDbtRecarregarObjects1= [];
gdjs.GameOverCode.GDbtRecarregarObjects2= [];
gdjs.GameOverCode.GDbuttonDicasObjects1= [];
gdjs.GameOverCode.GDbuttonDicasObjects2= [];
gdjs.GameOverCode.GDlblDicasObjects1= [];
gdjs.GameOverCode.GDlblDicasObjects2= [];
gdjs.GameOverCode.GDlabel3Objects1= [];
gdjs.GameOverCode.GDlabel3Objects2= [];
gdjs.GameOverCode.GDlabel1Objects1= [];
gdjs.GameOverCode.GDlabel1Objects2= [];
gdjs.GameOverCode.GDlabel4Objects1= [];
gdjs.GameOverCode.GDlabel4Objects2= [];
gdjs.GameOverCode.GDlabel2Objects1= [];
gdjs.GameOverCode.GDlabel2Objects2= [];
gdjs.GameOverCode.GDlabel3Objects1= [];
gdjs.GameOverCode.GDlabel3Objects2= [];
gdjs.GameOverCode.GDlabelObjects1= [];
gdjs.GameOverCode.GDlabelObjects2= [];
gdjs.GameOverCode.GDbtContinuarObjects1= [];
gdjs.GameOverCode.GDbtContinuarObjects2= [];
gdjs.GameOverCode.GDpalavra3Objects1= [];
gdjs.GameOverCode.GDpalavra3Objects2= [];
gdjs.GameOverCode.GDpalavra2Objects1= [];
gdjs.GameOverCode.GDpalavra2Objects2= [];
gdjs.GameOverCode.GDpalavra1Objects1= [];
gdjs.GameOverCode.GDpalavra1Objects2= [];
gdjs.GameOverCode.GDforca_95gameoverObjects1= [];
gdjs.GameOverCode.GDforca_95gameoverObjects2= [];

gdjs.GameOverCode.conditionTrue_0 = {val:false};
gdjs.GameOverCode.condition0IsTrue_0 = {val:false};
gdjs.GameOverCode.condition1IsTrue_0 = {val:false};
gdjs.GameOverCode.condition2IsTrue_0 = {val:false};
gdjs.GameOverCode.condition3IsTrue_0 = {val:false};
gdjs.GameOverCode.conditionTrue_1 = {val:false};
gdjs.GameOverCode.condition0IsTrue_1 = {val:false};
gdjs.GameOverCode.condition1IsTrue_1 = {val:false};
gdjs.GameOverCode.condition2IsTrue_1 = {val:false};
gdjs.GameOverCode.condition3IsTrue_1 = {val:false};


gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbtContinuarObjects1Objects = Hashtable.newFrom({"btContinuar": gdjs.GameOverCode.GDbtContinuarObjects1});gdjs.GameOverCode.eventsList0x705090 = function(runtimeScene) {

{


{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.GameOverCode.eventsList0x705090
gdjs.GameOverCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameOverCode.condition0IsTrue_0.val) {
gdjs.GameOverCode.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.GameOverCode.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.GameOverCode.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{for(var i = 0, len = gdjs.GameOverCode.GDpalavra1Objects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDpalavra1Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}
}{for(var i = 0, len = gdjs.GameOverCode.GDpalavra2Objects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDpalavra2Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(5)));
}
}{for(var i = 0, len = gdjs.GameOverCode.GDpalavra3Objects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDpalavra3Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(6)));
}
}}

}


{

gdjs.GameOverCode.GDbtContinuarObjects1.createFrom(runtimeScene.getObjects("btContinuar"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
gdjs.GameOverCode.condition2IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbtContinuarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameOverCode.condition1IsTrue_0.val ) {
{
{gdjs.GameOverCode.conditionTrue_1 = gdjs.GameOverCode.condition2IsTrue_0;
gdjs.GameOverCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7361116);
}
}}
}
if (gdjs.GameOverCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.GameOverCode.eventsList0x705090(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.GameOverCode.eventsList0xaff48


gdjs.GameOverCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.GameOverCode.GDletrasObjects1.length = 0;
gdjs.GameOverCode.GDletrasObjects2.length = 0;
gdjs.GameOverCode.GDforcaObjects1.length = 0;
gdjs.GameOverCode.GDforcaObjects2.length = 0;
gdjs.GameOverCode.GDbackgroundObjects1.length = 0;
gdjs.GameOverCode.GDbackgroundObjects2.length = 0;
gdjs.GameOverCode.GDdicasObjects1.length = 0;
gdjs.GameOverCode.GDdicasObjects2.length = 0;
gdjs.GameOverCode.GDtecladoObjects1.length = 0;
gdjs.GameOverCode.GDtecladoObjects2.length = 0;
gdjs.GameOverCode.GDbtAjudaObjects1.length = 0;
gdjs.GameOverCode.GDbtAjudaObjects2.length = 0;
gdjs.GameOverCode.GDbtPlayObjects1.length = 0;
gdjs.GameOverCode.GDbtPlayObjects2.length = 0;
gdjs.GameOverCode.GDbtRecarregarObjects1.length = 0;
gdjs.GameOverCode.GDbtRecarregarObjects2.length = 0;
gdjs.GameOverCode.GDbuttonDicasObjects1.length = 0;
gdjs.GameOverCode.GDbuttonDicasObjects2.length = 0;
gdjs.GameOverCode.GDlblDicasObjects1.length = 0;
gdjs.GameOverCode.GDlblDicasObjects2.length = 0;
gdjs.GameOverCode.GDlabel3Objects1.length = 0;
gdjs.GameOverCode.GDlabel3Objects2.length = 0;
gdjs.GameOverCode.GDlabel1Objects1.length = 0;
gdjs.GameOverCode.GDlabel1Objects2.length = 0;
gdjs.GameOverCode.GDlabel4Objects1.length = 0;
gdjs.GameOverCode.GDlabel4Objects2.length = 0;
gdjs.GameOverCode.GDlabel2Objects1.length = 0;
gdjs.GameOverCode.GDlabel2Objects2.length = 0;
gdjs.GameOverCode.GDlabel3Objects1.length = 0;
gdjs.GameOverCode.GDlabel3Objects2.length = 0;
gdjs.GameOverCode.GDlabelObjects1.length = 0;
gdjs.GameOverCode.GDlabelObjects2.length = 0;
gdjs.GameOverCode.GDbtContinuarObjects1.length = 0;
gdjs.GameOverCode.GDbtContinuarObjects2.length = 0;
gdjs.GameOverCode.GDpalavra3Objects1.length = 0;
gdjs.GameOverCode.GDpalavra3Objects2.length = 0;
gdjs.GameOverCode.GDpalavra2Objects1.length = 0;
gdjs.GameOverCode.GDpalavra2Objects2.length = 0;
gdjs.GameOverCode.GDpalavra1Objects1.length = 0;
gdjs.GameOverCode.GDpalavra1Objects2.length = 0;
gdjs.GameOverCode.GDforca_95gameoverObjects1.length = 0;
gdjs.GameOverCode.GDforca_95gameoverObjects2.length = 0;

gdjs.GameOverCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['GameOverCode'] = gdjs.GameOverCode;
