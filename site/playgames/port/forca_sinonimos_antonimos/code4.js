gdjs.AjudaCode = {};
gdjs.AjudaCode.GDletrasObjects1= [];
gdjs.AjudaCode.GDletrasObjects2= [];
gdjs.AjudaCode.GDforcaObjects1= [];
gdjs.AjudaCode.GDforcaObjects2= [];
gdjs.AjudaCode.GDbackgroundObjects1= [];
gdjs.AjudaCode.GDbackgroundObjects2= [];
gdjs.AjudaCode.GDdicasObjects1= [];
gdjs.AjudaCode.GDdicasObjects2= [];
gdjs.AjudaCode.GDtecladoObjects1= [];
gdjs.AjudaCode.GDtecladoObjects2= [];
gdjs.AjudaCode.GDbtAjudaObjects1= [];
gdjs.AjudaCode.GDbtAjudaObjects2= [];
gdjs.AjudaCode.GDbtPlayObjects1= [];
gdjs.AjudaCode.GDbtPlayObjects2= [];
gdjs.AjudaCode.GDbtRecarregarObjects1= [];
gdjs.AjudaCode.GDbtRecarregarObjects2= [];
gdjs.AjudaCode.GDbuttonDicasObjects1= [];
gdjs.AjudaCode.GDbuttonDicasObjects2= [];
gdjs.AjudaCode.GDlblDicasObjects1= [];
gdjs.AjudaCode.GDlblDicasObjects2= [];
gdjs.AjudaCode.GDlabel3Objects1= [];
gdjs.AjudaCode.GDlabel3Objects2= [];
gdjs.AjudaCode.GDlabel1Objects1= [];
gdjs.AjudaCode.GDlabel1Objects2= [];
gdjs.AjudaCode.GDbtVoltarObjects1= [];
gdjs.AjudaCode.GDbtVoltarObjects2= [];
gdjs.AjudaCode.GDantonimo_95definicaoObjects1= [];
gdjs.AjudaCode.GDantonimo_95definicaoObjects2= [];
gdjs.AjudaCode.GDsinonimo_95definicaoObjects1= [];
gdjs.AjudaCode.GDsinonimo_95definicaoObjects2= [];
gdjs.AjudaCode.GDlabelObjects1= [];
gdjs.AjudaCode.GDlabelObjects2= [];
gdjs.AjudaCode.GDantonimoObjects1= [];
gdjs.AjudaCode.GDantonimoObjects2= [];
gdjs.AjudaCode.GDsinonimoObjects1= [];
gdjs.AjudaCode.GDsinonimoObjects2= [];

gdjs.AjudaCode.conditionTrue_0 = {val:false};
gdjs.AjudaCode.condition0IsTrue_0 = {val:false};
gdjs.AjudaCode.condition1IsTrue_0 = {val:false};
gdjs.AjudaCode.condition2IsTrue_0 = {val:false};
gdjs.AjudaCode.condition3IsTrue_0 = {val:false};
gdjs.AjudaCode.conditionTrue_1 = {val:false};
gdjs.AjudaCode.condition0IsTrue_1 = {val:false};
gdjs.AjudaCode.condition1IsTrue_1 = {val:false};
gdjs.AjudaCode.condition2IsTrue_1 = {val:false};
gdjs.AjudaCode.condition3IsTrue_1 = {val:false};


gdjs.AjudaCode.mapOfGDgdjs_46AjudaCode_46GDbtVoltarObjects1Objects = Hashtable.newFrom({"btVoltar": gdjs.AjudaCode.GDbtVoltarObjects1});gdjs.AjudaCode.eventsList0xaff48 = function(runtimeScene) {

{

gdjs.AjudaCode.GDbtVoltarObjects1.createFrom(runtimeScene.getObjects("btVoltar"));

gdjs.AjudaCode.condition0IsTrue_0.val = false;
gdjs.AjudaCode.condition1IsTrue_0.val = false;
gdjs.AjudaCode.condition2IsTrue_0.val = false;
{
gdjs.AjudaCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.AjudaCode.mapOfGDgdjs_46AjudaCode_46GDbtVoltarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.AjudaCode.condition0IsTrue_0.val ) {
{
gdjs.AjudaCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.AjudaCode.condition1IsTrue_0.val ) {
{
{gdjs.AjudaCode.conditionTrue_1 = gdjs.AjudaCode.condition2IsTrue_0;
gdjs.AjudaCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7372588);
}
}}
}
if (gdjs.AjudaCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.popScene(runtimeScene);
}}

}


}; //End of gdjs.AjudaCode.eventsList0xaff48


gdjs.AjudaCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.AjudaCode.GDletrasObjects1.length = 0;
gdjs.AjudaCode.GDletrasObjects2.length = 0;
gdjs.AjudaCode.GDforcaObjects1.length = 0;
gdjs.AjudaCode.GDforcaObjects2.length = 0;
gdjs.AjudaCode.GDbackgroundObjects1.length = 0;
gdjs.AjudaCode.GDbackgroundObjects2.length = 0;
gdjs.AjudaCode.GDdicasObjects1.length = 0;
gdjs.AjudaCode.GDdicasObjects2.length = 0;
gdjs.AjudaCode.GDtecladoObjects1.length = 0;
gdjs.AjudaCode.GDtecladoObjects2.length = 0;
gdjs.AjudaCode.GDbtAjudaObjects1.length = 0;
gdjs.AjudaCode.GDbtAjudaObjects2.length = 0;
gdjs.AjudaCode.GDbtPlayObjects1.length = 0;
gdjs.AjudaCode.GDbtPlayObjects2.length = 0;
gdjs.AjudaCode.GDbtRecarregarObjects1.length = 0;
gdjs.AjudaCode.GDbtRecarregarObjects2.length = 0;
gdjs.AjudaCode.GDbuttonDicasObjects1.length = 0;
gdjs.AjudaCode.GDbuttonDicasObjects2.length = 0;
gdjs.AjudaCode.GDlblDicasObjects1.length = 0;
gdjs.AjudaCode.GDlblDicasObjects2.length = 0;
gdjs.AjudaCode.GDlabel3Objects1.length = 0;
gdjs.AjudaCode.GDlabel3Objects2.length = 0;
gdjs.AjudaCode.GDlabel1Objects1.length = 0;
gdjs.AjudaCode.GDlabel1Objects2.length = 0;
gdjs.AjudaCode.GDbtVoltarObjects1.length = 0;
gdjs.AjudaCode.GDbtVoltarObjects2.length = 0;
gdjs.AjudaCode.GDantonimo_95definicaoObjects1.length = 0;
gdjs.AjudaCode.GDantonimo_95definicaoObjects2.length = 0;
gdjs.AjudaCode.GDsinonimo_95definicaoObjects1.length = 0;
gdjs.AjudaCode.GDsinonimo_95definicaoObjects2.length = 0;
gdjs.AjudaCode.GDlabelObjects1.length = 0;
gdjs.AjudaCode.GDlabelObjects2.length = 0;
gdjs.AjudaCode.GDantonimoObjects1.length = 0;
gdjs.AjudaCode.GDantonimoObjects2.length = 0;
gdjs.AjudaCode.GDsinonimoObjects1.length = 0;
gdjs.AjudaCode.GDsinonimoObjects2.length = 0;

gdjs.AjudaCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['AjudaCode'] = gdjs.AjudaCode;
