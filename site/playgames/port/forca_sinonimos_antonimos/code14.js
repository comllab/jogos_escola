gdjs.Palavras10Code = {};
gdjs.Palavras10Code.forEachIndex2 = 0;

gdjs.Palavras10Code.forEachIndex3 = 0;

gdjs.Palavras10Code.forEachObjects2 = [];

gdjs.Palavras10Code.forEachObjects3 = [];

gdjs.Palavras10Code.forEachTemporary2 = null;

gdjs.Palavras10Code.forEachTemporary3 = null;

gdjs.Palavras10Code.forEachTotalCount2 = 0;

gdjs.Palavras10Code.forEachTotalCount3 = 0;

gdjs.Palavras10Code.GDletrasObjects1= [];
gdjs.Palavras10Code.GDletrasObjects2= [];
gdjs.Palavras10Code.GDletrasObjects3= [];
gdjs.Palavras10Code.GDforcaObjects1= [];
gdjs.Palavras10Code.GDforcaObjects2= [];
gdjs.Palavras10Code.GDforcaObjects3= [];
gdjs.Palavras10Code.GDbackgroundObjects1= [];
gdjs.Palavras10Code.GDbackgroundObjects2= [];
gdjs.Palavras10Code.GDbackgroundObjects3= [];
gdjs.Palavras10Code.GDdicasObjects1= [];
gdjs.Palavras10Code.GDdicasObjects2= [];
gdjs.Palavras10Code.GDdicasObjects3= [];
gdjs.Palavras10Code.GDtecladoObjects1= [];
gdjs.Palavras10Code.GDtecladoObjects2= [];
gdjs.Palavras10Code.GDtecladoObjects3= [];
gdjs.Palavras10Code.GDbtAjudaObjects1= [];
gdjs.Palavras10Code.GDbtAjudaObjects2= [];
gdjs.Palavras10Code.GDbtAjudaObjects3= [];
gdjs.Palavras10Code.GDbtPlayObjects1= [];
gdjs.Palavras10Code.GDbtPlayObjects2= [];
gdjs.Palavras10Code.GDbtPlayObjects3= [];
gdjs.Palavras10Code.GDbtRecarregarObjects1= [];
gdjs.Palavras10Code.GDbtRecarregarObjects2= [];
gdjs.Palavras10Code.GDbtRecarregarObjects3= [];
gdjs.Palavras10Code.GDbuttonDicasObjects1= [];
gdjs.Palavras10Code.GDbuttonDicasObjects2= [];
gdjs.Palavras10Code.GDbuttonDicasObjects3= [];
gdjs.Palavras10Code.GDlblDicasObjects1= [];
gdjs.Palavras10Code.GDlblDicasObjects2= [];
gdjs.Palavras10Code.GDlblDicasObjects3= [];
gdjs.Palavras10Code.GDlabel3Objects1= [];
gdjs.Palavras10Code.GDlabel3Objects2= [];
gdjs.Palavras10Code.GDlabel3Objects3= [];
gdjs.Palavras10Code.GDlabel1Objects1= [];
gdjs.Palavras10Code.GDlabel1Objects2= [];
gdjs.Palavras10Code.GDlabel1Objects3= [];
gdjs.Palavras10Code.GDpalavra1Objects1= [];
gdjs.Palavras10Code.GDpalavra1Objects2= [];
gdjs.Palavras10Code.GDpalavra1Objects3= [];
gdjs.Palavras10Code.GDpalavra2Objects1= [];
gdjs.Palavras10Code.GDpalavra2Objects2= [];
gdjs.Palavras10Code.GDpalavra2Objects3= [];
gdjs.Palavras10Code.GDpalavra3Objects1= [];
gdjs.Palavras10Code.GDpalavra3Objects2= [];
gdjs.Palavras10Code.GDpalavra3Objects3= [];
gdjs.Palavras10Code.GDdica1Objects1= [];
gdjs.Palavras10Code.GDdica1Objects2= [];
gdjs.Palavras10Code.GDdica1Objects3= [];
gdjs.Palavras10Code.GDdica2Objects1= [];
gdjs.Palavras10Code.GDdica2Objects2= [];
gdjs.Palavras10Code.GDdica2Objects3= [];
gdjs.Palavras10Code.GDdica3Objects1= [];
gdjs.Palavras10Code.GDdica3Objects2= [];
gdjs.Palavras10Code.GDdica3Objects3= [];

gdjs.Palavras10Code.conditionTrue_0 = {val:false};
gdjs.Palavras10Code.condition0IsTrue_0 = {val:false};
gdjs.Palavras10Code.condition1IsTrue_0 = {val:false};
gdjs.Palavras10Code.condition2IsTrue_0 = {val:false};
gdjs.Palavras10Code.condition3IsTrue_0 = {val:false};
gdjs.Palavras10Code.condition4IsTrue_0 = {val:false};
gdjs.Palavras10Code.conditionTrue_1 = {val:false};
gdjs.Palavras10Code.condition0IsTrue_1 = {val:false};
gdjs.Palavras10Code.condition1IsTrue_1 = {val:false};
gdjs.Palavras10Code.condition2IsTrue_1 = {val:false};
gdjs.Palavras10Code.condition3IsTrue_1 = {val:false};
gdjs.Palavras10Code.condition4IsTrue_1 = {val:false};


gdjs.Palavras10Code.eventsList0x701528 = function(runtimeScene) {

{

gdjs.Palavras10Code.GDletrasObjects1.createFrom(runtimeScene.getObjects("letras"));

gdjs.Palavras10Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras10Code.GDletrasObjects1.length;i<l;++i) {
    if ( gdjs.Palavras10Code.GDletrasObjects1[i].getVariableString(gdjs.Palavras10Code.GDletrasObjects1[i].getVariables().getFromIndex(0)) == "#" ) {
        gdjs.Palavras10Code.condition0IsTrue_0.val = true;
        gdjs.Palavras10Code.GDletrasObjects1[k] = gdjs.Palavras10Code.GDletrasObjects1[i];
        ++k;
    }
}
gdjs.Palavras10Code.GDletrasObjects1.length = k;}if (gdjs.Palavras10Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Palavras10Code.GDletrasObjects1 */
{for(var i = 0, len = gdjs.Palavras10Code.GDletrasObjects1.length ;i < len;++i) {
    gdjs.Palavras10Code.GDletrasObjects1[i].hide();
}
}}

}


}; //End of gdjs.Palavras10Code.eventsList0x701528
gdjs.Palavras10Code.mapOfGDgdjs_46Palavras10Code_46GDtecladoObjects1Objects = Hashtable.newFrom({"teclado": gdjs.Palavras10Code.GDtecladoObjects1});gdjs.Palavras10Code.eventsList0x736b10 = function(runtimeScene) {

}; //End of gdjs.Palavras10Code.eventsList0x736b10
gdjs.Palavras10Code.eventsList0x7366d0 = function(runtimeScene) {

{

gdjs.Palavras10Code.GDletrasObjects2.createFrom(runtimeScene.getObjects("letras"));

for(gdjs.Palavras10Code.forEachIndex3 = 0;gdjs.Palavras10Code.forEachIndex3 < gdjs.Palavras10Code.GDletrasObjects2.length;++gdjs.Palavras10Code.forEachIndex3) {
gdjs.Palavras10Code.GDletrasObjects3.createFrom(gdjs.Palavras10Code.GDletrasObjects2);

gdjs.Palavras10Code.GDtecladoObjects3.createFrom(gdjs.Palavras10Code.GDtecladoObjects1);


gdjs.Palavras10Code.forEachTemporary3 = gdjs.Palavras10Code.GDletrasObjects2[gdjs.Palavras10Code.forEachIndex3];
gdjs.Palavras10Code.GDletrasObjects3.length = 0;
gdjs.Palavras10Code.GDletrasObjects3.push(gdjs.Palavras10Code.forEachTemporary3);
gdjs.Palavras10Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras10Code.GDletrasObjects3.length;i<l;++i) {
    if ( gdjs.Palavras10Code.GDletrasObjects3[i].getVariableString(gdjs.Palavras10Code.GDletrasObjects3[i].getVariables().getFromIndex(0)) == (( gdjs.Palavras10Code.GDtecladoObjects3.length === 0 ) ? "" :gdjs.Palavras10Code.GDtecladoObjects3[0].getAnimationName()) ) {
        gdjs.Palavras10Code.condition0IsTrue_0.val = true;
        gdjs.Palavras10Code.GDletrasObjects3[k] = gdjs.Palavras10Code.GDletrasObjects3[i];
        ++k;
    }
}
gdjs.Palavras10Code.GDletrasObjects3.length = k;}if (gdjs.Palavras10Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Palavras10Code.GDletrasObjects3.length ;i < len;++i) {
    gdjs.Palavras10Code.GDletrasObjects3[i].setAnimationName((gdjs.RuntimeObject.getVariableString(gdjs.Palavras10Code.GDletrasObjects3[i].getVariables().getFromIndex(0))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}
}

}


{


gdjs.Palavras10Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras10Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0;
}if (gdjs.Palavras10Code.condition0IsTrue_0.val) {
gdjs.Palavras10Code.GDforcaObjects2.createFrom(runtimeScene.getObjects("forca"));
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras10Code.GDforcaObjects2.length ;i < len;++i) {
    gdjs.Palavras10Code.GDforcaObjects2[i].setAnimation((gdjs.Palavras10Code.GDforcaObjects2[i].getAnimation()) + 1);
}
}}

}


{


gdjs.Palavras10Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras10Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) > 0;
}if (gdjs.Palavras10Code.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise04.ogg", false, 100, 1);
}}

}


}; //End of gdjs.Palavras10Code.eventsList0x7366d0
gdjs.Palavras10Code.mapOfGDgdjs_46Palavras10Code_46GDbuttonDicasObjects1Objects = Hashtable.newFrom({"buttonDicas": gdjs.Palavras10Code.GDbuttonDicasObjects1});gdjs.Palavras10Code.mapOfGDgdjs_46Palavras10Code_46GDdicasObjects1Objects = Hashtable.newFrom({"dicas": gdjs.Palavras10Code.GDdicasObjects1});gdjs.Palavras10Code.eventsList0x7355c0 = function(runtimeScene) {

}; //End of gdjs.Palavras10Code.eventsList0x7355c0
gdjs.Palavras10Code.mapOfGDgdjs_46Palavras10Code_46GDbtAjudaObjects1Objects = Hashtable.newFrom({"btAjuda": gdjs.Palavras10Code.GDbtAjudaObjects1});gdjs.Palavras10Code.mapOfGDgdjs_46Palavras10Code_46GDbtRecarregarObjects1Objects = Hashtable.newFrom({"btRecarregar": gdjs.Palavras10Code.GDbtRecarregarObjects1});gdjs.Palavras10Code.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.Palavras10Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras10Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Palavras10Code.condition0IsTrue_0.val) {
gdjs.Palavras10Code.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.Palavras10Code.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.Palavras10Code.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{gdjs.evtTools.camera.hideLayer(runtimeScene, "dicas");
}{for(var i = 0, len = gdjs.Palavras10Code.GDpalavra1Objects1.length ;i < len;++i) {
    gdjs.Palavras10Code.GDpalavra1Objects1[i].hide();
}
for(var i = 0, len = gdjs.Palavras10Code.GDpalavra2Objects1.length ;i < len;++i) {
    gdjs.Palavras10Code.GDpalavra2Objects1[i].hide();
}
for(var i = 0, len = gdjs.Palavras10Code.GDpalavra3Objects1.length ;i < len;++i) {
    gdjs.Palavras10Code.GDpalavra3Objects1[i].hide();
}
}
{ //Subevents
gdjs.Palavras10Code.eventsList0x701528(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.Palavras10Code.GDtecladoObjects1.createFrom(runtimeScene.getObjects("teclado"));

gdjs.Palavras10Code.condition0IsTrue_0.val = false;
gdjs.Palavras10Code.condition1IsTrue_0.val = false;
gdjs.Palavras10Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras10Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras10Code.mapOfGDgdjs_46Palavras10Code_46GDtecladoObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras10Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras10Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras10Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras10Code.conditionTrue_1 = gdjs.Palavras10Code.condition2IsTrue_0;
gdjs.Palavras10Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7563396);
}
}}
}
if (gdjs.Palavras10Code.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.sound.playSound(runtimeScene, "artista desconhecido - pepdown.ogg", false, 100, 1);
}
{ //Subevents
gdjs.Palavras10Code.eventsList0x7366d0(runtimeScene);} //End of subevents
}

}


{

gdjs.Palavras10Code.GDbuttonDicasObjects1.createFrom(runtimeScene.getObjects("buttonDicas"));

gdjs.Palavras10Code.condition0IsTrue_0.val = false;
gdjs.Palavras10Code.condition1IsTrue_0.val = false;
gdjs.Palavras10Code.condition2IsTrue_0.val = false;
gdjs.Palavras10Code.condition3IsTrue_0.val = false;
{
gdjs.Palavras10Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras10Code.mapOfGDgdjs_46Palavras10Code_46GDbuttonDicasObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras10Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras10Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras10Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Palavras10Code.GDbuttonDicasObjects1.length;i<l;++i) {
    if ( gdjs.Palavras10Code.GDbuttonDicasObjects1[i].isVisible() ) {
        gdjs.Palavras10Code.condition2IsTrue_0.val = true;
        gdjs.Palavras10Code.GDbuttonDicasObjects1[k] = gdjs.Palavras10Code.GDbuttonDicasObjects1[i];
        ++k;
    }
}
gdjs.Palavras10Code.GDbuttonDicasObjects1.length = k;}if ( gdjs.Palavras10Code.condition2IsTrue_0.val ) {
{
{gdjs.Palavras10Code.conditionTrue_1 = gdjs.Palavras10Code.condition3IsTrue_0;
gdjs.Palavras10Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7566844);
}
}}
}
}
if (gdjs.Palavras10Code.condition3IsTrue_0.val) {
/* Reuse gdjs.Palavras10Code.GDbuttonDicasObjects1 */
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise04.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras10Code.GDbuttonDicasObjects1.length ;i < len;++i) {
    gdjs.Palavras10Code.GDbuttonDicasObjects1[i].hide();
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "dicas");
}}

}


{

gdjs.Palavras10Code.GDdicasObjects1.createFrom(runtimeScene.getObjects("dicas"));

gdjs.Palavras10Code.condition0IsTrue_0.val = false;
gdjs.Palavras10Code.condition1IsTrue_0.val = false;
{
gdjs.Palavras10Code.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "dicas");
}if ( gdjs.Palavras10Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras10Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras10Code.mapOfGDgdjs_46Palavras10Code_46GDdicasObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.Palavras10Code.condition1IsTrue_0.val) {
gdjs.Palavras10Code.GDbuttonDicasObjects1.createFrom(runtimeScene.getObjects("buttonDicas"));
{gdjs.evtTools.sound.playSound(runtimeScene, "artista desconhecido - pepdown.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras10Code.GDbuttonDicasObjects1.length ;i < len;++i) {
    gdjs.Palavras10Code.GDbuttonDicasObjects1[i].hide(false);
}
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "dicas");
}}

}


{



}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.Palavras10Code.GDletrasObjects1.createFrom(runtimeScene.getObjects("letras"));

for(gdjs.Palavras10Code.forEachIndex2 = 0;gdjs.Palavras10Code.forEachIndex2 < gdjs.Palavras10Code.GDletrasObjects1.length;++gdjs.Palavras10Code.forEachIndex2) {
gdjs.Palavras10Code.GDletrasObjects2.createFrom(gdjs.Palavras10Code.GDletrasObjects1);


gdjs.Palavras10Code.forEachTemporary2 = gdjs.Palavras10Code.GDletrasObjects1[gdjs.Palavras10Code.forEachIndex2];
gdjs.Palavras10Code.GDletrasObjects2.length = 0;
gdjs.Palavras10Code.GDletrasObjects2.push(gdjs.Palavras10Code.forEachTemporary2);
gdjs.Palavras10Code.condition0IsTrue_0.val = false;
gdjs.Palavras10Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras10Code.GDletrasObjects2.length;i<l;++i) {
    if ( gdjs.Palavras10Code.GDletrasObjects2[i].getVariableString(gdjs.Palavras10Code.GDletrasObjects2[i].getVariables().getFromIndex(0)) != "#" ) {
        gdjs.Palavras10Code.condition0IsTrue_0.val = true;
        gdjs.Palavras10Code.GDletrasObjects2[k] = gdjs.Palavras10Code.GDletrasObjects2[i];
        ++k;
    }
}
gdjs.Palavras10Code.GDletrasObjects2.length = k;}if ( gdjs.Palavras10Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Palavras10Code.GDletrasObjects2.length;i<l;++i) {
    if ( gdjs.Palavras10Code.GDletrasObjects2[i].getAnimation() == 0 ) {
        gdjs.Palavras10Code.condition1IsTrue_0.val = true;
        gdjs.Palavras10Code.GDletrasObjects2[k] = gdjs.Palavras10Code.GDletrasObjects2[i];
        ++k;
    }
}
gdjs.Palavras10Code.GDletrasObjects2.length = k;}}
if (gdjs.Palavras10Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}}
}

}


{


gdjs.Palavras10Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras10Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.Palavras10Code.condition0IsTrue_0.val) {
gdjs.Palavras10Code.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.Palavras10Code.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.Palavras10Code.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{runtimeScene.getGame().getVariables().getFromIndex(4).setString((( gdjs.Palavras10Code.GDpalavra1Objects1.length === 0 ) ? "" :gdjs.Palavras10Code.GDpalavra1Objects1[0].getString()));
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString((( gdjs.Palavras10Code.GDpalavra2Objects1.length === 0 ) ? "" :gdjs.Palavras10Code.GDpalavra2Objects1[0].getString()));
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString((( gdjs.Palavras10Code.GDpalavra3Objects1.length === 0 ) ? "" :gdjs.Palavras10Code.GDpalavra3Objects1[0].getString()));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "End", false);
}}

}


{

gdjs.Palavras10Code.GDforcaObjects1.createFrom(runtimeScene.getObjects("forca"));

gdjs.Palavras10Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras10Code.GDforcaObjects1.length;i<l;++i) {
    if ( gdjs.Palavras10Code.GDforcaObjects1[i].getAnimation() == 6 ) {
        gdjs.Palavras10Code.condition0IsTrue_0.val = true;
        gdjs.Palavras10Code.GDforcaObjects1[k] = gdjs.Palavras10Code.GDforcaObjects1[i];
        ++k;
    }
}
gdjs.Palavras10Code.GDforcaObjects1.length = k;}if (gdjs.Palavras10Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GameOver", false);
}}

}


{



}


{

gdjs.Palavras10Code.GDbtAjudaObjects1.createFrom(runtimeScene.getObjects("btAjuda"));

gdjs.Palavras10Code.condition0IsTrue_0.val = false;
gdjs.Palavras10Code.condition1IsTrue_0.val = false;
gdjs.Palavras10Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras10Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras10Code.mapOfGDgdjs_46Palavras10Code_46GDbtAjudaObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras10Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras10Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras10Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras10Code.conditionTrue_1 = gdjs.Palavras10Code.condition2IsTrue_0;
gdjs.Palavras10Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7561604);
}
}}
}
if (gdjs.Palavras10Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "Ajuda");
}}

}


{

gdjs.Palavras10Code.GDbtRecarregarObjects1.createFrom(runtimeScene.getObjects("btRecarregar"));

gdjs.Palavras10Code.condition0IsTrue_0.val = false;
gdjs.Palavras10Code.condition1IsTrue_0.val = false;
gdjs.Palavras10Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras10Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras10Code.mapOfGDgdjs_46Palavras10Code_46GDbtRecarregarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras10Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras10Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras10Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras10Code.conditionTrue_1 = gdjs.Palavras10Code.condition2IsTrue_0;
gdjs.Palavras10Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7562476);
}
}}
}
if (gdjs.Palavras10Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.Palavras10Code.eventsList0xaff48


gdjs.Palavras10Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.Palavras10Code.GDletrasObjects1.length = 0;
gdjs.Palavras10Code.GDletrasObjects2.length = 0;
gdjs.Palavras10Code.GDletrasObjects3.length = 0;
gdjs.Palavras10Code.GDforcaObjects1.length = 0;
gdjs.Palavras10Code.GDforcaObjects2.length = 0;
gdjs.Palavras10Code.GDforcaObjects3.length = 0;
gdjs.Palavras10Code.GDbackgroundObjects1.length = 0;
gdjs.Palavras10Code.GDbackgroundObjects2.length = 0;
gdjs.Palavras10Code.GDbackgroundObjects3.length = 0;
gdjs.Palavras10Code.GDdicasObjects1.length = 0;
gdjs.Palavras10Code.GDdicasObjects2.length = 0;
gdjs.Palavras10Code.GDdicasObjects3.length = 0;
gdjs.Palavras10Code.GDtecladoObjects1.length = 0;
gdjs.Palavras10Code.GDtecladoObjects2.length = 0;
gdjs.Palavras10Code.GDtecladoObjects3.length = 0;
gdjs.Palavras10Code.GDbtAjudaObjects1.length = 0;
gdjs.Palavras10Code.GDbtAjudaObjects2.length = 0;
gdjs.Palavras10Code.GDbtAjudaObjects3.length = 0;
gdjs.Palavras10Code.GDbtPlayObjects1.length = 0;
gdjs.Palavras10Code.GDbtPlayObjects2.length = 0;
gdjs.Palavras10Code.GDbtPlayObjects3.length = 0;
gdjs.Palavras10Code.GDbtRecarregarObjects1.length = 0;
gdjs.Palavras10Code.GDbtRecarregarObjects2.length = 0;
gdjs.Palavras10Code.GDbtRecarregarObjects3.length = 0;
gdjs.Palavras10Code.GDbuttonDicasObjects1.length = 0;
gdjs.Palavras10Code.GDbuttonDicasObjects2.length = 0;
gdjs.Palavras10Code.GDbuttonDicasObjects3.length = 0;
gdjs.Palavras10Code.GDlblDicasObjects1.length = 0;
gdjs.Palavras10Code.GDlblDicasObjects2.length = 0;
gdjs.Palavras10Code.GDlblDicasObjects3.length = 0;
gdjs.Palavras10Code.GDlabel3Objects1.length = 0;
gdjs.Palavras10Code.GDlabel3Objects2.length = 0;
gdjs.Palavras10Code.GDlabel3Objects3.length = 0;
gdjs.Palavras10Code.GDlabel1Objects1.length = 0;
gdjs.Palavras10Code.GDlabel1Objects2.length = 0;
gdjs.Palavras10Code.GDlabel1Objects3.length = 0;
gdjs.Palavras10Code.GDpalavra1Objects1.length = 0;
gdjs.Palavras10Code.GDpalavra1Objects2.length = 0;
gdjs.Palavras10Code.GDpalavra1Objects3.length = 0;
gdjs.Palavras10Code.GDpalavra2Objects1.length = 0;
gdjs.Palavras10Code.GDpalavra2Objects2.length = 0;
gdjs.Palavras10Code.GDpalavra2Objects3.length = 0;
gdjs.Palavras10Code.GDpalavra3Objects1.length = 0;
gdjs.Palavras10Code.GDpalavra3Objects2.length = 0;
gdjs.Palavras10Code.GDpalavra3Objects3.length = 0;
gdjs.Palavras10Code.GDdica1Objects1.length = 0;
gdjs.Palavras10Code.GDdica1Objects2.length = 0;
gdjs.Palavras10Code.GDdica1Objects3.length = 0;
gdjs.Palavras10Code.GDdica2Objects1.length = 0;
gdjs.Palavras10Code.GDdica2Objects2.length = 0;
gdjs.Palavras10Code.GDdica2Objects3.length = 0;
gdjs.Palavras10Code.GDdica3Objects1.length = 0;
gdjs.Palavras10Code.GDdica3Objects2.length = 0;
gdjs.Palavras10Code.GDdica3Objects3.length = 0;

gdjs.Palavras10Code.eventsList0xaff48(runtimeScene);
return;
}
gdjs['Palavras10Code'] = gdjs.Palavras10Code;
