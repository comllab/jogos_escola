gdjs.StartCode = {};
gdjs.StartCode.GDletrasObjects1= [];
gdjs.StartCode.GDletrasObjects2= [];
gdjs.StartCode.GDforcaObjects1= [];
gdjs.StartCode.GDforcaObjects2= [];
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDdicasObjects1= [];
gdjs.StartCode.GDdicasObjects2= [];
gdjs.StartCode.GDtecladoObjects1= [];
gdjs.StartCode.GDtecladoObjects2= [];
gdjs.StartCode.GDbtAjudaObjects1= [];
gdjs.StartCode.GDbtAjudaObjects2= [];
gdjs.StartCode.GDbtPlayObjects1= [];
gdjs.StartCode.GDbtPlayObjects2= [];
gdjs.StartCode.GDbtRecarregarObjects1= [];
gdjs.StartCode.GDbtRecarregarObjects2= [];
gdjs.StartCode.GDbuttonDicasObjects1= [];
gdjs.StartCode.GDbuttonDicasObjects2= [];
gdjs.StartCode.GDlblDicasObjects1= [];
gdjs.StartCode.GDlblDicasObjects2= [];
gdjs.StartCode.GDlabel3Objects1= [];
gdjs.StartCode.GDlabel3Objects2= [];
gdjs.StartCode.GDlabel1Objects1= [];
gdjs.StartCode.GDlabel1Objects2= [];
gdjs.StartCode.GDNewObjectObjects1= [];
gdjs.StartCode.GDNewObjectObjects2= [];
gdjs.StartCode.GDNewObject2Objects1= [];
gdjs.StartCode.GDNewObject2Objects2= [];
gdjs.StartCode.GDNewObject3Objects1= [];
gdjs.StartCode.GDNewObject3Objects2= [];
gdjs.StartCode.GDlblJogarObjects1= [];
gdjs.StartCode.GDlblJogarObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};
gdjs.StartCode.condition3IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtPlayObjects1Objects = Hashtable.newFrom({"btPlay": gdjs.StartCode.GDbtPlayObjects1});gdjs.StartCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "menu_music_1.ogg", true, 100, 1);
}}

}


{

gdjs.StartCode.GDbtPlayObjects1.createFrom(runtimeScene.getObjects("btPlay"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtPlayObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7347876);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xaff48


gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.StartCode.GDletrasObjects1.length = 0;
gdjs.StartCode.GDletrasObjects2.length = 0;
gdjs.StartCode.GDforcaObjects1.length = 0;
gdjs.StartCode.GDforcaObjects2.length = 0;
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDdicasObjects1.length = 0;
gdjs.StartCode.GDdicasObjects2.length = 0;
gdjs.StartCode.GDtecladoObjects1.length = 0;
gdjs.StartCode.GDtecladoObjects2.length = 0;
gdjs.StartCode.GDbtAjudaObjects1.length = 0;
gdjs.StartCode.GDbtAjudaObjects2.length = 0;
gdjs.StartCode.GDbtPlayObjects1.length = 0;
gdjs.StartCode.GDbtPlayObjects2.length = 0;
gdjs.StartCode.GDbtRecarregarObjects1.length = 0;
gdjs.StartCode.GDbtRecarregarObjects2.length = 0;
gdjs.StartCode.GDbuttonDicasObjects1.length = 0;
gdjs.StartCode.GDbuttonDicasObjects2.length = 0;
gdjs.StartCode.GDlblDicasObjects1.length = 0;
gdjs.StartCode.GDlblDicasObjects2.length = 0;
gdjs.StartCode.GDlabel3Objects1.length = 0;
gdjs.StartCode.GDlabel3Objects2.length = 0;
gdjs.StartCode.GDlabel1Objects1.length = 0;
gdjs.StartCode.GDlabel1Objects2.length = 0;
gdjs.StartCode.GDNewObjectObjects1.length = 0;
gdjs.StartCode.GDNewObjectObjects2.length = 0;
gdjs.StartCode.GDNewObject2Objects1.length = 0;
gdjs.StartCode.GDNewObject2Objects2.length = 0;
gdjs.StartCode.GDNewObject3Objects1.length = 0;
gdjs.StartCode.GDNewObject3Objects2.length = 0;
gdjs.StartCode.GDlblJogarObjects1.length = 0;
gdjs.StartCode.GDlblJogarObjects2.length = 0;

gdjs.StartCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['StartCode'] = gdjs.StartCode;
