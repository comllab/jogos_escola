gdjs.Palavras14Code = {};
gdjs.Palavras14Code.forEachIndex2 = 0;

gdjs.Palavras14Code.forEachIndex3 = 0;

gdjs.Palavras14Code.forEachObjects2 = [];

gdjs.Palavras14Code.forEachObjects3 = [];

gdjs.Palavras14Code.forEachTemporary2 = null;

gdjs.Palavras14Code.forEachTemporary3 = null;

gdjs.Palavras14Code.forEachTotalCount2 = 0;

gdjs.Palavras14Code.forEachTotalCount3 = 0;

gdjs.Palavras14Code.GDletrasObjects1= [];
gdjs.Palavras14Code.GDletrasObjects2= [];
gdjs.Palavras14Code.GDletrasObjects3= [];
gdjs.Palavras14Code.GDforcaObjects1= [];
gdjs.Palavras14Code.GDforcaObjects2= [];
gdjs.Palavras14Code.GDforcaObjects3= [];
gdjs.Palavras14Code.GDbackgroundObjects1= [];
gdjs.Palavras14Code.GDbackgroundObjects2= [];
gdjs.Palavras14Code.GDbackgroundObjects3= [];
gdjs.Palavras14Code.GDdicasObjects1= [];
gdjs.Palavras14Code.GDdicasObjects2= [];
gdjs.Palavras14Code.GDdicasObjects3= [];
gdjs.Palavras14Code.GDtecladoObjects1= [];
gdjs.Palavras14Code.GDtecladoObjects2= [];
gdjs.Palavras14Code.GDtecladoObjects3= [];
gdjs.Palavras14Code.GDbtAjudaObjects1= [];
gdjs.Palavras14Code.GDbtAjudaObjects2= [];
gdjs.Palavras14Code.GDbtAjudaObjects3= [];
gdjs.Palavras14Code.GDbtPlayObjects1= [];
gdjs.Palavras14Code.GDbtPlayObjects2= [];
gdjs.Palavras14Code.GDbtPlayObjects3= [];
gdjs.Palavras14Code.GDbtRecarregarObjects1= [];
gdjs.Palavras14Code.GDbtRecarregarObjects2= [];
gdjs.Palavras14Code.GDbtRecarregarObjects3= [];
gdjs.Palavras14Code.GDbuttonDicasObjects1= [];
gdjs.Palavras14Code.GDbuttonDicasObjects2= [];
gdjs.Palavras14Code.GDbuttonDicasObjects3= [];
gdjs.Palavras14Code.GDlblDicasObjects1= [];
gdjs.Palavras14Code.GDlblDicasObjects2= [];
gdjs.Palavras14Code.GDlblDicasObjects3= [];
gdjs.Palavras14Code.GDlabel3Objects1= [];
gdjs.Palavras14Code.GDlabel3Objects2= [];
gdjs.Palavras14Code.GDlabel3Objects3= [];
gdjs.Palavras14Code.GDlabel1Objects1= [];
gdjs.Palavras14Code.GDlabel1Objects2= [];
gdjs.Palavras14Code.GDlabel1Objects3= [];
gdjs.Palavras14Code.GDpalavra1Objects1= [];
gdjs.Palavras14Code.GDpalavra1Objects2= [];
gdjs.Palavras14Code.GDpalavra1Objects3= [];
gdjs.Palavras14Code.GDpalavra2Objects1= [];
gdjs.Palavras14Code.GDpalavra2Objects2= [];
gdjs.Palavras14Code.GDpalavra2Objects3= [];
gdjs.Palavras14Code.GDpalavra3Objects1= [];
gdjs.Palavras14Code.GDpalavra3Objects2= [];
gdjs.Palavras14Code.GDpalavra3Objects3= [];
gdjs.Palavras14Code.GDdica1Objects1= [];
gdjs.Palavras14Code.GDdica1Objects2= [];
gdjs.Palavras14Code.GDdica1Objects3= [];
gdjs.Palavras14Code.GDdica2Objects1= [];
gdjs.Palavras14Code.GDdica2Objects2= [];
gdjs.Palavras14Code.GDdica2Objects3= [];
gdjs.Palavras14Code.GDdica3Objects1= [];
gdjs.Palavras14Code.GDdica3Objects2= [];
gdjs.Palavras14Code.GDdica3Objects3= [];

gdjs.Palavras14Code.conditionTrue_0 = {val:false};
gdjs.Palavras14Code.condition0IsTrue_0 = {val:false};
gdjs.Palavras14Code.condition1IsTrue_0 = {val:false};
gdjs.Palavras14Code.condition2IsTrue_0 = {val:false};
gdjs.Palavras14Code.condition3IsTrue_0 = {val:false};
gdjs.Palavras14Code.condition4IsTrue_0 = {val:false};
gdjs.Palavras14Code.conditionTrue_1 = {val:false};
gdjs.Palavras14Code.condition0IsTrue_1 = {val:false};
gdjs.Palavras14Code.condition1IsTrue_1 = {val:false};
gdjs.Palavras14Code.condition2IsTrue_1 = {val:false};
gdjs.Palavras14Code.condition3IsTrue_1 = {val:false};
gdjs.Palavras14Code.condition4IsTrue_1 = {val:false};


gdjs.Palavras14Code.eventsList0x701528 = function(runtimeScene) {

{

gdjs.Palavras14Code.GDletrasObjects1.createFrom(runtimeScene.getObjects("letras"));

gdjs.Palavras14Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras14Code.GDletrasObjects1.length;i<l;++i) {
    if ( gdjs.Palavras14Code.GDletrasObjects1[i].getVariableString(gdjs.Palavras14Code.GDletrasObjects1[i].getVariables().getFromIndex(0)) == "#" ) {
        gdjs.Palavras14Code.condition0IsTrue_0.val = true;
        gdjs.Palavras14Code.GDletrasObjects1[k] = gdjs.Palavras14Code.GDletrasObjects1[i];
        ++k;
    }
}
gdjs.Palavras14Code.GDletrasObjects1.length = k;}if (gdjs.Palavras14Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Palavras14Code.GDletrasObjects1 */
{for(var i = 0, len = gdjs.Palavras14Code.GDletrasObjects1.length ;i < len;++i) {
    gdjs.Palavras14Code.GDletrasObjects1[i].hide();
}
}}

}


}; //End of gdjs.Palavras14Code.eventsList0x701528
gdjs.Palavras14Code.mapOfGDgdjs_46Palavras14Code_46GDtecladoObjects1Objects = Hashtable.newFrom({"teclado": gdjs.Palavras14Code.GDtecladoObjects1});gdjs.Palavras14Code.eventsList0x74de10 = function(runtimeScene) {

}; //End of gdjs.Palavras14Code.eventsList0x74de10
gdjs.Palavras14Code.eventsList0x74d9d0 = function(runtimeScene) {

{

gdjs.Palavras14Code.GDletrasObjects2.createFrom(runtimeScene.getObjects("letras"));

for(gdjs.Palavras14Code.forEachIndex3 = 0;gdjs.Palavras14Code.forEachIndex3 < gdjs.Palavras14Code.GDletrasObjects2.length;++gdjs.Palavras14Code.forEachIndex3) {
gdjs.Palavras14Code.GDletrasObjects3.createFrom(gdjs.Palavras14Code.GDletrasObjects2);

gdjs.Palavras14Code.GDtecladoObjects3.createFrom(gdjs.Palavras14Code.GDtecladoObjects1);


gdjs.Palavras14Code.forEachTemporary3 = gdjs.Palavras14Code.GDletrasObjects2[gdjs.Palavras14Code.forEachIndex3];
gdjs.Palavras14Code.GDletrasObjects3.length = 0;
gdjs.Palavras14Code.GDletrasObjects3.push(gdjs.Palavras14Code.forEachTemporary3);
gdjs.Palavras14Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras14Code.GDletrasObjects3.length;i<l;++i) {
    if ( gdjs.Palavras14Code.GDletrasObjects3[i].getVariableString(gdjs.Palavras14Code.GDletrasObjects3[i].getVariables().getFromIndex(0)) == (( gdjs.Palavras14Code.GDtecladoObjects3.length === 0 ) ? "" :gdjs.Palavras14Code.GDtecladoObjects3[0].getAnimationName()) ) {
        gdjs.Palavras14Code.condition0IsTrue_0.val = true;
        gdjs.Palavras14Code.GDletrasObjects3[k] = gdjs.Palavras14Code.GDletrasObjects3[i];
        ++k;
    }
}
gdjs.Palavras14Code.GDletrasObjects3.length = k;}if (gdjs.Palavras14Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Palavras14Code.GDletrasObjects3.length ;i < len;++i) {
    gdjs.Palavras14Code.GDletrasObjects3[i].setAnimationName((gdjs.RuntimeObject.getVariableString(gdjs.Palavras14Code.GDletrasObjects3[i].getVariables().getFromIndex(0))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}
}

}


{


gdjs.Palavras14Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras14Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0;
}if (gdjs.Palavras14Code.condition0IsTrue_0.val) {
gdjs.Palavras14Code.GDforcaObjects2.createFrom(runtimeScene.getObjects("forca"));
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras14Code.GDforcaObjects2.length ;i < len;++i) {
    gdjs.Palavras14Code.GDforcaObjects2[i].setAnimation((gdjs.Palavras14Code.GDforcaObjects2[i].getAnimation()) + 1);
}
}}

}


{


gdjs.Palavras14Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras14Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) > 0;
}if (gdjs.Palavras14Code.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise04.ogg", false, 100, 1);
}}

}


}; //End of gdjs.Palavras14Code.eventsList0x74d9d0
gdjs.Palavras14Code.mapOfGDgdjs_46Palavras14Code_46GDbuttonDicasObjects1Objects = Hashtable.newFrom({"buttonDicas": gdjs.Palavras14Code.GDbuttonDicasObjects1});gdjs.Palavras14Code.mapOfGDgdjs_46Palavras14Code_46GDdicasObjects1Objects = Hashtable.newFrom({"dicas": gdjs.Palavras14Code.GDdicasObjects1});gdjs.Palavras14Code.eventsList0x74c8c0 = function(runtimeScene) {

}; //End of gdjs.Palavras14Code.eventsList0x74c8c0
gdjs.Palavras14Code.mapOfGDgdjs_46Palavras14Code_46GDbtAjudaObjects1Objects = Hashtable.newFrom({"btAjuda": gdjs.Palavras14Code.GDbtAjudaObjects1});gdjs.Palavras14Code.mapOfGDgdjs_46Palavras14Code_46GDbtRecarregarObjects1Objects = Hashtable.newFrom({"btRecarregar": gdjs.Palavras14Code.GDbtRecarregarObjects1});gdjs.Palavras14Code.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.Palavras14Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras14Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Palavras14Code.condition0IsTrue_0.val) {
gdjs.Palavras14Code.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.Palavras14Code.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.Palavras14Code.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{gdjs.evtTools.camera.hideLayer(runtimeScene, "dicas");
}{for(var i = 0, len = gdjs.Palavras14Code.GDpalavra1Objects1.length ;i < len;++i) {
    gdjs.Palavras14Code.GDpalavra1Objects1[i].hide();
}
for(var i = 0, len = gdjs.Palavras14Code.GDpalavra2Objects1.length ;i < len;++i) {
    gdjs.Palavras14Code.GDpalavra2Objects1[i].hide();
}
for(var i = 0, len = gdjs.Palavras14Code.GDpalavra3Objects1.length ;i < len;++i) {
    gdjs.Palavras14Code.GDpalavra3Objects1[i].hide();
}
}
{ //Subevents
gdjs.Palavras14Code.eventsList0x701528(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.Palavras14Code.GDtecladoObjects1.createFrom(runtimeScene.getObjects("teclado"));

gdjs.Palavras14Code.condition0IsTrue_0.val = false;
gdjs.Palavras14Code.condition1IsTrue_0.val = false;
gdjs.Palavras14Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras14Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras14Code.mapOfGDgdjs_46Palavras14Code_46GDtecladoObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras14Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras14Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras14Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras14Code.conditionTrue_1 = gdjs.Palavras14Code.condition2IsTrue_0;
gdjs.Palavras14Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7658372);
}
}}
}
if (gdjs.Palavras14Code.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.sound.playSound(runtimeScene, "artista desconhecido - pepdown.ogg", false, 100, 1);
}
{ //Subevents
gdjs.Palavras14Code.eventsList0x74d9d0(runtimeScene);} //End of subevents
}

}


{

gdjs.Palavras14Code.GDbuttonDicasObjects1.createFrom(runtimeScene.getObjects("buttonDicas"));

gdjs.Palavras14Code.condition0IsTrue_0.val = false;
gdjs.Palavras14Code.condition1IsTrue_0.val = false;
gdjs.Palavras14Code.condition2IsTrue_0.val = false;
gdjs.Palavras14Code.condition3IsTrue_0.val = false;
{
gdjs.Palavras14Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras14Code.mapOfGDgdjs_46Palavras14Code_46GDbuttonDicasObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras14Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras14Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras14Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Palavras14Code.GDbuttonDicasObjects1.length;i<l;++i) {
    if ( gdjs.Palavras14Code.GDbuttonDicasObjects1[i].isVisible() ) {
        gdjs.Palavras14Code.condition2IsTrue_0.val = true;
        gdjs.Palavras14Code.GDbuttonDicasObjects1[k] = gdjs.Palavras14Code.GDbuttonDicasObjects1[i];
        ++k;
    }
}
gdjs.Palavras14Code.GDbuttonDicasObjects1.length = k;}if ( gdjs.Palavras14Code.condition2IsTrue_0.val ) {
{
{gdjs.Palavras14Code.conditionTrue_1 = gdjs.Palavras14Code.condition3IsTrue_0;
gdjs.Palavras14Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7661820);
}
}}
}
}
if (gdjs.Palavras14Code.condition3IsTrue_0.val) {
/* Reuse gdjs.Palavras14Code.GDbuttonDicasObjects1 */
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise04.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras14Code.GDbuttonDicasObjects1.length ;i < len;++i) {
    gdjs.Palavras14Code.GDbuttonDicasObjects1[i].hide();
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "dicas");
}}

}


{

gdjs.Palavras14Code.GDdicasObjects1.createFrom(runtimeScene.getObjects("dicas"));

gdjs.Palavras14Code.condition0IsTrue_0.val = false;
gdjs.Palavras14Code.condition1IsTrue_0.val = false;
{
gdjs.Palavras14Code.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "dicas");
}if ( gdjs.Palavras14Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras14Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras14Code.mapOfGDgdjs_46Palavras14Code_46GDdicasObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.Palavras14Code.condition1IsTrue_0.val) {
gdjs.Palavras14Code.GDbuttonDicasObjects1.createFrom(runtimeScene.getObjects("buttonDicas"));
{gdjs.evtTools.sound.playSound(runtimeScene, "artista desconhecido - pepdown.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras14Code.GDbuttonDicasObjects1.length ;i < len;++i) {
    gdjs.Palavras14Code.GDbuttonDicasObjects1[i].hide(false);
}
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "dicas");
}}

}


{



}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.Palavras14Code.GDletrasObjects1.createFrom(runtimeScene.getObjects("letras"));

for(gdjs.Palavras14Code.forEachIndex2 = 0;gdjs.Palavras14Code.forEachIndex2 < gdjs.Palavras14Code.GDletrasObjects1.length;++gdjs.Palavras14Code.forEachIndex2) {
gdjs.Palavras14Code.GDletrasObjects2.createFrom(gdjs.Palavras14Code.GDletrasObjects1);


gdjs.Palavras14Code.forEachTemporary2 = gdjs.Palavras14Code.GDletrasObjects1[gdjs.Palavras14Code.forEachIndex2];
gdjs.Palavras14Code.GDletrasObjects2.length = 0;
gdjs.Palavras14Code.GDletrasObjects2.push(gdjs.Palavras14Code.forEachTemporary2);
gdjs.Palavras14Code.condition0IsTrue_0.val = false;
gdjs.Palavras14Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras14Code.GDletrasObjects2.length;i<l;++i) {
    if ( gdjs.Palavras14Code.GDletrasObjects2[i].getVariableString(gdjs.Palavras14Code.GDletrasObjects2[i].getVariables().getFromIndex(0)) != "#" ) {
        gdjs.Palavras14Code.condition0IsTrue_0.val = true;
        gdjs.Palavras14Code.GDletrasObjects2[k] = gdjs.Palavras14Code.GDletrasObjects2[i];
        ++k;
    }
}
gdjs.Palavras14Code.GDletrasObjects2.length = k;}if ( gdjs.Palavras14Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Palavras14Code.GDletrasObjects2.length;i<l;++i) {
    if ( gdjs.Palavras14Code.GDletrasObjects2[i].getAnimation() == 0 ) {
        gdjs.Palavras14Code.condition1IsTrue_0.val = true;
        gdjs.Palavras14Code.GDletrasObjects2[k] = gdjs.Palavras14Code.GDletrasObjects2[i];
        ++k;
    }
}
gdjs.Palavras14Code.GDletrasObjects2.length = k;}}
if (gdjs.Palavras14Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}}
}

}


{


gdjs.Palavras14Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras14Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.Palavras14Code.condition0IsTrue_0.val) {
gdjs.Palavras14Code.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.Palavras14Code.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.Palavras14Code.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{runtimeScene.getGame().getVariables().getFromIndex(4).setString((( gdjs.Palavras14Code.GDpalavra1Objects1.length === 0 ) ? "" :gdjs.Palavras14Code.GDpalavra1Objects1[0].getString()));
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString((( gdjs.Palavras14Code.GDpalavra2Objects1.length === 0 ) ? "" :gdjs.Palavras14Code.GDpalavra2Objects1[0].getString()));
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString((( gdjs.Palavras14Code.GDpalavra3Objects1.length === 0 ) ? "" :gdjs.Palavras14Code.GDpalavra3Objects1[0].getString()));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "End", false);
}}

}


{

gdjs.Palavras14Code.GDforcaObjects1.createFrom(runtimeScene.getObjects("forca"));

gdjs.Palavras14Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras14Code.GDforcaObjects1.length;i<l;++i) {
    if ( gdjs.Palavras14Code.GDforcaObjects1[i].getAnimation() == 6 ) {
        gdjs.Palavras14Code.condition0IsTrue_0.val = true;
        gdjs.Palavras14Code.GDforcaObjects1[k] = gdjs.Palavras14Code.GDforcaObjects1[i];
        ++k;
    }
}
gdjs.Palavras14Code.GDforcaObjects1.length = k;}if (gdjs.Palavras14Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GameOver", false);
}}

}


{



}


{

gdjs.Palavras14Code.GDbtAjudaObjects1.createFrom(runtimeScene.getObjects("btAjuda"));

gdjs.Palavras14Code.condition0IsTrue_0.val = false;
gdjs.Palavras14Code.condition1IsTrue_0.val = false;
gdjs.Palavras14Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras14Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras14Code.mapOfGDgdjs_46Palavras14Code_46GDbtAjudaObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras14Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras14Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras14Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras14Code.conditionTrue_1 = gdjs.Palavras14Code.condition2IsTrue_0;
gdjs.Palavras14Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7656580);
}
}}
}
if (gdjs.Palavras14Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "Ajuda");
}}

}


{

gdjs.Palavras14Code.GDbtRecarregarObjects1.createFrom(runtimeScene.getObjects("btRecarregar"));

gdjs.Palavras14Code.condition0IsTrue_0.val = false;
gdjs.Palavras14Code.condition1IsTrue_0.val = false;
gdjs.Palavras14Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras14Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras14Code.mapOfGDgdjs_46Palavras14Code_46GDbtRecarregarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras14Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras14Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras14Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras14Code.conditionTrue_1 = gdjs.Palavras14Code.condition2IsTrue_0;
gdjs.Palavras14Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7657452);
}
}}
}
if (gdjs.Palavras14Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.Palavras14Code.eventsList0xaff48


gdjs.Palavras14Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.Palavras14Code.GDletrasObjects1.length = 0;
gdjs.Palavras14Code.GDletrasObjects2.length = 0;
gdjs.Palavras14Code.GDletrasObjects3.length = 0;
gdjs.Palavras14Code.GDforcaObjects1.length = 0;
gdjs.Palavras14Code.GDforcaObjects2.length = 0;
gdjs.Palavras14Code.GDforcaObjects3.length = 0;
gdjs.Palavras14Code.GDbackgroundObjects1.length = 0;
gdjs.Palavras14Code.GDbackgroundObjects2.length = 0;
gdjs.Palavras14Code.GDbackgroundObjects3.length = 0;
gdjs.Palavras14Code.GDdicasObjects1.length = 0;
gdjs.Palavras14Code.GDdicasObjects2.length = 0;
gdjs.Palavras14Code.GDdicasObjects3.length = 0;
gdjs.Palavras14Code.GDtecladoObjects1.length = 0;
gdjs.Palavras14Code.GDtecladoObjects2.length = 0;
gdjs.Palavras14Code.GDtecladoObjects3.length = 0;
gdjs.Palavras14Code.GDbtAjudaObjects1.length = 0;
gdjs.Palavras14Code.GDbtAjudaObjects2.length = 0;
gdjs.Palavras14Code.GDbtAjudaObjects3.length = 0;
gdjs.Palavras14Code.GDbtPlayObjects1.length = 0;
gdjs.Palavras14Code.GDbtPlayObjects2.length = 0;
gdjs.Palavras14Code.GDbtPlayObjects3.length = 0;
gdjs.Palavras14Code.GDbtRecarregarObjects1.length = 0;
gdjs.Palavras14Code.GDbtRecarregarObjects2.length = 0;
gdjs.Palavras14Code.GDbtRecarregarObjects3.length = 0;
gdjs.Palavras14Code.GDbuttonDicasObjects1.length = 0;
gdjs.Palavras14Code.GDbuttonDicasObjects2.length = 0;
gdjs.Palavras14Code.GDbuttonDicasObjects3.length = 0;
gdjs.Palavras14Code.GDlblDicasObjects1.length = 0;
gdjs.Palavras14Code.GDlblDicasObjects2.length = 0;
gdjs.Palavras14Code.GDlblDicasObjects3.length = 0;
gdjs.Palavras14Code.GDlabel3Objects1.length = 0;
gdjs.Palavras14Code.GDlabel3Objects2.length = 0;
gdjs.Palavras14Code.GDlabel3Objects3.length = 0;
gdjs.Palavras14Code.GDlabel1Objects1.length = 0;
gdjs.Palavras14Code.GDlabel1Objects2.length = 0;
gdjs.Palavras14Code.GDlabel1Objects3.length = 0;
gdjs.Palavras14Code.GDpalavra1Objects1.length = 0;
gdjs.Palavras14Code.GDpalavra1Objects2.length = 0;
gdjs.Palavras14Code.GDpalavra1Objects3.length = 0;
gdjs.Palavras14Code.GDpalavra2Objects1.length = 0;
gdjs.Palavras14Code.GDpalavra2Objects2.length = 0;
gdjs.Palavras14Code.GDpalavra2Objects3.length = 0;
gdjs.Palavras14Code.GDpalavra3Objects1.length = 0;
gdjs.Palavras14Code.GDpalavra3Objects2.length = 0;
gdjs.Palavras14Code.GDpalavra3Objects3.length = 0;
gdjs.Palavras14Code.GDdica1Objects1.length = 0;
gdjs.Palavras14Code.GDdica1Objects2.length = 0;
gdjs.Palavras14Code.GDdica1Objects3.length = 0;
gdjs.Palavras14Code.GDdica2Objects1.length = 0;
gdjs.Palavras14Code.GDdica2Objects2.length = 0;
gdjs.Palavras14Code.GDdica2Objects3.length = 0;
gdjs.Palavras14Code.GDdica3Objects1.length = 0;
gdjs.Palavras14Code.GDdica3Objects2.length = 0;
gdjs.Palavras14Code.GDdica3Objects3.length = 0;

gdjs.Palavras14Code.eventsList0xaff48(runtimeScene);
return;
}
gdjs['Palavras14Code'] = gdjs.Palavras14Code;
