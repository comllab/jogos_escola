gdjs.Palavras3Code = {};
gdjs.Palavras3Code.forEachIndex2 = 0;

gdjs.Palavras3Code.forEachIndex3 = 0;

gdjs.Palavras3Code.forEachObjects2 = [];

gdjs.Palavras3Code.forEachObjects3 = [];

gdjs.Palavras3Code.forEachTemporary2 = null;

gdjs.Palavras3Code.forEachTemporary3 = null;

gdjs.Palavras3Code.forEachTotalCount2 = 0;

gdjs.Palavras3Code.forEachTotalCount3 = 0;

gdjs.Palavras3Code.GDletrasObjects1= [];
gdjs.Palavras3Code.GDletrasObjects2= [];
gdjs.Palavras3Code.GDletrasObjects3= [];
gdjs.Palavras3Code.GDforcaObjects1= [];
gdjs.Palavras3Code.GDforcaObjects2= [];
gdjs.Palavras3Code.GDforcaObjects3= [];
gdjs.Palavras3Code.GDbackgroundObjects1= [];
gdjs.Palavras3Code.GDbackgroundObjects2= [];
gdjs.Palavras3Code.GDbackgroundObjects3= [];
gdjs.Palavras3Code.GDdicasObjects1= [];
gdjs.Palavras3Code.GDdicasObjects2= [];
gdjs.Palavras3Code.GDdicasObjects3= [];
gdjs.Palavras3Code.GDtecladoObjects1= [];
gdjs.Palavras3Code.GDtecladoObjects2= [];
gdjs.Palavras3Code.GDtecladoObjects3= [];
gdjs.Palavras3Code.GDbtAjudaObjects1= [];
gdjs.Palavras3Code.GDbtAjudaObjects2= [];
gdjs.Palavras3Code.GDbtAjudaObjects3= [];
gdjs.Palavras3Code.GDbtPlayObjects1= [];
gdjs.Palavras3Code.GDbtPlayObjects2= [];
gdjs.Palavras3Code.GDbtPlayObjects3= [];
gdjs.Palavras3Code.GDbtRecarregarObjects1= [];
gdjs.Palavras3Code.GDbtRecarregarObjects2= [];
gdjs.Palavras3Code.GDbtRecarregarObjects3= [];
gdjs.Palavras3Code.GDbuttonDicasObjects1= [];
gdjs.Palavras3Code.GDbuttonDicasObjects2= [];
gdjs.Palavras3Code.GDbuttonDicasObjects3= [];
gdjs.Palavras3Code.GDlblDicasObjects1= [];
gdjs.Palavras3Code.GDlblDicasObjects2= [];
gdjs.Palavras3Code.GDlblDicasObjects3= [];
gdjs.Palavras3Code.GDlabel3Objects1= [];
gdjs.Palavras3Code.GDlabel3Objects2= [];
gdjs.Palavras3Code.GDlabel3Objects3= [];
gdjs.Palavras3Code.GDlabel1Objects1= [];
gdjs.Palavras3Code.GDlabel1Objects2= [];
gdjs.Palavras3Code.GDlabel1Objects3= [];
gdjs.Palavras3Code.GDpalavra1Objects1= [];
gdjs.Palavras3Code.GDpalavra1Objects2= [];
gdjs.Palavras3Code.GDpalavra1Objects3= [];
gdjs.Palavras3Code.GDpalavra2Objects1= [];
gdjs.Palavras3Code.GDpalavra2Objects2= [];
gdjs.Palavras3Code.GDpalavra2Objects3= [];
gdjs.Palavras3Code.GDpalavra3Objects1= [];
gdjs.Palavras3Code.GDpalavra3Objects2= [];
gdjs.Palavras3Code.GDpalavra3Objects3= [];
gdjs.Palavras3Code.GDdica1Objects1= [];
gdjs.Palavras3Code.GDdica1Objects2= [];
gdjs.Palavras3Code.GDdica1Objects3= [];
gdjs.Palavras3Code.GDdica2Objects1= [];
gdjs.Palavras3Code.GDdica2Objects2= [];
gdjs.Palavras3Code.GDdica2Objects3= [];
gdjs.Palavras3Code.GDdica3Objects1= [];
gdjs.Palavras3Code.GDdica3Objects2= [];
gdjs.Palavras3Code.GDdica3Objects3= [];

gdjs.Palavras3Code.conditionTrue_0 = {val:false};
gdjs.Palavras3Code.condition0IsTrue_0 = {val:false};
gdjs.Palavras3Code.condition1IsTrue_0 = {val:false};
gdjs.Palavras3Code.condition2IsTrue_0 = {val:false};
gdjs.Palavras3Code.condition3IsTrue_0 = {val:false};
gdjs.Palavras3Code.condition4IsTrue_0 = {val:false};
gdjs.Palavras3Code.conditionTrue_1 = {val:false};
gdjs.Palavras3Code.condition0IsTrue_1 = {val:false};
gdjs.Palavras3Code.condition1IsTrue_1 = {val:false};
gdjs.Palavras3Code.condition2IsTrue_1 = {val:false};
gdjs.Palavras3Code.condition3IsTrue_1 = {val:false};
gdjs.Palavras3Code.condition4IsTrue_1 = {val:false};


gdjs.Palavras3Code.eventsList0x701528 = function(runtimeScene) {

{

gdjs.Palavras3Code.GDletrasObjects1.createFrom(runtimeScene.getObjects("letras"));

gdjs.Palavras3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras3Code.GDletrasObjects1.length;i<l;++i) {
    if ( gdjs.Palavras3Code.GDletrasObjects1[i].getVariableString(gdjs.Palavras3Code.GDletrasObjects1[i].getVariables().getFromIndex(0)) == "#" ) {
        gdjs.Palavras3Code.condition0IsTrue_0.val = true;
        gdjs.Palavras3Code.GDletrasObjects1[k] = gdjs.Palavras3Code.GDletrasObjects1[i];
        ++k;
    }
}
gdjs.Palavras3Code.GDletrasObjects1.length = k;}if (gdjs.Palavras3Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Palavras3Code.GDletrasObjects1 */
{for(var i = 0, len = gdjs.Palavras3Code.GDletrasObjects1.length ;i < len;++i) {
    gdjs.Palavras3Code.GDletrasObjects1[i].hide();
}
}}

}


}; //End of gdjs.Palavras3Code.eventsList0x701528
gdjs.Palavras3Code.mapOfGDgdjs_46Palavras3Code_46GDtecladoObjects1Objects = Hashtable.newFrom({"teclado": gdjs.Palavras3Code.GDtecladoObjects1});gdjs.Palavras3Code.eventsList0x714d08 = function(runtimeScene) {

}; //End of gdjs.Palavras3Code.eventsList0x714d08
gdjs.Palavras3Code.eventsList0x7148c8 = function(runtimeScene) {

{

gdjs.Palavras3Code.GDletrasObjects2.createFrom(runtimeScene.getObjects("letras"));

for(gdjs.Palavras3Code.forEachIndex3 = 0;gdjs.Palavras3Code.forEachIndex3 < gdjs.Palavras3Code.GDletrasObjects2.length;++gdjs.Palavras3Code.forEachIndex3) {
gdjs.Palavras3Code.GDletrasObjects3.createFrom(gdjs.Palavras3Code.GDletrasObjects2);

gdjs.Palavras3Code.GDtecladoObjects3.createFrom(gdjs.Palavras3Code.GDtecladoObjects1);


gdjs.Palavras3Code.forEachTemporary3 = gdjs.Palavras3Code.GDletrasObjects2[gdjs.Palavras3Code.forEachIndex3];
gdjs.Palavras3Code.GDletrasObjects3.length = 0;
gdjs.Palavras3Code.GDletrasObjects3.push(gdjs.Palavras3Code.forEachTemporary3);
gdjs.Palavras3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras3Code.GDletrasObjects3.length;i<l;++i) {
    if ( gdjs.Palavras3Code.GDletrasObjects3[i].getVariableString(gdjs.Palavras3Code.GDletrasObjects3[i].getVariables().getFromIndex(0)) == (( gdjs.Palavras3Code.GDtecladoObjects3.length === 0 ) ? "" :gdjs.Palavras3Code.GDtecladoObjects3[0].getAnimationName()) ) {
        gdjs.Palavras3Code.condition0IsTrue_0.val = true;
        gdjs.Palavras3Code.GDletrasObjects3[k] = gdjs.Palavras3Code.GDletrasObjects3[i];
        ++k;
    }
}
gdjs.Palavras3Code.GDletrasObjects3.length = k;}if (gdjs.Palavras3Code.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.Palavras3Code.GDletrasObjects3.length ;i < len;++i) {
    gdjs.Palavras3Code.GDletrasObjects3[i].setAnimationName((gdjs.RuntimeObject.getVariableString(gdjs.Palavras3Code.GDletrasObjects3[i].getVariables().getFromIndex(0))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).add(1);
}}
}

}


{


gdjs.Palavras3Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras3Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0;
}if (gdjs.Palavras3Code.condition0IsTrue_0.val) {
gdjs.Palavras3Code.GDforcaObjects2.createFrom(runtimeScene.getObjects("forca"));
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_SAX07.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras3Code.GDforcaObjects2.length ;i < len;++i) {
    gdjs.Palavras3Code.GDforcaObjects2[i].setAnimation((gdjs.Palavras3Code.GDforcaObjects2[i].getAnimation()) + 1);
}
}}

}


{


gdjs.Palavras3Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras3Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) > 0;
}if (gdjs.Palavras3Code.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise04.ogg", false, 100, 1);
}}

}


}; //End of gdjs.Palavras3Code.eventsList0x7148c8
gdjs.Palavras3Code.mapOfGDgdjs_46Palavras3Code_46GDbuttonDicasObjects1Objects = Hashtable.newFrom({"buttonDicas": gdjs.Palavras3Code.GDbuttonDicasObjects1});gdjs.Palavras3Code.mapOfGDgdjs_46Palavras3Code_46GDdicasObjects1Objects = Hashtable.newFrom({"dicas": gdjs.Palavras3Code.GDdicasObjects1});gdjs.Palavras3Code.eventsList0x7137b8 = function(runtimeScene) {

}; //End of gdjs.Palavras3Code.eventsList0x7137b8
gdjs.Palavras3Code.mapOfGDgdjs_46Palavras3Code_46GDbtAjudaObjects1Objects = Hashtable.newFrom({"btAjuda": gdjs.Palavras3Code.GDbtAjudaObjects1});gdjs.Palavras3Code.mapOfGDgdjs_46Palavras3Code_46GDbtRecarregarObjects1Objects = Hashtable.newFrom({"btRecarregar": gdjs.Palavras3Code.GDbtRecarregarObjects1});gdjs.Palavras3Code.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.Palavras3Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras3Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Palavras3Code.condition0IsTrue_0.val) {
gdjs.Palavras3Code.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.Palavras3Code.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.Palavras3Code.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{for(var i = 0, len = gdjs.Palavras3Code.GDpalavra1Objects1.length ;i < len;++i) {
    gdjs.Palavras3Code.GDpalavra1Objects1[i].hide();
}
for(var i = 0, len = gdjs.Palavras3Code.GDpalavra2Objects1.length ;i < len;++i) {
    gdjs.Palavras3Code.GDpalavra2Objects1[i].hide();
}
for(var i = 0, len = gdjs.Palavras3Code.GDpalavra3Objects1.length ;i < len;++i) {
    gdjs.Palavras3Code.GDpalavra3Objects1[i].hide();
}
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "dicas");
}
{ //Subevents
gdjs.Palavras3Code.eventsList0x701528(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.Palavras3Code.GDtecladoObjects1.createFrom(runtimeScene.getObjects("teclado"));

gdjs.Palavras3Code.condition0IsTrue_0.val = false;
gdjs.Palavras3Code.condition1IsTrue_0.val = false;
gdjs.Palavras3Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras3Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras3Code.mapOfGDgdjs_46Palavras3Code_46GDtecladoObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras3Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras3Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras3Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras3Code.conditionTrue_1 = gdjs.Palavras3Code.condition2IsTrue_0;
gdjs.Palavras3Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7424636);
}
}}
}
if (gdjs.Palavras3Code.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.sound.playSound(runtimeScene, "artista desconhecido - pepdown.ogg", false, 100, 1);
}
{ //Subevents
gdjs.Palavras3Code.eventsList0x7148c8(runtimeScene);} //End of subevents
}

}


{

gdjs.Palavras3Code.GDbuttonDicasObjects1.createFrom(runtimeScene.getObjects("buttonDicas"));

gdjs.Palavras3Code.condition0IsTrue_0.val = false;
gdjs.Palavras3Code.condition1IsTrue_0.val = false;
gdjs.Palavras3Code.condition2IsTrue_0.val = false;
gdjs.Palavras3Code.condition3IsTrue_0.val = false;
{
gdjs.Palavras3Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras3Code.mapOfGDgdjs_46Palavras3Code_46GDbuttonDicasObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras3Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras3Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras3Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Palavras3Code.GDbuttonDicasObjects1.length;i<l;++i) {
    if ( gdjs.Palavras3Code.GDbuttonDicasObjects1[i].isVisible() ) {
        gdjs.Palavras3Code.condition2IsTrue_0.val = true;
        gdjs.Palavras3Code.GDbuttonDicasObjects1[k] = gdjs.Palavras3Code.GDbuttonDicasObjects1[i];
        ++k;
    }
}
gdjs.Palavras3Code.GDbuttonDicasObjects1.length = k;}if ( gdjs.Palavras3Code.condition2IsTrue_0.val ) {
{
{gdjs.Palavras3Code.conditionTrue_1 = gdjs.Palavras3Code.condition3IsTrue_0;
gdjs.Palavras3Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7428084);
}
}}
}
}
if (gdjs.Palavras3Code.condition3IsTrue_0.val) {
/* Reuse gdjs.Palavras3Code.GDbuttonDicasObjects1 */
{gdjs.evtTools.sound.playSound(runtimeScene, "Rise04.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras3Code.GDbuttonDicasObjects1.length ;i < len;++i) {
    gdjs.Palavras3Code.GDbuttonDicasObjects1[i].hide();
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "dicas");
}}

}


{

gdjs.Palavras3Code.GDdicasObjects1.createFrom(runtimeScene.getObjects("dicas"));

gdjs.Palavras3Code.condition0IsTrue_0.val = false;
gdjs.Palavras3Code.condition1IsTrue_0.val = false;
{
gdjs.Palavras3Code.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "dicas");
}if ( gdjs.Palavras3Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras3Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras3Code.mapOfGDgdjs_46Palavras3Code_46GDdicasObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.Palavras3Code.condition1IsTrue_0.val) {
gdjs.Palavras3Code.GDbuttonDicasObjects1.createFrom(runtimeScene.getObjects("buttonDicas"));
{gdjs.evtTools.sound.playSound(runtimeScene, "artista desconhecido - pepdown.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.Palavras3Code.GDbuttonDicasObjects1.length ;i < len;++i) {
    gdjs.Palavras3Code.GDbuttonDicasObjects1[i].hide(false);
}
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "dicas");
}}

}


{



}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}}

}


{

gdjs.Palavras3Code.GDletrasObjects1.createFrom(runtimeScene.getObjects("letras"));

for(gdjs.Palavras3Code.forEachIndex2 = 0;gdjs.Palavras3Code.forEachIndex2 < gdjs.Palavras3Code.GDletrasObjects1.length;++gdjs.Palavras3Code.forEachIndex2) {
gdjs.Palavras3Code.GDletrasObjects2.createFrom(gdjs.Palavras3Code.GDletrasObjects1);


gdjs.Palavras3Code.forEachTemporary2 = gdjs.Palavras3Code.GDletrasObjects1[gdjs.Palavras3Code.forEachIndex2];
gdjs.Palavras3Code.GDletrasObjects2.length = 0;
gdjs.Palavras3Code.GDletrasObjects2.push(gdjs.Palavras3Code.forEachTemporary2);
gdjs.Palavras3Code.condition0IsTrue_0.val = false;
gdjs.Palavras3Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras3Code.GDletrasObjects2.length;i<l;++i) {
    if ( gdjs.Palavras3Code.GDletrasObjects2[i].getVariableString(gdjs.Palavras3Code.GDletrasObjects2[i].getVariables().getFromIndex(0)) != "#" ) {
        gdjs.Palavras3Code.condition0IsTrue_0.val = true;
        gdjs.Palavras3Code.GDletrasObjects2[k] = gdjs.Palavras3Code.GDletrasObjects2[i];
        ++k;
    }
}
gdjs.Palavras3Code.GDletrasObjects2.length = k;}if ( gdjs.Palavras3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Palavras3Code.GDletrasObjects2.length;i<l;++i) {
    if ( gdjs.Palavras3Code.GDletrasObjects2[i].getAnimation() == 0 ) {
        gdjs.Palavras3Code.condition1IsTrue_0.val = true;
        gdjs.Palavras3Code.GDletrasObjects2[k] = gdjs.Palavras3Code.GDletrasObjects2[i];
        ++k;
    }
}
gdjs.Palavras3Code.GDletrasObjects2.length = k;}}
if (gdjs.Palavras3Code.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}}
}

}


{


gdjs.Palavras3Code.condition0IsTrue_0.val = false;
{
gdjs.Palavras3Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.Palavras3Code.condition0IsTrue_0.val) {
gdjs.Palavras3Code.GDpalavra1Objects1.createFrom(runtimeScene.getObjects("palavra1"));
gdjs.Palavras3Code.GDpalavra2Objects1.createFrom(runtimeScene.getObjects("palavra2"));
gdjs.Palavras3Code.GDpalavra3Objects1.createFrom(runtimeScene.getObjects("palavra3"));
{runtimeScene.getGame().getVariables().getFromIndex(4).setString((( gdjs.Palavras3Code.GDpalavra1Objects1.length === 0 ) ? "" :gdjs.Palavras3Code.GDpalavra1Objects1[0].getString()));
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString((( gdjs.Palavras3Code.GDpalavra2Objects1.length === 0 ) ? "" :gdjs.Palavras3Code.GDpalavra2Objects1[0].getString()));
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString((( gdjs.Palavras3Code.GDpalavra3Objects1.length === 0 ) ? "" :gdjs.Palavras3Code.GDpalavra3Objects1[0].getString()));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "End", false);
}}

}


{

gdjs.Palavras3Code.GDforcaObjects1.createFrom(runtimeScene.getObjects("forca"));

gdjs.Palavras3Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Palavras3Code.GDforcaObjects1.length;i<l;++i) {
    if ( gdjs.Palavras3Code.GDforcaObjects1[i].getAnimation() == 6 ) {
        gdjs.Palavras3Code.condition0IsTrue_0.val = true;
        gdjs.Palavras3Code.GDforcaObjects1[k] = gdjs.Palavras3Code.GDforcaObjects1[i];
        ++k;
    }
}
gdjs.Palavras3Code.GDforcaObjects1.length = k;}if (gdjs.Palavras3Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GameOver", false);
}}

}


{



}


{

gdjs.Palavras3Code.GDbtAjudaObjects1.createFrom(runtimeScene.getObjects("btAjuda"));

gdjs.Palavras3Code.condition0IsTrue_0.val = false;
gdjs.Palavras3Code.condition1IsTrue_0.val = false;
gdjs.Palavras3Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras3Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras3Code.mapOfGDgdjs_46Palavras3Code_46GDbtAjudaObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras3Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras3Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras3Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras3Code.conditionTrue_1 = gdjs.Palavras3Code.condition2IsTrue_0;
gdjs.Palavras3Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7422844);
}
}}
}
if (gdjs.Palavras3Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "Ajuda");
}}

}


{

gdjs.Palavras3Code.GDbtRecarregarObjects1.createFrom(runtimeScene.getObjects("btRecarregar"));

gdjs.Palavras3Code.condition0IsTrue_0.val = false;
gdjs.Palavras3Code.condition1IsTrue_0.val = false;
gdjs.Palavras3Code.condition2IsTrue_0.val = false;
{
gdjs.Palavras3Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Palavras3Code.mapOfGDgdjs_46Palavras3Code_46GDbtRecarregarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.Palavras3Code.condition0IsTrue_0.val ) {
{
gdjs.Palavras3Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Palavras3Code.condition1IsTrue_0.val ) {
{
{gdjs.Palavras3Code.conditionTrue_1 = gdjs.Palavras3Code.condition2IsTrue_0;
gdjs.Palavras3Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7423716);
}
}}
}
if (gdjs.Palavras3Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.Palavras3Code.eventsList0xaff48


gdjs.Palavras3Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.Palavras3Code.GDletrasObjects1.length = 0;
gdjs.Palavras3Code.GDletrasObjects2.length = 0;
gdjs.Palavras3Code.GDletrasObjects3.length = 0;
gdjs.Palavras3Code.GDforcaObjects1.length = 0;
gdjs.Palavras3Code.GDforcaObjects2.length = 0;
gdjs.Palavras3Code.GDforcaObjects3.length = 0;
gdjs.Palavras3Code.GDbackgroundObjects1.length = 0;
gdjs.Palavras3Code.GDbackgroundObjects2.length = 0;
gdjs.Palavras3Code.GDbackgroundObjects3.length = 0;
gdjs.Palavras3Code.GDdicasObjects1.length = 0;
gdjs.Palavras3Code.GDdicasObjects2.length = 0;
gdjs.Palavras3Code.GDdicasObjects3.length = 0;
gdjs.Palavras3Code.GDtecladoObjects1.length = 0;
gdjs.Palavras3Code.GDtecladoObjects2.length = 0;
gdjs.Palavras3Code.GDtecladoObjects3.length = 0;
gdjs.Palavras3Code.GDbtAjudaObjects1.length = 0;
gdjs.Palavras3Code.GDbtAjudaObjects2.length = 0;
gdjs.Palavras3Code.GDbtAjudaObjects3.length = 0;
gdjs.Palavras3Code.GDbtPlayObjects1.length = 0;
gdjs.Palavras3Code.GDbtPlayObjects2.length = 0;
gdjs.Palavras3Code.GDbtPlayObjects3.length = 0;
gdjs.Palavras3Code.GDbtRecarregarObjects1.length = 0;
gdjs.Palavras3Code.GDbtRecarregarObjects2.length = 0;
gdjs.Palavras3Code.GDbtRecarregarObjects3.length = 0;
gdjs.Palavras3Code.GDbuttonDicasObjects1.length = 0;
gdjs.Palavras3Code.GDbuttonDicasObjects2.length = 0;
gdjs.Palavras3Code.GDbuttonDicasObjects3.length = 0;
gdjs.Palavras3Code.GDlblDicasObjects1.length = 0;
gdjs.Palavras3Code.GDlblDicasObjects2.length = 0;
gdjs.Palavras3Code.GDlblDicasObjects3.length = 0;
gdjs.Palavras3Code.GDlabel3Objects1.length = 0;
gdjs.Palavras3Code.GDlabel3Objects2.length = 0;
gdjs.Palavras3Code.GDlabel3Objects3.length = 0;
gdjs.Palavras3Code.GDlabel1Objects1.length = 0;
gdjs.Palavras3Code.GDlabel1Objects2.length = 0;
gdjs.Palavras3Code.GDlabel1Objects3.length = 0;
gdjs.Palavras3Code.GDpalavra1Objects1.length = 0;
gdjs.Palavras3Code.GDpalavra1Objects2.length = 0;
gdjs.Palavras3Code.GDpalavra1Objects3.length = 0;
gdjs.Palavras3Code.GDpalavra2Objects1.length = 0;
gdjs.Palavras3Code.GDpalavra2Objects2.length = 0;
gdjs.Palavras3Code.GDpalavra2Objects3.length = 0;
gdjs.Palavras3Code.GDpalavra3Objects1.length = 0;
gdjs.Palavras3Code.GDpalavra3Objects2.length = 0;
gdjs.Palavras3Code.GDpalavra3Objects3.length = 0;
gdjs.Palavras3Code.GDdica1Objects1.length = 0;
gdjs.Palavras3Code.GDdica1Objects2.length = 0;
gdjs.Palavras3Code.GDdica1Objects3.length = 0;
gdjs.Palavras3Code.GDdica2Objects1.length = 0;
gdjs.Palavras3Code.GDdica2Objects2.length = 0;
gdjs.Palavras3Code.GDdica2Objects3.length = 0;
gdjs.Palavras3Code.GDdica3Objects1.length = 0;
gdjs.Palavras3Code.GDdica3Objects2.length = 0;
gdjs.Palavras3Code.GDdica3Objects3.length = 0;

gdjs.Palavras3Code.eventsList0xaff48(runtimeScene);
return;
}
gdjs['Palavras3Code'] = gdjs.Palavras3Code;
