gdjs.StartCode = {};
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDautorObjects1= [];
gdjs.StartCode.GDautorObjects2= [];
gdjs.StartCode.GDsiteObjects1= [];
gdjs.StartCode.GDsiteObjects2= [];
gdjs.StartCode.GDboxObjects1= [];
gdjs.StartCode.GDboxObjects2= [];
gdjs.StartCode.GDJObjects1= [];
gdjs.StartCode.GDJObjects2= [];
gdjs.StartCode.GDOObjects1= [];
gdjs.StartCode.GDOObjects2= [];
gdjs.StartCode.GDGObjects1= [];
gdjs.StartCode.GDGObjects2= [];
gdjs.StartCode.GDDObjects1= [];
gdjs.StartCode.GDDObjects2= [];
gdjs.StartCode.GDAObjects1= [];
gdjs.StartCode.GDAObjects2= [];
gdjs.StartCode.GDLObjects1= [];
gdjs.StartCode.GDLObjects2= [];
gdjs.StartCode.GDFObjects1= [];
gdjs.StartCode.GDFObjects2= [];
gdjs.StartCode.GDBObjects1= [];
gdjs.StartCode.GDBObjects2= [];
gdjs.StartCode.GDEObjects1= [];
gdjs.StartCode.GDEObjects2= [];
gdjs.StartCode.GDTObjects1= [];
gdjs.StartCode.GDTObjects2= [];
gdjs.StartCode.GDtxtStartObjects1= [];
gdjs.StartCode.GDtxtStartObjects2= [];
gdjs.StartCode.GDstarButtonObjects1= [];
gdjs.StartCode.GDstarButtonObjects2= [];
gdjs.StartCode.GDbeeObjects1= [];
gdjs.StartCode.GDbeeObjects2= [];
gdjs.StartCode.GDflip_95leftObjects1= [];
gdjs.StartCode.GDflip_95leftObjects2= [];
gdjs.StartCode.GDfli_95rigthObjects1= [];
gdjs.StartCode.GDfli_95rigthObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDstarButtonObjects1Objects = Hashtable.newFrom({"starButton": gdjs.StartCode.GDstarButtonObjects1});gdjs.StartCode.eventsList0xa86e0 = function(runtimeScene, context) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
gdjs.StartCode.GDfli_95rigthObjects1.createFrom(runtimeScene.getObjects("fli_rigth"));
gdjs.StartCode.GDflip_95leftObjects1.createFrom(runtimeScene.getObjects("flip_left"));
{gdjs.evtTools.sound.playMusic(runtimeScene, "menu_music_1.ogg", true, 100, 1);
}{for(var i = 0, len = gdjs.StartCode.GDflip_95leftObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDflip_95leftObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.StartCode.GDfli_95rigthObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDfli_95rigthObjects1[i].setAnimation(1);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "bee.ogg", false, 80, 1);
}}

}


{

gdjs.StartCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));

gdjs.StartCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.StartCode.GDbeeObjects1[i].getVariableNumber(gdjs.StartCode.GDbeeObjects1[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDbeeObjects1[k] = gdjs.StartCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.StartCode.GDbeeObjects1.length = k;}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDbeeObjects1 */
{for(var i = 0, len = gdjs.StartCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDbeeObjects1[i].addForce(-90, 0, 0);
}
}}

}


{

gdjs.StartCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));

gdjs.StartCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.StartCode.GDbeeObjects1[i].getVariableNumber(gdjs.StartCode.GDbeeObjects1[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDbeeObjects1[k] = gdjs.StartCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.StartCode.GDbeeObjects1.length = k;}if (gdjs.StartCode.condition0IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDbeeObjects1 */
{for(var i = 0, len = gdjs.StartCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDbeeObjects1[i].addForce(90, 0, 0);
}
}}

}


{

gdjs.StartCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));
gdjs.StartCode.GDflip_95leftObjects1.createFrom(runtimeScene.getObjects("flip_left"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.StartCode.GDbeeObjects1[i].getX() <= (( gdjs.StartCode.GDflip_95leftObjects1.length === 0 ) ? 0 :gdjs.StartCode.GDflip_95leftObjects1[0].getPointX("")) ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDbeeObjects1[k] = gdjs.StartCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.StartCode.GDbeeObjects1.length = k;}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition1IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = context.triggerOnce(6237628);
}
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDbeeObjects1 */
{for(var i = 0, len = gdjs.StartCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDbeeObjects1[i].flipX(true);
}
}{for(var i = 0, len = gdjs.StartCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDbeeObjects1[i].returnVariable(gdjs.StartCode.GDbeeObjects1[i].getVariables().getFromIndex(0)).setNumber(0);
}
}}

}


{

gdjs.StartCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));
gdjs.StartCode.GDfli_95rigthObjects1.createFrom(runtimeScene.getObjects("fli_rigth"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.StartCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.StartCode.GDbeeObjects1[i].getX() >= (( gdjs.StartCode.GDfli_95rigthObjects1.length === 0 ) ? 0 :gdjs.StartCode.GDfli_95rigthObjects1[0].getPointX("")) ) {
        gdjs.StartCode.condition0IsTrue_0.val = true;
        gdjs.StartCode.GDbeeObjects1[k] = gdjs.StartCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.StartCode.GDbeeObjects1.length = k;}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition1IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = context.triggerOnce(6238436);
}
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
/* Reuse gdjs.StartCode.GDbeeObjects1 */
{for(var i = 0, len = gdjs.StartCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDbeeObjects1[i].flipX(false);
}
}{for(var i = 0, len = gdjs.StartCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.StartCode.GDbeeObjects1[i].returnVariable(gdjs.StartCode.GDbeeObjects1[i].getVariables().getFromIndex(0)).setNumber(1);
}
}}

}


{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.50, "zoo");
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "bee.ogg", false, 45, 1);
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "zoo");
}}

}


{

gdjs.StartCode.GDstarButtonObjects1.createFrom(runtimeScene.getObjects("starButton"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDstarButtonObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.StartCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xa86e0


gdjs.StartCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDautorObjects1.length = 0;
gdjs.StartCode.GDautorObjects2.length = 0;
gdjs.StartCode.GDsiteObjects1.length = 0;
gdjs.StartCode.GDsiteObjects2.length = 0;
gdjs.StartCode.GDboxObjects1.length = 0;
gdjs.StartCode.GDboxObjects2.length = 0;
gdjs.StartCode.GDJObjects1.length = 0;
gdjs.StartCode.GDJObjects2.length = 0;
gdjs.StartCode.GDOObjects1.length = 0;
gdjs.StartCode.GDOObjects2.length = 0;
gdjs.StartCode.GDGObjects1.length = 0;
gdjs.StartCode.GDGObjects2.length = 0;
gdjs.StartCode.GDDObjects1.length = 0;
gdjs.StartCode.GDDObjects2.length = 0;
gdjs.StartCode.GDAObjects1.length = 0;
gdjs.StartCode.GDAObjects2.length = 0;
gdjs.StartCode.GDLObjects1.length = 0;
gdjs.StartCode.GDLObjects2.length = 0;
gdjs.StartCode.GDFObjects1.length = 0;
gdjs.StartCode.GDFObjects2.length = 0;
gdjs.StartCode.GDBObjects1.length = 0;
gdjs.StartCode.GDBObjects2.length = 0;
gdjs.StartCode.GDEObjects1.length = 0;
gdjs.StartCode.GDEObjects2.length = 0;
gdjs.StartCode.GDTObjects1.length = 0;
gdjs.StartCode.GDTObjects2.length = 0;
gdjs.StartCode.GDtxtStartObjects1.length = 0;
gdjs.StartCode.GDtxtStartObjects2.length = 0;
gdjs.StartCode.GDstarButtonObjects1.length = 0;
gdjs.StartCode.GDstarButtonObjects2.length = 0;
gdjs.StartCode.GDbeeObjects1.length = 0;
gdjs.StartCode.GDbeeObjects2.length = 0;
gdjs.StartCode.GDflip_95leftObjects1.length = 0;
gdjs.StartCode.GDflip_95leftObjects2.length = 0;
gdjs.StartCode.GDfli_95rigthObjects1.length = 0;
gdjs.StartCode.GDfli_95rigthObjects2.length = 0;

gdjs.StartCode.eventsList0xa86e0(runtimeScene, context);return;
}
gdjs['StartCode']= gdjs.StartCode;
