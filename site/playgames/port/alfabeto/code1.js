gdjs.GameCode = {};
gdjs.GameCode.GDcaixa_95letrasObjects1= [];
gdjs.GameCode.GDcaixa_95letrasObjects2= [];
gdjs.GameCode.GDcaixa_95letrasObjects3= [];
gdjs.GameCode.GDbeeObjects1= [];
gdjs.GameCode.GDbeeObjects2= [];
gdjs.GameCode.GDbeeObjects3= [];
gdjs.GameCode.GDentregaObjects1= [];
gdjs.GameCode.GDentregaObjects2= [];
gdjs.GameCode.GDentregaObjects3= [];
gdjs.GameCode.GDDEBUGObjects1= [];
gdjs.GameCode.GDDEBUGObjects2= [];
gdjs.GameCode.GDDEBUGObjects3= [];
gdjs.GameCode.GDbackgroundObjects1= [];
gdjs.GameCode.GDbackgroundObjects2= [];
gdjs.GameCode.GDbackgroundObjects3= [];
gdjs.GameCode.GDerrosObjects1= [];
gdjs.GameCode.GDerrosObjects2= [];
gdjs.GameCode.GDerrosObjects3= [];
gdjs.GameCode.GDbee_95errObjects1= [];
gdjs.GameCode.GDbee_95errObjects2= [];
gdjs.GameCode.GDbee_95errObjects3= [];
gdjs.GameCode.GDautorObjects1= [];
gdjs.GameCode.GDautorObjects2= [];
gdjs.GameCode.GDautorObjects3= [];
gdjs.GameCode.GDstartObjects1= [];
gdjs.GameCode.GDstartObjects2= [];
gdjs.GameCode.GDstartObjects3= [];
gdjs.GameCode.GDrestartObjects1= [];
gdjs.GameCode.GDrestartObjects2= [];
gdjs.GameCode.GDrestartObjects3= [];
gdjs.GameCode.GDparabensObjects1= [];
gdjs.GameCode.GDparabensObjects2= [];
gdjs.GameCode.GDparabensObjects3= [];
gdjs.GameCode.GDconseguiuObjects1= [];
gdjs.GameCode.GDconseguiuObjects2= [];
gdjs.GameCode.GDconseguiuObjects3= [];
gdjs.GameCode.GDbom_95trabalhoObjects1= [];
gdjs.GameCode.GDbom_95trabalhoObjects2= [];
gdjs.GameCode.GDbom_95trabalhoObjects3= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.condition4IsTrue_0 = {val:false};
gdjs.GameCode.condition5IsTrue_0 = {val:false};
gdjs.GameCode.condition6IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};
gdjs.GameCode.condition4IsTrue_1 = {val:false};
gdjs.GameCode.condition5IsTrue_1 = {val:false};
gdjs.GameCode.condition6IsTrue_1 = {val:false};


gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbeeObjects1Objects = Hashtable.newFrom({"bee": gdjs.GameCode.GDbeeObjects1});gdjs.GameCode.eventsList0x5f5a80 = function(runtimeScene, context) {

{



}


{

gdjs.GameCode.GDbeeObjects2.createFrom(gdjs.GameCode.GDbeeObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects2[i].getVariableNumber(gdjs.GameCode.GDbeeObjects2[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects2[k] = gdjs.GameCode.GDbeeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbeeObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDbeeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects2[i].returnVariable(gdjs.GameCode.GDbeeObjects2[i].getVariables().getFromIndex(0)).setNumber((gdjs.random(13) + gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.GameCode.GDbeeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects2[i].setAnimationName((gdjs.RuntimeObject.getVariableString(gdjs.GameCode.GDbeeObjects2[i].getVariables().getFromIndex(0))));
}
}}

}


{

gdjs.GameCode.GDbeeObjects2.createFrom(gdjs.GameCode.GDbeeObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects2[i].getVariableNumber(gdjs.GameCode.GDbeeObjects2[i].getVariables().getFromIndex(0)) > 26 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects2[k] = gdjs.GameCode.GDbeeObjects2[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbeeObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDbeeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects2[i].returnVariable(gdjs.GameCode.GDbeeObjects2[i].getVariables().getFromIndex(0)).setNumber((gdjs.random(25) + 1));
}
}{for(var i = 0, len = gdjs.GameCode.GDbeeObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects2[i].setAnimationName((gdjs.RuntimeObject.getVariableString(gdjs.GameCode.GDbeeObjects2[i].getVariables().getFromIndex(0))));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 25, "letra_timer");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbeeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].returnVariable(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(0)).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].setAnimationName((gdjs.RuntimeObject.getVariableString(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(0))));
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "letra_timer");
}}

}


}; //End of gdjs.GameCode.eventsList0x5f5a80
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbeeObjects1Objects = Hashtable.newFrom({"bee": gdjs.GameCode.GDbeeObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbeeObjects1Objects = Hashtable.newFrom({"bee": gdjs.GameCode.GDbeeObjects1});gdjs.GameCode.eventsList0x5f90f0 = function(runtimeScene, context) {

{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 14;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDentregaObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDentregaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDentregaObjects1[i].setPosition(36,111);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x5f90f0
gdjs.GameCode.eventsList0x5f86e8 = function(runtimeScene, context) {

{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 26;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDrestartObjects2.createFrom(runtimeScene.getObjects("restart"));
gdjs.GameCode.GDstartObjects2.createFrom(runtimeScene.getObjects("start"));
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}{for(var i = 0, len = gdjs.GameCode.GDstartObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDstartObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.GameCode.GDrestartObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDrestartObjects2[i].hide(false);
}
}}

}


{



}


{

/* Reuse gdjs.GameCode.GDbeeObjects1 */
gdjs.GameCode.GDcaixa_95letrasObjects1.createFrom(runtimeScene.getObjects("caixa_letras"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(3)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcaixa_95letrasObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcaixa_95letrasObjects1[i].getVariableNumber(gdjs.GameCode.GDcaixa_95letrasObjects1[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDcaixa_95letrasObjects1[k] = gdjs.GameCode.GDcaixa_95letrasObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcaixa_95letrasObjects1.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcaixa_95letrasObjects1 */
/* Reuse gdjs.GameCode.GDentregaObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDcaixa_95letrasObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcaixa_95letrasObjects1[i].setAnimation(gdjs.evtTools.common.toNumber(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.GameCode.GDcaixa_95letrasObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcaixa_95letrasObjects1[i].setScale(0.94);
}
}{for(var i = 0, len = gdjs.GameCode.GDentregaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDentregaObjects1[i].setPosition((gdjs.GameCode.GDentregaObjects1[i].getPointX("")) + 60,(gdjs.GameCode.GDentregaObjects1[i].getPointY("")));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x5f90f0(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x5f86e8
gdjs.GameCode.eventsList0x5f84c0 = function(runtimeScene, context) {

{

/* Reuse gdjs.GameCode.GDbeeObjects1 */
/* Reuse gdjs.GameCode.GDentregaObjects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getY() < (( gdjs.GameCode.GDentregaObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDentregaObjects1[0].getPointY("")) - (gdjs.GameCode.GDbeeObjects1[i].getHeight())/2 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbeeObjects1 */
{gdjs.evtTools.sound.playSound(runtimeScene, "entrega2.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].returnVariable(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(2)).setNumber(0);
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].setAnimationName("out");
}
}{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].returnVariable(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(3)).setNumber(1);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x5f86e8(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x5f84c0
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbeeObjects1Objects = Hashtable.newFrom({"bee": gdjs.GameCode.GDbeeObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDstartObjects1Objects = Hashtable.newFrom({"start": gdjs.GameCode.GDstartObjects1});gdjs.GameCode.eventsList0xa86e0 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDentregaObjects1.createFrom(runtimeScene.getObjects("entrega"));
gdjs.GameCode.GDerrosObjects1.createFrom(runtimeScene.getObjects("erros"));
gdjs.GameCode.GDrestartObjects1.createFrom(runtimeScene.getObjects("restart"));
gdjs.GameCode.GDstartObjects1.createFrom(runtimeScene.getObjects("start"));
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}{gdjs.evtTools.sound.playMusic(runtimeScene, "Rezoner - Happy.ogg", true, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDentregaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDentregaObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.GameCode.GDerrosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDerrosObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)) + " ERRO(S)");
}
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{for(var i = 0, len = gdjs.GameCode.GDstartObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDstartObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.GameCode.GDrestartObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDrestartObjects1[i].hide();
}
}}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, gdjs.random(2) + 3, "spawn_bee");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.condition0IsTrue_1.val = false;
{
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0;
}gdjs.GameCode.conditionTrue_1.val = true && gdjs.GameCode.condition0IsTrue_1.val;
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
gdjs.GameCode.GDbeeObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbeeObjects1Objects, 778, gdjs.random(250) + 200, "");
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "spawn_bee");
}
{ //Subevents
gdjs.GameCode.eventsList0x5f5a80(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
gdjs.GameCode.condition4IsTrue_0.val = false;
gdjs.GameCode.condition5IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbeeObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().get("ID")) == (gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().get("ID"))) ) {
        gdjs.GameCode.condition3IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}if ( gdjs.GameCode.condition3IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(0)) != gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition4IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}if ( gdjs.GameCode.condition4IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition5IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(6254460);
}
}}
}
}
}
}
if (gdjs.GameCode.condition5IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbeeObjects1 */
gdjs.GameCode.GDerrosObjects1.createFrom(runtimeScene.getObjects("erros"));
{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].returnVariable(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(1)).setNumber(1);
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(1);
}{for(var i = 0, len = gdjs.GameCode.GDerrosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDerrosObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)) + " ERRO(S)");
}
}}

}


{



}


{

gdjs.GameCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
gdjs.GameCode.condition4IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbeeObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().get("ID")) == (gdjs.RuntimeObject.getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().get("ID"))) ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) ) {
        gdjs.GameCode.condition3IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}if ( gdjs.GameCode.condition3IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition4IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(6256316);
}
}}
}
}
}
if (gdjs.GameCode.condition4IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbeeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].returnVariable(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(2)).setNumber(1);
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(1);
}}

}


{

gdjs.GameCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbeeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].returnVariable(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(1)).setNumber(1);
}
}}

}


{



}


{

gdjs.GameCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(1)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.condition0IsTrue_1.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_1.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}gdjs.GameCode.conditionTrue_1.val = true && gdjs.GameCode.condition0IsTrue_1.val;
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbeeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].addForce(-1*(gdjs.random(30) + 30), 0, 0);
}
}}

}


{



}


{

gdjs.GameCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(1)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(2)) == 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(3)) == 0 ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbeeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].addForce(-60, 90, 0);
}
}}

}


{



}


{

gdjs.GameCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(2)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbeeObjects1 */
gdjs.GameCode.GDentregaObjects1.createFrom(runtimeScene.getObjects("entrega"));
{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].addForceTowardPosition((( gdjs.GameCode.GDentregaObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDentregaObjects1[0].getPointX("")), (( gdjs.GameCode.GDentregaObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDentregaObjects1[0].getPointY("")), 100, 0);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x5f84c0(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbeeObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbeeObjects1[i].getVariableNumber(gdjs.GameCode.GDbeeObjects1[i].getVariables().getFromIndex(3)) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbeeObjects1[k] = gdjs.GameCode.GDbeeObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbeeObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbeeObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbeeObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbeeObjects1[i].addForce(-1, -100, 0);
}
}}

}


{



}


{

gdjs.GameCode.GDbeeObjects1.createFrom(runtimeScene.getObjects("bee"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.50, "zoo");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbeeObjects1Objects) > 0;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "bee.ogg", false, 80, 1);
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "zoo");
}}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbom_95trabalhoObjects1.createFrom(runtimeScene.getObjects("bom_trabalho"));
gdjs.GameCode.GDconseguiuObjects1.createFrom(runtimeScene.getObjects("conseguiu"));
gdjs.GameCode.GDparabensObjects1.createFrom(runtimeScene.getObjects("parabens"));
{for(var i = 0, len = gdjs.GameCode.GDparabensObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDparabensObjects1[i].addForce(0, -30, 0);
}
}{for(var i = 0, len = gdjs.GameCode.GDconseguiuObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDconseguiuObjects1[i].addForce(0, -30, 0);
}
}{for(var i = 0, len = gdjs.GameCode.GDbom_95trabalhoObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbom_95trabalhoObjects1[i].addForce(0, -30, 0);
}
}}

}


{

gdjs.GameCode.GDstartObjects1.createFrom(runtimeScene.getObjects("start"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDstartObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDstartObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDstartObjects1[i].isVisible() ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDstartObjects1[k] = gdjs.GameCode.GDstartObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDstartObjects1.length = k;}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


}; //End of gdjs.GameCode.eventsList0xa86e0


gdjs.GameCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GameCode.GDcaixa_95letrasObjects1.length = 0;
gdjs.GameCode.GDcaixa_95letrasObjects2.length = 0;
gdjs.GameCode.GDcaixa_95letrasObjects3.length = 0;
gdjs.GameCode.GDbeeObjects1.length = 0;
gdjs.GameCode.GDbeeObjects2.length = 0;
gdjs.GameCode.GDbeeObjects3.length = 0;
gdjs.GameCode.GDentregaObjects1.length = 0;
gdjs.GameCode.GDentregaObjects2.length = 0;
gdjs.GameCode.GDentregaObjects3.length = 0;
gdjs.GameCode.GDDEBUGObjects1.length = 0;
gdjs.GameCode.GDDEBUGObjects2.length = 0;
gdjs.GameCode.GDDEBUGObjects3.length = 0;
gdjs.GameCode.GDbackgroundObjects1.length = 0;
gdjs.GameCode.GDbackgroundObjects2.length = 0;
gdjs.GameCode.GDbackgroundObjects3.length = 0;
gdjs.GameCode.GDerrosObjects1.length = 0;
gdjs.GameCode.GDerrosObjects2.length = 0;
gdjs.GameCode.GDerrosObjects3.length = 0;
gdjs.GameCode.GDbee_95errObjects1.length = 0;
gdjs.GameCode.GDbee_95errObjects2.length = 0;
gdjs.GameCode.GDbee_95errObjects3.length = 0;
gdjs.GameCode.GDautorObjects1.length = 0;
gdjs.GameCode.GDautorObjects2.length = 0;
gdjs.GameCode.GDautorObjects3.length = 0;
gdjs.GameCode.GDstartObjects1.length = 0;
gdjs.GameCode.GDstartObjects2.length = 0;
gdjs.GameCode.GDstartObjects3.length = 0;
gdjs.GameCode.GDrestartObjects1.length = 0;
gdjs.GameCode.GDrestartObjects2.length = 0;
gdjs.GameCode.GDrestartObjects3.length = 0;
gdjs.GameCode.GDparabensObjects1.length = 0;
gdjs.GameCode.GDparabensObjects2.length = 0;
gdjs.GameCode.GDparabensObjects3.length = 0;
gdjs.GameCode.GDconseguiuObjects1.length = 0;
gdjs.GameCode.GDconseguiuObjects2.length = 0;
gdjs.GameCode.GDconseguiuObjects3.length = 0;
gdjs.GameCode.GDbom_95trabalhoObjects1.length = 0;
gdjs.GameCode.GDbom_95trabalhoObjects2.length = 0;
gdjs.GameCode.GDbom_95trabalhoObjects3.length = 0;

gdjs.GameCode.eventsList0xa86e0(runtimeScene, context);return;
}
gdjs['GameCode']= gdjs.GameCode;
