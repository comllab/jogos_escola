gdjs.InfoCode = {};
gdjs.InfoCode.GDplaneObjects1= [];
gdjs.InfoCode.GDplaneObjects2= [];
gdjs.InfoCode.GDbackground1Objects1= [];
gdjs.InfoCode.GDbackground1Objects2= [];
gdjs.InfoCode.GDkeyboardObjects1= [];
gdjs.InfoCode.GDkeyboardObjects2= [];
gdjs.InfoCode.GDtabletObjects1= [];
gdjs.InfoCode.GDtabletObjects2= [];
gdjs.InfoCode.GDtapObjects1= [];
gdjs.InfoCode.GDtapObjects2= [];
gdjs.InfoCode.GDinfoObjects1= [];
gdjs.InfoCode.GDinfoObjects2= [];
gdjs.InfoCode.GDbtnStartObjects1= [];
gdjs.InfoCode.GDbtnStartObjects2= [];
gdjs.InfoCode.GDtxtStartObjects1= [];
gdjs.InfoCode.GDtxtStartObjects2= [];
gdjs.InfoCode.GDsiteObjects1= [];
gdjs.InfoCode.GDsiteObjects2= [];
gdjs.InfoCode.GDNewObjectObjects1= [];
gdjs.InfoCode.GDNewObjectObjects2= [];

gdjs.InfoCode.conditionTrue_0 = {val:false};
gdjs.InfoCode.condition0IsTrue_0 = {val:false};
gdjs.InfoCode.condition1IsTrue_0 = {val:false};
gdjs.InfoCode.condition2IsTrue_0 = {val:false};


gdjs.InfoCode.mapOfGDgdjs_46InfoCode_46GDbtnStartObjects1Objects = Hashtable.newFrom({"btnStart": gdjs.InfoCode.GDbtnStartObjects1});gdjs.InfoCode.eventsList0xac5d0 = function(runtimeScene, context) {

{


gdjs.InfoCode.condition0IsTrue_0.val = false;
{
gdjs.InfoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.InfoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "plane.ogg", 1, true, 15, 1);
}}

}


{

gdjs.InfoCode.GDbtnStartObjects1.createFrom(runtimeScene.getObjects("btnStart"));

gdjs.InfoCode.condition0IsTrue_0.val = false;
gdjs.InfoCode.condition1IsTrue_0.val = false;
{
gdjs.InfoCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.InfoCode.condition0IsTrue_0.val ) {
{
gdjs.InfoCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.InfoCode.mapOfGDgdjs_46InfoCode_46GDbtnStartObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.InfoCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GamePlane", false);
}}

}


{


gdjs.InfoCode.condition0IsTrue_0.val = false;
{
gdjs.InfoCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
}if (gdjs.InfoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GamePlane", false);
}}

}


}; //End of gdjs.InfoCode.eventsList0xac5d0


gdjs.InfoCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.InfoCode.GDplaneObjects1.length = 0;
gdjs.InfoCode.GDplaneObjects2.length = 0;
gdjs.InfoCode.GDbackground1Objects1.length = 0;
gdjs.InfoCode.GDbackground1Objects2.length = 0;
gdjs.InfoCode.GDkeyboardObjects1.length = 0;
gdjs.InfoCode.GDkeyboardObjects2.length = 0;
gdjs.InfoCode.GDtabletObjects1.length = 0;
gdjs.InfoCode.GDtabletObjects2.length = 0;
gdjs.InfoCode.GDtapObjects1.length = 0;
gdjs.InfoCode.GDtapObjects2.length = 0;
gdjs.InfoCode.GDinfoObjects1.length = 0;
gdjs.InfoCode.GDinfoObjects2.length = 0;
gdjs.InfoCode.GDbtnStartObjects1.length = 0;
gdjs.InfoCode.GDbtnStartObjects2.length = 0;
gdjs.InfoCode.GDtxtStartObjects1.length = 0;
gdjs.InfoCode.GDtxtStartObjects2.length = 0;
gdjs.InfoCode.GDsiteObjects1.length = 0;
gdjs.InfoCode.GDsiteObjects2.length = 0;
gdjs.InfoCode.GDNewObjectObjects1.length = 0;
gdjs.InfoCode.GDNewObjectObjects2.length = 0;

gdjs.InfoCode.eventsList0xac5d0(runtimeScene, context);return;
}
gdjs['InfoCode']= gdjs.InfoCode;
