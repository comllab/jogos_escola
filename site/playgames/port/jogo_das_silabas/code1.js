gdjs.GameCode = {};
gdjs.GameCode.GDplayerObjects1= [];
gdjs.GameCode.GDplayerObjects2= [];
gdjs.GameCode.GDplayerObjects3= [];
gdjs.GameCode.GDskyObjects1= [];
gdjs.GameCode.GDskyObjects2= [];
gdjs.GameCode.GDskyObjects3= [];
gdjs.GameCode.GDbaObjects1= [];
gdjs.GameCode.GDbaObjects2= [];
gdjs.GameCode.GDbaObjects3= [];
gdjs.GameCode.GDcaObjects1= [];
gdjs.GameCode.GDcaObjects2= [];
gdjs.GameCode.GDcaObjects3= [];
gdjs.GameCode.GDdaObjects1= [];
gdjs.GameCode.GDdaObjects2= [];
gdjs.GameCode.GDdaObjects3= [];
gdjs.GameCode.GDfaObjects1= [];
gdjs.GameCode.GDfaObjects2= [];
gdjs.GameCode.GDfaObjects3= [];
gdjs.GameCode.GDgaObjects1= [];
gdjs.GameCode.GDgaObjects2= [];
gdjs.GameCode.GDgaObjects3= [];
gdjs.GameCode.GDhaObjects1= [];
gdjs.GameCode.GDhaObjects2= [];
gdjs.GameCode.GDhaObjects3= [];
gdjs.GameCode.GDjaObjects1= [];
gdjs.GameCode.GDjaObjects2= [];
gdjs.GameCode.GDjaObjects3= [];
gdjs.GameCode.GDlaObjects1= [];
gdjs.GameCode.GDlaObjects2= [];
gdjs.GameCode.GDlaObjects3= [];
gdjs.GameCode.GDmaObjects1= [];
gdjs.GameCode.GDmaObjects2= [];
gdjs.GameCode.GDmaObjects3= [];
gdjs.GameCode.GDnaObjects1= [];
gdjs.GameCode.GDnaObjects2= [];
gdjs.GameCode.GDnaObjects3= [];
gdjs.GameCode.GDpaObjects1= [];
gdjs.GameCode.GDpaObjects2= [];
gdjs.GameCode.GDpaObjects3= [];
gdjs.GameCode.GDquaObjects1= [];
gdjs.GameCode.GDquaObjects2= [];
gdjs.GameCode.GDquaObjects3= [];
gdjs.GameCode.GDraObjects1= [];
gdjs.GameCode.GDraObjects2= [];
gdjs.GameCode.GDraObjects3= [];
gdjs.GameCode.GDsaObjects1= [];
gdjs.GameCode.GDsaObjects2= [];
gdjs.GameCode.GDsaObjects3= [];
gdjs.GameCode.GDtaObjects1= [];
gdjs.GameCode.GDtaObjects2= [];
gdjs.GameCode.GDtaObjects3= [];
gdjs.GameCode.GDvaObjects1= [];
gdjs.GameCode.GDvaObjects2= [];
gdjs.GameCode.GDvaObjects3= [];
gdjs.GameCode.GDxaObjects1= [];
gdjs.GameCode.GDxaObjects2= [];
gdjs.GameCode.GDxaObjects3= [];
gdjs.GameCode.GDzaObjects1= [];
gdjs.GameCode.GDzaObjects2= [];
gdjs.GameCode.GDzaObjects3= [];
gdjs.GameCode.GDsilaba1Objects1= [];
gdjs.GameCode.GDsilaba1Objects2= [];
gdjs.GameCode.GDsilaba1Objects3= [];
gdjs.GameCode.GDsilaba2Objects1= [];
gdjs.GameCode.GDsilaba2Objects2= [];
gdjs.GameCode.GDsilaba2Objects3= [];
gdjs.GameCode.GDsilaba3Objects1= [];
gdjs.GameCode.GDsilaba3Objects2= [];
gdjs.GameCode.GDsilaba3Objects3= [];
gdjs.GameCode.GDsilaba4Objects1= [];
gdjs.GameCode.GDsilaba4Objects2= [];
gdjs.GameCode.GDsilaba4Objects3= [];
gdjs.GameCode.GDsilaba5Objects1= [];
gdjs.GameCode.GDsilaba5Objects2= [];
gdjs.GameCode.GDsilaba5Objects3= [];
gdjs.GameCode.GDsilaba6Objects1= [];
gdjs.GameCode.GDsilaba6Objects2= [];
gdjs.GameCode.GDsilaba6Objects3= [];
gdjs.GameCode.GDcolisor_95playerObjects1= [];
gdjs.GameCode.GDcolisor_95playerObjects2= [];
gdjs.GameCode.GDcolisor_95playerObjects3= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};


gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbaObjects2Objects = Hashtable.newFrom({"ba": gdjs.GameCode.GDbaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcaObjects2Objects = Hashtable.newFrom({"ca": gdjs.GameCode.GDcaObjects2});gdjs.GameCode.eventsList0x6ff2f0 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("BA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("BE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("BI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("BO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("BU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("BÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x6ff2f0
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcaObjects2Objects = Hashtable.newFrom({"ca": gdjs.GameCode.GDcaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDdaObjects2Objects = Hashtable.newFrom({"da": gdjs.GameCode.GDdaObjects2});gdjs.GameCode.eventsList0x6ffd20 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("CA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("CE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("CI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("CO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("CU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("CÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x6ffd20
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDdaObjects2Objects = Hashtable.newFrom({"da": gdjs.GameCode.GDdaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDfaObjects2Objects = Hashtable.newFrom({"fa": gdjs.GameCode.GDfaObjects2});gdjs.GameCode.eventsList0x700740 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("DA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("DE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("DI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("DO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("DU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("DÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x700740
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDfaObjects2Objects = Hashtable.newFrom({"fa": gdjs.GameCode.GDfaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDgaObjects2Objects = Hashtable.newFrom({"ga": gdjs.GameCode.GDgaObjects2});gdjs.GameCode.eventsList0x701148 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("FA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("FE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("FI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("FO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("FU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("FÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x701148
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDgaObjects2Objects = Hashtable.newFrom({"ga": gdjs.GameCode.GDgaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDhaObjects2Objects = Hashtable.newFrom({"ha": gdjs.GameCode.GDhaObjects2});gdjs.GameCode.eventsList0x701b60 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("GA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("GE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("GI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("GO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("GU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("GÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x701b60
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDhaObjects2Objects = Hashtable.newFrom({"ha": gdjs.GameCode.GDhaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDjaObjects2Objects = Hashtable.newFrom({"ja": gdjs.GameCode.GDjaObjects2});gdjs.GameCode.eventsList0x7025b0 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("HA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("HE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("HI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("HO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("HU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("HÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x7025b0
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDjaObjects2Objects = Hashtable.newFrom({"ja": gdjs.GameCode.GDjaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlaObjects2Objects = Hashtable.newFrom({"la": gdjs.GameCode.GDlaObjects2});gdjs.GameCode.eventsList0x702f90 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("JA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("JE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("JI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("JO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("JU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("JÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x702f90
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlaObjects2Objects = Hashtable.newFrom({"la": gdjs.GameCode.GDlaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmaObjects2Objects = Hashtable.newFrom({"ma": gdjs.GameCode.GDmaObjects2});gdjs.GameCode.eventsList0x703998 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("LA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("LE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("LI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("LO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("LU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("LÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x703998
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmaObjects2Objects = Hashtable.newFrom({"ma": gdjs.GameCode.GDmaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDnaObjects2Objects = Hashtable.newFrom({"na": gdjs.GameCode.GDnaObjects2});gdjs.GameCode.eventsList0x7043a0 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("MA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("ME");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("MI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("MO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("MU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("MÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x7043a0
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDnaObjects2Objects = Hashtable.newFrom({"na": gdjs.GameCode.GDnaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpaObjects2Objects = Hashtable.newFrom({"pa": gdjs.GameCode.GDpaObjects2});gdjs.GameCode.eventsList0x704e30 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("NA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("NE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("NI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("NO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("NU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("NÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x704e30
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpaObjects2Objects = Hashtable.newFrom({"pa": gdjs.GameCode.GDpaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquaObjects2Objects = Hashtable.newFrom({"qua": gdjs.GameCode.GDquaObjects2});gdjs.GameCode.eventsList0x705820 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("PA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("PE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("PI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("PO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("PU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("PÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x705820
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquaObjects2Objects = Hashtable.newFrom({"qua": gdjs.GameCode.GDquaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDraObjects2Objects = Hashtable.newFrom({"ra": gdjs.GameCode.GDraObjects2});gdjs.GameCode.eventsList0x706258 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("QUA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("QUE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("QUI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("QUO");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("QUÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x706258
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDraObjects2Objects = Hashtable.newFrom({"ra": gdjs.GameCode.GDraObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDsaObjects2Objects = Hashtable.newFrom({"sa": gdjs.GameCode.GDsaObjects2});gdjs.GameCode.eventsList0x706c90 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("RA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("RE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("RI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("RO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("RU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("RÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x706c90
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDsaObjects2Objects = Hashtable.newFrom({"sa": gdjs.GameCode.GDsaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDtaObjects2Objects = Hashtable.newFrom({"ta": gdjs.GameCode.GDtaObjects2});gdjs.GameCode.eventsList0x7076c8 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("SA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("SE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("SI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("SO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("SU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("SÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x7076c8
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDtaObjects2Objects = Hashtable.newFrom({"ta": gdjs.GameCode.GDtaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDvaObjects2Objects = Hashtable.newFrom({"va": gdjs.GameCode.GDvaObjects2});gdjs.GameCode.eventsList0x708100 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("TA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("TE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("TI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("TO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("TU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("TÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x708100
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDvaObjects2Objects = Hashtable.newFrom({"va": gdjs.GameCode.GDvaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDxaObjects2Objects = Hashtable.newFrom({"xa": gdjs.GameCode.GDxaObjects2});gdjs.GameCode.eventsList0x708b38 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("VA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("VE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("VI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("VO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("VU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("VÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x708b38
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDxaObjects2Objects = Hashtable.newFrom({"xa": gdjs.GameCode.GDxaObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDzaObjects2Objects = Hashtable.newFrom({"za": gdjs.GameCode.GDzaObjects2});gdjs.GameCode.eventsList0x709570 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("XA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("XE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("XI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("XO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("XU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("XÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x709570
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDzaObjects1Objects = Hashtable.newFrom({"za": gdjs.GameCode.GDzaObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDxaObjects1Objects = Hashtable.newFrom({"xa": gdjs.GameCode.GDxaObjects1});gdjs.GameCode.eventsList0x709fa8 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("silaba1")) == "*";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("ZA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("ZE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("ZI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("ZO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("ZU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("ZÃO");
}}

}


}; //End of gdjs.GameCode.eventsList0x709fa8
gdjs.GameCode.eventsList0x6ff120 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "BA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDbaObjects2.length = 0;

gdjs.GameCode.GDcaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x6ff2f0(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "CA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDcaObjects2.length = 0;

gdjs.GameCode.GDdaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDdaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x6ffd20(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "DA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDdaObjects2.length = 0;

gdjs.GameCode.GDfaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDdaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDfaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x700740(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "FA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDfaObjects2.length = 0;

gdjs.GameCode.GDgaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDfaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDgaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x701148(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "GA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDgaObjects2.length = 0;

gdjs.GameCode.GDhaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDgaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDhaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x701b60(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "HA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDhaObjects2.length = 0;

gdjs.GameCode.GDjaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDhaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDjaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x7025b0(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "JA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDjaObjects2.length = 0;

gdjs.GameCode.GDlaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDjaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x702f90(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "LA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDlaObjects2.length = 0;

gdjs.GameCode.GDmaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x703998(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "MA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDmaObjects2.length = 0;

gdjs.GameCode.GDnaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDmaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDnaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x7043a0(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "NA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDnaObjects2.length = 0;

gdjs.GameCode.GDpaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDnaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x704e30(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "PA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDpaObjects2.length = 0;

gdjs.GameCode.GDquaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDpaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x705820(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "QUA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDquaObjects2.length = 0;

gdjs.GameCode.GDraObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDquaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDraObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x706258(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "RA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDraObjects2.length = 0;

gdjs.GameCode.GDsaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDraObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDsaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x706c90(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "SA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDsaObjects2.length = 0;

gdjs.GameCode.GDtaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDsaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDtaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x7076c8(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "TA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDtaObjects2.length = 0;

gdjs.GameCode.GDvaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDtaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDvaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x708100(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "VA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDvaObjects2.length = 0;

gdjs.GameCode.GDxaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDvaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDxaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x708b38(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "XA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDxaObjects2.length = 0;

gdjs.GameCode.GDzaObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDxaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDzaObjects2Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x709570(runtimeScene, context);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "ZA";
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDxaObjects1.length = 0;

gdjs.GameCode.GDzaObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDzaObjects1Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, -70, "silabas");
}{gdjs.evtTools.object.createObjectOnScene(runtimeScene, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDxaObjects1Objects, gdjs.random(gdjs.evtTools.window.getCanvasWidth(runtimeScene) - 128) + 64, gdjs.random(70) *-1 - 150, "silabas");
}{for(var i = 0, len = gdjs.GameCode.GDzaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects1[i].setAnimation(gdjs.random(5));
}
}{for(var i = 0, len = gdjs.GameCode.GDxaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects1[i].setAnimation(gdjs.random(5));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x709fa8(runtimeScene, context);} //End of subevents
}

}


}; //End of gdjs.GameCode.eventsList0x6ff120
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbaObjects1ObjectsGDgdjs_46GameCode_46GDcaObjects1ObjectsGDgdjs_46GameCode_46GDdaObjects1ObjectsGDgdjs_46GameCode_46GDfaObjects1ObjectsGDgdjs_46GameCode_46GDgaObjects1ObjectsGDgdjs_46GameCode_46GDhaObjects1ObjectsGDgdjs_46GameCode_46GDjaObjects1ObjectsGDgdjs_46GameCode_46GDlaObjects1ObjectsGDgdjs_46GameCode_46GDmaObjects1ObjectsGDgdjs_46GameCode_46GDnaObjects1ObjectsGDgdjs_46GameCode_46GDpaObjects1ObjectsGDgdjs_46GameCode_46GDquaObjects1ObjectsGDgdjs_46GameCode_46GDraObjects1ObjectsGDgdjs_46GameCode_46GDsaObjects1ObjectsGDgdjs_46GameCode_46GDtaObjects1ObjectsGDgdjs_46GameCode_46GDvaObjects1ObjectsGDgdjs_46GameCode_46GDxaObjects1ObjectsGDgdjs_46GameCode_46GDzaObjects1Objects = Hashtable.newFrom({"ba": gdjs.GameCode.GDbaObjects1, "ca": gdjs.GameCode.GDcaObjects1, "da": gdjs.GameCode.GDdaObjects1, "fa": gdjs.GameCode.GDfaObjects1, "ga": gdjs.GameCode.GDgaObjects1, "ha": gdjs.GameCode.GDhaObjects1, "ja": gdjs.GameCode.GDjaObjects1, "la": gdjs.GameCode.GDlaObjects1, "ma": gdjs.GameCode.GDmaObjects1, "na": gdjs.GameCode.GDnaObjects1, "pa": gdjs.GameCode.GDpaObjects1, "qua": gdjs.GameCode.GDquaObjects1, "ra": gdjs.GameCode.GDraObjects1, "sa": gdjs.GameCode.GDsaObjects1, "ta": gdjs.GameCode.GDtaObjects1, "va": gdjs.GameCode.GDvaObjects1, "xa": gdjs.GameCode.GDxaObjects1, "za": gdjs.GameCode.GDzaObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcolisor_9595playerObjects1Objects = Hashtable.newFrom({"colisor_player": gdjs.GameCode.GDcolisor_95playerObjects1});gdjs.GameCode.eventsList0x6f7a88 = function(runtimeScene, context) {

{

gdjs.GameCode.GDbaObjects2.createFrom(gdjs.GameCode.GDbaObjects1);

gdjs.GameCode.GDcaObjects2.createFrom(gdjs.GameCode.GDcaObjects1);

gdjs.GameCode.GDdaObjects2.createFrom(gdjs.GameCode.GDdaObjects1);

gdjs.GameCode.GDfaObjects2.createFrom(gdjs.GameCode.GDfaObjects1);

gdjs.GameCode.GDgaObjects2.createFrom(gdjs.GameCode.GDgaObjects1);

gdjs.GameCode.GDhaObjects2.createFrom(gdjs.GameCode.GDhaObjects1);

gdjs.GameCode.GDjaObjects2.createFrom(gdjs.GameCode.GDjaObjects1);

gdjs.GameCode.GDlaObjects2.createFrom(gdjs.GameCode.GDlaObjects1);

gdjs.GameCode.GDmaObjects2.createFrom(gdjs.GameCode.GDmaObjects1);

gdjs.GameCode.GDnaObjects2.createFrom(gdjs.GameCode.GDnaObjects1);

gdjs.GameCode.GDpaObjects2.createFrom(gdjs.GameCode.GDpaObjects1);

gdjs.GameCode.GDquaObjects2.createFrom(gdjs.GameCode.GDquaObjects1);

gdjs.GameCode.GDraObjects2.createFrom(gdjs.GameCode.GDraObjects1);

gdjs.GameCode.GDsaObjects2.createFrom(gdjs.GameCode.GDsaObjects1);

gdjs.GameCode.GDtaObjects2.createFrom(gdjs.GameCode.GDtaObjects1);

gdjs.GameCode.GDvaObjects2.createFrom(gdjs.GameCode.GDvaObjects1);

gdjs.GameCode.GDxaObjects2.createFrom(gdjs.GameCode.GDxaObjects1);

gdjs.GameCode.GDzaObjects2.createFrom(gdjs.GameCode.GDzaObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)) == (( gdjs.GameCode.GDzaObjects2.length === 0 ) ? (( gdjs.GameCode.GDxaObjects2.length === 0 ) ? (( gdjs.GameCode.GDvaObjects2.length === 0 ) ? (( gdjs.GameCode.GDtaObjects2.length === 0 ) ? (( gdjs.GameCode.GDsaObjects2.length === 0 ) ? (( gdjs.GameCode.GDraObjects2.length === 0 ) ? (( gdjs.GameCode.GDquaObjects2.length === 0 ) ? (( gdjs.GameCode.GDpaObjects2.length === 0 ) ? (( gdjs.GameCode.GDnaObjects2.length === 0 ) ? (( gdjs.GameCode.GDmaObjects2.length === 0 ) ? (( gdjs.GameCode.GDlaObjects2.length === 0 ) ? (( gdjs.GameCode.GDjaObjects2.length === 0 ) ? (( gdjs.GameCode.GDhaObjects2.length === 0 ) ? (( gdjs.GameCode.GDgaObjects2.length === 0 ) ? (( gdjs.GameCode.GDfaObjects2.length === 0 ) ? (( gdjs.GameCode.GDdaObjects2.length === 0 ) ? (( gdjs.GameCode.GDcaObjects2.length === 0 ) ? (( gdjs.GameCode.GDbaObjects2.length === 0 ) ? "" :gdjs.GameCode.GDbaObjects2[0].getAnimationName()) :gdjs.GameCode.GDcaObjects2[0].getAnimationName()) :gdjs.GameCode.GDdaObjects2[0].getAnimationName()) :gdjs.GameCode.GDfaObjects2[0].getAnimationName()) :gdjs.GameCode.GDgaObjects2[0].getAnimationName()) :gdjs.GameCode.GDhaObjects2[0].getAnimationName()) :gdjs.GameCode.GDjaObjects2[0].getAnimationName()) :gdjs.GameCode.GDlaObjects2[0].getAnimationName()) :gdjs.GameCode.GDmaObjects2[0].getAnimationName()) :gdjs.GameCode.GDnaObjects2[0].getAnimationName()) :gdjs.GameCode.GDpaObjects2[0].getAnimationName()) :gdjs.GameCode.GDquaObjects2[0].getAnimationName()) :gdjs.GameCode.GDraObjects2[0].getAnimationName()) :gdjs.GameCode.GDsaObjects2[0].getAnimationName()) :gdjs.GameCode.GDtaObjects2[0].getAnimationName()) :gdjs.GameCode.GDvaObjects2[0].getAnimationName()) :gdjs.GameCode.GDxaObjects2[0].getAnimationName()) :gdjs.GameCode.GDzaObjects2[0].getAnimationName());
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(7306980);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbaObjects2 */
/* Reuse gdjs.GameCode.GDcaObjects2 */
/* Reuse gdjs.GameCode.GDdaObjects2 */
/* Reuse gdjs.GameCode.GDfaObjects2 */
/* Reuse gdjs.GameCode.GDgaObjects2 */
/* Reuse gdjs.GameCode.GDhaObjects2 */
/* Reuse gdjs.GameCode.GDjaObjects2 */
/* Reuse gdjs.GameCode.GDlaObjects2 */
/* Reuse gdjs.GameCode.GDmaObjects2 */
/* Reuse gdjs.GameCode.GDnaObjects2 */
/* Reuse gdjs.GameCode.GDpaObjects2 */
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

/* Reuse gdjs.GameCode.GDquaObjects2 */
/* Reuse gdjs.GameCode.GDraObjects2 */
/* Reuse gdjs.GameCode.GDsaObjects2 */
gdjs.GameCode.GDsilaba1Objects2.createFrom(runtimeScene.getObjects("silaba1"));
/* Reuse gdjs.GameCode.GDtaObjects2 */
/* Reuse gdjs.GameCode.GDvaObjects2 */
/* Reuse gdjs.GameCode.GDxaObjects2 */
/* Reuse gdjs.GameCode.GDzaObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].returnVariable(gdjs.GameCode.GDbaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].returnVariable(gdjs.GameCode.GDcaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].returnVariable(gdjs.GameCode.GDdaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].returnVariable(gdjs.GameCode.GDfaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].returnVariable(gdjs.GameCode.GDgaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].returnVariable(gdjs.GameCode.GDhaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].returnVariable(gdjs.GameCode.GDjaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].returnVariable(gdjs.GameCode.GDlaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].returnVariable(gdjs.GameCode.GDmaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].returnVariable(gdjs.GameCode.GDnaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].returnVariable(gdjs.GameCode.GDpaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].returnVariable(gdjs.GameCode.GDquaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].returnVariable(gdjs.GameCode.GDraObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].returnVariable(gdjs.GameCode.GDsaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].returnVariable(gdjs.GameCode.GDtaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].returnVariable(gdjs.GameCode.GDvaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].returnVariable(gdjs.GameCode.GDxaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].returnVariable(gdjs.GameCode.GDzaObjects2[i].getVariables().get("cair")).setNumber(0);
}
}{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba1Objects2[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.GameCode.GDsilaba1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDsilaba1Objects2[i].hide();
}
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise03.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].returnVariable(gdjs.GameCode.GDplayerObjects2[i].getVariables().getFromIndex(0)).setNumber(1);
}
}}

}


{

gdjs.GameCode.GDbaObjects2.createFrom(gdjs.GameCode.GDbaObjects1);

gdjs.GameCode.GDcaObjects2.createFrom(gdjs.GameCode.GDcaObjects1);

gdjs.GameCode.GDdaObjects2.createFrom(gdjs.GameCode.GDdaObjects1);

gdjs.GameCode.GDfaObjects2.createFrom(gdjs.GameCode.GDfaObjects1);

gdjs.GameCode.GDgaObjects2.createFrom(gdjs.GameCode.GDgaObjects1);

gdjs.GameCode.GDhaObjects2.createFrom(gdjs.GameCode.GDhaObjects1);

gdjs.GameCode.GDjaObjects2.createFrom(gdjs.GameCode.GDjaObjects1);

gdjs.GameCode.GDlaObjects2.createFrom(gdjs.GameCode.GDlaObjects1);

gdjs.GameCode.GDmaObjects2.createFrom(gdjs.GameCode.GDmaObjects1);

gdjs.GameCode.GDnaObjects2.createFrom(gdjs.GameCode.GDnaObjects1);

gdjs.GameCode.GDpaObjects2.createFrom(gdjs.GameCode.GDpaObjects1);

gdjs.GameCode.GDquaObjects2.createFrom(gdjs.GameCode.GDquaObjects1);

gdjs.GameCode.GDraObjects2.createFrom(gdjs.GameCode.GDraObjects1);

gdjs.GameCode.GDsaObjects2.createFrom(gdjs.GameCode.GDsaObjects1);

gdjs.GameCode.GDtaObjects2.createFrom(gdjs.GameCode.GDtaObjects1);

gdjs.GameCode.GDvaObjects2.createFrom(gdjs.GameCode.GDvaObjects1);

gdjs.GameCode.GDxaObjects2.createFrom(gdjs.GameCode.GDxaObjects1);

gdjs.GameCode.GDzaObjects2.createFrom(gdjs.GameCode.GDzaObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)) == (( gdjs.GameCode.GDzaObjects2.length === 0 ) ? (( gdjs.GameCode.GDxaObjects2.length === 0 ) ? (( gdjs.GameCode.GDvaObjects2.length === 0 ) ? (( gdjs.GameCode.GDtaObjects2.length === 0 ) ? (( gdjs.GameCode.GDsaObjects2.length === 0 ) ? (( gdjs.GameCode.GDraObjects2.length === 0 ) ? (( gdjs.GameCode.GDquaObjects2.length === 0 ) ? (( gdjs.GameCode.GDpaObjects2.length === 0 ) ? (( gdjs.GameCode.GDnaObjects2.length === 0 ) ? (( gdjs.GameCode.GDmaObjects2.length === 0 ) ? (( gdjs.GameCode.GDlaObjects2.length === 0 ) ? (( gdjs.GameCode.GDjaObjects2.length === 0 ) ? (( gdjs.GameCode.GDhaObjects2.length === 0 ) ? (( gdjs.GameCode.GDgaObjects2.length === 0 ) ? (( gdjs.GameCode.GDfaObjects2.length === 0 ) ? (( gdjs.GameCode.GDdaObjects2.length === 0 ) ? (( gdjs.GameCode.GDcaObjects2.length === 0 ) ? (( gdjs.GameCode.GDbaObjects2.length === 0 ) ? "" :gdjs.GameCode.GDbaObjects2[0].getAnimationName()) :gdjs.GameCode.GDcaObjects2[0].getAnimationName()) :gdjs.GameCode.GDdaObjects2[0].getAnimationName()) :gdjs.GameCode.GDfaObjects2[0].getAnimationName()) :gdjs.GameCode.GDgaObjects2[0].getAnimationName()) :gdjs.GameCode.GDhaObjects2[0].getAnimationName()) :gdjs.GameCode.GDjaObjects2[0].getAnimationName()) :gdjs.GameCode.GDlaObjects2[0].getAnimationName()) :gdjs.GameCode.GDmaObjects2[0].getAnimationName()) :gdjs.GameCode.GDnaObjects2[0].getAnimationName()) :gdjs.GameCode.GDpaObjects2[0].getAnimationName()) :gdjs.GameCode.GDquaObjects2[0].getAnimationName()) :gdjs.GameCode.GDraObjects2[0].getAnimationName()) :gdjs.GameCode.GDsaObjects2[0].getAnimationName()) :gdjs.GameCode.GDtaObjects2[0].getAnimationName()) :gdjs.GameCode.GDvaObjects2[0].getAnimationName()) :gdjs.GameCode.GDxaObjects2[0].getAnimationName()) :gdjs.GameCode.GDzaObjects2[0].getAnimationName());
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(7308780);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbaObjects2 */
/* Reuse gdjs.GameCode.GDcaObjects2 */
/* Reuse gdjs.GameCode.GDdaObjects2 */
/* Reuse gdjs.GameCode.GDfaObjects2 */
/* Reuse gdjs.GameCode.GDgaObjects2 */
/* Reuse gdjs.GameCode.GDhaObjects2 */
/* Reuse gdjs.GameCode.GDjaObjects2 */
/* Reuse gdjs.GameCode.GDlaObjects2 */
/* Reuse gdjs.GameCode.GDmaObjects2 */
/* Reuse gdjs.GameCode.GDnaObjects2 */
/* Reuse gdjs.GameCode.GDpaObjects2 */
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

/* Reuse gdjs.GameCode.GDquaObjects2 */
/* Reuse gdjs.GameCode.GDraObjects2 */
/* Reuse gdjs.GameCode.GDsaObjects2 */
gdjs.GameCode.GDsilaba2Objects2.createFrom(runtimeScene.getObjects("silaba2"));
/* Reuse gdjs.GameCode.GDtaObjects2 */
/* Reuse gdjs.GameCode.GDvaObjects2 */
/* Reuse gdjs.GameCode.GDxaObjects2 */
/* Reuse gdjs.GameCode.GDzaObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].returnVariable(gdjs.GameCode.GDbaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].returnVariable(gdjs.GameCode.GDcaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].returnVariable(gdjs.GameCode.GDdaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].returnVariable(gdjs.GameCode.GDfaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].returnVariable(gdjs.GameCode.GDgaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].returnVariable(gdjs.GameCode.GDhaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].returnVariable(gdjs.GameCode.GDjaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].returnVariable(gdjs.GameCode.GDlaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].returnVariable(gdjs.GameCode.GDmaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].returnVariable(gdjs.GameCode.GDnaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].returnVariable(gdjs.GameCode.GDpaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].returnVariable(gdjs.GameCode.GDquaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].returnVariable(gdjs.GameCode.GDraObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].returnVariable(gdjs.GameCode.GDsaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].returnVariable(gdjs.GameCode.GDtaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].returnVariable(gdjs.GameCode.GDvaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].returnVariable(gdjs.GameCode.GDxaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].returnVariable(gdjs.GameCode.GDzaObjects2[i].getVariables().get("cair")).setNumber(0);
}
}{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba2Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba2Objects2[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.GameCode.GDsilaba2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDsilaba2Objects2[i].hide();
}
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise03.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].returnVariable(gdjs.GameCode.GDplayerObjects2[i].getVariables().getFromIndex(0)).setNumber(1);
}
}}

}


{

gdjs.GameCode.GDbaObjects2.createFrom(gdjs.GameCode.GDbaObjects1);

gdjs.GameCode.GDcaObjects2.createFrom(gdjs.GameCode.GDcaObjects1);

gdjs.GameCode.GDdaObjects2.createFrom(gdjs.GameCode.GDdaObjects1);

gdjs.GameCode.GDfaObjects2.createFrom(gdjs.GameCode.GDfaObjects1);

gdjs.GameCode.GDgaObjects2.createFrom(gdjs.GameCode.GDgaObjects1);

gdjs.GameCode.GDhaObjects2.createFrom(gdjs.GameCode.GDhaObjects1);

gdjs.GameCode.GDjaObjects2.createFrom(gdjs.GameCode.GDjaObjects1);

gdjs.GameCode.GDlaObjects2.createFrom(gdjs.GameCode.GDlaObjects1);

gdjs.GameCode.GDmaObjects2.createFrom(gdjs.GameCode.GDmaObjects1);

gdjs.GameCode.GDnaObjects2.createFrom(gdjs.GameCode.GDnaObjects1);

gdjs.GameCode.GDpaObjects2.createFrom(gdjs.GameCode.GDpaObjects1);

gdjs.GameCode.GDquaObjects2.createFrom(gdjs.GameCode.GDquaObjects1);

gdjs.GameCode.GDraObjects2.createFrom(gdjs.GameCode.GDraObjects1);

gdjs.GameCode.GDsaObjects2.createFrom(gdjs.GameCode.GDsaObjects1);

gdjs.GameCode.GDtaObjects2.createFrom(gdjs.GameCode.GDtaObjects1);

gdjs.GameCode.GDvaObjects2.createFrom(gdjs.GameCode.GDvaObjects1);

gdjs.GameCode.GDxaObjects2.createFrom(gdjs.GameCode.GDxaObjects1);

gdjs.GameCode.GDzaObjects2.createFrom(gdjs.GameCode.GDzaObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 2;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(3)) == (( gdjs.GameCode.GDzaObjects2.length === 0 ) ? (( gdjs.GameCode.GDxaObjects2.length === 0 ) ? (( gdjs.GameCode.GDvaObjects2.length === 0 ) ? (( gdjs.GameCode.GDtaObjects2.length === 0 ) ? (( gdjs.GameCode.GDsaObjects2.length === 0 ) ? (( gdjs.GameCode.GDraObjects2.length === 0 ) ? (( gdjs.GameCode.GDquaObjects2.length === 0 ) ? (( gdjs.GameCode.GDpaObjects2.length === 0 ) ? (( gdjs.GameCode.GDnaObjects2.length === 0 ) ? (( gdjs.GameCode.GDmaObjects2.length === 0 ) ? (( gdjs.GameCode.GDlaObjects2.length === 0 ) ? (( gdjs.GameCode.GDjaObjects2.length === 0 ) ? (( gdjs.GameCode.GDhaObjects2.length === 0 ) ? (( gdjs.GameCode.GDgaObjects2.length === 0 ) ? (( gdjs.GameCode.GDfaObjects2.length === 0 ) ? (( gdjs.GameCode.GDdaObjects2.length === 0 ) ? (( gdjs.GameCode.GDcaObjects2.length === 0 ) ? (( gdjs.GameCode.GDbaObjects2.length === 0 ) ? "" :gdjs.GameCode.GDbaObjects2[0].getAnimationName()) :gdjs.GameCode.GDcaObjects2[0].getAnimationName()) :gdjs.GameCode.GDdaObjects2[0].getAnimationName()) :gdjs.GameCode.GDfaObjects2[0].getAnimationName()) :gdjs.GameCode.GDgaObjects2[0].getAnimationName()) :gdjs.GameCode.GDhaObjects2[0].getAnimationName()) :gdjs.GameCode.GDjaObjects2[0].getAnimationName()) :gdjs.GameCode.GDlaObjects2[0].getAnimationName()) :gdjs.GameCode.GDmaObjects2[0].getAnimationName()) :gdjs.GameCode.GDnaObjects2[0].getAnimationName()) :gdjs.GameCode.GDpaObjects2[0].getAnimationName()) :gdjs.GameCode.GDquaObjects2[0].getAnimationName()) :gdjs.GameCode.GDraObjects2[0].getAnimationName()) :gdjs.GameCode.GDsaObjects2[0].getAnimationName()) :gdjs.GameCode.GDtaObjects2[0].getAnimationName()) :gdjs.GameCode.GDvaObjects2[0].getAnimationName()) :gdjs.GameCode.GDxaObjects2[0].getAnimationName()) :gdjs.GameCode.GDzaObjects2[0].getAnimationName());
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(7310604);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbaObjects2 */
/* Reuse gdjs.GameCode.GDcaObjects2 */
/* Reuse gdjs.GameCode.GDdaObjects2 */
/* Reuse gdjs.GameCode.GDfaObjects2 */
/* Reuse gdjs.GameCode.GDgaObjects2 */
/* Reuse gdjs.GameCode.GDhaObjects2 */
/* Reuse gdjs.GameCode.GDjaObjects2 */
/* Reuse gdjs.GameCode.GDlaObjects2 */
/* Reuse gdjs.GameCode.GDmaObjects2 */
/* Reuse gdjs.GameCode.GDnaObjects2 */
/* Reuse gdjs.GameCode.GDpaObjects2 */
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

/* Reuse gdjs.GameCode.GDquaObjects2 */
/* Reuse gdjs.GameCode.GDraObjects2 */
/* Reuse gdjs.GameCode.GDsaObjects2 */
gdjs.GameCode.GDsilaba3Objects2.createFrom(runtimeScene.getObjects("silaba3"));
/* Reuse gdjs.GameCode.GDtaObjects2 */
/* Reuse gdjs.GameCode.GDvaObjects2 */
/* Reuse gdjs.GameCode.GDxaObjects2 */
/* Reuse gdjs.GameCode.GDzaObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].returnVariable(gdjs.GameCode.GDbaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].returnVariable(gdjs.GameCode.GDcaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].returnVariable(gdjs.GameCode.GDdaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].returnVariable(gdjs.GameCode.GDfaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].returnVariable(gdjs.GameCode.GDgaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].returnVariable(gdjs.GameCode.GDhaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].returnVariable(gdjs.GameCode.GDjaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].returnVariable(gdjs.GameCode.GDlaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].returnVariable(gdjs.GameCode.GDmaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].returnVariable(gdjs.GameCode.GDnaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].returnVariable(gdjs.GameCode.GDpaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].returnVariable(gdjs.GameCode.GDquaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].returnVariable(gdjs.GameCode.GDraObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].returnVariable(gdjs.GameCode.GDsaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].returnVariable(gdjs.GameCode.GDtaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].returnVariable(gdjs.GameCode.GDvaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].returnVariable(gdjs.GameCode.GDxaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].returnVariable(gdjs.GameCode.GDzaObjects2[i].getVariables().get("cair")).setNumber(0);
}
}{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba3Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba3Objects2[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.GameCode.GDsilaba3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDsilaba3Objects2[i].hide();
}
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise03.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].returnVariable(gdjs.GameCode.GDplayerObjects2[i].getVariables().getFromIndex(0)).setNumber(1);
}
}}

}


{

gdjs.GameCode.GDbaObjects2.createFrom(gdjs.GameCode.GDbaObjects1);

gdjs.GameCode.GDcaObjects2.createFrom(gdjs.GameCode.GDcaObjects1);

gdjs.GameCode.GDdaObjects2.createFrom(gdjs.GameCode.GDdaObjects1);

gdjs.GameCode.GDfaObjects2.createFrom(gdjs.GameCode.GDfaObjects1);

gdjs.GameCode.GDgaObjects2.createFrom(gdjs.GameCode.GDgaObjects1);

gdjs.GameCode.GDhaObjects2.createFrom(gdjs.GameCode.GDhaObjects1);

gdjs.GameCode.GDjaObjects2.createFrom(gdjs.GameCode.GDjaObjects1);

gdjs.GameCode.GDlaObjects2.createFrom(gdjs.GameCode.GDlaObjects1);

gdjs.GameCode.GDmaObjects2.createFrom(gdjs.GameCode.GDmaObjects1);

gdjs.GameCode.GDnaObjects2.createFrom(gdjs.GameCode.GDnaObjects1);

gdjs.GameCode.GDpaObjects2.createFrom(gdjs.GameCode.GDpaObjects1);

gdjs.GameCode.GDquaObjects2.createFrom(gdjs.GameCode.GDquaObjects1);

gdjs.GameCode.GDraObjects2.createFrom(gdjs.GameCode.GDraObjects1);

gdjs.GameCode.GDsaObjects2.createFrom(gdjs.GameCode.GDsaObjects1);

gdjs.GameCode.GDtaObjects2.createFrom(gdjs.GameCode.GDtaObjects1);

gdjs.GameCode.GDvaObjects2.createFrom(gdjs.GameCode.GDvaObjects1);

gdjs.GameCode.GDxaObjects2.createFrom(gdjs.GameCode.GDxaObjects1);

gdjs.GameCode.GDzaObjects2.createFrom(gdjs.GameCode.GDzaObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 3;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)) == (( gdjs.GameCode.GDzaObjects2.length === 0 ) ? (( gdjs.GameCode.GDxaObjects2.length === 0 ) ? (( gdjs.GameCode.GDvaObjects2.length === 0 ) ? (( gdjs.GameCode.GDtaObjects2.length === 0 ) ? (( gdjs.GameCode.GDsaObjects2.length === 0 ) ? (( gdjs.GameCode.GDraObjects2.length === 0 ) ? (( gdjs.GameCode.GDquaObjects2.length === 0 ) ? (( gdjs.GameCode.GDpaObjects2.length === 0 ) ? (( gdjs.GameCode.GDnaObjects2.length === 0 ) ? (( gdjs.GameCode.GDmaObjects2.length === 0 ) ? (( gdjs.GameCode.GDlaObjects2.length === 0 ) ? (( gdjs.GameCode.GDjaObjects2.length === 0 ) ? (( gdjs.GameCode.GDhaObjects2.length === 0 ) ? (( gdjs.GameCode.GDgaObjects2.length === 0 ) ? (( gdjs.GameCode.GDfaObjects2.length === 0 ) ? (( gdjs.GameCode.GDdaObjects2.length === 0 ) ? (( gdjs.GameCode.GDcaObjects2.length === 0 ) ? (( gdjs.GameCode.GDbaObjects2.length === 0 ) ? "" :gdjs.GameCode.GDbaObjects2[0].getAnimationName()) :gdjs.GameCode.GDcaObjects2[0].getAnimationName()) :gdjs.GameCode.GDdaObjects2[0].getAnimationName()) :gdjs.GameCode.GDfaObjects2[0].getAnimationName()) :gdjs.GameCode.GDgaObjects2[0].getAnimationName()) :gdjs.GameCode.GDhaObjects2[0].getAnimationName()) :gdjs.GameCode.GDjaObjects2[0].getAnimationName()) :gdjs.GameCode.GDlaObjects2[0].getAnimationName()) :gdjs.GameCode.GDmaObjects2[0].getAnimationName()) :gdjs.GameCode.GDnaObjects2[0].getAnimationName()) :gdjs.GameCode.GDpaObjects2[0].getAnimationName()) :gdjs.GameCode.GDquaObjects2[0].getAnimationName()) :gdjs.GameCode.GDraObjects2[0].getAnimationName()) :gdjs.GameCode.GDsaObjects2[0].getAnimationName()) :gdjs.GameCode.GDtaObjects2[0].getAnimationName()) :gdjs.GameCode.GDvaObjects2[0].getAnimationName()) :gdjs.GameCode.GDxaObjects2[0].getAnimationName()) :gdjs.GameCode.GDzaObjects2[0].getAnimationName());
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(7312420);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbaObjects2 */
/* Reuse gdjs.GameCode.GDcaObjects2 */
/* Reuse gdjs.GameCode.GDdaObjects2 */
/* Reuse gdjs.GameCode.GDfaObjects2 */
/* Reuse gdjs.GameCode.GDgaObjects2 */
/* Reuse gdjs.GameCode.GDhaObjects2 */
/* Reuse gdjs.GameCode.GDjaObjects2 */
/* Reuse gdjs.GameCode.GDlaObjects2 */
/* Reuse gdjs.GameCode.GDmaObjects2 */
/* Reuse gdjs.GameCode.GDnaObjects2 */
/* Reuse gdjs.GameCode.GDpaObjects2 */
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

/* Reuse gdjs.GameCode.GDquaObjects2 */
/* Reuse gdjs.GameCode.GDraObjects2 */
/* Reuse gdjs.GameCode.GDsaObjects2 */
gdjs.GameCode.GDsilaba4Objects2.createFrom(runtimeScene.getObjects("silaba4"));
/* Reuse gdjs.GameCode.GDtaObjects2 */
/* Reuse gdjs.GameCode.GDvaObjects2 */
/* Reuse gdjs.GameCode.GDxaObjects2 */
/* Reuse gdjs.GameCode.GDzaObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].returnVariable(gdjs.GameCode.GDbaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].returnVariable(gdjs.GameCode.GDcaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].returnVariable(gdjs.GameCode.GDdaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].returnVariable(gdjs.GameCode.GDfaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].returnVariable(gdjs.GameCode.GDgaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].returnVariable(gdjs.GameCode.GDhaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].returnVariable(gdjs.GameCode.GDjaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].returnVariable(gdjs.GameCode.GDlaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].returnVariable(gdjs.GameCode.GDmaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].returnVariable(gdjs.GameCode.GDnaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].returnVariable(gdjs.GameCode.GDpaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].returnVariable(gdjs.GameCode.GDquaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].returnVariable(gdjs.GameCode.GDraObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].returnVariable(gdjs.GameCode.GDsaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].returnVariable(gdjs.GameCode.GDtaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].returnVariable(gdjs.GameCode.GDvaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].returnVariable(gdjs.GameCode.GDxaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].returnVariable(gdjs.GameCode.GDzaObjects2[i].getVariables().get("cair")).setNumber(0);
}
}{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba4Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba4Objects2[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.GameCode.GDsilaba4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDsilaba4Objects2[i].hide();
}
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise03.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].returnVariable(gdjs.GameCode.GDplayerObjects2[i].getVariables().getFromIndex(0)).setNumber(1);
}
}}

}


{

gdjs.GameCode.GDbaObjects2.createFrom(gdjs.GameCode.GDbaObjects1);

gdjs.GameCode.GDcaObjects2.createFrom(gdjs.GameCode.GDcaObjects1);

gdjs.GameCode.GDdaObjects2.createFrom(gdjs.GameCode.GDdaObjects1);

gdjs.GameCode.GDfaObjects2.createFrom(gdjs.GameCode.GDfaObjects1);

gdjs.GameCode.GDgaObjects2.createFrom(gdjs.GameCode.GDgaObjects1);

gdjs.GameCode.GDhaObjects2.createFrom(gdjs.GameCode.GDhaObjects1);

gdjs.GameCode.GDjaObjects2.createFrom(gdjs.GameCode.GDjaObjects1);

gdjs.GameCode.GDlaObjects2.createFrom(gdjs.GameCode.GDlaObjects1);

gdjs.GameCode.GDmaObjects2.createFrom(gdjs.GameCode.GDmaObjects1);

gdjs.GameCode.GDnaObjects2.createFrom(gdjs.GameCode.GDnaObjects1);

gdjs.GameCode.GDpaObjects2.createFrom(gdjs.GameCode.GDpaObjects1);

gdjs.GameCode.GDquaObjects2.createFrom(gdjs.GameCode.GDquaObjects1);

gdjs.GameCode.GDraObjects2.createFrom(gdjs.GameCode.GDraObjects1);

gdjs.GameCode.GDsaObjects2.createFrom(gdjs.GameCode.GDsaObjects1);

gdjs.GameCode.GDtaObjects2.createFrom(gdjs.GameCode.GDtaObjects1);

gdjs.GameCode.GDvaObjects2.createFrom(gdjs.GameCode.GDvaObjects1);

gdjs.GameCode.GDxaObjects2.createFrom(gdjs.GameCode.GDxaObjects1);

gdjs.GameCode.GDzaObjects2.createFrom(gdjs.GameCode.GDzaObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 4;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(5)) == (( gdjs.GameCode.GDzaObjects2.length === 0 ) ? (( gdjs.GameCode.GDxaObjects2.length === 0 ) ? (( gdjs.GameCode.GDvaObjects2.length === 0 ) ? (( gdjs.GameCode.GDtaObjects2.length === 0 ) ? (( gdjs.GameCode.GDsaObjects2.length === 0 ) ? (( gdjs.GameCode.GDraObjects2.length === 0 ) ? (( gdjs.GameCode.GDquaObjects2.length === 0 ) ? (( gdjs.GameCode.GDpaObjects2.length === 0 ) ? (( gdjs.GameCode.GDnaObjects2.length === 0 ) ? (( gdjs.GameCode.GDmaObjects2.length === 0 ) ? (( gdjs.GameCode.GDlaObjects2.length === 0 ) ? (( gdjs.GameCode.GDjaObjects2.length === 0 ) ? (( gdjs.GameCode.GDhaObjects2.length === 0 ) ? (( gdjs.GameCode.GDgaObjects2.length === 0 ) ? (( gdjs.GameCode.GDfaObjects2.length === 0 ) ? (( gdjs.GameCode.GDdaObjects2.length === 0 ) ? (( gdjs.GameCode.GDcaObjects2.length === 0 ) ? (( gdjs.GameCode.GDbaObjects2.length === 0 ) ? "" :gdjs.GameCode.GDbaObjects2[0].getAnimationName()) :gdjs.GameCode.GDcaObjects2[0].getAnimationName()) :gdjs.GameCode.GDdaObjects2[0].getAnimationName()) :gdjs.GameCode.GDfaObjects2[0].getAnimationName()) :gdjs.GameCode.GDgaObjects2[0].getAnimationName()) :gdjs.GameCode.GDhaObjects2[0].getAnimationName()) :gdjs.GameCode.GDjaObjects2[0].getAnimationName()) :gdjs.GameCode.GDlaObjects2[0].getAnimationName()) :gdjs.GameCode.GDmaObjects2[0].getAnimationName()) :gdjs.GameCode.GDnaObjects2[0].getAnimationName()) :gdjs.GameCode.GDpaObjects2[0].getAnimationName()) :gdjs.GameCode.GDquaObjects2[0].getAnimationName()) :gdjs.GameCode.GDraObjects2[0].getAnimationName()) :gdjs.GameCode.GDsaObjects2[0].getAnimationName()) :gdjs.GameCode.GDtaObjects2[0].getAnimationName()) :gdjs.GameCode.GDvaObjects2[0].getAnimationName()) :gdjs.GameCode.GDxaObjects2[0].getAnimationName()) :gdjs.GameCode.GDzaObjects2[0].getAnimationName());
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(7314220);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbaObjects2 */
/* Reuse gdjs.GameCode.GDcaObjects2 */
/* Reuse gdjs.GameCode.GDdaObjects2 */
/* Reuse gdjs.GameCode.GDfaObjects2 */
/* Reuse gdjs.GameCode.GDgaObjects2 */
/* Reuse gdjs.GameCode.GDhaObjects2 */
/* Reuse gdjs.GameCode.GDjaObjects2 */
/* Reuse gdjs.GameCode.GDlaObjects2 */
/* Reuse gdjs.GameCode.GDmaObjects2 */
/* Reuse gdjs.GameCode.GDnaObjects2 */
/* Reuse gdjs.GameCode.GDpaObjects2 */
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

/* Reuse gdjs.GameCode.GDquaObjects2 */
/* Reuse gdjs.GameCode.GDraObjects2 */
/* Reuse gdjs.GameCode.GDsaObjects2 */
gdjs.GameCode.GDsilaba5Objects2.createFrom(runtimeScene.getObjects("silaba5"));
/* Reuse gdjs.GameCode.GDtaObjects2 */
/* Reuse gdjs.GameCode.GDvaObjects2 */
/* Reuse gdjs.GameCode.GDxaObjects2 */
/* Reuse gdjs.GameCode.GDzaObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].returnVariable(gdjs.GameCode.GDbaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].returnVariable(gdjs.GameCode.GDcaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].returnVariable(gdjs.GameCode.GDdaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].returnVariable(gdjs.GameCode.GDfaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].returnVariable(gdjs.GameCode.GDgaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].returnVariable(gdjs.GameCode.GDhaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].returnVariable(gdjs.GameCode.GDjaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].returnVariable(gdjs.GameCode.GDlaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].returnVariable(gdjs.GameCode.GDmaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].returnVariable(gdjs.GameCode.GDnaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].returnVariable(gdjs.GameCode.GDpaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].returnVariable(gdjs.GameCode.GDquaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].returnVariable(gdjs.GameCode.GDraObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].returnVariable(gdjs.GameCode.GDsaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].returnVariable(gdjs.GameCode.GDtaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].returnVariable(gdjs.GameCode.GDvaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].returnVariable(gdjs.GameCode.GDxaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].returnVariable(gdjs.GameCode.GDzaObjects2[i].getVariables().get("cair")).setNumber(0);
}
}{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba5Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba5Objects2[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.GameCode.GDsilaba5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDsilaba5Objects2[i].hide();
}
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise03.ogg", false, 100, 1);
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].returnVariable(gdjs.GameCode.GDplayerObjects2[i].getVariables().getFromIndex(0)).setNumber(1);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 4;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "QUA";
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(7316092);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

{runtimeScene.getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].returnVariable(gdjs.GameCode.GDplayerObjects2[i].getVariables().getFromIndex(0)).setNumber(1);
}
}}

}


{

gdjs.GameCode.GDbaObjects2.createFrom(gdjs.GameCode.GDbaObjects1);

gdjs.GameCode.GDcaObjects2.createFrom(gdjs.GameCode.GDcaObjects1);

gdjs.GameCode.GDdaObjects2.createFrom(gdjs.GameCode.GDdaObjects1);

gdjs.GameCode.GDfaObjects2.createFrom(gdjs.GameCode.GDfaObjects1);

gdjs.GameCode.GDgaObjects2.createFrom(gdjs.GameCode.GDgaObjects1);

gdjs.GameCode.GDhaObjects2.createFrom(gdjs.GameCode.GDhaObjects1);

gdjs.GameCode.GDjaObjects2.createFrom(gdjs.GameCode.GDjaObjects1);

gdjs.GameCode.GDlaObjects2.createFrom(gdjs.GameCode.GDlaObjects1);

gdjs.GameCode.GDmaObjects2.createFrom(gdjs.GameCode.GDmaObjects1);

gdjs.GameCode.GDnaObjects2.createFrom(gdjs.GameCode.GDnaObjects1);

gdjs.GameCode.GDpaObjects2.createFrom(gdjs.GameCode.GDpaObjects1);

gdjs.GameCode.GDquaObjects2.createFrom(gdjs.GameCode.GDquaObjects1);

gdjs.GameCode.GDraObjects2.createFrom(gdjs.GameCode.GDraObjects1);

gdjs.GameCode.GDsaObjects2.createFrom(gdjs.GameCode.GDsaObjects1);

gdjs.GameCode.GDtaObjects2.createFrom(gdjs.GameCode.GDtaObjects1);

gdjs.GameCode.GDvaObjects2.createFrom(gdjs.GameCode.GDvaObjects1);

gdjs.GameCode.GDxaObjects2.createFrom(gdjs.GameCode.GDxaObjects1);

gdjs.GameCode.GDzaObjects2.createFrom(gdjs.GameCode.GDzaObjects1);


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 5;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(6)) == (( gdjs.GameCode.GDzaObjects2.length === 0 ) ? (( gdjs.GameCode.GDxaObjects2.length === 0 ) ? (( gdjs.GameCode.GDvaObjects2.length === 0 ) ? (( gdjs.GameCode.GDtaObjects2.length === 0 ) ? (( gdjs.GameCode.GDsaObjects2.length === 0 ) ? (( gdjs.GameCode.GDraObjects2.length === 0 ) ? (( gdjs.GameCode.GDquaObjects2.length === 0 ) ? (( gdjs.GameCode.GDpaObjects2.length === 0 ) ? (( gdjs.GameCode.GDnaObjects2.length === 0 ) ? (( gdjs.GameCode.GDmaObjects2.length === 0 ) ? (( gdjs.GameCode.GDlaObjects2.length === 0 ) ? (( gdjs.GameCode.GDjaObjects2.length === 0 ) ? (( gdjs.GameCode.GDhaObjects2.length === 0 ) ? (( gdjs.GameCode.GDgaObjects2.length === 0 ) ? (( gdjs.GameCode.GDfaObjects2.length === 0 ) ? (( gdjs.GameCode.GDdaObjects2.length === 0 ) ? (( gdjs.GameCode.GDcaObjects2.length === 0 ) ? (( gdjs.GameCode.GDbaObjects2.length === 0 ) ? "" :gdjs.GameCode.GDbaObjects2[0].getAnimationName()) :gdjs.GameCode.GDcaObjects2[0].getAnimationName()) :gdjs.GameCode.GDdaObjects2[0].getAnimationName()) :gdjs.GameCode.GDfaObjects2[0].getAnimationName()) :gdjs.GameCode.GDgaObjects2[0].getAnimationName()) :gdjs.GameCode.GDhaObjects2[0].getAnimationName()) :gdjs.GameCode.GDjaObjects2[0].getAnimationName()) :gdjs.GameCode.GDlaObjects2[0].getAnimationName()) :gdjs.GameCode.GDmaObjects2[0].getAnimationName()) :gdjs.GameCode.GDnaObjects2[0].getAnimationName()) :gdjs.GameCode.GDpaObjects2[0].getAnimationName()) :gdjs.GameCode.GDquaObjects2[0].getAnimationName()) :gdjs.GameCode.GDraObjects2[0].getAnimationName()) :gdjs.GameCode.GDsaObjects2[0].getAnimationName()) :gdjs.GameCode.GDtaObjects2[0].getAnimationName()) :gdjs.GameCode.GDvaObjects2[0].getAnimationName()) :gdjs.GameCode.GDxaObjects2[0].getAnimationName()) :gdjs.GameCode.GDzaObjects2[0].getAnimationName());
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(7317100);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbaObjects2 */
/* Reuse gdjs.GameCode.GDcaObjects2 */
/* Reuse gdjs.GameCode.GDdaObjects2 */
/* Reuse gdjs.GameCode.GDfaObjects2 */
/* Reuse gdjs.GameCode.GDgaObjects2 */
/* Reuse gdjs.GameCode.GDhaObjects2 */
/* Reuse gdjs.GameCode.GDjaObjects2 */
/* Reuse gdjs.GameCode.GDlaObjects2 */
/* Reuse gdjs.GameCode.GDmaObjects2 */
/* Reuse gdjs.GameCode.GDnaObjects2 */
/* Reuse gdjs.GameCode.GDpaObjects2 */
gdjs.GameCode.GDplayerObjects2.createFrom(gdjs.GameCode.GDplayerObjects1);

/* Reuse gdjs.GameCode.GDquaObjects2 */
/* Reuse gdjs.GameCode.GDraObjects2 */
/* Reuse gdjs.GameCode.GDsaObjects2 */
gdjs.GameCode.GDsilaba6Objects2.createFrom(runtimeScene.getObjects("silaba6"));
/* Reuse gdjs.GameCode.GDtaObjects2 */
/* Reuse gdjs.GameCode.GDvaObjects2 */
/* Reuse gdjs.GameCode.GDxaObjects2 */
/* Reuse gdjs.GameCode.GDzaObjects2 */
{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].returnVariable(gdjs.GameCode.GDbaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].returnVariable(gdjs.GameCode.GDcaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].returnVariable(gdjs.GameCode.GDdaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].returnVariable(gdjs.GameCode.GDfaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].returnVariable(gdjs.GameCode.GDgaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].returnVariable(gdjs.GameCode.GDhaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].returnVariable(gdjs.GameCode.GDjaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].returnVariable(gdjs.GameCode.GDlaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].returnVariable(gdjs.GameCode.GDmaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].returnVariable(gdjs.GameCode.GDnaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].returnVariable(gdjs.GameCode.GDpaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].returnVariable(gdjs.GameCode.GDquaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].returnVariable(gdjs.GameCode.GDraObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].returnVariable(gdjs.GameCode.GDsaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].returnVariable(gdjs.GameCode.GDtaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].returnVariable(gdjs.GameCode.GDvaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].returnVariable(gdjs.GameCode.GDxaObjects2[i].getVariables().get("cair")).setNumber(0);
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].returnVariable(gdjs.GameCode.GDzaObjects2[i].getVariables().get("cair")).setNumber(0);
}
}{for(var i = 0, len = gdjs.GameCode.GDbaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDraObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects2[i].setPosition((( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointX("")),(( gdjs.GameCode.GDsilaba6Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDsilaba6Objects2[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.GameCode.GDsilaba6Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDsilaba6Objects2[i].hide();
}
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.GameCode.GDplayerObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects2[i].returnVariable(gdjs.GameCode.GDplayerObjects2[i].getVariables().getFromIndex(0)).setNumber(1);
}
}{runtimeScene.getGame().getVariables().getFromIndex(7).setString("PARABÉNS! VOCÊ CONSEGUIU!");
}{runtimeScene.getGame().getVariables().getFromIndex(9).setNumber(0);
}{gdjs.evtTools.sound.playSound(runtimeScene, "Rise03.ogg", false, 100, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Transicao", false);
}}

}


{

/* Reuse gdjs.GameCode.GDplayerObjects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getVariableNumber(gdjs.GameCode.GDplayerObjects1[i].getVariables().getFromIndex(0)) == 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(7).setString("VAMOS TENTAR DE NOVO");
}{runtimeScene.getGame().getVariables().getFromIndex(9).setNumber(2);
}{gdjs.evtTools.sound.playSound(runtimeScene, "Downer01.ogg", false, 100, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Transicao", false);
}}

}


}; //End of gdjs.GameCode.eventsList0x6f7a88
gdjs.GameCode.eventsList0xaf9c0 = function(runtimeScene, context) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDskyObjects1.createFrom(runtimeScene.getObjects("sky"));
{for(var i = 0, len = gdjs.GameCode.GDskyObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDskyObjects1[i].setXOffset(gdjs.GameCode.GDskyObjects1[i].getXOffset() + (0.2));
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(0);
}{gdjs.evtTools.sound.playMusic(runtimeScene, "Iceland Theme.ogg", true, 100, 1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "bird_flap.ogg", true, 100, 1.5);
}}

}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDcolisor_95playerObjects1.createFrom(runtimeScene.getObjects("colisor_player"));
{for(var i = 0, len = gdjs.GameCode.GDcolisor_95playerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcolisor_95playerObjects1[i].hide();
}
}}

}


{



}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getX() >= 0 ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].addForce(-130, 0, 0);
}
}}

}


{

gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDplayerObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDplayerObjects1[i].getX() <= gdjs.evtTools.window.getCanvasWidth(runtimeScene) - (gdjs.GameCode.GDplayerObjects1[i].getWidth()) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDplayerObjects1[k] = gdjs.GameCode.GDplayerObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDplayerObjects1.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDplayerObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].addForce(130, 0, 0);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.logicalNegation(false);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDcolisor_95playerObjects1.createFrom(runtimeScene.getObjects("colisor_player"));
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.GameCode.GDcolisor_95playerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcolisor_95playerObjects1[i].setPosition((( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointX("")) + (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getWidth())/2 - (gdjs.GameCode.GDcolisor_95playerObjects1[i].getWidth())/2,(( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getPointY("")) + (( gdjs.GameCode.GDplayerObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDplayerObjects1[0].getHeight())/2 - (gdjs.GameCode.GDcolisor_95playerObjects1[i].getHeight())/2);
}
}}

}


{



}


{



}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "criar_silaba");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "criar_silaba");
}
{ //Subevents
gdjs.GameCode.eventsList0x6ff120(runtimeScene, context);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDbaObjects1.createFrom(runtimeScene.getObjects("ba"));
gdjs.GameCode.GDcaObjects1.createFrom(runtimeScene.getObjects("ca"));
gdjs.GameCode.GDdaObjects1.createFrom(runtimeScene.getObjects("da"));
gdjs.GameCode.GDfaObjects1.createFrom(runtimeScene.getObjects("fa"));
gdjs.GameCode.GDgaObjects1.createFrom(runtimeScene.getObjects("ga"));
gdjs.GameCode.GDhaObjects1.createFrom(runtimeScene.getObjects("ha"));
gdjs.GameCode.GDjaObjects1.createFrom(runtimeScene.getObjects("ja"));
gdjs.GameCode.GDlaObjects1.createFrom(runtimeScene.getObjects("la"));
gdjs.GameCode.GDmaObjects1.createFrom(runtimeScene.getObjects("ma"));
gdjs.GameCode.GDnaObjects1.createFrom(runtimeScene.getObjects("na"));
gdjs.GameCode.GDpaObjects1.createFrom(runtimeScene.getObjects("pa"));
gdjs.GameCode.GDquaObjects1.createFrom(runtimeScene.getObjects("qua"));
gdjs.GameCode.GDraObjects1.createFrom(runtimeScene.getObjects("ra"));
gdjs.GameCode.GDsaObjects1.createFrom(runtimeScene.getObjects("sa"));
gdjs.GameCode.GDtaObjects1.createFrom(runtimeScene.getObjects("ta"));
gdjs.GameCode.GDvaObjects1.createFrom(runtimeScene.getObjects("va"));
gdjs.GameCode.GDxaObjects1.createFrom(runtimeScene.getObjects("xa"));
gdjs.GameCode.GDzaObjects1.createFrom(runtimeScene.getObjects("za"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbaObjects1[i].getVariableNumber(gdjs.GameCode.GDbaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbaObjects1[k] = gdjs.GameCode.GDbaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcaObjects1[i].getVariableNumber(gdjs.GameCode.GDcaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcaObjects1[k] = gdjs.GameCode.GDcaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDdaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDdaObjects1[i].getVariableNumber(gdjs.GameCode.GDdaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDdaObjects1[k] = gdjs.GameCode.GDdaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDdaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDfaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDfaObjects1[i].getVariableNumber(gdjs.GameCode.GDfaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDfaObjects1[k] = gdjs.GameCode.GDfaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDfaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDgaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDgaObjects1[i].getVariableNumber(gdjs.GameCode.GDgaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDgaObjects1[k] = gdjs.GameCode.GDgaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDgaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDhaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDhaObjects1[i].getVariableNumber(gdjs.GameCode.GDhaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDhaObjects1[k] = gdjs.GameCode.GDhaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDhaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDjaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDjaObjects1[i].getVariableNumber(gdjs.GameCode.GDjaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDjaObjects1[k] = gdjs.GameCode.GDjaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDjaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDlaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDlaObjects1[i].getVariableNumber(gdjs.GameCode.GDlaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDlaObjects1[k] = gdjs.GameCode.GDlaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDlaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDmaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDmaObjects1[i].getVariableNumber(gdjs.GameCode.GDmaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDmaObjects1[k] = gdjs.GameCode.GDmaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDmaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDnaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDnaObjects1[i].getVariableNumber(gdjs.GameCode.GDnaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnaObjects1[k] = gdjs.GameCode.GDnaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDnaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDpaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDpaObjects1[i].getVariableNumber(gdjs.GameCode.GDpaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDpaObjects1[k] = gdjs.GameCode.GDpaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDpaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDquaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDquaObjects1[i].getVariableNumber(gdjs.GameCode.GDquaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquaObjects1[k] = gdjs.GameCode.GDquaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDquaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDraObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDraObjects1[i].getVariableNumber(gdjs.GameCode.GDraObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDraObjects1[k] = gdjs.GameCode.GDraObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDraObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDsaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDsaObjects1[i].getVariableNumber(gdjs.GameCode.GDsaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDsaObjects1[k] = gdjs.GameCode.GDsaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDsaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDtaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDtaObjects1[i].getVariableNumber(gdjs.GameCode.GDtaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDtaObjects1[k] = gdjs.GameCode.GDtaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDtaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDvaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDvaObjects1[i].getVariableNumber(gdjs.GameCode.GDvaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDvaObjects1[k] = gdjs.GameCode.GDvaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDvaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDxaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDxaObjects1[i].getVariableNumber(gdjs.GameCode.GDxaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDxaObjects1[k] = gdjs.GameCode.GDxaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDxaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDzaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDzaObjects1[i].getVariableNumber(gdjs.GameCode.GDzaObjects1[i].getVariables().get("cair")) == 1 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDzaObjects1[k] = gdjs.GameCode.GDzaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDzaObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbaObjects1 */
/* Reuse gdjs.GameCode.GDcaObjects1 */
/* Reuse gdjs.GameCode.GDdaObjects1 */
/* Reuse gdjs.GameCode.GDfaObjects1 */
/* Reuse gdjs.GameCode.GDgaObjects1 */
/* Reuse gdjs.GameCode.GDhaObjects1 */
/* Reuse gdjs.GameCode.GDjaObjects1 */
/* Reuse gdjs.GameCode.GDlaObjects1 */
/* Reuse gdjs.GameCode.GDmaObjects1 */
/* Reuse gdjs.GameCode.GDnaObjects1 */
/* Reuse gdjs.GameCode.GDpaObjects1 */
/* Reuse gdjs.GameCode.GDquaObjects1 */
/* Reuse gdjs.GameCode.GDraObjects1 */
/* Reuse gdjs.GameCode.GDsaObjects1 */
/* Reuse gdjs.GameCode.GDtaObjects1 */
/* Reuse gdjs.GameCode.GDvaObjects1 */
/* Reuse gdjs.GameCode.GDxaObjects1 */
/* Reuse gdjs.GameCode.GDzaObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDraObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects1[i].addForce(0, 60, 0);
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects1[i].addForce(0, 60, 0);
}
}}

}


{

gdjs.GameCode.GDbaObjects1.createFrom(runtimeScene.getObjects("ba"));
gdjs.GameCode.GDcaObjects1.createFrom(runtimeScene.getObjects("ca"));
gdjs.GameCode.GDdaObjects1.createFrom(runtimeScene.getObjects("da"));
gdjs.GameCode.GDfaObjects1.createFrom(runtimeScene.getObjects("fa"));
gdjs.GameCode.GDgaObjects1.createFrom(runtimeScene.getObjects("ga"));
gdjs.GameCode.GDhaObjects1.createFrom(runtimeScene.getObjects("ha"));
gdjs.GameCode.GDjaObjects1.createFrom(runtimeScene.getObjects("ja"));
gdjs.GameCode.GDlaObjects1.createFrom(runtimeScene.getObjects("la"));
gdjs.GameCode.GDmaObjects1.createFrom(runtimeScene.getObjects("ma"));
gdjs.GameCode.GDnaObjects1.createFrom(runtimeScene.getObjects("na"));
gdjs.GameCode.GDpaObjects1.createFrom(runtimeScene.getObjects("pa"));
gdjs.GameCode.GDquaObjects1.createFrom(runtimeScene.getObjects("qua"));
gdjs.GameCode.GDraObjects1.createFrom(runtimeScene.getObjects("ra"));
gdjs.GameCode.GDsaObjects1.createFrom(runtimeScene.getObjects("sa"));
gdjs.GameCode.GDtaObjects1.createFrom(runtimeScene.getObjects("ta"));
gdjs.GameCode.GDvaObjects1.createFrom(runtimeScene.getObjects("va"));
gdjs.GameCode.GDxaObjects1.createFrom(runtimeScene.getObjects("xa"));
gdjs.GameCode.GDzaObjects1.createFrom(runtimeScene.getObjects("za"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDbaObjects1[k] = gdjs.GameCode.GDbaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcaObjects1[k] = gdjs.GameCode.GDcaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDdaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDdaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDdaObjects1[k] = gdjs.GameCode.GDdaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDdaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDfaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDfaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDfaObjects1[k] = gdjs.GameCode.GDfaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDfaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDgaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDgaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDgaObjects1[k] = gdjs.GameCode.GDgaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDgaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDhaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDhaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDhaObjects1[k] = gdjs.GameCode.GDhaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDhaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDjaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDjaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDjaObjects1[k] = gdjs.GameCode.GDjaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDjaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDlaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDlaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDlaObjects1[k] = gdjs.GameCode.GDlaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDlaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDmaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDmaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDmaObjects1[k] = gdjs.GameCode.GDmaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDmaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDnaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDnaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDnaObjects1[k] = gdjs.GameCode.GDnaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDnaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDpaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDpaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDpaObjects1[k] = gdjs.GameCode.GDpaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDpaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDquaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDquaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDquaObjects1[k] = gdjs.GameCode.GDquaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDquaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDraObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDraObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDraObjects1[k] = gdjs.GameCode.GDraObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDraObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDsaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDsaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDsaObjects1[k] = gdjs.GameCode.GDsaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDsaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDtaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDtaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDtaObjects1[k] = gdjs.GameCode.GDtaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDtaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDvaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDvaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDvaObjects1[k] = gdjs.GameCode.GDvaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDvaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDxaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDxaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDxaObjects1[k] = gdjs.GameCode.GDxaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDxaObjects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDzaObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDzaObjects1[i].getY() > 600 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDzaObjects1[k] = gdjs.GameCode.GDzaObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDzaObjects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbaObjects1 */
/* Reuse gdjs.GameCode.GDcaObjects1 */
/* Reuse gdjs.GameCode.GDdaObjects1 */
/* Reuse gdjs.GameCode.GDfaObjects1 */
/* Reuse gdjs.GameCode.GDgaObjects1 */
/* Reuse gdjs.GameCode.GDhaObjects1 */
/* Reuse gdjs.GameCode.GDjaObjects1 */
/* Reuse gdjs.GameCode.GDlaObjects1 */
/* Reuse gdjs.GameCode.GDmaObjects1 */
/* Reuse gdjs.GameCode.GDnaObjects1 */
/* Reuse gdjs.GameCode.GDpaObjects1 */
/* Reuse gdjs.GameCode.GDquaObjects1 */
/* Reuse gdjs.GameCode.GDraObjects1 */
/* Reuse gdjs.GameCode.GDsaObjects1 */
/* Reuse gdjs.GameCode.GDtaObjects1 */
/* Reuse gdjs.GameCode.GDvaObjects1 */
/* Reuse gdjs.GameCode.GDxaObjects1 */
/* Reuse gdjs.GameCode.GDzaObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDcaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDdaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDdaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDfaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDfaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDgaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDgaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDhaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDhaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDjaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDjaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDlaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDlaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDmaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDmaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDnaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDnaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDpaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDpaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDquaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDquaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDraObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDraObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDsaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDsaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDtaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDvaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDvaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDxaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDxaObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.GameCode.GDzaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDzaObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{

gdjs.GameCode.GDbaObjects1.createFrom(runtimeScene.getObjects("ba"));
gdjs.GameCode.GDcaObjects1.createFrom(runtimeScene.getObjects("ca"));
gdjs.GameCode.GDcolisor_95playerObjects1.createFrom(runtimeScene.getObjects("colisor_player"));
gdjs.GameCode.GDdaObjects1.createFrom(runtimeScene.getObjects("da"));
gdjs.GameCode.GDfaObjects1.createFrom(runtimeScene.getObjects("fa"));
gdjs.GameCode.GDgaObjects1.createFrom(runtimeScene.getObjects("ga"));
gdjs.GameCode.GDhaObjects1.createFrom(runtimeScene.getObjects("ha"));
gdjs.GameCode.GDjaObjects1.createFrom(runtimeScene.getObjects("ja"));
gdjs.GameCode.GDlaObjects1.createFrom(runtimeScene.getObjects("la"));
gdjs.GameCode.GDmaObjects1.createFrom(runtimeScene.getObjects("ma"));
gdjs.GameCode.GDnaObjects1.createFrom(runtimeScene.getObjects("na"));
gdjs.GameCode.GDpaObjects1.createFrom(runtimeScene.getObjects("pa"));
gdjs.GameCode.GDquaObjects1.createFrom(runtimeScene.getObjects("qua"));
gdjs.GameCode.GDraObjects1.createFrom(runtimeScene.getObjects("ra"));
gdjs.GameCode.GDsaObjects1.createFrom(runtimeScene.getObjects("sa"));
gdjs.GameCode.GDtaObjects1.createFrom(runtimeScene.getObjects("ta"));
gdjs.GameCode.GDvaObjects1.createFrom(runtimeScene.getObjects("va"));
gdjs.GameCode.GDxaObjects1.createFrom(runtimeScene.getObjects("xa"));
gdjs.GameCode.GDzaObjects1.createFrom(runtimeScene.getObjects("za"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDbaObjects1ObjectsGDgdjs_46GameCode_46GDcaObjects1ObjectsGDgdjs_46GameCode_46GDdaObjects1ObjectsGDgdjs_46GameCode_46GDfaObjects1ObjectsGDgdjs_46GameCode_46GDgaObjects1ObjectsGDgdjs_46GameCode_46GDhaObjects1ObjectsGDgdjs_46GameCode_46GDjaObjects1ObjectsGDgdjs_46GameCode_46GDlaObjects1ObjectsGDgdjs_46GameCode_46GDmaObjects1ObjectsGDgdjs_46GameCode_46GDnaObjects1ObjectsGDgdjs_46GameCode_46GDpaObjects1ObjectsGDgdjs_46GameCode_46GDquaObjects1ObjectsGDgdjs_46GameCode_46GDraObjects1ObjectsGDgdjs_46GameCode_46GDsaObjects1ObjectsGDgdjs_46GameCode_46GDtaObjects1ObjectsGDgdjs_46GameCode_46GDvaObjects1ObjectsGDgdjs_46GameCode_46GDxaObjects1ObjectsGDgdjs_46GameCode_46GDzaObjects1Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcolisor_9595playerObjects1Objects, false, runtimeScene);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition1IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = context.triggerOnce(7306156);
}
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
gdjs.GameCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{for(var i = 0, len = gdjs.GameCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDplayerObjects1[i].returnVariable(gdjs.GameCode.GDplayerObjects1[i].getVariables().getFromIndex(0)).setNumber(0);
}
}
{ //Subevents
gdjs.GameCode.eventsList0x6f7a88(runtimeScene, context);} //End of subevents
}

}


{



}


}; //End of gdjs.GameCode.eventsList0xaf9c0


gdjs.GameCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GameCode.GDplayerObjects1.length = 0;
gdjs.GameCode.GDplayerObjects2.length = 0;
gdjs.GameCode.GDplayerObjects3.length = 0;
gdjs.GameCode.GDskyObjects1.length = 0;
gdjs.GameCode.GDskyObjects2.length = 0;
gdjs.GameCode.GDskyObjects3.length = 0;
gdjs.GameCode.GDbaObjects1.length = 0;
gdjs.GameCode.GDbaObjects2.length = 0;
gdjs.GameCode.GDbaObjects3.length = 0;
gdjs.GameCode.GDcaObjects1.length = 0;
gdjs.GameCode.GDcaObjects2.length = 0;
gdjs.GameCode.GDcaObjects3.length = 0;
gdjs.GameCode.GDdaObjects1.length = 0;
gdjs.GameCode.GDdaObjects2.length = 0;
gdjs.GameCode.GDdaObjects3.length = 0;
gdjs.GameCode.GDfaObjects1.length = 0;
gdjs.GameCode.GDfaObjects2.length = 0;
gdjs.GameCode.GDfaObjects3.length = 0;
gdjs.GameCode.GDgaObjects1.length = 0;
gdjs.GameCode.GDgaObjects2.length = 0;
gdjs.GameCode.GDgaObjects3.length = 0;
gdjs.GameCode.GDhaObjects1.length = 0;
gdjs.GameCode.GDhaObjects2.length = 0;
gdjs.GameCode.GDhaObjects3.length = 0;
gdjs.GameCode.GDjaObjects1.length = 0;
gdjs.GameCode.GDjaObjects2.length = 0;
gdjs.GameCode.GDjaObjects3.length = 0;
gdjs.GameCode.GDlaObjects1.length = 0;
gdjs.GameCode.GDlaObjects2.length = 0;
gdjs.GameCode.GDlaObjects3.length = 0;
gdjs.GameCode.GDmaObjects1.length = 0;
gdjs.GameCode.GDmaObjects2.length = 0;
gdjs.GameCode.GDmaObjects3.length = 0;
gdjs.GameCode.GDnaObjects1.length = 0;
gdjs.GameCode.GDnaObjects2.length = 0;
gdjs.GameCode.GDnaObjects3.length = 0;
gdjs.GameCode.GDpaObjects1.length = 0;
gdjs.GameCode.GDpaObjects2.length = 0;
gdjs.GameCode.GDpaObjects3.length = 0;
gdjs.GameCode.GDquaObjects1.length = 0;
gdjs.GameCode.GDquaObjects2.length = 0;
gdjs.GameCode.GDquaObjects3.length = 0;
gdjs.GameCode.GDraObjects1.length = 0;
gdjs.GameCode.GDraObjects2.length = 0;
gdjs.GameCode.GDraObjects3.length = 0;
gdjs.GameCode.GDsaObjects1.length = 0;
gdjs.GameCode.GDsaObjects2.length = 0;
gdjs.GameCode.GDsaObjects3.length = 0;
gdjs.GameCode.GDtaObjects1.length = 0;
gdjs.GameCode.GDtaObjects2.length = 0;
gdjs.GameCode.GDtaObjects3.length = 0;
gdjs.GameCode.GDvaObjects1.length = 0;
gdjs.GameCode.GDvaObjects2.length = 0;
gdjs.GameCode.GDvaObjects3.length = 0;
gdjs.GameCode.GDxaObjects1.length = 0;
gdjs.GameCode.GDxaObjects2.length = 0;
gdjs.GameCode.GDxaObjects3.length = 0;
gdjs.GameCode.GDzaObjects1.length = 0;
gdjs.GameCode.GDzaObjects2.length = 0;
gdjs.GameCode.GDzaObjects3.length = 0;
gdjs.GameCode.GDsilaba1Objects1.length = 0;
gdjs.GameCode.GDsilaba1Objects2.length = 0;
gdjs.GameCode.GDsilaba1Objects3.length = 0;
gdjs.GameCode.GDsilaba2Objects1.length = 0;
gdjs.GameCode.GDsilaba2Objects2.length = 0;
gdjs.GameCode.GDsilaba2Objects3.length = 0;
gdjs.GameCode.GDsilaba3Objects1.length = 0;
gdjs.GameCode.GDsilaba3Objects2.length = 0;
gdjs.GameCode.GDsilaba3Objects3.length = 0;
gdjs.GameCode.GDsilaba4Objects1.length = 0;
gdjs.GameCode.GDsilaba4Objects2.length = 0;
gdjs.GameCode.GDsilaba4Objects3.length = 0;
gdjs.GameCode.GDsilaba5Objects1.length = 0;
gdjs.GameCode.GDsilaba5Objects2.length = 0;
gdjs.GameCode.GDsilaba5Objects3.length = 0;
gdjs.GameCode.GDsilaba6Objects1.length = 0;
gdjs.GameCode.GDsilaba6Objects2.length = 0;
gdjs.GameCode.GDsilaba6Objects3.length = 0;
gdjs.GameCode.GDcolisor_95playerObjects1.length = 0;
gdjs.GameCode.GDcolisor_95playerObjects2.length = 0;
gdjs.GameCode.GDcolisor_95playerObjects3.length = 0;

gdjs.GameCode.eventsList0xaf9c0(runtimeScene, context);return;
}
gdjs['GameCode']= gdjs.GameCode;
