gdjs.TransicaoCode = {};
gdjs.TransicaoCode.GDplayerObjects1= [];
gdjs.TransicaoCode.GDplayerObjects2= [];
gdjs.TransicaoCode.GDplayerObjects3= [];
gdjs.TransicaoCode.GDskyObjects1= [];
gdjs.TransicaoCode.GDskyObjects2= [];
gdjs.TransicaoCode.GDskyObjects3= [];
gdjs.TransicaoCode.GDsilaba1Objects1= [];
gdjs.TransicaoCode.GDsilaba1Objects2= [];
gdjs.TransicaoCode.GDsilaba1Objects3= [];
gdjs.TransicaoCode.GDsilaba2Objects1= [];
gdjs.TransicaoCode.GDsilaba2Objects2= [];
gdjs.TransicaoCode.GDsilaba2Objects3= [];
gdjs.TransicaoCode.GDsilaba3Objects1= [];
gdjs.TransicaoCode.GDsilaba3Objects2= [];
gdjs.TransicaoCode.GDsilaba3Objects3= [];
gdjs.TransicaoCode.GDsilaba4Objects1= [];
gdjs.TransicaoCode.GDsilaba4Objects2= [];
gdjs.TransicaoCode.GDsilaba4Objects3= [];
gdjs.TransicaoCode.GDsilaba5Objects1= [];
gdjs.TransicaoCode.GDsilaba5Objects2= [];
gdjs.TransicaoCode.GDsilaba5Objects3= [];
gdjs.TransicaoCode.GDsilaba6Objects1= [];
gdjs.TransicaoCode.GDsilaba6Objects2= [];
gdjs.TransicaoCode.GDsilaba6Objects3= [];
gdjs.TransicaoCode.GDcolisor_95playerObjects1= [];
gdjs.TransicaoCode.GDcolisor_95playerObjects2= [];
gdjs.TransicaoCode.GDcolisor_95playerObjects3= [];
gdjs.TransicaoCode.GDtxtSilaba1Objects1= [];
gdjs.TransicaoCode.GDtxtSilaba1Objects2= [];
gdjs.TransicaoCode.GDtxtSilaba1Objects3= [];
gdjs.TransicaoCode.GDtxtSilaba2Objects1= [];
gdjs.TransicaoCode.GDtxtSilaba2Objects2= [];
gdjs.TransicaoCode.GDtxtSilaba2Objects3= [];
gdjs.TransicaoCode.GDtxtSilaba3Objects1= [];
gdjs.TransicaoCode.GDtxtSilaba3Objects2= [];
gdjs.TransicaoCode.GDtxtSilaba3Objects3= [];
gdjs.TransicaoCode.GDtxtSilaba4Objects1= [];
gdjs.TransicaoCode.GDtxtSilaba4Objects2= [];
gdjs.TransicaoCode.GDtxtSilaba4Objects3= [];
gdjs.TransicaoCode.GDtxtSilaba5Objects1= [];
gdjs.TransicaoCode.GDtxtSilaba5Objects2= [];
gdjs.TransicaoCode.GDtxtSilaba5Objects3= [];
gdjs.TransicaoCode.GDtxtSilaba6Objects1= [];
gdjs.TransicaoCode.GDtxtSilaba6Objects2= [];
gdjs.TransicaoCode.GDtxtSilaba6Objects3= [];
gdjs.TransicaoCode.GDmsgObjects1= [];
gdjs.TransicaoCode.GDmsgObjects2= [];
gdjs.TransicaoCode.GDmsgObjects3= [];
gdjs.TransicaoCode.GDtimerObjects1= [];
gdjs.TransicaoCode.GDtimerObjects2= [];
gdjs.TransicaoCode.GDtimerObjects3= [];

gdjs.TransicaoCode.conditionTrue_0 = {val:false};
gdjs.TransicaoCode.condition0IsTrue_0 = {val:false};
gdjs.TransicaoCode.condition1IsTrue_0 = {val:false};


gdjs.TransicaoCode.eventsList0x650168 = function(runtimeScene, context) {

{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "BA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("BA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("BE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("BI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("BO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("BU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("BÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "CA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("CA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("CE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("CI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("CO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("CU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("CÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "DA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("DA");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("DI");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("DE");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("DO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("DU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("DÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "FA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("FA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("FE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("FI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("FO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("FU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("FÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "GA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("GA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("GE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("GI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("GO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("GU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("GÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "HA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("HA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("HE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("HI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("HO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("HU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("HÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "JA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("JA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("JE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("JI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("JO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("JU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("JÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "LA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("LA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("LE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("LI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("LO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("LU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("LÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "MA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("MA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("ME");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("MI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("MO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("MU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("MÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "NA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("NA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("NE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("NI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("NO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("NU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("NÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "PA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("PA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("PE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("PI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("PO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("PU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("PÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "QUA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
gdjs.TransicaoCode.GDtxtSilaba1Objects2.createFrom(runtimeScene.getObjects("txtSilaba1"));
gdjs.TransicaoCode.GDtxtSilaba2Objects2.createFrom(runtimeScene.getObjects("txtSilaba2"));
gdjs.TransicaoCode.GDtxtSilaba3Objects2.createFrom(runtimeScene.getObjects("txtSilaba3"));
gdjs.TransicaoCode.GDtxtSilaba4Objects2.createFrom(runtimeScene.getObjects("txtSilaba4"));
gdjs.TransicaoCode.GDtxtSilaba5Objects2.createFrom(runtimeScene.getObjects("txtSilaba5"));
gdjs.TransicaoCode.GDtxtSilaba6Objects2.createFrom(runtimeScene.getObjects("txtSilaba6"));
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("QUA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("QUE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("QUI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("QUO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("QUÃO");
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba1Objects2.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba1Objects2[i].setCharacterSize(20);
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba2Objects2.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba2Objects2[i].setCharacterSize(20);
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba3Objects2.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba3Objects2[i].setCharacterSize(20);
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba4Objects2.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba4Objects2[i].setCharacterSize(20);
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba5Objects2.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba5Objects2[i].setCharacterSize(20);
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba6Objects2.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba6Objects2[i].setCharacterSize(16);
}
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "RA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("RA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("RE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("RI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("RO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("RU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("RÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "SA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("SA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("SE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("SI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("SO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("SU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("SÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "TA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("TA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("TE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("TI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("TO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("TU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("TÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "VA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("VA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("VE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("VI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("VO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("VU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("VÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "XA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("XA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("XE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("XI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("XO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("XU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("XÃO");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) == "ZA";
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setString("ZA");
}{runtimeScene.getGame().getVariables().getFromIndex(2).setString("ZE");
}{runtimeScene.getGame().getVariables().getFromIndex(3).setString("ZI");
}{runtimeScene.getGame().getVariables().getFromIndex(4).setString("ZO");
}{runtimeScene.getGame().getVariables().getFromIndex(5).setString("ZU");
}{runtimeScene.getGame().getVariables().getFromIndex(6).setString("ZÃO");
}}

}


}; //End of gdjs.TransicaoCode.eventsList0x650168
gdjs.TransicaoCode.eventsList0x7533a0 = function(runtimeScene, context) {

{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9)) == 0;
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "chipquest.ogg", false, 100, 1);
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9)) == 2;
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "jingles_NES00.ogg", false, 100, 1);
}}

}


}; //End of gdjs.TransicaoCode.eventsList0x7533a0
gdjs.TransicaoCode.eventsList0x7547f8 = function(runtimeScene, context) {

{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9)) == 0;
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Familias", false);
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9)) == 1;
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9)) == 2;
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.TransicaoCode.eventsList0x7547f8
gdjs.TransicaoCode.eventsList0xaf9c0 = function(runtimeScene, context) {

{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "Desert Theme.ogg", true, 50, 1);
}{gdjs.evtTools.sound.playSound(runtimeScene, "bird_flap.ogg", true, 100, 1.5);
}
{ //Subevents
gdjs.TransicaoCode.eventsList0x650168(runtimeScene, context);} //End of subevents
}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
gdjs.TransicaoCode.GDmsgObjects1.createFrom(runtimeScene.getObjects("msg"));
gdjs.TransicaoCode.GDsilaba1Objects1.createFrom(runtimeScene.getObjects("silaba1"));
gdjs.TransicaoCode.GDsilaba2Objects1.createFrom(runtimeScene.getObjects("silaba2"));
gdjs.TransicaoCode.GDsilaba3Objects1.createFrom(runtimeScene.getObjects("silaba3"));
gdjs.TransicaoCode.GDsilaba4Objects1.createFrom(runtimeScene.getObjects("silaba4"));
gdjs.TransicaoCode.GDsilaba5Objects1.createFrom(runtimeScene.getObjects("silaba5"));
gdjs.TransicaoCode.GDsilaba6Objects1.createFrom(runtimeScene.getObjects("silaba6"));
gdjs.TransicaoCode.GDtxtSilaba1Objects1.createFrom(runtimeScene.getObjects("txtSilaba1"));
gdjs.TransicaoCode.GDtxtSilaba2Objects1.createFrom(runtimeScene.getObjects("txtSilaba2"));
gdjs.TransicaoCode.GDtxtSilaba3Objects1.createFrom(runtimeScene.getObjects("txtSilaba3"));
gdjs.TransicaoCode.GDtxtSilaba4Objects1.createFrom(runtimeScene.getObjects("txtSilaba4"));
gdjs.TransicaoCode.GDtxtSilaba5Objects1.createFrom(runtimeScene.getObjects("txtSilaba5"));
gdjs.TransicaoCode.GDtxtSilaba6Objects1.createFrom(runtimeScene.getObjects("txtSilaba6"));
{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba1Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba1Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba2Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba2Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)));
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba3Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba3Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(3)));
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba4Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba4Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(4)));
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba5Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba5Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(5)));
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba6Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba6Objects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(6)));
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDmsgObjects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDmsgObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(7)));
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDmsgObjects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDmsgObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.TransicaoCode.GDmsgObjects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba1Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba1Objects1[i].setX((( gdjs.TransicaoCode.GDsilaba1Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba1Objects1[0].getPointX("")) + (( gdjs.TransicaoCode.GDsilaba1Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba1Objects1[0].getWidth())/2 - (gdjs.TransicaoCode.GDtxtSilaba1Objects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba2Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba2Objects1[i].setX((( gdjs.TransicaoCode.GDsilaba2Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba2Objects1[0].getPointX("")) + (( gdjs.TransicaoCode.GDsilaba2Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba2Objects1[0].getWidth())/2 - (gdjs.TransicaoCode.GDtxtSilaba2Objects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba3Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba3Objects1[i].setX((( gdjs.TransicaoCode.GDsilaba3Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba3Objects1[0].getPointX("")) + (( gdjs.TransicaoCode.GDsilaba3Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba3Objects1[0].getWidth())/2 - (gdjs.TransicaoCode.GDtxtSilaba3Objects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba4Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba4Objects1[i].setX((( gdjs.TransicaoCode.GDsilaba4Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba4Objects1[0].getPointX("")) + (( gdjs.TransicaoCode.GDsilaba4Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba4Objects1[0].getWidth())/2 - (gdjs.TransicaoCode.GDtxtSilaba4Objects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba5Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba5Objects1[i].setX((( gdjs.TransicaoCode.GDsilaba5Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba5Objects1[0].getPointX("")) + (( gdjs.TransicaoCode.GDsilaba5Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba5Objects1[0].getWidth())/2 - (gdjs.TransicaoCode.GDtxtSilaba5Objects1[i].getWidth())/2);
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtxtSilaba6Objects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtxtSilaba6Objects1[i].setX((( gdjs.TransicaoCode.GDsilaba6Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba6Objects1[0].getPointX("")) + (( gdjs.TransicaoCode.GDsilaba6Objects1.length === 0 ) ? 0 :gdjs.TransicaoCode.GDsilaba6Objects1[0].getWidth())/2 - (gdjs.TransicaoCode.GDtxtSilaba6Objects1[i].getWidth())/2);
}
}
{ //Subevents
gdjs.TransicaoCode.eventsList0x7533a0(runtimeScene, context);} //End of subevents
}

}


{


{
gdjs.TransicaoCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));
{runtimeScene.getVariables().getFromIndex(0).add(50*gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene));
}{for(var i = 0, len = gdjs.TransicaoCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDplayerObjects1[i].putAround(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2, gdjs.evtTools.window.getCanvasHeight(runtimeScene)/2, 100, gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDplayerObjects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDplayerObjects1[i].rotate(90, runtimeScene);
}
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 10, "back_game");
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.TransicaoCode.eventsList0x7547f8(runtimeScene, context);} //End of subevents
}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.5, "hide");
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
gdjs.TransicaoCode.GDmsgObjects1.createFrom(runtimeScene.getObjects("msg"));
{for(var i = 0, len = gdjs.TransicaoCode.GDmsgObjects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDmsgObjects1[i].hide();
}
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 1, "show");
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
gdjs.TransicaoCode.GDmsgObjects1.createFrom(runtimeScene.getObjects("msg"));
{for(var i = 0, len = gdjs.TransicaoCode.GDmsgObjects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDmsgObjects1[i].hide(false);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "hide");
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "show");
}}

}


{


gdjs.TransicaoCode.condition0IsTrue_0.val = false;
{
gdjs.TransicaoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 1, "tempo");
}if (gdjs.TransicaoCode.condition0IsTrue_0.val) {
gdjs.TransicaoCode.GDtimerObjects1.createFrom(runtimeScene.getObjects("timer"));
{runtimeScene.getVariables().getFromIndex(1).sub(1);
}{for(var i = 0, len = gdjs.TransicaoCode.GDtimerObjects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtimerObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().getFromIndex(1)));
}
}{for(var i = 0, len = gdjs.TransicaoCode.GDtimerObjects1.length ;i < len;++i) {
    gdjs.TransicaoCode.GDtimerObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.TransicaoCode.GDtimerObjects1[i].getWidth())/2);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "tempo");
}}

}


}; //End of gdjs.TransicaoCode.eventsList0xaf9c0


gdjs.TransicaoCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.TransicaoCode.GDplayerObjects1.length = 0;
gdjs.TransicaoCode.GDplayerObjects2.length = 0;
gdjs.TransicaoCode.GDplayerObjects3.length = 0;
gdjs.TransicaoCode.GDskyObjects1.length = 0;
gdjs.TransicaoCode.GDskyObjects2.length = 0;
gdjs.TransicaoCode.GDskyObjects3.length = 0;
gdjs.TransicaoCode.GDsilaba1Objects1.length = 0;
gdjs.TransicaoCode.GDsilaba1Objects2.length = 0;
gdjs.TransicaoCode.GDsilaba1Objects3.length = 0;
gdjs.TransicaoCode.GDsilaba2Objects1.length = 0;
gdjs.TransicaoCode.GDsilaba2Objects2.length = 0;
gdjs.TransicaoCode.GDsilaba2Objects3.length = 0;
gdjs.TransicaoCode.GDsilaba3Objects1.length = 0;
gdjs.TransicaoCode.GDsilaba3Objects2.length = 0;
gdjs.TransicaoCode.GDsilaba3Objects3.length = 0;
gdjs.TransicaoCode.GDsilaba4Objects1.length = 0;
gdjs.TransicaoCode.GDsilaba4Objects2.length = 0;
gdjs.TransicaoCode.GDsilaba4Objects3.length = 0;
gdjs.TransicaoCode.GDsilaba5Objects1.length = 0;
gdjs.TransicaoCode.GDsilaba5Objects2.length = 0;
gdjs.TransicaoCode.GDsilaba5Objects3.length = 0;
gdjs.TransicaoCode.GDsilaba6Objects1.length = 0;
gdjs.TransicaoCode.GDsilaba6Objects2.length = 0;
gdjs.TransicaoCode.GDsilaba6Objects3.length = 0;
gdjs.TransicaoCode.GDcolisor_95playerObjects1.length = 0;
gdjs.TransicaoCode.GDcolisor_95playerObjects2.length = 0;
gdjs.TransicaoCode.GDcolisor_95playerObjects3.length = 0;
gdjs.TransicaoCode.GDtxtSilaba1Objects1.length = 0;
gdjs.TransicaoCode.GDtxtSilaba1Objects2.length = 0;
gdjs.TransicaoCode.GDtxtSilaba1Objects3.length = 0;
gdjs.TransicaoCode.GDtxtSilaba2Objects1.length = 0;
gdjs.TransicaoCode.GDtxtSilaba2Objects2.length = 0;
gdjs.TransicaoCode.GDtxtSilaba2Objects3.length = 0;
gdjs.TransicaoCode.GDtxtSilaba3Objects1.length = 0;
gdjs.TransicaoCode.GDtxtSilaba3Objects2.length = 0;
gdjs.TransicaoCode.GDtxtSilaba3Objects3.length = 0;
gdjs.TransicaoCode.GDtxtSilaba4Objects1.length = 0;
gdjs.TransicaoCode.GDtxtSilaba4Objects2.length = 0;
gdjs.TransicaoCode.GDtxtSilaba4Objects3.length = 0;
gdjs.TransicaoCode.GDtxtSilaba5Objects1.length = 0;
gdjs.TransicaoCode.GDtxtSilaba5Objects2.length = 0;
gdjs.TransicaoCode.GDtxtSilaba5Objects3.length = 0;
gdjs.TransicaoCode.GDtxtSilaba6Objects1.length = 0;
gdjs.TransicaoCode.GDtxtSilaba6Objects2.length = 0;
gdjs.TransicaoCode.GDtxtSilaba6Objects3.length = 0;
gdjs.TransicaoCode.GDmsgObjects1.length = 0;
gdjs.TransicaoCode.GDmsgObjects2.length = 0;
gdjs.TransicaoCode.GDmsgObjects3.length = 0;
gdjs.TransicaoCode.GDtimerObjects1.length = 0;
gdjs.TransicaoCode.GDtimerObjects2.length = 0;
gdjs.TransicaoCode.GDtimerObjects3.length = 0;

gdjs.TransicaoCode.eventsList0xaf9c0(runtimeScene, context);return;
}
gdjs['TransicaoCode']= gdjs.TransicaoCode;
