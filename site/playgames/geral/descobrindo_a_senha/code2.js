gdjs.EndCode = {};
gdjs.EndCode.GDbackgroundObjects1= [];
gdjs.EndCode.GDbackgroundObjects2= [];
gdjs.EndCode.GDbackgroundObjects3= [];
gdjs.EndCode.GDendObjects1= [];
gdjs.EndCode.GDendObjects2= [];
gdjs.EndCode.GDendObjects3= [];
gdjs.EndCode.GDlblSenhaObjects1= [];
gdjs.EndCode.GDlblSenhaObjects2= [];
gdjs.EndCode.GDlblSenhaObjects3= [];
gdjs.EndCode.GDlblStartObjects1= [];
gdjs.EndCode.GDlblStartObjects2= [];
gdjs.EndCode.GDlblStartObjects3= [];
gdjs.EndCode.GDlblTempoObjects1= [];
gdjs.EndCode.GDlblTempoObjects2= [];
gdjs.EndCode.GDlblTempoObjects3= [];
gdjs.EndCode.GDlblTempoMinutosObjects1= [];
gdjs.EndCode.GDlblTempoMinutosObjects2= [];
gdjs.EndCode.GDlblTempoMinutosObjects3= [];
gdjs.EndCode.GDlblTempoSegundosObjects1= [];
gdjs.EndCode.GDlblTempoSegundosObjects2= [];
gdjs.EndCode.GDlblTempoSegundosObjects3= [];
gdjs.EndCode.GDlblTempoSeparadorObjects1= [];
gdjs.EndCode.GDlblTempoSeparadorObjects2= [];
gdjs.EndCode.GDlblTempoSeparadorObjects3= [];

gdjs.EndCode.conditionTrue_0 = {val:false};
gdjs.EndCode.condition0IsTrue_0 = {val:false};
gdjs.EndCode.condition1IsTrue_0 = {val:false};
gdjs.EndCode.condition2IsTrue_0 = {val:false};


gdjs.EndCode.eventsList0x5c23d0 = function(runtimeScene, context) {

{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) < 10;
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDlblTempoMinutosObjects2.createFrom(runtimeScene.getObjects("lblTempoMinutos"));
{for(var i = 0, len = gdjs.EndCode.GDlblTempoMinutosObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDlblTempoMinutosObjects2[i].setString("0" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(6)));
}
}}

}


{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) >= 10;
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDlblTempoMinutosObjects2.createFrom(runtimeScene.getObjects("lblTempoMinutos"));
{for(var i = 0, len = gdjs.EndCode.GDlblTempoMinutosObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDlblTempoMinutosObjects2[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(6)));
}
}}

}


{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) < 10;
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDlblTempoSegundosObjects2.createFrom(runtimeScene.getObjects("lblTempoSegundos"));
{for(var i = 0, len = gdjs.EndCode.GDlblTempoSegundosObjects2.length ;i < len;++i) {
    gdjs.EndCode.GDlblTempoSegundosObjects2[i].setString("0" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(5)));
}
}}

}


{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) >= 10;
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDlblTempoSegundosObjects1.createFrom(runtimeScene.getObjects("lblTempoSegundos"));
{for(var i = 0, len = gdjs.EndCode.GDlblTempoSegundosObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDlblTempoSegundosObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(5)));
}
}}

}


}; //End of gdjs.EndCode.eventsList0x5c23d0
gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDlblStartObjects1Objects = Hashtable.newFrom({"lblStart": gdjs.EndCode.GDlblStartObjects1});gdjs.EndCode.eventsList0xa86e0 = function(runtimeScene, context) {

{


gdjs.EndCode.condition0IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.EndCode.condition0IsTrue_0.val) {
gdjs.EndCode.GDlblSenhaObjects1.createFrom(runtimeScene.getObjects("lblSenha"));
{for(var i = 0, len = gdjs.EndCode.GDlblSenhaObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDlblSenhaObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)) + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(2)) + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(3)));
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "0001.ogg", false, 100, 1);
}
{ //Subevents
gdjs.EndCode.eventsList0x5c23d0(runtimeScene, context);} //End of subevents
}

}


{

gdjs.EndCode.GDlblStartObjects1.createFrom(runtimeScene.getObjects("lblStart"));

gdjs.EndCode.condition0IsTrue_0.val = false;
gdjs.EndCode.condition1IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.5, "piscar");
}if ( gdjs.EndCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.EndCode.GDlblStartObjects1.length;i<l;++i) {
    if ( gdjs.EndCode.GDlblStartObjects1[i].isVisible() ) {
        gdjs.EndCode.condition1IsTrue_0.val = true;
        gdjs.EndCode.GDlblStartObjects1[k] = gdjs.EndCode.GDlblStartObjects1[i];
        ++k;
    }
}
gdjs.EndCode.GDlblStartObjects1.length = k;}}
if (gdjs.EndCode.condition1IsTrue_0.val) {
/* Reuse gdjs.EndCode.GDlblStartObjects1 */
{for(var i = 0, len = gdjs.EndCode.GDlblStartObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDlblStartObjects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "piscar");
}}

}


{

gdjs.EndCode.GDlblStartObjects1.createFrom(runtimeScene.getObjects("lblStart"));

gdjs.EndCode.condition0IsTrue_0.val = false;
gdjs.EndCode.condition1IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.5, "piscar");
}if ( gdjs.EndCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.EndCode.GDlblStartObjects1.length;i<l;++i) {
    if ( !(gdjs.EndCode.GDlblStartObjects1[i].isVisible()) ) {
        gdjs.EndCode.condition1IsTrue_0.val = true;
        gdjs.EndCode.GDlblStartObjects1[k] = gdjs.EndCode.GDlblStartObjects1[i];
        ++k;
    }
}
gdjs.EndCode.GDlblStartObjects1.length = k;}}
if (gdjs.EndCode.condition1IsTrue_0.val) {
/* Reuse gdjs.EndCode.GDlblStartObjects1 */
{for(var i = 0, len = gdjs.EndCode.GDlblStartObjects1.length ;i < len;++i) {
    gdjs.EndCode.GDlblStartObjects1[i].hide(false);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "piscar");
}}

}


{

gdjs.EndCode.GDlblStartObjects1.createFrom(runtimeScene.getObjects("lblStart"));

gdjs.EndCode.condition0IsTrue_0.val = false;
gdjs.EndCode.condition1IsTrue_0.val = false;
{
gdjs.EndCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.EndCode.condition0IsTrue_0.val ) {
{
gdjs.EndCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.EndCode.mapOfGDgdjs_46EndCode_46GDlblStartObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.EndCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


}; //End of gdjs.EndCode.eventsList0xa86e0


gdjs.EndCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.EndCode.GDbackgroundObjects1.length = 0;
gdjs.EndCode.GDbackgroundObjects2.length = 0;
gdjs.EndCode.GDbackgroundObjects3.length = 0;
gdjs.EndCode.GDendObjects1.length = 0;
gdjs.EndCode.GDendObjects2.length = 0;
gdjs.EndCode.GDendObjects3.length = 0;
gdjs.EndCode.GDlblSenhaObjects1.length = 0;
gdjs.EndCode.GDlblSenhaObjects2.length = 0;
gdjs.EndCode.GDlblSenhaObjects3.length = 0;
gdjs.EndCode.GDlblStartObjects1.length = 0;
gdjs.EndCode.GDlblStartObjects2.length = 0;
gdjs.EndCode.GDlblStartObjects3.length = 0;
gdjs.EndCode.GDlblTempoObjects1.length = 0;
gdjs.EndCode.GDlblTempoObjects2.length = 0;
gdjs.EndCode.GDlblTempoObjects3.length = 0;
gdjs.EndCode.GDlblTempoMinutosObjects1.length = 0;
gdjs.EndCode.GDlblTempoMinutosObjects2.length = 0;
gdjs.EndCode.GDlblTempoMinutosObjects3.length = 0;
gdjs.EndCode.GDlblTempoSegundosObjects1.length = 0;
gdjs.EndCode.GDlblTempoSegundosObjects2.length = 0;
gdjs.EndCode.GDlblTempoSegundosObjects3.length = 0;
gdjs.EndCode.GDlblTempoSeparadorObjects1.length = 0;
gdjs.EndCode.GDlblTempoSeparadorObjects2.length = 0;
gdjs.EndCode.GDlblTempoSeparadorObjects3.length = 0;

gdjs.EndCode.eventsList0xa86e0(runtimeScene, context);return;
}
gdjs['EndCode']= gdjs.EndCode;
