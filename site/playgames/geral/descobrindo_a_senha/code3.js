gdjs.RulesCode = {};
gdjs.RulesCode.GDbackgroundObjects1= [];
gdjs.RulesCode.GDbackgroundObjects2= [];
gdjs.RulesCode.GDregrasObjects1= [];
gdjs.RulesCode.GDregrasObjects2= [];
gdjs.RulesCode.GDlblVoltarObjects1= [];
gdjs.RulesCode.GDlblVoltarObjects2= [];

gdjs.RulesCode.conditionTrue_0 = {val:false};
gdjs.RulesCode.condition0IsTrue_0 = {val:false};
gdjs.RulesCode.condition1IsTrue_0 = {val:false};
gdjs.RulesCode.condition2IsTrue_0 = {val:false};


gdjs.RulesCode.mapOfGDgdjs_46RulesCode_46GDlblVoltarObjects1Objects = Hashtable.newFrom({"lblVoltar": gdjs.RulesCode.GDlblVoltarObjects1});gdjs.RulesCode.eventsList0xa86e0 = function(runtimeScene, context) {

{

gdjs.RulesCode.GDlblVoltarObjects1.createFrom(runtimeScene.getObjects("lblVoltar"));

gdjs.RulesCode.condition0IsTrue_0.val = false;
gdjs.RulesCode.condition1IsTrue_0.val = false;
{
gdjs.RulesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.RulesCode.condition0IsTrue_0.val ) {
{
gdjs.RulesCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.RulesCode.mapOfGDgdjs_46RulesCode_46GDlblVoltarObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.RulesCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


{

gdjs.RulesCode.GDlblVoltarObjects1.createFrom(runtimeScene.getObjects("lblVoltar"));

gdjs.RulesCode.condition0IsTrue_0.val = false;
gdjs.RulesCode.condition1IsTrue_0.val = false;
{
gdjs.RulesCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.5, "piscar");
}if ( gdjs.RulesCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.RulesCode.GDlblVoltarObjects1.length;i<l;++i) {
    if ( gdjs.RulesCode.GDlblVoltarObjects1[i].isVisible() ) {
        gdjs.RulesCode.condition1IsTrue_0.val = true;
        gdjs.RulesCode.GDlblVoltarObjects1[k] = gdjs.RulesCode.GDlblVoltarObjects1[i];
        ++k;
    }
}
gdjs.RulesCode.GDlblVoltarObjects1.length = k;}}
if (gdjs.RulesCode.condition1IsTrue_0.val) {
/* Reuse gdjs.RulesCode.GDlblVoltarObjects1 */
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "piscar");
}{for(var i = 0, len = gdjs.RulesCode.GDlblVoltarObjects1.length ;i < len;++i) {
    gdjs.RulesCode.GDlblVoltarObjects1[i].hide();
}
}}

}


{

gdjs.RulesCode.GDlblVoltarObjects1.createFrom(runtimeScene.getObjects("lblVoltar"));

gdjs.RulesCode.condition0IsTrue_0.val = false;
gdjs.RulesCode.condition1IsTrue_0.val = false;
{
gdjs.RulesCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.5, "piscar");
}if ( gdjs.RulesCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.RulesCode.GDlblVoltarObjects1.length;i<l;++i) {
    if ( !(gdjs.RulesCode.GDlblVoltarObjects1[i].isVisible()) ) {
        gdjs.RulesCode.condition1IsTrue_0.val = true;
        gdjs.RulesCode.GDlblVoltarObjects1[k] = gdjs.RulesCode.GDlblVoltarObjects1[i];
        ++k;
    }
}
gdjs.RulesCode.GDlblVoltarObjects1.length = k;}}
if (gdjs.RulesCode.condition1IsTrue_0.val) {
/* Reuse gdjs.RulesCode.GDlblVoltarObjects1 */
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "piscar");
}{for(var i = 0, len = gdjs.RulesCode.GDlblVoltarObjects1.length ;i < len;++i) {
    gdjs.RulesCode.GDlblVoltarObjects1[i].hide(false);
}
}}

}


}; //End of gdjs.RulesCode.eventsList0xa86e0


gdjs.RulesCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.RulesCode.GDbackgroundObjects1.length = 0;
gdjs.RulesCode.GDbackgroundObjects2.length = 0;
gdjs.RulesCode.GDregrasObjects1.length = 0;
gdjs.RulesCode.GDregrasObjects2.length = 0;
gdjs.RulesCode.GDlblVoltarObjects1.length = 0;
gdjs.RulesCode.GDlblVoltarObjects2.length = 0;

gdjs.RulesCode.eventsList0xa86e0(runtimeScene, context);return;
}
gdjs['RulesCode']= gdjs.RulesCode;
