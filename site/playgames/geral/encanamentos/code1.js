gdjs.GameCode = {};
gdjs.GameCode.GDcano10Objects1_1final = [];

gdjs.GameCode.GDcano11Objects1_1final = [];

gdjs.GameCode.GDcano1Objects1_1final = [];

gdjs.GameCode.GDcano2Objects1_1final = [];

gdjs.GameCode.GDcano3Objects1_1final = [];

gdjs.GameCode.GDcano4Objects1_1final = [];

gdjs.GameCode.GDcano5Objects1_1final = [];

gdjs.GameCode.GDcano6Objects1_1final = [];

gdjs.GameCode.GDcano7Objects1_1final = [];

gdjs.GameCode.GDcano9Objects1_1final = [];

gdjs.GameCode.GDtopObjects1_1final = [];

gdjs.GameCode.GDvaziosObjects1_1final = [];

gdjs.GameCode.forEachCount0_2 = 0;

gdjs.GameCode.forEachCount1_2 = 0;

gdjs.GameCode.forEachCount2_2 = 0;

gdjs.GameCode.forEachCount3_2 = 0;

gdjs.GameCode.forEachCount4_2 = 0;

gdjs.GameCode.forEachCount5_2 = 0;

gdjs.GameCode.forEachCount6_2 = 0;

gdjs.GameCode.forEachCount7_2 = 0;

gdjs.GameCode.forEachCount8_2 = 0;

gdjs.GameCode.forEachCount9_2 = 0;

gdjs.GameCode.forEachIndex2 = 0;

gdjs.GameCode.forEachObjects2 = [];

gdjs.GameCode.forEachTotalCount2 = 0;

gdjs.GameCode.GDbackgroundObjects1= [];
gdjs.GameCode.GDbackgroundObjects2= [];
gdjs.GameCode.GDbackgroundObjects3= [];
gdjs.GameCode.GDcano1Objects1= [];
gdjs.GameCode.GDcano1Objects2= [];
gdjs.GameCode.GDcano1Objects3= [];
gdjs.GameCode.GDcano2Objects1= [];
gdjs.GameCode.GDcano2Objects2= [];
gdjs.GameCode.GDcano2Objects3= [];
gdjs.GameCode.GDcano11Objects1= [];
gdjs.GameCode.GDcano11Objects2= [];
gdjs.GameCode.GDcano11Objects3= [];
gdjs.GameCode.GDcano3Objects1= [];
gdjs.GameCode.GDcano3Objects2= [];
gdjs.GameCode.GDcano3Objects3= [];
gdjs.GameCode.GDcano4Objects1= [];
gdjs.GameCode.GDcano4Objects2= [];
gdjs.GameCode.GDcano4Objects3= [];
gdjs.GameCode.GDcano10Objects1= [];
gdjs.GameCode.GDcano10Objects2= [];
gdjs.GameCode.GDcano10Objects3= [];
gdjs.GameCode.GDcano5Objects1= [];
gdjs.GameCode.GDcano5Objects2= [];
gdjs.GameCode.GDcano5Objects3= [];
gdjs.GameCode.GDcano6Objects1= [];
gdjs.GameCode.GDcano6Objects2= [];
gdjs.GameCode.GDcano6Objects3= [];
gdjs.GameCode.GDcano7Objects1= [];
gdjs.GameCode.GDcano7Objects2= [];
gdjs.GameCode.GDcano7Objects3= [];
gdjs.GameCode.GDcano9Objects1= [];
gdjs.GameCode.GDcano9Objects2= [];
gdjs.GameCode.GDcano9Objects3= [];
gdjs.GameCode.GDvaziosObjects1= [];
gdjs.GameCode.GDvaziosObjects2= [];
gdjs.GameCode.GDvaziosObjects3= [];
gdjs.GameCode.GDalavancaObjects1= [];
gdjs.GameCode.GDalavancaObjects2= [];
gdjs.GameCode.GDalavancaObjects3= [];
gdjs.GameCode.GDbaseObjects1= [];
gdjs.GameCode.GDbaseObjects2= [];
gdjs.GameCode.GDbaseObjects3= [];
gdjs.GameCode.GDbandeiraObjects1= [];
gdjs.GameCode.GDbandeiraObjects2= [];
gdjs.GameCode.GDbandeiraObjects3= [];
gdjs.GameCode.GDcanoInicioObjects1= [];
gdjs.GameCode.GDcanoInicioObjects2= [];
gdjs.GameCode.GDcanoInicioObjects3= [];
gdjs.GameCode.GDcanoFinalObjects1= [];
gdjs.GameCode.GDcanoFinalObjects2= [];
gdjs.GameCode.GDcanoFinalObjects3= [];
gdjs.GameCode.GDboxObjects1= [];
gdjs.GameCode.GDboxObjects2= [];
gdjs.GameCode.GDboxObjects3= [];
gdjs.GameCode.GDgalaoObjects1= [];
gdjs.GameCode.GDgalaoObjects2= [];
gdjs.GameCode.GDgalaoObjects3= [];
gdjs.GameCode.GDDialogObjects1= [];
gdjs.GameCode.GDDialogObjects2= [];
gdjs.GameCode.GDDialogObjects3= [];
gdjs.GameCode.GDtxtAguaObjects1= [];
gdjs.GameCode.GDtxtAguaObjects2= [];
gdjs.GameCode.GDtxtAguaObjects3= [];
gdjs.GameCode.GDbtnCanoObjects1= [];
gdjs.GameCode.GDbtnCanoObjects2= [];
gdjs.GameCode.GDbtnCanoObjects3= [];
gdjs.GameCode.GDlblContinuarObjects1= [];
gdjs.GameCode.GDlblContinuarObjects2= [];
gdjs.GameCode.GDlblContinuarObjects3= [];
gdjs.GameCode.GDtopObjects1= [];
gdjs.GameCode.GDtopObjects2= [];
gdjs.GameCode.GDtopObjects3= [];
gdjs.GameCode.GDfaixaObjects1= [];
gdjs.GameCode.GDfaixaObjects2= [];
gdjs.GameCode.GDfaixaObjects3= [];

gdjs.GameCode.conditionTrue_0 = {val:false};
gdjs.GameCode.condition0IsTrue_0 = {val:false};
gdjs.GameCode.condition1IsTrue_0 = {val:false};
gdjs.GameCode.condition2IsTrue_0 = {val:false};
gdjs.GameCode.condition3IsTrue_0 = {val:false};
gdjs.GameCode.condition4IsTrue_0 = {val:false};
gdjs.GameCode.conditionTrue_1 = {val:false};
gdjs.GameCode.condition0IsTrue_1 = {val:false};
gdjs.GameCode.condition1IsTrue_1 = {val:false};
gdjs.GameCode.condition2IsTrue_1 = {val:false};
gdjs.GameCode.condition3IsTrue_1 = {val:false};
gdjs.GameCode.condition4IsTrue_1 = {val:false};


gdjs.GameCode.eventsList0x771830 = function(runtimeScene) {

{


{
gdjs.GameCode.GDbandeiraObjects1.createFrom(runtimeScene.getObjects("bandeira"));
gdjs.GameCode.GDvaziosObjects1.createFrom(runtimeScene.getObjects("vazios"));
{for(var i = 0, len = gdjs.GameCode.GDvaziosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDvaziosObjects1[i].setAnimation(gdjs.random(11));
}
}{for(var i = 0, len = gdjs.GameCode.GDbandeiraObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbandeiraObjects1[i].hide();
}
}}

}


}; //End of gdjs.GameCode.eventsList0x771830
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects1ObjectsGDgdjs_46GameCode_46GDcano2Objects1ObjectsGDgdjs_46GameCode_46GDcano3Objects1ObjectsGDgdjs_46GameCode_46GDcano4Objects1ObjectsGDgdjs_46GameCode_46GDcano5Objects1ObjectsGDgdjs_46GameCode_46GDcano6Objects1ObjectsGDgdjs_46GameCode_46GDcano7Objects1ObjectsGDgdjs_46GameCode_46GDcano9Objects1ObjectsGDgdjs_46GameCode_46GDcano10Objects1ObjectsGDgdjs_46GameCode_46GDcano11Objects1Objects = Hashtable.newFrom({"cano1": gdjs.GameCode.GDcano1Objects1, "cano2": gdjs.GameCode.GDcano2Objects1, "cano3": gdjs.GameCode.GDcano3Objects1, "cano4": gdjs.GameCode.GDcano4Objects1, "cano5": gdjs.GameCode.GDcano5Objects1, "cano6": gdjs.GameCode.GDcano6Objects1, "cano7": gdjs.GameCode.GDcano7Objects1, "cano9": gdjs.GameCode.GDcano9Objects1, "cano10": gdjs.GameCode.GDcano10Objects1, "cano11": gdjs.GameCode.GDcano11Objects1});gdjs.GameCode.eventsList0x773420 = function(runtimeScene) {

{

/* Reuse gdjs.GameCode.GDcano1Objects1 */
/* Reuse gdjs.GameCode.GDcano10Objects1 */
/* Reuse gdjs.GameCode.GDcano11Objects1 */
/* Reuse gdjs.GameCode.GDcano2Objects1 */
/* Reuse gdjs.GameCode.GDcano3Objects1 */
/* Reuse gdjs.GameCode.GDcano4Objects1 */
/* Reuse gdjs.GameCode.GDcano5Objects1 */
/* Reuse gdjs.GameCode.GDcano6Objects1 */
/* Reuse gdjs.GameCode.GDcano7Objects1 */
/* Reuse gdjs.GameCode.GDcano9Objects1 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcano1Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano1Objects1[i].getAngle() >= 360 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano1Objects1[k] = gdjs.GameCode.GDcano1Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano1Objects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano2Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano2Objects1[i].getAngle() >= 360 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano2Objects1[k] = gdjs.GameCode.GDcano2Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano2Objects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano3Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano3Objects1[i].getAngle() >= 360 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano3Objects1[k] = gdjs.GameCode.GDcano3Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano3Objects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano4Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano4Objects1[i].getAngle() >= 360 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano4Objects1[k] = gdjs.GameCode.GDcano4Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano4Objects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano5Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano5Objects1[i].getAngle() >= 360 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano5Objects1[k] = gdjs.GameCode.GDcano5Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano5Objects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano6Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano6Objects1[i].getAngle() >= 360 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano6Objects1[k] = gdjs.GameCode.GDcano6Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano6Objects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano7Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano7Objects1[i].getAngle() >= 360 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano7Objects1[k] = gdjs.GameCode.GDcano7Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano7Objects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano9Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano9Objects1[i].getAngle() >= 360 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano9Objects1[k] = gdjs.GameCode.GDcano9Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano9Objects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano10Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano10Objects1[i].getAngle() >= 360 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano10Objects1[k] = gdjs.GameCode.GDcano10Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano10Objects1.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano11Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano11Objects1[i].getAngle() >= 360 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano11Objects1[k] = gdjs.GameCode.GDcano11Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano11Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcano1Objects1 */
/* Reuse gdjs.GameCode.GDcano10Objects1 */
/* Reuse gdjs.GameCode.GDcano11Objects1 */
/* Reuse gdjs.GameCode.GDcano2Objects1 */
/* Reuse gdjs.GameCode.GDcano3Objects1 */
/* Reuse gdjs.GameCode.GDcano4Objects1 */
/* Reuse gdjs.GameCode.GDcano5Objects1 */
/* Reuse gdjs.GameCode.GDcano6Objects1 */
/* Reuse gdjs.GameCode.GDcano7Objects1 */
/* Reuse gdjs.GameCode.GDcano9Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDcano1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano1Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano2Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano3Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano4Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano5Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano5Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano6Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano6Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano7Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano7Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano9Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano9Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano10Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano10Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano11Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano11Objects1[i].setAngle(0);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x773420
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDvaziosObjects1Objects = Hashtable.newFrom({"vazios": gdjs.GameCode.GDvaziosObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalavancaObjects1Objects = Hashtable.newFrom({"alavanca": gdjs.GameCode.GDalavancaObjects1});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlblContinuarObjects1Objects = Hashtable.newFrom({"lblContinuar": gdjs.GameCode.GDlblContinuarObjects1});gdjs.GameCode.eventsList0x774518 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1));
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1));
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.GameCode.eventsList0x774518
gdjs.GameCode.eventsList0x774f68 = function(runtimeScene) {

}; //End of gdjs.GameCode.eventsList0x774f68
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects2ObjectsGDgdjs_46GameCode_46GDcano2Objects2ObjectsGDgdjs_46GameCode_46GDcano3Objects2ObjectsGDgdjs_46GameCode_46GDcano4Objects2ObjectsGDgdjs_46GameCode_46GDcano5Objects2ObjectsGDgdjs_46GameCode_46GDcano6Objects2ObjectsGDgdjs_46GameCode_46GDcano7Objects2ObjectsGDgdjs_46GameCode_46GDcano9Objects2ObjectsGDgdjs_46GameCode_46GDcano10Objects2ObjectsGDgdjs_46GameCode_46GDcano11Objects2Objects = Hashtable.newFrom({"cano1": gdjs.GameCode.GDcano1Objects2, "cano2": gdjs.GameCode.GDcano2Objects2, "cano3": gdjs.GameCode.GDcano3Objects2, "cano4": gdjs.GameCode.GDcano4Objects2, "cano5": gdjs.GameCode.GDcano5Objects2, "cano6": gdjs.GameCode.GDcano6Objects2, "cano7": gdjs.GameCode.GDcano7Objects2, "cano9": gdjs.GameCode.GDcano9Objects2, "cano10": gdjs.GameCode.GDcano10Objects2, "cano11": gdjs.GameCode.GDcano11Objects2});gdjs.GameCode.eventsList0x775618 = function(runtimeScene) {

{

/* Reuse gdjs.GameCode.GDcano1Objects2 */
/* Reuse gdjs.GameCode.GDcano10Objects2 */
/* Reuse gdjs.GameCode.GDcano11Objects2 */
/* Reuse gdjs.GameCode.GDcano2Objects2 */
/* Reuse gdjs.GameCode.GDcano3Objects2 */
/* Reuse gdjs.GameCode.GDcano4Objects2 */
/* Reuse gdjs.GameCode.GDcano5Objects2 */
/* Reuse gdjs.GameCode.GDcano6Objects2 */
/* Reuse gdjs.GameCode.GDcano7Objects2 */
/* Reuse gdjs.GameCode.GDcano9Objects2 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcano1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano1Objects2[i].getVariableNumber(gdjs.GameCode.GDcano1Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano1Objects2[k] = gdjs.GameCode.GDcano1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano1Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano2Objects2[i].getVariableNumber(gdjs.GameCode.GDcano2Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano2Objects2[k] = gdjs.GameCode.GDcano2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano2Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano3Objects2[i].getVariableNumber(gdjs.GameCode.GDcano3Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano3Objects2[k] = gdjs.GameCode.GDcano3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano3Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano4Objects2[i].getVariableNumber(gdjs.GameCode.GDcano4Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano4Objects2[k] = gdjs.GameCode.GDcano4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano4Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano5Objects2[i].getVariableNumber(gdjs.GameCode.GDcano5Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano5Objects2[k] = gdjs.GameCode.GDcano5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano5Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano6Objects2[i].getVariableNumber(gdjs.GameCode.GDcano6Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano6Objects2[k] = gdjs.GameCode.GDcano6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano6Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano7Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano7Objects2[i].getVariableNumber(gdjs.GameCode.GDcano7Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano7Objects2[k] = gdjs.GameCode.GDcano7Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano7Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano9Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano9Objects2[i].getVariableNumber(gdjs.GameCode.GDcano9Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano9Objects2[k] = gdjs.GameCode.GDcano9Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano9Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano10Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano10Objects2[i].getVariableNumber(gdjs.GameCode.GDcano10Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano10Objects2[k] = gdjs.GameCode.GDcano10Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano10Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano11Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano11Objects2[i].getVariableNumber(gdjs.GameCode.GDcano11Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano11Objects2[k] = gdjs.GameCode.GDcano11Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano11Objects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcano1Objects2 */
/* Reuse gdjs.GameCode.GDcano10Objects2 */
/* Reuse gdjs.GameCode.GDcano11Objects2 */
/* Reuse gdjs.GameCode.GDcano2Objects2 */
/* Reuse gdjs.GameCode.GDcano3Objects2 */
/* Reuse gdjs.GameCode.GDcano4Objects2 */
/* Reuse gdjs.GameCode.GDcano5Objects2 */
/* Reuse gdjs.GameCode.GDcano6Objects2 */
/* Reuse gdjs.GameCode.GDcano7Objects2 */
/* Reuse gdjs.GameCode.GDcano9Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDcano1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano1Objects2[i].setAnimation(1);
}
for(var i = 0, len = gdjs.GameCode.GDcano2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano2Objects2[i].setAnimation(1);
}
for(var i = 0, len = gdjs.GameCode.GDcano3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano3Objects2[i].setAnimation(1);
}
for(var i = 0, len = gdjs.GameCode.GDcano4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano4Objects2[i].setAnimation(1);
}
for(var i = 0, len = gdjs.GameCode.GDcano5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano5Objects2[i].setAnimation(1);
}
for(var i = 0, len = gdjs.GameCode.GDcano6Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano6Objects2[i].setAnimation(1);
}
for(var i = 0, len = gdjs.GameCode.GDcano7Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano7Objects2[i].setAnimation(1);
}
for(var i = 0, len = gdjs.GameCode.GDcano9Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano9Objects2[i].setAnimation(1);
}
for(var i = 0, len = gdjs.GameCode.GDcano10Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano10Objects2[i].setAnimation(1);
}
for(var i = 0, len = gdjs.GameCode.GDcano11Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano11Objects2[i].setAnimation(1);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x775618
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects1ObjectsGDgdjs_46GameCode_46GDcano2Objects1ObjectsGDgdjs_46GameCode_46GDcano3Objects1ObjectsGDgdjs_46GameCode_46GDcano4Objects1ObjectsGDgdjs_46GameCode_46GDcano5Objects1ObjectsGDgdjs_46GameCode_46GDcano6Objects1ObjectsGDgdjs_46GameCode_46GDcano7Objects1ObjectsGDgdjs_46GameCode_46GDcano9Objects1ObjectsGDgdjs_46GameCode_46GDcano10Objects1ObjectsGDgdjs_46GameCode_46GDcano11Objects1Objects = Hashtable.newFrom({"cano1": gdjs.GameCode.GDcano1Objects1, "cano2": gdjs.GameCode.GDcano2Objects1, "cano3": gdjs.GameCode.GDcano3Objects1, "cano4": gdjs.GameCode.GDcano4Objects1, "cano5": gdjs.GameCode.GDcano5Objects1, "cano6": gdjs.GameCode.GDcano6Objects1, "cano7": gdjs.GameCode.GDcano7Objects1, "cano9": gdjs.GameCode.GDcano9Objects1, "cano10": gdjs.GameCode.GDcano10Objects1, "cano11": gdjs.GameCode.GDcano11Objects1});gdjs.GameCode.eventsList0x775180 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.2, "prepara_canos");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).add(1);
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "prepara_canos");
}}

}


{

gdjs.GameCode.GDcano1Objects2.createFrom(runtimeScene.getObjects("cano1"));
gdjs.GameCode.GDcano10Objects2.createFrom(runtimeScene.getObjects("cano10"));
gdjs.GameCode.GDcano11Objects2.createFrom(runtimeScene.getObjects("cano11"));
gdjs.GameCode.GDcano2Objects2.createFrom(runtimeScene.getObjects("cano2"));
gdjs.GameCode.GDcano3Objects2.createFrom(runtimeScene.getObjects("cano3"));
gdjs.GameCode.GDcano4Objects2.createFrom(runtimeScene.getObjects("cano4"));
gdjs.GameCode.GDcano5Objects2.createFrom(runtimeScene.getObjects("cano5"));
gdjs.GameCode.GDcano6Objects2.createFrom(runtimeScene.getObjects("cano6"));
gdjs.GameCode.GDcano7Objects2.createFrom(runtimeScene.getObjects("cano7"));
gdjs.GameCode.GDcano9Objects2.createFrom(runtimeScene.getObjects("cano9"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects2ObjectsGDgdjs_46GameCode_46GDcano2Objects2ObjectsGDgdjs_46GameCode_46GDcano3Objects2ObjectsGDgdjs_46GameCode_46GDcano4Objects2ObjectsGDgdjs_46GameCode_46GDcano5Objects2ObjectsGDgdjs_46GameCode_46GDcano6Objects2ObjectsGDgdjs_46GameCode_46GDcano7Objects2ObjectsGDgdjs_46GameCode_46GDcano9Objects2ObjectsGDgdjs_46GameCode_46GDcano10Objects2ObjectsGDgdjs_46GameCode_46GDcano11Objects2Objects) >= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4));
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x775618(runtimeScene);} //End of subevents
}

}


{

gdjs.GameCode.GDbandeiraObjects1.createFrom(runtimeScene.getObjects("bandeira"));
gdjs.GameCode.GDcano1Objects1.createFrom(runtimeScene.getObjects("cano1"));
gdjs.GameCode.GDcano10Objects1.createFrom(runtimeScene.getObjects("cano10"));
gdjs.GameCode.GDcano11Objects1.createFrom(runtimeScene.getObjects("cano11"));
gdjs.GameCode.GDcano2Objects1.createFrom(runtimeScene.getObjects("cano2"));
gdjs.GameCode.GDcano3Objects1.createFrom(runtimeScene.getObjects("cano3"));
gdjs.GameCode.GDcano4Objects1.createFrom(runtimeScene.getObjects("cano4"));
gdjs.GameCode.GDcano5Objects1.createFrom(runtimeScene.getObjects("cano5"));
gdjs.GameCode.GDcano6Objects1.createFrom(runtimeScene.getObjects("cano6"));
gdjs.GameCode.GDcano7Objects1.createFrom(runtimeScene.getObjects("cano7"));
gdjs.GameCode.GDcano9Objects1.createFrom(runtimeScene.getObjects("cano9"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.object.pickedObjectsCount(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects1ObjectsGDgdjs_46GameCode_46GDcano2Objects1ObjectsGDgdjs_46GameCode_46GDcano3Objects1ObjectsGDgdjs_46GameCode_46GDcano4Objects1ObjectsGDgdjs_46GameCode_46GDcano5Objects1ObjectsGDgdjs_46GameCode_46GDcano6Objects1ObjectsGDgdjs_46GameCode_46GDcano7Objects1ObjectsGDgdjs_46GameCode_46GDcano9Objects1ObjectsGDgdjs_46GameCode_46GDcano10Objects1ObjectsGDgdjs_46GameCode_46GDcano11Objects1Objects);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbandeiraObjects1.length;i<l;++i) {
    if ( !(gdjs.GameCode.GDbandeiraObjects1[i].isVisible()) ) {
        gdjs.GameCode.condition1IsTrue_0.val = true;
        gdjs.GameCode.GDbandeiraObjects1[k] = gdjs.GameCode.GDbandeiraObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbandeiraObjects1.length = k;}}
if (gdjs.GameCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDbandeiraObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDbandeiraObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDbandeiraObjects1[i].hide(false);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x775180
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects2ObjectsGDgdjs_46GameCode_46GDcano2Objects2ObjectsGDgdjs_46GameCode_46GDcano3Objects2ObjectsGDgdjs_46GameCode_46GDcano4Objects2ObjectsGDgdjs_46GameCode_46GDcano5Objects2ObjectsGDgdjs_46GameCode_46GDcano6Objects2ObjectsGDgdjs_46GameCode_46GDcano7Objects2ObjectsGDgdjs_46GameCode_46GDcano9Objects2ObjectsGDgdjs_46GameCode_46GDcano10Objects2ObjectsGDgdjs_46GameCode_46GDcano11Objects2Objects = Hashtable.newFrom({"cano1": gdjs.GameCode.GDcano1Objects2, "cano2": gdjs.GameCode.GDcano2Objects2, "cano3": gdjs.GameCode.GDcano3Objects2, "cano4": gdjs.GameCode.GDcano4Objects2, "cano5": gdjs.GameCode.GDcano5Objects2, "cano6": gdjs.GameCode.GDcano6Objects2, "cano7": gdjs.GameCode.GDcano7Objects2, "cano9": gdjs.GameCode.GDcano9Objects2, "cano10": gdjs.GameCode.GDcano10Objects2, "cano11": gdjs.GameCode.GDcano11Objects2});gdjs.GameCode.eventsList0x776170 = function(runtimeScene) {

{

/* Reuse gdjs.GameCode.GDcano1Objects2 */
/* Reuse gdjs.GameCode.GDcano10Objects2 */
/* Reuse gdjs.GameCode.GDcano11Objects2 */
/* Reuse gdjs.GameCode.GDcano2Objects2 */
/* Reuse gdjs.GameCode.GDcano3Objects2 */
/* Reuse gdjs.GameCode.GDcano4Objects2 */
/* Reuse gdjs.GameCode.GDcano5Objects2 */
/* Reuse gdjs.GameCode.GDcano6Objects2 */
/* Reuse gdjs.GameCode.GDcano7Objects2 */
/* Reuse gdjs.GameCode.GDcano9Objects2 */

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcano1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano1Objects2[i].getVariableNumber(gdjs.GameCode.GDcano1Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano1Objects2[k] = gdjs.GameCode.GDcano1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano1Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano2Objects2[i].getVariableNumber(gdjs.GameCode.GDcano2Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano2Objects2[k] = gdjs.GameCode.GDcano2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano2Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano3Objects2[i].getVariableNumber(gdjs.GameCode.GDcano3Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano3Objects2[k] = gdjs.GameCode.GDcano3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano3Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano4Objects2[i].getVariableNumber(gdjs.GameCode.GDcano4Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano4Objects2[k] = gdjs.GameCode.GDcano4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano4Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano5Objects2[i].getVariableNumber(gdjs.GameCode.GDcano5Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano5Objects2[k] = gdjs.GameCode.GDcano5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano5Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano6Objects2[i].getVariableNumber(gdjs.GameCode.GDcano6Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano6Objects2[k] = gdjs.GameCode.GDcano6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano6Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano7Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano7Objects2[i].getVariableNumber(gdjs.GameCode.GDcano7Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano7Objects2[k] = gdjs.GameCode.GDcano7Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano7Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano9Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano9Objects2[i].getVariableNumber(gdjs.GameCode.GDcano9Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano9Objects2[k] = gdjs.GameCode.GDcano9Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano9Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano10Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano10Objects2[i].getVariableNumber(gdjs.GameCode.GDcano10Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano10Objects2[k] = gdjs.GameCode.GDcano10Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano10Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano11Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano11Objects2[i].getVariableNumber(gdjs.GameCode.GDcano11Objects2[i].getVariables().get("agua")) == gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano11Objects2[k] = gdjs.GameCode.GDcano11Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano11Objects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcano1Objects2 */
/* Reuse gdjs.GameCode.GDcano10Objects2 */
/* Reuse gdjs.GameCode.GDcano11Objects2 */
/* Reuse gdjs.GameCode.GDcano2Objects2 */
/* Reuse gdjs.GameCode.GDcano3Objects2 */
/* Reuse gdjs.GameCode.GDcano4Objects2 */
/* Reuse gdjs.GameCode.GDcano5Objects2 */
/* Reuse gdjs.GameCode.GDcano6Objects2 */
/* Reuse gdjs.GameCode.GDcano7Objects2 */
/* Reuse gdjs.GameCode.GDcano9Objects2 */
{for(var i = 0, len = gdjs.GameCode.GDcano1Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano1Objects2[i].setAnimation(2);
}
for(var i = 0, len = gdjs.GameCode.GDcano2Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano2Objects2[i].setAnimation(2);
}
for(var i = 0, len = gdjs.GameCode.GDcano3Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano3Objects2[i].setAnimation(2);
}
for(var i = 0, len = gdjs.GameCode.GDcano4Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano4Objects2[i].setAnimation(2);
}
for(var i = 0, len = gdjs.GameCode.GDcano5Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano5Objects2[i].setAnimation(2);
}
for(var i = 0, len = gdjs.GameCode.GDcano6Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano6Objects2[i].setAnimation(2);
}
for(var i = 0, len = gdjs.GameCode.GDcano7Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano7Objects2[i].setAnimation(2);
}
for(var i = 0, len = gdjs.GameCode.GDcano9Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano9Objects2[i].setAnimation(2);
}
for(var i = 0, len = gdjs.GameCode.GDcano10Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano10Objects2[i].setAnimation(2);
}
for(var i = 0, len = gdjs.GameCode.GDcano11Objects2.length ;i < len;++i) {
    gdjs.GameCode.GDcano11Objects2[i].setAnimation(2);
}
}}

}


}; //End of gdjs.GameCode.eventsList0x776170
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects1ObjectsGDgdjs_46GameCode_46GDcano2Objects1ObjectsGDgdjs_46GameCode_46GDcano3Objects1ObjectsGDgdjs_46GameCode_46GDcano4Objects1ObjectsGDgdjs_46GameCode_46GDcano5Objects1ObjectsGDgdjs_46GameCode_46GDcano6Objects1ObjectsGDgdjs_46GameCode_46GDcano7Objects1ObjectsGDgdjs_46GameCode_46GDcano9Objects1ObjectsGDgdjs_46GameCode_46GDcano10Objects1ObjectsGDgdjs_46GameCode_46GDcano11Objects1Objects = Hashtable.newFrom({"cano1": gdjs.GameCode.GDcano1Objects1, "cano2": gdjs.GameCode.GDcano2Objects1, "cano3": gdjs.GameCode.GDcano3Objects1, "cano4": gdjs.GameCode.GDcano4Objects1, "cano5": gdjs.GameCode.GDcano5Objects1, "cano6": gdjs.GameCode.GDcano6Objects1, "cano7": gdjs.GameCode.GDcano7Objects1, "cano9": gdjs.GameCode.GDcano9Objects1, "cano10": gdjs.GameCode.GDcano10Objects1, "cano11": gdjs.GameCode.GDcano11Objects1});gdjs.GameCode.eventsList0x775c28 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.3, "liberar_agua");
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDcanoInicioObjects2.createFrom(runtimeScene.getObjects("canoInicio"));
{for(var i = 0, len = gdjs.GameCode.GDcanoInicioObjects2.length ;i < len;++i) {
    gdjs.GameCode.GDcanoInicioObjects2[i].setAnimation(1);
}
}{runtimeScene.getGame().getVariables().getFromIndex(4).add(1);
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "liberar_agua");
}}

}


{

gdjs.GameCode.GDcano1Objects2.createFrom(runtimeScene.getObjects("cano1"));
gdjs.GameCode.GDcano10Objects2.createFrom(runtimeScene.getObjects("cano10"));
gdjs.GameCode.GDcano11Objects2.createFrom(runtimeScene.getObjects("cano11"));
gdjs.GameCode.GDcano2Objects2.createFrom(runtimeScene.getObjects("cano2"));
gdjs.GameCode.GDcano3Objects2.createFrom(runtimeScene.getObjects("cano3"));
gdjs.GameCode.GDcano4Objects2.createFrom(runtimeScene.getObjects("cano4"));
gdjs.GameCode.GDcano5Objects2.createFrom(runtimeScene.getObjects("cano5"));
gdjs.GameCode.GDcano6Objects2.createFrom(runtimeScene.getObjects("cano6"));
gdjs.GameCode.GDcano7Objects2.createFrom(runtimeScene.getObjects("cano7"));
gdjs.GameCode.GDcano9Objects2.createFrom(runtimeScene.getObjects("cano9"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects2ObjectsGDgdjs_46GameCode_46GDcano2Objects2ObjectsGDgdjs_46GameCode_46GDcano3Objects2ObjectsGDgdjs_46GameCode_46GDcano4Objects2ObjectsGDgdjs_46GameCode_46GDcano5Objects2ObjectsGDgdjs_46GameCode_46GDcano6Objects2ObjectsGDgdjs_46GameCode_46GDcano7Objects2ObjectsGDgdjs_46GameCode_46GDcano9Objects2ObjectsGDgdjs_46GameCode_46GDcano10Objects2ObjectsGDgdjs_46GameCode_46GDcano11Objects2Objects) >= gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4));
}if (gdjs.GameCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x776170(runtimeScene);} //End of subevents
}

}


{

gdjs.GameCode.GDcano1Objects1.createFrom(runtimeScene.getObjects("cano1"));
gdjs.GameCode.GDcano10Objects1.createFrom(runtimeScene.getObjects("cano10"));
gdjs.GameCode.GDcano11Objects1.createFrom(runtimeScene.getObjects("cano11"));
gdjs.GameCode.GDcano2Objects1.createFrom(runtimeScene.getObjects("cano2"));
gdjs.GameCode.GDcano3Objects1.createFrom(runtimeScene.getObjects("cano3"));
gdjs.GameCode.GDcano4Objects1.createFrom(runtimeScene.getObjects("cano4"));
gdjs.GameCode.GDcano5Objects1.createFrom(runtimeScene.getObjects("cano5"));
gdjs.GameCode.GDcano6Objects1.createFrom(runtimeScene.getObjects("cano6"));
gdjs.GameCode.GDcano7Objects1.createFrom(runtimeScene.getObjects("cano7"));
gdjs.GameCode.GDcano9Objects1.createFrom(runtimeScene.getObjects("cano9"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == gdjs.evtTools.object.pickedObjectsCount(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects1ObjectsGDgdjs_46GameCode_46GDcano2Objects1ObjectsGDgdjs_46GameCode_46GDcano3Objects1ObjectsGDgdjs_46GameCode_46GDcano4Objects1ObjectsGDgdjs_46GameCode_46GDcano5Objects1ObjectsGDgdjs_46GameCode_46GDcano6Objects1ObjectsGDgdjs_46GameCode_46GDcano7Objects1ObjectsGDgdjs_46GameCode_46GDcano9Objects1ObjectsGDgdjs_46GameCode_46GDcano10Objects1ObjectsGDgdjs_46GameCode_46GDcano11Objects1Objects);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDcanoFinalObjects1.createFrom(runtimeScene.getObjects("canoFinal"));
{for(var i = 0, len = gdjs.GameCode.GDcanoFinalObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDcanoFinalObjects1[i].setAnimation(1);
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "Dialog");
}}

}


}; //End of gdjs.GameCode.eventsList0x775c28
gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDtopObjects2Objects = Hashtable.newFrom({"top": gdjs.GameCode.GDtopObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects2ObjectsGDgdjs_46GameCode_46GDcano2Objects2ObjectsGDgdjs_46GameCode_46GDcano3Objects2ObjectsGDgdjs_46GameCode_46GDcano4Objects2ObjectsGDgdjs_46GameCode_46GDcano5Objects2ObjectsGDgdjs_46GameCode_46GDcano6Objects2ObjectsGDgdjs_46GameCode_46GDcano7Objects2ObjectsGDgdjs_46GameCode_46GDcano9Objects2ObjectsGDgdjs_46GameCode_46GDcano10Objects2ObjectsGDgdjs_46GameCode_46GDcano11Objects2Objects = Hashtable.newFrom({"cano1": gdjs.GameCode.GDcano1Objects2, "cano2": gdjs.GameCode.GDcano2Objects2, "cano3": gdjs.GameCode.GDcano3Objects2, "cano4": gdjs.GameCode.GDcano4Objects2, "cano5": gdjs.GameCode.GDcano5Objects2, "cano6": gdjs.GameCode.GDcano6Objects2, "cano7": gdjs.GameCode.GDcano7Objects2, "cano9": gdjs.GameCode.GDcano9Objects2, "cano10": gdjs.GameCode.GDcano10Objects2, "cano11": gdjs.GameCode.GDcano11Objects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDtopObjects2Objects = Hashtable.newFrom({"top": gdjs.GameCode.GDtopObjects2});gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDvaziosObjects2Objects = Hashtable.newFrom({"vazios": gdjs.GameCode.GDvaziosObjects2});gdjs.GameCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "senario" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)), 0, 0);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(-1);
}{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(0);
}
{ //Subevents
gdjs.GameCode.eventsList0x771830(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.GameCode.GDcano1Objects1.createFrom(runtimeScene.getObjects("cano1"));
gdjs.GameCode.GDcano10Objects1.createFrom(runtimeScene.getObjects("cano10"));
gdjs.GameCode.GDcano11Objects1.createFrom(runtimeScene.getObjects("cano11"));
gdjs.GameCode.GDcano2Objects1.createFrom(runtimeScene.getObjects("cano2"));
gdjs.GameCode.GDcano3Objects1.createFrom(runtimeScene.getObjects("cano3"));
gdjs.GameCode.GDcano4Objects1.createFrom(runtimeScene.getObjects("cano4"));
gdjs.GameCode.GDcano5Objects1.createFrom(runtimeScene.getObjects("cano5"));
gdjs.GameCode.GDcano6Objects1.createFrom(runtimeScene.getObjects("cano6"));
gdjs.GameCode.GDcano7Objects1.createFrom(runtimeScene.getObjects("cano7"));
gdjs.GameCode.GDcano9Objects1.createFrom(runtimeScene.getObjects("cano9"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects1ObjectsGDgdjs_46GameCode_46GDcano2Objects1ObjectsGDgdjs_46GameCode_46GDcano3Objects1ObjectsGDgdjs_46GameCode_46GDcano4Objects1ObjectsGDgdjs_46GameCode_46GDcano5Objects1ObjectsGDgdjs_46GameCode_46GDcano6Objects1ObjectsGDgdjs_46GameCode_46GDcano7Objects1ObjectsGDgdjs_46GameCode_46GDcano9Objects1ObjectsGDgdjs_46GameCode_46GDcano10Objects1ObjectsGDgdjs_46GameCode_46GDcano11Objects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 0;
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition3IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7812740);
}
}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcano1Objects1 */
/* Reuse gdjs.GameCode.GDcano10Objects1 */
/* Reuse gdjs.GameCode.GDcano11Objects1 */
/* Reuse gdjs.GameCode.GDcano2Objects1 */
/* Reuse gdjs.GameCode.GDcano3Objects1 */
/* Reuse gdjs.GameCode.GDcano4Objects1 */
/* Reuse gdjs.GameCode.GDcano5Objects1 */
/* Reuse gdjs.GameCode.GDcano6Objects1 */
/* Reuse gdjs.GameCode.GDcano7Objects1 */
/* Reuse gdjs.GameCode.GDcano9Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDcano1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano1Objects1[i].setAngle(gdjs.GameCode.GDcano1Objects1[i].getAngle() + (90));
}
for(var i = 0, len = gdjs.GameCode.GDcano2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano2Objects1[i].setAngle(gdjs.GameCode.GDcano2Objects1[i].getAngle() + (90));
}
for(var i = 0, len = gdjs.GameCode.GDcano3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano3Objects1[i].setAngle(gdjs.GameCode.GDcano3Objects1[i].getAngle() + (90));
}
for(var i = 0, len = gdjs.GameCode.GDcano4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano4Objects1[i].setAngle(gdjs.GameCode.GDcano4Objects1[i].getAngle() + (90));
}
for(var i = 0, len = gdjs.GameCode.GDcano5Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano5Objects1[i].setAngle(gdjs.GameCode.GDcano5Objects1[i].getAngle() + (90));
}
for(var i = 0, len = gdjs.GameCode.GDcano6Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano6Objects1[i].setAngle(gdjs.GameCode.GDcano6Objects1[i].getAngle() + (90));
}
for(var i = 0, len = gdjs.GameCode.GDcano7Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano7Objects1[i].setAngle(gdjs.GameCode.GDcano7Objects1[i].getAngle() + (90));
}
for(var i = 0, len = gdjs.GameCode.GDcano9Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano9Objects1[i].setAngle(gdjs.GameCode.GDcano9Objects1[i].getAngle() + (90));
}
for(var i = 0, len = gdjs.GameCode.GDcano10Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano10Objects1[i].setAngle(gdjs.GameCode.GDcano10Objects1[i].getAngle() + (90));
}
for(var i = 0, len = gdjs.GameCode.GDcano11Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano11Objects1[i].setAngle(gdjs.GameCode.GDcano11Objects1[i].getAngle() + (90));
}
}
{ //Subevents
gdjs.GameCode.eventsList0x773420(runtimeScene);} //End of subevents
}

}


{

gdjs.GameCode.GDcano10Objects1.createFrom(runtimeScene.getObjects("cano10"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcano10Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano10Objects1[i].getAngle() != 90 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano10Objects1[k] = gdjs.GameCode.GDcano10Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano10Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcano10Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDcano10Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano10Objects1[i].setAngle(0);
}
}}

}


{

gdjs.GameCode.GDcano4Objects1.createFrom(runtimeScene.getObjects("cano4"));

gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcano4Objects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano4Objects1[i].getAngle() != 90 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano4Objects1[k] = gdjs.GameCode.GDcano4Objects1[i];
        ++k;
    }
}
gdjs.GameCode.GDcano4Objects1.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDcano4Objects1 */
{for(var i = 0, len = gdjs.GameCode.GDcano4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano4Objects1[i].setAngle(0);
}
}}

}


{

gdjs.GameCode.GDvaziosObjects1.createFrom(runtimeScene.getObjects("vazios"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDvaziosObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition2IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7814796);
}
}}
}
if (gdjs.GameCode.condition2IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDvaziosObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDvaziosObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDvaziosObjects1[i].setAngle(gdjs.GameCode.GDvaziosObjects1[i].getAngle() + (90));
}
}}

}


{

gdjs.GameCode.GDalavancaObjects1.createFrom(runtimeScene.getObjects("alavanca"));
gdjs.GameCode.GDbandeiraObjects1.createFrom(runtimeScene.getObjects("bandeira"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDalavancaObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDbandeiraObjects1.length;i<l;++i) {
    if ( gdjs.GameCode.GDbandeiraObjects1[i].isVisible() ) {
        gdjs.GameCode.condition2IsTrue_0.val = true;
        gdjs.GameCode.GDbandeiraObjects1[k] = gdjs.GameCode.GDbandeiraObjects1[i];
        ++k;
    }
}
gdjs.GameCode.GDbandeiraObjects1.length = k;}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition3IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7815740);
}
}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
/* Reuse gdjs.GameCode.GDalavancaObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDalavancaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDalavancaObjects1[i].setAnimation(1);
}
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(1);
}}

}


{

gdjs.GameCode.GDlblContinuarObjects1.createFrom(runtimeScene.getObjects("lblContinuar"));

gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
gdjs.GameCode.condition2IsTrue_0.val = false;
gdjs.GameCode.condition3IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "Dialog");
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDlblContinuarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameCode.condition1IsTrue_0.val ) {
{
gdjs.GameCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameCode.condition2IsTrue_0.val ) {
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition3IsTrue_0;
gdjs.GameCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7817116);
}
}}
}
}
if (gdjs.GameCode.condition3IsTrue_0.val) {
{gdjs.evtTools.camera.hideLayer(runtimeScene, "Dialog");
}
{ //Subevents
gdjs.GameCode.eventsList0x774518(runtimeScene);} //End of subevents
}

}


{



}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}}

}


{

gdjs.GameCode.GDcano1Objects1.createFrom(runtimeScene.getObjects("cano1"));
gdjs.GameCode.GDcano10Objects1.createFrom(runtimeScene.getObjects("cano10"));
gdjs.GameCode.GDcano11Objects1.createFrom(runtimeScene.getObjects("cano11"));
gdjs.GameCode.GDcano2Objects1.createFrom(runtimeScene.getObjects("cano2"));
gdjs.GameCode.GDcano3Objects1.createFrom(runtimeScene.getObjects("cano3"));
gdjs.GameCode.GDcano4Objects1.createFrom(runtimeScene.getObjects("cano4"));
gdjs.GameCode.GDcano5Objects1.createFrom(runtimeScene.getObjects("cano5"));
gdjs.GameCode.GDcano6Objects1.createFrom(runtimeScene.getObjects("cano6"));
gdjs.GameCode.GDcano7Objects1.createFrom(runtimeScene.getObjects("cano7"));
gdjs.GameCode.GDcano9Objects1.createFrom(runtimeScene.getObjects("cano9"));

gdjs.GameCode.forEachTotalCount2 = 0;
gdjs.GameCode.forEachObjects2.length = 0;
gdjs.GameCode.forEachCount0_2 = gdjs.GameCode.GDcano1Objects1.length;
gdjs.GameCode.forEachTotalCount2 += gdjs.GameCode.forEachCount0_2;
gdjs.GameCode.forEachObjects2.push.apply(gdjs.GameCode.forEachObjects2,gdjs.GameCode.GDcano1Objects1);
gdjs.GameCode.forEachCount1_2 = gdjs.GameCode.GDcano2Objects1.length;
gdjs.GameCode.forEachTotalCount2 += gdjs.GameCode.forEachCount1_2;
gdjs.GameCode.forEachObjects2.push.apply(gdjs.GameCode.forEachObjects2,gdjs.GameCode.GDcano2Objects1);
gdjs.GameCode.forEachCount2_2 = gdjs.GameCode.GDcano3Objects1.length;
gdjs.GameCode.forEachTotalCount2 += gdjs.GameCode.forEachCount2_2;
gdjs.GameCode.forEachObjects2.push.apply(gdjs.GameCode.forEachObjects2,gdjs.GameCode.GDcano3Objects1);
gdjs.GameCode.forEachCount3_2 = gdjs.GameCode.GDcano4Objects1.length;
gdjs.GameCode.forEachTotalCount2 += gdjs.GameCode.forEachCount3_2;
gdjs.GameCode.forEachObjects2.push.apply(gdjs.GameCode.forEachObjects2,gdjs.GameCode.GDcano4Objects1);
gdjs.GameCode.forEachCount4_2 = gdjs.GameCode.GDcano5Objects1.length;
gdjs.GameCode.forEachTotalCount2 += gdjs.GameCode.forEachCount4_2;
gdjs.GameCode.forEachObjects2.push.apply(gdjs.GameCode.forEachObjects2,gdjs.GameCode.GDcano5Objects1);
gdjs.GameCode.forEachCount5_2 = gdjs.GameCode.GDcano6Objects1.length;
gdjs.GameCode.forEachTotalCount2 += gdjs.GameCode.forEachCount5_2;
gdjs.GameCode.forEachObjects2.push.apply(gdjs.GameCode.forEachObjects2,gdjs.GameCode.GDcano6Objects1);
gdjs.GameCode.forEachCount6_2 = gdjs.GameCode.GDcano7Objects1.length;
gdjs.GameCode.forEachTotalCount2 += gdjs.GameCode.forEachCount6_2;
gdjs.GameCode.forEachObjects2.push.apply(gdjs.GameCode.forEachObjects2,gdjs.GameCode.GDcano7Objects1);
gdjs.GameCode.forEachCount7_2 = gdjs.GameCode.GDcano9Objects1.length;
gdjs.GameCode.forEachTotalCount2 += gdjs.GameCode.forEachCount7_2;
gdjs.GameCode.forEachObjects2.push.apply(gdjs.GameCode.forEachObjects2,gdjs.GameCode.GDcano9Objects1);
gdjs.GameCode.forEachCount8_2 = gdjs.GameCode.GDcano10Objects1.length;
gdjs.GameCode.forEachTotalCount2 += gdjs.GameCode.forEachCount8_2;
gdjs.GameCode.forEachObjects2.push.apply(gdjs.GameCode.forEachObjects2,gdjs.GameCode.GDcano10Objects1);
gdjs.GameCode.forEachCount9_2 = gdjs.GameCode.GDcano11Objects1.length;
gdjs.GameCode.forEachTotalCount2 += gdjs.GameCode.forEachCount9_2;
gdjs.GameCode.forEachObjects2.push.apply(gdjs.GameCode.forEachObjects2,gdjs.GameCode.GDcano11Objects1);
for(gdjs.GameCode.forEachIndex2 = 0;gdjs.GameCode.forEachIndex2 < gdjs.GameCode.forEachTotalCount2;++gdjs.GameCode.forEachIndex2) {
gdjs.GameCode.GDcano1Objects2.createFrom(gdjs.GameCode.GDcano1Objects1);

gdjs.GameCode.GDcano10Objects2.createFrom(gdjs.GameCode.GDcano10Objects1);

gdjs.GameCode.GDcano11Objects2.createFrom(gdjs.GameCode.GDcano11Objects1);

gdjs.GameCode.GDcano2Objects2.createFrom(gdjs.GameCode.GDcano2Objects1);

gdjs.GameCode.GDcano3Objects2.createFrom(gdjs.GameCode.GDcano3Objects1);

gdjs.GameCode.GDcano4Objects2.createFrom(gdjs.GameCode.GDcano4Objects1);

gdjs.GameCode.GDcano5Objects2.createFrom(gdjs.GameCode.GDcano5Objects1);

gdjs.GameCode.GDcano6Objects2.createFrom(gdjs.GameCode.GDcano6Objects1);

gdjs.GameCode.GDcano7Objects2.createFrom(gdjs.GameCode.GDcano7Objects1);

gdjs.GameCode.GDcano9Objects2.createFrom(gdjs.GameCode.GDcano9Objects1);


gdjs.GameCode.GDcano1Objects2.length = 0;
gdjs.GameCode.GDcano2Objects2.length = 0;
gdjs.GameCode.GDcano3Objects2.length = 0;
gdjs.GameCode.GDcano4Objects2.length = 0;
gdjs.GameCode.GDcano5Objects2.length = 0;
gdjs.GameCode.GDcano6Objects2.length = 0;
gdjs.GameCode.GDcano7Objects2.length = 0;
gdjs.GameCode.GDcano9Objects2.length = 0;
gdjs.GameCode.GDcano10Objects2.length = 0;
gdjs.GameCode.GDcano11Objects2.length = 0;
if (gdjs.GameCode.forEachIndex2 < gdjs.GameCode.forEachCount0_2) {
    gdjs.GameCode.GDcano1Objects2.push(gdjs.GameCode.forEachObjects2[gdjs.GameCode.forEachIndex2]);
}
else if (gdjs.GameCode.forEachIndex2 < gdjs.GameCode.forEachCount0_2+gdjs.GameCode.forEachCount1_2) {
    gdjs.GameCode.GDcano2Objects2.push(gdjs.GameCode.forEachObjects2[gdjs.GameCode.forEachIndex2]);
}
else if (gdjs.GameCode.forEachIndex2 < gdjs.GameCode.forEachCount0_2+gdjs.GameCode.forEachCount1_2+gdjs.GameCode.forEachCount2_2) {
    gdjs.GameCode.GDcano3Objects2.push(gdjs.GameCode.forEachObjects2[gdjs.GameCode.forEachIndex2]);
}
else if (gdjs.GameCode.forEachIndex2 < gdjs.GameCode.forEachCount0_2+gdjs.GameCode.forEachCount1_2+gdjs.GameCode.forEachCount2_2+gdjs.GameCode.forEachCount3_2) {
    gdjs.GameCode.GDcano4Objects2.push(gdjs.GameCode.forEachObjects2[gdjs.GameCode.forEachIndex2]);
}
else if (gdjs.GameCode.forEachIndex2 < gdjs.GameCode.forEachCount0_2+gdjs.GameCode.forEachCount1_2+gdjs.GameCode.forEachCount2_2+gdjs.GameCode.forEachCount3_2+gdjs.GameCode.forEachCount4_2) {
    gdjs.GameCode.GDcano5Objects2.push(gdjs.GameCode.forEachObjects2[gdjs.GameCode.forEachIndex2]);
}
else if (gdjs.GameCode.forEachIndex2 < gdjs.GameCode.forEachCount0_2+gdjs.GameCode.forEachCount1_2+gdjs.GameCode.forEachCount2_2+gdjs.GameCode.forEachCount3_2+gdjs.GameCode.forEachCount4_2+gdjs.GameCode.forEachCount5_2) {
    gdjs.GameCode.GDcano6Objects2.push(gdjs.GameCode.forEachObjects2[gdjs.GameCode.forEachIndex2]);
}
else if (gdjs.GameCode.forEachIndex2 < gdjs.GameCode.forEachCount0_2+gdjs.GameCode.forEachCount1_2+gdjs.GameCode.forEachCount2_2+gdjs.GameCode.forEachCount3_2+gdjs.GameCode.forEachCount4_2+gdjs.GameCode.forEachCount5_2+gdjs.GameCode.forEachCount6_2) {
    gdjs.GameCode.GDcano7Objects2.push(gdjs.GameCode.forEachObjects2[gdjs.GameCode.forEachIndex2]);
}
else if (gdjs.GameCode.forEachIndex2 < gdjs.GameCode.forEachCount0_2+gdjs.GameCode.forEachCount1_2+gdjs.GameCode.forEachCount2_2+gdjs.GameCode.forEachCount3_2+gdjs.GameCode.forEachCount4_2+gdjs.GameCode.forEachCount5_2+gdjs.GameCode.forEachCount6_2+gdjs.GameCode.forEachCount7_2) {
    gdjs.GameCode.GDcano9Objects2.push(gdjs.GameCode.forEachObjects2[gdjs.GameCode.forEachIndex2]);
}
else if (gdjs.GameCode.forEachIndex2 < gdjs.GameCode.forEachCount0_2+gdjs.GameCode.forEachCount1_2+gdjs.GameCode.forEachCount2_2+gdjs.GameCode.forEachCount3_2+gdjs.GameCode.forEachCount4_2+gdjs.GameCode.forEachCount5_2+gdjs.GameCode.forEachCount6_2+gdjs.GameCode.forEachCount7_2+gdjs.GameCode.forEachCount8_2) {
    gdjs.GameCode.GDcano10Objects2.push(gdjs.GameCode.forEachObjects2[gdjs.GameCode.forEachIndex2]);
}
else if (gdjs.GameCode.forEachIndex2 < gdjs.GameCode.forEachCount0_2+gdjs.GameCode.forEachCount1_2+gdjs.GameCode.forEachCount2_2+gdjs.GameCode.forEachCount3_2+gdjs.GameCode.forEachCount4_2+gdjs.GameCode.forEachCount5_2+gdjs.GameCode.forEachCount6_2+gdjs.GameCode.forEachCount7_2+gdjs.GameCode.forEachCount8_2+gdjs.GameCode.forEachCount9_2) {
    gdjs.GameCode.GDcano11Objects2.push(gdjs.GameCode.forEachObjects2[gdjs.GameCode.forEachIndex2]);
}
gdjs.GameCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameCode.GDcano1Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano1Objects2[i].getAngle() > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano1Objects2[k] = gdjs.GameCode.GDcano1Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano1Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano2Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano2Objects2[i].getAngle() > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano2Objects2[k] = gdjs.GameCode.GDcano2Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano2Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano3Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano3Objects2[i].getAngle() > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano3Objects2[k] = gdjs.GameCode.GDcano3Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano3Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano4Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano4Objects2[i].getAngle() > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano4Objects2[k] = gdjs.GameCode.GDcano4Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano4Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano5Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano5Objects2[i].getAngle() > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano5Objects2[k] = gdjs.GameCode.GDcano5Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano5Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano6Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano6Objects2[i].getAngle() > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano6Objects2[k] = gdjs.GameCode.GDcano6Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano6Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano7Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano7Objects2[i].getAngle() > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano7Objects2[k] = gdjs.GameCode.GDcano7Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano7Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano9Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano9Objects2[i].getAngle() > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano9Objects2[k] = gdjs.GameCode.GDcano9Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano9Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano10Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano10Objects2[i].getAngle() > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano10Objects2[k] = gdjs.GameCode.GDcano10Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano10Objects2.length = k;for(var i = 0, k = 0, l = gdjs.GameCode.GDcano11Objects2.length;i<l;++i) {
    if ( gdjs.GameCode.GDcano11Objects2[i].getAngle() > 0 ) {
        gdjs.GameCode.condition0IsTrue_0.val = true;
        gdjs.GameCode.GDcano11Objects2[k] = gdjs.GameCode.GDcano11Objects2[i];
        ++k;
    }
}
gdjs.GameCode.GDcano11Objects2.length = k;}if (gdjs.GameCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).add((( gdjs.GameCode.GDcano11Objects2.length === 0 ) ? (( gdjs.GameCode.GDcano10Objects2.length === 0 ) ? (( gdjs.GameCode.GDcano9Objects2.length === 0 ) ? (( gdjs.GameCode.GDcano7Objects2.length === 0 ) ? (( gdjs.GameCode.GDcano6Objects2.length === 0 ) ? (( gdjs.GameCode.GDcano5Objects2.length === 0 ) ? (( gdjs.GameCode.GDcano4Objects2.length === 0 ) ? (( gdjs.GameCode.GDcano3Objects2.length === 0 ) ? (( gdjs.GameCode.GDcano2Objects2.length === 0 ) ? (( gdjs.GameCode.GDcano1Objects2.length === 0 ) ? 0 :gdjs.GameCode.GDcano1Objects2[0].getAngle()) :gdjs.GameCode.GDcano2Objects2[0].getAngle()) :gdjs.GameCode.GDcano3Objects2[0].getAngle()) :gdjs.GameCode.GDcano4Objects2[0].getAngle()) :gdjs.GameCode.GDcano5Objects2[0].getAngle()) :gdjs.GameCode.GDcano6Objects2[0].getAngle()) :gdjs.GameCode.GDcano7Objects2[0].getAngle()) :gdjs.GameCode.GDcano9Objects2[0].getAngle()) :gdjs.GameCode.GDcano10Objects2[0].getAngle()) :gdjs.GameCode.GDcano11Objects2[0].getAngle()));
}}
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 0;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x775180(runtimeScene);} //End of subevents
}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
gdjs.GameCode.condition1IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 0;
}if ( gdjs.GameCode.condition0IsTrue_0.val ) {
{
gdjs.GameCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 1;
}}
if (gdjs.GameCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.GameCode.eventsList0x775c28(runtimeScene);} //End of subevents
}

}


{

gdjs.GameCode.GDcano1Objects1.length = 0;

gdjs.GameCode.GDcano10Objects1.length = 0;

gdjs.GameCode.GDcano11Objects1.length = 0;

gdjs.GameCode.GDcano2Objects1.length = 0;

gdjs.GameCode.GDcano3Objects1.length = 0;

gdjs.GameCode.GDcano4Objects1.length = 0;

gdjs.GameCode.GDcano5Objects1.length = 0;

gdjs.GameCode.GDcano6Objects1.length = 0;

gdjs.GameCode.GDcano7Objects1.length = 0;

gdjs.GameCode.GDcano9Objects1.length = 0;

gdjs.GameCode.GDtopObjects1.length = 0;

gdjs.GameCode.GDvaziosObjects1.length = 0;


gdjs.GameCode.condition0IsTrue_0.val = false;
{
{gdjs.GameCode.conditionTrue_1 = gdjs.GameCode.condition0IsTrue_0;
gdjs.GameCode.GDcano1Objects1_1final.length = 0;gdjs.GameCode.GDcano10Objects1_1final.length = 0;gdjs.GameCode.GDcano11Objects1_1final.length = 0;gdjs.GameCode.GDcano2Objects1_1final.length = 0;gdjs.GameCode.GDcano3Objects1_1final.length = 0;gdjs.GameCode.GDcano4Objects1_1final.length = 0;gdjs.GameCode.GDcano5Objects1_1final.length = 0;gdjs.GameCode.GDcano6Objects1_1final.length = 0;gdjs.GameCode.GDcano7Objects1_1final.length = 0;gdjs.GameCode.GDcano9Objects1_1final.length = 0;gdjs.GameCode.GDtopObjects1_1final.length = 0;gdjs.GameCode.GDvaziosObjects1_1final.length = 0;gdjs.GameCode.condition0IsTrue_1.val = false;
gdjs.GameCode.condition1IsTrue_1.val = false;
{
gdjs.GameCode.GDcano1Objects2.createFrom(runtimeScene.getObjects("cano1"));
gdjs.GameCode.GDcano10Objects2.createFrom(runtimeScene.getObjects("cano10"));
gdjs.GameCode.GDcano11Objects2.createFrom(runtimeScene.getObjects("cano11"));
gdjs.GameCode.GDcano2Objects2.createFrom(runtimeScene.getObjects("cano2"));
gdjs.GameCode.GDcano3Objects2.createFrom(runtimeScene.getObjects("cano3"));
gdjs.GameCode.GDcano4Objects2.createFrom(runtimeScene.getObjects("cano4"));
gdjs.GameCode.GDcano5Objects2.createFrom(runtimeScene.getObjects("cano5"));
gdjs.GameCode.GDcano6Objects2.createFrom(runtimeScene.getObjects("cano6"));
gdjs.GameCode.GDcano7Objects2.createFrom(runtimeScene.getObjects("cano7"));
gdjs.GameCode.GDcano9Objects2.createFrom(runtimeScene.getObjects("cano9"));
gdjs.GameCode.GDtopObjects2.createFrom(runtimeScene.getObjects("top"));
gdjs.GameCode.condition0IsTrue_1.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDtopObjects2Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDcano1Objects2ObjectsGDgdjs_46GameCode_46GDcano2Objects2ObjectsGDgdjs_46GameCode_46GDcano3Objects2ObjectsGDgdjs_46GameCode_46GDcano4Objects2ObjectsGDgdjs_46GameCode_46GDcano5Objects2ObjectsGDgdjs_46GameCode_46GDcano6Objects2ObjectsGDgdjs_46GameCode_46GDcano7Objects2ObjectsGDgdjs_46GameCode_46GDcano9Objects2ObjectsGDgdjs_46GameCode_46GDcano10Objects2ObjectsGDgdjs_46GameCode_46GDcano11Objects2Objects, false, runtimeScene);
if( gdjs.GameCode.condition0IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDcano1Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDcano1Objects1_1final.indexOf(gdjs.GameCode.GDcano1Objects2[j]) === -1 )
            gdjs.GameCode.GDcano1Objects1_1final.push(gdjs.GameCode.GDcano1Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDcano10Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDcano10Objects1_1final.indexOf(gdjs.GameCode.GDcano10Objects2[j]) === -1 )
            gdjs.GameCode.GDcano10Objects1_1final.push(gdjs.GameCode.GDcano10Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDcano11Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDcano11Objects1_1final.indexOf(gdjs.GameCode.GDcano11Objects2[j]) === -1 )
            gdjs.GameCode.GDcano11Objects1_1final.push(gdjs.GameCode.GDcano11Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDcano2Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDcano2Objects1_1final.indexOf(gdjs.GameCode.GDcano2Objects2[j]) === -1 )
            gdjs.GameCode.GDcano2Objects1_1final.push(gdjs.GameCode.GDcano2Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDcano3Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDcano3Objects1_1final.indexOf(gdjs.GameCode.GDcano3Objects2[j]) === -1 )
            gdjs.GameCode.GDcano3Objects1_1final.push(gdjs.GameCode.GDcano3Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDcano4Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDcano4Objects1_1final.indexOf(gdjs.GameCode.GDcano4Objects2[j]) === -1 )
            gdjs.GameCode.GDcano4Objects1_1final.push(gdjs.GameCode.GDcano4Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDcano5Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDcano5Objects1_1final.indexOf(gdjs.GameCode.GDcano5Objects2[j]) === -1 )
            gdjs.GameCode.GDcano5Objects1_1final.push(gdjs.GameCode.GDcano5Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDcano6Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDcano6Objects1_1final.indexOf(gdjs.GameCode.GDcano6Objects2[j]) === -1 )
            gdjs.GameCode.GDcano6Objects1_1final.push(gdjs.GameCode.GDcano6Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDcano7Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDcano7Objects1_1final.indexOf(gdjs.GameCode.GDcano7Objects2[j]) === -1 )
            gdjs.GameCode.GDcano7Objects1_1final.push(gdjs.GameCode.GDcano7Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDcano9Objects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDcano9Objects1_1final.indexOf(gdjs.GameCode.GDcano9Objects2[j]) === -1 )
            gdjs.GameCode.GDcano9Objects1_1final.push(gdjs.GameCode.GDcano9Objects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDtopObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDtopObjects1_1final.indexOf(gdjs.GameCode.GDtopObjects2[j]) === -1 )
            gdjs.GameCode.GDtopObjects1_1final.push(gdjs.GameCode.GDtopObjects2[j]);
    }
}
}
{
gdjs.GameCode.GDtopObjects2.createFrom(runtimeScene.getObjects("top"));
gdjs.GameCode.GDvaziosObjects2.createFrom(runtimeScene.getObjects("vazios"));
gdjs.GameCode.condition1IsTrue_1.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDtopObjects2Objects, gdjs.GameCode.mapOfGDgdjs_46GameCode_46GDvaziosObjects2Objects, false, runtimeScene);
if( gdjs.GameCode.condition1IsTrue_1.val ) {
    gdjs.GameCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.GameCode.GDtopObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDtopObjects1_1final.indexOf(gdjs.GameCode.GDtopObjects2[j]) === -1 )
            gdjs.GameCode.GDtopObjects1_1final.push(gdjs.GameCode.GDtopObjects2[j]);
    }
    for(var j = 0, jLen = gdjs.GameCode.GDvaziosObjects2.length;j<jLen;++j) {
        if ( gdjs.GameCode.GDvaziosObjects1_1final.indexOf(gdjs.GameCode.GDvaziosObjects2[j]) === -1 )
            gdjs.GameCode.GDvaziosObjects1_1final.push(gdjs.GameCode.GDvaziosObjects2[j]);
    }
}
}
{
gdjs.GameCode.GDcano1Objects1.createFrom(gdjs.GameCode.GDcano1Objects1_1final);
gdjs.GameCode.GDcano10Objects1.createFrom(gdjs.GameCode.GDcano10Objects1_1final);
gdjs.GameCode.GDcano11Objects1.createFrom(gdjs.GameCode.GDcano11Objects1_1final);
gdjs.GameCode.GDcano2Objects1.createFrom(gdjs.GameCode.GDcano2Objects1_1final);
gdjs.GameCode.GDcano3Objects1.createFrom(gdjs.GameCode.GDcano3Objects1_1final);
gdjs.GameCode.GDcano4Objects1.createFrom(gdjs.GameCode.GDcano4Objects1_1final);
gdjs.GameCode.GDcano5Objects1.createFrom(gdjs.GameCode.GDcano5Objects1_1final);
gdjs.GameCode.GDcano6Objects1.createFrom(gdjs.GameCode.GDcano6Objects1_1final);
gdjs.GameCode.GDcano7Objects1.createFrom(gdjs.GameCode.GDcano7Objects1_1final);
gdjs.GameCode.GDcano9Objects1.createFrom(gdjs.GameCode.GDcano9Objects1_1final);
gdjs.GameCode.GDtopObjects1.createFrom(gdjs.GameCode.GDtopObjects1_1final);
gdjs.GameCode.GDvaziosObjects1.createFrom(gdjs.GameCode.GDvaziosObjects1_1final);
}
}
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDfaixaObjects1.createFrom(runtimeScene.getObjects("faixa"));
/* Reuse gdjs.GameCode.GDtopObjects1 */
{for(var i = 0, len = gdjs.GameCode.GDtopObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDtopObjects1[i].setY(gdjs.GameCode.GDtopObjects1[i].getY() - (1));
}
}{for(var i = 0, len = gdjs.GameCode.GDfaixaObjects1.length ;i < len;++i) {
    gdjs.GameCode.GDfaixaObjects1[i].setY((( gdjs.GameCode.GDtopObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDtopObjects1[0].getY()) + (( gdjs.GameCode.GDtopObjects1.length === 0 ) ? 0 :gdjs.GameCode.GDtopObjects1[0].getHeight()) - (gdjs.GameCode.GDfaixaObjects1[i].getHeight())/2);
}
}}

}


{


gdjs.GameCode.condition0IsTrue_0.val = false;
{
gdjs.GameCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) == 1;
}if (gdjs.GameCode.condition0IsTrue_0.val) {
gdjs.GameCode.GDcano1Objects1.createFrom(runtimeScene.getObjects("cano1"));
gdjs.GameCode.GDcano10Objects1.createFrom(runtimeScene.getObjects("cano10"));
gdjs.GameCode.GDcano11Objects1.createFrom(runtimeScene.getObjects("cano11"));
gdjs.GameCode.GDcano2Objects1.createFrom(runtimeScene.getObjects("cano2"));
gdjs.GameCode.GDcano3Objects1.createFrom(runtimeScene.getObjects("cano3"));
gdjs.GameCode.GDcano4Objects1.createFrom(runtimeScene.getObjects("cano4"));
gdjs.GameCode.GDcano5Objects1.createFrom(runtimeScene.getObjects("cano5"));
gdjs.GameCode.GDcano6Objects1.createFrom(runtimeScene.getObjects("cano6"));
gdjs.GameCode.GDcano7Objects1.createFrom(runtimeScene.getObjects("cano7"));
gdjs.GameCode.GDcano9Objects1.createFrom(runtimeScene.getObjects("cano9"));
{for(var i = 0, len = gdjs.GameCode.GDcano1Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano1Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano2Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano2Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano3Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano3Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano4Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano4Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano5Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano5Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano6Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano6Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano7Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano7Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano9Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano9Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano10Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano10Objects1[i].setAngle(0);
}
for(var i = 0, len = gdjs.GameCode.GDcano11Objects1.length ;i < len;++i) {
    gdjs.GameCode.GDcano11Objects1[i].setAngle(0);
}
}}

}


}; //End of gdjs.GameCode.eventsList0xaff48


gdjs.GameCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.GameCode.GDbackgroundObjects1.length = 0;
gdjs.GameCode.GDbackgroundObjects2.length = 0;
gdjs.GameCode.GDbackgroundObjects3.length = 0;
gdjs.GameCode.GDcano1Objects1.length = 0;
gdjs.GameCode.GDcano1Objects2.length = 0;
gdjs.GameCode.GDcano1Objects3.length = 0;
gdjs.GameCode.GDcano2Objects1.length = 0;
gdjs.GameCode.GDcano2Objects2.length = 0;
gdjs.GameCode.GDcano2Objects3.length = 0;
gdjs.GameCode.GDcano11Objects1.length = 0;
gdjs.GameCode.GDcano11Objects2.length = 0;
gdjs.GameCode.GDcano11Objects3.length = 0;
gdjs.GameCode.GDcano3Objects1.length = 0;
gdjs.GameCode.GDcano3Objects2.length = 0;
gdjs.GameCode.GDcano3Objects3.length = 0;
gdjs.GameCode.GDcano4Objects1.length = 0;
gdjs.GameCode.GDcano4Objects2.length = 0;
gdjs.GameCode.GDcano4Objects3.length = 0;
gdjs.GameCode.GDcano10Objects1.length = 0;
gdjs.GameCode.GDcano10Objects2.length = 0;
gdjs.GameCode.GDcano10Objects3.length = 0;
gdjs.GameCode.GDcano5Objects1.length = 0;
gdjs.GameCode.GDcano5Objects2.length = 0;
gdjs.GameCode.GDcano5Objects3.length = 0;
gdjs.GameCode.GDcano6Objects1.length = 0;
gdjs.GameCode.GDcano6Objects2.length = 0;
gdjs.GameCode.GDcano6Objects3.length = 0;
gdjs.GameCode.GDcano7Objects1.length = 0;
gdjs.GameCode.GDcano7Objects2.length = 0;
gdjs.GameCode.GDcano7Objects3.length = 0;
gdjs.GameCode.GDcano9Objects1.length = 0;
gdjs.GameCode.GDcano9Objects2.length = 0;
gdjs.GameCode.GDcano9Objects3.length = 0;
gdjs.GameCode.GDvaziosObjects1.length = 0;
gdjs.GameCode.GDvaziosObjects2.length = 0;
gdjs.GameCode.GDvaziosObjects3.length = 0;
gdjs.GameCode.GDalavancaObjects1.length = 0;
gdjs.GameCode.GDalavancaObjects2.length = 0;
gdjs.GameCode.GDalavancaObjects3.length = 0;
gdjs.GameCode.GDbaseObjects1.length = 0;
gdjs.GameCode.GDbaseObjects2.length = 0;
gdjs.GameCode.GDbaseObjects3.length = 0;
gdjs.GameCode.GDbandeiraObjects1.length = 0;
gdjs.GameCode.GDbandeiraObjects2.length = 0;
gdjs.GameCode.GDbandeiraObjects3.length = 0;
gdjs.GameCode.GDcanoInicioObjects1.length = 0;
gdjs.GameCode.GDcanoInicioObjects2.length = 0;
gdjs.GameCode.GDcanoInicioObjects3.length = 0;
gdjs.GameCode.GDcanoFinalObjects1.length = 0;
gdjs.GameCode.GDcanoFinalObjects2.length = 0;
gdjs.GameCode.GDcanoFinalObjects3.length = 0;
gdjs.GameCode.GDboxObjects1.length = 0;
gdjs.GameCode.GDboxObjects2.length = 0;
gdjs.GameCode.GDboxObjects3.length = 0;
gdjs.GameCode.GDgalaoObjects1.length = 0;
gdjs.GameCode.GDgalaoObjects2.length = 0;
gdjs.GameCode.GDgalaoObjects3.length = 0;
gdjs.GameCode.GDDialogObjects1.length = 0;
gdjs.GameCode.GDDialogObjects2.length = 0;
gdjs.GameCode.GDDialogObjects3.length = 0;
gdjs.GameCode.GDtxtAguaObjects1.length = 0;
gdjs.GameCode.GDtxtAguaObjects2.length = 0;
gdjs.GameCode.GDtxtAguaObjects3.length = 0;
gdjs.GameCode.GDbtnCanoObjects1.length = 0;
gdjs.GameCode.GDbtnCanoObjects2.length = 0;
gdjs.GameCode.GDbtnCanoObjects3.length = 0;
gdjs.GameCode.GDlblContinuarObjects1.length = 0;
gdjs.GameCode.GDlblContinuarObjects2.length = 0;
gdjs.GameCode.GDlblContinuarObjects3.length = 0;
gdjs.GameCode.GDtopObjects1.length = 0;
gdjs.GameCode.GDtopObjects2.length = 0;
gdjs.GameCode.GDtopObjects3.length = 0;
gdjs.GameCode.GDfaixaObjects1.length = 0;
gdjs.GameCode.GDfaixaObjects2.length = 0;
gdjs.GameCode.GDfaixaObjects3.length = 0;

gdjs.GameCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['GameCode'] = gdjs.GameCode;
