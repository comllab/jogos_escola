gdjs.StartCode = {};
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDNewObjectObjects1= [];
gdjs.StartCode.GDNewObjectObjects2= [];
gdjs.StartCode.GDNewObject2Objects1= [];
gdjs.StartCode.GDNewObject2Objects2= [];
gdjs.StartCode.GDjogarObjects1= [];
gdjs.StartCode.GDjogarObjects2= [];
gdjs.StartCode.GDsombraObjects1= [];
gdjs.StartCode.GDsombraObjects2= [];
gdjs.StartCode.GDNewObject3Objects1= [];
gdjs.StartCode.GDNewObject3Objects2= [];
gdjs.StartCode.GDbtStartObjects1= [];
gdjs.StartCode.GDbtStartObjects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};
gdjs.StartCode.condition3IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtStartObjects1Objects = Hashtable.newFrom({"btStart": gdjs.StartCode.GDbtStartObjects1});gdjs.StartCode.eventsList0xaff48 = function(runtimeScene) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Grasslands Theme.ogg", 1, true, 100, 1);
}}

}


{

gdjs.StartCode.GDbtStartObjects1.createFrom(runtimeScene.getObjects("btStart"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtStartObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7807252);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


}; //End of gdjs.StartCode.eventsList0xaff48


gdjs.StartCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDNewObjectObjects1.length = 0;
gdjs.StartCode.GDNewObjectObjects2.length = 0;
gdjs.StartCode.GDNewObject2Objects1.length = 0;
gdjs.StartCode.GDNewObject2Objects2.length = 0;
gdjs.StartCode.GDjogarObjects1.length = 0;
gdjs.StartCode.GDjogarObjects2.length = 0;
gdjs.StartCode.GDsombraObjects1.length = 0;
gdjs.StartCode.GDsombraObjects2.length = 0;
gdjs.StartCode.GDNewObject3Objects1.length = 0;
gdjs.StartCode.GDNewObject3Objects2.length = 0;
gdjs.StartCode.GDbtStartObjects1.length = 0;
gdjs.StartCode.GDbtStartObjects2.length = 0;

gdjs.StartCode.eventsList0xaff48(runtimeScene);
return;
}
gdjs['StartCode'] = gdjs.StartCode;
