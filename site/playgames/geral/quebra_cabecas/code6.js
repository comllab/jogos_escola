gdjs.peixesCode = {};
gdjs.peixesCode.GDpuzze1Objects1= [];
gdjs.peixesCode.GDpuzze1Objects2= [];
gdjs.peixesCode.GDpuzze2Objects1= [];
gdjs.peixesCode.GDpuzze2Objects2= [];
gdjs.peixesCode.GDpuzze3Objects1= [];
gdjs.peixesCode.GDpuzze3Objects2= [];
gdjs.peixesCode.GDpuzze4Objects1= [];
gdjs.peixesCode.GDpuzze4Objects2= [];
gdjs.peixesCode.GDNewTextObjects1= [];
gdjs.peixesCode.GDNewTextObjects2= [];
gdjs.peixesCode.GDbackground_9595puzzeObjects1= [];
gdjs.peixesCode.GDbackground_9595puzzeObjects2= [];
gdjs.peixesCode.GDbackObjects1= [];
gdjs.peixesCode.GDbackObjects2= [];


gdjs.peixesCode.mapOfGDgdjs_9546peixesCode_9546GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.peixesCode.GDbackObjects1});
gdjs.peixesCode.mapOfGDgdjs_9546peixesCode_9546GDpuzze1Objects1Objects = Hashtable.newFrom({"puzze1": gdjs.peixesCode.GDpuzze1Objects1});
gdjs.peixesCode.mapOfGDgdjs_9546peixesCode_9546GDpuzze2Objects1Objects = Hashtable.newFrom({"puzze2": gdjs.peixesCode.GDpuzze2Objects1});
gdjs.peixesCode.mapOfGDgdjs_9546peixesCode_9546GDpuzze3Objects1Objects = Hashtable.newFrom({"puzze3": gdjs.peixesCode.GDpuzze3Objects1});
gdjs.peixesCode.mapOfGDgdjs_9546peixesCode_9546GDpuzze4Objects1Objects = Hashtable.newFrom({"puzze4": gdjs.peixesCode.GDpuzze4Objects1});
gdjs.peixesCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("puzze1"), gdjs.peixesCode.GDpuzze1Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze2"), gdjs.peixesCode.GDpuzze2Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze3"), gdjs.peixesCode.GDpuzze3Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze4"), gdjs.peixesCode.GDpuzze4Objects1);
{for(var i = 0, len = gdjs.peixesCode.GDpuzze1Objects1.length ;i < len;++i) {
    gdjs.peixesCode.GDpuzze1Objects1[i].getBehavior("Tween").addObjectPositionTween("dino1", 351, 309, "swingFromTo", 1200, false);
}
}{for(var i = 0, len = gdjs.peixesCode.GDpuzze2Objects1.length ;i < len;++i) {
    gdjs.peixesCode.GDpuzze2Objects1[i].getBehavior("Tween").addObjectPositionTween("dino2", 52, 30, "swingFromTo", 1300, false);
}
}{for(var i = 0, len = gdjs.peixesCode.GDpuzze3Objects1.length ;i < len;++i) {
    gdjs.peixesCode.GDpuzze3Objects1[i].getBehavior("Tween").addObjectPositionTween("dino3", 737, 475, "swingFromTo", 900, false);
}
}{for(var i = 0, len = gdjs.peixesCode.GDpuzze4Objects1.length ;i < len;++i) {
    gdjs.peixesCode.GDpuzze4Objects1[i].getBehavior("Tween").addObjectPositionTween("dino4", 725, 48, "swingFromTo", 950, false);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.peixesCode.GDbackObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.peixesCode.mapOfGDgdjs_9546peixesCode_9546GDbackObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(8772348);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "start", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze1"), gdjs.peixesCode.GDpuzze1Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.peixesCode.mapOfGDgdjs_9546peixesCode_9546GDpuzze1Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(8773260);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze2"), gdjs.peixesCode.GDpuzze2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.peixesCode.mapOfGDgdjs_9546peixesCode_9546GDpuzze2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(8774140);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino2", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze3"), gdjs.peixesCode.GDpuzze3Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.peixesCode.mapOfGDgdjs_9546peixesCode_9546GDpuzze3Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(8775268);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino3", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze4"), gdjs.peixesCode.GDpuzze4Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.peixesCode.mapOfGDgdjs_9546peixesCode_9546GDpuzze4Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(8771756);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino4", false);
}}

}


};

gdjs.peixesCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.peixesCode.GDpuzze1Objects1.length = 0;
gdjs.peixesCode.GDpuzze1Objects2.length = 0;
gdjs.peixesCode.GDpuzze2Objects1.length = 0;
gdjs.peixesCode.GDpuzze2Objects2.length = 0;
gdjs.peixesCode.GDpuzze3Objects1.length = 0;
gdjs.peixesCode.GDpuzze3Objects2.length = 0;
gdjs.peixesCode.GDpuzze4Objects1.length = 0;
gdjs.peixesCode.GDpuzze4Objects2.length = 0;
gdjs.peixesCode.GDNewTextObjects1.length = 0;
gdjs.peixesCode.GDNewTextObjects2.length = 0;
gdjs.peixesCode.GDbackground_9595puzzeObjects1.length = 0;
gdjs.peixesCode.GDbackground_9595puzzeObjects2.length = 0;
gdjs.peixesCode.GDbackObjects1.length = 0;
gdjs.peixesCode.GDbackObjects2.length = 0;

gdjs.peixesCode.eventsList0(runtimeScene);

return;

}

gdjs['peixesCode'] = gdjs.peixesCode;
