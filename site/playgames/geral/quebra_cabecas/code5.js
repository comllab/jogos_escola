gdjs.frutasCode = {};
gdjs.frutasCode.GDpuzze1Objects1= [];
gdjs.frutasCode.GDpuzze1Objects2= [];
gdjs.frutasCode.GDpuzze2Objects1= [];
gdjs.frutasCode.GDpuzze2Objects2= [];
gdjs.frutasCode.GDpuzze3Objects1= [];
gdjs.frutasCode.GDpuzze3Objects2= [];
gdjs.frutasCode.GDpuzze4Objects1= [];
gdjs.frutasCode.GDpuzze4Objects2= [];
gdjs.frutasCode.GDNewTextObjects1= [];
gdjs.frutasCode.GDNewTextObjects2= [];
gdjs.frutasCode.GDbackground_9595puzzeObjects1= [];
gdjs.frutasCode.GDbackground_9595puzzeObjects2= [];
gdjs.frutasCode.GDbackObjects1= [];
gdjs.frutasCode.GDbackObjects2= [];


gdjs.frutasCode.mapOfGDgdjs_9546frutasCode_9546GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.frutasCode.GDbackObjects1});
gdjs.frutasCode.mapOfGDgdjs_9546frutasCode_9546GDpuzze1Objects1Objects = Hashtable.newFrom({"puzze1": gdjs.frutasCode.GDpuzze1Objects1});
gdjs.frutasCode.mapOfGDgdjs_9546frutasCode_9546GDpuzze2Objects1Objects = Hashtable.newFrom({"puzze2": gdjs.frutasCode.GDpuzze2Objects1});
gdjs.frutasCode.mapOfGDgdjs_9546frutasCode_9546GDpuzze3Objects1Objects = Hashtable.newFrom({"puzze3": gdjs.frutasCode.GDpuzze3Objects1});
gdjs.frutasCode.mapOfGDgdjs_9546frutasCode_9546GDpuzze4Objects1Objects = Hashtable.newFrom({"puzze4": gdjs.frutasCode.GDpuzze4Objects1});
gdjs.frutasCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("puzze1"), gdjs.frutasCode.GDpuzze1Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze2"), gdjs.frutasCode.GDpuzze2Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze3"), gdjs.frutasCode.GDpuzze3Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze4"), gdjs.frutasCode.GDpuzze4Objects1);
{for(var i = 0, len = gdjs.frutasCode.GDpuzze1Objects1.length ;i < len;++i) {
    gdjs.frutasCode.GDpuzze1Objects1[i].getBehavior("Tween").addObjectPositionTween("dino1", 351, 309, "swingFromTo", 1200, false);
}
}{for(var i = 0, len = gdjs.frutasCode.GDpuzze2Objects1.length ;i < len;++i) {
    gdjs.frutasCode.GDpuzze2Objects1[i].getBehavior("Tween").addObjectPositionTween("dino2", 52, 30, "swingFromTo", 1300, false);
}
}{for(var i = 0, len = gdjs.frutasCode.GDpuzze3Objects1.length ;i < len;++i) {
    gdjs.frutasCode.GDpuzze3Objects1[i].getBehavior("Tween").addObjectPositionTween("dino3", 737, 475, "swingFromTo", 900, false);
}
}{for(var i = 0, len = gdjs.frutasCode.GDpuzze4Objects1.length ;i < len;++i) {
    gdjs.frutasCode.GDpuzze4Objects1[i].getBehavior("Tween").addObjectPositionTween("dino4", 725, 48, "swingFromTo", 950, false);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.frutasCode.GDbackObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.frutasCode.mapOfGDgdjs_9546frutasCode_9546GDbackObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(7786100);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "start", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze1"), gdjs.frutasCode.GDpuzze1Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.frutasCode.mapOfGDgdjs_9546frutasCode_9546GDpuzze1Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(7368644);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze2"), gdjs.frutasCode.GDpuzze2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.frutasCode.mapOfGDgdjs_9546frutasCode_9546GDpuzze2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9173444);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino2", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze3"), gdjs.frutasCode.GDpuzze3Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.frutasCode.mapOfGDgdjs_9546frutasCode_9546GDpuzze3Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9174388);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino3", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze4"), gdjs.frutasCode.GDpuzze4Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.frutasCode.mapOfGDgdjs_9546frutasCode_9546GDpuzze4Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9175388);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino4", false);
}}

}


};

gdjs.frutasCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.frutasCode.GDpuzze1Objects1.length = 0;
gdjs.frutasCode.GDpuzze1Objects2.length = 0;
gdjs.frutasCode.GDpuzze2Objects1.length = 0;
gdjs.frutasCode.GDpuzze2Objects2.length = 0;
gdjs.frutasCode.GDpuzze3Objects1.length = 0;
gdjs.frutasCode.GDpuzze3Objects2.length = 0;
gdjs.frutasCode.GDpuzze4Objects1.length = 0;
gdjs.frutasCode.GDpuzze4Objects2.length = 0;
gdjs.frutasCode.GDNewTextObjects1.length = 0;
gdjs.frutasCode.GDNewTextObjects2.length = 0;
gdjs.frutasCode.GDbackground_9595puzzeObjects1.length = 0;
gdjs.frutasCode.GDbackground_9595puzzeObjects2.length = 0;
gdjs.frutasCode.GDbackObjects1.length = 0;
gdjs.frutasCode.GDbackObjects2.length = 0;

gdjs.frutasCode.eventsList0(runtimeScene);

return;

}

gdjs['frutasCode'] = gdjs.frutasCode;
