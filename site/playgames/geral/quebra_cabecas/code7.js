gdjs.dinosCode = {};
gdjs.dinosCode.GDpuzze1Objects1= [];
gdjs.dinosCode.GDpuzze1Objects2= [];
gdjs.dinosCode.GDpuzze2Objects1= [];
gdjs.dinosCode.GDpuzze2Objects2= [];
gdjs.dinosCode.GDpuzze3Objects1= [];
gdjs.dinosCode.GDpuzze3Objects2= [];
gdjs.dinosCode.GDpuzze4Objects1= [];
gdjs.dinosCode.GDpuzze4Objects2= [];
gdjs.dinosCode.GDbackground_9595puzzeObjects1= [];
gdjs.dinosCode.GDbackground_9595puzzeObjects2= [];
gdjs.dinosCode.GDbackObjects1= [];
gdjs.dinosCode.GDbackObjects2= [];


gdjs.dinosCode.mapOfGDgdjs_9546dinosCode_9546GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.dinosCode.GDbackObjects1});
gdjs.dinosCode.mapOfGDgdjs_9546dinosCode_9546GDpuzze1Objects1Objects = Hashtable.newFrom({"puzze1": gdjs.dinosCode.GDpuzze1Objects1});
gdjs.dinosCode.mapOfGDgdjs_9546dinosCode_9546GDpuzze2Objects1Objects = Hashtable.newFrom({"puzze2": gdjs.dinosCode.GDpuzze2Objects1});
gdjs.dinosCode.mapOfGDgdjs_9546dinosCode_9546GDpuzze3Objects1Objects = Hashtable.newFrom({"puzze3": gdjs.dinosCode.GDpuzze3Objects1});
gdjs.dinosCode.mapOfGDgdjs_9546dinosCode_9546GDpuzze4Objects1Objects = Hashtable.newFrom({"puzze4": gdjs.dinosCode.GDpuzze4Objects1});
gdjs.dinosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("puzze1"), gdjs.dinosCode.GDpuzze1Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze2"), gdjs.dinosCode.GDpuzze2Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze3"), gdjs.dinosCode.GDpuzze3Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze4"), gdjs.dinosCode.GDpuzze4Objects1);
{for(var i = 0, len = gdjs.dinosCode.GDpuzze1Objects1.length ;i < len;++i) {
    gdjs.dinosCode.GDpuzze1Objects1[i].getBehavior("Tween").addObjectPositionTween("dino1", 351, 309, "swingFromTo", 1200, false);
}
}{for(var i = 0, len = gdjs.dinosCode.GDpuzze2Objects1.length ;i < len;++i) {
    gdjs.dinosCode.GDpuzze2Objects1[i].getBehavior("Tween").addObjectPositionTween("dino2", 52, 30, "swingFromTo", 1300, false);
}
}{for(var i = 0, len = gdjs.dinosCode.GDpuzze3Objects1.length ;i < len;++i) {
    gdjs.dinosCode.GDpuzze3Objects1[i].getBehavior("Tween").addObjectPositionTween("dino3", 737, 475, "swingFromTo", 900, false);
}
}{for(var i = 0, len = gdjs.dinosCode.GDpuzze4Objects1.length ;i < len;++i) {
    gdjs.dinosCode.GDpuzze4Objects1[i].getBehavior("Tween").addObjectPositionTween("dino4", 725, 48, "swingFromTo", 950, false);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.dinosCode.GDbackObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.dinosCode.mapOfGDgdjs_9546dinosCode_9546GDbackObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9150684);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "start", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze1"), gdjs.dinosCode.GDpuzze1Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.dinosCode.mapOfGDgdjs_9546dinosCode_9546GDpuzze1Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(8632804);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze2"), gdjs.dinosCode.GDpuzze2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.dinosCode.mapOfGDgdjs_9546dinosCode_9546GDpuzze2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(8742812);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino2", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze3"), gdjs.dinosCode.GDpuzze3Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.dinosCode.mapOfGDgdjs_9546dinosCode_9546GDpuzze3Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(7378340);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino3", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("puzze4"), gdjs.dinosCode.GDpuzze4Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.dinosCode.mapOfGDgdjs_9546dinosCode_9546GDpuzze4Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(8634940);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "puzze_dino4", false);
}}

}


};

gdjs.dinosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.dinosCode.GDpuzze1Objects1.length = 0;
gdjs.dinosCode.GDpuzze1Objects2.length = 0;
gdjs.dinosCode.GDpuzze2Objects1.length = 0;
gdjs.dinosCode.GDpuzze2Objects2.length = 0;
gdjs.dinosCode.GDpuzze3Objects1.length = 0;
gdjs.dinosCode.GDpuzze3Objects2.length = 0;
gdjs.dinosCode.GDpuzze4Objects1.length = 0;
gdjs.dinosCode.GDpuzze4Objects2.length = 0;
gdjs.dinosCode.GDbackground_9595puzzeObjects1.length = 0;
gdjs.dinosCode.GDbackground_9595puzzeObjects2.length = 0;
gdjs.dinosCode.GDbackObjects1.length = 0;
gdjs.dinosCode.GDbackObjects2.length = 0;

gdjs.dinosCode.eventsList0(runtimeScene);

return;

}

gdjs['dinosCode'] = gdjs.dinosCode;
