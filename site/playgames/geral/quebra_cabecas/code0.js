gdjs.startCode = {};
gdjs.startCode.GDbackgroundObjects1= [];
gdjs.startCode.GDbackgroundObjects2= [];
gdjs.startCode.GDdinossaurosObjects1= [];
gdjs.startCode.GDdinossaurosObjects2= [];
gdjs.startCode.GDbtnPeixesObjects1= [];
gdjs.startCode.GDbtnPeixesObjects2= [];
gdjs.startCode.GDbtnFrutasObjects1= [];
gdjs.startCode.GDbtnFrutasObjects2= [];
gdjs.startCode.GDtxtDinossaurosObjects1= [];
gdjs.startCode.GDtxtDinossaurosObjects2= [];
gdjs.startCode.GDtxtPeixesObjects1= [];
gdjs.startCode.GDtxtPeixesObjects2= [];
gdjs.startCode.GDtxtFrutasObjects1= [];
gdjs.startCode.GDtxtFrutasObjects2= [];
gdjs.startCode.GDESCOLAPLAYObjects1= [];
gdjs.startCode.GDESCOLAPLAYObjects2= [];
gdjs.startCode.GDQUEBRA_9595CABECASObjects1= [];
gdjs.startCode.GDQUEBRA_9595CABECASObjects2= [];
gdjs.startCode.GDbackground_9595puzzeObjects1= [];
gdjs.startCode.GDbackground_9595puzzeObjects2= [];
gdjs.startCode.GDbackObjects1= [];
gdjs.startCode.GDbackObjects2= [];


gdjs.startCode.mapOfGDgdjs_9546startCode_9546GDdinossaurosObjects1Objects = Hashtable.newFrom({"dinossauros": gdjs.startCode.GDdinossaurosObjects1});
gdjs.startCode.mapOfGDgdjs_9546startCode_9546GDbtnPeixesObjects1Objects = Hashtable.newFrom({"btnPeixes": gdjs.startCode.GDbtnPeixesObjects1});
gdjs.startCode.mapOfGDgdjs_9546startCode_9546GDbtnFrutasObjects1Objects = Hashtable.newFrom({"btnFrutas": gdjs.startCode.GDbtnFrutasObjects1});
gdjs.startCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("background"), gdjs.startCode.GDbackgroundObjects1);
{for(var i = 0, len = gdjs.startCode.GDbackgroundObjects1.length ;i < len;++i) {
    gdjs.startCode.GDbackgroundObjects1[i].setX(gdjs.evtTools.window.getGameResolutionWidth(runtimeScene) / 2 - (gdjs.startCode.GDbackgroundObjects1[i].getWidth()) / 2);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("dinossauros"), gdjs.startCode.GDdinossaurosObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.startCode.mapOfGDgdjs_9546startCode_9546GDdinossaurosObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(8034108);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "dinos", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("btnPeixes"), gdjs.startCode.GDbtnPeixesObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.startCode.mapOfGDgdjs_9546startCode_9546GDbtnPeixesObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(8670676);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "peixes", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("btnFrutas"), gdjs.startCode.GDbtnFrutasObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.startCode.mapOfGDgdjs_9546startCode_9546GDbtnFrutasObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(8453604);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "frutas", false);
}}

}


};

gdjs.startCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.startCode.GDbackgroundObjects1.length = 0;
gdjs.startCode.GDbackgroundObjects2.length = 0;
gdjs.startCode.GDdinossaurosObjects1.length = 0;
gdjs.startCode.GDdinossaurosObjects2.length = 0;
gdjs.startCode.GDbtnPeixesObjects1.length = 0;
gdjs.startCode.GDbtnPeixesObjects2.length = 0;
gdjs.startCode.GDbtnFrutasObjects1.length = 0;
gdjs.startCode.GDbtnFrutasObjects2.length = 0;
gdjs.startCode.GDtxtDinossaurosObjects1.length = 0;
gdjs.startCode.GDtxtDinossaurosObjects2.length = 0;
gdjs.startCode.GDtxtPeixesObjects1.length = 0;
gdjs.startCode.GDtxtPeixesObjects2.length = 0;
gdjs.startCode.GDtxtFrutasObjects1.length = 0;
gdjs.startCode.GDtxtFrutasObjects2.length = 0;
gdjs.startCode.GDESCOLAPLAYObjects1.length = 0;
gdjs.startCode.GDESCOLAPLAYObjects2.length = 0;
gdjs.startCode.GDQUEBRA_9595CABECASObjects1.length = 0;
gdjs.startCode.GDQUEBRA_9595CABECASObjects2.length = 0;
gdjs.startCode.GDbackground_9595puzzeObjects1.length = 0;
gdjs.startCode.GDbackground_9595puzzeObjects2.length = 0;
gdjs.startCode.GDbackObjects1.length = 0;
gdjs.startCode.GDbackObjects2.length = 0;

gdjs.startCode.eventsList0(runtimeScene);

return;

}

gdjs['startCode'] = gdjs.startCode;
