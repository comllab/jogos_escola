gdjs.puzze_95dino3Code = {};
gdjs.puzze_95dino3Code.GDpeace1Objects1= [];
gdjs.puzze_95dino3Code.GDpeace1Objects2= [];
gdjs.puzze_95dino3Code.GDpeace1Objects3= [];
gdjs.puzze_95dino3Code.GDpeace2Objects1= [];
gdjs.puzze_95dino3Code.GDpeace2Objects2= [];
gdjs.puzze_95dino3Code.GDpeace2Objects3= [];
gdjs.puzze_95dino3Code.GDpeace3Objects1= [];
gdjs.puzze_95dino3Code.GDpeace3Objects2= [];
gdjs.puzze_95dino3Code.GDpeace3Objects3= [];
gdjs.puzze_95dino3Code.GDpeace4Objects1= [];
gdjs.puzze_95dino3Code.GDpeace4Objects2= [];
gdjs.puzze_95dino3Code.GDpeace4Objects3= [];
gdjs.puzze_95dino3Code.GDpuzzeObjects1= [];
gdjs.puzze_95dino3Code.GDpuzzeObjects2= [];
gdjs.puzze_95dino3Code.GDpuzzeObjects3= [];
gdjs.puzze_95dino3Code.GDbackground_9595puzzeObjects1= [];
gdjs.puzze_95dino3Code.GDbackground_9595puzzeObjects2= [];
gdjs.puzze_95dino3Code.GDbackground_9595puzzeObjects3= [];
gdjs.puzze_95dino3Code.GDbackObjects1= [];
gdjs.puzze_95dino3Code.GDbackObjects2= [];
gdjs.puzze_95dino3Code.GDbackObjects3= [];


gdjs.puzze_95dino3Code.mapOfGDgdjs_9546puzze_959595dino3Code_9546GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.puzze_95dino3Code.GDbackObjects1});
gdjs.puzze_95dino3Code.mapOfGDgdjs_9546puzze_959595dino3Code_9546GDpuzzeObjects1Objects = Hashtable.newFrom({"puzze": gdjs.puzze_95dino3Code.GDpuzzeObjects1});
gdjs.puzze_95dino3Code.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.puzze_95dino3Code.GDpuzzeObjects1, gdjs.puzze_95dino3Code.GDpuzzeObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpuzzeObjects2.length;i<l;++i) {
    if ( gdjs.puzze_95dino3Code.GDpuzzeObjects2[i].getBehavior("Animation").getAnimationIndex() == 0 ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpuzzeObjects2[k] = gdjs.puzze_95dino3Code.GDpuzzeObjects2[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpuzzeObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.puzze_95dino3Code.GDpeace1Objects1, gdjs.puzze_95dino3Code.GDpeace1Objects2);

/* Reuse gdjs.puzze_95dino3Code.GDpuzzeObjects2 */
{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpuzzeObjects2.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpuzzeObjects2[i].getBehavior("Animation").setAnimationIndex(4);
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace1Objects2.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace1Objects2[i].deleteFromScene(runtimeScene);
}
}}

}


{

/* Reuse gdjs.puzze_95dino3Code.GDpuzzeObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpuzzeObjects1.length;i<l;++i) {
    if ( !(gdjs.puzze_95dino3Code.GDpuzzeObjects1[i].getBehavior("Animation").getAnimationIndex() == 0) ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpuzzeObjects1[k] = gdjs.puzze_95dino3Code.GDpuzzeObjects1[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpuzzeObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.puzze_95dino3Code.GDpeace1Objects1 */
{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace1Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace1Objects1[i].setX((gdjs.RuntimeObject.getVariableNumber(gdjs.puzze_95dino3Code.GDpeace1Objects1[i].getVariables().getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace1Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace1Objects1[i].setY((gdjs.RuntimeObject.getVariableNumber(gdjs.puzze_95dino3Code.GDpeace1Objects1[i].getVariables().getFromIndex(1))));
}
}}

}


};gdjs.puzze_95dino3Code.mapOfGDgdjs_9546puzze_959595dino3Code_9546GDpuzzeObjects1Objects = Hashtable.newFrom({"puzze": gdjs.puzze_95dino3Code.GDpuzzeObjects1});
gdjs.puzze_95dino3Code.eventsList1 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.puzze_95dino3Code.GDpuzzeObjects1, gdjs.puzze_95dino3Code.GDpuzzeObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpuzzeObjects2.length;i<l;++i) {
    if ( gdjs.puzze_95dino3Code.GDpuzzeObjects2[i].getBehavior("Animation").getAnimationIndex() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpuzzeObjects2[k] = gdjs.puzze_95dino3Code.GDpuzzeObjects2[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpuzzeObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.puzze_95dino3Code.GDpeace2Objects1, gdjs.puzze_95dino3Code.GDpeace2Objects2);

/* Reuse gdjs.puzze_95dino3Code.GDpuzzeObjects2 */
{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpuzzeObjects2.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpuzzeObjects2[i].getBehavior("Animation").setAnimationIndex(5);
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace2Objects2.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace2Objects2[i].deleteFromScene(runtimeScene);
}
}}

}


{

/* Reuse gdjs.puzze_95dino3Code.GDpuzzeObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpuzzeObjects1.length;i<l;++i) {
    if ( !(gdjs.puzze_95dino3Code.GDpuzzeObjects1[i].getBehavior("Animation").getAnimationIndex() == 1) ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpuzzeObjects1[k] = gdjs.puzze_95dino3Code.GDpuzzeObjects1[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpuzzeObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.puzze_95dino3Code.GDpeace2Objects1 */
{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace2Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace2Objects1[i].setX((gdjs.RuntimeObject.getVariableNumber(gdjs.puzze_95dino3Code.GDpeace2Objects1[i].getVariables().getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace2Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace2Objects1[i].setY((gdjs.RuntimeObject.getVariableNumber(gdjs.puzze_95dino3Code.GDpeace2Objects1[i].getVariables().getFromIndex(1))));
}
}}

}


};gdjs.puzze_95dino3Code.mapOfGDgdjs_9546puzze_959595dino3Code_9546GDpuzzeObjects1Objects = Hashtable.newFrom({"puzze": gdjs.puzze_95dino3Code.GDpuzzeObjects1});
gdjs.puzze_95dino3Code.eventsList2 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.puzze_95dino3Code.GDpuzzeObjects1, gdjs.puzze_95dino3Code.GDpuzzeObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpuzzeObjects2.length;i<l;++i) {
    if ( gdjs.puzze_95dino3Code.GDpuzzeObjects2[i].getBehavior("Animation").getAnimationIndex() == 2 ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpuzzeObjects2[k] = gdjs.puzze_95dino3Code.GDpuzzeObjects2[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpuzzeObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.puzze_95dino3Code.GDpeace3Objects1, gdjs.puzze_95dino3Code.GDpeace3Objects2);

/* Reuse gdjs.puzze_95dino3Code.GDpuzzeObjects2 */
{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpuzzeObjects2.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpuzzeObjects2[i].getBehavior("Animation").setAnimationIndex(6);
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace3Objects2.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace3Objects2[i].deleteFromScene(runtimeScene);
}
}}

}


{

/* Reuse gdjs.puzze_95dino3Code.GDpuzzeObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpuzzeObjects1.length;i<l;++i) {
    if ( !(gdjs.puzze_95dino3Code.GDpuzzeObjects1[i].getBehavior("Animation").getAnimationIndex() == 2) ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpuzzeObjects1[k] = gdjs.puzze_95dino3Code.GDpuzzeObjects1[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpuzzeObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.puzze_95dino3Code.GDpeace3Objects1 */
{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace3Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace3Objects1[i].setX((gdjs.RuntimeObject.getVariableNumber(gdjs.puzze_95dino3Code.GDpeace3Objects1[i].getVariables().getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace3Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace3Objects1[i].setY((gdjs.RuntimeObject.getVariableNumber(gdjs.puzze_95dino3Code.GDpeace3Objects1[i].getVariables().getFromIndex(1))));
}
}}

}


};gdjs.puzze_95dino3Code.mapOfGDgdjs_9546puzze_959595dino3Code_9546GDpuzzeObjects1Objects = Hashtable.newFrom({"puzze": gdjs.puzze_95dino3Code.GDpuzzeObjects1});
gdjs.puzze_95dino3Code.eventsList3 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.puzze_95dino3Code.GDpuzzeObjects1, gdjs.puzze_95dino3Code.GDpuzzeObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpuzzeObjects2.length;i<l;++i) {
    if ( gdjs.puzze_95dino3Code.GDpuzzeObjects2[i].getBehavior("Animation").getAnimationIndex() == 3 ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpuzzeObjects2[k] = gdjs.puzze_95dino3Code.GDpuzzeObjects2[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpuzzeObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.puzze_95dino3Code.GDpeace4Objects1, gdjs.puzze_95dino3Code.GDpeace4Objects2);

/* Reuse gdjs.puzze_95dino3Code.GDpuzzeObjects2 */
{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpuzzeObjects2.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpuzzeObjects2[i].getBehavior("Animation").setAnimationIndex(7);
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace4Objects2.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace4Objects2[i].deleteFromScene(runtimeScene);
}
}}

}


{

/* Reuse gdjs.puzze_95dino3Code.GDpuzzeObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpuzzeObjects1.length;i<l;++i) {
    if ( !(gdjs.puzze_95dino3Code.GDpuzzeObjects1[i].getBehavior("Animation").getAnimationIndex() == 3) ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpuzzeObjects1[k] = gdjs.puzze_95dino3Code.GDpuzzeObjects1[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpuzzeObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.puzze_95dino3Code.GDpeace4Objects1 */
{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace4Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace4Objects1[i].setX((gdjs.RuntimeObject.getVariableNumber(gdjs.puzze_95dino3Code.GDpeace4Objects1[i].getVariables().getFromIndex(0))));
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace4Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace4Objects1[i].setY((gdjs.RuntimeObject.getVariableNumber(gdjs.puzze_95dino3Code.GDpeace4Objects1[i].getVariables().get("initY"))));
}
}}

}


};gdjs.puzze_95dino3Code.eventsList4 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("peace1"), gdjs.puzze_95dino3Code.GDpeace1Objects1);
gdjs.copyArray(runtimeScene.getObjects("peace2"), gdjs.puzze_95dino3Code.GDpeace2Objects1);
gdjs.copyArray(runtimeScene.getObjects("peace3"), gdjs.puzze_95dino3Code.GDpeace3Objects1);
gdjs.copyArray(runtimeScene.getObjects("peace4"), gdjs.puzze_95dino3Code.GDpeace4Objects1);
{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace1Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace1Objects1[i].returnVariable(gdjs.puzze_95dino3Code.GDpeace1Objects1[i].getVariables().getFromIndex(0)).setNumber((gdjs.puzze_95dino3Code.GDpeace1Objects1[i].getPointX("")));
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace1Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace1Objects1[i].returnVariable(gdjs.puzze_95dino3Code.GDpeace1Objects1[i].getVariables().getFromIndex(0)).setNumber((gdjs.puzze_95dino3Code.GDpeace1Objects1[i].getPointX("")));
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace2Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace2Objects1[i].returnVariable(gdjs.puzze_95dino3Code.GDpeace2Objects1[i].getVariables().getFromIndex(1)).setNumber((gdjs.puzze_95dino3Code.GDpeace2Objects1[i].getPointY("")));
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace2Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace2Objects1[i].returnVariable(gdjs.puzze_95dino3Code.GDpeace2Objects1[i].getVariables().getFromIndex(0)).setNumber((gdjs.puzze_95dino3Code.GDpeace2Objects1[i].getPointX("")));
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace3Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace3Objects1[i].returnVariable(gdjs.puzze_95dino3Code.GDpeace3Objects1[i].getVariables().getFromIndex(1)).setNumber((gdjs.puzze_95dino3Code.GDpeace3Objects1[i].getPointY("")));
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace3Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace3Objects1[i].returnVariable(gdjs.puzze_95dino3Code.GDpeace3Objects1[i].getVariables().getFromIndex(1)).setNumber((gdjs.puzze_95dino3Code.GDpeace3Objects1[i].getPointY("")));
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace4Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace4Objects1[i].returnVariable(gdjs.puzze_95dino3Code.GDpeace4Objects1[i].getVariables().get("initY")).setNumber((gdjs.puzze_95dino3Code.GDpeace4Objects1[i].getPointY("")));
}
}{for(var i = 0, len = gdjs.puzze_95dino3Code.GDpeace4Objects1.length ;i < len;++i) {
    gdjs.puzze_95dino3Code.GDpeace4Objects1[i].returnVariable(gdjs.puzze_95dino3Code.GDpeace4Objects1[i].getVariables().getFromIndex(0)).setNumber((gdjs.puzze_95dino3Code.GDpeace4Objects1[i].getPointX("")));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.puzze_95dino3Code.GDbackObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.puzze_95dino3Code.mapOfGDgdjs_9546puzze_959595dino3Code_9546GDbackObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.hasTouchEnded(runtimeScene, 1);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9155668);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "dinos", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("peace1"), gdjs.puzze_95dino3Code.GDpeace1Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze"), gdjs.puzze_95dino3Code.GDpuzzeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpeace1Objects1.length;i<l;++i) {
    if ( gdjs.puzze_95dino3Code.GDpeace1Objects1[i].getBehavior("Draggable").wasJustDropped() ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpeace1Objects1[k] = gdjs.puzze_95dino3Code.GDpeace1Objects1[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpeace1Objects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.puzze_95dino3Code.mapOfGDgdjs_9546puzze_959595dino3Code_9546GDpuzzeObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.puzze_95dino3Code.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("peace2"), gdjs.puzze_95dino3Code.GDpeace2Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze"), gdjs.puzze_95dino3Code.GDpuzzeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpeace2Objects1.length;i<l;++i) {
    if ( gdjs.puzze_95dino3Code.GDpeace2Objects1[i].getBehavior("Draggable").wasJustDropped() ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpeace2Objects1[k] = gdjs.puzze_95dino3Code.GDpeace2Objects1[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpeace2Objects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.puzze_95dino3Code.mapOfGDgdjs_9546puzze_959595dino3Code_9546GDpuzzeObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.puzze_95dino3Code.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("peace3"), gdjs.puzze_95dino3Code.GDpeace3Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze"), gdjs.puzze_95dino3Code.GDpuzzeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpeace3Objects1.length;i<l;++i) {
    if ( gdjs.puzze_95dino3Code.GDpeace3Objects1[i].getBehavior("Draggable").wasJustDropped() ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpeace3Objects1[k] = gdjs.puzze_95dino3Code.GDpeace3Objects1[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpeace3Objects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.puzze_95dino3Code.mapOfGDgdjs_9546puzze_959595dino3Code_9546GDpuzzeObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.puzze_95dino3Code.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("peace4"), gdjs.puzze_95dino3Code.GDpeace4Objects1);
gdjs.copyArray(runtimeScene.getObjects("puzze"), gdjs.puzze_95dino3Code.GDpuzzeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.puzze_95dino3Code.GDpeace4Objects1.length;i<l;++i) {
    if ( gdjs.puzze_95dino3Code.GDpeace4Objects1[i].getBehavior("Draggable").wasJustDropped() ) {
        isConditionTrue_0 = true;
        gdjs.puzze_95dino3Code.GDpeace4Objects1[k] = gdjs.puzze_95dino3Code.GDpeace4Objects1[i];
        ++k;
    }
}
gdjs.puzze_95dino3Code.GDpeace4Objects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.puzze_95dino3Code.mapOfGDgdjs_9546puzze_959595dino3Code_9546GDpuzzeObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.puzze_95dino3Code.eventsList3(runtimeScene);} //End of subevents
}

}


};

gdjs.puzze_95dino3Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.puzze_95dino3Code.GDpeace1Objects1.length = 0;
gdjs.puzze_95dino3Code.GDpeace1Objects2.length = 0;
gdjs.puzze_95dino3Code.GDpeace1Objects3.length = 0;
gdjs.puzze_95dino3Code.GDpeace2Objects1.length = 0;
gdjs.puzze_95dino3Code.GDpeace2Objects2.length = 0;
gdjs.puzze_95dino3Code.GDpeace2Objects3.length = 0;
gdjs.puzze_95dino3Code.GDpeace3Objects1.length = 0;
gdjs.puzze_95dino3Code.GDpeace3Objects2.length = 0;
gdjs.puzze_95dino3Code.GDpeace3Objects3.length = 0;
gdjs.puzze_95dino3Code.GDpeace4Objects1.length = 0;
gdjs.puzze_95dino3Code.GDpeace4Objects2.length = 0;
gdjs.puzze_95dino3Code.GDpeace4Objects3.length = 0;
gdjs.puzze_95dino3Code.GDpuzzeObjects1.length = 0;
gdjs.puzze_95dino3Code.GDpuzzeObjects2.length = 0;
gdjs.puzze_95dino3Code.GDpuzzeObjects3.length = 0;
gdjs.puzze_95dino3Code.GDbackground_9595puzzeObjects1.length = 0;
gdjs.puzze_95dino3Code.GDbackground_9595puzzeObjects2.length = 0;
gdjs.puzze_95dino3Code.GDbackground_9595puzzeObjects3.length = 0;
gdjs.puzze_95dino3Code.GDbackObjects1.length = 0;
gdjs.puzze_95dino3Code.GDbackObjects2.length = 0;
gdjs.puzze_95dino3Code.GDbackObjects3.length = 0;

gdjs.puzze_95dino3Code.eventsList4(runtimeScene);

return;

}

gdjs['puzze_95dino3Code'] = gdjs.puzze_95dino3Code;
