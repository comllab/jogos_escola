gdjs.GameOverCode = {};
gdjs.GameOverCode.GDajudaObjects1= [];
gdjs.GameOverCode.GDajudaObjects2= [];
gdjs.GameOverCode.GDbtJogarObjects1= [];
gdjs.GameOverCode.GDbtJogarObjects2= [];
gdjs.GameOverCode.GDlblJogarObjects1= [];
gdjs.GameOverCode.GDlblJogarObjects2= [];
gdjs.GameOverCode.GDbackgroundObjects1= [];
gdjs.GameOverCode.GDbackgroundObjects2= [];
gdjs.GameOverCode.GDNewObjectObjects1= [];
gdjs.GameOverCode.GDNewObjectObjects2= [];
gdjs.GameOverCode.GDNewObject2Objects1= [];
gdjs.GameOverCode.GDNewObject2Objects2= [];
gdjs.GameOverCode.GDlblPontuacaoObjects1= [];
gdjs.GameOverCode.GDlblPontuacaoObjects2= [];
gdjs.GameOverCode.GDNewObject4Objects1= [];
gdjs.GameOverCode.GDNewObject4Objects2= [];
gdjs.GameOverCode.GDlblSiteObjects1= [];
gdjs.GameOverCode.GDlblSiteObjects2= [];
gdjs.GameOverCode.GDtxtPontosObjects1= [];
gdjs.GameOverCode.GDtxtPontosObjects2= [];
gdjs.GameOverCode.GDgameover_95movimObjects1= [];
gdjs.GameOverCode.GDgameover_95movimObjects2= [];
gdjs.GameOverCode.GDgameover_95tempoObjects1= [];
gdjs.GameOverCode.GDgameover_95tempoObjects2= [];

gdjs.GameOverCode.conditionTrue_0 = {val:false};
gdjs.GameOverCode.condition0IsTrue_0 = {val:false};
gdjs.GameOverCode.condition1IsTrue_0 = {val:false};
gdjs.GameOverCode.condition2IsTrue_0 = {val:false};
gdjs.GameOverCode.condition3IsTrue_0 = {val:false};
gdjs.GameOverCode.conditionTrue_1 = {val:false};
gdjs.GameOverCode.condition0IsTrue_1 = {val:false};
gdjs.GameOverCode.condition1IsTrue_1 = {val:false};
gdjs.GameOverCode.condition2IsTrue_1 = {val:false};
gdjs.GameOverCode.condition3IsTrue_1 = {val:false};


gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbtJogarObjects1Objects = Hashtable.newFrom({"btJogar": gdjs.GameOverCode.GDbtJogarObjects1});gdjs.GameOverCode.eventsList0xafd70 = function(runtimeScene, context) {

{


gdjs.GameOverCode.condition0IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameOverCode.condition0IsTrue_0.val) {
gdjs.GameOverCode.GDtxtPontosObjects1.createFrom(runtimeScene.getObjects("txtPontos"));
{gdjs.evtTools.sound.playMusic(runtimeScene, "gameover_music.ogg", true, 100, 1);
}{for(var i = 0, len = gdjs.GameOverCode.GDtxtPontosObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDtxtPontosObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(9)));
}
}{for(var i = 0, len = gdjs.GameOverCode.GDtxtPontosObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDtxtPontosObjects1[i].setX(gdjs.evtTools.window.getCanvasWidth(runtimeScene)/2 - (gdjs.GameOverCode.GDtxtPontosObjects1[i].getWidth())/2);
}
}}

}


{

gdjs.GameOverCode.GDgameover_95tempoObjects1.createFrom(runtimeScene.getObjects("gameover_tempo"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(10)) <= 0;
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameOverCode.GDgameover_95tempoObjects1.length;i<l;++i) {
    if ( gdjs.GameOverCode.GDgameover_95tempoObjects1[i].getY() < 280 ) {
        gdjs.GameOverCode.condition1IsTrue_0.val = true;
        gdjs.GameOverCode.GDgameover_95tempoObjects1[k] = gdjs.GameOverCode.GDgameover_95tempoObjects1[i];
        ++k;
    }
}
gdjs.GameOverCode.GDgameover_95tempoObjects1.length = k;}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameOverCode.GDgameover_95tempoObjects1 */
{for(var i = 0, len = gdjs.GameOverCode.GDgameover_95tempoObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDgameover_95tempoObjects1[i].addForce(0, 90, 0.5);
}
}}

}


{

gdjs.GameOverCode.GDgameover_95tempoObjects1.createFrom(runtimeScene.getObjects("gameover_tempo"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameOverCode.GDgameover_95tempoObjects1.length;i<l;++i) {
    if ( gdjs.GameOverCode.GDgameover_95tempoObjects1[i].getY() > 280 ) {
        gdjs.GameOverCode.condition0IsTrue_0.val = true;
        gdjs.GameOverCode.GDgameover_95tempoObjects1[k] = gdjs.GameOverCode.GDgameover_95tempoObjects1[i];
        ++k;
    }
}
gdjs.GameOverCode.GDgameover_95tempoObjects1.length = k;}if (gdjs.GameOverCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameOverCode.GDgameover_95tempoObjects1 */
{for(var i = 0, len = gdjs.GameOverCode.GDgameover_95tempoObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDgameover_95tempoObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.GameOverCode.GDgameover_95tempoObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDgameover_95tempoObjects1[i].setY(280);
}
}}

}


{

gdjs.GameOverCode.GDgameover_95movimObjects1.createFrom(runtimeScene.getObjects("gameover_movim"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(11)) <= 0;
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameOverCode.GDgameover_95movimObjects1.length;i<l;++i) {
    if ( gdjs.GameOverCode.GDgameover_95movimObjects1[i].getY() < 280 ) {
        gdjs.GameOverCode.condition1IsTrue_0.val = true;
        gdjs.GameOverCode.GDgameover_95movimObjects1[k] = gdjs.GameOverCode.GDgameover_95movimObjects1[i];
        ++k;
    }
}
gdjs.GameOverCode.GDgameover_95movimObjects1.length = k;}}
if (gdjs.GameOverCode.condition1IsTrue_0.val) {
/* Reuse gdjs.GameOverCode.GDgameover_95movimObjects1 */
{for(var i = 0, len = gdjs.GameOverCode.GDgameover_95movimObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDgameover_95movimObjects1[i].addForce(0, 90, 0.5);
}
}}

}


{

gdjs.GameOverCode.GDgameover_95movimObjects1.createFrom(runtimeScene.getObjects("gameover_movim"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameOverCode.GDgameover_95movimObjects1.length;i<l;++i) {
    if ( gdjs.GameOverCode.GDgameover_95movimObjects1[i].getY() > 280 ) {
        gdjs.GameOverCode.condition0IsTrue_0.val = true;
        gdjs.GameOverCode.GDgameover_95movimObjects1[k] = gdjs.GameOverCode.GDgameover_95movimObjects1[i];
        ++k;
    }
}
gdjs.GameOverCode.GDgameover_95movimObjects1.length = k;}if (gdjs.GameOverCode.condition0IsTrue_0.val) {
/* Reuse gdjs.GameOverCode.GDgameover_95movimObjects1 */
{for(var i = 0, len = gdjs.GameOverCode.GDgameover_95movimObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDgameover_95movimObjects1[i].clearForces();
}
}{for(var i = 0, len = gdjs.GameOverCode.GDgameover_95movimObjects1.length ;i < len;++i) {
    gdjs.GameOverCode.GDgameover_95movimObjects1[i].setY(280);
}
}}

}


{

gdjs.GameOverCode.GDbtJogarObjects1.createFrom(runtimeScene.getObjects("btJogar"));

gdjs.GameOverCode.condition0IsTrue_0.val = false;
gdjs.GameOverCode.condition1IsTrue_0.val = false;
gdjs.GameOverCode.condition2IsTrue_0.val = false;
{
gdjs.GameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.GameOverCode.mapOfGDgdjs_46GameOverCode_46GDbtJogarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.GameOverCode.condition0IsTrue_0.val ) {
{
gdjs.GameOverCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.GameOverCode.condition1IsTrue_0.val ) {
{
{gdjs.GameOverCode.conditionTrue_1 = gdjs.GameOverCode.condition2IsTrue_0;
gdjs.GameOverCode.conditionTrue_1.val = context.triggerOnce(7090060);
}
}}
}
if (gdjs.GameOverCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start", false);
}}

}


}; //End of gdjs.GameOverCode.eventsList0xafd70


gdjs.GameOverCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GameOverCode.GDajudaObjects1.length = 0;
gdjs.GameOverCode.GDajudaObjects2.length = 0;
gdjs.GameOverCode.GDbtJogarObjects1.length = 0;
gdjs.GameOverCode.GDbtJogarObjects2.length = 0;
gdjs.GameOverCode.GDlblJogarObjects1.length = 0;
gdjs.GameOverCode.GDlblJogarObjects2.length = 0;
gdjs.GameOverCode.GDbackgroundObjects1.length = 0;
gdjs.GameOverCode.GDbackgroundObjects2.length = 0;
gdjs.GameOverCode.GDNewObjectObjects1.length = 0;
gdjs.GameOverCode.GDNewObjectObjects2.length = 0;
gdjs.GameOverCode.GDNewObject2Objects1.length = 0;
gdjs.GameOverCode.GDNewObject2Objects2.length = 0;
gdjs.GameOverCode.GDlblPontuacaoObjects1.length = 0;
gdjs.GameOverCode.GDlblPontuacaoObjects2.length = 0;
gdjs.GameOverCode.GDNewObject4Objects1.length = 0;
gdjs.GameOverCode.GDNewObject4Objects2.length = 0;
gdjs.GameOverCode.GDlblSiteObjects1.length = 0;
gdjs.GameOverCode.GDlblSiteObjects2.length = 0;
gdjs.GameOverCode.GDtxtPontosObjects1.length = 0;
gdjs.GameOverCode.GDtxtPontosObjects2.length = 0;
gdjs.GameOverCode.GDgameover_95movimObjects1.length = 0;
gdjs.GameOverCode.GDgameover_95movimObjects2.length = 0;
gdjs.GameOverCode.GDgameover_95tempoObjects1.length = 0;
gdjs.GameOverCode.GDgameover_95tempoObjects2.length = 0;

gdjs.GameOverCode.eventsList0xafd70(runtimeScene, context);return;
}
gdjs['GameOverCode']= gdjs.GameOverCode;
