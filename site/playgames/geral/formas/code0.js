gdjs.StartCode = {};
gdjs.StartCode.GDajudaObjects1= [];
gdjs.StartCode.GDajudaObjects2= [];
gdjs.StartCode.GDbtJogarObjects1= [];
gdjs.StartCode.GDbtJogarObjects2= [];
gdjs.StartCode.GDlblJogarObjects1= [];
gdjs.StartCode.GDlblJogarObjects2= [];
gdjs.StartCode.GDbackgroundObjects1= [];
gdjs.StartCode.GDbackgroundObjects2= [];
gdjs.StartCode.GDbtJogarObjects1= [];
gdjs.StartCode.GDbtJogarObjects2= [];
gdjs.StartCode.GDlblJogarObjects1= [];
gdjs.StartCode.GDlblJogarObjects2= [];
gdjs.StartCode.GDtituloObjects1= [];
gdjs.StartCode.GDtituloObjects2= [];
gdjs.StartCode.GDNewObjectObjects1= [];
gdjs.StartCode.GDNewObjectObjects2= [];
gdjs.StartCode.GDNewObject2Objects1= [];
gdjs.StartCode.GDNewObject2Objects2= [];

gdjs.StartCode.conditionTrue_0 = {val:false};
gdjs.StartCode.condition0IsTrue_0 = {val:false};
gdjs.StartCode.condition1IsTrue_0 = {val:false};
gdjs.StartCode.condition2IsTrue_0 = {val:false};
gdjs.StartCode.condition3IsTrue_0 = {val:false};
gdjs.StartCode.conditionTrue_1 = {val:false};
gdjs.StartCode.condition0IsTrue_1 = {val:false};
gdjs.StartCode.condition1IsTrue_1 = {val:false};
gdjs.StartCode.condition2IsTrue_1 = {val:false};
gdjs.StartCode.condition3IsTrue_1 = {val:false};


gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtJogarObjects1Objects = Hashtable.newFrom({"btJogar": gdjs.StartCode.GDbtJogarObjects1});gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDajudaObjects1Objects = Hashtable.newFrom({"ajuda": gdjs.StartCode.GDajudaObjects1});gdjs.StartCode.eventsList0xafd70 = function(runtimeScene, context) {

{


gdjs.StartCode.condition0IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.StartCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "Rezoner - Happy.ogg", true, 50, 1);
}}

}


{

gdjs.StartCode.GDbtJogarObjects1.createFrom(runtimeScene.getObjects("btJogar"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDbtJogarObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = context.triggerOnce(7066516);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Game", false);
}}

}


{

gdjs.StartCode.GDajudaObjects1.createFrom(runtimeScene.getObjects("ajuda"));

gdjs.StartCode.condition0IsTrue_0.val = false;
gdjs.StartCode.condition1IsTrue_0.val = false;
gdjs.StartCode.condition2IsTrue_0.val = false;
{
gdjs.StartCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartCode.mapOfGDgdjs_46StartCode_46GDajudaObjects1Objects, runtimeScene, true, false);
}if ( gdjs.StartCode.condition0IsTrue_0.val ) {
{
gdjs.StartCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.StartCode.condition1IsTrue_0.val ) {
{
{gdjs.StartCode.conditionTrue_1 = gdjs.StartCode.condition2IsTrue_0;
gdjs.StartCode.conditionTrue_1.val = context.triggerOnce(7067340);
}
}}
}
if (gdjs.StartCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "Ajuda");
}}

}


}; //End of gdjs.StartCode.eventsList0xafd70


gdjs.StartCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.StartCode.GDajudaObjects1.length = 0;
gdjs.StartCode.GDajudaObjects2.length = 0;
gdjs.StartCode.GDbtJogarObjects1.length = 0;
gdjs.StartCode.GDbtJogarObjects2.length = 0;
gdjs.StartCode.GDlblJogarObjects1.length = 0;
gdjs.StartCode.GDlblJogarObjects2.length = 0;
gdjs.StartCode.GDbackgroundObjects1.length = 0;
gdjs.StartCode.GDbackgroundObjects2.length = 0;
gdjs.StartCode.GDbtJogarObjects1.length = 0;
gdjs.StartCode.GDbtJogarObjects2.length = 0;
gdjs.StartCode.GDlblJogarObjects1.length = 0;
gdjs.StartCode.GDlblJogarObjects2.length = 0;
gdjs.StartCode.GDtituloObjects1.length = 0;
gdjs.StartCode.GDtituloObjects2.length = 0;
gdjs.StartCode.GDNewObjectObjects1.length = 0;
gdjs.StartCode.GDNewObjectObjects2.length = 0;
gdjs.StartCode.GDNewObject2Objects1.length = 0;
gdjs.StartCode.GDNewObject2Objects2.length = 0;

gdjs.StartCode.eventsList0xafd70(runtimeScene, context);return;
}
gdjs['StartCode']= gdjs.StartCode;
